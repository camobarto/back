﻿using Cavipetrol.SICSES.DAL.Mapeo.Reportes;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace Cavipetrol.SICSES.DAL
{
    public partial class SICSESContexto : DbContext
    {
        public SICSESContexto() : base("name=REPORTES_SICSES")
        {
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 1200;
        }

        public DbSet<ParametroUsuarioCavipetrol> ParametrosCavipetrol { get; set; }
        public DbSet<ReporteHistorico> ReporteHistorico { get; set; }
        public DbSet<ReporteIdentificacion> ReportesIdentificacion { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ReporteHistoricoMapa());
            modelBuilder.Configurations.Add(new ParametroCavipetrolMapa());
            modelBuilder.Configurations.Add(new ReporteIdentificacionMapa());
        }

        public ObjectResult<SolicitudesCreditoDTO> ConsultarSolicitudesCreditoPorFecha(DateTime FechaInicio, DateTime FechaFin)
        {
            var @params = new Object[]{
               new SqlParameter("FechaInicio", FechaInicio),
               new SqlParameter("FechaFin", FechaFin)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SolicitudesCreditoDTO>
                ("dbo.PA_CONSULTAR_SOLICITUDES_POR_FECHA @FechaInicio, @FechaFin", @params);
        }
    }
}
