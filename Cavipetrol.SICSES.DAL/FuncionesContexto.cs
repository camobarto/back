﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL
{
    public partial class FuncionesContexto : DbContext
    {
        public FuncionesContexto() : base("name=PARAMETROS_SICSES")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {


        }

        public virtual ObjectResult<DatosSMS> EnviarSMS(DatosSMS datossms)
        {
            SqlParameter param1 = datossms.LMOL_LLAVE == null ? new SqlParameter("@LMOL_LLAVE", DBNull.Value) : new SqlParameter("@LMOL_LLAVE", datossms.LMOL_LLAVE);
            SqlParameter param2 = datossms.LMOL_AFINIT == null ? new SqlParameter("@LMOL_AFINIT", DBNull.Value) : new SqlParameter("@LMOL_AFINIT", datossms.LMOL_AFINIT);
            SqlParameter param3 = datossms.LMOL_AFINOMBRE == null ? new SqlParameter("@LMOL_AFINOMBRE", DBNull.Value) : new SqlParameter("@LMOL_AFINOMBRE", datossms.LMOL_AFINOMBRE);
            SqlParameter param4 = datossms.LMOL_VALORACUM == null ? new SqlParameter("@LMOL_VALORACUM", DBNull.Value) : new SqlParameter("@LMOL_VALORACUM", datossms.LMOL_VALORACUM);
            SqlParameter param5 = datossms.LMOL_ESTADO == null ? new SqlParameter("@LMOL_ESTADO", DBNull.Value) : new SqlParameter("@LMOL_ESTADO", datossms.LMOL_ESTADO);
            SqlParameter param6 = datossms.LMOL_TRANSACCION == null ? new SqlParameter("@LMOL_TRANSACCION", DBNull.Value) : new SqlParameter("@LMOL_TRANSACCION", datossms.LMOL_TRANSACCION);
            SqlParameter param7 = datossms.Radicado == null ? new SqlParameter("@Radicado", DBNull.Value) : new SqlParameter("@Radicado",Convert.ToInt32(datossms.Radicado));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosSMS>("SICAV.dbo.PA_GuardarDatosSMS @LMOL_LLAVE,@LMOL_AFINIT, @LMOL_AFINOMBRE,@LMOL_VALORACUM,@LMOL_ESTADO,@LMOL_TRANSACCION,@Radicado", @param1, @param2, @param3, @param4, @param5, @param6, @param7);
        }

    }
}
