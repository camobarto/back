﻿using Cavipetrol.SICSES.DAL.Mapeo.Administracion;
using Cavipetrol.SICSES.Infraestructura.Model.Administracion;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Administracion;
using Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace Cavipetrol.SICSES.DAL
{

    public partial class AdministracionContexto : DbContext
    {
        string DB = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;
        public AdministracionContexto() : base("name=PARAMETROS_SICSES")
        {

        }

        public DbSet<LineaCredito> LineaCredito { get; set; }
        public DbSet<TiposCredito> TiposCredito { get; set; }

        public DbSet<PapelCavipetrol> PapelCavipetrol { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TiposLineaCreditoMapa());
            modelBuilder.Configurations.Add(new TiposCreditoMapa());
            modelBuilder.Configurations.Add(new PapelCavipetrolMapa());

        }

        public virtual ObjectResult<ViewTiposCreditos> ObtenerTiposCredito()
        {
           
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ViewTiposCreditos>("dbo.PA_ObtenerTiposCredito");
        }
        public virtual ObjectResult<ViewPapelCavipetrol> ObtenerPapelCavipetrol()
        {

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ViewPapelCavipetrol>("dbo.PA_ObtenerPapelCavipetrol");
        }

        public virtual ObjectResult<ViewTipoCreditoTipoRelacion> ObtenerTipoCreditoTipoRolRelacion()
        {

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ViewTipoCreditoTipoRelacion>("dbo.PA_ObtenerTiposCreditoTiposRolRelacion");
        }

        public virtual ObjectResult<string> AdministrarTipoCreditoTipoRol(TipoCreditoTipoRol datos)
        {
            SqlParameter param1 = datos.IdTipoCredito == 0 ? new SqlParameter("@IdTipoCredito", DBNull.Value) : new SqlParameter("@IdTipoCredito", datos.IdTipoCredito);
            SqlParameter param2 = datos.IdPapelCavipetrol == 0 ? new SqlParameter("@IdPapelCavipetrol", DBNull.Value) : new SqlParameter("@IdPapelCavipetrol", datos.IdPapelCavipetrol);
            SqlParameter param3 = datos.PlazoMaximo == "" ? new SqlParameter("@PlazoMaximo", DBNull.Value) : new SqlParameter("@PlazoMaximo", datos.PlazoMaximo);
            SqlParameter param4 = datos.IdTipoCupoMaximo == 0 ? new SqlParameter("@IdTipoCupoMaximo", DBNull.Value) : new SqlParameter("@IdTipoCupoMaximo", datos.IdTipoCupoMaximo);
            SqlParameter param5 = datos.CupoMaximo == "" ? new SqlParameter("@CupoMaximo", DBNull.Value) : new SqlParameter("@CupoMaximo", datos.CupoMaximo);
            SqlParameter param6 = datos.InteresNominalQuincenaVendida == "" ? new SqlParameter("@InteresNominalQuincenaVendida", DBNull.Value) : new SqlParameter("@InteresNominalQuincenaVendida", datos.InteresNominalQuincenaVendida);
            SqlParameter param7 = datos.InteresEfectivoAnual == "" ? new SqlParameter("@InteresEfectivoAnual", DBNull.Value) : new SqlParameter("@InteresEfectivoAnual", datos.InteresEfectivoAnual);
            SqlParameter param8 = datos.PorcentajeNovacion == "" ? new SqlParameter("@PorcentajeNovacion", DBNull.Value) : new SqlParameter("@PorcentajeNovacion", datos.PorcentajeNovacion);
            SqlParameter param9 = datos.TiempoMinimoDeAfiliacionAnhos == "" ? new SqlParameter("@TiempoMinimoDeAfiliacionAnhos", DBNull.Value) : new SqlParameter("@TiempoMinimoDeAfiliacionAnhos", datos.TiempoMinimoDeAfiliacionAnhos);
            SqlParameter param10 = datos.PlazoMinimo == "" ? new SqlParameter("@PlazoMinimo", DBNull.Value) : new SqlParameter("@PlazoMinimo", datos.PlazoMinimo);
            SqlParameter param11 = datos.IdProductoFYC == "" ? new SqlParameter("@IdProductoFYC", DBNull.Value) : new SqlParameter("@IdProductoFYC", datos.IdProductoFYC);
            SqlParameter param12 = datos.Bandera == 0 ? new SqlParameter("@Bandera", DBNull.Value) : new SqlParameter("@Bandera", datos.Bandera);
            SqlParameter param13 = datos.Usuario == "" ? new SqlParameter("@Usuario", DBNull.Value) : new SqlParameter("@Usuario", datos.Usuario);
            SqlParameter param14 = datos.IdTipoCreditoTipoRolRelacion == 0 ? new SqlParameter("@IdTipoCreditoTipoRolRelacion", DBNull.Value) : new SqlParameter("@IdTipoCreditoTipoRolRelacion", datos.IdTipoCreditoTipoRolRelacion);
            
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<string> ("dbo.PA_AdministrarTipoCreditoTipoRol @IdTipoCredito,@IdPapelCavipetrol,@PlazoMaximo,@IdTipoCupoMaximo,@CupoMaximo,@InteresNominalQuincenaVendida,@InteresEfectivoAnual,@PorcentajeNovacion,@TiempoMinimoDeAfiliacionAnhos,@PlazoMinimo,@IdProductoFYC,@Bandera ,@Usuario,@IdTipoCreditoTipoRolRelacion", @param1, @param2, @param3, param4, @param5, @param6, @param7, @param8, @param9, @param10, @param11, @param12, @param13, @param14);
        }

        // GUARDA LOS DATOS DEL NUEVO USO DE CREDITO </jarp>
        public virtual ObjectResult<UsosCreditosEcopetrolGuardar> GuardarNuevoUso(UsosCreditosEcopetrolGuardar Obj)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 1);
                SqlParameter param2 = new SqlParameter("@Descripcion", Obj.Descripcion);
                SqlParameter param3 = new SqlParameter("@IdUso", Obj.IdUso);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<UsosCreditosEcopetrolGuardar>(""+ DB + ".dbo.PA_GESTION_USOS_CREDITOS @Operacion, @Descripcion, @IdUso", @param1, param2, param3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // ACTUALIZA LOS DATOS DEL NUEVO USO DE CREDITO </jarp>
        public virtual ObjectResult<UsosCreditosEcopetrolObtener> ActualizarUso(UsosCreditosEcopetrolObtener Obj)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 3);
                SqlParameter param2 = new SqlParameter("@Descripcion", Obj.Descripcion);
                SqlParameter param3 = new SqlParameter("@IdUso", Obj.IdUso);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<UsosCreditosEcopetrolObtener>("" + DB + ".dbo.PA_GESTION_USOS_CREDITOS @Operacion, @Descripcion, @IdUso", @param1, param2, param3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // SELECCIONA DATOS CON BASE A UNAS CONDICIONES DISPUESTA EN EL SP OPERACION 2 </jarp>
        public virtual ObjectResult<UsosCreditosEcopetrolObtener> ObtenerUsosCreditoECP()
        {
            try
            {
                UsosCreditosEcopetrolObtener Obj = new UsosCreditosEcopetrolObtener();
                SqlParameter param1 = new SqlParameter("@Operacion", 2);
                SqlParameter param2 = new SqlParameter("@Descripcion", "");
                SqlParameter param3 = new SqlParameter("@IdUso", Obj.IdUso);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<UsosCreditosEcopetrolObtener>("" + DB + ".dbo.PA_GESTION_USOS_CREDITOS @Operacion, @Descripcion, @IdUso", @param1, param2, param3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // SELECCIONA DATOS CON BASE A UNAS CONDICIONES DISPUESTA EN EL SP OPERACION 2 </jarp>
        public virtual ObjectResult<DocumentosCreditosEcopetrolObtener> ObtenerDocumentosCreditoECP()
        {
            try
            {
                DocumentosCreditosEcopetrolObtener Obj = new DocumentosCreditosEcopetrolObtener();
                SqlParameter param1 = new SqlParameter("@Operacion", 2);
                SqlParameter param2 = new SqlParameter("@Descripcion", "");
                SqlParameter param3 = new SqlParameter("@IdDocumento", Obj.IdDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DocumentosCreditosEcopetrolObtener>("" + DB + ".dbo.PA_GESTION_DOCUMENTOS_CREDITOS @Operacion, @Descripcion, @IdDocumento", @param1, param2, param3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GUARDA LOS DATOS DE LOS NUEVOS DOCUMENTOS DE CREDITO </jarp>
        public virtual ObjectResult<DocumentosCreditosEcopetrolGuardar> GuardarNuevoDocumento(DocumentosCreditosEcopetrolGuardar Obj)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 1);
                SqlParameter param2 = new SqlParameter("@Descripcion", Obj.Descripcion);
                SqlParameter param3 = new SqlParameter("@IdDocumento", Obj.IdDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DocumentosCreditosEcopetrolGuardar>("" + DB + ".dbo.PA_GESTION_DOCUMENTOS_CREDITOS @Operacion, @Descripcion, @IdDocumento", @param1, param2, param3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // ACTUALIZA LOS DATOS DE LOS DOCUMENTOS DE CREDITO </jarp>
        public virtual ObjectResult<DocumentosCreditosEcopetrolObtener> ActualizarDocumento(DocumentosCreditosEcopetrolObtener Obj)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 3);
                SqlParameter param2 = new SqlParameter("@Descripcion", Obj.Descripcion);
                SqlParameter param3 = new SqlParameter("@IdDocumento", Obj.IdDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DocumentosCreditosEcopetrolObtener>("" + DB + ".dbo.PA_GESTION_DOCUMENTOS_CREDITOS @Operacion, @Descripcion, @IdDocumento", @param1, param2, param3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GUARDA LA RELACION ENTRE USOS DOCUMENTOS
        /// </summary>
        /// <param name="Obj">ENVIA COMO PARAMETROS EL IDUSO Y EL IDDOCUMENTO</param>
        /// <returns>RETORNA TRUE PARA CONFIRMAR QUE SE REALIZO CORRECTAMENTE</returns>
        public virtual ObjectResult<UsosDocumentosCreditosEcopoetrolGuardar> GuardarRelacionUsoDocumento(UsosDocumentosCreditosEcopoetrolGuardar Obj)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 1);
                SqlParameter param2 = new SqlParameter("@IdUso", Obj.IdUso);
                SqlParameter param3 = new SqlParameter("@IdDocumento", Obj.IdDocumento);
                SqlParameter param4 = new SqlParameter("@IdUsoDocumento", Obj.IdUsoDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<UsosDocumentosCreditosEcopoetrolGuardar>("" + DB + ".dbo.PA_GESTION_RELACION_USOS_DOCUMENTOS @Operacion, @IdUso,@IdDocumento, @IdUsoDocumento", @param1, param2, param3, param4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // SELECCIONA DATOS CON BASE A UNAS CONDICIONES DISPUESTA EN EL SP OPERACION 2 </jarp>
        public virtual ObjectResult<UsosDocumentosCreditosEcopoetrolObtener> ObtenerUsoDocumentoCreditoECP()
        {
            try
            {
                UsosDocumentosCreditosEcopoetrolObtener Obj = new UsosDocumentosCreditosEcopoetrolObtener();
                SqlParameter param1 = new SqlParameter("@Operacion", 2);
                SqlParameter param2 = new SqlParameter("@IdUso", Obj.IdUso);
                SqlParameter param3 = new SqlParameter("@IdDocumento", Obj.IdDocumento);
                SqlParameter param4 = new SqlParameter("@IdUsoDocumento", Obj.IdUsoDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<UsosDocumentosCreditosEcopoetrolObtener>("" + DB + ".dbo.PA_GESTION_RELACION_USOS_DOCUMENTOS @Operacion, @IdUso, @IdDocumento, @IdUsoDocumento", @param1, @param2, @param3, param4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// ACTUALIZA EL ESTO(HABILITA) DEL USO CREDITO SELECCIONADO
        /// </summary>
        /// <param name="Obj">TRAE EL ID DE LA RELACION PARA ACTUALIZARLA</param>
        /// <returns></returns>
        public virtual ObjectResult<UsosDocumentosCreditosEcopoetrolObtener> ActualizarEstadoUsoDocumentoHabilitar(UsosDocumentosCreditosEcopoetrolObtener Obj)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 3);
                SqlParameter param2 = new SqlParameter("@IdUso", Obj.IdUsoDocumento);
                SqlParameter param3 = new SqlParameter("@IdDocumento", Obj.IdDocumento);
                SqlParameter param4 = new SqlParameter("@IdUsoDocumento", Obj.IdUsoDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<UsosDocumentosCreditosEcopoetrolObtener>("" + DB + ".dbo.PA_GESTION_RELACION_USOS_DOCUMENTOS @Operacion, @IdUso, @IdDocumento, @IdUsoDocumento", @param1, param2, param3, param4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// ACTUALIZA EL ESTO(DESHABILITA) DEL USO CREDITO SELECCIONADO
        /// </summary>
        /// <param name="Obj">TRAE EL ID DE LA RELACION PARA ACTUALIZARLA</param>
        /// <returns></returns>
        public virtual ObjectResult<UsosDocumentosCreditosEcopoetrolObtener> ActualizarEstadoUsoDocumentoDeshabilitar(UsosDocumentosCreditosEcopoetrolObtener Obj)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 4);
                SqlParameter param2 = new SqlParameter("@IdUso", Obj.IdUsoDocumento);
                SqlParameter param3 = new SqlParameter("@IdDocumento", Obj.IdDocumento);
                SqlParameter param4 = new SqlParameter("@IdUsoDocumento", Obj.IdUsoDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<UsosDocumentosCreditosEcopoetrolObtener>("" + DB + ".dbo.PA_GESTION_RELACION_USOS_DOCUMENTOS @Operacion, @IdUso, @IdDocumento, @IdUsoDocumento", @param1, param2, param3, param4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<UsosDocumentosCreditosEcopoetrolObtener> ActualizarUsoDocumento(UsosDocumentosCreditosEcopoetrolObtener Obj)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 5);
                SqlParameter param2 = new SqlParameter("@IdUso", Obj.IdUso);
                SqlParameter param3 = new SqlParameter("@IdDocumento", Obj.IdDocumento);
                SqlParameter param4 = new SqlParameter("@IdUsoDocumento", Obj.IdUsoDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<UsosDocumentosCreditosEcopoetrolObtener>("" + DB + ".dbo.PA_GESTION_RELACION_USOS_DOCUMENTOS @Operacion, @IdUso, @IdDocumento, @IdUsoDocumento", @param1, param2, param3, param4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // SELECCIONA DATOS CON BASE A UNAS CONDICIONES DISPUESTA EN EL SP OPERACION 6 </jarp>
        public virtual ObjectResult<UsosDocumentosCreditosEcopoetrolObtener> ObtenerDocumentoxUsoECP(int IdUso)
        {
            try
            {
                UsosDocumentosCreditosEcopoetrolObtener Obj = new UsosDocumentosCreditosEcopoetrolObtener();
                SqlParameter param1 = new SqlParameter("@Operacion", 6);
                SqlParameter param2 = new SqlParameter("@IdUso", IdUso);
                SqlParameter param3 = new SqlParameter("@IdDocumento", Obj.IdDocumento);
                SqlParameter param4 = new SqlParameter("@IdUsoDocumento", Obj.IdUsoDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<UsosDocumentosCreditosEcopoetrolObtener>("" + DB + ".dbo.PA_GESTION_RELACION_USOS_DOCUMENTOS @Operacion, @IdUso, @IdDocumento, @IdUsoDocumento", @param1, @param2, @param3, param4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
