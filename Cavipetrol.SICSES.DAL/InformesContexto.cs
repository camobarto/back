﻿using Cavipetrol.SICSES.DAL.Mapeo.Informes;
using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL
{
    public partial class InformesContexto : DbContext
    {
        public InformesContexto(string db = "name= SICAV") : base(db)
        {
			((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 1200;
		}
        public DbSet<DatosIva> DatosGarantias { get; set; }
        public DbSet<Nota> Nota { get; set; }
        public DbSet<Items> Items { get; set; }
        public DbSet<Etiquetas> Etiquetas { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new NotaMapa());
            modelBuilder.Configurations.Add(new ItemsMapa());
            modelBuilder.Configurations.Add(new EtiquetasMapa());
            modelBuilder.Configurations.Add(new RetencionMOVMapa());
        }

        public virtual ObjectResult<RetencionProveedores> ObtenerInformacionGarantia(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "")
        {
            SqlParameter param1 = IdOpcionInfo == null ?
            new SqlParameter("@BANDERA", DBNull.Value) :
            new SqlParameter("@BANDERA", IdOpcionInfo);

            SqlParameter param2 = Nit == null ?
            new SqlParameter("@NIT", DBNull.Value) :
            new SqlParameter("@NIT", Nit);
            
            SqlParameter param3 = Fc_Desde == null ?
            new SqlParameter("@DESDE", DBNull.Value) :
            new SqlParameter("@DESDE", Fc_Desde);

            SqlParameter param4 = Fc_Hasta == null ?
            new SqlParameter("@HASTA", DBNull.Value) :
            new SqlParameter("@HASTA", Fc_Hasta);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<RetencionProveedores>("dbo.PA_CONSULTA_CERTIFICADO_PROVEEDOR @BANDERA,@NIT,@DESDE,@HASTA", @param1, @param2, @param3, @param4);

        }
        public virtual ObjectResult<RetencionProveedores_ICA> ObtenerInformacionGarantia2(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "")
        {
            SqlParameter param1 = IdOpcionInfo == null ?
            new SqlParameter("@BANDERA", DBNull.Value) :
            new SqlParameter("@BANDERA", IdOpcionInfo);

            SqlParameter param2 = Nit == null ?
            new SqlParameter("@NIT", DBNull.Value) :
            new SqlParameter("@NIT", Nit);

            SqlParameter param3 = Fc_Desde == null ?
            new SqlParameter("@DESDE", DBNull.Value) :
            new SqlParameter("@DESDE", Fc_Desde);

            SqlParameter param4 = Fc_Hasta == null ?
            new SqlParameter("@HASTA", DBNull.Value) :
            new SqlParameter("@HASTA", Fc_Hasta);

            SqlParameter param5 = Bimestre == null ?
            new SqlParameter("@BIMESTRE", DBNull.Value) :
            new SqlParameter("@BIMESTRE", Bimestre);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<RetencionProveedores_ICA>("dbo.PA_CONSULTA_CERTIFICADO_PROVEEDOR @BANDERA,@NIT,@DESDE,@HASTA,@BIMESTRE", @param1, @param2, @param3, @param4, @param5);

        }
        public virtual ObjectResult<RetencionProveedores_IVA> ObtenerInformacionGarantia3(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "")
        {
            SqlParameter param1 = IdOpcionInfo == null ?
            new SqlParameter("@BANDERA", DBNull.Value) :
            new SqlParameter("@BANDERA", IdOpcionInfo);

            SqlParameter param2 = Nit == null ?
            new SqlParameter("@NIT", DBNull.Value) :
            new SqlParameter("@NIT", Nit);

            SqlParameter param3 = Fc_Desde == null ?
            new SqlParameter("@DESDE", DBNull.Value) :
            new SqlParameter("@DESDE", Fc_Desde);

            SqlParameter param4 = Fc_Hasta == null ?
            new SqlParameter("@HASTA", DBNull.Value) :
            new SqlParameter("@HASTA", Fc_Hasta);

            SqlParameter param5 = Bimestre == null ?
            new SqlParameter("@BIMESTRE", DBNull.Value) :
            new SqlParameter("@BIMESTRE", Bimestre);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<RetencionProveedores_IVA>("dbo.PA_CONSULTA_CERTIFICADO_PROVEEDOR @BANDERA,@NIT,@DESDE,@HASTA,@BIMESTRE", @param1, @param2, @param3, @param4, @param5);

        }        
        public virtual void ActualizaInfoProveedores(int bandera)
        {
            SqlParameter param1 = new SqlParameter("@BANDERA", bandera);
            SqlParameter param2 = new SqlParameter("@NIT", "");
            SqlParameter param3 = new SqlParameter("@DESDE", "");
            SqlParameter param4 = new SqlParameter("@HASTA", "");
            SqlParameter param5 = new SqlParameter("@BIMESTRE", "");

            ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("dbo.PA_CONSULTA_CERTIFICADO_PROVEEDOR @BANDERA,@NIT,@DESDE,@HASTA,@BIMESTRE", @param1, @param2, @param3, @param4, @param5);
            // ExecuteStoreCommand
        }
        public virtual ObjectResult<ProveedoresFaltantes> GeneraProveedoresFaltantes(int bandera)
        {
            SqlParameter param1 = new SqlParameter("@BANDERA", bandera);
            SqlParameter param2 = new SqlParameter("@NIT", "");
            SqlParameter param3 = new SqlParameter("@DESDE", "");
            SqlParameter param4 = new SqlParameter("@HASTA", "");

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ProveedoresFaltantes>("dbo.PA_CONSULTA_CERTIFICADO_PROVEEDOR @BANDERA,@NIT,@DESDE,@HASTA", @param1, @param2, @param3, @param4);
            
        }
        public virtual ObjectResult<CuentaContable> ObtenerCuentaContable(int bandera = 1)
        {
            SqlParameter param1 = bandera == 0 ?
            new SqlParameter("@BANDERA", DBNull.Value) :
            new SqlParameter("@BANDERA", bandera);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CuentaContable>("dbo.PA_CONSULTA_CUENTA_CONTABLE @BANDERA", @param1);

        }
        public virtual ObjectResult<AuxTercero> ConsultaAuxTercero(string mes, string anio, string cuenta)
        {
            SqlParameter param1 = mes == null ?
            new SqlParameter("@MES", DBNull.Value) :
            new SqlParameter("@MES", mes);

            SqlParameter param2 = anio == null ?
            new SqlParameter("@ANIO", DBNull.Value) :
            new SqlParameter("@ANIO", anio);

            SqlParameter param3 = cuenta == null ?
            new SqlParameter("@BANDERA", DBNull.Value) :
            new SqlParameter("@BANDERA", cuenta);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<AuxTercero>("dbo.PA_CONSULTA_AUX_TERCERO @MES, @ANIO, @BANDERA", @param1, @param2, @param3);
        }
        public virtual ObjectResult<AuditaCuenta> ConsultaAuditaCuenta(string Fc_Ini, string Fc_Fin, string Cuenta)
        {
            SqlParameter param1 = Fc_Ini == null ?
            new SqlParameter("@FECHA_Inicio", DBNull.Value) :
            new SqlParameter("@FECHA_Inicio", Fc_Ini);

            SqlParameter param2 = Fc_Fin == null ?
            new SqlParameter("@FECHA_Final", DBNull.Value) :
            new SqlParameter("@FECHA_Final", Fc_Fin);

            SqlParameter param3 = Cuenta == null ?
            new SqlParameter("@CUENTA", DBNull.Value) :
            new SqlParameter("@CUENTA", Cuenta);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<AuditaCuenta>("dbo.PA_AUDITA_CUENTA @FECHA_Inicio, @FECHA_Final, @CUENTA", @param1, @param2, @param3);

        }
        public virtual ObjectResult<GMF_BUno> ConsutaGMF_BUno(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            SqlParameter param1 = Fc_Ini == null ?
            new SqlParameter("@fechaIni", DBNull.Value) :
            new SqlParameter("@fechaIni", Fc_Ini);

            SqlParameter param2 = Fc_Fin == null ?
            new SqlParameter("@fechaFin", DBNull.Value) :
            new SqlParameter("@fechaFin", Fc_Fin);

            SqlParameter param3 = Bandera == 0 ?
            new SqlParameter("@BANDERA", DBNull.Value) :
            new SqlParameter("@BANDERA", Bandera);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<GMF_BUno>("dbo.PA_GENERA_GMF @fechaIni, @fechaFin, @BANDERA", @param1, @param2, @param3);

        }
        public virtual ObjectResult<GMF_BDos> ConsutaGMF_BDos(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            SqlParameter param1 = Fc_Ini == null ?
            new SqlParameter("@fechaIni", DBNull.Value) :
            new SqlParameter("@fechaIni", Fc_Ini);

            SqlParameter param2 = Fc_Fin == null ?
            new SqlParameter("@fechaFin", DBNull.Value) :
            new SqlParameter("@fechaFin", Fc_Fin);

            SqlParameter param3 = Bandera == 0 ?
            new SqlParameter("@BANDERA", DBNull.Value) :
            new SqlParameter("@BANDERA", Bandera);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<GMF_BDos>("dbo.PA_GENERA_GMF @fechaIni, @fechaFin, @BANDERA", @param1, @param2, @param3);

        }
        public virtual ObjectResult<GMF_BTres> ConsutaGMF_BTres(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            SqlParameter param1 = Fc_Ini == null ?
            new SqlParameter("@fechaIni", DBNull.Value) :
            new SqlParameter("@fechaIni", Fc_Ini);

            SqlParameter param2 = Fc_Fin == null ?
            new SqlParameter("@fechaFin", DBNull.Value) :
            new SqlParameter("@fechaFin", Fc_Fin);

            SqlParameter param3 = Bandera == 0 ?
            new SqlParameter("@BANDERA", DBNull.Value) :
            new SqlParameter("@BANDERA", Bandera);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<GMF_BTres>("dbo.PA_GENERA_GMF @fechaIni, @fechaFin, @BANDERA", @param1, @param2, @param3);

        }

        public virtual ObjectResult<SaldoInicialCarteraEcopetrol> ObtenerSaldoInicialInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            DateTime Fechainicio = DateTime.ParseExact(FechaDesde, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime FechaFin = DateTime.ParseExact(FechaHasta, "dd/MM/yyyy", CultureInfo.InvariantCulture); 
            SqlParameter param1 = TipoNit == null ?  new SqlParameter("@TIPONIT", DBNull.Value) :  new SqlParameter("@TIPONIT", TipoNit);
            SqlParameter param2 = NumeroIdentificacion == null ? new SqlParameter("@NUMNIT", DBNull.Value) :  new SqlParameter("@NUMNIT", NumeroIdentificacion);
            SqlParameter param3 = producto == null ? new SqlParameter("@PRODUCTO", DBNull.Value) :    new SqlParameter("@PRODUCTO", producto);
            SqlParameter param4 = Doctipo == null ? new SqlParameter("@DOCTIPO", DBNull.Value) : new SqlParameter("@DOCTIPO", Doctipo);
            SqlParameter param5 = Docunumero == 0 ? new SqlParameter("@DOCNUMERO", DBNull.Value) : new SqlParameter("@DOCNUMERO", Docunumero);
            SqlParameter param6 = Fechainicio == null ? new SqlParameter("@DESDE", DBNull.Value) : new SqlParameter("@DESDE", Fechainicio);
            SqlParameter param7 = FechaFin == null ? new SqlParameter("@HASTA", DBNull.Value) : new SqlParameter("@HASTA", FechaFin);
            SqlParameter param8 = Bandera == 0 ? new SqlParameter("@BANDERA", DBNull.Value) : new SqlParameter("@BANDERA", Bandera);

            

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SaldoInicialCarteraEcopetrol>("PROCESOS_ECP.dbo.ECOPETROL_CONSULTA_DOCUMENTO_FECHAS @TIPONIT, @NUMNIT,@PRODUCTO,@DOCTIPO,@DOCNUMERO,@DESDE,@HASTA,@BANDERA", @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8);

        }
        public virtual ObjectResult<EncabezadoCarteraEcopetrol> ObtenerEncabezadoInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            DateTime Fechainicio = DateTime.ParseExact(FechaDesde, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime FechaFin = DateTime.ParseExact(FechaHasta, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            SqlParameter param1 = TipoNit == null ? new SqlParameter("@TIPONIT", DBNull.Value) : new SqlParameter("@TIPONIT", TipoNit);
            SqlParameter param2 = NumeroIdentificacion == null ? new SqlParameter("@NUMNIT", DBNull.Value) : new SqlParameter("@NUMNIT", NumeroIdentificacion);
            SqlParameter param3 = producto == null ? new SqlParameter("@PRODUCTO", DBNull.Value) : new SqlParameter("@PRODUCTO", producto);
            SqlParameter param4 = Doctipo == null ? new SqlParameter("@DOCTIPO", DBNull.Value) : new SqlParameter("@DOCTIPO", Doctipo);
            SqlParameter param5 = Docunumero == 0 ? new SqlParameter("@DOCNUMERO", DBNull.Value) : new SqlParameter("@DOCNUMERO", Docunumero);
            SqlParameter param6 = Fechainicio == null ? new SqlParameter("@DESDE", DBNull.Value) : new SqlParameter("@DESDE", Fechainicio);
            SqlParameter param7 = FechaFin == null ? new SqlParameter("@HASTA", DBNull.Value) : new SqlParameter("@HASTA", FechaFin);
            SqlParameter param8 = Bandera == 0 ? new SqlParameter("@BANDERA", DBNull.Value) : new SqlParameter("@BANDERA", Bandera);


            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<EncabezadoCarteraEcopetrol>("PROCESOS_ECP.dbo.ECOPETROL_CONSULTA_DOCUMENTO_FECHAS @TIPONIT, @NUMNIT,@PRODUCTO,@DOCTIPO,@DOCNUMERO,@DESDE,@HASTA,@BANDERA", @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8);

        }
        public virtual ObjectResult<PlanPagosCarteraEcopetrol> ObtenerPlanPagosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            DateTime Fechainicio = DateTime.ParseExact(FechaDesde, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime FechaFin = DateTime.ParseExact(FechaHasta, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            SqlParameter param1 = TipoNit == null ? new SqlParameter("@TIPONIT", DBNull.Value) : new SqlParameter("@TIPONIT", TipoNit);
            SqlParameter param2 = NumeroIdentificacion == null ? new SqlParameter("@NUMNIT", DBNull.Value) : new SqlParameter("@NUMNIT", NumeroIdentificacion);
            SqlParameter param3 = producto == null ? new SqlParameter("@PRODUCTO", DBNull.Value) : new SqlParameter("@PRODUCTO", producto);
            SqlParameter param4 = Doctipo == null ? new SqlParameter("@DOCTIPO", DBNull.Value) : new SqlParameter("@DOCTIPO", Doctipo);
            SqlParameter param5 = Docunumero == 0 ? new SqlParameter("@DOCNUMERO", DBNull.Value) : new SqlParameter("@DOCNUMERO", Docunumero);
            SqlParameter param6 = Fechainicio == null ? new SqlParameter("@DESDE", DBNull.Value) : new SqlParameter("@DESDE", Fechainicio);
            SqlParameter param7 = FechaFin == null ? new SqlParameter("@HASTA", DBNull.Value) : new SqlParameter("@HASTA", FechaFin);
            SqlParameter param8 = Bandera == 0 ? new SqlParameter("@BANDERA", DBNull.Value) : new SqlParameter("@BANDERA", Bandera);


            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<PlanPagosCarteraEcopetrol>("PROCESOS_ECP.dbo.ECOPETROL_CONSULTA_DOCUMENTO_FECHAS @TIPONIT, @NUMNIT,@PRODUCTO,@DOCTIPO,@DOCNUMERO,@DESDE,@HASTA,@BANDERA", @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8);

        }
        public virtual ObjectResult<MovimientosCarteraEcopetrol> ObtenerMovimientosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            DateTime Fechainicio = DateTime.ParseExact(FechaDesde, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime FechaFin = DateTime.ParseExact(FechaHasta, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            SqlParameter param1 = TipoNit == null ? new SqlParameter("@TIPONIT", DBNull.Value) : new SqlParameter("@TIPONIT", TipoNit);
            SqlParameter param2 = NumeroIdentificacion == null ? new SqlParameter("@NUMNIT", DBNull.Value) : new SqlParameter("@NUMNIT", NumeroIdentificacion);
            SqlParameter param3 = producto == null ? new SqlParameter("@PRODUCTO", DBNull.Value) : new SqlParameter("@PRODUCTO", producto);
            SqlParameter param4 = Doctipo == null ? new SqlParameter("@DOCTIPO", DBNull.Value) : new SqlParameter("@DOCTIPO", Doctipo);
            SqlParameter param5 = Docunumero == 0 ? new SqlParameter("@DOCNUMERO", DBNull.Value) : new SqlParameter("@DOCNUMERO", Docunumero);
            SqlParameter param6 = Fechainicio == null ? new SqlParameter("@DESDE", DBNull.Value) : new SqlParameter("@DESDE", Fechainicio);
            SqlParameter param7 = FechaFin == null ? new SqlParameter("@HASTA", DBNull.Value) : new SqlParameter("@HASTA", FechaFin);
            SqlParameter param8 = Bandera == 0 ? new SqlParameter("@BANDERA", DBNull.Value) : new SqlParameter("@BANDERA", Bandera);


            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<MovimientosCarteraEcopetrol>("PROCESOS_ECP.dbo.ECOPETROL_CONSULTA_DOCUMENTO_FECHAS @TIPONIT, @NUMNIT,@PRODUCTO,@DOCTIPO,@DOCNUMERO,@DESDE,@HASTA,@BANDERA", @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8);

        }

        public virtual ObjectResult<TipoDocumentoPorCedula> ObtenerTipoDocumentoPorCedula(string TipoIdentificacion, int Cedula)
        {
            SqlParameter param1 = TipoIdentificacion == null ? new SqlParameter("@TIPONIT", DBNull.Value) : new SqlParameter("@TIPONIT", TipoIdentificacion);
            SqlParameter param2 = Cedula == 0 ?  new SqlParameter("@NUMNIT", DBNull.Value) : new SqlParameter("@NUMNIT", Cedula);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<TipoDocumentoPorCedula>("PROCESOS_ECP.dbo.ECOPETROL_CONSULTA_NIT @TIPONIT, @NUMNIT", @param1, @param2);

        }
        public virtual ObjectResult<FacturaElectronica> ObtenerFacturaElectronica(string Factura, string Servidor)
        {
            SqlParameter param1 = Factura == null ? new SqlParameter("@Factura", DBNull.Value) : new SqlParameter("@Factura", Factura);
            SqlParameter param2 = Servidor == null ? new SqlParameter("@Servidor", DBNull.Value) : new SqlParameter("@Servidor", Servidor);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<FacturaElectronica>("PA_GENERAR_FACTURA @Factura, @Servidor", @param1, @param2);
        }
        public virtual ObjectResult<Certificaciones_Ingresos_Retenciones> Certificaciones(string NumeroIdentificacion, DateTime FechaCorte)
        {
            SqlParameter param1 = FechaCorte == null ? new SqlParameter("@fechaPeriodo", DBNull.Value) : new SqlParameter("@fechaPeriodo", FechaCorte);
            SqlParameter param2 = NumeroIdentificacion == null ? new SqlParameter("@identificacion", DBNull.Value) : new SqlParameter("@identificacion", NumeroIdentificacion);
            var j = ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Certificaciones_Ingresos_Retenciones>("PA_OBTENER_DATOS_CERTIFICACIONES @fechaPeriodo, @identificacion", @param1, @param2);
            return j;
        }

    }
}
