﻿using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public interface IPolizasExternasUnidadTrabajo
    {
        bool InsertarDatosPolizasExternas(List<PolizasExternas> datosPolizasExternas);

        bool AdministrarPolizasExternas(ViewModelPolizasExternas datosPolizasExternas);

        bool ConsultarHistorico(string NumeroDocumento, string NumeroPoliza, string ProductoAmparado, string DocumentoProducto);


        IEnumerable<ViewModelPolizasExternas> ConsultarPolizasExternas(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza, int DiasVencimiento,string EstadoPoliza);

        IEnumerable<Aseguradoras> ObtenerAseguradoras();

        IEnumerable<TipoPoliza> ObtenerTipoPoliza();

        bool EliminarPolizaExterna(string TipoIdentificacion, string NumeroIdentificacion, string Aseguradora, string NumeroPoliza);

        IEnumerable<HistoricoPolizasExternas> ConsultarHistoricoPoliza(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza);

        RespuestaValidacion ValidarPolizasExternas(string aseguradora, string tipopoliza, string producto, string documento,string nit);

        IEnumerable<Infraestructura.Model.ProductoPorAsociado> ObtenerProductoPorAsociado(string Cedula);
    }
}
