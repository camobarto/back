﻿using System;
using System.Collections.Generic;
using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.Model.ReportesUIAF;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public class ReportesUIAFUnidadTrabajo : IReportesUIAFUnidadTrabajo
    {
        private readonly SICSESContexto _context;
        private readonly IRepositorioReportesUIAF _repositorioReportes;

        public ReportesUIAFUnidadTrabajo()
        {
            _context = new SICSESContexto();
            _repositorioReportes = new RepositorioReportesUIAF(_context);
        } 

        public IEnumerable<ReporteTransaccionesEnEfectivo> ObtenerReporteTransaccionesEnEfectivo(DateTime fechaInicio, DateTime fechaFin)
        {
            return _repositorioReportes.ObtenerReporteTransaccionesEnEfectivo(fechaInicio, fechaFin);
        }

        public IEnumerable<ReporteTransaccionesEnEfectivoRiesgos> ObtenerReporteTransaccionesEnEfectivoRiesgos(DateTime fechaInicio, DateTime fechaFin)
        {
            return _repositorioReportes.ObtenerReporteTransaccionesEnEfectivoRiesgos(fechaInicio, fechaFin);
        }

        public IEnumerable<ReporteProductosEconomiaSolidaria> ObtenerReporteProductosEconomiaSolidaria(DateTime fechaCorte)
        {
            return _repositorioReportes.ObtenerReporteProductosEconomiaSolidaria(fechaCorte);
        }
    }
}
