﻿using System;
using System.Collections.Generic;
using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public class ReportesUnidadTrabajo : IReportesUnidadTrabajo
    {
        private readonly SICSESContexto _context;
        private readonly IRepositorioReportes _repositorioReportes;

        public ReportesUnidadTrabajo()
        {
            _context = new SICSESContexto();
            _repositorioReportes = new RepositorioReportes(_context);
        }

        public bool ActulizarParametrosCavipetrol(ParametroUsuarioCavipetrol registroParametrosCavipetrol)
        {
            _repositorioReportes.ActulizarParametrosCavipetrol(registroParametrosCavipetrol);
            return GuardarCambios();
        }

        public bool InsertarHistoricoReporte(ReporteHistorico reporteHistorico)
        {
            _repositorioReportes.InsertarHistoricoReporte(reporteHistorico);

            return GuardarCambios();
        }

        public IEnumerable<ParametroUsuarioCavipetrol> ObtenerParametrosRegistroCavipetrol()
        {
            return _repositorioReportes.ObtenerParametrosRegistroCavipetrol();
        }

        public IEnumerable<ReporteInformacionEstadistica> ObtenerReporteInfromacionEstadisticaInicial(DateTime fecha_perido)
        {
            return _repositorioReportes.ObtenerReporteInformacionEstadisticaInicial(fecha_perido);
        }

        public IEnumerable<ReporteIndividualDeCarteraDeCredito> ObtenerReporteindividualDeCarteraDeCredito(int Anio, int Mes)
        {
            return _repositorioReportes.ObtenerReporteindividualDeCarteraDeCredito(Anio, Mes);
        }

        private bool GuardarCambios()
        {
            int operations = _context.SaveChanges();
            return operations > 0 ? true : false;
        }

        public IEnumerable<ReporteOrganosDireccionyControl> ObtenerReporteOrganosDirreccionControl()
        {
            return _repositorioReportes.ObtenerReporteOrganosDirreccionControl();
        }

        public IEnumerable<ReporteUsuarios> ObtenerReporteUsuarios(DateTime fechaCorte)
        {
            return _repositorioReportes.ObtenerReporteUsuarios(fechaCorte);
        }

        public IEnumerable<ReporteRedOficinasYCorresponsalesNoBancarios> ObtenerReporteRedOficinasYCorresponsalesNoBancarios()
        {
            return _repositorioReportes.ObtenerReporteRedOficinasYCorresponsalesNoBancarios();
        }

        public ReporteHistorico ConsultarHistorico(int idReporte, int mes, int ano)
        {
            return _repositorioReportes.ConsultarHistorico(idReporte, mes, ano);
        }

        public IEnumerable<ReporteParentescos> ObtenerInformeParentescos()
        {
            return _repositorioReportes.ObtenerInformeParentescos();
        }

        public IEnumerable<ReporteRelacionBienesRecibosPago> ObtenerRelacionBienesPagoRecibidos()
        {
            return _repositorioReportes.ObtenerRelacionBienesPagoRecibidos();
        }
        public IEnumerable<ReporteIndividualCarteraCredito> ObtenerInformacionCarteraCredito(string anho, string mes)
        {
            return _repositorioReportes.ObtenerInformacionCarteraCredito(anho, mes);
        }

        public IEnumerable<ReporteInformeIndividualCaptaciones> ObtenerInformeIndividualCaptaciones(DateTime fechaCorte)
        {
            return _repositorioReportes.ObtenerInformeIndividualCaptaciones(fechaCorte);
        }


        public IEnumerable<ReportePUC> ObtenerInformePUC(DateTime fechaCorte, int periodo)
        {
            return _repositorioReportes.ObtenerInformePUC(fechaCorte, periodo);                                                        
        }

        public IEnumerable<ReporteAportesContribuciones> ObtenerReporteAportesContribuciones(DateTime fechaCorte)
        {
            return _repositorioReportes.ObtenerReporteAportesContribuciones(fechaCorte);
        }

        public IEnumerable<ReporteRelacionPropiedadesPlantaEquipo> ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(DateTime fechaCorte)
        {
            return _repositorioReportes.ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(fechaCorte);
        }

        public IEnumerable<ReporteRelacionInversiones> ObtenerReporteInformeRelacionInversiones(DateTime fecha_perido)
        {
            return _repositorioReportes.ObtenerReporteInformeRelacionInversiones(fecha_perido);
        }

        public IEnumerable<ReporteProductosOfrecidos> ObtenerReporte4Uiaf()
        {
            return _repositorioReportes.ObtenerReporte4Uiaf();
        }
        public IEnumerable<ClasificacionExcedentes> ObtenerAplicacionExcedentes(DateTime fechaCorte)
        {
            return _repositorioReportes.ObtenerAplicacionExcedentes(fechaCorte);
        }
        public IEnumerable<ReporteRetiroeIngresoAsociados>ObtenerInformeRetiroeIngresoAsociados(DateTime fechaCorte)
        {
            return _repositorioReportes.ObtenerInformeRetiroeIngresoAsociados(fechaCorte);
        }

        public IEnumerable<ReporteDetalleInformacionEstadistica> ObtenerReporteDetalleInfromacionEstadistica(DateTime fecha_perido)
        {
            return _repositorioReportes.ObtenerReporteDetalleInformacionEstadistica(fecha_perido);
        }
    }
}
