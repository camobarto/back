﻿using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Historico;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;
using Cavipetrol.SICSES.Infraestructura.Model.fyc.Riesgos;
using Cavipetrol.SICSES.Infraestructura.Model.Seguridad;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public class SicavUnidadTrabajo : ISicavUnidadTrabajo
    {
        private readonly IRepositorioSICAV _repositorioSicav;
        private readonly SICAVContexto _contexto;
        private readonly ParametrosCoreContexto _paremetrosCorecontexto;

        public SicavUnidadTrabajo()
        {
            _contexto = new SICAVContexto();       
            _repositorioSicav = new RepositorioSICAV(_contexto);
        }
        public Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago GuardarCapacidadPago(Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago)
        {
            return _repositorioSicav.GuardarCapacidadPago(capacidadPago);
        }
        public RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> GuardarSolicitudCredito(Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito solicitudCredito,
                                                                                                      Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago,
                                                                                                      Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras horasExtras,
                                                                                                      List<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCreditoFormaPago> listaSolicitudFormaPago,
                                                                                                      List<FormCapacidadPagoCodeudoresGrupos> listaFormCapacidadPagoCodeudoresGrupos,
                                                                                                       List<FormCapacidadPagoTerceroCodeudor> listaFormCapacidadPagoTerceroCodeudor)
        {
            return _repositorioSicav.GuardarSolicitudCredito(solicitudCredito, capacidadPago, horasExtras, listaSolicitudFormaPago, listaFormCapacidadPagoCodeudoresGrupos,listaFormCapacidadPagoTerceroCodeudor);
        }
        public CapacidadPagoHorasExtras GuardarCapacidadPagoHorasExtras(CapacidadPagoHorasExtras horasExtras)
        {
            return _repositorioSicav.GuardarCapacidadPagoHorasExtras(horasExtras);
        }
        public IEnumerable<CapacidadPagoHorasExtras> ConsultarCapacidadPagoHorasExtras(int idCapacidadPago = 0) {
            return _repositorioSicav.ConsultarCapacidadPagoHorasExtras(idCapacidadPago);
        }
        public void GuardarHistorico(Historico historico) {
            _repositorioSicav.GuardarHistorico(historico);
        }

        public IEnumerable<PerfilMenuRelacion> ObtenerMenu(int idPerfil)
        {
            return _repositorioSicav.ObtenerMenu(idPerfil);
        }
        public IEnumerable<Menu> MenuCompleto ()
        {
            return _repositorioSicav.MenuCompleto();
        }
        public IEnumerable<PerfilMenuRelacion> ObtenerPerfilMenuRelacion()
        {
            return _repositorioSicav.ObtenerPerfilMenuRelacion();
        }
        public IEnumerable<AsegurabilidadOpcion> ObtenerAsegurabilidadOpciones(bool? noAsegurable)
        {
            return _repositorioSicav.ObtenerAsegurabilidadOpciones(noAsegurable);
        }

        public IEnumerable<ViewModelSolicitudCredito> ObtenerSolicitudCredito(int? idSolicitud = null,
                                                                     int? idTipoCredito = null,
                                                                     string idTipoIdentificacion = null,
                                                                     string numeroIdentificacionAsociado = null,
                                                                     short? idAseguabilidadOpcion = null,
                                                                     string idEstadoSolicitudCredito = null,
                                                                     string usuarioCrea = null,
                                                                     string usuarioModifica = null,
                                                                     long? consecutivoSolicitud = null,
                                                                     int? idPerfil = null)
        {
            return _repositorioSicav.ObtenerSolicitudCredito(idSolicitud, idTipoCredito, idTipoIdentificacion, numeroIdentificacionAsociado, idAseguabilidadOpcion, idEstadoSolicitudCredito, usuarioCrea, usuarioModifica, consecutivoSolicitud, idPerfil);
        }
        public CapacidadPago ObtenerSolicitudCapacidadPago(int idSolicitud)
        {
            return _repositorioSicav.ObtenerSolicitudCapacidadPago(idSolicitud);
        }
        public SolicitudCredito CambiarEstadoSolicitud(int idSolicitud)
        {
            return _repositorioSicav.CambiarEstadoSolicitud(idSolicitud);
        }
        public SolicitudCredito ObtenerSolicitudCredito(int idSolicitud)
        {
            return _repositorioSicav.ObtenerSolicitudCredito(idSolicitud);
        }

        public void ActualizarEstadoSolicitudCredito(List<Parametros> causalesNegacion, SolicitudCredito solicitudCredito, string UsuRegistra, Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago)
        {
            _repositorioSicav.ActualizarEstadoSolicitudCredito(causalesNegacion, solicitudCredito, UsuRegistra, capacidadPago);
        }

        public IEnumerable<SolicitudCreditoCausalesNegacion> ObtenerCausalesNegacion()
        {
            return _repositorioSicav.ObtenerCausalesNegacion();
        }

        public RespuestaNegocio<string> GuardarReferenciasAsociado(List<ReferenciasAsociado> referencias)
        {
            return _repositorioSicav.GuardarReferenciasAsociado(referencias);
        }

        public List<ReferenciasAsociado> ObtenerReferenciasAsociado(string tipo, string numero)
        {
            return _repositorioSicav.ObtenerReferenciasAsociado(tipo, numero);
        }

        public Usuario ObtenerUsuario(string uniqueName)
        {
            return _repositorioSicav.ObtenerUsuario(uniqueName);
        }
        public IEnumerable<ViewModelConsolidadoAtribuciones> ObtenerConsolidadoAtribuciones(string idTipoIdentificacion, string numeroIdentificacionAsociado, int? idPerfil, int? valorcredito)
        {
            return _repositorioSicav.ObtenerConsolidadoAtribuciones(idTipoIdentificacion, numeroIdentificacionAsociado, idPerfil, valorcredito);
        }

        public void GuardarSolicitudCreditoIntentoLog(SolicitudCreditoIntentosLog model)
        {
            _repositorioSicav.GuardarSolicitudCreditoIntentoLog(model);
        }
        public List<CapacidadPagoCodeudores> ObtenerCoincidenciaCodeudorCreditos()
        {
            return _repositorioSicav.ObtenerCoincidenciaCodeudorCreditos();
        }
        public List<SolicitudCredito> ObtenerTodasSolicitudCredito(short? idEstado = null)
        {
            return _repositorioSicav.ObtenerTodasSolicitudCredito(idEstado);
        }
        public List<CapacidadPagoCodeudores> ObtenerSolicitudCreditoDetalleCodeudores(int idSolicitud)
        {
            return _repositorioSicav.ObtenerSolicitudCreditoDetalleCodeudores(idSolicitud);
        }

        public void GuardarUsuarioTemporal(string Usuario, string Contrasena)
        {
            _repositorioSicav.GuardarUsuarioTemporal(Usuario, Contrasena);
        }

        public RespuestaValidacion ValidarInicioUsuario(string usuario)
        {
            return _repositorioSicav.ValidarInicioUsuario(usuario);
        }

        public UsuarioLogin AdministrarInicioSesion(string usuario, string contrasena, string NuevaContrasena, int bandera)
        {
            return _repositorioSicav.AdministrarInicioSesion(usuario, contrasena, NuevaContrasena, bandera);
        }
        public RespuestaNegocio<CapacidadPagoTerceroCodeudor> GuardarCapacidadPagoTerceroCodeudor(string TipoIdentificacion, string NumeroIdentificacion, string Nombre, string FechaNacimiento, string FormCapacidad)
        {
            return _repositorioSicav.GuardarCapacidadPagoTerceroCodeudor( TipoIdentificacion,  NumeroIdentificacion,  Nombre,  FechaNacimiento,  FormCapacidad);
        }

        public double ObtenerCupoMaximoCredito(string TipoIdentificacion, string NumeroIdentificacion)
        {
            return _repositorioSicav.ObtenerCupoMaximoCredito(TipoIdentificacion, NumeroIdentificacion);
        }



        public RespuestaValidacion ObtenerSolicitudCreditoAdjunto(int idSolicitud, int idTipoAdjunto) => _repositorioSicav.ObtenerSolicitudCreditoAdjunto(idSolicitud, idTipoAdjunto);
        public SolicitudCreditoAdjunto GuardarSolicitudCreditoAdjunto(SolicitudCreditoAdjunto solicitudCreditoAdjunto) => _repositorioSicav.GuardarSolicitudCreditoAdjunto(solicitudCreditoAdjunto);
        public List<HipotecaSolicitud> ObtenerHipotecaSolicitudCredito(int idSolicitud) => _repositorioSicav.ObtenerHipotecaSolicitudCredito(idSolicitud);
        public List<Usuario> ObtenerUsuario()
        {
            return _repositorioSicav.ObtenerUsuario();
        }
        public List<Perfil> ObtenerPerfil()
        {
            return _repositorioSicav.ObtenerPerfil();
        }
        public Moras ObtenerMoras(string numeroDocumento)
        {
            return _repositorioSicav.ObtenerMoras(numeroDocumento);
        }
        public Usuario GuardarUsuario(Usuario usuario)
        {
            return _repositorioSicav.GuardarUsuario(usuario);
        }
        public Perfil GuardarPerfil(Perfil perfil)
        {
            return _repositorioSicav.GuardarPerfil(perfil);
        }
        public bool GuardarSolicitudCreditoHipoteca(SolicitudCreditoHipoteca solicitudCreditoHipoteca) => _repositorioSicav.GuardarSolicitudCreditoHipoteca(solicitudCreditoHipoteca);
        public string GenerarHipotecaGuardarMinuta(SolicitudMinutaFYC solicitudMinutaFYC)
        {
            return _repositorioSicav.GenerarHipotecaGuardarMinuta(solicitudMinutaFYC);
        }
        public bool ActualizarPerfilMenuRelacion(ListaMenuPerfil perfilMenuRelacion)
        {
            return _repositorioSicav.ActualizarPerfilMenuRelacion(perfilMenuRelacion);
        }

        public Factura ObtenerNumeroFactura(string servidor)
        {
            return _repositorioSicav.ObtenerNumeroFactura(servidor);
        }

        public string ObtenerCodeudores(int Identificacion)
        {
            return _repositorioSicav.ObtenerCodeudores(Identificacion);
        }

        public string GuardarJubilado(DetalleAsociado asociado)
        {
            return _repositorioSicav.GuardarJubilado(asociado);
        }

        public bool ConfirmarMesada14(string Cedula)
        {
            return _repositorioSicav.ConfirmarMesada14(Cedula);
        }

        public IEnumerable<SolicitudOperacion> SolicitudOperacion(string TipoIdentificacion, string NumeroIdentificacion, string NumeroOperacion)
        {
            return _repositorioSicav.SolicitudOperacion(TipoIdentificacion, NumeroIdentificacion, NumeroOperacion);
        }
        public void GuardarDatosPersonaTransaccionEfectivo(int Operacion, string Tipoidentificacion, string Numeroidentificacion, string Primernombre,
                                                        string Segundonombre, string Primerapellido, string Segundoapellido, string Direccion, string Telefono,
                                                        string Consulta, string Tipopersona)
        {
            _repositorioSicav.GuardarDatosPersonaTransaccionEfectivo(Operacion, Tipoidentificacion, Numeroidentificacion, Primernombre, Segundonombre, Primerapellido,
                                                                    Segundoapellido, Direccion, Telefono, Consulta, Tipopersona);
        }

        public void GuardarDatosOrigenFondos(int Operacion, int Opcion, string Consulta, string Detalle)
        {
            _repositorioSicav.GuardarDatosOrigenFondos(Operacion, Opcion, Consulta, Detalle);
        }

        public void GuardarSolicitudCreditoHistorico(SolicitudCreditoHistorico solicitudCreditoHistorico)
        {
            _repositorioSicav.GuardarSolicitudCreditoHistorico(solicitudCreditoHistorico);
        }

        public List<CartaCondicionesCreditoModel> ObtenerDatosCartaCondiciones(int idSolicitud)
        {
            return _repositorioSicav.ObtenerDatosCartaCondiciones(idSolicitud);
        }

		public IEnumerable<PorcentajesTasas> ObtenerTasas(string nomPorcentaje)
		{
			return _repositorioSicav.ObtenerTasas(nomPorcentaje);
		}

		public void CrearNuevaTasa(string nomPorcentaje, string porcentaje)
		{
			_repositorioSicav.CrearNuevaTasa(nomPorcentaje, porcentaje);
		}
		public IEnumerable<FomentoEmpresarial> ObtenerFomentoEmresarial()
		{
			return _repositorioSicav.ObtenerFomentoEmresarial();
		}
		public void MarcarCreditoFodes(string documentoTipo, string documentoNumero, int numeroUnico, string tipoDocumento, string numeroDocumento, string descripcion)
		{
			_repositorioSicav.MarcarCreditoFodes(documentoTipo, documentoNumero, numeroUnico, tipoDocumento, numeroDocumento, descripcion);
		}

		public IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMarcados()
		{
			return _repositorioSicav.ObtenerCreditosFomentoEmresarialMarcados().ToList();
		}

		public void DesmarcarCreditoFodes(List<FodesGenerico> listaDesmarcacion)
		{
			_repositorioSicav.DesmarcarCreditoFodes(listaDesmarcacion);
		}

		public IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMora()
		{
			return _repositorioSicav.ObtenerCreditosFomentoEmresarialMora().ToList();
		}

		public IEnumerable<InfoFodes> InformacionFodes()
		{
			return _repositorioSicav.InformacionFodes().ToList();
		}
	}
}
