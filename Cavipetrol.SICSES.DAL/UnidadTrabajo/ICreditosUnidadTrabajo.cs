﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public interface ICreditosUnidadTrabajo
    {
        InformacionSolicitante ObtenerInformacionEmpleado(string numeroIdentificacion, string tipoIdentificacion);
        InformacionSolicitanteCampanaEspecial ObtenerInformacionSolicitanteCampanaEspecial(string numeroIdentificacion, string tipoIdentificacion);
        InformacionSolicitante ObtenerInformacionAsociado(string numeroIdentificacion, string tipoIdentificacion);
        InformacionAsegurabilidad ObtenerInformacionAsegurabilidad(string numeroIdentificacion, string tipoIdentificacion);
        ValidacionesSolicitudCreditoUsuario ObternerValidacionesSolicitudCredito(string numeroIdentificacion, string tipoIdentificacion);
        IEnumerable<SaldosColocacion>ObtenerCreditosDisponiblesSaldo(string numeroIdentificacion, string tipoIdentificacion, string papel);        
        IEnumerable<NormaProductos> ObtenerInformacionNormas(string producto, string papel, short idTipoCredito);
        ValidacionesSolicitudCreditoProducto ObtenerRestriccionesProducto(string identificacion, string tipoIdentificacion, string papel, string nombreProducto);
        NormaProductos ObtenerInformacionDetalleNorma(string producto, string norma);
        void GuardarAsociado(GuardarInformacionSolicitante guardarInformacionSolicitante);
        Aportes ObtenerAportes(string tipoDocumento, string numeroDocumento, short IdTipoRol);
        Cartera ObtenerCartera(string idTipoIdentificacion, string numeroDocumento);


        IEnumerable<Garantia> ObtenerInformacionGarantia(string NumSolicitud, string TipoDoc, string NumeroDoc);
        IEnumerable<ViewModelFormaPagoFYC> ObtenerFormasPagoFYC(string producto, string papel, short idTipoCredito);

        VMSaldoAsegurabilidad ObtenerSaldoAsegurabilidad(string numeroIdentificacion);

        RespuestaValidacion ValidarCodeudor(string NumeroDocumento);
        IEnumerable<TipoGarantiaPorProducto> ObtenerTipoGarantiaPorProducto(string Producto);

        Familiar GuardarFamiliar(Familiar informacionFamiliar);
        IEnumerable<FormaPagoCredito> ObtenerFormaDePagoSolicitud(string idSolicitud);
        IEnumerable<ViewModelFormaPagoFYC> ObtenerFormaPagoPorNorma(string norma);
        ObjectInt guardarSolicitudCreditoFyc(SolicitudCreditoFyc solicitudCreditoFyc);

        void guardarPagareGarantia(VMConceptoGarantiaCredito vMConceptoGarantiaCredito);
        ObjectInt generarNumeroPagare(SolicitudCreditoPagare solicitudCreditoPagare);
        void guardarSolicitudCreditoPagare(SolicitudCreditoPagare solicitudCreditoPagare);

        IEnumerable<SolicitudesCredito> ObtenerSolicitudes(int numeroSolicitud, string tipoDocumento, int numeroDocumento, int tipoCredito);

        IEnumerable<AsegurabilidadOpcion> ObtenerOpcionesAsegurabilidad(bool IdAsegurable, string idCredito, string idPapel);

        int ValidarITP(string cedula, int bandera);
        int ValidarEmbargo(string cedula, int bandera);
        int ValidarExisExtraprima(string cedula, string tipoIdentificacion, int bandera);
        IEnumerable<ExtraPrima> TraeExtraprima(string cedula, string tipoIdentificacion, int bandera);
        void actualizarEstado(string NumeroSolicitud, string Estado);
        IEnumerable<ArbolAmortizacion> ObtenerArbolAmortizacion(string idtipocredito, string idpapelcavipetrol);
        void GuardarSolicitudInterproductos(List<InformacionProductosCreditos> datos);
    }
}
