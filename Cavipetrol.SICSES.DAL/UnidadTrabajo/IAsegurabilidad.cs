﻿using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public interface IAsegurabilidad
    {
        bool InsertarDatosAsegurabilidad(List<AsegurabilidadModel> datosasegurailidad);
        IEnumerable<VMPolizasVigentes> ObtenerPolizasVigentes(string tipo, string numero);
    }

}
