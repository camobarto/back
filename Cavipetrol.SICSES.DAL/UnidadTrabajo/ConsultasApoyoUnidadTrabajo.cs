﻿using System.Collections.Generic;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System.Linq;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public class ConsultasApoyoUnidadTrabajo : IConsultasApoyoUnidadTrabajo
    {
        private readonly IRepositorioUbicacion _repositorioUbicacion;
        private readonly IRepositorioParametros _repositorioParametros;
        private readonly ParametrosCoreContexto _contexto;
        public ConsultasApoyoUnidadTrabajo()
        {
            _contexto = new ParametrosCoreContexto();
            _repositorioUbicacion = new RepositorioUbicacion(_contexto);
            _repositorioParametros = new RepositorioParametros(_contexto);
        }

        public IEnumerable<AsociacionEntidadSolidaria> ObtenerAsociacionesEntidadesSolidarias()
        {
            return _repositorioParametros.ObtenerAsociacionesEntidadesSolidarias();
        }

        public IEnumerable<Departamento> ObtenerDepartamentos()
        {
            return _repositorioUbicacion.ObtenerDepartamentos();
        }

        public IEnumerable<FormatoVigenteSICSES> ObtenerFormatosVigentesSICSES()
        {
            return _repositorioParametros.ObtenerFormatosVigentesSICSES();
        }

        public IEnumerable<Municipio> ObtenerMunicipiosPorIdDepartamento(string idDepartamento)
        {
            return _repositorioUbicacion.ObtenerMunicipiosPorIdDepartamento(idDepartamento);
        }

        public IEnumerable<TipoNivelEscolaridad> ObtenerNivelesEscolaridad()
        {
            return _repositorioParametros.ObtenerNivelesEscolaridad();
        }

        public IEnumerable<TipoContrato> ObtenerTiposContratos()
        {
            return _repositorioParametros.ObtenerTiposContratos();
        }

        public IEnumerable<TipoEstadoCivil> ObtenerTiposEstadosCiviles()
        {
            return _repositorioParametros.ObtenerTiposEstadosCiviles();
        }

        public IEnumerable<TipoEstrato> ObtenerTiposEstratos()
        {
            return _repositorioParametros.ObtenerTiposEstratos();
        }

        public IEnumerable<TipoIdentificacion> ObtenerTiposIdentificaciones()
        {
            return _repositorioParametros.ObtenerTiposIdentificaciones();
        }

        public IEnumerable<TipoJornadaLaboral> ObtenerTiposJornadasLaborales()
        {
            return _repositorioParametros.ObtenerTiposJornadasLaborales();
        }

        public IEnumerable<TipoOcupacion> ObtenerTiposOcupacion()
        {
            return _repositorioParametros.ObtenerTiposOcupacion();
        }

        public IEnumerable<TipoOrganoDirectivoControl> ObtenerTiposOrganosDirectivosControl()
        {
            return _repositorioParametros.ObtenerTiposOrganosDirectivosControl();
        }

        public IEnumerable<TipoRol> ObtenerTiposRoles()
        {
            return _repositorioParametros.ObtenerTiposRoles();
        }

        public IEnumerable<TipoSectorEconomico> ObtenerTiposSectorEconomico()
        {
            return _repositorioParametros.ObtenerTiposSectorEconomico();
        }

        public IEnumerable<TipoEntidad> ObtenerEntidades()
        {
            return _repositorioParametros.ObtenerEntidades();
        }
        public IEnumerable<ClasificacionCIIU> ObtenerClasificacionCUU()
        {
            return _repositorioParametros.ObtenerClasificacionCUU();
        }

        public IEnumerable<Meses> ObtenerMeses()
        {
            return _repositorioParametros.ObtenerMeses();
        }

        public IEnumerable<Anos> ObtenerAnos()
        {
            return _repositorioParametros.ObtenerAnos();
        }

        public IEnumerable<ClasificacionExcedentes> ObtenerInfoExcedentes()
        {
            return _repositorioParametros.ObtenerInfoExcedentes();
        }

        public IEnumerable<FormCapacidadPago> ObtenerFormCapacidadPago(int? idLineaCredito = null)
        {
            return _repositorioParametros.ObtenerFormCapacidadPago(idLineaCredito);
        }

        public IEnumerable<FormCapacidadPagoTipoCreditoTipoRolRelacion> ObtenerFormCapacidadPagoTipoCreditoTipoRolRelacion()
        {
            return _repositorioParametros.ObtenerFormCapacidadPagoTipoCreditoTipoRolRelacion();
        }

        public IEnumerable<FormCapacidadPagoGrupos> ObtenerFormCapacidadPagoGrupos(int? idLineaCredito = null) {
            return _repositorioParametros.ObtenerFormCapacidadPagoGrupos();
        }

        public IEnumerable<EvaluacionRiesgoLiquidez> ObtenerInfoEvaluacionRiesgoLiquidez()
        {
            return _repositorioParametros.ObtenerInfoEvaluacionRiesgoLiquidez();
        }
        public IEnumerable<TiposTitulos> ObtenerTiposTitulos()
        {
            return _repositorioParametros.ObtenerTiposTitulos();
        }
        public IEnumerable<TipoLineaCredito> ObtenerTipoLineaCredito() {
            return _repositorioParametros.ObtenerTipoLineaCredito();
        }
        public IEnumerable<TipoCredito> ObtenerTipoCredito(int? idLineaCredito = null) {
            return _repositorioParametros.ObtenerTipoCredito();
        }


        public IEnumerable<TipoObligacion> ObtenerTipoOligacion()
        {
            return _repositorioParametros.ObtenerTipoOligacion();
        }

        public IEnumerable<TipoCuota> ObtenerTipoCuota()
        {
            return _repositorioParametros.ObtenerTipoCuota();
        }

        public IEnumerable<ClaseGarantia> ObtenerClaseGarantia()
        {
            return _repositorioParametros.ObtenerClaseGarantia();
        }
        public IEnumerable<DestinoCredito> ObtenerDestinoCredito()
        {
            return _repositorioParametros.ObtenerDestinoCredito();
        }
        public IEnumerable<ClaseVivienda> ObtenerClaseVivienda()
        {
            return _repositorioParametros.ObtenerClaseVivienda();
        }
        public IEnumerable<CategoriaReestructurado> ObtenerCategoriaRestructurado()
        {
            return _repositorioParametros.ObtenerCategoriaRestructurado();
        }

        public IEnumerable<TipoEntidadBeneficiaria> ObtenerTipoEntidadBeneficiaria()
        {
            return _repositorioParametros.ObtenerTipoEntidadBeneficiaria();
        }

        public IEnumerable<TipoModalidad> ObtenerTipoModalidad()
        {
            return _repositorioParametros.ObtenerTipoModalidad();
        }
        public IEnumerable<TipoDestino> ObtenerTipoDestino()
        {
            return _repositorioParametros.ObtenerTipoDestino();
        }
        public IEnumerable<NivelEducacion> ObtenerNivelEducacion()
        {
            return _repositorioParametros.ObtenerNivelEducacion();
        }

        public IEnumerable<TipoBeneficiarios> ObtenerTiposBeneficiarios()
        {
            return _repositorioParametros.ObtenerTiposBeneficiarios();
        }

        public IEnumerable<AlternativasDecreto2880> ObtenerAlternativasDecreto2880()
        {
            return _repositorioParametros.ObtenerAlternativasDecreto2880();
        }
        public IEnumerable<ClaseNaturaleza> ObtenerClaseNaturaleza()
        {
            return _repositorioParametros.ObtenerClaseNaturaleza();
        }
        public IEnumerable<ClaseEstadoProcesoActual> ObtenerClaseEstadoProcesoActual()
        {
            return _repositorioParametros.ObtenerClaseEstadoProcesoActual();
        }
        public IEnumerable<ClaseConcepto> ObtenerClaseConcepto()
        {
            return _repositorioParametros.ObtenerClaseConcepto();
        }
        public IEnumerable<TipoUsuarioTieneAporte> ObtenerTipoUsuarioTieneAporte()
        {
            return _repositorioParametros.ObtenerTipoUsuarioTieneAporte();
        }
        public IEnumerable<ClaseDeActivo> ObtenerClaseDeActivo()
        {
            return _repositorioParametros.ObtenerClaseDeActivo();
        }

        public IEnumerable<InformacionRelacionadaGrupoInteres> ObtenerInfoReporteGrupoDeInteres()
        {
            return _repositorioParametros.ObtenerInfoReporteGrupoDeInteres();
        }
        public IEnumerable<TipoModalidadContratacion> ObtenerTipoModalidadContratacion()
        {
            return _repositorioParametros.ObtenerTipoModalidadContratacion();
        }
        public IEnumerable<TipoProductoServicioContratado> ObtenerTipoProductoServicioContratado()
        {
            return _repositorioParametros.ObtenerTipoProductoServicioContratado();
        }

        public IEnumerable<TipoTitulo> ObtenerTipoTitulo()
        {
            return _repositorioParametros.ObtenerTipoTitulo();
        }

        public IEnumerable<TipoTasa> ObtenerTipoTasa()
        {
            return _repositorioParametros.ObtenerTipoTasa();
        }

        public IEnumerable<TipoCreditoTipoRolRelacion> ObtenerTiposCreditoTiposRolRelacion(int idTipoRol = 0, int? idTipoCreditoTipoRolRelacion = null)
        {
            return _repositorioParametros.ObtenerTiposCreditoTiposRolRelacion(idTipoRol, idTipoCreditoTipoRolRelacion);
        }
        public IEnumerable<Etiquetas> ObtenerEtiquetas(int idTipoEtiqueta)
        {
            return _repositorioParametros.ObtenerEtiquetas(idTipoEtiqueta);
        }
        public IEnumerable<Parametros> ObtenerParametros(string nombre = null)
        {
            return _repositorioParametros.ObtenerParametros(nombre);
        }

        public IEnumerable<TiposDocumentoRequisito> ObtenerTiposDocumentoRequisito()
        {
            return _repositorioParametros.ObtenerTiposDocumentoRequisito();
        }

        public Plantilla ObtenerPlantilla(short idPlantilla)
        {
            return _repositorioParametros.ObtenerPlantilla(idPlantilla);
        }

        public IEnumerable<Cupo> ObtenerCupos(short idTipoCredito = 0)
        {
            return _repositorioParametros.ObtenerCupos(idTipoCredito);
        }
        public IEnumerable<TiposCupo> ObtenerTiposCupo()
        {
            return _repositorioParametros.ObtenerTiposCupo();
        }
        public IEnumerable<TiposCreditoTipoSeguroRelacion> ObtenerTiposCreditoTiposSeguroRelacion(short idTipoCredito)
        {
            return _repositorioParametros.ObtenerTiposCreditoTiposSeguroRelacion(idTipoCredito);
        }

        public IEnumerable<TiposCreditoTiposGarantiaRelacion> ObtenerTiposCreditoTiposGarantiaRelacion(short idTipoCredito)
        {
            return _repositorioParametros.ObtenerTiposCreditoTiposGarantiaRelacion(idTipoCredito);
        }

        public IEnumerable<Bimestre> ObtenerBimestre()
        {
            return _repositorioParametros.ObtenerBimestre();
        }
        public IEnumerable<TiposGarantiasPagares> ObtenerTiposGarantiasPagares()
        {
            return _repositorioParametros.ObtenerTiposGarantiasPagares();
        }

        public IEnumerable<UsosCredito> ObtenerUsosCredito(int idTipoLinea)
        {
            return _repositorioParametros.ObtenerUsosCredito(idTipoLinea);
        }

        public IEnumerable<CuposAsegurabilidad> ObtenerCuposAsegurabilidad()
        {
            return _repositorioParametros.ObtenerCuposAsegurabilidad();
        }

        public IEnumerable<TipoGarantiaCredito> ObtenerTiposGarantiasCredito(int? idTipoGarantiaCredito = null)
        {
            return _repositorioParametros.ObtenerTiposGarantiasCredito(idTipoGarantiaCredito);
        }

        
        public PapelCavipetrol ObtenerPapelCavipetrol(short id)
        {
            return _repositorioParametros.ObtenerPapelCavipetrol(id);
        }
        public IEnumerable<FormaPago> ObtenerFormaPago(int id = 0)
        {
            return _repositorioParametros.ObtenerFormaPago(id);
        }

        public IEnumerable<FormaPagoRelacion> ObtenerFormaPagoRelacion(int idFormaPagoRelacion = 0, int idTipoCreditoTipoRolRelacion = 0,  bool mesada14 = false)
        {
            var respuesta = _repositorioParametros.ObtenerFormaPagoRelacion(idFormaPagoRelacion, idTipoCreditoTipoRolRelacion, mesada14);
            return respuesta;
        }
        

        public IEnumerable<SeguroTasas> ObtenerSegurosTasas(int? idSeguroTasa = null, int? idTipoSeguro = null, byte? edad = null)
        {
            return _repositorioParametros.ObtenerSegurosTasas(idSeguroTasa, idTipoSeguro, edad);
        }

        public IEnumerable<TiposPagareTiposConceptoRelacion> ObtenerTiposGarantiaTiposConceptoRelacion(int idPagare)
        {
            return _repositorioParametros.ObtenerTiposGarantiaTiposConceptoRelacion(idPagare);
        }

        public IEnumerable<TiposConceptosGarantia> ObtenerTiposConceptoGarantia()
        {
            return _repositorioParametros.ObtenerTiposConceptoGarantia();
        }

        public int ObtenerCodeudorPorGarantia(string idTipoGarantia, string idTipoCodeudor, string valor)
        {
            return _repositorioParametros.ObtenerCodeudorPorGarantia(idTipoGarantia, idTipoCodeudor, valor);
        }

        public string ValidarUsuarioCavipetrol(string NumeroIdentificacion)
        {
            return _repositorioParametros.ValidarUsuarioCavipetrol(NumeroIdentificacion);
        }

        public IEnumerable<TiposConceptosAdjunto> ObtenerConceptosAdjuntos(string idConcepto)
        {
            return _repositorioParametros.ObtenerConceptosAdjuntos(idConcepto);
        }
        public Oficina ObtenerOficinas(int? idOficina)
        {
            return _repositorioParametros.ObtenerOficinas(idOficina);
        }
        public Municipio ObtenerMunicipio(string Id, string IdDepartamento)
        {
            return _repositorioParametros.ObtenerMunicipio(Id, IdDepartamento);
        }
        public List<Oficina> ObtenerOficinas()
        {
            return _repositorioParametros.ObtenerOficinas();
        }


        public IEnumerable<TipoConceptoAdjuntoRelacion> ObtenerTiposConceptosTipoAdjuntoRelacion(int idTipoAdjunto) => _repositorioParametros.ObtenerTiposConceptosTipoAdjuntoRelacion(idTipoAdjunto);
        public IEnumerable<TipoConceptoAdjunto> ObtenerTiposConceptos() => _repositorioParametros.ObtenerTiposConceptos();

        public IEnumerable<TiposContrato> ObtenerTiposContrato() => _repositorioParametros.ObtenerTiposContrato();
        public List<CiudadesServidor> ObtenerCiudadesServidor()
        {
            return _contexto.ObtenerCiudadesServidor().ToList();
        }

        public IEnumerable<Usuarios> ObtenerUsuariosPorPerfil(string perfil)
        {
            return _repositorioParametros.ObtenerUsuariosPorPerfil(perfil);
        }
        public string AsignarUsuarioSolicitud(int idSolicitud, int idUsuario)
        {
            return _repositorioParametros.AsignarUsuarioSolicitud(idSolicitud, idUsuario);
        }
        public IEnumerable<SolicitudDescripcionFondos> SolicitudDescripcionFondos()
        {
            return _repositorioParametros.SolicitudDescripcionFondos();
        }

        public IEnumerable<ModelCartaSolicitudCredito> ObtenerCartaSolicitud(string IdSolicitud)
        {
            return _repositorioParametros.ObtenerCartaSolicitud(IdSolicitud);
        }

        public IEnumerable<Interproductos> ObtenerModelInterproductos(string IdSolicitud)
        {
            return _repositorioParametros.ObtenerModelInterproductos(IdSolicitud);
        }

		public IEnumerable<CausalesFodes> ObtenerCausalesFodes()
		{
			return _repositorioParametros.ObtenerCausalesFodes();
		}
	}
}
