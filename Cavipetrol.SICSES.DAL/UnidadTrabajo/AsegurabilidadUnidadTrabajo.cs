﻿using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public class AsegurabilidadUnidadTrabajo : IAsegurabilidad
    {

        private readonly AsegurabilidadContexto _context;
        private readonly IRepositorioAsegurabilidad _repositorioAsegurabilidad;

        public AsegurabilidadUnidadTrabajo()
        {
            _context = new AsegurabilidadContexto();
            _repositorioAsegurabilidad = new RepositorioAsegurabilidad(_context);
        }

        public bool InsertarDatosAsegurabilidad(List<AsegurabilidadModel> datosasegurabilidad)
        {
            _repositorioAsegurabilidad.InsertarDatosAsegurabilidad(datosasegurabilidad);

            return GuardarCambios();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipo">Tipo de documento</param>
        /// <param name="numero">Numero de documento</param>
        /// <returns></returns>
        public IEnumerable<VMPolizasVigentes> ObtenerPolizasVigentes(string tipo, string numero)
        {
            return _repositorioAsegurabilidad.ObtenerPolizasVigentes(tipo, numero);
        }

        private bool GuardarCambios()
        {
            int operations = _context.SaveChanges();
            return operations > 0 ? true : false;
        }
    }
}
