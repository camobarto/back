﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Sap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public interface ISapUnidadTrabajo
    {

        IEnumerable<AsiSoporte> ObtenerAsiSoporte();

        IEnumerable<Asientos> ConsultarAsientos(string AsiSoporte, string AsiConsecutivo, string Asioperacion, string AsiOpeConsecutivo, string Fecha, string Ciudad, string MensajeError);

        IEnumerable<string> ActualizarAsiento();
        IEnumerable<Proveedores> ConsultaProveedores(string CardCode, string CardName);

        IEnumerable<string> ActualizarEstadoProveedores();
        IEnumerable<MensajeErrorAsientos> ConsultarMensajeError();




    }
}
