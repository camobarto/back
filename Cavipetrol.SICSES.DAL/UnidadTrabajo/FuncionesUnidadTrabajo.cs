﻿using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public class FuncionesUnidadTrabajo : IFunciones
    {

        private readonly FuncionesContexto _contexto;
        private readonly IRepositorioFunciones _repositorioFunciones;

        public FuncionesUnidadTrabajo()
        {
            _contexto = new FuncionesContexto();
            _repositorioFunciones = new RepositorioFunciones(_contexto);
        }

        public bool EnviarSMS(DatosSMS datossms)
        {
            _repositorioFunciones.EnviarSMS(datossms);

            return GuardarCambios();
        }

        private bool GuardarCambios()
        {
            int operations = _contexto.SaveChanges();
            return operations > 0 ? true : false;
        }

    }
}
