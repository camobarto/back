﻿using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public class InformesUnidadTrabajo : IInformesUnidadTrabajo
    {
        private readonly IRepositorioInformes _repositorioInformes;
        private readonly InformesContexto _contexto;

        IRepositorioInformes _repositorioSolicitudCreditoBD;
        InformesContexto _contextoBD;

        public InformesUnidadTrabajo()
        {
            _contexto = new InformesContexto();
            _repositorioInformes = new RepositorioInformes(_contexto);
        }
        

        public IEnumerable<RetencionProveedores> ObtenerInformacionGarantia(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "")
        {
            return _repositorioInformes.ObtenerInformacionGarantia(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta);
        }
        public IEnumerable<RetencionProveedores_ICA> ObtenerInformacionGarantia2(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "")
        {
            return _repositorioInformes.ObtenerInformacionGarantia2(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta, Bimestre);
        }
        public IEnumerable<RetencionProveedores_IVA> ObtenerInformacionGarantia3(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "")
        {
            return _repositorioInformes.ObtenerInformacionGarantia3(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta, Bimestre);
        }

        public bool InsertarDatosGarantias(DataTable datosasegurabilidad)
        {
            _repositorioInformes.InsertarDatosGarantias(datosasegurabilidad);

            return GuardarCambios();
        }
        private bool GuardarCambios()
        {
            int operations = _contexto.SaveChanges();
            return operations > 0 ? true : false;
        }
        public bool ActualizaInfoProveedores(int bandera)
        {
            _repositorioInformes.ActualizaInfoProveedores(bandera);

            return GuardarCambios();
        }
        public IEnumerable<ProveedoresFaltantes> GeneraProveedoresFaltantes(int bandera)
        {
            return _repositorioInformes.GeneraProveedoresFaltantes(bandera);
        }
        public IEnumerable<CuentaContable> ObtenerCuentaContable(int bandera)
        {
            return _repositorioInformes.ObtenerCuentaContable(bandera);
        }
        public IEnumerable<AuxTercero> ConsultaAuxTercero(string mes, string anio, string cuenta)
        {
            //_contextoBD = new InformesContexto(""); //FYC_CENTRAL
            //_repositorioSolicitudCreditoBD = new RepositorioInformes(_contextoBD);
            return _repositorioInformes.ConsultaAuxTercero(mes, anio, cuenta);
        }
        public IEnumerable<AuditaCuenta> ConsultaAuditaCuenta(string Fc_Ini, string Fc_Fin, string Cuenta)
        {
            //IRepositorioInformes _repositorioSolicitudCreditoBD;
            //InformesContexto _contextoBD;
            //_contextoBD = new InformesContexto("CAVIPETROL"); //CAVIPETROL
            //_repositorioSolicitudCreditoBD = new RepositorioInformes(_contextoBD);
            return _repositorioInformes.ConsultaAuditaCuenta(Fc_Ini, Fc_Fin, Cuenta);
        }
        public IEnumerable<GMF_BUno> ConsutaGMF_BUno(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            //IRepositorioInformes _repositorioSolicitudCreditoBD;
            //InformesContexto _contextoBD;
            //_contextoBD = new InformesContexto("Asegurabilidad"); //FYC_CENTRAL
            //_repositorioSolicitudCreditoBD = new RepositorioInformes(_contextoBD);
            return _repositorioInformes.ConsutaGMF_BUno(Fc_Ini, Fc_Fin, Bandera);
        }
        public IEnumerable<GMF_BDos> ConsutaGMF_BDos(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            //IRepositorioInformes _repositorioSolicitudCreditoBD;
            //InformesContexto _contextoBD;
            //_contextoBD = new InformesContexto("Asegurabilidad"); //FYC_CENTRAL
            //_repositorioSolicitudCreditoBD = new RepositorioInformes(_contextoBD);
            return _repositorioInformes.ConsutaGMF_BDos(Fc_Ini, Fc_Fin, Bandera);
        }
        public IEnumerable<GMF_BTres> ConsutaGMF_BTres(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            //IRepositorioInformes _repositorioSolicitudCreditoBD;
            //InformesContexto _contextoBD;
            //_contextoBD = new InformesContexto("Asegurabilidad"); //FYC_CENTRAL
            //_repositorioSolicitudCreditoBD = new RepositorioInformes(_contextoBD);
            return _repositorioInformes.ConsutaGMF_BTres(Fc_Ini, Fc_Fin, Bandera);
        }

        public IEnumerable<SaldoInicialCarteraEcopetrol> ObtenerSaldoInicialInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _repositorioInformes.ObtenerSaldoInicialInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }
        public IEnumerable<EncabezadoCarteraEcopetrol> ObtenerEncabezadoInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _repositorioInformes.ObtenerEncabezadoInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }
        public IEnumerable<PlanPagosCarteraEcopetrol> ObtenerPlanPagosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _repositorioInformes.ObtenerPlanPagosInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }
        public IEnumerable<MovimientosCarteraEcopetrol> ObtenerMovimientosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _repositorioInformes.ObtenerMovimientosInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }
        public IEnumerable<TipoDocumentoPorCedula> ObtenerTipoDocumentoPorCedula(string TipoIdentificacion, int Cedula)
        {
            return _repositorioInformes.ObtenerTipoDocumentoPorCedula(TipoIdentificacion, Cedula);
        }
        public IEnumerable<FacturaElectronica> ObtenerFacturaElectronica(string Factura, string Servidor)
        {
            return _repositorioInformes.ObtenerFacturaElectronica(Factura, Servidor);
        }

        public void GuardarNota(Nota nota)
        {
            _repositorioInformes.GuardarNota(nota);
        }

        public IEnumerable<Certificaciones_Ingresos_Retenciones> Certificaiones(string NumeroIdentificacion, DateTime FechaCorte)
        {
            return _repositorioInformes.Certificaiones(NumeroIdentificacion, FechaCorte);
        }
    }
}
