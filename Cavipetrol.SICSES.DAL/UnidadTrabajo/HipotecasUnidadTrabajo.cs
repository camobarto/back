﻿using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Model.Hipotecas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public class HipotecasUnidadTrabajo : IHipotecasUnidadTrabajo
    {
        private readonly IRepositorioHipotecas _repositorioSolicitudCredito;
        private readonly HipotecasContexto _contexto;

        IRepositorioInformes _repositorioSolicitudCreditoBD;
        HipotecasContexto _contextoBD;

        public HipotecasUnidadTrabajo()
        {
            _contexto = new HipotecasContexto();
            _repositorioSolicitudCredito = new RepositorioHipotecas(_contexto);
        }

        public IEnumerable<Hipoteca> ObtenerHipotecas(string NumeroHipoteca, string TipoIdentificacion, string NumeroIdentificacion, string NumeroSolicitud)
        {
            return _repositorioSolicitudCredito.ObtenerHipotecas(NumeroHipoteca, TipoIdentificacion, NumeroIdentificacion, NumeroSolicitud);
        }
        private bool GuardarCambios()
        {

            int operations = _contexto.SaveChanges();
            return operations > 0 ? true : false;
        }
        public bool ActualizaHipotecas(int IdHipoteca, string AudUsuarioModificacion, string FechaEscrituracion, string NumeroEscritura, string ValorAvaluo, string VigenciaInicial, string VigenciaFinal, string NumeroNotaria, string Oficina)
        {
            try
            {
                _repositorioSolicitudCredito.ActualizaHipotecas(IdHipoteca, AudUsuarioModificacion, FechaEscrituracion, NumeroEscritura, ValorAvaluo, VigenciaInicial, VigenciaFinal, NumeroNotaria, Oficina);

                // return GuardarCambios();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<Hipoteca> ObtenerHipotecasXCedula(string TipoIdentificacion, string NumeroIdentificacion)
        {
            return _repositorioSolicitudCredito.ObtenerHipotecasXCedula(TipoIdentificacion, NumeroIdentificacion);
        }
        public bool ActualizarHipoteca(int NumeroHipoteca, double ValorCredito)
        {
            return _repositorioSolicitudCredito.ActualizarHipoteca(NumeroHipoteca, ValorCredito);
        }
    }
}
