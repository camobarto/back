﻿using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Model.Administracion;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Administracion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public  class AdministracionUnidadTrabajo : IAdministracion
    {
        private readonly AdministracionContexto _context;
        private readonly IRepositorioAdministracion _repositorioAdministracion;

        public AdministracionUnidadTrabajo()
        {
            _context = new AdministracionContexto();
            _repositorioAdministracion = new RepositorioAdministracion(_context);
        }

        public IEnumerable<LineaCredito> ObtenerLineaCredito()
        {
            return _repositorioAdministracion.ObtenerLineaCredito();
        }
        public string GuardarLineaCredito(int id,string nombre, string Descripcion, int bandera)
        {
           return  _repositorioAdministracion.GuardarLineaCredito(id,nombre, Descripcion, bandera);
        }

        public string EliminarLineaCredito(int id)
        {
            return _repositorioAdministracion.EliminarLineaCredito(id);
        }
        public IEnumerable<ViewTiposCreditos> ObtenerTiposCredito()
        {
            return _repositorioAdministracion.ObtenerTiposCredito();
        }
        public string AdministrarTiposCredito(int id, string nombre, int idLinea, int bandera)
        {
            return _repositorioAdministracion.AdministrarTiposCredito(id, nombre, idLinea, bandera);
        }
        public string EliminarTiposCredito(int id)
        {
            return _repositorioAdministracion.EliminarTiposCredito(id);
        }
        public IEnumerable<ViewPapelCavipetrol> ObtenerPapelCavipetrol()
        {
            return _repositorioAdministracion.ObtenerPapelCavipetrol();
        }
        public string AdministrarPapelCavipetrol(int id, string nombre, string Descripcion, int idcontrato, int bandera)
        {
            return _repositorioAdministracion.AdministrarPapelCavipetrol(id, nombre, Descripcion, idcontrato, bandera);
        }
        public string EliminarPapelCavipetrol(int id)
        {
            return _repositorioAdministracion.EliminarPapelCavipetrol(id);
        }
        public IEnumerable<ViewTipoCreditoTipoRelacion> ObtenerTipoCreditoTipoRolRelacion()
        {
            return _repositorioAdministracion.ObtenerTipoCreditoTipoRolRelacion();
        }
        public string AdministrarTipoCreditoTipoRol(TipoCreditoTipoRol datos)
        {
            return _repositorioAdministracion.AdministrarTipoCreditoTipoRol(datos);
        }
    }
}
