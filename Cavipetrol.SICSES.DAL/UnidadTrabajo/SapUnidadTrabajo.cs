﻿using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Sap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public  class SapUnidadTrabajo : ISapUnidadTrabajo
    {
        public readonly IRepositorioSap _repositoriosap;
        private readonly SapContexto _contexto;

        public SapUnidadTrabajo()
        {
            _contexto = new SapContexto();

            _repositoriosap = new RepositorioSap(_contexto);
        }

        public IEnumerable<AsiSoporte> ObtenerAsiSoporte()
        {
            return _repositoriosap.ObtenerAsiSoporte();
        }

        public IEnumerable<Asientos> ConsultarAsientos(string AsiSoporte, string AsiConsecutivo, string Asioperacion, string AsiOpeConsecutivo, string Fecha, string Ciudad, string MensajeError)
        {
            return _repositoriosap.ConsultarAsientos(AsiSoporte, AsiConsecutivo, Asioperacion, AsiOpeConsecutivo, Fecha, Ciudad, MensajeError);
        }

        public IEnumerable<string> ActualizarAsiento()
        {
             return _repositoriosap.ActualizarAsiento();
        }
        public IEnumerable<Proveedores> ConsultaProveedores(string CardCode, string CardName)
        {
            return _repositoriosap.ConsultaProveedores(CardCode, CardName);
        }

        public IEnumerable<string> ActualizarEstadoProveedores()
        {
            return _repositoriosap.ActualizarEstadoProveedores();
        }
        public IEnumerable<MensajeErrorAsientos> ConsultarMensajeError()
        {
            return _repositoriosap.ConsultarMensajeError();
        }

    }
}
