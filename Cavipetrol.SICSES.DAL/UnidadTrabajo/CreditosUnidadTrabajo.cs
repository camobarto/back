﻿using System.Collections.Generic;
using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public class CreditosUnidadTrabajo : ICreditosUnidadTrabajo
    {
        private readonly IRepositorioSolicitudCredito _repositorioSolicitudCredito;
        private readonly CreditosContexto _contexto;

        public CreditosUnidadTrabajo()
        {
            _contexto = new CreditosContexto();
            _repositorioSolicitudCredito = new RepositorioSolicitudCredito(_contexto);
        }

        public InformacionAsegurabilidad ObtenerInformacionAsegurabilidad(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _repositorioSolicitudCredito.ObtenerInformacionAsegurabilidad(numeroIdentificacion, tipoIdentificacion);
        }

        public InformacionSolicitante ObtenerInformacionAsociado(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _repositorioSolicitudCredito.ObtenerInformacionAsociado(numeroIdentificacion, tipoIdentificacion);
        }

        public NormaProductos ObtenerInformacionDetalleNorma(string producto, string norma)
        {
            return _repositorioSolicitudCredito.ObtenerInformacionDetalleNorma(producto, norma);
        }

        public InformacionSolicitante ObtenerInformacionEmpleado(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _repositorioSolicitudCredito.ObtenerInformacionEmpleado(numeroIdentificacion, tipoIdentificacion);
        }

        public InformacionSolicitanteCampanaEspecial ObtenerInformacionSolicitanteCampanaEspecial(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _repositorioSolicitudCredito.ObtenerInformacionSolicitanteCampanaEspecial(numeroIdentificacion, tipoIdentificacion);
        }

        public IEnumerable<NormaProductos>ObtenerInformacionNormas(string producto, string papel, short idTipoCredito)
        {
            return _repositorioSolicitudCredito.ObtenerInformacionNormas(producto, papel, idTipoCredito);
        }
        public IEnumerable<SaldosColocacion>ObtenerCreditosDisponiblesSaldo(string numeroIdentificacion, string tipoIdentificacion, string papel)
        {
            return _repositorioSolicitudCredito.ObtenerCreditosDisponiblesSaldo(numeroIdentificacion, tipoIdentificacion, papel);
        }        
        public ValidacionesSolicitudCreditoProducto ObtenerRestriccionesProducto(string identificacion, string tipoIdentificacion, string papel, string nombreProducto)
        {
            return _repositorioSolicitudCredito.ObtenerRestriccionesProducto(identificacion, tipoIdentificacion, papel, nombreProducto);
        }

        public ValidacionesSolicitudCreditoUsuario ObternerValidacionesSolicitudCredito(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _repositorioSolicitudCredito.ObtenerValidacionesSolicitudCredito(numeroIdentificacion, tipoIdentificacion);
        }
        public void GuardarAsociado(GuardarInformacionSolicitante guardarInformacionSolicitante)
        {
            _repositorioSolicitudCredito.GuardarAsociado(guardarInformacionSolicitante);
        }

        public Aportes ObtenerAportes(string tipoDocumento, string numeroDocumento, short IdTipoRol)
        {
            return _repositorioSolicitudCredito.ObtenerAportes(tipoDocumento, numeroDocumento, IdTipoRol);
        }

        public Cartera ObtenerCartera(string idTipoIdentificacion, string numeroDocumento)
        {
            return _repositorioSolicitudCredito.ObtenerCartera(idTipoIdentificacion, numeroDocumento);
        }


        public IEnumerable<Garantia> ObtenerInformacionGarantia(string NumSolicitud, string TipoDoc, string NumeroDoc)
        {
            return _repositorioSolicitudCredito.ObtenerInformacionGarantia(NumSolicitud, TipoDoc, NumeroDoc);
        }
        public IEnumerable<ViewModelFormaPagoFYC> ObtenerFormasPagoFYC(string producto, string papel, short idTipoCredito)
        {
            return _repositorioSolicitudCredito.ObtenerFormasPagoFYC(producto, papel, idTipoCredito);
        }
        public RespuestaValidacion ValidarCodeudor(string NumeroDocumento)

        {
            return _repositorioSolicitudCredito.ValidarCodeudor(NumeroDocumento);
        }

        public VMSaldoAsegurabilidad ObtenerSaldoAsegurabilidad(string numeroIdentificacion)
        {
            return _repositorioSolicitudCredito.ObtenerSaldoAsegurabilidad(numeroIdentificacion);
        }

        public IEnumerable<TipoGarantiaPorProducto> ObtenerTipoGarantiaPorProducto(string Producto)
        {
            return _repositorioSolicitudCredito.ObtenerTipoGarantiaPorProducto(Producto);
        }
        public Familiar GuardarFamiliar(Familiar informacionFamiliar)
        {
            return _repositorioSolicitudCredito.GuardarFamiliar(informacionFamiliar);
        }
        public IEnumerable<FormaPagoCredito> ObtenerFormaDePagoSolicitud(string idSolicitud)
        {
            return _repositorioSolicitudCredito.ObtenerFormaDePagoSolicitud(idSolicitud);
        }
        public IEnumerable<ViewModelFormaPagoFYC> ObtenerFormaPagoPorNorma(string norma)
        {
            return _repositorioSolicitudCredito.ObtenerFormaPagoPorNorma(norma);
        }
        public ObjectInt guardarSolicitudCreditoFyc(SolicitudCreditoFyc solicitudCreditoFyc)
        {
            return _repositorioSolicitudCredito.guardarSolicitudCreditoFyc(solicitudCreditoFyc);
        }

        public void guardarPagareGarantia(VMConceptoGarantiaCredito vMConceptoGarantiaCredito)
        {
            _repositorioSolicitudCredito.guardarPagareGarantia(vMConceptoGarantiaCredito);
        }
        public ObjectInt generarNumeroPagare(SolicitudCreditoPagare solicitudCreditoPagare)
        {
            return _repositorioSolicitudCredito.generarNumeroPagare(solicitudCreditoPagare);
        }
        public void guardarSolicitudCreditoPagare(SolicitudCreditoPagare solicitudCreditoPagare)
        {
            _repositorioSolicitudCredito.guardarSolicitudCreditoPagare(solicitudCreditoPagare);
        }

        public IEnumerable<SolicitudesCredito> ObtenerSolicitudes(int numeroSolicitud, string tipoDocumento, int numeroDocumento, int tipoCredito)
        {
            return _repositorioSolicitudCredito.ObtenerSolicitudes(numeroSolicitud, tipoDocumento, numeroDocumento, tipoCredito);
        }
        public IEnumerable<AsegurabilidadOpcion> ObtenerOpcionesAsegurabilidad(bool IdAsegurable, string idCredito, string idPapel)
        {
            return _repositorioSolicitudCredito.ObtenerOpcionesAsegurabilidad(IdAsegurable, idCredito, idPapel);
        }

        public int ValidarITP(string cedula, int bandera)
        {
            return _repositorioSolicitudCredito.ValidarITP(cedula, bandera);
        }

        public int ValidarEmbargo(string cedula, int bandera)
        {
            return _repositorioSolicitudCredito.ValidarEmbargo(cedula, bandera);
        }

        public int ValidarExisExtraprima(string cedula, string tipoIdentificacion, int bandera)
        {
            return _repositorioSolicitudCredito.ValidarExisExtraprima(cedula, tipoIdentificacion, bandera);
        }

        public IEnumerable<ExtraPrima> TraeExtraprima(string cedula, string tipoIdentificacion, int bandera)
        {
            return _repositorioSolicitudCredito.TraeExtraprima(cedula, tipoIdentificacion, bandera);
        }

        public void actualizarEstado(string NumeroSolicitud, string Estado)
        {
            _repositorioSolicitudCredito.actualizarEstado(NumeroSolicitud, Estado);
        }

        public IEnumerable<ArbolAmortizacion> ObtenerArbolAmortizacion(string idtipocredito, string idpapelcavipetrol)
        {
            return _repositorioSolicitudCredito.ObtenerArbolAmortizacion(idtipocredito, idpapelcavipetrol);
        }

        public virtual void GuardarSolicitudInterproductos(List<InformacionProductosCreditos> datos)
        {
            _repositorioSolicitudCredito.GuardarSolicitudInterproductos(datos);
        }
    }
}
