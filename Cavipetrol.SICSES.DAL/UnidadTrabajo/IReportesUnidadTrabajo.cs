﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using System;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public interface IReportesUnidadTrabajo
    {
        IEnumerable<ParametroUsuarioCavipetrol> ObtenerParametrosRegistroCavipetrol();
        bool ActulizarParametrosCavipetrol(ParametroUsuarioCavipetrol registroParametrosCavipetrol);
        bool InsertarHistoricoReporte(ReporteHistorico reporteHistorico);
        IEnumerable<ReporteInformacionEstadistica> ObtenerReporteInfromacionEstadisticaInicial(DateTime fecha_perido);
 
        IEnumerable<ReporteOrganosDireccionyControl> ObtenerReporteOrganosDirreccionControl();

        IEnumerable<ReporteUsuarios> ObtenerReporteUsuarios(DateTime fechaCorte);

        IEnumerable<ReporteRedOficinasYCorresponsalesNoBancarios> ObtenerReporteRedOficinasYCorresponsalesNoBancarios();

        ReporteHistorico ConsultarHistorico(int idReporte, int mes, int ano);

        IEnumerable<ReporteParentescos> ObtenerInformeParentescos();

        IEnumerable<ReporteRelacionBienesRecibosPago> ObtenerRelacionBienesPagoRecibidos();

        IEnumerable<ReporteIndividualDeCarteraDeCredito> ObtenerReporteindividualDeCarteraDeCredito(int Anio, int Mes);

        IEnumerable<ReporteInformeIndividualCaptaciones> ObtenerInformeIndividualCaptaciones(DateTime fechaCorte);

        IEnumerable<ReportePUC> ObtenerInformePUC(DateTime fechaCorte, int periodo);             

        IEnumerable<ReporteAportesContribuciones> ObtenerReporteAportesContribuciones(DateTime fechaCorte);

        IEnumerable<ReporteRelacionPropiedadesPlantaEquipo> ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(DateTime fechaCorte);

        IEnumerable<ReporteRelacionInversiones> ObtenerReporteInformeRelacionInversiones(DateTime fecha_perido);

        IEnumerable<ReporteProductosOfrecidos> ObtenerReporte4Uiaf();

        IEnumerable<ClasificacionExcedentes> ObtenerAplicacionExcedentes(DateTime fechaCorte);

        IEnumerable<ReporteRetiroeIngresoAsociados>ObtenerInformeRetiroeIngresoAsociados(DateTime fechaCorte);

        IEnumerable<ReporteIndividualCarteraCredito> ObtenerInformacionCarteraCredito(string anho, string mes);

        IEnumerable<ReporteDetalleInformacionEstadistica> ObtenerReporteDetalleInfromacionEstadistica(DateTime fecha_perido);
    }
}