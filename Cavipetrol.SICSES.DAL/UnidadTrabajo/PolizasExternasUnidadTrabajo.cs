﻿using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public class PolizasExternasUnidadTrabajo : IPolizasExternasUnidadTrabajo
    {
        private readonly IRepositorioPolizasExternas _repositorioPolizasExternas;
        private readonly PolizaExternaContexto _contexto;
        public PolizasExternasUnidadTrabajo()
        {
            _contexto = new PolizaExternaContexto();
            _repositorioPolizasExternas = new RepositorioPolizasExternas(_contexto);
        }

        public bool InsertarDatosPolizasExternas(List<PolizasExternas> datosPolizasExternas)
        {
            _repositorioPolizasExternas.InsertarDatosPolizasExternas(datosPolizasExternas);

            return GuardarCambios();
        }


        public bool AdministrarPolizasExternas(ViewModelPolizasExternas datosPolizasExternas)
        {
              _repositorioPolizasExternas.AdministrarPolizasExternas(datosPolizasExternas);

            return GuardarCambios();


        }

        public bool ConsultarHistorico(string NumeroDocumento, string NumeroPoliza, string ProductoAmparado, string DocumentoProducto)
        {
            _repositorioPolizasExternas.ConsultarHistorico(NumeroDocumento, NumeroPoliza, ProductoAmparado, DocumentoProducto);

            return GuardarCambios();
        }


        public IEnumerable<ViewModelPolizasExternas> ConsultarPolizasExternas(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza, int DiasVencimiento ,string EstadoPoliza)
        {
            return _repositorioPolizasExternas.ConsultarPolizasExternas( TipoIdentificacion, NumeroDocumento, Nombres, NumeroPoliza, DiasVencimiento, EstadoPoliza);
        }

        public IEnumerable<Aseguradoras> ObtenerAseguradoras()
        {
            return _repositorioPolizasExternas.ObtenerAseguradoras();
        }

        public IEnumerable<TipoPoliza> ObtenerTipoPoliza()
        {
            return _repositorioPolizasExternas.ObtenerTipoPoliza();
        }

        public bool EliminarPolizaExterna(string TipoIdentificacion, string NumeroIdentificacion, string Aseguradora, string NumeroPoliza)
        {
            _repositorioPolizasExternas.EliminarPolizaExterna(TipoIdentificacion, NumeroIdentificacion, Aseguradora, NumeroPoliza);

            return GuardarCambios();
        }

        public IEnumerable<HistoricoPolizasExternas> ConsultarHistoricoPoliza(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza)

        {
            return _repositorioPolizasExternas.ConsultarHistoricoPoliza(TipoIdentificacion, NumeroDocumento, Nombres, NumeroPoliza);
        }

        public RespuestaValidacion ValidarPolizasExternas(string aseguradora, string tipopoliza, string producto, string documento, string nit)

        {
            return _repositorioPolizasExternas.ValidarPolizasExternas(aseguradora, tipopoliza, producto, documento, nit);
        }

        public IEnumerable<Infraestructura.Model.ProductoPorAsociado> ObtenerProductoPorAsociado(string Cedula)
        {
            return _repositorioPolizasExternas.ObtenerProductoPorAsociado(Cedula);
        }

        private bool GuardarCambios()
        {
            int operations = _contexto.SaveChanges();
            return operations > 0 ? true : false;
        }
    }
}
