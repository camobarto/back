﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.DAL.UnidadTrabajo
{
    public interface IConsultasApoyoUnidadTrabajo
    {
        IEnumerable<Municipio> ObtenerMunicipiosPorIdDepartamento(string idDepartamento);
        IEnumerable<Departamento> ObtenerDepartamentos();
        IEnumerable<TipoContrato> ObtenerTiposContratos();
        IEnumerable<TipoEstadoCivil> ObtenerTiposEstadosCiviles();
        IEnumerable<TipoEstrato> ObtenerTiposEstratos();
        IEnumerable<TipoIdentificacion> ObtenerTiposIdentificaciones();
        IEnumerable<TipoJornadaLaboral> ObtenerTiposJornadasLaborales();
        IEnumerable<TipoNivelEscolaridad> ObtenerNivelesEscolaridad();
        IEnumerable<TipoOcupacion> ObtenerTiposOcupacion();
        IEnumerable<TipoOrganoDirectivoControl> ObtenerTiposOrganosDirectivosControl();
        IEnumerable<TipoRol> ObtenerTiposRoles();
        IEnumerable<TipoSectorEconomico> ObtenerTiposSectorEconomico();
        IEnumerable<AsociacionEntidadSolidaria> ObtenerAsociacionesEntidadesSolidarias();
        IEnumerable<FormatoVigenteSICSES> ObtenerFormatosVigentesSICSES();
        IEnumerable<TipoEntidad> ObtenerEntidades();
        IEnumerable<ClasificacionCIIU> ObtenerClasificacionCUU();
        IEnumerable<Meses> ObtenerMeses();
        IEnumerable<Anos> ObtenerAnos();
        IEnumerable<ClasificacionExcedentes> ObtenerInfoExcedentes();
        IEnumerable<FormCapacidadPago> ObtenerFormCapacidadPago(int? idLineaCredito = null);
        IEnumerable<FormCapacidadPagoTipoCreditoTipoRolRelacion> ObtenerFormCapacidadPagoTipoCreditoTipoRolRelacion();
        IEnumerable<FormCapacidadPagoGrupos> ObtenerFormCapacidadPagoGrupos(int? idLineaCredito = null);
        IEnumerable<EvaluacionRiesgoLiquidez> ObtenerInfoEvaluacionRiesgoLiquidez();
        IEnumerable<TiposTitulos> ObtenerTiposTitulos();
        IEnumerable<ClaseGarantia> ObtenerClaseGarantia();
        IEnumerable<DestinoCredito> ObtenerDestinoCredito();
        IEnumerable<ClaseVivienda> ObtenerClaseVivienda();
        IEnumerable<CategoriaReestructurado> ObtenerCategoriaRestructurado();
        IEnumerable<TipoObligacion> ObtenerTipoOligacion();
        IEnumerable<TipoCuota> ObtenerTipoCuota();
        IEnumerable<TipoEntidadBeneficiaria> ObtenerTipoEntidadBeneficiaria();
        IEnumerable<TipoModalidad> ObtenerTipoModalidad();
        IEnumerable<TipoDestino> ObtenerTipoDestino();
        IEnumerable<NivelEducacion> ObtenerNivelEducacion();
        IEnumerable<TipoBeneficiarios> ObtenerTiposBeneficiarios();
        IEnumerable<AlternativasDecreto2880> ObtenerAlternativasDecreto2880();
        IEnumerable<ClaseNaturaleza> ObtenerClaseNaturaleza();
        IEnumerable<ClaseEstadoProcesoActual> ObtenerClaseEstadoProcesoActual();
        IEnumerable<ClaseConcepto> ObtenerClaseConcepto();
        IEnumerable<TipoUsuarioTieneAporte> ObtenerTipoUsuarioTieneAporte();
        IEnumerable<ClaseDeActivo> ObtenerClaseDeActivo();
        IEnumerable<InformacionRelacionadaGrupoInteres> ObtenerInfoReporteGrupoDeInteres();
        IEnumerable<TipoLineaCredito> ObtenerTipoLineaCredito(); 
        IEnumerable<TipoTitulo> ObtenerTipoTitulo();
        IEnumerable<TipoTasa> ObtenerTipoTasa();
        IEnumerable<TipoCredito> ObtenerTipoCredito(int? idLineaCredito = null);
        IEnumerable<TipoModalidadContratacion> ObtenerTipoModalidadContratacion();
        IEnumerable<TipoProductoServicioContratado> ObtenerTipoProductoServicioContratado();
        IEnumerable<TipoCreditoTipoRolRelacion> ObtenerTiposCreditoTiposRolRelacion(int idTipoRol = 0, int? idTipoCreditoTipoRolRelacion = null);
        IEnumerable<Etiquetas> ObtenerEtiquetas(int idTipoEtiqueta);
        IEnumerable<Parametros> ObtenerParametros(string nombre = null);
        IEnumerable<TiposDocumentoRequisito> ObtenerTiposDocumentoRequisito();
        Plantilla ObtenerPlantilla(short idPlantilla);
        IEnumerable<Cupo> ObtenerCupos(short idTipoCredito = 0);
        IEnumerable<TiposCupo> ObtenerTiposCupo();
        IEnumerable<TiposCreditoTipoSeguroRelacion> ObtenerTiposCreditoTiposSeguroRelacion(short idTipoCredito);
        IEnumerable<TiposCreditoTiposGarantiaRelacion> ObtenerTiposCreditoTiposGarantiaRelacion(short idTipoCredito);
        IEnumerable<Bimestre> ObtenerBimestre();
        IEnumerable<TiposGarantiasPagares> ObtenerTiposGarantiasPagares();
        IEnumerable<UsosCredito> ObtenerUsosCredito(int idTipoLinea);
        IEnumerable<CuposAsegurabilidad> ObtenerCuposAsegurabilidad();
        IEnumerable<TipoGarantiaCredito> ObtenerTiposGarantiasCredito(int? idTipoGarantiaCredito = null);
        PapelCavipetrol ObtenerPapelCavipetrol(short id);
        IEnumerable<FormaPago> ObtenerFormaPago(int id = 0);
        IEnumerable<FormaPagoRelacion> ObtenerFormaPagoRelacion(int idTipoCredito, int idTipoPapel, bool mesada14);
        IEnumerable<SeguroTasas> ObtenerSegurosTasas(int? idSeguroTasa = null, int? idTipoSeguro = null, byte? edad = null);
        IEnumerable<TiposPagareTiposConceptoRelacion> ObtenerTiposGarantiaTiposConceptoRelacion(int idPagare);
        IEnumerable<TiposConceptosGarantia> ObtenerTiposConceptoGarantia();
        int ObtenerCodeudorPorGarantia(string idTipoGarantia, string idTipoCodeudor, string valor);
        string ValidarUsuarioCavipetrol(string NumeroIdentificacion);
        IEnumerable<TiposConceptosAdjunto> ObtenerConceptosAdjuntos(string idConcepto);

        Oficina ObtenerOficinas(int? idOficina);
        Municipio ObtenerMunicipio(string Id, string IdDepartamento);
        IEnumerable<TipoConceptoAdjuntoRelacion> ObtenerTiposConceptosTipoAdjuntoRelacion(int idTipoAdjunto);
        IEnumerable<TipoConceptoAdjunto> ObtenerTiposConceptos();
        List<Oficina> ObtenerOficinas();

        IEnumerable<TiposContrato> ObtenerTiposContrato();
        List<CiudadesServidor> ObtenerCiudadesServidor();

        IEnumerable<Usuarios> ObtenerUsuariosPorPerfil(string perfil);

        string AsignarUsuarioSolicitud(int idSolicitud, int idUsuario);
        IEnumerable<SolicitudDescripcionFondos> SolicitudDescripcionFondos();

        IEnumerable<ModelCartaSolicitudCredito> ObtenerCartaSolicitud(string IdSolicitud);

        IEnumerable<Interproductos> ObtenerModelInterproductos(string IdSolicitud);

		IEnumerable<CausalesFodes> ObtenerCausalesFodes();

	}
}
