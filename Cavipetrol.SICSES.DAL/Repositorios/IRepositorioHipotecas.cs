﻿using Cavipetrol.SICSES.Infraestructura.Model.Hipotecas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public interface IRepositorioHipotecas
    {
        IEnumerable<Hipoteca> ObtenerHipotecas(string NumeroHipoteca, string TipoIdentificacion, string NumeroIdentificacion, string NumeroSolicitud);
        void ActualizaHipotecas(int IdHipoteca, string AudUsuarioModificacion, string FechaEscrituracion, string NumeroEscritura, string ValorAvaluo, string VigenciaInicial, string VigenciaFinal,string NumeroNotaria , string Oficina);
        List<Hipoteca> ObtenerHipotecasXCedula(string TipoIdentificacion, string NumeroIdentificacion);
        bool ActualizarHipoteca(int NumeroHipoteca, double ValorCredito);
    }
}
