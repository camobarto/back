﻿using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Data.SqlClient;
using System.Configuration;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioInformes : IRepositorioInformes
    {
        private readonly InformesContexto _contexto;


        public RepositorioInformes(InformesContexto context)
        {
            _contexto = context;
        }
        public IEnumerable<RetencionProveedores> ObtenerInformacionGarantia(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "")
        {
            return _contexto.ObtenerInformacionGarantia(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta).ToList();
        }
        public IEnumerable<RetencionProveedores_ICA> ObtenerInformacionGarantia2(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "")
        {
            return _contexto.ObtenerInformacionGarantia2(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta, Bimestre).ToList();
        }
        public IEnumerable<RetencionProveedores_IVA> ObtenerInformacionGarantia3(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "")
        {
            return _contexto.ObtenerInformacionGarantia3(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta, Bimestre).ToList();
        }

        public void InsertarDatosGarantias(DataTable Datosiva)
        {
            string conString = string.Empty;
            conString = ConfigurationManager.ConnectionStrings["SICAV"].ConnectionString;
            using (SqlConnection con = new SqlConnection(conString))
            {

                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                {
                    //Set the database table name.
                    sqlBulkCopy.DestinationTableName = "dbo.RETENCION_MOVIMIENTO";
                    con.Open();
                    sqlBulkCopy.WriteToServer(Datosiva);
                    con.Close();
                }
            }
            }
        public void ActualizaInfoProveedores(int bandera)
        {
            _contexto.ActualizaInfoProveedores(bandera);
        }
        public IEnumerable<ProveedoresFaltantes> GeneraProveedoresFaltantes(int bandera)
        {
            return _contexto.GeneraProveedoresFaltantes(bandera);
        }
        public IEnumerable<CuentaContable> ObtenerCuentaContable(int bandera)
        {
            return _contexto.ObtenerCuentaContable(bandera);
        }
        public IEnumerable<AuxTercero> ConsultaAuxTercero(string mes, string anio, string cuenta)
        {
            return _contexto.ConsultaAuxTercero(mes, anio, cuenta);
        }
        public IEnumerable<AuditaCuenta> ConsultaAuditaCuenta(string Fc_Ini, string Fc_Fin, string Cuenta)
        {
            return _contexto.ConsultaAuditaCuenta(Fc_Ini, Fc_Fin, Cuenta);
        }
        public IEnumerable<GMF_BUno> ConsutaGMF_BUno(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            _contexto.Database.CommandTimeout = 500;
            return _contexto.ConsutaGMF_BUno(Fc_Ini, Fc_Fin, Bandera);
        }
        public IEnumerable<GMF_BDos> ConsutaGMF_BDos(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            _contexto.Database.CommandTimeout = 500;
            return _contexto.ConsutaGMF_BDos(Fc_Ini, Fc_Fin, Bandera);
        }
        public IEnumerable<GMF_BTres> ConsutaGMF_BTres(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            _contexto.Database.CommandTimeout = 500;
            return _contexto.ConsutaGMF_BTres(Fc_Ini, Fc_Fin, Bandera);
        }

        public IEnumerable<SaldoInicialCarteraEcopetrol> ObtenerSaldoInicialInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _contexto.ObtenerSaldoInicialInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera).ToList();
        }
        public IEnumerable<EncabezadoCarteraEcopetrol> ObtenerEncabezadoInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _contexto.ObtenerEncabezadoInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera).ToList();
        }
        public IEnumerable<PlanPagosCarteraEcopetrol> ObtenerPlanPagosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _contexto.ObtenerPlanPagosInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera).ToList();
        }
        public IEnumerable<MovimientosCarteraEcopetrol> ObtenerMovimientosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _contexto.ObtenerMovimientosInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera).ToList();
        }
        public IEnumerable<TipoDocumentoPorCedula> ObtenerTipoDocumentoPorCedula(string TipoIdentificacion, int Cedula)
        {
            return _contexto.ObtenerTipoDocumentoPorCedula(TipoIdentificacion, Cedula);
        }
        public IEnumerable<FacturaElectronica> ObtenerFacturaElectronica(string Factura, string Servidor)
        {
            return _contexto.ObtenerFacturaElectronica(Factura, Servidor);
        }

        public void GuardarNota(Nota nota)
        {
            List<Items> items = new List<Items>();
            items = nota.Items;
            List<Etiquetas> etiquetasList = new List<Etiquetas>();
            etiquetasList = nota.Etiquetas;
            using (var transaccion = _contexto.Database.BeginTransaction())
            {
                try
                {
                    var variableNota = _contexto.Nota.Where(x => x.IdNota == nota.IdNota).SingleOrDefault();
                    if (variableNota == null)
                    {
                        _contexto.Entry(nota).State = EntityState.Added;
                    }
                    else
                    {
                        _contexto.Entry(nota).State = EntityState.Modified;
                    }
                    _contexto.SaveChanges();

                    foreach (Items item in items)
                    {
                        item.IdNota = nota.IdNota;
                        if (item.IdItemNota > 0)
                        {
                            _contexto.Entry(item).State = EntityState.Modified;
                        }
                        else
                        {
                            _contexto.Entry(item).State = EntityState.Added;
                        }
                        _contexto.SaveChanges();
                    }

                    foreach (Etiquetas etiqueta in etiquetasList)
                    {
                        etiqueta.IdNota = nota.IdNota;
                        if (etiqueta.IdEtiquetaNota > 0)
                        {
                            _contexto.Entry(etiquetasList).State = EntityState.Modified;
                        }
                        else
                        {
                            _contexto.Entry(etiqueta).State = EntityState.Added;
                        }
                        _contexto.SaveChanges();
                    }
                    transaccion.Commit();
                }
                catch (Exception ex)
                {
                    transaccion.Rollback();
                }
               
            }

        }

        public IEnumerable<Certificaciones_Ingresos_Retenciones> Certificaiones(string NumeroIdentificacion, DateTime FechaCorte)
        {
            return _contexto.Certificaciones(NumeroIdentificacion, FechaCorte).ToList();
        }
    }
}
