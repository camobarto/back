﻿using System.Collections.Generic;
using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity;
using System.Linq;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using System;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioReportes : IRepositorioReportes
    {
        private readonly SICSESContexto _contexto;

        public RepositorioReportes(SICSESContexto context)
        {
            _contexto = context;
        }

        public void ActulizarParametrosCavipetrol(ParametroUsuarioCavipetrol registroParametrosCavipetrol)
        {
            ReporteIdentificacion informacionReporte = registroParametrosCavipetrol.ReportesIdentificacion.FirstOrDefault();

            if (informacionReporte != null && informacionReporte.IdReporteIdentificacion == 0)
            {
                if (!_contexto.ReportesIdentificacion.Any())
                {
                    informacionReporte.IdParametroCavipetrol = registroParametrosCavipetrol.Id;
                    _contexto.Entry(informacionReporte).State = EntityState.Added;
                }
            }
            else
            {
                _contexto.Entry(informacionReporte).State = EntityState.Modified;
            }
            _contexto.Entry(registroParametrosCavipetrol).State = EntityState.Modified;
        }

        public void InsertarHistoricoReporte(ReporteHistorico ReporteHistorico)
        {
            _contexto.Entry(ReporteHistorico).State = EntityState.Added;
        }

        public IEnumerable<ParametroUsuarioCavipetrol> ObtenerParametrosRegistroCavipetrol()
        {
            return _contexto.ParametrosCavipetrol.Include(p => p.ReportesIdentificacion).AsNoTracking().ToList();
        }

        public IEnumerable<ReporteInformacionEstadistica> ObtenerReporteInformacionEstadisticaInicial(DateTime fecha_perido)
        {
            return _contexto.ObtenerReporteInformacionEstadisticaInicial(fecha_perido).ToList();
        }

        public IEnumerable<ReporteOrganosDireccionyControl> ObtenerReporteOrganosDirreccionControl()
        {
            return _contexto.ObtenerReporteOrganosDirreccionControl().ToList();
        }

        public IEnumerable<ReporteIndividualDeCarteraDeCredito> ObtenerReporteindividualDeCarteraDeCredito(int Anio, int Mes)
        {
            return _contexto.ObtenerReporteindividualDeCarteraDeCredito(Anio, Mes).ToList();
        }
        public IEnumerable<ReporteRedOficinasYCorresponsalesNoBancarios> ObtenerReporteRedOficinasYCorresponsalesNoBancarios()
        {
            return _contexto.ObtenerRedOficinasYCorresponsalesNoBancarios().ToList();
        }

        public IEnumerable<ReporteUsuarios> ObtenerReporteUsuarios(DateTime fechaCorte)
        {
            return _contexto.ObtenerReporteUsuarios(fechaCorte).ToList();
        }

        public ReporteHistorico ConsultarHistorico(int idReporte, int mes, int ano)
        {
            string strMes = mes.ToString();
            string strAno = ano.ToString();
            var result = _contexto.ReporteHistorico
                .Where(x => x.NumeroReporte == idReporte && x.Mes == strMes && x.Ano == strAno)
                .FirstOrDefault();

            return result;
        }

        public IEnumerable<ReporteParentescos> ObtenerInformeParentescos()
        {
            return _contexto.ObtenerInformeParentescos().ToList();
        }

        public IEnumerable<ReporteRelacionBienesRecibosPago> ObtenerRelacionBienesPagoRecibidos()
        {
            return _contexto.ObtenerRelacionBienesPagoRecibidos().ToList();
        }
        public IEnumerable<ReporteIndividualCarteraCredito> ObtenerInformacionCarteraCredito(string anho, string mes)
        {
            _contexto.Database.CommandTimeout = 5000;
            var algo = _contexto.ObtenerInformacionCarteraCredito(anho, mes).ToList();
            return algo;
        }
        public IEnumerable<ReporteInformeIndividualCaptaciones> ObtenerInformeIndividualCaptaciones(DateTime fechaCorte)
        {
            
            return  _contexto.ObtenerReporteInformeIndividualCaptaciones(fechaCorte).ToList();
        }



        public IEnumerable<ReportePUC> ObtenerInformePUC(DateTime fechaCorte, int periodo)
        {
            return _contexto.ObtenerReportePUC(fechaCorte, periodo).ToList();
        }
        public IEnumerable<ReporteAportesContribuciones> ObtenerReporteAportesContribuciones(DateTime fechaCorte)
        {
            return _contexto.ObtenerReporteAportesContribuciones(fechaCorte).ToList();
        }

        public IEnumerable<ReporteRelacionPropiedadesPlantaEquipo> ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(DateTime fechaCorte)
        {
            return _contexto.ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(fechaCorte).ToList();
        }

        public IEnumerable<ReporteRelacionInversiones> ObtenerReporteInformeRelacionInversiones(DateTime fecha_perido)
        {
            return _contexto.ObtenerReporteInformeRelacionInversiones(fecha_perido).ToList();
        }

        public IEnumerable<ReporteProductosOfrecidos> ObtenerReporte4Uiaf()
        {
            return _contexto.ObtenerReporte4Uiaf().ToList();
        }

        public IEnumerable<ClasificacionExcedentes> ObtenerAplicacionExcedentes(DateTime fechaCorte)
        {
            return _contexto.ObtenerAplicacionExcedentes(fechaCorte).ToList();
        }

        public IEnumerable<ReporteRetiroeIngresoAsociados>ObtenerInformeRetiroeIngresoAsociados(DateTime fechaCorte)
        {
            return _contexto.ObtenerInformeRetiroeIngresoAsociados(fechaCorte).ToList();
        }        

        public IEnumerable<ReporteDetalleInformacionEstadistica> ObtenerReporteDetalleInformacionEstadistica(DateTime fecha_perido)
        {
            return _contexto.ObtenerReporteDetalleInformacionEstadistica(fecha_perido).ToList();
        }
    }
}
