﻿using Cavipetrol.SICSES.Infraestructura.Model.Hipotecas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioHipotecas : IRepositorioHipotecas
    {
        private readonly HipotecasContexto _contexto;
        
        public RepositorioHipotecas(HipotecasContexto context)
        {
            _contexto = context;
        }
        public IEnumerable<Hipoteca> ObtenerHipotecas(string NumeroHipoteca, string TipoIdentificacion, string NumeroIdentificacion, string NumeroSolicitud)
        {
            int NumeroSolicitudint = 0;
            if (NumeroSolicitud != "")
            {
                NumeroSolicitudint = int.Parse(NumeroSolicitud);
            }
            return _contexto.DatosHipoteca.Where(x => x.NumeroHipoteca == (NumeroHipoteca == "" ? x.NumeroHipoteca : NumeroHipoteca) 
                                                && x.TipoDocumentoHipotecario == (TipoIdentificacion == "" ? x.TipoDocumentoHipotecario : TipoIdentificacion)
                                                && x.CedulaHipotecario == (NumeroIdentificacion == "" ? x.CedulaHipotecario : NumeroIdentificacion)
                                                && x.IdHipoteca == (NumeroSolicitud == "" ? x.IdHipoteca : NumeroSolicitudint));
        }
        public void ActualizaHipotecas(int IdHipoteca, string AudUsuarioModificacion, string FechaEscrituracion, string NumeroEscritura, string ValorAvaluo, string VigenciaInicial, string VigenciaFinal, string NumeroNotaria, string c)
        {
            _contexto.ActualizaHipotecas(IdHipoteca, AudUsuarioModificacion, FechaEscrituracion, NumeroEscritura, ValorAvaluo, VigenciaInicial, VigenciaFinal, NumeroNotaria, NumeroNotaria);
        }
        public List<Hipoteca> ObtenerHipotecasXCedula(string TipoIdentificacion, string NumeroIdentificacion)
        {
           return  _contexto.DatosHipoteca.Where(x => x.TipoDocumentoHipotecario == (TipoIdentificacion == "" ? x.TipoDocumentoHipotecario : TipoIdentificacion)
                                                   && x.CedulaHipotecario == (NumeroIdentificacion == "" ? x.CedulaHipotecario : NumeroIdentificacion)).ToList();
        }
        public bool ActualizarHipoteca(int NumeroHipoteca, double ValorCredito)
        {
            var hipoteca = _contexto.DatosHipoteca.Find(NumeroHipoteca);
            if (hipoteca != null)
            {
                hipoteca.ValorCubierto += ValorCredito;
                _contexto.Entry(hipoteca).State = System.Data.Entity.EntityState.Modified;
            }
            return _contexto.SaveChanges() > 0;
        }
    }
}
