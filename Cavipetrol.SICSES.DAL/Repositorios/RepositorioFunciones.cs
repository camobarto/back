﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioFunciones : IRepositorioFunciones
    {
        private readonly FuncionesContexto _contexto;

        public RepositorioFunciones(FuncionesContexto context)
        {
            _contexto = context;
        }

        public void EnviarSMS(DatosSMS datossms  )
        {
            _contexto.EnviarSMS(datossms);
        }

    }
}
