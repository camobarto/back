﻿using Cavipetrol.SICSES.DAL.Mapeo.Parametros;
using Cavipetrol.SICSES.DAL.Mapeo;
using Cavipetrol.SICSES.Infraestructura.Model;
using System.Collections.Generic;
using System.Linq;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Menu;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioParametros : IRepositorioParametros
    {
        private readonly ParametrosCoreContexto _contexto;
        public RepositorioParametros(ParametrosCoreContexto contexto)
        {
            _contexto = contexto;
        }

        public IEnumerable<TipoContrato> ObtenerTiposContratos()
        {
            return _contexto.TiposContratos.ToList();
        }

        public IEnumerable<TipoEstadoCivil> ObtenerTiposEstadosCiviles()
        {
            return _contexto.TiposEstadosCiviles.ToList();
        }
        public IEnumerable<TipoEstrato> ObtenerTiposEstratos()
        {
            return _contexto.TiposEstratos.ToList();
        }
        public IEnumerable<TipoIdentificacion> ObtenerTiposIdentificaciones()
        {
            return _contexto.TiposIdentificaciones.ToList();
        }
        public IEnumerable<TipoJornadaLaboral> ObtenerTiposJornadasLaborales()
        {
            return _contexto.TiposJornadasLaborales.ToList();
        }
        public IEnumerable<TipoNivelEscolaridad> ObtenerNivelesEscolaridad()
        {
            return _contexto.TiposNivelesEscolaridad.ToList();
        }
        public IEnumerable<TipoOcupacion> ObtenerTiposOcupacion()
        {
            return _contexto.TiposOcupaciones.ToList();
        }
        public IEnumerable<TipoOrganoDirectivoControl> ObtenerTiposOrganosDirectivosControl()
        {
            return _contexto.TiposOrganosDirectivosControl.ToList();
        }
        public IEnumerable<TipoRol> ObtenerTiposRoles()
        {
            return _contexto.TiposRoles.ToList();
        }
        public IEnumerable<TipoSectorEconomico> ObtenerTiposSectorEconomico()
        {
            return _contexto.TiposSectoresEconomicos.ToList();
        }
        public IEnumerable<AsociacionEntidadSolidaria> ObtenerAsociacionesEntidadesSolidarias()
        {
            return _contexto.AsociacionesEntidadSolidaria.ToList();
        }
        public IEnumerable<FormatoVigenteSICSES> ObtenerFormatosVigentesSICSES()
        {
            return _contexto.FormatosVigentesSICSES.ToList();
        }

        public IEnumerable<TipoEntidad> ObtenerEntidades()
        {
            return _contexto.TiposEntidades.ToList();
        }
        public IEnumerable<ClasificacionCIIU> ObtenerClasificacionCUU()
        {
            return _contexto.TiposClasificacionCIIU.ToList();
        }
        public IEnumerable<Meses> ObtenerMeses()
        {
            return _contexto.Meses.ToList();
        }

        public IEnumerable<Anos> ObtenerAnos()
        {
            return _contexto.Ano.ToList();
        }

        public IEnumerable<ClasificacionExcedentes> ObtenerInfoExcedentes()
        {
            return _contexto.Excedentes.ToList();
        }
        public IEnumerable<FormCapacidadPago> ObtenerFormCapacidadPago(int? idLineaNegocio = null)
        {
            return _contexto.FormCapacidadPago.ToList();
        }
        public IEnumerable<FormCapacidadPagoTipoCreditoTipoRolRelacion> ObtenerFormCapacidadPagoTipoCreditoTipoRolRelacion()
        {
            return _contexto.FormCapacidadPagoTipoCreditoTipoRolRelacion.ToList();
        }
        public IEnumerable<FormCapacidadPagoGrupos> ObtenerFormCapacidadPagoGrupos()
        {
            return _contexto.FormCapacidadPagoGrupos.ToList();
        }

        public IEnumerable<EvaluacionRiesgoLiquidez> ObtenerInfoEvaluacionRiesgoLiquidez()
        {
            return _contexto.RiesgoLiquidez.OrderBy(x => x.UNIDAD_CAPTURA).ToList();
        }
        public IEnumerable<TiposTitulos> ObtenerTiposTitulos()
        {
            return _contexto.TiposTitulos.ToList();
        }

        public IEnumerable<TipoObligacion> ObtenerTipoOligacion()
        {
            return _contexto.TiposObligaciones.ToList();
        }

        public IEnumerable<TipoCuota> ObtenerTipoCuota()
        {
            return _contexto.Tiposcuotas.ToList();
        }

        public IEnumerable<ClaseGarantia> ObtenerClaseGarantia()
        {
            return _contexto.ClaseGarantia.ToList();
        }
        public IEnumerable<DestinoCredito> ObtenerDestinoCredito()
        {
            return _contexto.DestinoCredito.ToList();
        }
        public IEnumerable<ClaseVivienda> ObtenerClaseVivienda()
        {
            return _contexto.ClaseVivienda.ToList();
        }
        public IEnumerable<CategoriaReestructurado> ObtenerCategoriaRestructurado()
        {
            return _contexto.CategoriaRestructurado.ToList();
        }

        public IEnumerable<TipoEntidadBeneficiaria> ObtenerTipoEntidadBeneficiaria()
        {
            return _contexto.TipoEntidadBeneficiaria.ToList();
        }

        public IEnumerable<TipoModalidad> ObtenerTipoModalidad()
        {
            return _contexto.TipoModalidad.ToList();
        }
        public IEnumerable<ClaseNaturaleza> ObtenerClaseNaturaleza()
        {
            return _contexto.ClaseNaturaleza.ToList();
        }
        public IEnumerable<ClaseEstadoProcesoActual> ObtenerClaseEstadoProcesoActual()
        {
            return _contexto.ClaseEstadoProcesoActual.ToList();
        }
        public IEnumerable<ClaseConcepto> ObtenerClaseConcepto()
        {
            return _contexto.ClaseConcepto.ToList();
        }

        public IEnumerable<TipoDestino> ObtenerTipoDestino()
        {
            return _contexto.TipoDestino.ToList();
        }

        public IEnumerable<NivelEducacion> ObtenerNivelEducacion()
        {
            return _contexto.NivelEducacion.ToList();
        }

        public IEnumerable<TipoBeneficiarios> ObtenerTiposBeneficiarios()
        {
            return _contexto.TipoBeneficiarios.ToList();
        }

        public IEnumerable<AlternativasDecreto2880> ObtenerAlternativasDecreto2880()
        {
            return _contexto.AlternativasDecreto2880.ToList();
        }

        public IEnumerable<TipoUsuarioTieneAporte> ObtenerTipoUsuarioTieneAporte()
        {
            return _contexto.TipoUsuarioTieneAporte.ToList();
        }
        public IEnumerable<ClaseDeActivo> ObtenerClaseDeActivo()
        {
            return _contexto.ClaseDeActivo.ToList();
        }
        public IEnumerable<InformacionRelacionadaGrupoInteres> ObtenerInfoReporteGrupoDeInteres()
        {
            return _contexto.GrupoInteres.ToList();
        }

        public IEnumerable<TipoLineaCredito> ObtenerTipoLineaCredito()
        {
            return _contexto.TipoLineaCredito.ToList();
        }
        public IEnumerable<TipoCredito> ObtenerTipoCredito()
        {
            return _contexto.TipoCredito.ToList();
        }
        public IEnumerable<TipoTitulo> ObtenerTipoTitulo()
        {
            return _contexto.TipoDetitulo.ToList();
        }

        public IEnumerable<TipoTasa> ObtenerTipoTasa()
        {
            return _contexto.TiposTasas.ToList();
        }

        public IEnumerable<TipoModalidadContratacion> ObtenerTipoModalidadContratacion()
        {
            return _contexto.TipoModalidadContratacion.ToList();
        }
        public IEnumerable<TipoProductoServicioContratado> ObtenerTipoProductoServicioContratado()
        {
            return _contexto.TipoProductoServicioContratado.ToList();
        }
        public IEnumerable<TipoCreditoTipoRolRelacion> ObtenerTiposCreditoTiposRolRelacion(int idTipoRol = 0, int? idTipoCreditoTipoRolRelacion = null)
        {
            return _contexto.TiposCreditoTiposRolRelacion.Where(x => x.IdPapelCavipetrol == (idTipoRol == 0? x.IdPapelCavipetrol: idTipoRol))
                                                         .Where(y => y.IdTipoCreditoTipoRolRelacion == (idTipoCreditoTipoRolRelacion == null? y.IdTipoCreditoTipoRolRelacion : idTipoCreditoTipoRolRelacion)) 
                                                         .ToList();
        }
        public IEnumerable<Etiquetas> ObtenerEtiquetas(int idTipoEtiqueta)
        {
            return _contexto.Etiquetas.Where(x => x.IdTipoEtiqueta == idTipoEtiqueta).ToList();
        }
        public IEnumerable<Parametros> ObtenerParametros(string nombre = null)
        {
            return _contexto.Parametros.Where(x => x.Nombre == (nombre == null ? x.Nombre : nombre)).ToList();
        }

        public IEnumerable<TiposDocumentoRequisito> ObtenerTiposDocumentoRequisito()
        {
            return _contexto.TiposDocumentoRequisito.ToList();
        }

        public Plantilla ObtenerPlantilla(short idPlantilla)
        {
            return _contexto.Plantilla.Where(x => x.IdPlantilla == idPlantilla).SingleOrDefault();
        }

        public IEnumerable<Cupo> ObtenerCupos(short idTipoCredito = 0)
        {
            return _contexto.Cupo.Where(x => x.IdTipoCredito == (idTipoCredito == 0 ? x.IdTipoCredito : idTipoCredito)).ToList();
        }

        public IEnumerable<TiposCupo> ObtenerTiposCupo()
        {
            return _contexto.TipoCupo.ToList();
        }
        public IEnumerable<TiposCreditoTipoSeguroRelacion> ObtenerTiposCreditoTiposSeguroRelacion(short idTipoCredito)
        {
            return _contexto.TiposCreditoTipoSeguroRelacion.Where(x => x.IdTipoCredito == idTipoCredito).ToList();
        }

        public IEnumerable<TiposCreditoTiposGarantiaRelacion> ObtenerTiposCreditoTiposGarantiaRelacion(short idTipoCredito)
        {
            return _contexto.TiposCreditoTiposGarantiaRelacion.Include("TiposGarantia").Where(x => x.IdTipoCredito == idTipoCredito).ToList();
        }

        public IEnumerable<Bimestre> ObtenerBimestre()
        {
            return _contexto.Bimestre.ToList();
        }
        public IEnumerable<TiposGarantiasPagares> ObtenerTiposGarantiasPagares()
        {
            return _contexto.TiposGarantiasPagares.ToList();
        }


        public IEnumerable<UsosCredito> ObtenerUsosCredito(int idTipoLinea)
        {
            return _contexto.UsosCredito.Where(x => x.IdTipoLineaCredito == idTipoLinea).ToList().OrderBy(x => x.Nombre);
        }

        public IEnumerable<CuposAsegurabilidad> ObtenerCuposAsegurabilidad()
        {
            return _contexto.CuposAsegurabilidad.ToList();
        }

        public IEnumerable<TipoGarantiaCredito> ObtenerTiposGarantiasCredito(int? idTipoGarantiaCredito = null)
        {
            return _contexto.ObtenerTiposGarantiasCredito(idTipoGarantiaCredito).ToList();
        }       
        public PapelCavipetrol ObtenerPapelCavipetrol(short id)
        {
            return _contexto.PapelCavipetrol.Where(x => x.IdPapelCavipetrol == id).SingleOrDefault();
        }
        public IEnumerable<FormaPago> ObtenerFormaPago(int id = 0)
        {
            return _contexto.FormaPago.Where(x => x.IdFormaPago == (id == 0 ? x.IdFormaPago : id));
        }

        public IEnumerable<FormaPagoRelacion> ObtenerFormaPagoRelacion(int idTipoCredito, int idPapelCavipetrol, bool mesada14)
        {
            var respuesta = _contexto.FormaPagoRelacion.Where(x => x.IdTipoCredito == idTipoCredito && x.IdPapelCavipetrol == idPapelCavipetrol
                                                   && x.Mesada14 == (mesada14 == true ? mesada14 : false)).ToList();
            return respuesta;
        }
        public IEnumerable<SeguroTasas> ObtenerSegurosTasas(int? idSeguroTasa, int? idTipoSeguro, byte? edad)
        {
            IEnumerable<SeguroTasas> listaSegurosTasas = _contexto.SeguroTasas.Where(x => x.IdSeguroTasa == (idSeguroTasa ?? x.IdSeguroTasa)
                                                                                       && x.IdTipoSeguro == (idTipoSeguro ?? x.IdTipoSeguro)).ToList();
            if (edad != null) {
                listaSegurosTasas = listaSegurosTasas.Where(x => x.EdadInicial <= edad
                                                              && x.EdadFinal >= edad).ToList();
            }

            return listaSegurosTasas;
        }

        public IEnumerable<TiposPagareTiposConceptoRelacion> ObtenerTiposGarantiaTiposConceptoRelacion(int idPagare)
        {   
            return _contexto.TiposPagareTiposConceptoRelacion.Where(x => x.IdPagare == idPagare).ToList();
        }

        public IEnumerable<TiposConceptosGarantia> ObtenerTiposConceptoGarantia()
        {
            return _contexto.TiposConceptosGarantia.ToList();
        }

        public int ObtenerCodeudorPorGarantia(string idTipoGarantia, string idTipoCodeudor, string valor)
        {
            int idGarantia = 0;
            int idVariable = 0;
            double rango = 0;
            int.TryParse(idTipoGarantia, out idGarantia);
            int.TryParse(idTipoCodeudor, out idVariable);
            rango = Double.Parse(valor);
            
            if (idVariable == 1)
            {
                int Salario = 1;
                int.TryParse(_contexto.Parametros.Where(x => x.Nombre == "SMMLV").FirstOrDefault().Valor, out Salario);
                rango = rango / Salario;
            }
            if ((idGarantia == 2 && rango > 110) || (idGarantia == 10 && rango > 120) || (idGarantia == 18 && rango > 50))
            {
                var guardar = 404;
                return guardar;
            }
            else
            {
                var guardar = _contexto.GarantiaVariableRelacion.Where(x => x.IdTipoCreditoTipoGarantia == idGarantia && x.IdGarantiaVariable == idVariable
                && x.RangoInicial < rango && x.RangoFinal >= rango).SingleOrDefault();
                return guardar == null ? 0 : guardar.Codeudores;
            }
            
        }
        public string ValidarUsuarioCavipetrol(string NumeroIdentificacion)
        {
            return _contexto.ValidarUsuarioCavipetrol(NumeroIdentificacion).FirstOrDefault();
        }

        public IEnumerable<TiposConceptosAdjunto> ObtenerConceptosAdjuntos(string idConcepto)
        {
            return _contexto.ObtenerConceptosAdjuntos(idConcepto).ToList();
        }
        public Oficina ObtenerOficinas(int? idOficina)
        {
            return _contexto.Oficina.Where(x => x.IdOficina == idOficina).FirstOrDefault();
        }
        public Municipio ObtenerMunicipio(string Id,string IdDepartamento)
        {
            return _contexto.Municipios.Where(x => x.Id.Equals(Id) && x.IdDepartamento.Equals(IdDepartamento)).FirstOrDefault();
        }

        public IEnumerable<TipoConceptoAdjuntoRelacion> ObtenerTiposConceptosTipoAdjuntoRelacion(int idTipoAdjunto) => _contexto.TipoConceptoAdjuntoRelacion.Where(x => x.IdTipoAdjunto == idTipoAdjunto).ToList();
        public IEnumerable<TipoConceptoAdjunto> ObtenerTiposConceptos() => _contexto.TipoConceptoAdjunto.ToList();
        public List<Oficina> ObtenerOficinas()
        {
            return _contexto.Oficina.ToList();
        }
        public IEnumerable<TiposContrato> ObtenerTiposContrato() => _contexto.TiposContratoSolicitud.ToList();
        public List<CiudadesServidor> ObtenerCiudadesServidor()
        {
            return _contexto.ObtenerCiudadesServidor().ToList();
        }
        public IEnumerable<Usuarios> ObtenerUsuariosPorPerfil(string perfil)
        {
            return _contexto.ObtenerUsuariosPorPerfil(perfil).ToList();
        }
        public string AsignarUsuarioSolicitud(int idSolicitud, int idUsuario)
        {
            return _contexto.AsignarUsuarioSolicitud(idSolicitud, idUsuario).FirstOrDefault();
        }
        public IEnumerable<SolicitudDescripcionFondos> SolicitudDescripcionFondos()
        {
            return _contexto.SolicitudDescripcionFondos.ToList();
        }

        public IEnumerable<ModelCartaSolicitudCredito> ObtenerCartaSolicitud(string IdSolicitud)
        {
            return _contexto.ObtenerCartaSolicitud(IdSolicitud).ToList();
        }

        public IEnumerable<Interproductos> ObtenerModelInterproductos(string IdSolicitud)
        {
            return _contexto.ObtenerModelInterproductos(IdSolicitud).ToList();
        }

		public IEnumerable<CausalesFodes> ObtenerCausalesFodes()
		{
			return _contexto.ObtenerCausalesFodes().ToList();
		}
	}
}
