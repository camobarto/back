﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioPolizasExternas : IRepositorioPolizasExternas
    {
        private readonly PolizaExternaContexto _contexto;

        public RepositorioPolizasExternas(PolizaExternaContexto context)
        {
            _contexto = context;
        }
       
        public void InsertarDatosPolizasExternas(List<PolizasExternas> datosPolizasExternas)
        {
            List<TipoPoliza> TiposPolizas = new List<TipoPoliza>();
            List<Aseguradoras> Aseguradoras = new List<Aseguradoras>();

            Aseguradoras = _contexto.Aseguradoras.ToList();
            TiposPolizas = _contexto.TipoPoliza.ToList();




            datosPolizasExternas = datosPolizasExternas.Join(Aseguradoras, post => post.Aseguradora == null ? string.Empty : post.Aseguradora.Replace(" ", "").Trim().ToUpper(),
                                                                            meta => meta.Nombre_aseguradora == null ? string.Empty : meta.Nombre_aseguradora.Replace(" ", "").Trim().ToUpper(),
                                                                            (post, meta) => new PolizasExternas()
                                                                            {
                                                                                id_Aseguradora = meta.id_aseguradora,
                                                                                Oficina = post.Oficina,
                                                                                Tipo_documento = post.Tipo_documento,
                                                                                Cedula = post.Cedula,
                                                                                Nombre_Apellidos = post.Nombre_Apellidos,
                                                                                Producto_Amparado = post.Producto_Amparado,
                                                                                Documento_No = post.Documento_No,
                                                                                Numero_Poliza = post.Numero_Poliza,
                                                                                Nombre_poliza = post.Nombre_poliza,
                                                                                Valor_Asegurado = post.Valor_Asegurado,
                                                                                Tomador = post.Tomador,
                                                                                Aplicar_Colectiva=post.Aplicar_Colectiva,
                                                                                TipoNitTomador = post.TipoNitTomador,
                                                                                Nit_Tomador = post.Nit_Tomador,
                                                                                Vigencia_Desde = post.Vigencia_Desde,
                                                                                Vigencia_Hasta = post.Vigencia_Hasta,
                                                                                Direccion = post.Direccion,
                                                                                Matricula_Inmobiliaria = post.Matricula_Inmobiliaria,
                                                                                Observaciones = post.Observaciones,
                                                                                Tipo_poliza = post.Tipo_poliza,
                                                                               
                                                                            }).ToList();


            datosPolizasExternas = datosPolizasExternas.Join(TiposPolizas, post => post.Nombre_poliza,
                                                                           meta => meta.Nombre_Poliza,
                                                                           (post, meta) => new PolizasExternas()
                                                                           {
                                                                               //    id_Aseguradora = meta.id_aseguradora,
                                                                               Oficina = post.Oficina,
                                                                               Tipo_documento = post.Tipo_documento,
                                                                               Cedula = post.Cedula,
                                                                               Nombre_Apellidos = post.Nombre_Apellidos,
                                                                               Producto_Amparado = post.Producto_Amparado,
                                                                               Documento_No = post.Documento_No,
                                                                               Numero_Poliza = post.Numero_Poliza,
                                                                               Valor_Asegurado = post.Valor_Asegurado,
                                                                               Tomador = post.Tomador,
                                                                               Aplicar_Colectiva = post.Aplicar_Colectiva,
                                                                               TipoNitTomador = post.TipoNitTomador,
                                                                               Nit_Tomador = post.Nit_Tomador,
                                                                               Vigencia_Desde = post.Vigencia_Desde,
                                                                               Vigencia_Hasta = post.Vigencia_Hasta,
                                                                               Direccion = post.Direccion,
                                                                               Matricula_Inmobiliaria = post.Matricula_Inmobiliaria,
                                                                               Observaciones = post.Observaciones,
                                                                               Tipo_poliza = meta.id_TipoPoliza,
                                                                               id_Aseguradora = post.id_Aseguradora,
                                                                              
                                                                           }).ToList();

            using (var trans = _contexto.Database.BeginTransaction())
            {
                try
                {
                    foreach (PolizasExternas modelo in datosPolizasExternas)
                    {

                        if (_contexto.PolizasExternas.Where(x => x.Cedula == modelo.Cedula && x.Tipo_documento == modelo.Tipo_documento && x.Numero_Poliza == modelo.Numero_Poliza && x.id_Aseguradora == modelo.id_Aseguradora).FirstOrDefault() == null)
                        {
                            _contexto.Entry(modelo).State = EntityState.Added;
                        }
                        else
                        {
                            PolizasExternas modd = _contexto.PolizasExternas.Where(x => x.Cedula == modelo.Cedula && x.Tipo_documento == modelo.Tipo_documento && x.Numero_Poliza == modelo.Numero_Poliza && x.id_Aseguradora == modelo.id_Aseguradora).FirstOrDefault();

                            modd.Nombre_Apellidos = modelo.Nombre_Apellidos;
                            modd.Valor_Asegurado = modelo.Valor_Asegurado;
                            modd.Tomador = modelo.Tomador;
                            modd.Nit_Tomador = modelo.Nit_Tomador;                           
                            modd.Vigencia_Desde = modelo.Vigencia_Desde;
                            modd.Vigencia_Hasta = modelo.Vigencia_Hasta;
                            modd.Direccion = modelo.Direccion;
                            modd.Matricula_Inmobiliaria = modelo.Matricula_Inmobiliaria;
                            modd.Observaciones = modelo.Observaciones;
                            modd.Tipo_poliza = modelo.Tipo_poliza;
                            modd.Activo = "1";

                            _contexto.Entry(modd).State = EntityState.Modified;

                        }
                        _contexto.SaveChanges();

                    }
                    

                    foreach (PolizasExternas modelo in datosPolizasExternas)
                    {

                        _contexto.GuardarPolizaExternaCredito(modelo.Tipo_documento, modelo.Cedula, modelo.id_Aseguradora.ToString(), modelo.Numero_Poliza, modelo.Documento_No, modelo.Producto_Amparado);


                    }
                    trans.Commit();
                }
                catch (Exception e)
                {
                    trans.Rollback();
                }
            }                                       
        }


        public void AdministrarPolizasExternas(ViewModelPolizasExternas datosPolizasExternas)
        {

             _contexto.AdministrarPolizasExternas(datosPolizasExternas);

        }

        public void ConsultarHistorico(string NumeroDocumento, string NumeroPoliza, string ProductoAmparado, string DocumentoProducto)
        {

        }
        public void EliminarPolizaExterna(string TipoIdentificacion, string NumeroIdentificacion, string Aseguradora, string NumeroPoliza)
        {
             _contexto.EliminarPolizaExterna(TipoIdentificacion, NumeroIdentificacion, Aseguradora, NumeroPoliza);
        }



        public  IEnumerable<ViewModelPolizasExternas> ConsultarPolizasExternas(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza, int DiasVencimiento,string EstadoPoliza)
        {
            return _contexto.ConsultarPolizasExternas(TipoIdentificacion, NumeroDocumento, Nombres, NumeroPoliza, DiasVencimiento, EstadoPoliza);
        }

        public IEnumerable<Aseguradoras> ObtenerAseguradoras()
        {
            return _contexto.Aseguradoras.ToList();
        }

        public IEnumerable<TipoPoliza> ObtenerTipoPoliza()
        {
            return _contexto.TipoPoliza.ToList();
        }


        public IEnumerable<HistoricoPolizasExternas> ConsultarHistoricoPoliza(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza)

        {
            return _contexto.ConsultarHistoricoPoliza(TipoIdentificacion, NumeroDocumento, Nombres, NumeroPoliza);
        }

        public RespuestaValidacion ValidarPolizasExternas(string aseguradora, string tipopoliza, string producto, string documento,string nit)
        {
           return  _contexto.ValidarPolizasExternas(aseguradora, tipopoliza, producto, documento, nit).FirstOrDefault();
            
       
        }
        public IEnumerable<Infraestructura.Model.ProductoPorAsociado> ObtenerProductoPorAsociado(string Cedula)
        {
            return _contexto.ObtenerProductoPorAsociado(Cedula).ToList();
        }
    }
}
