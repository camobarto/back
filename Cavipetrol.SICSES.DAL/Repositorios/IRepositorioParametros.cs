﻿using Cavipetrol.SICSES.DAL.Mapeo.Reportes;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Menu;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public interface IRepositorioParametros
    {
        IEnumerable<TipoContrato> ObtenerTiposContratos();
        IEnumerable<TipoEstadoCivil> ObtenerTiposEstadosCiviles();
        IEnumerable<TipoEstrato> ObtenerTiposEstratos();
        IEnumerable<TipoIdentificacion> ObtenerTiposIdentificaciones();
        IEnumerable<TipoJornadaLaboral> ObtenerTiposJornadasLaborales();
        IEnumerable<TipoNivelEscolaridad> ObtenerNivelesEscolaridad();
        IEnumerable<TipoOcupacion> ObtenerTiposOcupacion();
        IEnumerable<TipoOrganoDirectivoControl> ObtenerTiposOrganosDirectivosControl();
        IEnumerable<TipoRol> ObtenerTiposRoles();
        IEnumerable<TipoSectorEconomico> ObtenerTiposSectorEconomico();
        IEnumerable<AsociacionEntidadSolidaria> ObtenerAsociacionesEntidadesSolidarias();
        IEnumerable<FormatoVigenteSICSES> ObtenerFormatosVigentesSICSES();

        IEnumerable<TipoEntidad> ObtenerEntidades();

        IEnumerable<ClasificacionCIIU> ObtenerClasificacionCUU();

        IEnumerable<Meses> ObtenerMeses();

        IEnumerable<Anos> ObtenerAnos();

        IEnumerable<ClasificacionExcedentes> ObtenerInfoExcedentes();
        IEnumerable<FormCapacidadPago> ObtenerFormCapacidadPago(int? idLineaNegocio = null);
        IEnumerable<FormCapacidadPagoTipoCreditoTipoRolRelacion> ObtenerFormCapacidadPagoTipoCreditoTipoRolRelacion();

        IEnumerable<FormCapacidadPagoGrupos> ObtenerFormCapacidadPagoGrupos();

        IEnumerable<EvaluacionRiesgoLiquidez> ObtenerInfoEvaluacionRiesgoLiquidez();
        IEnumerable<TiposTitulos> ObtenerTiposTitulos();

        IEnumerable<TipoObligacion> ObtenerTipoOligacion();

        IEnumerable<TipoCuota> ObtenerTipoCuota();
        
        IEnumerable<ClaseGarantia> ObtenerClaseGarantia();
        IEnumerable<DestinoCredito> ObtenerDestinoCredito();
        IEnumerable<ClaseVivienda> ObtenerClaseVivienda();
        IEnumerable<CategoriaReestructurado> ObtenerCategoriaRestructurado();

        IEnumerable<TipoEntidadBeneficiaria> ObtenerTipoEntidadBeneficiaria();

        IEnumerable<TipoModalidad> ObtenerTipoModalidad();

        IEnumerable<TipoDestino> ObtenerTipoDestino();

        IEnumerable<NivelEducacion> ObtenerNivelEducacion();

        IEnumerable<TipoBeneficiarios> ObtenerTiposBeneficiarios();

        IEnumerable<AlternativasDecreto2880> ObtenerAlternativasDecreto2880();
        IEnumerable<ClaseNaturaleza> ObtenerClaseNaturaleza();
        IEnumerable<ClaseEstadoProcesoActual> ObtenerClaseEstadoProcesoActual();
        IEnumerable<ClaseConcepto> ObtenerClaseConcepto();
        IEnumerable<TipoUsuarioTieneAporte> ObtenerTipoUsuarioTieneAporte();
        IEnumerable<ClaseDeActivo> ObtenerClaseDeActivo();

        IEnumerable<InformacionRelacionadaGrupoInteres> ObtenerInfoReporteGrupoDeInteres();
        IEnumerable<TipoLineaCredito> ObtenerTipoLineaCredito();
        IEnumerable<TipoCredito> ObtenerTipoCredito();

        IEnumerable<TipoTitulo> ObtenerTipoTitulo();

        IEnumerable<TipoTasa> ObtenerTipoTasa();
        IEnumerable<TipoModalidadContratacion> ObtenerTipoModalidadContratacion();
        IEnumerable<TipoProductoServicioContratado> ObtenerTipoProductoServicioContratado();
        IEnumerable<TipoCreditoTipoRolRelacion> ObtenerTiposCreditoTiposRolRelacion(int idTipoRol = 0, int? idTipoCreditoTipoRolRelacion = null);
        IEnumerable<Etiquetas> ObtenerEtiquetas(int idTipoEtiqueta);
        IEnumerable<Parametros> ObtenerParametros(string nombre = null);
        IEnumerable<TiposDocumentoRequisito> ObtenerTiposDocumentoRequisito();
        Plantilla ObtenerPlantilla(short idPlantilla);
        IEnumerable<Cupo> ObtenerCupos(short idTipoCredito = 0);
        IEnumerable<TiposCupo> ObtenerTiposCupo();

        IEnumerable<TiposCreditoTipoSeguroRelacion> ObtenerTiposCreditoTiposSeguroRelacion(short idTipoCredito);
        IEnumerable<TiposCreditoTiposGarantiaRelacion> ObtenerTiposCreditoTiposGarantiaRelacion(short idTipoCredito);

        IEnumerable<Bimestre> ObtenerBimestre();
        IEnumerable<TiposGarantiasPagares> ObtenerTiposGarantiasPagares();
        
        IEnumerable<UsosCredito> ObtenerUsosCredito(int idTipoLinea);

        IEnumerable<CuposAsegurabilidad> ObtenerCuposAsegurabilidad();
        IEnumerable<TipoGarantiaCredito> ObtenerTiposGarantiasCredito(int? idTipoGarantiaCredito = null);

        
        PapelCavipetrol ObtenerPapelCavipetrol(short id);
        IEnumerable<FormaPago> ObtenerFormaPago(int id);

        IEnumerable<FormaPagoRelacion> ObtenerFormaPagoRelacion(int idFormaPagoRelacion = 0, int idTipoCreditoTipoRolRelacion = 0, bool mesada14 = false);
        IEnumerable<SeguroTasas> ObtenerSegurosTasas(int? idSeguroTasa, int? idTipoSeguro, byte? edad);
        IEnumerable<TiposPagareTiposConceptoRelacion> ObtenerTiposGarantiaTiposConceptoRelacion(int idTPagare);
        IEnumerable<TiposConceptosGarantia> ObtenerTiposConceptoGarantia();

        int ObtenerCodeudorPorGarantia(string idTipoGarantia, string idTipoCodeudor, string valor);

        string ValidarUsuarioCavipetrol(string NumeroIdentificacion);

        IEnumerable<TiposConceptosAdjunto> ObtenerConceptosAdjuntos(string idConcepto);
        Oficina ObtenerOficinas(int? idOficina);

        Municipio ObtenerMunicipio(string Id, string IdDepartamento);
        List<Oficina> ObtenerOficinas();

        IEnumerable<TipoConceptoAdjuntoRelacion> ObtenerTiposConceptosTipoAdjuntoRelacion(int idTipoAdjunto);
        IEnumerable<TipoConceptoAdjunto> ObtenerTiposConceptos();

        IEnumerable<TiposContrato> ObtenerTiposContrato();
        List<CiudadesServidor> ObtenerCiudadesServidor();

        IEnumerable<Usuarios> ObtenerUsuariosPorPerfil(string perfil);

        string AsignarUsuarioSolicitud(int idSolicitud, int idUsuario);
        IEnumerable<SolicitudDescripcionFondos> SolicitudDescripcionFondos();

        IEnumerable<ModelCartaSolicitudCredito> ObtenerCartaSolicitud(string IdSolicitud);

        IEnumerable<Interproductos> ObtenerModelInterproductos(string IdSolicitud);

		IEnumerable<CausalesFodes> ObtenerCausalesFodes();

	}
}
