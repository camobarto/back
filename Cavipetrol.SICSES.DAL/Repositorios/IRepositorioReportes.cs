﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using System;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public interface IRepositorioReportes
    {
        IEnumerable<ParametroUsuarioCavipetrol> ObtenerParametrosRegistroCavipetrol();
        void ActulizarParametrosCavipetrol(ParametroUsuarioCavipetrol registroParametrosCavipetrol);
        void InsertarHistoricoReporte(ReporteHistorico reporteHistorico);
        IEnumerable<ReporteInformacionEstadistica> ObtenerReporteInformacionEstadisticaInicial(DateTime fecha_perido);
        IEnumerable<ReporteOrganosDireccionyControl> ObtenerReporteOrganosDirreccionControl();
        IEnumerable<ReporteUsuarios> ObtenerReporteUsuarios(DateTime fechaCorte);
        IEnumerable<ReporteRedOficinasYCorresponsalesNoBancarios> ObtenerReporteRedOficinasYCorresponsalesNoBancarios();

        IEnumerable<ReporteIndividualDeCarteraDeCredito> ObtenerReporteindividualDeCarteraDeCredito(int Anio, int Mes);
        ReporteHistorico ConsultarHistorico(int idReporte, int mes, int ano);

        IEnumerable<ReporteParentescos> ObtenerInformeParentescos();
        IEnumerable<ReporteRelacionBienesRecibosPago> ObtenerRelacionBienesPagoRecibidos();
        IEnumerable<ReporteInformeIndividualCaptaciones> ObtenerInformeIndividualCaptaciones(DateTime fechaCorte);

        IEnumerable<ReportePUC> ObtenerInformePUC(DateTime fechaCorte,int periodo);

        IEnumerable<ReporteAportesContribuciones> ObtenerReporteAportesContribuciones(DateTime fechaCorte);
        IEnumerable<ReporteRelacionPropiedadesPlantaEquipo> ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(DateTime fechaCorte);

        IEnumerable<ReporteRelacionInversiones> ObtenerReporteInformeRelacionInversiones(DateTime fecha_perido);

        IEnumerable<ReporteProductosOfrecidos> ObtenerReporte4Uiaf();

        IEnumerable<ClasificacionExcedentes> ObtenerAplicacionExcedentes(DateTime fechaCorte);
        IEnumerable<ReporteRetiroeIngresoAsociados>ObtenerInformeRetiroeIngresoAsociados(DateTime fechaCorte);
        IEnumerable<ReporteIndividualCarteraCredito> ObtenerInformacionCarteraCredito(string anho, string mes);
        IEnumerable<ReporteDetalleInformacionEstadistica> ObtenerReporteDetalleInformacionEstadistica(DateTime fecha_perido);
    }
}
