﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.Model.ReportesUIAF;
using System;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public interface IRepositorioReportesUIAF
    {
        
        IEnumerable<ReporteProductosEconomiaSolidaria> ObtenerReporteProductosEconomiaSolidaria(DateTime fechaCorte);
        IEnumerable<ReporteTransaccionesEnEfectivo> ObtenerReporteTransaccionesEnEfectivo(DateTime fechaInicio, DateTime fechaFin);
        IEnumerable<ReporteTransaccionesEnEfectivoRiesgos> ObtenerReporteTransaccionesEnEfectivoRiesgos(DateTime fechaInicio, DateTime fechaFin);
    }
}
