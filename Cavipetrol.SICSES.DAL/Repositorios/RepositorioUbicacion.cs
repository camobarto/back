﻿using System.Collections.Generic;
using Cavipetrol.SICSES.Infraestructura.Model;
using System.Linq;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioUbicacion : IRepositorioUbicacion
    {
        private readonly ParametrosCoreContexto _contexto;
        public RepositorioUbicacion(ParametrosCoreContexto contexto)
        {
            _contexto = contexto;
        }
        public IEnumerable<Departamento> ObtenerDepartamentos()
        {
            return _contexto.Departamentos.ToList();
        }

        public IEnumerable<Municipio> ObtenerMunicipiosPorIdDepartamento(string idDepartamento)
        {
            return _contexto.Municipios.Where(m => m.IdDepartamento == idDepartamento).ToList();
        }
    }
}
