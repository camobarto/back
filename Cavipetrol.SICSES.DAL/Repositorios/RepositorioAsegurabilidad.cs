﻿using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioAsegurabilidad : IRepositorioAsegurabilidad
    {
        private readonly AsegurabilidadContexto _contexto;
        public RepositorioAsegurabilidad(AsegurabilidadContexto context)
        {
            _contexto = context;
        }
        public void InsertarDatosAsegurabilidad(List<AsegurabilidadModel> DatosAsegurabilidad)
        {
            foreach (AsegurabilidadModel modelo in DatosAsegurabilidad) {
                _contexto.Entry(modelo).State = EntityState.Added;
                _contexto.SaveChanges();
            }
        }
        public IEnumerable<VMPolizasVigentes> ObtenerPolizasVigentes(string tipo, string numero)
        {
            return _contexto.ObtenerPolizasVigentes(tipo, numero);
        }

    }
}
