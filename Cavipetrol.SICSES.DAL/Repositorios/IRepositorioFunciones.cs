﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public interface IRepositorioFunciones
    {
        void EnviarSMS(DatosSMS datossms);

    }
}
