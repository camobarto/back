﻿using System.Collections.Generic;
using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity;
using System.Linq;
using Cavipetrol.SICSES.Infraestructura.Model.ReportesUIAF;
using System;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioReportesUIAF : IRepositorioReportesUIAF
    {
        private readonly SICSESContexto _contexto;

        public RepositorioReportesUIAF(SICSESContexto context)
        {
            _contexto = context;
        }


        public IEnumerable<ReporteTransaccionesEnEfectivo> ObtenerReporteTransaccionesEnEfectivo(DateTime fechaInicio, DateTime fechaFin)
        {
            return _contexto.ObtenerReporteTransaccionesEnEfectivo(fechaInicio, fechaFin).ToList();
        }

        public IEnumerable<ReporteTransaccionesEnEfectivoRiesgos> ObtenerReporteTransaccionesEnEfectivoRiesgos(DateTime fechaInicio, DateTime fechaFin)
        {
            return _contexto.ObtenerReporteTransaccionesEnEfectivoRiesgos(fechaInicio, fechaFin).ToList();
        }

        public IEnumerable<ReporteProductosEconomiaSolidaria> ObtenerReporteProductosEconomiaSolidaria(DateTime fechaCorte)
        {
            return _contexto.ObtenerReporteProductosEconomiaSolidaria(fechaCorte).ToList();
        }

    }
}
