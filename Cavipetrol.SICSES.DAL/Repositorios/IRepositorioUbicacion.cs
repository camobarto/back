﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public interface IRepositorioUbicacion
    {
        IEnumerable<Municipio> ObtenerMunicipiosPorIdDepartamento(string idDepartamento);
        IEnumerable<Departamento> ObtenerDepartamentos();
    }
}
