﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioSolicitudCredito : IRepositorioSolicitudCredito
    {
        private CreditosContexto _contexto;


        public RepositorioSolicitudCredito(CreditosContexto context)
        {
            _contexto = context;
        }

        public InformacionAsegurabilidad ObtenerInformacionAsegurabilidad(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _contexto.ObtenerInformacionAsegurabilidad(numeroIdentificacion, tipoIdentificacion).FirstOrDefault();
        }

        public InformacionSolicitante ObtenerInformacionAsociado(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _contexto.ObtenerInformacionAsociado(numeroIdentificacion, tipoIdentificacion).FirstOrDefault();
        }

        public InformacionSolicitante ObtenerInformacionEmpleado(string numeroIdentificacion, string tipoIdentificacion)
        {
            InformacionSolicitante result = _contexto.ObtenerInformacionEmpleado(numeroIdentificacion, tipoIdentificacion).FirstOrDefault();
            return result;
        }

        public InformacionSolicitanteCampanaEspecial ObtenerInformacionSolicitanteCampanaEspecial(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _contexto.ObtenerInformacionSolicitanteCampanaEspecial(numeroIdentificacion, tipoIdentificacion).FirstOrDefault();
        }
        public IEnumerable<NormaProductos> ObtenerInformacionNormas(string producto, string papel, short idTipoCredito)
        {
            var lista = _contexto.ObtenerInformacionNormas(producto, papel, idTipoCredito).ToList();
            return lista;
        }        
        public IEnumerable<SaldosColocacion>ObtenerCreditosDisponiblesSaldo(string numeroIdentificacion, string tipoIdentificacion, string papel)
        {
            return _contexto.ObtenerCreditosDisponiblesSaldo(numeroIdentificacion, tipoIdentificacion, papel).ToList();
        }
        
        public ValidacionesSolicitudCreditoProducto ObtenerRestriccionesProducto(string identificacion, string tipoIdentificacion, string papel, string nombreProducto)
        {
            var listaDocumentos = _contexto.ObtenerRestriccionesProducto(identificacion, tipoIdentificacion, papel, nombreProducto).ToList();
            ValidacionesSolicitudCreditoProducto validaciones = new ValidacionesSolicitudCreditoProducto();
            if (listaDocumentos != null || listaDocumentos.Count <= 0)
            {
                validaciones.Saldo = listaDocumentos.Sum(x => x.Saldo);
                validaciones.FechaDesembolso = listaDocumentos.Where(x => x.Saldo > 0).Min(x => x.FechaDesembolso);
                validaciones.Producto = nombreProducto;
            }
            else
            {
                validaciones.Saldo = 0;
                validaciones.FechaDesembolso = new System.DateTime(1900, 1, 1);
            }
            return validaciones;
        }

        public ValidacionesSolicitudCreditoUsuario ObtenerValidacionesSolicitudCredito(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _contexto.ObternerValidacionesSolicitudCredito(numeroIdentificacion, tipoIdentificacion).FirstOrDefault();
        }

        public NormaProductos ObtenerInformacionDetalleNorma(string producto, string norma)
        {

            var detalles = _contexto.ObtenerInformacionDetalleNorma(producto, norma);
            NormaProductos normaProducto = new NormaProductos();
            foreach (var detalle in detalles)
            {
                normaProducto.Norma = norma;
                if (detalle.PlazoMaximo < 1)
                {
                    normaProducto.PlazoMaximo = (int)detalle.PlazoMaximo * 12;
                    normaProducto.TipoPlazo = "Mensual";
                }
                else
                {
                    normaProducto.PlazoMaximo = (int)detalle.PlazoMaximo;
                    normaProducto.TipoPlazo = "Anual";
                }
                normaProducto.PlazoMinimo = detalle.PlazoMinimo;
                normaProducto.Tasa = detalle.Tasa;
                normaProducto.Tipo = detalle.Tipo;
                normaProducto.MontoMaximo = detalle.MontoMaximo;
                normaProducto.MontoMinimo = detalle.MontoMinimo;
                normaProducto.Vencimientos.Add(new GrupoVencimientoNorma { Clave = detalle.Clave, Equivalencia = detalle.Equivalencia });
            }

            return normaProducto;
        }
        public virtual void GuardarAsociado(GuardarInformacionSolicitante guardarInformacionSolicitante)
        {
            _contexto.GuardarAsociado(guardarInformacionSolicitante);
        }

        public Aportes ObtenerAportes(string tipoDocumento, string numeroDocumento, short IdTipoRol)
        {
            return _contexto.ObtenerAportes(tipoDocumento, numeroDocumento, IdTipoRol).SingleOrDefault();
        }

        public Cartera ObtenerCartera(string idTipoIdentificacion, string numeroDocumento)
        {
            _contexto.Database.CommandTimeout = 100;
            return _contexto.ObtenerCartera(idTipoIdentificacion, numeroDocumento).SingleOrDefault();
        }
        public IEnumerable<Garantia> ObtenerInformacionGarantia(string NumSolicitud, string TipoDoc, string NumeroDoc)
        {
            return _contexto.ObtenerInformacionGarantia(NumSolicitud, TipoDoc, NumeroDoc).ToList();
        }
        public IEnumerable<ViewModelFormaPagoFYC> ObtenerFormasPagoFYC(string producto, string papel, short idTipoCredito)
        {
            var norma = _contexto.ObtenerInformacionNormas(producto, papel, idTipoCredito).FirstOrDefault() == null ? string.Empty : _contexto.ObtenerInformacionNormas(producto, papel, idTipoCredito).FirstOrDefault().Norma;
            IEnumerable<ViewModelFormaPagoFYC> formaPago = _contexto.ObtenerFormasPagoFYC(norma).ToList();
            return formaPago;
        }

        public IEnumerable<ViewModelFormaPagoFYC> ObtenerFormaPagoPorNorma(string norma)
        {
            IEnumerable<ViewModelFormaPagoFYC> formaPago = _contexto.ObtenerFormasPagoFYC(norma).ToList();
            return formaPago;
        }

        public VMSaldoAsegurabilidad ObtenerSaldoAsegurabilidad(string numeroIdentificacion)
        {
            return _contexto.ObtenerSaldoAsegurabilidad(numeroIdentificacion).FirstOrDefault();
        }


        public RespuestaValidacion ValidarCodeudor(string numeroDocumento)
        {
            return _contexto.ValidarCodeudor(numeroDocumento).FirstOrDefault();
        }

        public IEnumerable<TipoGarantiaPorProducto> ObtenerTipoGarantiaPorProducto(string Producto)
        {
            // return _contexto.ObtenerTipoGarantia(Producto).Where(x => x.IdTipoLineaCredito == idTipoLinea).ToList().OrderBy(x => x.Nombre);
            return _contexto.ObtenerTipoGarantiaPorProducto(Producto).ToList();
        }

        public Familiar GuardarFamiliar(Familiar informacionFamiliar)
        {
            return _contexto.GuardarFamiliar(informacionFamiliar).FirstOrDefault();
        }
        public IEnumerable<FormaPagoCredito> ObtenerFormaDePagoSolicitud(string idSolicitud)
        {
            return _contexto.ObtenerFormaDePagoSolicitud(idSolicitud).ToList();
        }
        public ObjectInt guardarSolicitudCreditoFyc(SolicitudCreditoFyc solicitudCreditoFyc)
        {
            return _contexto.guardarSolicitudCreditoFyc(solicitudCreditoFyc).FirstOrDefault(); ;
        }

        public void guardarPagareGarantia(VMConceptoGarantiaCredito vMConceptoGarantiaCredito)
        {
            _contexto.guardarPagareGarantia(vMConceptoGarantiaCredito);
        }

        public ObjectInt generarNumeroPagare(SolicitudCreditoPagare solicitudCreditoPagare)
        {

            return _contexto.generarNumeroPagare(solicitudCreditoPagare).FirstOrDefault();
        }

        public void guardarSolicitudCreditoPagare(SolicitudCreditoPagare solicitudCreditoPagare)
        {
            _contexto.guardarSolicitudCreditoPagare(solicitudCreditoPagare);
        }

        public IEnumerable<SolicitudesCredito> ObtenerSolicitudes(int numeroSolicitud, string tipoDocumento, int numeroDocumento, int tipoCredito)
        {
            return _contexto.ObtenerSolicitudes(numeroSolicitud, tipoDocumento, numeroDocumento, tipoCredito).ToList();
        }

        public IEnumerable<AsegurabilidadOpcion> ObtenerOpcionesAsegurabilidad(bool IdAsegurable, string idCredito, string idPapel)
        {
            return _contexto.ObtenerOpcionesAsegurabilidad(IdAsegurable, idCredito, idPapel).ToList();
        }

        public int ValidarITP(string cedula, int bandera)
        {
            return _contexto.ValidarITP(cedula, bandera).FirstOrDefault();
        }

        public int ValidarEmbargo(string cedula, int bandera)
        {
            return _contexto.ValidarEmbargo(cedula, bandera).FirstOrDefault();
        }

        public int ValidarExisExtraprima(string cedula, string tipoIdentificacion, int bandera)
        {
            return _contexto.ValidarExisExtraprima(cedula, tipoIdentificacion, bandera).FirstOrDefault();
        }

        public IEnumerable<ExtraPrima> TraeExtraprima(string cedula, string tipoIdentificacion, int bandera)
        {
            
            return  _contexto.TraeExtraprima(cedula, tipoIdentificacion, bandera).ToList();
            
        }

        public void actualizarEstado(string NumeroSolicitud, string Estado)
        {
            _contexto.actualizarEstado(NumeroSolicitud, Estado);
        }

        public IEnumerable<ArbolAmortizacion> ObtenerArbolAmortizacion(string idtipocredito, string idpapelcavipetrol)
        {
            return _contexto.ObtenerArbolAmortizacion(idtipocredito, idpapelcavipetrol).ToList();
        }

        public virtual void GuardarSolicitudInterproductos(List<InformacionProductosCreditos> datos)
        {
            foreach (var item in datos)
            {
                if (item.ValorACruzar != 0)
                {

                    _contexto.GuardarSolicitudInterproductos(Convert.ToInt32(item.ConsecutivoSolicitud), item.Producto, Convert.ToDecimal(item.ValorACruzar),item.Documento_tipo,item.Documento_Numero);
                }

            }

        }
    }
}
