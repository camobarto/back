﻿using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public interface IRepositorioAsegurabilidad
    {
        void InsertarDatosAsegurabilidad(List<AsegurabilidadModel> datosasegurabilidad);
        IEnumerable<VMPolizasVigentes> ObtenerPolizasVigentes(string tipo, string numero);
    }
}
