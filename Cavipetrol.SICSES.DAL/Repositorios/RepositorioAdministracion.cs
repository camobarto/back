﻿using Cavipetrol.SICSES.Infraestructura.Model.Administracion;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Administracion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioAdministracion : IRepositorioAdministracion
    {
        private readonly AdministracionContexto _contexto;
        public RepositorioAdministracion(AdministracionContexto context)
        {
            _contexto = context;
        }

        public IEnumerable<LineaCredito> ObtenerLineaCredito()
        {

            return _contexto.LineaCredito.ToList();
        }
        public string GuardarLineaCredito(int id ,string nombre, string Descripcion, int bandera)
        {
            LineaCredito credito = new LineaCredito() { Nombre = nombre, Descripcion = Descripcion };
            string respuesta = "";

            if (bandera == 1)
            {
                _contexto.Entry(credito).State = System.Data.Entity.EntityState.Added;
                respuesta = "Registro guardado correctamente";
            }
            else
            {
                LineaCredito creditoModificar = _contexto.LineaCredito.Where(x => x.IdTipoLineaCredito == id).FirstOrDefault();

               creditoModificar.Nombre = nombre;
                creditoModificar.Descripcion = Descripcion;


                    _contexto.Entry(creditoModificar).State = System.Data.Entity.EntityState.Modified;
                    respuesta = "Registro modificacado  correctamente";
                
            }
            _contexto.SaveChanges();
            return respuesta;
        }

        public string EliminarLineaCredito(int id)
        {
            LineaCredito credito = new LineaCredito() { IdTipoLineaCredito = id};
            string respuesta = "";

            _contexto.Entry(credito).State = System.Data.Entity.EntityState.Deleted;
            _contexto.SaveChanges();
            respuesta = "Registro eliminado  correctamente";
            return respuesta;
        }

        public IEnumerable<ViewTiposCreditos> ObtenerTiposCredito()
        {
            return _contexto.ObtenerTiposCredito().ToList();
        }
        public string AdministrarTiposCredito(int id, string nombre, int idLinea, int bandera)
        {
            TiposCredito Tiposcredito = new TiposCredito() { Nombre = nombre,IdTipoLineaCredito = idLinea };
            string respuesta = "";

            if (bandera == 1)
            {
                _contexto.Entry(Tiposcredito).State = System.Data.Entity.EntityState.Added;
                respuesta = "Registro guardado correctamente";
            }
            else
            {
                TiposCredito creditoModificar = _contexto.TiposCredito.Where(x => x.IdTipoCredito == id).FirstOrDefault();

                creditoModificar.Nombre = nombre;
                creditoModificar.IdTipoLineaCredito = idLinea;


                _contexto.Entry(creditoModificar).State = System.Data.Entity.EntityState.Modified;
                respuesta = "Registro modificacado  correctamente";

            }
            _contexto.SaveChanges();
            return respuesta;
        }
        public string EliminarTiposCredito(int id)
        {
            TiposCredito credito = new TiposCredito() { IdTipoCredito = Convert.ToInt16(id) };
            string respuesta = "";

            _contexto.Entry(credito).State = System.Data.Entity.EntityState.Deleted;
            _contexto.SaveChanges();
            respuesta = "Registro eliminado  correctamente";
            return respuesta;
        }
        public IEnumerable<ViewPapelCavipetrol> ObtenerPapelCavipetrol()
        {
            return _contexto.ObtenerPapelCavipetrol().ToList();
        }

        public string AdministrarPapelCavipetrol(int id, string nombre, string descripcion, int idcontrato, int bandera)
        {
            PapelCavipetrol Tiposcredito = new PapelCavipetrol() { Nombre = nombre, Descripcion =  descripcion,IdTipoContrato = Convert.ToInt16(idcontrato) };
            string respuesta = "";

            if (bandera == 1)
            {
                _contexto.Entry(Tiposcredito).State = System.Data.Entity.EntityState.Added;
                respuesta = "Registro guardado correctamente";
            }
            else
            {
                PapelCavipetrol creditoModificar = _contexto.PapelCavipetrol.Where(x => x.IdPapelCavipetrol == id).FirstOrDefault();

                creditoModificar.Nombre = nombre;
                creditoModificar.Descripcion = descripcion;
                creditoModificar.IdTipoContrato = Convert.ToInt16(idcontrato);


                _contexto.Entry(creditoModificar).State = System.Data.Entity.EntityState.Modified;
                respuesta = "Registro modificacado  correctamente";

            }
            _contexto.SaveChanges();
            return respuesta;
        }
        public string EliminarPapelCavipetrol(int id)
        {
            PapelCavipetrol credito = new PapelCavipetrol() { IdPapelCavipetrol = Convert.ToInt16(id) };
            string respuesta = "";

            _contexto.Entry(credito).State = System.Data.Entity.EntityState.Deleted;
            _contexto.SaveChanges();
            respuesta = "Registro eliminado  correctamente";
            return respuesta;
        }

        public IEnumerable<ViewTipoCreditoTipoRelacion> ObtenerTipoCreditoTipoRolRelacion()
        {
            return _contexto.ObtenerTipoCreditoTipoRolRelacion().ToList();
        }
        public string AdministrarTipoCreditoTipoRol(TipoCreditoTipoRol datos)
        {
            return _contexto.AdministrarTipoCreditoTipoRol(datos).FirstOrDefault();
        }

    }
}
