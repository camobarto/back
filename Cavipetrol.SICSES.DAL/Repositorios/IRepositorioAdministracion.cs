﻿using Cavipetrol.SICSES.Infraestructura.Model.Administracion;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Administracion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public interface IRepositorioAdministracion
    {
        IEnumerable<LineaCredito> ObtenerLineaCredito();
        string GuardarLineaCredito(int id,string nombre, string Descripcion, int bandera);
        string AdministrarTiposCredito(int id, string nombre, int idLinea, int bandera);
        string EliminarLineaCredito(int id);

        string EliminarTiposCredito(int id);

        string EliminarPapelCavipetrol(int id);

        IEnumerable<ViewTiposCreditos> ObtenerTiposCredito();

        IEnumerable<ViewPapelCavipetrol> ObtenerPapelCavipetrol();

        string AdministrarPapelCavipetrol(int id, string nombre, string Descripcion, int idcontrato, int bandera);

        IEnumerable<ViewTipoCreditoTipoRelacion> ObtenerTipoCreditoTipoRolRelacion();
        string AdministrarTipoCreditoTipoRol(TipoCreditoTipoRol datos);



    }
}
