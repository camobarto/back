﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Sap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioSap : IRepositorioSap
    {
        private readonly SapContexto _contexto;

        public RepositorioSap(SapContexto context)
        {
            _contexto = context;
        }

        public IEnumerable<AsiSoporte> ObtenerAsiSoporte()
        {
            return _contexto.ObtenerAsiSoporte().ToList();
        }

        public IEnumerable<Asientos> ConsultarAsientos(string AsiSoporte, string AsiConsecutivo, string Asioperacion, string AsiOpeConsecutivo, string Fecha, string Ciudad, string MensajeError)
        {
            return _contexto.ConsultarAsientos(AsiSoporte, AsiConsecutivo, Asioperacion, AsiOpeConsecutivo, Fecha, Ciudad, MensajeError).ToList();
        }

        public IEnumerable<string> ActualizarAsiento()
        {
             return _contexto.ActualizarAsiento().ToList();
        }
        public IEnumerable<Proveedores> ConsultaProveedores(string CardCode, string CardName)
        {
            return _contexto.ConsultaProveedores(CardCode, CardName);
        }

        public IEnumerable<string> ActualizarEstadoProveedores()
        {
            return _contexto.ActualizarEstadoProveedores();
        }

        public IEnumerable<MensajeErrorAsientos> ConsultarMensajeError()
        {
            return _contexto.ConsultarMensajeError();
        }


    }
}
