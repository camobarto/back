﻿using Cavipetrol.SICSES.Infraestructura.Historico;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;
using Cavipetrol.SICSES.Infraestructura.Model.fyc.Riesgos;
using Cavipetrol.SICSES.Infraestructura.Model.Seguridad;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;

using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public interface IRepositorioSICAV
    {
        Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago GuardarCapacidadPago(Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago);
        RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> GuardarSolicitudCredito(Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito solicitudCredito,
                                                                                                      Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago,
                                                                                                      CapacidadPagoHorasExtras horasExtras,
                                                                                                      List<SolicitudCreditoFormaPago> listaFormaPago,
                                                                                                      List<FormCapacidadPagoCodeudoresGrupos> listaFormCapacidadPagoCodeudoresGrupos,
                                                                                                       List<FormCapacidadPagoTerceroCodeudor> listaFormCapacidadPagoTerceroCodeudor);
        CapacidadPagoHorasExtras GuardarCapacidadPagoHorasExtras(CapacidadPagoHorasExtras horasExtras);
        IEnumerable<CapacidadPagoHorasExtras> ConsultarCapacidadPagoHorasExtras(int idCapacidadPago = 0);

        void GuardarHistorico(Historico historico);
        IEnumerable<PerfilMenuRelacion> ObtenerMenu(int idPerfil);
        IEnumerable<Menu> MenuCompleto();
        IEnumerable<PerfilMenuRelacion> ObtenerPerfilMenuRelacion(); 
        IEnumerable<AsegurabilidadOpcion> ObtenerAsegurabilidadOpciones(bool? noAsegurable);
        IEnumerable<ViewModelSolicitudCredito> ObtenerSolicitudCredito(int? idSolicitud, int? idTipoCredito,
                                                         string idTipoIdentificacion, string numeroIdentificacionAsociado,
                                                         short? idAseguabilidadOpcion, string idEstadoSolicitudCredito,
                                                         string usuarioCrea, string usuarioModifica, long? consecutivoSolicitud,
                                                         int? idPerfil);
        CapacidadPago ObtenerSolicitudCapacidadPago(int idSolicitud);
        SolicitudCredito CambiarEstadoSolicitud(int idSolicitud);
        SolicitudCredito ObtenerSolicitudCredito(int idSolicitud);
        void ActualizarEstadoSolicitudCredito(List<Parametros> causalesNegacion, SolicitudCredito solicitudCredito, string UsuRegistra, Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago);
        IEnumerable<SolicitudCreditoCausalesNegacion> ObtenerCausalesNegacion();
        RespuestaNegocio<string> GuardarReferenciasAsociado(List<ReferenciasAsociado> referencias);

        List<ReferenciasAsociado> ObtenerReferenciasAsociado(string tipo, string numero);
        Usuario ObtenerUsuario(string uniqueName);
        IEnumerable<ViewModelConsolidadoAtribuciones> ObtenerConsolidadoAtribuciones(string idTipoIdentificacion, string numeroIdentificacionAsociado, int? idPerfil, int? valorcredito);
        IEnumerable<SolicitudCreditoFormaPago> ObtenerSolicitudCreditoFormaPago(long? idSolicitudCreditoFormaPago);
        void GuardarSolicitudCreditoIntentoLog(SolicitudCreditoIntentosLog model);
        List<CapacidadPagoCodeudores> ObtenerCoincidenciaCodeudorCreditos();
        List<SolicitudCredito> ObtenerTodasSolicitudCredito(short? idEstado = null);
        List<CapacidadPagoCodeudores> ObtenerSolicitudCreditoDetalleCodeudores(int idSolicitud);
        List<Usuario> ObtenerUsuario();
        List<Perfil> ObtenerPerfil();
        Moras ObtenerMoras(string numeroDocumento);
        Usuario GuardarUsuario(Usuario usuario);
        Perfil GuardarPerfil(Perfil perfil);

        void GuardarUsuarioTemporal(string Usuario, string Contrasena);

        RespuestaValidacion ValidarInicioUsuario(string usuario);
        UsuarioLogin AdministrarInicioSesion(string usuario, string contrasena, string NuevaContrasena, int bandera);

        RespuestaNegocio<CapacidadPagoTerceroCodeudor> GuardarCapacidadPagoTerceroCodeudor(string TipoIdentificacion, string NumeroIdentificacion, string Nombre, string FechaNacimiento, string FormCapacidad);

        double ObtenerCupoMaximoCredito(string TipoIdentificacion, string NumeroIdentificacion);

        RespuestaValidacion ObtenerSolicitudCreditoAdjunto(int idSolicitud, int idTipoAdjunto);
        SolicitudCreditoAdjunto GuardarSolicitudCreditoAdjunto(SolicitudCreditoAdjunto solicitudCreditoAdjunto);

        List<HipotecaSolicitud> ObtenerHipotecaSolicitudCredito(int idSolicitud);
        bool GuardarSolicitudCreditoHipoteca(SolicitudCreditoHipoteca solicitudCreditoHipoteca);
        string GenerarHipotecaGuardarMinuta(SolicitudMinutaFYC solicitudMinutaFYC);
        bool ActualizarPerfilMenuRelacion(ListaMenuPerfil perfilMenuRelacion);
        Factura ObtenerNumeroFactura(string servidor);
        string ObtenerCodeudores(int Identificacion);
        string GuardarJubilado(DetalleAsociado asociado);
        bool ConfirmarMesada14(string Cedula);
        IEnumerable<SolicitudOperacion> SolicitudOperacion(string TipoIdentificacion, string NumeroIdentificacion, string NumeroOperacion);
        void GuardarDatosPersonaTransaccionEfectivo(int Operacion, string Tipoidentificacion, string Numeroidentificacion, string Primernombre,
                                                        string Segundonombre, string Primerapellido, string Segundoapellido, string Direccion, string Telefono,
                                                        string Consulta, string Tipopersona);

        void GuardarDatosOrigenFondos(int Operacion, int Opcion, string Consulta, string Detalle);
        void GuardarSolicitudCreditoHistorico(SolicitudCreditoHistorico solicitudCreditoHistorico);
        List<CartaCondicionesCreditoModel> ObtenerDatosCartaCondiciones(int idSolicitud);
		IEnumerable<PorcentajesTasas> ObtenerTasas(string nomPorcentaje);
		void CrearNuevaTasa(string nomPorcentaje, string porcentaje);
		IEnumerable<FomentoEmpresarial> ObtenerFomentoEmresarial();
		void MarcarCreditoFodes(string documentoTipo, string documentoNumero, int numeroUnico, string tipoDocumento, string numeroDocumento, string descripcion);
		IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMarcados();
		void DesmarcarCreditoFodes(List<FodesGenerico> listaDesmarcacion);
		IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMora();
		IEnumerable<InfoFodes> InformacionFodes();
	}
}