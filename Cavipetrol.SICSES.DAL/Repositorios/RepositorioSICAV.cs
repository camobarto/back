﻿using Cavipetrol.SICSES.Infraestructura.Historico;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.Seguridad;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.Creditos;
using System.Web.Script.Serialization;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using System.Data.Entity;
using Cavipetrol.SICSES.Infraestructura.Model.fyc.Riesgos;

namespace Cavipetrol.SICSES.DAL.Repositorios
{
    public class RepositorioSICAV : IRepositorioSICAV
    {
        private readonly SICAVContexto _contextoSICAV;



        public RepositorioSICAV(SICAVContexto context)
        {
            _contextoSICAV = context;
        }

        public Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago GuardarCapacidadPago(Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago)
        {
            if (capacidadPago.IdCapacidadPago > 0)
            {
                _contextoSICAV.Entry(capacidadPago).State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                _contextoSICAV.Entry(capacidadPago).State = System.Data.Entity.EntityState.Added;
            }
            _contextoSICAV.SaveChanges();
            return capacidadPago;
        }
        public RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> GuardarSolicitudCredito(Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito solicitudCredito,
                                                                                                      Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago,
                                                                                                      CapacidadPagoHorasExtras horasExtras,
                                                                                                      List<SolicitudCreditoFormaPago> listaFormaPago,
                                                                                                      List<FormCapacidadPagoCodeudoresGrupos> listaFormCapacidadPagoCodeudoresGrupos,
                                                                                                       List<FormCapacidadPagoTerceroCodeudor> listaFormCapacidadPagoTerceroCodeudor)
        {
            RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> rta = new RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito>();
            using (var transaccion = _contextoSICAV.Database.BeginTransaction())
            {
                try
                {
                    long consecutivoSolictud = 0;                
                    var maximo = _contextoSICAV.SolicitudCredito.Max(x => x.ConsecutivoSolicitud);
                    if (maximo == null) {
                        consecutivoSolictud = 1;
                    }
                    else {
                        consecutivoSolictud = (long)(maximo ?? 0) + 1;
                    }
                    solicitudCredito.ConsecutivoSolicitud = consecutivoSolictud;

                    if (solicitudCredito.IdSolicitud > 0)
                    {
                        _contextoSICAV.Entry(solicitudCredito).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        _contextoSICAV.Entry(solicitudCredito).State = System.Data.Entity.EntityState.Added;
                    }
                    _contextoSICAV.SaveChanges();


                    capacidadPago.IdSolicitud = solicitudCredito.IdSolicitud;
                    if (capacidadPago.IdCapacidadPago > 0)
                    {
                        _contextoSICAV.Entry(capacidadPago).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        _contextoSICAV.Entry(capacidadPago).State = System.Data.Entity.EntityState.Added;
                    }
                    _contextoSICAV.SaveChanges();

                    horasExtras.IdCapacidadPago = capacidadPago.IdCapacidadPago;
                    if (_contextoSICAV.CapacidadPagoHorasExtras
                                 .Where(x => x.IdCapacidadPago == horasExtras.IdCapacidadPago)
                                 .SingleOrDefault() != null)
                    {
                        _contextoSICAV.Entry(horasExtras).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        _contextoSICAV.Entry(horasExtras).State = System.Data.Entity.EntityState.Added;
                    }


                    foreach (SolicitudCreditoFormaPago formaPago in listaFormaPago)
                    {
                        if (formaPago.Valor > 0)
                        {
                            formaPago.IdSolicitud = solicitudCredito.IdSolicitud;
                            if (formaPago.IdSolicitudCreditoFormaPago > 0)
                            {
                                _contextoSICAV.Entry(formaPago).State = System.Data.Entity.EntityState.Modified;
                            }
                            else
                            {
                                _contextoSICAV.Entry(formaPago).State = System.Data.Entity.EntityState.Added;
                            }
                        }
                    }


                    // Capacidad de pago codeudores
                    JavaScriptSerializer serializador = new JavaScriptSerializer();
                    Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoCodeudores capacidadPagoCodeudores = new Infraestructura.Model.Sicav.CapacidadPagoCodeudores();
                    string AccionRealiza = "";
                    foreach (FormCapacidadPagoCodeudoresGrupos PagoCodeudores in listaFormCapacidadPagoCodeudoresGrupos)
                    {
                        capacidadPagoCodeudores.IdEstadoCapacidadPago = 1; // Abierto
                        capacidadPagoCodeudores.IdTipoIdentificacionCodeudor = PagoCodeudores.TipoIdentificacion;
                        capacidadPagoCodeudores.NumeroIdentificacionCodeudor = PagoCodeudores.Cedula;
                        capacidadPagoCodeudores.NombreCodeudor = PagoCodeudores.Nombre;
                        capacidadPagoCodeudores.JsonCapacidadPago = serializador.Serialize(PagoCodeudores.FormCapacidadPagoGrupos);
                        capacidadPagoCodeudores.AudFechaCreacion = DateTime.Now;
                        capacidadPagoCodeudores.AudFechaModificacion = DateTime.Now;
                        capacidadPagoCodeudores.AudUsuarioCreacion = solicitudCredito.AudUsuarioCreacion; // UserLog;
                        capacidadPagoCodeudores.AudUsuarioModificacion = solicitudCredito.AudUsuarioCreacion; // UserLog;

                        capacidadPagoCodeudores.IdSolicitud = solicitudCredito.IdSolicitud;
                        if (capacidadPagoCodeudores.IdCapacidadPagoCodeudores > 0 && AccionRealiza != "INSERCION")
                        {
                            _contextoSICAV.Entry(capacidadPagoCodeudores).State = System.Data.Entity.EntityState.Modified;
                        }
                        else
                        {
                            _contextoSICAV.Entry(capacidadPagoCodeudores).State = System.Data.Entity.EntityState.Added;
                            AccionRealiza = "INSERCION";
                        }
                        _contextoSICAV.SaveChanges();
                        _contextoSICAV.GuardarDatosCodeudorFYC(Convert.ToInt32(solicitudCredito.fscNumSolicitud), capacidadPagoCodeudores.IdTipoIdentificacionCodeudor, capacidadPagoCodeudores.NumeroIdentificacionCodeudor,capacidadPagoCodeudores.NombreCodeudor);
                    }

                    //Capacidad de Pago Tercero Codeudor
                    CapacidadPagoTerceroCodeudor modeloTercero = new CapacidadPagoTerceroCodeudor();
                    foreach (FormCapacidadPagoTerceroCodeudor PagoTercero in listaFormCapacidadPagoTerceroCodeudor)
                    {
                        //modeloTercero.IdCapacidadPagoTerceroCodeudor = 0;
                        modeloTercero.idSolicitud = solicitudCredito.IdSolicitud;
                        modeloTercero.TipoIdentificacion = PagoTercero.TipoIdentificacion;
                        modeloTercero.NumeroIdentificacion = PagoTercero.Cedula;
                        modeloTercero.Nombre = PagoTercero.Nombre;
                        modeloTercero.FechaNacimiento = PagoTercero.FechaNacimiento;
                        modeloTercero.JsonCapacidadPago = serializador.Serialize(PagoTercero.FormCapacidadPagoGrupos);
                        _contextoSICAV.Entry(modeloTercero).State = System.Data.Entity.EntityState.Added;
                    }

                    _contextoSICAV.SaveChanges();

                    transaccion.Commit();                    
                    rta.Estado = true;
                    rta.Respuesta = solicitudCredito;
                }
                catch (Exception ex)
                {
                    transaccion.Rollback();
                    rta.Estado = false;
                    rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                }                
            }
            return rta;
        }
        public CapacidadPagoHorasExtras GuardarCapacidadPagoHorasExtras(CapacidadPagoHorasExtras horasExtras)
        {
            if (_contextoSICAV.CapacidadPagoHorasExtras
                              .Where(x => x.IdCapacidadPago == horasExtras.IdCapacidadPago)
                              .SingleOrDefault() != null)
            {
                _contextoSICAV.Entry(horasExtras).State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                _contextoSICAV.Entry(horasExtras).State = System.Data.Entity.EntityState.Added;
            }
            _contextoSICAV.SaveChanges();
            return horasExtras;
        }
        public IEnumerable<CapacidadPagoHorasExtras> ConsultarCapacidadPagoHorasExtras(int idCapacidadPago = 0)
        {
            return _contextoSICAV.CapacidadPagoHorasExtras.Where(x => x.IdCapacidadPago == (idCapacidadPago == 0 ? x.IdCapacidadPago : idCapacidadPago)).ToList();
        }
        public void GuardarHistorico(Historico historico)
        {
            _contextoSICAV.Entry(historico).State = System.Data.Entity.EntityState.Added;
            _contextoSICAV.SaveChanges();
        }

        public IEnumerable<PerfilMenuRelacion> ObtenerMenu(int idPerfil)
        {
            return _contextoSICAV.PerfilMenuRelacion.Include("Menu").Where(x => x.IdPerfil == idPerfil).ToList();
        }
        public IEnumerable<Menu> MenuCompleto()
        {
            return _contextoSICAV.Menu.ToList();
        }
        public IEnumerable<PerfilMenuRelacion> ObtenerPerfilMenuRelacion()
        {
            return _contextoSICAV.PerfilMenuRelacion.ToList();
        }

        public IEnumerable<AsegurabilidadOpcion> ObtenerAsegurabilidadOpciones(bool? noAsegurable)
        {
            var lista = _contextoSICAV.AsegurabilidadOpcion.ToList();
            var listaReturn = lista.Where(x => x.Asegurable == 2).ToList();
            if (noAsegurable == true)
            {
                foreach (var item in lista.Where(x => x.Asegurable == 3))
                {
                    listaReturn.Add(item);
                }
                return listaReturn.OrderBy(x => x.Nombre);
            }
            else
            {
                foreach (var item in lista.Where(x => x.Asegurable == 1))
                {
                    listaReturn.Add(item);
                }
                return listaReturn.OrderBy(x => x.Nombre);
            }
        }
        public IEnumerable<ViewModelSolicitudCredito> ObtenerSolicitudCredito(int? idSolicitud,
                                                                int? idTipoCredito,
                                                                string idTipoIdentificacion,
                                                                string numeroIdentificacionAsociado,
                                                                short? idAseguabilidadOpcion,
                                                                string idEstadoSolicitudCredito,
                                                                string usuarioCrea,
                                                                string usuarioModifica,
                                                                long? consecutivoSolicitud,
                                                                int? idPerfil)
        {
            var rta = _contextoSICAV.ObtenerSolicitudCredito(idSolicitud,
                                                          idTipoCredito,
                                                          idTipoIdentificacion,
                                                          numeroIdentificacionAsociado,
                                                          idAseguabilidadOpcion,
                                                          idEstadoSolicitudCredito,
                                                          usuarioCrea,
                                                          usuarioModifica,
                                                          consecutivoSolicitud: consecutivoSolicitud,
                                                          idPerfil: idPerfil).ToList();

            return rta;
        }

        public CapacidadPago ObtenerSolicitudCapacidadPago(int idSolicitud)
        {
            return _contextoSICAV.CapacidadPago.Where(x => x.IdSolicitud == idSolicitud).FirstOrDefault();
        }
        public SolicitudCredito ObtenerSolicitudCredito(int idSolicitud)
        {
            return _contextoSICAV.SolicitudCredito.Where(x => x.IdSolicitud == idSolicitud).SingleOrDefault();
        }
        public SolicitudCredito CambiarEstadoSolicitud(int idSolicitud)
        {
            var solicitud = _contextoSICAV.SolicitudCredito.Where(x => x.IdSolicitud == idSolicitud).SingleOrDefault();

            if (solicitud.IdEstadoSolicitudCredito != 5)
            {
                solicitud.IdEstadoSolicitudCredito = 5;
                _contextoSICAV.Entry(solicitud).State = System.Data.Entity.EntityState.Modified;
            }
            _contextoSICAV.SaveChanges();

            return solicitud;
        }
        public void ActualizarEstadoSolicitudCredito(List<Parametros> causalesNegacion, SolicitudCredito solicitudCredito, string UsuRegistra, Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago)
        {
            using (var transaccion = _contextoSICAV.Database.BeginTransaction())
            {
                try
                {
                    short idCausalNegacion;
                    foreach (Parametros parametro in causalesNegacion)
                    {
                        idCausalNegacion = 0;
                        short.TryParse(parametro.Valor, out idCausalNegacion);
                        SolicitudCreditoNegado solicitudCreditoNegado = new SolicitudCreditoNegado();
                        solicitudCreditoNegado.IdSolicitud = solicitudCredito.IdSolicitud;
                        solicitudCreditoNegado.IdCausalNegacion = idCausalNegacion;
                        _contextoSICAV.Entry(solicitudCreditoNegado).State = System.Data.Entity.EntityState.Added;
                    }
                    _contextoSICAV.Entry(solicitudCredito).State = System.Data.Entity.EntityState.Modified;

                    if (solicitudCredito.IdEstadoSolicitudCredito == 7)
                    {
                        ConsolidadoAprobacion consolidadoAprobacion = new ConsolidadoAprobacion();
                        consolidadoAprobacion.IdSolicitud = solicitudCredito.IdSolicitud;
                        consolidadoAprobacion.UsuarioAprobo = UsuRegistra;

                        _contextoSICAV.Entry(consolidadoAprobacion).State = System.Data.Entity.EntityState.Added;
                    }
                    _contextoSICAV.SaveChanges();

                    if (capacidadPago != null)
                    {
                        if (capacidadPago.IdCapacidadPago > 0)
                        {
                            _contextoSICAV.Entry(capacidadPago).State = System.Data.Entity.EntityState.Modified;
                        }
                        else
                        {
                            _contextoSICAV.Entry(capacidadPago).State = System.Data.Entity.EntityState.Added;
                        }
                        _contextoSICAV.SaveChanges();
                    }
                    transaccion.Commit();
                }
                catch (Exception)
                {
                    transaccion.Rollback();
                }
            }

        }

        public IEnumerable<SolicitudCreditoCausalesNegacion> ObtenerCausalesNegacion()
        {
            return _contextoSICAV.SolicitudCreditoCausalesNegacion.ToList();
        }

        public RespuestaNegocio<string> GuardarReferenciasAsociado(List<ReferenciasAsociado> referencias)
        {
            RespuestaNegocio<string> rta = new RespuestaNegocio<string>();
            using (var transaccion = _contextoSICAV.Database.BeginTransaction())
            {
                try
                {
                    foreach (ReferenciasAsociado referencia in referencias)
                    {
                        if (referencia.Nombre != null && referencia.NumeroIdentificacion != null &&
                            referencia.IdTipoIdentificacion != null && referencia.IdTipoReferencia > 0 &&
                            referencia.Numero != null && referencia.Relacion != null)
                        {
                            if (referencia.IdReferenciaAsociado == 0)
                            {
                                _contextoSICAV.Entry(referencia).State = System.Data.Entity.EntityState.Added;
                            }
                            else
                            {
                                _contextoSICAV.Entry(referencia).State = System.Data.Entity.EntityState.Modified;
                            }
                        }
                    }
                    _contextoSICAV.SaveChanges();
                    transaccion.Commit();
                    rta.Estado = true;
                }
                catch (Exception ex)
                {
                    transaccion.Rollback();
                    rta.Estado = false;
                    rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                }
                return rta;
            }
        }

        public List<ReferenciasAsociado> ObtenerReferenciasAsociado(string tipo, string numero)
        {
            return _contextoSICAV.ReferenciasAsociado.Where(x => x.IdTipoIdentificacion == tipo && x.NumeroIdentificacion == numero && x.Activo == true).ToList();
        }

        public Usuario ObtenerUsuario(string uniqueName)
        {          
            var usuario = _contextoSICAV.Usuario.Include("Perfil").FirstOrDefault(u => u.UniqueName == uniqueName);

            if (usuario == null)
            {
                usuario = new Usuario();
                Perfil perfil = new Perfil();
                perfil.IdPerfil = 0;
                perfil.Nombre = "Visitante";

                usuario.IdUsuario = 0;
                usuario.UniqueName = uniqueName;
                usuario.IdPerfil = 0;
                usuario.IdOficina = 0;
                usuario.Perfil = perfil;
            }
            
            return usuario;
        }

        public List<Perfil> ObtenerPerfilesUsuario(int idUsuario)
        {
            //Lista de perfiles a devolver
            List<Perfil> listaPerfiles = new List<Perfil>();

            //Se consultan los perfiles que tenga el usuario, filtrando por su id
            List<PerfilUsuario> perfilesUsuario = _contextoSICAV.PerfilUsuario
                .Where(pu => pu.IdUsuario == idUsuario)
                .ToList();

            //Por cada perfil que tenga el usuario se crea un objeto perfil
            foreach (PerfilUsuario perfil in perfilesUsuario) {
                listaPerfiles.Add(
                    _contextoSICAV.Perfil
                    .Where(p => p.IdPerfil == perfil.Idperfil)
                    .First()
                );
            }
            return listaPerfiles;
        }

        public IEnumerable<ViewModelConsolidadoAtribuciones> ObtenerConsolidadoAtribuciones(string idTipoIdentificacion, string numeroIdentificacionAsociado, int? idPerfil, int? valorcredito)
        {

            return _contextoSICAV.ObtenerConsolidadoAtribuciones(idTipoIdentificacion, numeroIdentificacionAsociado, idPerfil, valorcredito);
        }

        public IEnumerable<SolicitudCreditoFormaPago> ObtenerSolicitudCreditoFormaPago(long? idSolicitudCreditoFormaPago)
        {
            return _contextoSICAV.SolicitudCreditoFormaPago.Where(x => x.IdSolicitudCreditoFormaPago == (idSolicitudCreditoFormaPago == null ? x.IdSolicitudCreditoFormaPago : idSolicitudCreditoFormaPago));
        }

        public void GuardarSolicitudCreditoIntentoLog(SolicitudCreditoIntentosLog model)
        {
            if (model.IdSolicitudCreditoIntentoLog > 0)
            {
                _contextoSICAV.Entry(model).State = System.Data.Entity.EntityState.Modified;
                _contextoSICAV.Entry(model).Property(x => x.AudUsuCrea).IsModified = false;
                _contextoSICAV.Entry(model).Property(x => x.AudFechaCrea).IsModified = false;
            }
            else
            {
                _contextoSICAV.Entry(model).State = System.Data.Entity.EntityState.Added;
            }
            _contextoSICAV.SaveChanges();
        }
        public List<CapacidadPagoCodeudores> ObtenerCoincidenciaCodeudorCreditos()
        {
            return _contextoSICAV.CapacidadPagoCodeudores.ToList();
        }
        public List<SolicitudCredito> ObtenerTodasSolicitudCredito(short? idEstado = null)
        {
            return _contextoSICAV.SolicitudCredito.Where(x => x.IdEstadoSolicitudCredito == (idEstado == null ? x.IdEstadoSolicitudCredito : idEstado)).ToList();
        }
        public List<CapacidadPagoCodeudores> ObtenerSolicitudCreditoDetalleCodeudores(int idSolicitud)
        {
            return _contextoSICAV.CapacidadPagoCodeudores.Where(x => x.IdSolicitud == idSolicitud).ToList();
        }

        public virtual void GuardarUsuarioTemporal(string Usuario, string Contrasena)
        {
            _contextoSICAV.GuardarUsuarioTemporal(Usuario, Contrasena);
        }

        public RespuestaValidacion ValidarInicioUsuario(string usuario)
        {
            return _contextoSICAV.ValidarInicioUsuario(usuario).FirstOrDefault();
        }

        public UsuarioLogin AdministrarInicioSesion(string usuario, string contrasena, string NuevaContrasena, int bandera)
        {
            return _contextoSICAV.AdministrarInicioSesion(usuario, contrasena, NuevaContrasena, bandera).FirstOrDefault();
        }
        public RespuestaNegocio<CapacidadPagoTerceroCodeudor> GuardarCapacidadPagoTerceroCodeudor(string TipoIdentificacion, string NumeroIdentificacion, string Nombre, string FechaNacimiento, string FormCapacidad)
        {
            CapacidadPagoTerceroCodeudor model = new CapacidadPagoTerceroCodeudor();
            model.TipoIdentificacion = TipoIdentificacion;
            model.NumeroIdentificacion = NumeroIdentificacion;
            model.Nombre = Nombre;
            model.FechaNacimiento = DateTime.Now;
            model.JsonCapacidadPago = FormCapacidad;



            _contextoSICAV.Entry(model).State = System.Data.Entity.EntityState.Added;
            _contextoSICAV.SaveChanges();

            RespuestaNegocio<CapacidadPagoTerceroCodeudor> Respuesta = new RespuestaNegocio<CapacidadPagoTerceroCodeudor>();
            return Respuesta;
        }

        public double ObtenerCupoMaximoCredito(string TipoIdentificacion, string NumeroIdentificacion)
        {
            return _contextoSICAV.ObtenerCupoMaximoCredito(TipoIdentificacion, NumeroIdentificacion).FirstOrDefault();
        }
        public List<Usuario> ObtenerUsuario()
        {
            return _contextoSICAV.Usuario.ToList();
        }
        public List<Perfil> ObtenerPerfil()
        {
            return _contextoSICAV.Perfil.ToList();
        }
        public Moras ObtenerMoras(string numeroDocumento)
        {
            return _contextoSICAV.Moras.Where(x => x.Identificacion == numeroDocumento).FirstOrDefault();
        }
        public Usuario GuardarUsuario(Usuario usuario)
        {

            if (usuario.IdUsuario == 0)
            {
                usuario.Estado = true;
                _contextoSICAV.Entry(usuario).State = System.Data.Entity.EntityState.Added;

            }
            else
            {

                _contextoSICAV.Entry(usuario).State = System.Data.Entity.EntityState.Modified;
            }

            _contextoSICAV.SaveChanges();

            return usuario;
        }
        public Perfil GuardarPerfil(Perfil perfil)
        {

            if (perfil.IdPerfil == 0)
            {
                perfil.Estado = true;
                _contextoSICAV.Entry(perfil).State = System.Data.Entity.EntityState.Added;

            }
            else
            {

                _contextoSICAV.Entry(perfil).State = System.Data.Entity.EntityState.Modified;
            }

            _contextoSICAV.SaveChanges();

            return perfil;
        }


        public RespuestaValidacion ObtenerSolicitudCreditoAdjunto(int idSolicitud, int idTipoAdjunto) => _contextoSICAV.ObtenerSolicitudCreditoAdjunto(idSolicitud, idTipoAdjunto).FirstOrDefault();

        public Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCreditoAdjunto GuardarSolicitudCreditoAdjunto(Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCreditoAdjunto solicitudCreditoAdjunto)
        {
            _contextoSICAV.Entry(solicitudCreditoAdjunto).State = System.Data.Entity.EntityState.Added;
            if (_contextoSICAV.SaveChanges() > 0)
            {
                return solicitudCreditoAdjunto;
            }
            else
            {
                return null;
            }
        }

        public List<HipotecaSolicitud> ObtenerHipotecaSolicitudCredito(int idSolicitud)
        {
            return _contextoSICAV.ObtenerHipotecaSolicitudCredito(idSolicitud).ToList();
        }

        public bool GuardarSolicitudCreditoHipoteca(SolicitudCreditoHipoteca solicitudCreditoHipoteca)
        {
            var solicitud = _contextoSICAV.SolicitudCreditoHipoteca.Where(x => x.IdHipoteca == solicitudCreditoHipoteca.IdHipoteca && x.IdSolicitud == solicitudCreditoHipoteca.IdSolicitud && x.PagareGenerado == false).FirstOrDefault();
            if (solicitud != null)
            {
                solicitud.PagareGenerado = true;
                solicitud.Monto = solicitudCreditoHipoteca.Monto;
                solicitud.FechaModificacion = solicitudCreditoHipoteca.FechaModificacion;
                solicitud.UsuarioModificacion = solicitudCreditoHipoteca.UsuarioModificacion;
                _contextoSICAV.Entry(solicitud).State = System.Data.Entity.EntityState.Modified;
            }
            else
            {
                _contextoSICAV.Entry(solicitudCreditoHipoteca).State = System.Data.Entity.EntityState.Added;
            }

            return _contextoSICAV.SaveChanges() > 0;
        }
        public string GenerarHipotecaGuardarMinuta(SolicitudMinutaFYC solicitudMinutaFYC)
        {
            return _contextoSICAV.GenerarHipotecaGuardarMinuta(solicitudMinutaFYC).FirstOrDefault().Respuesta;
        }
        public bool ActualizarPerfilMenuRelacion(ListaMenuPerfil perfilMenuRelacion)
        {
            List<PerfilMenuRelacion> listaPerfil = ObtenerPerfilMenuRelacion().ToList();
            _contextoSICAV.PerfilMenuRelacion.RemoveRange(listaPerfil.Where(x => x.IdPerfil == perfilMenuRelacion.IdPerfil));
            _contextoSICAV.SaveChanges();
            perfilMenuRelacion.PerfilMenu.ForEach(m => _contextoSICAV.PerfilMenuRelacion.Add(m));
            return _contextoSICAV.SaveChanges() > 0;
        }

        public Factura ObtenerNumeroFactura(string servidor)
        {
            return _contextoSICAV.ObtenerNumeroFactura(servidor).SingleOrDefault();
        }
        public string ObtenerCodeudores(int Identificacion)
        {
            return _contextoSICAV.ObtenerCodeudores(Identificacion).SingleOrDefault();
        }
        public string GuardarJubilado(DetalleAsociado asociado)
        {
            var mensaje = "";
            var variableAsociado = _contextoSICAV.DetalleAsociado.AsNoTracking().Where(x => x.NumeroIdentificacion == asociado.NumeroIdentificacion).SingleOrDefault();
             
            if (variableAsociado == null)
            {
                _contextoSICAV.Entry(asociado).State = EntityState.Added;
                mensaje = "Creado correactamente";
                
            }
            else
            {
                asociado.IdDetalle = variableAsociado.IdDetalle;
                _contextoSICAV.Entry(asociado).State = EntityState.Modified;
                mensaje = "Se modifico el asociado correctamente";
            }
            _contextoSICAV.SaveChanges();

            return mensaje;
        }
        public bool ConfirmarMesada14(string Cedula)
        {           
            var consulta = _contextoSICAV.DetalleAsociado.Where(x => x.NumeroIdentificacion == Cedula).FirstOrDefault();
            if (consulta == null)
            {
                return false;
            }
            else {
                return (bool)consulta.Mesada14;
            }            
        }

        public IEnumerable<SolicitudOperacion> SolicitudOperacion(string TipoIdentificacion, string NumeroIdentificacion, string NumeroOperacion)
        {            
            return _contextoSICAV.SolicitudOperacion(TipoIdentificacion, NumeroIdentificacion, NumeroOperacion).ToList(); 
        }

        public void GuardarDatosPersonaTransaccionEfectivo(int Operacion, string Tipoidentificacion, string Numeroidentificacion, string Primernombre, string Segundonombre, string Primerapellido,
                        string Segundoapellido, string Direccion, string Telefono, string Consulta, string Tipopersona)
        {
            _contextoSICAV.GuardarDatosPersonaTransaccionEfectivo( Operacion, Tipoidentificacion, Numeroidentificacion, Primernombre, Segundonombre, Primerapellido,
                        Segundoapellido, Direccion, Telefono, Consulta, Tipopersona);
        }

        public void GuardarDatosOrigenFondos(int Operacion, int Opcion, string Consulta, string Detalle)
        {
            _contextoSICAV.GuardarDatosOrigenFondos(Operacion, Opcion, Consulta, Detalle);
        }

        public void GuardarSolicitudCreditoHistorico(SolicitudCreditoHistorico solicitudCreditoHistorico)
        {
            _contextoSICAV.Entry(solicitudCreditoHistorico).State = EntityState.Added;
            _contextoSICAV.SaveChanges();
        }

        public List<CartaCondicionesCreditoModel>  ObtenerDatosCartaCondiciones(int idSolicitud)
        {   
            return _contextoSICAV.ObtenerDatosCartaCondiciones(idSolicitud).ToList();
        }

		public IEnumerable<PorcentajesTasas> ObtenerTasas(string nomPorcentaje)
		{
			return _contextoSICAV.ObtenerTasas(nomPorcentaje);
		}

		public void CrearNuevaTasa(string nomPorcentaje, string porcentaje)
		{
			_contextoSICAV.CrearNuevaTasa(nomPorcentaje, porcentaje);
		}

		public IEnumerable<FomentoEmpresarial> ObtenerFomentoEmresarial()
		{
			return _contextoSICAV.ObtenerFomentoEmresarial().ToList();
		}

		public void MarcarCreditoFodes(string documentoTipo, string documentoNumero, int numeroUnico, string tipoDocumento, string numeroDocumento, string descripcion)
		{
			_contextoSICAV.MarcarCreditoFodes(documentoTipo, documentoNumero, numeroUnico, tipoDocumento, numeroDocumento, descripcion);
		}

		public IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMarcados()
		{
			return _contextoSICAV.ObtenerCreditosFomentoEmresarialMarcados().ToList();
		}

		public void DesmarcarCreditoFodes(List<FodesGenerico> listaDesmarcacion)
		{
			foreach (FodesGenerico credito in listaDesmarcacion)
			{
				_contextoSICAV.DesmarcarCreditoFodes(credito.AcuerdoTipo, credito.NumeroDocumento, credito.NumeroUnico, credito.TipoIdentificacion, credito.Identificacion, credito.Descripcion);
			}
		}

		public IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMora()
		{
			return _contextoSICAV.ObtenerCreditosFomentoEmresarialMora().ToList();
		}

		public IEnumerable<InfoFodes> InformacionFodes()
		{
			return _contextoSICAV.InformacionFodes().ToList();
		}
	}
}
