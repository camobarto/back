﻿using Cavipetrol.SICSES.Infraestructura.Model.Administracion;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Administracion
{
    public class TiposCreditoMapa : EntityTypeConfiguration<TiposCredito>
    {
        public TiposCreditoMapa()
        {
            HasKey(p => p.IdTipoCredito);

            ToTable("TIPOS_CREDITO");
            Property(p => p.IdTipoCredito).HasColumnName("IdTipoCredito");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.IdTipoLineaCredito).HasColumnName("IdTipoLineaCredito");
        }
    }


}