﻿using Cavipetrol.SICSES.Infraestructura.Model.Administracion;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Administracion
{
    public class PapelCavipetrolMapa : EntityTypeConfiguration<PapelCavipetrol>
    {
        public PapelCavipetrolMapa()
        {
            HasKey(p => p.IdPapelCavipetrol);

            ToTable("PAPEL_CAVIPETROL");
            Property(p => p.IdPapelCavipetrol).HasColumnName("IdPapelCavipetrol");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
            Property(p => p.IdTipoContrato).HasColumnName("IdTipoContrato");
        }
    }
}
