﻿using Cavipetrol.SICSES.Infraestructura.Model.Administracion;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Administracion
{
    public class TiposLineaCreditoMapa : EntityTypeConfiguration<LineaCredito>
    {
        public TiposLineaCreditoMapa()
        {
            HasKey(p => p.IdTipoLineaCredito);

            ToTable("TIPOS_LINEA_CREDITO");
            Property(p => p.IdTipoLineaCredito).HasColumnName("IdTipoLineaCredito");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
        }
    }


}