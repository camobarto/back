﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class CapacidadPagoCodeudoresMapa : EntityTypeConfiguration<CapacidadPagoCodeudores>
    {
        public CapacidadPagoCodeudoresMapa()
        {
            HasKey(p => p.IdCapacidadPagoCodeudores);

            ToTable("CAPACIDAD_PAGO_CODEUDORES");
            Property(p => p.IdCapacidadPagoCodeudores).HasColumnName("IdCapacidadPagoCodeudores");
            Property(p => p.IdEstadoCapacidadPago).HasColumnName("IdEstadoCapacidadPago");
            Property(p => p.IdTipoIdentificacionCodeudor).HasColumnName("IdTipoIdentificacionCodeudor");
            Property(p => p.NumeroIdentificacionCodeudor).HasColumnName("NumeroIdentificacionCodeudor");
            Property(p => p.NombreCodeudor).HasColumnName("NombreCodeudor");
            Property(p => p.IdSolicitud).HasColumnName("IdSolicitud");
            Property(p => p.JsonCapacidadPago).HasColumnName("JsonCapacidadPago");
            Property(p => p.AudFechaCreacion).HasColumnName("AudFechaCreacion");
            Property(p => p.AudFechaModificacion).HasColumnName("AudFechaModificacion");
            Property(p => p.AudUsuarioCreacion).HasColumnName("AudUsuarioCreacion");
            Property(p => p.AudUsuarioModificacion).HasColumnName("AudUsuarioModificacion");
        }
    }
}
