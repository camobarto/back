﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class SolicitudCreditoCausalesNegacionMapa : EntityTypeConfiguration<SolicitudCreditoCausalesNegacion>
    {
        public SolicitudCreditoCausalesNegacionMapa()
        {
            HasKey(p => p.IdCausalNegacion);

            ToTable("SOLICITUD_CREDITO_CAUSALES_NEGACION");

            Property(p => p.IdCausalNegacion).HasColumnName("IdCausalNegacion");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
        }
    }
}
