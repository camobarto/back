﻿using Cavipetrol.SICSES.Infraestructura.Historico;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class HistoricoMapa : EntityTypeConfiguration<Historico>
    {
        public HistoricoMapa()
        {
            HasKey(p => p.IdHistorico);

            ToTable("HISTORICO");
            Property(p => p.IdHistorico).HasColumnName("IdHistorico");
            Property(p => p.IdMenu).HasColumnName("IdMenu");
            Property(p => p.Datos).HasColumnName("Datos");
            Property(p => p.AudFechaCreacion).HasColumnName("AudFechaCreacion");
            Property(p => p.AudFechaModificacion).HasColumnName("AudFechaModificacion");            
            Property(p => p.AudUsuarioCreacion).HasColumnName("AudUsuarioCreacion");
            Property(p => p.AudUsuarioModificacion).HasColumnName("AudUsuarioModificacion");
        }
    }
}
