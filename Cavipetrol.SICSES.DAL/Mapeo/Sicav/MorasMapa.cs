﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System.Data.Entity.ModelConfiguration;


namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class MorasMapa : EntityTypeConfiguration<Moras>
    {
        public MorasMapa()
        {
            HasKey(p => p.Id);

            ToTable("ClienteMora_90D");

            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Identificacion).HasColumnName("Identificacion");
            Property(p => p.Fecha_ultimo_aporte).HasColumnName("Fecha_ultimo_aporte");
            Property(p => p.Dias_mora).HasColumnName("Dias_mora");
            Property(p => p.Sancion).HasColumnName("Sancion");
        }
    }
}
