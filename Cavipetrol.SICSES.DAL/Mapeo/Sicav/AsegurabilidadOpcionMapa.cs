﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class AsegurabilidadOpcionMapa :  EntityTypeConfiguration<AsegurabilidadOpcion>
    {
        public AsegurabilidadOpcionMapa() {
            HasKey(p => p.IdAsegurabilidadOpcion);
            ToTable("ASEGURABILIDAD_OPCIONES");
            Property(p => p.IdAsegurabilidadOpcion).HasColumnName("IdAsegurabilidadOpcion");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
            Property(p => p.Asegurable).HasColumnName("Asegurable");
            
        }
    }
}
