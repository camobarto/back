﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class PerfilMapa : EntityTypeConfiguration<Perfil>
    {
        public PerfilMapa()
        {
            HasKey(p => p.IdPerfil);

            ToTable("PERFIL");
            Property(p => p.IdPerfil).HasColumnName("IdPerfil");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
            Property(p => p.Estado).HasColumnName("Estado");
        }
    }
}
