﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class SolicitudCreditoFormaPagoMapa : EntityTypeConfiguration<SolicitudCreditoFormaPago>
    {
        public SolicitudCreditoFormaPagoMapa()
        {
            HasKey(p => p.IdSolicitudCreditoFormaPago);
            ToTable("SOLICITUD_CREDITO_FORMA_PAGO");
            Property(p => p.IdSolicitudCreditoFormaPago).HasColumnName("IdSolicitudCreditoFormaPago");
            Property(p => p.IdSolicitud).HasColumnName("IdSolicitud");
            Property(p => p.IdFormaPago).HasColumnName("IdFormaPago");
            Property(p => p.Plazo).HasColumnName("Plazo");
            Property(p => p.Valor).HasColumnName("Valor");
            Property(p => p.Activo).HasColumnName("Activo");
        }
    }
}
