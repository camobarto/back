﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class DetalleAsociadoMapa : EntityTypeConfiguration<DetalleAsociado>
    {
        public DetalleAsociadoMapa()
        {
            HasKey(p => p.IdDetalle);
            ToTable("DETALLE_ASOCIADO");
            Property(p => p.IdDetalle).HasColumnName("IdDetalle");
            Property(p => p.TipoIdentificacion).HasColumnName("TipoIdentificacion");
            Property(p => p.NumeroIdentificacion).HasColumnName("NumeroIdentificacion");
            Property(p => p.NombreAsociado).HasColumnName("NombreAsociado");
            Property(p => p.Mesada14).HasColumnName("Mesada14");
        }
    }
}
