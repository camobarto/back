﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class CapacidadPagoHorasExtrasMapa : EntityTypeConfiguration<CapacidadPagoHorasExtras>
    {
        public CapacidadPagoHorasExtrasMapa() {
            HasKey(p => p.IdCapacidadPagoHorasExtras);
            ToTable("CAPACIDAD_PAGO_HORAS_EXTRAS");
            Property(p => p.IdCapacidadPagoHorasExtras).HasColumnName("IdCapacidadPagoHorasExtras");
            Property(p => p.IdCapacidadPago).HasColumnName("IdCapacidadPago");
            Property(p => p.JsonHorasExtras).HasColumnName("JsonHorasExtras");
        }
    }
}
