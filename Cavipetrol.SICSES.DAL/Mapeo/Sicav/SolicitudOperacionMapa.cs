﻿using Cavipetrol.SICSES.Infraestructura.Model.fyc.Riesgos;
using System.Data.Entity.ModelConfiguration;


namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class SolicitudOperacionMapa : EntityTypeConfiguration<SolicitudOperacion>
    {
        public SolicitudOperacionMapa()
        {
            HasKey(p => p.Consecutivo);

            ToTable("OPERACION");

            Property(p => p.Ciudad).HasColumnName("Ciudad");
            Property(p => p.Oficina).HasColumnName("Oficina");
            Property(p => p.Fecha).HasColumnName("Fecha");
            Property(p => p.Soporte).HasColumnName("Soporte");
            Property(p => p.EstadoProducto).HasColumnName("EstadoProducto");
            Property(p => p.TipoDocumento).HasColumnName("TipoDocumento");
            Property(p => p.NumeroDocumento).HasColumnName("NumeroDocumento");
            Property(p => p.NumeroTermino).HasColumnName("NumeroTermino");
            Property(p => p.TipoOperacion).HasColumnName("TipoOperacion");
            Property(p => p.Consecutivo).HasColumnName("Consecutivo");
            Property(p => p.Valor).HasColumnName("Valor");
            Property(p => p.Usuario).HasColumnName("Usuario");
            Property(p => p.PrimerNombre).HasColumnName("PrimerNombre");
            Property(p => p.SegundoNombre).HasColumnName("SegundoNombre");
            Property(p => p.PrimerApellido).HasColumnName("PrimerApellido");
            Property(p => p.SegundoApellido).HasColumnName("SegundoApellido");
            Property(p => p.Direccion).HasColumnName("Direccion");
            Property(p => p.Telefono).HasColumnName("Telefono");
            Property(p => p.TipoIdentificacion).HasColumnName("TipoIdentificacion");
            Property(p => p.NumeroIdentificacion).HasColumnName("NumeroIdentificacion");
        }
    }
}
