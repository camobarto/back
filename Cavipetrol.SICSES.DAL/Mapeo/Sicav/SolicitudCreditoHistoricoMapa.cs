﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class SolicitudCreditoHistoricoMapa : EntityTypeConfiguration<SolicitudCreditoHistorico>
    {
        public SolicitudCreditoHistoricoMapa()
        {
            HasKey(p => p.IdSolicitudHistorico);
            ToTable("SOLICITUD_CREDITO_HISTORICO");
            Property(p => p.IdSolicitudHistorico).HasColumnName("IdSolicitudHistorico");
            Property(p => p.IdSolicitud).HasColumnName("IdSolicitud");
            Property(p => p.IdTipoCreditoTipoRolRelacion).HasColumnName("IdTipoCreditoTipoRolRelacion");
            Property(p => p.IdTipoIdentificacionAsociado).HasColumnName("IdTipoIdentificacionAsociado");
            Property(p => p.NumeroIdentificacionAsociado).HasColumnName("NumeroIdentificacionAsociado");
            Property(p => p.AudFechaModificacion).HasColumnName("AudFechaModificacion");
            Property(p => p.AudFechaCreacion).HasColumnName("AudFechaCreacion");
            Property(p => p.AudUsuarioCreacion).HasColumnName("AudUsuarioCreacion");
            Property(p => p.AudUsuarioModificacion).HasColumnName("AudUsuarioModificacion");
            Property(p => p.ValorSolicitado).HasColumnName("ValorSolicitado");
            Property(p => p.IdEstadoSolicitudCredito).HasColumnName("IdEstadoSolicitudCredito");
            Property(p => p.ValorCruceInterproductos).HasColumnName("ValorCruceInterproductos");
            Property(p => p.ValorCuentaFai).HasColumnName("ValorCuentaFai");
            Property(p => p.ValorGirarCheque).HasColumnName("ValorGirarCheque");
            Property(p => p.ValorSaldoAnterior).HasColumnName("ValorSaldoAnterior");
            Property(p => p.ValorInmueble).HasColumnName("ValorInmueble");
            Property(p => p.IdAseguabilidadOpcion).HasColumnName("IdAseguabilidadOpcion");
            Property(p => p.ConsecutivoSolicitud).HasColumnName("ConsecutivoSolicitud");
            Property(p => p.Observaciones).HasColumnName("Observaciones");
            Property(p => p.IdUsoCredito).HasColumnName("IdUsoCredito");
            Property(p => p.IdOficina).HasColumnName("IdOficina");
            Property(p => p.IdTipoGarantia).HasColumnName("IdTipoGarantia");
            Property(p => p.fscNumSolicitud).HasColumnName("FSC_NUMSOLICITUD");
            Property(p => p.IdCiuuFomento).HasColumnName("IdCiuuFomento");
            Property(p => p.CursoFodes).HasColumnName("CursoFodes");

        }
    }
}
