﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class ConsolidadoAprobacionMapa :EntityTypeConfiguration<ConsolidadoAprobacion>
    {
        public ConsolidadoAprobacionMapa()
        {
            HasKey(p => p.IdConsolidadoAprobacion);

            ToTable("CONSOLIDADO_APROBACION");
            Property(p => p.IdConsolidadoAprobacion).HasColumnName("IdConsolidadoAprobacion");
            Property(p => p.IdSolicitud).HasColumnName("IdSolicitud");
            // Property(p => p.FechaAprobacion).HasColumnName("FechaAprobacion");
            Property(p => p.UsuarioAprobo).HasColumnName("UsuarioAprobo");

        }
    }
}
