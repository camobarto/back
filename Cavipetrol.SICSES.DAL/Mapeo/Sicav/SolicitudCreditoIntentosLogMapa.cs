﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class SolicitudCreditoIntentosLogMapa : EntityTypeConfiguration<SolicitudCreditoIntentosLog>
    {
        public SolicitudCreditoIntentosLogMapa()
        {
            HasKey(p => p.IdSolicitudCreditoIntentoLog);
            ToTable("SOLICITUD_CREDITO_INTENTOS_LOG");
            Property(p => p.IdSolicitudCreditoIntentoLog).HasColumnName("IdSolicitudCreditoIntentoLog");
            Property(p => p.IdTipoIdentificacion).HasColumnName("IdTipoIdentificacion");
            Property(p => p.NumeroIdentificacion).HasColumnName("NumeroIdentificacion");
            Property(p => p.IdSolicitudCreditoConcepto).HasColumnName("IdSolicitudCreditoConcepto");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
            Property(p => p.AudFechaCrea).HasColumnName("AudFechaCrea");
            Property(p => p.AudFechaMod).HasColumnName("AudFechaMod");
            Property(p => p.AudUsuCrea).HasColumnName("AudUsuCrea");
            Property(p => p.AudUsuMod).HasColumnName("AudUsuMod");
            Property(p => p.Activo).HasColumnName("Activo");
            Property(p => p.IdTipoCreditoTipoRolRelacion).HasColumnName("IdTipoCreditoTipoRolRelacion");
        }
    }
}
