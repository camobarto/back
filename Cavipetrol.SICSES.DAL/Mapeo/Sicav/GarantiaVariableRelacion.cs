﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class GarantiaVariableRelacionMapa : EntityTypeConfiguration<GarantiaVariableRelacion>
    {
        public GarantiaVariableRelacionMapa()
        {
            HasKey(p => p.IdGarantiaVariableRel);

            ToTable("GARANTIA_VARIABLE_RELACION");
            Property(p => p.IdGarantiaVariableRel).HasColumnName("IdGarantiaVariableRel");
            Property(p => p.IdTipoCreditoTipoGarantia).HasColumnName("IdTipoCreditoTipoGarantia");
            Property(p => p.IdGarantiaVariable).HasColumnName("IdGarantiaVariable");
            Property(p => p.RangoInicial).HasColumnName("RangoInicial");
            Property(p => p.RangoFinal).HasColumnName("RangoFinal");
            Property(p => p.Codeudores).HasColumnName("Codeudores");
        }
    }
}