﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class MenuMapa : EntityTypeConfiguration<Menu>
    {
        public MenuMapa()
        {
            HasKey(p => p.IdMenu);

            ToTable("MENU");
            Property(p => p.IdMenu).HasColumnName("IdMenu");
            Property(p => p.IdMenuOrigen).HasColumnName("IdMenuOrigen");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");

            Property(p => p.Ruta).HasColumnName("Ruta");
        }
    }
}
