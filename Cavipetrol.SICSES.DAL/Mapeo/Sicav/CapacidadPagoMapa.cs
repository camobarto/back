﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class CapacidadPagoMapa :  EntityTypeConfiguration<CapacidadPago>
    {
        public CapacidadPagoMapa()
        {

            HasKey(p => p.IdCapacidadPago);

            ToTable("CAPACIDAD_PAGO");
            Property(p => p.IdCapacidadPago).HasColumnName("IdCapacidadPago");
            Property(p => p.IdEstadoCapacidadPago).HasColumnName("IdEstadoCapacidadPago");
            Property(p => p.IdSolicitud).HasColumnName("IdSolicitud");
            Property(p => p.JsonCapacidadPago).HasColumnName("JsonCapacidadPago");
            Property(p => p.AudFechaCreacion).HasColumnName("AudFechaCreacion");
            Property(p => p.AudFechaModificacion).HasColumnName("AudFechaModificacion");
            Property(p => p.AudUsuarioCreacion).HasColumnName("AudUsuarioCreacion");
            Property(p => p.AudUsuarioModificacion).HasColumnName("AudUsuarioModificacion");
            Property(p => p.ValorNegociado).HasColumnName("ValorNegociado");
        }
    }
}
