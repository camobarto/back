﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class SolicitudCreditoNegadoMapa : EntityTypeConfiguration<SolicitudCreditoNegado>
    {
        public SolicitudCreditoNegadoMapa()
        {
            HasKey(p => p.IdSolicitudCreditoNegacion);

            ToTable("SOLICITUD_CREDITO_NEGADO");

            Property(p => p.IdSolicitudCreditoNegacion).HasColumnName("IdSolicitudCreditoNegacion");
            Property(p => p.IdSolicitud).HasColumnName("IdSolicitud");
            Property(p => p.IdCausalNegacion).HasColumnName("IdCausalNegacion");
            Property(p => p.Activo).HasColumnName("Activo");
        }
    }
}
