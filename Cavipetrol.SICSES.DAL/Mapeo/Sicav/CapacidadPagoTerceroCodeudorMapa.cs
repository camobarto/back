﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class CapacidadPagoTerceroCodeudorMapa : EntityTypeConfiguration<CapacidadPagoTerceroCodeudor>
    {
        public CapacidadPagoTerceroCodeudorMapa()
        {
            HasKey(p => p.IdCapacidadPagoTerceroCodeudor);

            ToTable("CAPACIDAD_PAGO_TERCEROCODEUDOR");
            Property(p => p.IdCapacidadPagoTerceroCodeudor).HasColumnName("IdCapacidadPagoTerceroCodeudor");
            Property(p => p.idSolicitud).HasColumnName("idSolicitud");
            Property(p => p.TipoIdentificacion).HasColumnName("TipoIdentificacionTercero    ");
            Property(p => p.NumeroIdentificacion).HasColumnName("NumeroIdentificacionCodeudor");
            Property(p => p.Nombre).HasColumnName("NombreTerceroCodeudor");
            Property(p => p.FechaNacimiento).HasColumnName("FechaNacimientoTercero");
            Property(p => p.JsonCapacidadPago).HasColumnName("JsonCapacidadPago");
        }
    }
}
