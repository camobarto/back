﻿using Cavipetrol.SICSES.Infraestructura.Model.Seguridad;
using System.Data.Entity.ModelConfiguration;


namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class UsuarioMapa : EntityTypeConfiguration<Usuario>
    {
        public UsuarioMapa()
        {
            HasKey(p => p.IdUsuario);

            ToTable("USUARIOS");

            Property(p => p.IdUsuario).HasColumnName("IdUsuario");
            Property(p => p.UniqueName).HasColumnName("UniqueName");
            Property(p => p.Contrasena).HasColumnName("Contrasena");
            Property(p => p.IdPerfil).HasColumnName("IdPerfil");
            Property(p => p.IdOficina).HasColumnName("IdOficina");
            Property(p => p.IP).HasColumnName("IP");
            Property(p => p.Estado).HasColumnName("Estado");
        }
    }
}
