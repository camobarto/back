﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class ReferenciasAsociadoMapa : EntityTypeConfiguration<ReferenciasAsociado>
    {
        public ReferenciasAsociadoMapa()
        {
            HasKey(p => p.IdReferenciaAsociado);

            ToTable("REFERENCIAS_ASOCIADO");
            Property(p => p.IdReferenciaAsociado).HasColumnName("IdReferenciaAsociado");
            Property(p => p.IdTipoIdentificacion).HasColumnName("IdTipoIdentificacion");
            Property(p => p.NumeroIdentificacion).HasColumnName("NumeroIdentificacion");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Numero).HasColumnName("Numero");
            Property(p => p.Relacion).HasColumnName("Relacion");
            Property(p => p.IdTipoReferencia).HasColumnName("IdTipoReferencia");
            Property(p => p.Activo).HasColumnName("Activo");
        }
    }
}
