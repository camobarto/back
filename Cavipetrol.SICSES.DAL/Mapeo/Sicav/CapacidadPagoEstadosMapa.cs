﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class CapacidadPagoEstadosMapa : EntityTypeConfiguration<CapacidadPagoEstados>
    {
        public CapacidadPagoEstadosMapa()
        {

            HasKey(p => p.IdEstadoCapacidadPago);

            ToTable("CAPACIDAD_PAGO_ESTADOS");
            Property(p => p.IdEstadoCapacidadPago).HasColumnName("IdEstadoCapacidadPago");
            Property(p => p.Nombre).HasColumnName("Nombre");
            
        }
    }
}
