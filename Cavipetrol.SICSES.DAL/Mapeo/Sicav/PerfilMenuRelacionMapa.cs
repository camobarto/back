﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System.Data.Entity.ModelConfiguration;


namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class PerfilMenuRelacionMapa : EntityTypeConfiguration<PerfilMenuRelacion>
    {
        public PerfilMenuRelacionMapa() {
            HasKey(p => p.IdPerfilMenuRelacion);

            ToTable("PERFIL_MENU_RELACION");
            Property(p => p.IdPerfilMenuRelacion).HasColumnName("IdPerfilMenuRelacion");
            Property(p => p.IdMenu).HasColumnName("IdMenu");
            Property(p => p.IdPerfil).HasColumnName("IdPerfil");
        }
    }
}
