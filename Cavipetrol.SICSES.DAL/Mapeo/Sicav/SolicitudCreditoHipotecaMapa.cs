﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class SolicitudCreditoHipotecaMapa : EntityTypeConfiguration<SolicitudCreditoHipoteca>
    {
        public SolicitudCreditoHipotecaMapa()
        {
            HasKey(p => p.IdSolicitudHipoteca);

            ToTable("SOLICITUD_CREDITO_HIPOTECA");
            Property(p => p.IdSolicitudHipoteca).HasColumnName("IdSolicitudHipoteca");
            Property(p => p.IdSolicitud).HasColumnName("IdSolicitud");
            Property(p => p.Monto).HasColumnName("Monto");
            Property(p => p.Activo).HasColumnName("Activo");
            Property(p => p.UsuarioCreacion).HasColumnName("UsuarioCreacion");
            Property(p => p.UsuarioModificacion).HasColumnName("UsuarioModificacion");
            Property(p => p.FechaCreacion).HasColumnName("FechaCreacion");
            Property(p => p.FechaModificacion).HasColumnName("FechaModificacion");
            Property(p => p.PagareGenerado).HasColumnName("PagareGenerado");
        }
    }
}
