﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Sicav
{
    public class SolicitudCreditoAdjuntoMapa : EntityTypeConfiguration<SolicitudCreditoAdjunto>
    {
        public SolicitudCreditoAdjuntoMapa()
        {
            HasKey(p => p.IdSolicitudCreditoDocumento);
            ToTable("SOLICITUD_CREDITO_ADJUNTO");
            Property(p => p.IdSolicitudCreditoDocumento).HasColumnName("IdSolicitudCreditoDocumento");
            Property(p => p.IdSolicitud).HasColumnName("IdSolicitud");
            Property(p => p.IdTipoAdjunto).HasColumnName("IdTipoAdjunto");
            Property(p => p.FechaCreacion).HasColumnName("FechaCreacion");
            Property(p => p.FechaModificacion).HasColumnName("FechaModificacion");
            Property(p => p.UsuarioCreacion).HasColumnName("UsuarioCreacion");
            Property(p => p.UsuarioModificacion).HasColumnName("UsuarioModificacion");
        }
    }
}
