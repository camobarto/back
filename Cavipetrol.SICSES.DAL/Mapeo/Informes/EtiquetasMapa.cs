﻿using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Informes
{
    public class EtiquetasMapa : EntityTypeConfiguration<Etiquetas>
    {
        public EtiquetasMapa()
        {
            HasKey(p => p.IdEtiquetaNota);

            ToTable("ETIQUETA_NOTA");
            Property(p => p.IdEtiquetaNota).HasColumnName("IdEtiquetaNota");
            Property(p => p.IdNota).HasColumnName("IdNota");
            Property(p => p.Etiqueta).HasColumnName("Etiqueta");
            Property(p => p.ValorEtiqueta).HasColumnName("ValorEtiqueta");
        }
    }
}
