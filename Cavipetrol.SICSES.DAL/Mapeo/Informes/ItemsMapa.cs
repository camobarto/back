﻿using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Informes
{
  
    public class ItemsMapa : EntityTypeConfiguration<Items>
    {
        public ItemsMapa()
        {
            HasKey(p => p.IdItemNota);

            ToTable("ITEM_NOTA");
            Property(p => p.IdItemNota).HasColumnName("IdItemNota");
            Property(p => p.IdNota).HasColumnName("IdNota");
            Property(p => p.NumeroItem).HasColumnName("NumeroItem");
            Property(p => p.IdTipoImpuesto).HasColumnName("IdTipoImpuesto");
            Property(p => p.ValorSinImpuesto).HasColumnName("ValorSinImpuesto");
            Property(p => p.Porcentaje).HasColumnName("Porcentaje");
            Property(p => p.TotalImpuesto).HasColumnName("TotalImpuesto");
            Property(p => p.Descuento).HasColumnName("Descuento");
            Property(p => p.CodigoProducto).HasColumnName("CodigoProducto");
            Property(p => p.DescripcionProducto).HasColumnName("DescripcionProducto");
            Property(p => p.Cantidad).HasColumnName("Cantidad");
            Property(p => p.PrecioUnitario).HasColumnName("PrecioUnitario");
            Property(p => p.ValorTotalItem).HasColumnName("ValorTotalItem");
            Property(p => p.PorcentajeIVA).HasColumnName("PorcentajeIVA");
            Property(p => p.ValorIVA).HasColumnName("ValorIVA");
            Property(p => p.PorcentajeConsumo).HasColumnName("PorcentajeConsumo");
            Property(p => p.ValorConsumo).HasColumnName("ValorConsumo");
            Property(p => p.PorcentajeICA).HasColumnName("PorcentajeICA");
            Property(p => p.ValorICA).HasColumnName("ValorICA");
            Property(p => p.DescripcionAdicional).HasColumnName("DescripcionAdicional");
            Property(p => p.ValorDescuentoAplicado).HasColumnName("ValorDescuentoAplicado");
            Property(p => p.PorcentajeImpuesto).HasColumnName("PorcentajeImpuesto");
        }
    }
}
