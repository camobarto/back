﻿using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Informes
{
    public class NotaMapa : EntityTypeConfiguration<Nota>
    {
        public NotaMapa() {
            HasKey(p => p.IdNota);

            ToTable("NOTA");
            Property(p => p.IdNota).HasColumnName("IdNota");
            Property(p => p.FechaCreacion).HasColumnName("FechaCreacion");
            Property(p => p.IdFactura).HasColumnName("IdFactura");
            Property(p => p.IdTipoIdentificacionCliente).HasColumnName("IdTipoIdentificacionCliente");
            Property(p => p.IdTipoNota).HasColumnName("IdTipoNota");
            Property(p => p.MotivoNotaCredito).HasColumnName("MotivoNotaCredito");
            Property(p => p.NumAsientoFYC).HasColumnName("NumAsientoFYC");
            Property(p => p.NumIdentificacionCliente).HasColumnName("NumIdentificacionCliente");
            Property(p => p.SecuencialDocumentoSustento).HasColumnName("SecuencialDocumentoSustento");
            Property(p => p.Usuario).HasColumnName("Usuario");            
        }
    }
}
