﻿using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Informes
{
    public  class RetencionMOVMapa : EntityTypeConfiguration<DatosIva>
    {
        public RetencionMOVMapa()
        {
            HasKey(p => new { p.RET_TIPO_SOPORTE, p.RET_NUM_SOPORTE, p.RET_CIUDAD,p.RET_CUENTA,p.RET_TIPO_NIT, p.RET_NUM_NIT });

            ToTable("RETENCION_MOVIMIENTO");
            Property(p => p.RET_TIPO_SOPORTE).HasColumnName("RET_TIPO_SOPORTE");
            Property(p => p.RET_NUM_SOPORTE).HasColumnName("RET_NUM_SOPORTE");
            Property(p => p.ASI_FECHA).HasColumnName("RET_FECHA");
            Property(p => p.RET_CIUDAD).HasColumnName("RET_CIUDAD");
            Property(p => p.RET_CUENTA).HasColumnName("RET_CUENTA");
            Property(p => p.NOMBRE_CUENTA).HasColumnName("RET_CONCEPTO");
            Property(p => p.MVC_VALOR_DEBITO).HasColumnName("RET_DEBITO");
            Property(p => p.MVC_VALOR_CREDITO).HasColumnName("RET_CREDITO");
            Property(p => p.RET_TIPO_NIT).HasColumnName("RET_TIPO_NIT");
            Property(p => p.RET_NUM_NIT).HasColumnName("RET_NUM_NIT");
            Property(p => p.RET_PORCENTAJE).HasColumnName("RET_PORCENTAJE");
            Property(p => p.RET_BASE).HasColumnName("RET_BASE");
            Property(p => p.TIPO).HasColumnName("TIPO");



        }
    }
}
