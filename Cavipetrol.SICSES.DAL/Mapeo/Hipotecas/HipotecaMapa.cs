﻿using Cavipetrol.SICSES.Infraestructura.Model.Hipotecas;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Hipotecas
{
    class HipotecaMapa : EntityTypeConfiguration<Hipoteca>
    {
        public HipotecaMapa()
        {
            HasKey(p => p.IdHipoteca);

            ToTable("HIPOTECA");
            Property(p => p.IdHipoteca).HasColumnName("IdHipoteca");
            Property(p => p.NumeroHipoteca).HasColumnName("NumeroHipoteca");
            Property(p => p.TipoDocumentoHipotecario).HasColumnName("TipoDocumentoHipotecario");
            Property(p => p.CedulaHipotecario).HasColumnName("CedulaHipotecario");
            Property(p => p.NombreHipotecario).HasColumnName("NombreHipotecario");
            Property(p => p.FechaEscrituracion).HasColumnName("FechaEscrituracion");
            Property(p => p.ValorCubierto).HasColumnName("ValorCubierto");
            Property(p => p.ValorCobertura).HasColumnName("ValorCobertura");
            Property(p => p.ValorAvaluo).HasColumnName("ValorAvaluo");
            Property(p => p.VigenciaInicial).HasColumnName("VigenciaInicial");
            Property(p => p.VigenciaFinal).HasColumnName("VigenciaFinal");


        }
    }
}
