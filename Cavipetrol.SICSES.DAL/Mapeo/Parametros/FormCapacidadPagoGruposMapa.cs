﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class FormCapacidadPagoGruposMapa : EntityTypeConfiguration<FormCapacidadPagoGrupos>
    {
        public FormCapacidadPagoGruposMapa() {
            HasKey(p => p.IdGrupo);
            ToTable("FORM_CAPACIDAD_PAGO_GRUPOS");
            Property(p => p.IdGrupo).HasColumnName("IdGrupo");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Total).HasColumnName("Total");
            Property(p => p.MostrarTotal).HasColumnName("MostrarTotal");
            Property(p => p.Mostrar).HasColumnName("Mostrar");
            Property(p => p.Orden).HasColumnName("Orden");
        }        
    }

}
