﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class ClaseDeActivoMapa : EntityTypeConfiguration<ClaseDeActivo>
    {
        public ClaseDeActivoMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_CLASE_ACTIVO");
            Property(p => p.Id).HasColumnName("ID_CLASE_ACTIVO");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(200);
        }
    }
}
