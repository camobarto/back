﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoEntidadBeneficiariaMapa : EntityTypeConfiguration<TipoEntidadBeneficiaria>
    {
        public TipoEntidadBeneficiariaMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPO_ENTIDAD_BENEFICIARIA");
            Property(p => p.Id).HasColumnName("ID_ENTIDAD_BENEFICIARIA");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(80);
        }
    }
}
