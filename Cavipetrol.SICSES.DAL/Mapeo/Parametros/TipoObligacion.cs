﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoObligacionMapa : EntityTypeConfiguration<TipoObligacion>
    {
        public TipoObligacionMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_OBLIGACIONES");
            Property(p => p.Id).HasColumnName("ID_OBLIGACION");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(80);
        }
    }
}
