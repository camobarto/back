﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo
{
    public class TipoEntidadMapa : EntityTypeConfiguration<TipoEntidad>
    {
        public TipoEntidadMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_ENTIDAD");
            Property(p => p.Id).HasColumnName("ID_ENTIDAD");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(30);
        }
    }
}
