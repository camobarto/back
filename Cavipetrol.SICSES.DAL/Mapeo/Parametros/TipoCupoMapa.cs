﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoCupoMapa : EntityTypeConfiguration<TiposCupo>
    {
        public TipoCupoMapa()
        {
            HasKey(p => p.IdTipoCupo);

            ToTable("TIPOS_CUPO");
            Property(p => p.IdTipoCupo).HasColumnName("IdTipoCupo");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
        }
    }
}
