﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TiposTitulosMapa : EntityTypeConfiguration<TiposTitulos>
    {
        public TiposTitulosMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_TITULOS");
            Property(p => p.Id).HasColumnName("ID_TIPO_TITULO");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(200);

        }
    }
}
