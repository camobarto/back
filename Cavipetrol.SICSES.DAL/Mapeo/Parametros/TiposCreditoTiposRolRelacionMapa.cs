﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TiposCreditoTiposRolRelacionMapa : EntityTypeConfiguration<TipoCreditoTipoRolRelacion>
    {
        public TiposCreditoTiposRolRelacionMapa()
        {
            HasKey(p => p.IdTipoCreditoTipoRolRelacion);

            ToTable("TIPOS_CREDITO_TIPOS_ROL_RELACION");
            Property(p => p.IdTipoCreditoTipoRolRelacion).HasColumnName("IdTipoCreditoTipoRolRelacion");
            Property(p => p.IdTipoCredito).HasColumnName("IdTipoCredito");
            Property(p => p.IdTipoCupoMaximo).HasColumnName("IdTipoCupoMaximo");
            Property(p => p.IdPapelCavipetrol).HasColumnName("IdPapelCavipetrol");
            Property(p => p.InteresEfectivoAnual).HasColumnName("InteresEfectivoAnual");
            Property(p => p.InteresNominalQuincenaVendida).HasColumnName("InteresNominalQuincenaVendida");
            Property(p => p.PlazoMaximo).HasColumnName("PlazoMaximo");
            Property(p => p.PlazoMinimo).HasColumnName("PlazoMinimo");
            Property(p => p.PorcentajeNovacion).HasColumnName("PorcentajeNovacion");
            Property(p => p.TiempoMinimoDeAfiliacionAnhos).HasColumnName("TiempoMinimoDeAfiliacionAnhos");
            Property(p => p.CupoMaximo).HasColumnName("CupoMaximo");
            Property(p => p.IdProductoFYC).HasColumnName("IdProductoFYC");
            Property(p => p.TipoVehiculo).HasColumnName("TipoVehiculo");
        }
    }
}
