﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoJornadaLaboralMapa : EntityTypeConfiguration<TipoJornadaLaboral>
    {
        public TipoJornadaLaboralMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_JORNADA_LABORAL");
            Property(p => p.Id).HasColumnName("ID_LABORAL");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(80);
        }
    }
}
