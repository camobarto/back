﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System.Data.Entity.ModelConfiguration;


namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class SolicitudDescripcionFondosMapa : EntityTypeConfiguration<SolicitudDescripcionFondos>
    {
        public SolicitudDescripcionFondosMapa()
        {
            HasKey(p => p.Id);

            ToTable("DESCRIPCION_ORIGEN_FONDOS");

            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.Descripcion_Origen).HasColumnName("Descripcion_Origen");
            Property(p => p.Opcion).HasColumnName("Opcion");           
        }
    }
}
