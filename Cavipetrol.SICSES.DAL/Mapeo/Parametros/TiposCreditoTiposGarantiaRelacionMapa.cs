﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    class TiposCreditoTiposGarantiaRelacionMapa : EntityTypeConfiguration<TiposCreditoTiposGarantiaRelacion>
    {
        
            public TiposCreditoTiposGarantiaRelacionMapa()
            {
                HasKey(p => p.IdTipoCreditoTIpoGarantia);

                ToTable("TIPOS_CREDITO_TIPOS_GARANTIA_RELACION");
                Property(p => p.IdTipoCredito).HasColumnName("IdTipoCredito");
                Property(p => p.IdTipoGarantia).HasColumnName("IdTipoGarantia");
                Property(p => p.IdTipoCreditoTIpoGarantia).HasColumnName("IdTipoCreditoTIpoGarantia");
                Property(p => p.IdPagare).HasColumnName("IdPagare");
                Property(p => p.Codeudores).HasColumnName("Codeudores");
        }       
    }
}
