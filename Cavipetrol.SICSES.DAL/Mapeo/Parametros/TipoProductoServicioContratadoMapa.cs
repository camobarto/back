﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoProductoServicioContratadoMapa : EntityTypeConfiguration<TipoProductoServicioContratado>
    {
        public TipoProductoServicioContratadoMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_PRODUCTO_SERVICIO_CONTRATADO");
            Property(p => p.Id).HasColumnName("ID_PRODUCTO_SERVICIO");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(80);
        }
    }
}
