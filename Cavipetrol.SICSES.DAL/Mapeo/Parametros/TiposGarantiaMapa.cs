﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TiposGarantiaMapa : EntityTypeConfiguration<TiposGarantia>
    {
        public TiposGarantiaMapa()
        {
            HasKey(p => p.IdTipoGarantia);

            ToTable("TIPOS_GARANTIA");
            Property(p => p.IdTipoGarantia).HasColumnName("IdTipoGarantia");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
        }
    }
}
