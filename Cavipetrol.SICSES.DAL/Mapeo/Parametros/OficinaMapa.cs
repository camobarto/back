﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class OficinaMapa : EntityTypeConfiguration<Oficina>
    {
        public OficinaMapa()
        {
            HasKey(p => p.IdOficina);

            ToTable("OFICINA");

            Property(p => p.IdOficina).HasColumnName("IdOficina");
            Property(p => p.OFI_NOM_OFICINA).HasColumnName("OFI_NOM_OFICINA");
            Property(p => p.OFI_NOM_CIUDAD).HasColumnName("OFI_NOM_CIUDAD");
            Property(p => p.UBICACION).HasColumnName("UBICACION");
            Property(p => p.FECHA_APERTURA).HasColumnName("FECHA_APERTURA");
            Property(p => p.FECHA_CIERRE).HasColumnName("FECHA_CIERRE");
            Property(p => p.CANTIDAD_HABITANTES).HasColumnName("CANTIDAD_HABITANTES");
            Property(p => p.CANTIDAD_ENTIDADES_FRAS).HasColumnName("CANTIDAD_ENTIDADES_FRAS");
            Property(p => p.CANTIDAD_COOPERATIVAS).HasColumnName("CANTIDAD_COOPERATIVAS");
            Property(p => p.DEPARTAMENTO).HasColumnName("DEPARTAMENTO");
            Property(p => p.MUNICIPIO).HasColumnName("MUNICIPIO");
            Property(p => p.DPR_ID).HasColumnName("DPR_ID");
            Property(p => p.MNC_ID).HasColumnName("MNC_ID");
        }
    }
}
