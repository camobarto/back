﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    class ClaseNaturalezaMapa : EntityTypeConfiguration<ClaseNaturaleza>
    {
        public ClaseNaturalezaMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_NATURALEZA");
            Property(p => p.Id).HasColumnName("ID_NATURALEZA");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(200);

        }
    }
}
