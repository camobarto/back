﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class CuposAsegurabilidadMapa : EntityTypeConfiguration<CuposAsegurabilidad>
    {
        public CuposAsegurabilidadMapa()
        {
            HasKey(p => p.IdCupoAsegurabilidad);

            ToTable("CUPOS_ASEGURABILIDAD");
            Property(p => p.IdCupoAsegurabilidad).HasColumnName("IdCupoAsegurabilidad");
            Property(p => p.EdadInicial).HasColumnName("EdadInicial");
            Property(p => p.EdadFinal).HasColumnName("EdadFinal");
            Property(p => p.Valor).HasColumnName("Valor");
        }
    }
}
