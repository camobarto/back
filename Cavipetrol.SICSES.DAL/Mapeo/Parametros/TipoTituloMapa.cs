﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoTituloMapa : EntityTypeConfiguration<TipoTitulo>
    {
        public TipoTituloMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPO_TITULO");
            Property(p => p.Id).HasColumnName("ID_TITULO");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(200);
        }
    }
}
