﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class FormCapacidadPagoTipoCreditoTipoRolRelacionMapa : EntityTypeConfiguration<FormCapacidadPagoTipoCreditoTipoRolRelacion>
    {
        public FormCapacidadPagoTipoCreditoTipoRolRelacionMapa() {
            HasKey(p => p.IdCapacidadPagoTipoCreditoTipoRolRelacion);
            ToTable("FORM_CAPACIDAD_PAGO_TIPO_CREDITO_TIPO_ROL_RELACION");
            Property(p => p.IdCapacidadPagoTipoCreditoTipoRolRelacion).HasColumnName("IdCapacidadPagoTipoCreditoTipoRolRelacion");
            Property(p => p.IdTipoCreditoTipoRolRelacion).HasColumnName("IdTipoCreditoTipoRolRelacion");
            Property(p => p.IdCapacidadPago).HasColumnName("IdCapacidadPago");            
            Property(p => p.Orden).HasColumnName("Orden");
        }
    }
}
