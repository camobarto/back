﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class AnosMapa : EntityTypeConfiguration<Anos>
    {
        public AnosMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_ANOS");
            Property(p => p.Id).HasColumnName("ANOS");
        }
    }
}
