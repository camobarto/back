﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class AlternativasDecreto2880Mapa : EntityTypeConfiguration<AlternativasDecreto2880>
    {
        public AlternativasDecreto2880Mapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_ALTERNATIVAS_INVERSION_DECRETO_2880");
            Property(p => p.Id).HasColumnName("ID_DECRETO_2880");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(80);
        }
    }
}
