﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoLineaCreditoMapa : EntityTypeConfiguration<TipoLineaCredito>
    {
        public TipoLineaCreditoMapa() {
            HasKey(p => p.IdTipoLineaCredito);
            ToTable("TIPOS_LINEA_CREDITO");
            Property(p => p.IdTipoLineaCredito).HasColumnName("IdTipoLineaCredito");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
        }
    }
}
