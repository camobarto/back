﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoEstadoCivilMapa : EntityTypeConfiguration<TipoEstadoCivil>
    {
        public TipoEstadoCivilMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_ESTADO_CIVIL");
            Property(p => p.Id).HasColumnName("ID_ESTADO_CIVIL");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(50);
        }
    }
}
