﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class PlantillaMapa : EntityTypeConfiguration<Plantilla>
    {
        public PlantillaMapa()
        {
            HasKey(p => p.IdPlantilla);
            ToTable("PLANTILLA");
            Property(p => p.IdPlantilla).HasColumnName("IdPlantilla");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
            Property(p => p.Html).HasColumnName("Html");
        }
    }
}
