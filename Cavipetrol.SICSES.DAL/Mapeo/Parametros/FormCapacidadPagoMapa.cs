﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class FormCapacidadPagoMapa : EntityTypeConfiguration<FormCapacidadPago>
    {
        public FormCapacidadPagoMapa() {
            HasKey(p => p.IdCapacidadPago);
            ToTable("FORM_CAPACIDAD_PAGO");
            Property(p => p.IdCapacidadPago).HasColumnName("IdCapacidadPago");
            Property(p => p.IdGrupo).HasColumnName("IdGrupo");
            Property(p => p.Texto).HasColumnName("Texto");
            Property(p => p.ValorAsociado).HasColumnName("ValorAsociado");
            Property(p => p.ValorFamiliar).HasColumnName("ValorFamiliar");
            Property(p => p.EsSoloLecturaAsociado).HasColumnName("EsSoloLecturaAsociado");
            Property(p => p.EsSoloLecturaFamiliar).HasColumnName("EsSoloLecturaFamiliar");
            Property(p => p.Factor).HasColumnName("Factor");
            Property(p => p.Tipo).HasColumnName("Tipo");
            Property(p => p.TipoLista).HasColumnName("TipoLista");
        }
    }
}
