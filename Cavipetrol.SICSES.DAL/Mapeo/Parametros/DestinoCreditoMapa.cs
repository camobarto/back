﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class DestinoCreditoMapa : EntityTypeConfiguration<DestinoCredito>
    {
        public DestinoCreditoMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_DESTINO_CREDITO");
            Property(p => p.Id).HasColumnName("ID_DESTINO_CREDITO");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(200);

        }
    }
}
