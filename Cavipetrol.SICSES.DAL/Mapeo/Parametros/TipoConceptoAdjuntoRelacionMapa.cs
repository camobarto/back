﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoConceptoAdjuntoRelacionMapa : EntityTypeConfiguration<TipoConceptoAdjuntoRelacion>
    {
        public TipoConceptoAdjuntoRelacionMapa()
        {
            HasKey(p => p.IdTipoConceptoAdjuntoRelacion);

            ToTable("TIPO_CONCEPTO_ADJUNTO_RELACION");

            Property(p => p.IdTipoConceptoAdjuntoRelacion).HasColumnName("IdTipoConceptoAdjuntoRelacion");
            Property(p => p.IdTipoAdjunto).HasColumnName("IdTipoAdjunto");
            Property(p => p.IdConceptoAdjunto).HasColumnName("IdConceptoAdjunto");
            Property(p => p.Activo).HasColumnName("Activo");
        }
    }
}
