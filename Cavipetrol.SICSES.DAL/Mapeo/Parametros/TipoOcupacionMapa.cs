﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoOcupacionMapa : EntityTypeConfiguration<TipoOcupacion>
    {
        public TipoOcupacionMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_OCUPACION");
            Property(p => p.Id).HasColumnName("ID_OCUPACION");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(80);
        }
    }
}
