﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoConceptoAdjuntoMapa : EntityTypeConfiguration<TipoConceptoAdjunto>
    {
        public TipoConceptoAdjuntoMapa()
        {
            HasKey(p => p.IdConceptoAdjunto);

            ToTable("TIPOS_CONCEPTOS_ADJUNTO");

            Property(p => p.IdConceptoAdjunto).HasColumnName("IdConceptoAdjunto");
            Property(p => p.Concepto).HasColumnName("Concepto");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
        }
    }
}
