﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
   

    public class TipoBeneficiariosMapa : EntityTypeConfiguration<TipoBeneficiarios>
    {
        public TipoBeneficiariosMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_BENEFICIARIOS");
            Property(p => p.Id).HasColumnName("ID_BENEFICIARIOS");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(100);
        }
    }
}
