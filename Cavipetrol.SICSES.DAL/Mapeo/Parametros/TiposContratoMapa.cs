﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TiposContratoMapa : EntityTypeConfiguration<TiposContrato>
    {
        public TiposContratoMapa()
        {
            HasKey(p => p.IdTipoContrato);

            ToTable("TIPOS_CONTRATO");
            Property(p => p.IdTipoContrato).HasColumnName("IdTipoContrato");
            Property(p => p.Nombre).HasColumnName("Nombre");
        }
    }
}
