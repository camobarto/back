﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoParentescoGradoConsanguinidadMapa : EntityTypeConfiguration<TipoParentescoGradoConsanguinidad>
    {
        public TipoParentescoGradoConsanguinidadMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_PARENTESCO_GRADO_CONSANGUINIDAD");
            Property(p => p.Id).HasColumnName("ID_TIPOS_PARENTESCO");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(200);
        }
    }
}
