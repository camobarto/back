﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TiposCreditoTiposSeguroRelacionMapa : EntityTypeConfiguration<TiposCreditoTipoSeguroRelacion>
    {
        public TiposCreditoTiposSeguroRelacionMapa()
        {
            HasKey(p => new { p.IdTipoCredito, p.IdTipoSeguro, p.RangoInicial,  p.RangoFinal});         

            ToTable("TIPOS_CREDITO_TIPOS_SEGURO_RELACION");
            Property(p => p.IdTipoCredito).HasColumnName("IdTipoCredito");
            Property(p => p.IdTipoSeguro).HasColumnName("IdTipoSeguro");
            Property(p => p.Factor).HasColumnName("Factor");
            Property(p => p.RangoInicial).HasColumnName("RangoInicial");
            Property(p => p.RangoFinal).HasColumnName("RangoFinal");
            Property(p => p.TasaPorMil).HasColumnName("TasaPorMil");

        }
    }
}
