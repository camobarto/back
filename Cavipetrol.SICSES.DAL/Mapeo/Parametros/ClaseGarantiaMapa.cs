﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class ClaseGarantiaMapa : EntityTypeConfiguration<ClaseGarantia>
    {
        public ClaseGarantiaMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_CLASE_GARANTIA");
            Property(p => p.Id).HasColumnName("ID_CLASE_GARANTIA");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(200);

        }
    }
}
