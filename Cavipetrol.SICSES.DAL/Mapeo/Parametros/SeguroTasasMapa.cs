﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class SeguroTasasMapa : EntityTypeConfiguration<SeguroTasas>
    {
        public SeguroTasasMapa()
        {
            HasKey(p => p.IdSeguroTasa);
            ToTable("SEGURO_TASAS");
            Property(p => p.IdSeguroTasa).HasColumnName("IdSeguroTasa");
            Property(p => p.IdTipoSeguro).HasColumnName("IdTipoSeguro");
            Property(p => p.EdadInicial).HasColumnName("EdadInicial");
            Property(p => p.EdadFinal).HasColumnName("EdadFinal");
            Property(p => p.TasaAnual).HasColumnName("TasaAnual");
            Property(p => p.TasaMensual).HasColumnName("TasaMensual");
        }
    }
}
