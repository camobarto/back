﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoUsuarioTieneAporteMapa : EntityTypeConfiguration<TipoUsuarioTieneAporte>
    {
        public TipoUsuarioTieneAporteMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_USUARIO_TIENE_APORTE");
            Property(p => p.Id).HasColumnName("ID_USUARIO_TIENE_APORTE");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(200);
        }
    }
}
