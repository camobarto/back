﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoIdentificacionMapa : EntityTypeConfiguration<TipoIdentificacion>
    {
        public TipoIdentificacionMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_IDENTIFICACIONES");
            Property(p => p.Id).HasColumnName("tps_idn_id").HasMaxLength(1);
            Property(p => p.IdCavipetrol).HasColumnName("tps_idn_id_cavipetrol").HasMaxLength(5);
            Property(p => p.Descripcion).HasColumnName("Descripcion").HasMaxLength(200);
        }
    }
}
