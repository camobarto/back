﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoSectorEconomicoMapa : EntityTypeConfiguration<TipoSectorEconomico>
    {
        public TipoSectorEconomicoMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_SECTOR_ECONOMICO");
            Property(p => p.Id).HasColumnName("ACTIE_ID");
            Property(p => p.Nombre).HasColumnName("ACTIE_NOMBRE").HasMaxLength(80);
        }
    }
}
