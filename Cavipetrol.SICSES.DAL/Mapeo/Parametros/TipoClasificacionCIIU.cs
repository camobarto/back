﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
   public  class TipoClasificacionCIIU : EntityTypeConfiguration<ClasificacionCIIU>
    {
        public TipoClasificacionCIIU()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_CLASIFICACION_CIIU");
            Property(p => p.Id).HasColumnName("ID_CIIU");
            Property(p => p.Nombre).HasColumnName("DESCRIPCION").HasMaxLength(30);
        }
    }
}
