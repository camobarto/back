﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class UsosCreditoMapa : EntityTypeConfiguration<UsosCredito>
    {
        public UsosCreditoMapa()
        {
            HasKey(p => p.IdUsoCredito);

            ToTable("USOS_CREDITO");
            Property(p => p.IdUsoCredito).HasColumnName("IdUsoCredito");
            Property(p => p.IdTipoLineaCredito).HasColumnName("IdTipoLineaCredito");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
        }
    }
}
