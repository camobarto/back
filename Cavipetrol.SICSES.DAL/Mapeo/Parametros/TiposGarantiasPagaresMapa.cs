﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TiposGarantiasPagaresMapa : EntityTypeConfiguration<TiposGarantiasPagares>
    {
        public TiposGarantiasPagaresMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPO_GARANTIA_PAGARE");
            Property(p => p.Id).HasColumnName("ID_GARANTIA_PAGARE");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION");
        }
    }
}
