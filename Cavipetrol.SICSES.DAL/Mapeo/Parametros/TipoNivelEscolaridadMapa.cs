﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoNivelEscolaridadMapa : EntityTypeConfiguration<TipoNivelEscolaridad>
    {
        public TipoNivelEscolaridadMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_NIVEL_ESCOLARIDAD");
            Property(p => p.Id).HasColumnName("ID_NIVEL_ESCOLARIDAD");
            Property(p => p.IdEscolaridad).HasColumnName("ID_ESCOLARIDAD");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(40);
            Property(p => p.DescripcionCavipetrol).HasColumnName("DESCRIPCION_CAVIPETROL").HasMaxLength(100);
        }
    }
}
