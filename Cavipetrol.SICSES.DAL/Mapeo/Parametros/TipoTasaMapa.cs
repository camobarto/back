﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoTasaMapa : EntityTypeConfiguration<TipoTasa>
    {
        public TipoTasaMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPO_TASA");
            Property(p => p.Id).HasColumnName("ID_TASA");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(200);
        }
    }
}
