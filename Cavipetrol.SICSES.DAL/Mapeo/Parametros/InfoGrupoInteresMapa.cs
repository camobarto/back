﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class InfoGrupoInteresMapa : EntityTypeConfiguration<InformacionRelacionadaGrupoInteres>
    {
        public InfoGrupoInteresMapa()
        {
            HasKey(p => p.ID_INFORMACION_DETALLADA);

            ToTable("SICS_DATOS_INFO_GRUPOS_INTERES");
            Property(p => p.CODIGO_RENGLON).HasColumnName("CODIGO_RENGLON");
            Property(p => p.UNIDAD_CAPTURA).HasColumnName("UNIDAD_CAPTURA");
            Property(p => p.DESCRIPCION_DEL_RENGLON).HasColumnName("DESCRIPCION_DEL_RENGLON").HasMaxLength(200);
            Property(p => p.NUMERO).HasColumnName("NUMERO");
            Property(p => p.PORCENTAJE).HasColumnName("PORCENTAJE");
            Property(p => p.RECURSOS_GIRADOS_EN_EL_ANO).HasColumnName("RECURSOS_GIRADOS_EN_EL_ANO");
            Property(p => p.NUMERO_DE_ASOCIADOS_BENEFICIADOS).HasColumnName("NUMERO_DE_ASOCIADOS_BENEFICIADOS");
            Property(p => p.RECURSOS_GIRADOS_EN_EL_ANO_PARA_BENEFICIOS).HasColumnName("RECURSOS_GIRADOS_EN_EL_ANO_PARA_BENEFICIOS");
            Property(p => p.NUMERO_DE_EMPLEADOS_BENEFICIADOS).HasColumnName("NUMERO_DE_EMPLEADOS_BENEFICIADOS");
            Property(p => p.RECURSOS_GIRADOS_EN_ANO_BENEF_COMUNIDAD).HasColumnName("RECURSOS_GIRADOS_EN_ANO_BENEF_COMUNIDAD");
            Property(p => p.NUMERO_DE_PERSONAS_DE_LA_COMUNIDAD_BENEFICIADAS).HasColumnName("NUMERO_DE_PERSONAS_DE_LA_COMUNIDAD_BENEFICIADAS");




            //HasRequired(p => p.ParametroCavipetrol).WithMany(p => p.ReportesIdentificacion).HasForeignKey(p => p.IdParametroCavipetrol);
        }
    }
}
