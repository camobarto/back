﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class FormatoVigentesSICSESMapa : EntityTypeConfiguration<FormatoVigenteSICSES>
    {
        public FormatoVigentesSICSESMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_FORMATOS_VIGENTES_SICSES");
            Property(p => p.Id).HasColumnName("ID_FORMATO");
            Property(p => p.CodigoSicses).HasColumnName("CODIGO_SICSES").HasMaxLength(4);
            Property(p => p.CodigoRol).HasColumnName("CODIGO_ROL").HasMaxLength(4);
            Property(p => p.Formato).HasColumnName("FORMATO").HasMaxLength(2);
            Property(p => p.ReportesPeriodicos).HasColumnName("REPORTES_PERIODICOS").HasMaxLength(255);
        }
    }
}
