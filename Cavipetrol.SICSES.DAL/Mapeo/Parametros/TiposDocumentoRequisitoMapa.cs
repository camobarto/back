﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TiposDocumentoRequisitoMapa : EntityTypeConfiguration<TiposDocumentoRequisito>
    {
        public TiposDocumentoRequisitoMapa()
        {
            HasKey(p => p.IdTipoDocumentoRequisito);
            ToTable("TIPOS_DOCUMENTO_REQUISITO");
            Property(p => p.IdTipoDocumentoRequisito).HasColumnName("IdTipoDocumentoRequisito"); ;
            Property(p => p.Nombre).HasColumnName("Nombre"); ;
            Property(p => p.Descripcion).HasColumnName("Descripcion"); ;
        }
    }
}
