﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class EtiquetasMapa : EntityTypeConfiguration<Etiquetas>
    {
        public EtiquetasMapa()
        {
            HasKey(p => p.IdEtiqueta);

            ToTable("ETIQUETAS");
            Property(p => p.IdEtiqueta).HasColumnName("IdEtiqueta");
            Property(p => p.IdTipoEtiqueta).HasColumnName("IdTipoEtiqueta");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.esSoloLectura).HasColumnName("esSoloLectura");
            Property(p => p.esVisible).HasColumnName("esVisible");
            Property(p => p.Valor).HasColumnName("Valor");
        }
    }
}
