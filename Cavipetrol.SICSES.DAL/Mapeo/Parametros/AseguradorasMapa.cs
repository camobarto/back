﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class AseguradorasMapa : EntityTypeConfiguration<Aseguradoras>
    {
        public AseguradorasMapa()
        {
            HasKey(p => p.Id);

            ToTable("TIPOS_GARANTIA");
            Property(p => p.Id).HasColumnName("TA_ID_ASEGURADORA");
            Property(p => p.Descripcion).HasColumnName("TA_ASEGURADORA");
        }
    }
}


