﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    class TiposConceptosGarantiaMapa : EntityTypeConfiguration<TiposConceptosGarantia>
    {
        public TiposConceptosGarantiaMapa()
        {
            HasKey(p => p.IdConceptoGarantia);

            ToTable("TIPOS_CONCEPTOS_GARANTIA");
            Property(p => p.IdConceptoGarantia).HasColumnName("IdConceptoGarantia");
            Property(p => p.Concepto).HasColumnName("Concepto");
            Property(p => p.Descripcion).HasColumnName("Descripcion");
            Property(p => p.CampoRemplaza).HasColumnName("CampoRemplaza");
            Property(p => p.tipoVariable).HasColumnName("tipoVariable");
            Property(p => p.visible).HasColumnName("visible");
            Property(p => p.editable).HasColumnName("editable");
            Property(p => p.ciudad).HasColumnName("ciudad");
        }
    }
}
