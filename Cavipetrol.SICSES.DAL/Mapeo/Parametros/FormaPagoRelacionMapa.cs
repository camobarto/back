﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class FormaPagoRelacionMapa : EntityTypeConfiguration<FormaPagoRelacion>
    {
        public FormaPagoRelacionMapa()
        {
            HasKey(p => p.IdFormaPagoRelacion);

            ToTable("FORMA_PAGO_RELACION");
            Property(p => p.IdFormaPagoRelacion).HasColumnName("IdFormaPagoRelacion");
            Property(p => p.IdTipoCredito).HasColumnName("IdTipoCredito");
            Property(p => p.IdPapelCavipetrol).HasColumnName("IdPapelCavipetrol");
            Property(p => p.IdFormaPago).HasColumnName("IdFormaPago");
            Property(p => p.Activo).HasColumnName("Activo");
            Property(p => p.Clave).HasColumnName("Clave");
            Property(p => p.Mesada14).HasColumnName("Mesada14");
        }
    }
}
