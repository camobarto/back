﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class MesesMapa : EntityTypeConfiguration<Meses>
    {
        public MesesMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_MESES");
            Property(p => p.Id).HasColumnName("ID_MES");
            Property(p => p.Mes).HasColumnName("MES").HasMaxLength(200);
        }
    }
}
