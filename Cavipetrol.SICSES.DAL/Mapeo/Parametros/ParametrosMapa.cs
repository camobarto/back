﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cavipetrol.SICSES.Infraestructura.Model;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class ParametrosMapa : EntityTypeConfiguration<Cavipetrol.SICSES.Infraestructura.Model.Parametros>
    {
        public ParametrosMapa()
        {
            HasKey(p => p.Nombre);
            ToTable("PARAMETROS");
            Property(p => p.FechaInicio).HasColumnName("FECHA_INICIO");
            Property(p => p.FechaFin).HasColumnName("FECHA_FIN");
            Property(p => p.Nombre).HasColumnName("NOMBRE");
            Property(p => p.Valor).HasColumnName("VALOR");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCIÓN");
        }
    }
}
