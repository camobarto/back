﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoContratoMapa : EntityTypeConfiguration<TipoContrato>
    {
        public TipoContratoMapa()
        {
            HasKey(p => p.IdContrato);

            ToTable("SICS_TIPOS_CONTRATOS");
            Property(p => p.IdContrato).HasColumnName("ID_CONTRATO");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(30);
        }
    }
}
