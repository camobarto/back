﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoRolMapa : EntityTypeConfiguration<TipoRol>
    {
        public TipoRolMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_ROLES");
            Property(p => p.Id).HasColumnName("ID_ROL");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(200);
        }
    }
}
