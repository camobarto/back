﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class AsociacionEntidadSolidariaMapa : EntityTypeConfiguration<AsociacionEntidadSolidaria>
    {
        public AsociacionEntidadSolidariaMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_ASOCIACIONES_ENTIDAD_SOLIDARIA");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(255);
            Property(p => p.Id).HasColumnName("ID_ASOCIACIONES_ENTIDAD");
        }
    }
}
