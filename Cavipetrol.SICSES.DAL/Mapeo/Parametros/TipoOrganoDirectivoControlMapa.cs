﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoOrganoDirectivoControlMapa : EntityTypeConfiguration<TipoOrganoDirectivoControl>
    {
        public TipoOrganoDirectivoControlMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_ORGANOS_DIRECTIVO_CONTROL");
            Property(p => p.Id).HasColumnName("ID_DIRECTIVO_CONTROL");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(200);
            Property(p => p.EquivalenteCavipetrol).HasColumnName("EQUIVALENTE_CAVIPETROL").HasMaxLength(200);
            Property(p => p.NumeroPrincipales).HasColumnName("NUMERO_PRINCIPALES");
            Property(p => p.NumeroSuplentes).HasColumnName("NUMERO_SUPLENTES");
            Property(p => p.TipoPersona).HasColumnName("TIPO_PERSONA").HasMaxLength(20);
        }
    }
}
