﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    class TiposPagareTiposConceptoRelacionMapa : EntityTypeConfiguration<TiposPagareTiposConceptoRelacion>
    {
        public TiposPagareTiposConceptoRelacionMapa()
        {
            HasKey(p => p.IdTipoPagareTipoConcepto);

            ToTable("TIPOS_PAGARE_TIPOS_CONCEPTO_RELACION");
            Property(p => p.IdTipoPagareTipoConcepto).HasColumnName("IdTipoPagareTipoConcepto");
            Property(p => p.IdPagare).HasColumnName("IdPagare");
            Property(p => p.IdConceptoGarantia).HasColumnName("IdConceptoGarantia");
        }
    }
}
