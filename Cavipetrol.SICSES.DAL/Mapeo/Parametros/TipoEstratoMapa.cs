﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoEstratoMapa : EntityTypeConfiguration<TipoEstrato>
    {
        public TipoEstratoMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPOS_ESTRATO");
            Property(p => p.Id).HasColumnName("ID_ESTRATO");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(30);
        }
    }
}
