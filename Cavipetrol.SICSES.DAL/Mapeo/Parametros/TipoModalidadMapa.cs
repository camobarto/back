﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class TipoModalidadMapa : EntityTypeConfiguration<TipoModalidad>
    {
        public TipoModalidadMapa()
        {
            HasKey(p => p.Id);

            ToTable("SICS_TIPO_MODALIDAD");
            Property(p => p.Id).HasColumnName("ID_MODALIDAD");
            Property(p => p.Descripcion).HasColumnName("DESCRIPCION").HasMaxLength(80);
        }
    }
}
