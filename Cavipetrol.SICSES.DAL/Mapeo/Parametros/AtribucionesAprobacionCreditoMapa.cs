﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class AtribucionesAprobacionCreditoMapa : EntityTypeConfiguration<AtribucionesAprobacionCredito>
    {
        public AtribucionesAprobacionCreditoMapa()
        {
            HasKey(p => p.IdAtribucionAprobacion);

            ToTable("ATRIBUCIONES_APROBACION_CREDITO");
            Property(p => p.IdAtribucionAprobacion).HasColumnName("IdAtribucionAprobacion");
            Property(p => p.IdPerfil).HasColumnName("IdPerfil");
            Property(p => p.Maximo).HasColumnName("Maximo");
            Property(p => p.Consolidado).HasColumnName("Consolidado");
        }
    }
}
