﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class BimestreMapa : EntityTypeConfiguration<Bimestre>
    {
        public BimestreMapa()
        {
            HasKey(p => p.Id);

            ToTable("BIMESTRE");
            Property(p => p.Id).HasColumnName("id_Bimestre");
            Property(p => p.Descripcion).HasColumnName("Nombre_Bimestre");
        }
    }
}

