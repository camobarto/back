﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class FormaPagoMapa : EntityTypeConfiguration<FormaPago>
    {
        public FormaPagoMapa()
        {
            HasKey(p => p.IdFormaPago);

            ToTable("FORMA_PAGO");
            Property(p => p.IdFormaPago).HasColumnName("IdFormaPago");
            Property(p => p.Nombre).HasColumnName("Nombre");
            Property(p => p.Descripcion).HasColumnName("Descripcion");            
            
        }
    }
}
