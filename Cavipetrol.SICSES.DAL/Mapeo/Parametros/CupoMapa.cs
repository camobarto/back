﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Parametros
{
    public class CupoMapa : EntityTypeConfiguration<Cupo>
    {
        public CupoMapa()
        {
            HasKey(p => p.IdCupo);

            ToTable("CUPO");
            Property(p => p.IdCupo).HasColumnName("IdCupo");
            Property(p => p.IdTipoCredito).HasColumnName("IdTipoCredito");
            Property(p => p.IdTipoCupo).HasColumnName("IdTipoCupo");
            Property(p => p.AntiguedadInicial).HasColumnName("AntiguedadInicial");
            Property(p => p.AntiguedadFinal).HasColumnName("AntiguedadFinal");
            Property(p => p.Valor).HasColumnName("Valor");

        }
    }
}
