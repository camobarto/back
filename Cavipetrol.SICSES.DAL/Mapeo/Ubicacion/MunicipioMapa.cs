﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Ubicacion
{
    public class MunicipioMapa : EntityTypeConfiguration<Municipio>
    {
        public MunicipioMapa()
        {
            HasKey(p => new { p.Id, p.IdDepartamento });

            ToTable("MUNICIPIOS");
            Property(p => p.Id).HasColumnName("MNC_ID");
            Property(p => p.IdDepartamento).HasColumnName("DPR_ID");
            Property(p => p.Nombre).HasColumnName("MNC_NOMBRE");

            HasRequired(p => p.Departamento).WithMany(m => m.Municipios).HasForeignKey(p => p.IdDepartamento);
        }
    }
}
