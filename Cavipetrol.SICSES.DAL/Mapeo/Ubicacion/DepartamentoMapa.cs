﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Ubicacion
{
    public class DepartamentoMapa : EntityTypeConfiguration<Departamento>
    {
        public DepartamentoMapa()
        {
            HasKey(p => p.Id);

            ToTable("DEPARTAMENTOS");
            Property(p => p.Id).HasColumnName("DPR_ID");
            Property(p => p.Nombre).HasColumnName("DPR_NOMBRE");
        }
    }
}
