﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Reportes
{
      public class ReporteRiesgoLiquidez : EntityTypeConfiguration<EvaluacionRiesgoLiquidez>
    {
        public ReporteRiesgoLiquidez()
        {
            HasKey(p => p.ID_EVALUACION_RIESGO_LIQUIDEZ);

            ToTable("EVALUACION_RIESGO_LIQUIDEZ");
            Property(p => p.CODIGO_RENGLON).HasColumnName("CODIGO_RENGLON");
            Property(p => p.UNIDAD_CAPTURA).HasColumnName("UNIDAD_CAPTURA");
            Property(p => p.DESCRIPCION_RENGLON).HasColumnName("DESCRIPCION_RENGLON").HasMaxLength(200);
            Property(p => p.SALDO_A_LA_FECHA).HasColumnName("SALDO_A_LA_FECHA");
            Property(p => p.PRIMER_MES).HasColumnName("PRIMER_MES");
            Property(p => p.DE_1_A_2_MESES).HasColumnName("DE_1_A_2_MESES");
            Property(p => p.DE_2_A_3_MESES).HasColumnName("DE_2_A_3_MESES");
            Property(p => p.DE_3_A_6_MESES).HasColumnName("DE_3_A_6_MESES");
            Property(p => p.DE_6_A_9_MESES).HasColumnName("DE_6_A_9_MESES");
            Property(p => p.DE_9_A_12_MESES).HasColumnName("DE_9_A_12_MESES");
            Property(p => p.MAYOR_DE_12_MESES).HasColumnName("MAYOR_DE_12_MESES");
            Property(p => p.ID_EVALUACION_RIESGO_LIQUIDEZ).HasColumnName("ID_EVALUACION_RIESGO_LIQUIDEZ");




            //HasRequired(p => p.ParametroCavipetrol).WithMany(p => p.ReportesIdentificacion).HasForeignKey(p => p.IdParametroCavipetrol);
        }
    }
}
