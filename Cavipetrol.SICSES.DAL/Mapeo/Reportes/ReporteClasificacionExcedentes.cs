﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.Model;

namespace Cavipetrol.SICSES.DAL.Mapeo.Reportes
{
    

    public class ReporteClasificacionExcedentes : EntityTypeConfiguration<ClasificacionExcedentes>
    {
        public ReporteClasificacionExcedentes()
        {
            HasKey(p => p.CODIGO_RENGLON);

            ToTable("DESCRIPCION_EXCEDENTES");
            Property(p => p.CODIGO_RENGLON).HasColumnName("CODIGO_RENGLON");
            Property(p => p.UNIDAD_CAPTURA).HasColumnName("UNIDAD_CAPTURA");
            Property(p => p.DESCRIPCION_RENGLON).HasColumnName("DESCRIPCION_RENGLON").HasMaxLength(200);
            Property(p => p.SALDO).HasColumnName("SALDO");
         


            //HasRequired(p => p.ParametroCavipetrol).WithMany(p => p.ReportesIdentificacion).HasForeignKey(p => p.IdParametroCavipetrol);
        }
    }
}
