﻿using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Reportes
{
    public class ReporteIdentificacionMapa : EntityTypeConfiguration<ReporteIdentificacion>
    {
        public ReporteIdentificacionMapa()
        {            
            HasKey(p => p.IdReporteIdentificacion);

            ToTable("HST_REPORTE_IDENTIFICACION");
            Property(p => p.IdReporteIdentificacion).HasColumnName("ID_REPORTE_IDENTIFICACION");
            Property(p => p.IdParametroCavipetrol).HasColumnName("ID_PARAMETROS_CAVI");
            Property(p => p.Categoria).HasColumnName("CATEGORIA").HasMaxLength(30).IsRequired();
            Property(p => p.Sector).HasColumnName("SECTOR").HasMaxLength(20);
            Property(p => p.Grado).HasColumnName("GRADO").HasMaxLength(20);
            Property(p => p.TipoEntidad).HasColumnName("ENTIDAD_TIPO").HasMaxLength(80).IsRequired();
            Property(p => p.ClasificacionCooperativas).HasColumnName("CLASIFICACION_COOPERATIVAS").HasMaxLength(100);
            Property(p => p.ActividadEconomica).HasColumnName("ACTIVIDAD_ECONOMICA").HasMaxLength(100);
            Property(p => p.ClasificacionEconomica).HasColumnName("CLASIFICACION_ECONOMICA_CIIU").HasMaxLength(500);
            Property(p => p.NumeroResolucionActividadFinanciera).HasColumnName("NUMERO_RES_ACTIVIDAD_FINANCIERA");
            Property(p => p.FechaResolucionActvidadFinanciera).HasColumnName("FECHA_RES_FINANCIERA");
            Property(p => p.NumeroInscripcionFOAGCOOP).HasColumnName("NUMERO_RES_INSCRIPCION_FOGACOOP");
            Property(p => p.FechaResolucionFOAGCOOP).HasColumnName("FECHA_RES_FAGACOOP");
            Property(p => p.NumeroResolucionDesmonte).HasColumnName("NUMERO_RES_DESMONTE");
            Property(p => p.FechaResolucionDesmonte).HasColumnName("FECHA_RES_DESMONTE");
            Property(p => p.PreCooperativa).HasColumnName("PRECOOPERATIVA");
            Property(p => p.ClaseTransporte).HasColumnName("CLASE_TRANSPORTE").HasMaxLength(40);
            Property(p => p.SubClaseTransporte).HasColumnName("SUBCLASE_TRANSPORTE").HasMaxLength(60);
            Property(p => p.SenalVinculo).HasColumnName("SENAL_VINCULO").HasMaxLength(200);
            Property(p => p.RegistroCamaraComercio).HasColumnName("REGISTRO_CAMARA_COMERCIO");
            Property(p => p.Estado).HasColumnName("ESTADO").HasMaxLength(80);
            Property(p => p.FechaLiquidacion).HasColumnName("FECHA_LIQ");
            Property(p => p.NumeroResolucion).HasColumnName("NUMERO_RES");
            Property(p => p.FechaResolucionLiquidacion).HasColumnName("FECHA_RES_LIQ");
            Property(p => p.CompaniaAseguradora).HasColumnName("COMPANIA_ASEGURADORA").HasMaxLength(80);
            Property(p => p.FechaProximaRenovacion).HasColumnName("FECHA_PROXIMA_RENOVACION");
            Property(p => p.AsosiacionAfiliada).HasColumnName("ASOCIACION_ENCUENTRA_AFILIADA_ENT").HasMaxLength(250);
            Property(p => p.ResolucionMinTransporte).HasColumnName("RESOLUCION_MINTRAS");
            Property(p => p.CapacidadTransporteMinima).HasColumnName("CAPACIDAD_TRANS_MIN");
            Property(p => p.CapacidadTransporteMaxima).HasColumnName("CAPACIDAD_TRANS_MAX");

            HasRequired(p => p.ParametroCavipetrol).WithMany(p => p.ReportesIdentificacion).HasForeignKey(p => p.IdParametroCavipetrol);
        }
    }
}
