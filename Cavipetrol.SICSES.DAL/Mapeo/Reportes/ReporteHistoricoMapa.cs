﻿using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Reportes
{
    public class ReporteHistoricoMapa : EntityTypeConfiguration<ReporteHistorico>
    {
        public ReporteHistoricoMapa()
        {            
            HasKey(p => p.IdReporteHistorico);

            ToTable("HISTORICO_REPORTES_SICSES");
            Property(p => p.IdReporteHistorico).HasColumnName("ID_REPORTE_HISTORICO");
            Property(p => p.NumeroReporte).HasColumnName("NUMERO_REPORTE");
            Property(p => p.NombreReporte).HasColumnName("NOMBRE_REPORTE").HasMaxLength(200);
            Property(p => p.DatosReporte).HasColumnName("DATOS_REPORTE").HasColumnType("nvarchar(max)");
            Property(p => p.Mes).HasColumnName("MES").HasMaxLength(20);
            Property(p => p.Ano).HasColumnName("AÑO").HasMaxLength(4);
            Property(p => p.FechaReporte).HasColumnName("FECHA_REPORTE");


            //HasRequired(p => p.ParametroCavipetrol).WithMany(p => p.ReportesIdentificacion).HasForeignKey(p => p.IdParametroCavipetrol);
        }
    }
}
