﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cavipetrol.SICSES.DAL.Mapeo.Reportes
{
    public class ParametroCavipetrolMapa :  EntityTypeConfiguration<ParametroUsuarioCavipetrol>
    {
        public ParametroCavipetrolMapa()
        {
            
            HasKey(p => p.Id);

            ToTable("PARAMETROS_CAVIPETROL");
            Property(p => p.Id).HasColumnName("ID_PARAMETROS_CAVI");
            Property(p => p.Codigo).HasColumnName("CODIGO");
            Property(p => p.GrupoNormaTecnica).HasColumnName("GRUPO_NORMA_TECNICA");
            Property(p => p.Nombre).HasColumnName("NOMBRE_ENTIDAD").HasMaxLength(250).IsRequired();
            Property(p => p.Sigla).HasColumnName("SIGLA").HasMaxLength(30).IsRequired();
            Property(p => p.NIT).HasColumnName("NIT").HasMaxLength(80).IsRequired();
            Property(p => p.Direccion).HasColumnName("DIRECCION").HasMaxLength(80).IsRequired();
            Property(p => p.Telefono).HasColumnName("TELEFONO");
            Property(p => p.Fax).HasColumnName("FAX");
            Property(p => p.FechaConstitucion).HasColumnName("FECHA_CONSTITUCION");
            Property(p => p.IdMunicipio).HasColumnName("MNC_ID").HasMaxLength(3).IsRequired();
            Property(p => p.IdDepartamento).HasColumnName("DPR_ID").HasMaxLength(2).IsRequired();
        }
    }
}
