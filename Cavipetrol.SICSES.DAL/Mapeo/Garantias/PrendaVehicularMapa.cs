﻿using Cavipetrol.SICSES.Infraestructura.Model.Garantias;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Cavipetrol.SICSES.DAL.Mapeo.Garantias
{
    public class PrendaVehicularMapa : EntityTypeConfiguration<PrendaVehicular>
    {
        public PrendaVehicularMapa()
        {
            HasKey(p => p.NumeroConsecutivo);

            ToTable("PRENDA_VEHICULAR");
            Property(p => p.NumeroConsecutivo).HasColumnName("PV_NumeroConsecutivo");
            Property(p => p.TipoNit).HasColumnName("PV_TipoNit");
            Property(p => p.NumNit).HasColumnName("PV_NumNit");
            Property(p => p.NombreDeudor).HasColumnName("PV_NombreDeudor");
            Property(p => p.ValorPrestamo).HasColumnName("PV_ValorPrestamo");
            Property(p => p.Vendedor).HasColumnName("PV_Vendedor");
            Property(p => p.NitVendedor).HasColumnName("PV_NitVendedor");
            Property(p => p.Placa).HasColumnName("PV_Placa");
            Property(p => p.Chasis).HasColumnName("PV_Chasis");
            Property(p => p.Motor).HasColumnName("PV_Motor");
            Property(p => p.ClaseVeh).HasColumnName("PV_ClaseVeh");
            Property(p => p.Modelo).HasColumnName("PV_Modelo");
            Property(p => p.Marca).HasColumnName("PV_Marca");
            Property(p => p.Linea).HasColumnName("PV_Linea");
            Property(p => p.Serie).HasColumnName("PV_Serie");
            Property(p => p.Servicio).HasColumnName("PV_Servicio");
            Property(p => p.VIN).HasColumnName("PV_Vin");
            Property(p => p.Color).HasColumnName("PV_Color");
            Property(p => p.DireccionDeudor).HasColumnName("PV_DireccionDeudor");
            Property(p => p.Barrio).HasColumnName("PV_Barrio");
            Property(p => p.CiudadDeudor).HasColumnName("PV_CiudadDeudor");
            Property(p => p.Estado).HasColumnName("PV_Estado");
        }
    }
}