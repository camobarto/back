﻿using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Asegurabilidad
{
    public class TipoPolizaMapa : EntityTypeConfiguration<TipoPoliza>
    {
        public TipoPolizaMapa()
        {
            HasKey(p => p.id_TipoPoliza);

            ToTable("TABLAS_TIPO_POLIZA");
            Property(p => p.id_TipoPoliza).HasColumnName("TP_Id");
            Property(p => p.Nombre_Poliza).HasColumnName("TP_Descripción");

        }
    }
}


