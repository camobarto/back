﻿using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Asegurabilidad
{
    class AsegurabilidadMapa : EntityTypeConfiguration<AsegurabilidadModel>
    {
        public AsegurabilidadMapa()
        {
            HasKey(p => p.id);

            ToTable("Asegurabilidad");
            Property(p => p.id).HasColumnName("id");
            Property(p => p.Reporte_deudores).HasColumnName("Reporte_deudores");
            Property(p => p.nombre).HasColumnName("nombre");
            Property(p => p.nit).HasColumnName("nit");
            Property(p => p.Ciudad_Residencia).HasColumnName("Ciudad_Residencia");
            Property(p => p.Dirreccion_Residencia).HasColumnName("Dirreccion_Residencia");
            Property(p => p.Telefono_Residencia).HasColumnName("Telefono_Residencia");
            Property(p => p.Ciudad_oficina).HasColumnName("Ciudad_oficina");
            Property(p => p.Dirreccion_oficina).HasColumnName("Dirreccion_oficina");
            Property(p => p.TelefonoOficina).HasColumnName("TelefonoOficina");
            Property(p => p.Correo_Electronico_oficina).HasColumnName("Correo_Electronico_oficina");
            Property(p => p.CorreoElectronico_residencia).HasColumnName("CorreoElectronico_residencia");
            Property(p => p.Fecha_nacimiento).HasColumnName("Fecha_nacimiento");
            Property(p => p.Documento_ciudad).HasColumnName("Documento_ciudad");
            //Property(p => p.Saldo).HasColumnName("Saldo");
            Property(p => p.Control_inclusiones_cavipetrol).HasColumnName("Control_inclusiones_cavipetrol");
            Property(p => p.Cedula).HasColumnName("Cedula");
            Property(p => p.Mes_inc).HasColumnName("Mes_inc");
            Property(p => p.UltimoMes_reporte).HasColumnName("UltimoMes_reporte");
            Property(p => p.interrupcion).HasColumnName("interrupcion");
            Property(p => p.mes_inclusion).HasColumnName("mes_inclusion");
            Property(p => p.Va_por_Refinanciacion).HasColumnName("Va_por_Refinanciacion");
            Property(p => p.Cumulo_Refinan).HasColumnName("Cumulo_Refinan");
            Property(p => p.Etiquetas_de_fila).HasColumnName("Etiquetas_de_fila");
            Property(p => p.Continuidad).HasColumnName("Continuidad");
            Property(p => p.Di_17).HasColumnName("Di_17");
            Property(p => p.Rechazo_Aseguradora_anterior).HasColumnName("Rechazo_Aseguradora_anterior");
            Property(p => p.Tipologia).HasColumnName("Tipologia");
            Property(p => p.Declaracion).HasColumnName("Declaracion");

            Property(p => p.Vr_asegurado_mes_ant).HasColumnName("Vr_asegurado_mes_ant");
            Property(p => p.Observacion_mes_anterior).HasColumnName("Observacion_mes_anterior");
            Property(p => p.Porcentaje_extraprima).HasColumnName("Porcentaje_extraprima");
            Property(p => p.Concepto_Extraprima).HasColumnName("Concepto_Extraprima");
            Property(p => p.Calculo).HasColumnName("Calculo");


            Property(p => p.Edad_al_ingreso).HasColumnName("Edad_al_ingreso");
            Property(p => p.Tasa_por_mil_anual).HasColumnName("Tasa_por_mil_anual");
            Property(p => p.Limite_asegurado_segun_edad).HasColumnName("Limite_asegurado_segun_edad");
            Property(p => p.Aplica_cumulo).HasColumnName("Aplica_cumulo");
            Property(p => p.Valor_asegurado).HasColumnName("Valor_asegurado");
            Property(p => p.Valor_sin_cobertura).HasColumnName("Valor_sin_cobertura");
            Property(p => p.Prima_mensual).HasColumnName("Prima_mensual");
            Property(p => p.Valor_extraprima).HasColumnName("Valor_extraprima");
            Property(p => p.Prima_mensual_Extraprima).HasColumnName("Prima_mensual_Extraprima");
            Property(p => p.Retorno).HasColumnName("Retorno");

            

        }
    }

   
}

