﻿using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Asegurabilidad
{
   public class AseguradorasMapa : EntityTypeConfiguration<Aseguradoras>
    {
        public AseguradorasMapa()
        {
            HasKey(p => p.id_aseguradora);

            ToTable("TABLAS_ASEGURADORAS");
            Property(p => p.id_aseguradora).HasColumnName("TA_ID_ASEGURADORA");
            Property(p => p.Nombre_aseguradora).HasColumnName("TA_ASEGURADORA");
            Property(p => p.Tipo_nit).HasColumnName("TA_TIPONIT");
            Property(p => p.Numero_nit).HasColumnName("TA_NUMNIT");
            Property(p => p.Nombre_Aseguradora_vida).HasColumnName("TA_ASEGURADORA_VIDA");

        }
    }
}
