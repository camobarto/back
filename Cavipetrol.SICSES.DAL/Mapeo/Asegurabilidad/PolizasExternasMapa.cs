﻿using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL.Mapeo.Asegurabilidad
{
    public class PolizasExternasMapa : EntityTypeConfiguration<PolizasExternas>
    {
        public PolizasExternasMapa()
        {
            HasKey(p => new { p.Cedula,p.Tipo_documento,p.Numero_Poliza,p.id_Aseguradora });
        
            ToTable("TABLAS_POLIZA_EXTERNA");
            Property(p => p.Oficina).HasColumnName("TSE_Oficina");
            Property(p => p.Tipo_documento).HasColumnName("TSE_TipoNit");
            Property(p => p.Cedula).HasColumnName("TSE_NumNit");
            Property(p => p.Nombre_Apellidos).HasColumnName("TSE_Nombre_Apellidos");
            Property(p => p.id_Aseguradora).HasColumnName("TSE_Aseguradora");
            Property(p => p.Numero_Poliza).HasColumnName("TSE_NumPoliza");
            Property(p => p.Valor_Asegurado).HasColumnName("TSE_ValorAsegura");
            Property(p => p.Tomador).HasColumnName("TSE_NombreTomaPoliza"); 
            Property(p => p.TipoNitTomador).HasColumnName("TSE_TipoNitTomador");
            Property(p => p.Nit_Tomador).HasColumnName("TSE_NumNitTomador");
            Property(p => p.Vigencia_Desde).HasColumnName("TSE_VigenteDesde");
            Property(p => p.Vigencia_Hasta).HasColumnName("TSE_VigenteHasta");
            Property(p => p.Direccion).HasColumnName("TSE_Direccion");
            Property(p => p.Matricula_Inmobiliaria).HasColumnName("TSE_Matricula");
            Property(p => p.Observaciones).HasColumnName("TSE_Observaciones");
            Property(p => p.Tipo_poliza).HasColumnName("TSE_TipoPoliza");
            Property(p => p.Aplicar_Colectiva).HasColumnName("TSE_AplicarColectiva");
            Property(p => p.Activo).HasColumnName("TSE_ACTIVO");





        }
    }

}
