﻿using Cavipetrol.SICSES.Infraestructura.Model.Sap;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL
{
    public partial class SapContexto : DbContext
    {
        public SapContexto(string db = "name= SAP") : base(db)
        {

        }

        public virtual ObjectResult<AsiSoporte> ObtenerAsiSoporte()
        {

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<AsiSoporte>("dbo.PA_CONSULTAR_ASISOPORTE");
        }

        public virtual ObjectResult<Asientos> ConsultarAsientos(string AsiSoporte, string AsiConsecutivo, string Asioperacion, string AsiOpeConsecutivo, string Fecha, string Ciudad, string MensajeError)
        {
            //DateTime? fechaE = null ;
            
            //if (Fecha != "")
            //{
            //    fechaE = Convert.ToDateTime(Fecha);
            //}
         
            SqlParameter param1 = AsiSoporte == null ?  new SqlParameter("@AsiSoporte", DBNull.Value) : new SqlParameter("@AsiSoporte", AsiSoporte);
            SqlParameter param2 = AsiConsecutivo == null ? new SqlParameter("@AsiConsecutivo", DBNull.Value) : new SqlParameter("@AsiConsecutivo", AsiConsecutivo);
            SqlParameter param3 = Asioperacion == null ? new SqlParameter("@Asioperacion", DBNull.Value) : new SqlParameter("@Asioperacion", Asioperacion);
            SqlParameter param4 = AsiOpeConsecutivo == null ? new SqlParameter("@AsiOpeConsecutivo", DBNull.Value) : new SqlParameter("@AsiOpeConsecutivo", AsiOpeConsecutivo);
            SqlParameter param5 = Fecha == null ? new SqlParameter("@Fecha", DBNull.Value) : new SqlParameter("@Fecha", Fecha);
            SqlParameter param6 = Ciudad == null ? new SqlParameter("@Ciudad", DBNull.Value) : new SqlParameter("@Ciudad", Ciudad);
            SqlParameter param7 = MensajeError == null ? new SqlParameter("@MensajeError", DBNull.Value) : new SqlParameter("@MensajeError", MensajeError);



            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Asientos>("dbo.PA_CONSULTA_ERROR_ASIENTOS @AsiSoporte,@AsiConsecutivo,@Asioperacion,@AsiOpeConsecutivo,@Fecha,@Ciudad,@MensajeError", @param1, @param2, @param3, @param4, @param5, @param6, @param7);

        }

        public virtual ObjectResult<string> ActualizarAsiento()
        {
             return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<string>("dbo.PA_ACTUALIZAR_ASIENTO_ERROR");
        }

        public virtual ObjectResult<Proveedores> ConsultaProveedores(string CardCode, string CardName)
        {
            

            SqlParameter param1 = CardCode == null ? new SqlParameter("@CardCode", DBNull.Value) : new SqlParameter("@CardCode", CardCode);
            SqlParameter param2 = CardName == null ? new SqlParameter("@CardName", DBNull.Value) : new SqlParameter("@CardName", CardName);


            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Proveedores>("dbo.PA_CONSULTA_PROVEEDORES_ADMINISTRACION @CardCode,@CardName", @param1, @param2);

        }

        public virtual ObjectResult<string> ActualizarEstadoProveedores()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<string>("dbo.PA_ACTUALIZAR_PROVEEDOR_ERROR");
        }
        public virtual ObjectResult<MensajeErrorAsientos> ConsultarMensajeError()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<MensajeErrorAsientos>("dbo.PA_CONSULTAR_MENSAJE_ERROR_ASIENTOS");
        }
    }
}
