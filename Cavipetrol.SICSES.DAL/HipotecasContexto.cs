﻿using Cavipetrol.SICSES.DAL.Mapeo.Hipotecas;
using Cavipetrol.SICSES.Infraestructura.Model.Hipotecas;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL
{
    public partial class HipotecasContexto : DbContext
    {
        public HipotecasContexto(string db = "name= SICAV") : base(db)
        {

        }
        public DbSet<Hipoteca> DatosHipoteca { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new HipotecaMapa());
        }

        public virtual void ActualizaHipotecas(int IdHipoteca, string AudUsuarioModificacion, string FechaEscrituracion, string NumeroEscritura, string valorAvaluo, string VigenciaInicial, string VigenciaFinal, string NumeroNotaria = "", string Oficina = "")
        {
            DateTime fechaEscrituracion = new DateTime(1900, 01, 01);
            DateTime vigenciaInicial = new DateTime(1900, 01, 01);
            DateTime vigenciaFinal = new DateTime(1900, 01, 01);

            DateTime.TryParse(FechaEscrituracion, out fechaEscrituracion);
            DateTime.TryParseExact(VigenciaInicial, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out vigenciaInicial);
            DateTime.TryParseExact(VigenciaFinal, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out vigenciaFinal);


            SqlParameter param1 = new SqlParameter("@IdHipoteca", IdHipoteca);

            SqlParameter param2 = new SqlParameter("@AudUsuarioModificacion", @AudUsuarioModificacion);

            SqlParameter param3 = fechaEscrituracion <= new  DateTime(1900, 01, 01) ?
            new SqlParameter("@FechaEscrituracion", DBNull.Value) :
            new SqlParameter("@FechaEscrituracion", FechaEscrituracion);

            SqlParameter param4 = NumeroEscritura == null ?
            new SqlParameter("@NumeroEscritura", DBNull.Value) :
            new SqlParameter("@NumeroEscritura", NumeroEscritura);

            SqlParameter param5 = valorAvaluo == null ?
            new SqlParameter("@ValorAvaluo", DBNull.Value) :
            new SqlParameter("@ValorAvaluo", valorAvaluo);

            SqlParameter param6 = vigenciaInicial <= new DateTime(1900, 01, 01) ?
            new SqlParameter("@VigenciaInicial", DBNull.Value) :
            new SqlParameter("@VigenciaInicial", vigenciaInicial);

            SqlParameter param7 = vigenciaFinal <= new DateTime(1900, 01, 01) ?
            new SqlParameter("@VigenciaFinal", DBNull.Value) :
            new SqlParameter("@VigenciaFinal", vigenciaFinal);

            SqlParameter param8 = NumeroNotaria == null ?
            new SqlParameter("@NumeroNotaria", DBNull.Value) :
            new SqlParameter("@NumeroNotaria", NumeroNotaria);

            SqlParameter param9 = Oficina == null ?
            new SqlParameter("@Oficina", DBNull.Value) :
            new SqlParameter("@Oficina", Oficina);


            ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("dbo.PA_ACTUALIZAR_HIPOTECA @IdHipoteca,@AudUsuarioModificacion,@FechaEscrituracion,@NumeroEscritura,@ValorAvaluo,@VigenciaInicial,@VigenciaFinal,@NumeroNotaria,@Oficina", @param1, @param2, @param3, @param4,@param5, @param6, @param7, @param8, @param9);
            // ExecuteStoreCommand
        }

    }
}
