﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos.Consulta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.ComponentModel;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;

namespace Cavipetrol.SICSES.DAL
{
    public partial class CreditosContexto : DbContext
    {
        string DB = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;
        public CreditosContexto(string db = "name= REPORTES_SICSES") : base(db)
        {

        }

        public virtual ObjectResult<InformacionSolicitante> ObtenerInformacionEmpleado(string numeroIdentificacion, string tipoIdentificacion)
        {
            SqlParameter param1 = new SqlParameter("@DOCUMENTO_IDENTIFICACION", numeroIdentificacion);
            SqlParameter param2 = new SqlParameter("@TIPO_IDENTIFICACION", tipoIdentificacion);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<InformacionSolicitante>("dbo.PA_CONSULTAR_INFORMACION_USUARIO_SOLICITANTE_CREDITO @DOCUMENTO_IDENTIFICACION, @TIPO_IDENTIFICACION", @param1, param2);
        }
        /*saldos*/
        public virtual ObjectResult<ReportesSaldos> ReporteSaldosDiarios(string selectedFechaInicio, string selectedFechaFin)
        {
            SqlParameter param1 = new SqlParameter("@FECHA_INICIAL", selectedFechaInicio);
            SqlParameter param2 = new SqlParameter("@FECHA_FINAL", selectedFechaFin);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReportesSaldos>("dbo.PA_INFORME_SALDO_DIARIO_FAI @FECHA_INICIAL, @FECHA_FINAL", @param1, @param2);
        }
        public virtual ObjectResult<InformacionSolicitanteCampanaEspecial> ObtenerInformacionSolicitanteCampanaEspecial(string numeroIdentificacion, string tipoIdentificacion)
        {
            SqlParameter param1 = new SqlParameter("@DOCUMENTO_IDENTIFICACION", numeroIdentificacion);
            SqlParameter param2 = new SqlParameter("@TIPO_IDENTIFICACION", tipoIdentificacion);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<InformacionSolicitanteCampanaEspecial>("dbo.PA_CONSULTAR_INFORMACION_USUARIO_SOLICITANTE_CREDITO_ESPECIAL @DOCUMENTO_IDENTIFICACION, @TIPO_IDENTIFICACION", @param1, param2);
        }
        public virtual ObjectResult<InformacionSolicitante> ObtenerInformacionAsociado(string numeroIdentificacion, string tipoIdentificacion)
        {
            try
            {
                var @params = new[]{
                   new SqlParameter("@DOCUMENTO_IDENTIFICACION", numeroIdentificacion),
                   new SqlParameter("@TIPO_IDENTIFICACION", tipoIdentificacion)
                };

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<InformacionSolicitante>("dbo.PA_CONSULTAR_INFORMACION_ASOCIADO_SOLICITANTE_CREDITO @DOCUMENTO_IDENTIFICACION @TIPO_IDENTIFICACION", @params);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual ObjectResult<InformacionAsegurabilidad> ObtenerInformacionAsegurabilidad(string numeroIdentificacion, string tipoIdentificacion)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@DOCUMENTO_IDENTIFICACION", numeroIdentificacion);
                SqlParameter param2 = new SqlParameter("@TIPO_IDENTIFICACION", tipoIdentificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<InformacionAsegurabilidad>("dbo.PA_CONSULTAR_INFORMACION_ASEGURABILIDAD_SOLICITANTE_CREDITO @DOCUMENTO_IDENTIFICACION, @TIPO_IDENTIFICACION", @param1, @param2);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual ObjectResult<ValidacionesSolicitudCreditoUsuario> ObternerValidacionesSolicitudCredito(string numeroIdentificacion, string tipoIdentificacion)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@DOCUMENTO_IDENTIFICACION", numeroIdentificacion);
                SqlParameter param2 = new SqlParameter("@TIPO_IDENTIFICACION", tipoIdentificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ValidacionesSolicitudCreditoUsuario>("dbo.PA_CONSULTAR_INFORMACION_USARIO_REPORTADO_EMBARGADO @DOCUMENTO_IDENTIFICACION, @TIPO_IDENTIFICACION", @param1, @param2);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual ObjectResult<SaldosColocacion> ObtenerCreditosDisponiblesSaldo(string numeroIdentificacion, string tipoIdentificacion, string papel)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@DOCUMENTO_IDENTIFICACION", numeroIdentificacion);
                SqlParameter param2 = new SqlParameter("@TIPO_IDENTIFICACION", tipoIdentificacion);
                SqlParameter param3 = new SqlParameter("@PAPEL", papel);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SaldosColocacion>("dbo.PA_CONSULTAR_CREDITOS_DISPONIBLES_SALDO @DOCUMENTO_IDENTIFICACION, @TIPO_IDENTIFICACION, @PAPEL", @param1, @param2, @param3);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual ObjectResult<NormaProductos> ObtenerInformacionNormas(string producto, string papel, short idTipoCredito)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@PRODUCTO", producto);
                SqlParameter param2 = new SqlParameter("@PAPEL", papel);
                SqlParameter param3 = idTipoCredito != 0 ?
                new SqlParameter("@IdTipoCredito", idTipoCredito) :
                new SqlParameter("@IdTipoCredito", DBNull.Value);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<NormaProductos>("dbo.PA_CONSULTAR_NORMAS_POR_PRODUCTO_PAPEL @PRODUCTO, @PAPEL, @IdTipoCredito", @param1, @param2, @param3);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual ObjectResult<ValidacionesSolicitudCreditoProducto> ObtenerRestriccionesProducto(string identificacion, string tipoIdentificacion, string papel, string nombreProducto)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@DOCUMENTO_IDENTIFICACION", identificacion);
                SqlParameter param2 = new SqlParameter("@TIPO_IDENTIFICACION", tipoIdentificacion);
                SqlParameter param3 = new SqlParameter("@PAPEL", papel);
                SqlParameter param4 = new SqlParameter("@PRODUCTO", nombreProducto);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ValidacionesSolicitudCreditoProducto>("dbo.PA_CONSULTAR_VALIDACIONES_SOLICITUD_CREDITO @DOCUMENTO_IDENTIFICACION, @TIPO_IDENTIFICACION, @PAPEL, @PRODUCTO", @param1, @param2, @param3, @param4);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual ObjectResult<DetalleNorma> ObtenerInformacionDetalleNorma(string producto, string norma)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@PRODUCTO", producto);
                SqlParameter param2 = new SqlParameter("@NORMA", norma);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DetalleNorma>("dbo.PA_CONSULTAR_DETALLES_NORMA_SOLICITUD_CREDITO @PRODUCTO, @NORMA", @param1, @param2);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public virtual void GuardarAsociado(GuardarInformacionSolicitante guardarInformacionSolicitante)
        {
            try
            {
                SqlParameter nitAfiliadoParameter = guardarInformacionSolicitante.nitAfiliado != null ?
                    new SqlParameter("@NITAFILIADO", guardarInformacionSolicitante.nitAfiliado) :
                    new SqlParameter("@NITAFILIADO", DBNull.Value);
                SqlParameter registroParameter = guardarInformacionSolicitante.registro != null ?
                    new SqlParameter("@REGISTRO", guardarInformacionSolicitante.registro) :
                    new SqlParameter("@REGISTRO", DBNull.Value);
                SqlParameter fechaNacimientoParameter = guardarInformacionSolicitante.fechaNacimiento != null ?
                    new SqlParameter("@FECHANACIMIENTO", guardarInformacionSolicitante.fechaNacimiento) :
                    new SqlParameter("@FECHANACIMIENTO", DBNull.Value);
                SqlParameter direccionResidenciaParameter = guardarInformacionSolicitante.direccionResidencia != null ?
                    new SqlParameter("@DIRECCIONRESIDENCIA", guardarInformacionSolicitante.direccionResidencia) :
                    new SqlParameter("@DIRECCIONRESIDENCIA", DBNull.Value);
                SqlParameter ciudadResidenciaParameter = guardarInformacionSolicitante.ciudadResidencia != null ?
                    new SqlParameter("@CIUDADRESIDENCIA", guardarInformacionSolicitante.ciudadResidencia) :
                    new SqlParameter("@CIUDADRESIDENCIA", DBNull.Value);
                SqlParameter telefonoResidenciaParameter = guardarInformacionSolicitante.telefonoResidencia != null ?
                    new SqlParameter("@TELEFONORESIDENCIA", guardarInformacionSolicitante.telefonoResidencia) :
                    new SqlParameter("@TELEFONORESIDENCIA", DBNull.Value);
                SqlParameter celularParameter = guardarInformacionSolicitante.celular != null ?
                    new SqlParameter("@CELULAR", guardarInformacionSolicitante.celular) :
                    new SqlParameter("@CELULAR", DBNull.Value);
                SqlParameter correoResidenciaParameter = guardarInformacionSolicitante.correoResidencia != null ?
                    new SqlParameter("@CORREORESIDENCIA", guardarInformacionSolicitante.correoResidencia) :
                    new SqlParameter("@CORREORESIDENCIA", DBNull.Value);
                SqlParameter correoOficinaParameter = guardarInformacionSolicitante.correoOficina != null ?
                    new SqlParameter("@CORREOOFICINA", guardarInformacionSolicitante.correoOficina) :
                    new SqlParameter("@CORREOOFICINA", DBNull.Value);
                SqlParameter barrioParameter = guardarInformacionSolicitante.barrio != null ?
                    new SqlParameter("@BARRIO", guardarInformacionSolicitante.barrio) :
                    new SqlParameter("@BARRIO", DBNull.Value);
                SqlParameter nominaParameter = guardarInformacionSolicitante.nomina != null ?
                    new SqlParameter("@NOMINA", guardarInformacionSolicitante.nomina) :
                    new SqlParameter("@NOMINA", DBNull.Value);
                SqlParameter papelParameter = guardarInformacionSolicitante.papel != null ?
                    new SqlParameter("@PAPEL", guardarInformacionSolicitante.papel) :
                    new SqlParameter("@PAPEL", DBNull.Value);
                SqlParameter ocupacionParameter = guardarInformacionSolicitante.ocupacion != null ?
                    new SqlParameter("@OCUPACION", guardarInformacionSolicitante.ocupacion) :
                    new SqlParameter("@OCUPACION", DBNull.Value);
                SqlParameter profesionParameter = guardarInformacionSolicitante.profesion != null ?
                    new SqlParameter("@PROFESION", guardarInformacionSolicitante.profesion) :
                    new SqlParameter("@PROFESION", DBNull.Value);
                SqlParameter cargoParameter = guardarInformacionSolicitante.cargo != null ?
                    new SqlParameter("@CARGO", guardarInformacionSolicitante.cargo) :
                    new SqlParameter("@CARGO", DBNull.Value);
                SqlParameter direccOficinaParameter = guardarInformacionSolicitante.direccOficina != null ?
                    new SqlParameter("@DIRECCOFICINA", guardarInformacionSolicitante.direccOficina) :
                    new SqlParameter("@DIRECCOFICINA", DBNull.Value);
                SqlParameter ciudadOficinaParameter = guardarInformacionSolicitante.ciudadOficina != null ?
                    new SqlParameter("@CIUDADOFICINA", guardarInformacionSolicitante.ciudadOficina) :
                    new SqlParameter("@CIUDADOFICINA", DBNull.Value);
                SqlParameter telefonoOficinaParameter = guardarInformacionSolicitante.telefonoOficina != null ?
                    new SqlParameter("@TELEFONOOFICINA", guardarInformacionSolicitante.telefonoOficina) :
                    new SqlParameter("@TELEFONOOFICINA", DBNull.Value);
                SqlParameter estadoCivilParameter = guardarInformacionSolicitante.estadoCivil != null ?
                    new SqlParameter("@ESTADOCIVIL", guardarInformacionSolicitante.estadoCivil) :
                    new SqlParameter("@ESTADOCIVIL", DBNull.Value);
                SqlParameter regimenParameter = guardarInformacionSolicitante.regimen != null ?
                    new SqlParameter("@REGIMEN", guardarInformacionSolicitante.regimen) :
                    new SqlParameter("@REGIMEN", DBNull.Value);
                SqlParameter personaCargoParameter = guardarInformacionSolicitante.personaCargo != null ?
                    new SqlParameter("@PERSONACARGO", guardarInformacionSolicitante.personaCargo) :
                    new SqlParameter("@PERSONACARGO", DBNull.Value);
                SqlParameter estratoParameter = guardarInformacionSolicitante.estrato != null ?
                    new SqlParameter("@ESTRATO", guardarInformacionSolicitante.estrato) :
                    new SqlParameter("@ESTRATO", DBNull.Value);
                SqlParameter ingreMensualesParameter = guardarInformacionSolicitante.ingreMensuales != null ?
                    new SqlParameter("@INGREMENSUALES", guardarInformacionSolicitante.ingreMensuales) :
                    new SqlParameter("@INGREMENSUALES", DBNull.Value);
                SqlParameter egreMensualesParameter = guardarInformacionSolicitante.egreMensuales != null ?
                    new SqlParameter("@EGREMENSUALES", guardarInformacionSolicitante.egreMensuales) :
                    new SqlParameter("@EGREMENSUALES", DBNull.Value);
                SqlParameter activosParameter = guardarInformacionSolicitante.activos != null ?
                    new SqlParameter("@ACTIVOS", guardarInformacionSolicitante.activos) :
                    new SqlParameter("@ACTIVOS", DBNull.Value);
                SqlParameter pasivosParameter = guardarInformacionSolicitante.pasivos != null ?
                    new SqlParameter("@PASIVOS", guardarInformacionSolicitante.pasivos) :
                    new SqlParameter("@PASIVOS", DBNull.Value);
                SqlParameter salarioActualParameter = guardarInformacionSolicitante.salarioActual != null ?
                    new SqlParameter("@SALARIOACTUAL", guardarInformacionSolicitante.salarioActual) :
                    new SqlParameter("@SALARIOACTUAL", DBNull.Value);
                SqlParameter otrosIngresosParameter = guardarInformacionSolicitante.otrosIngresos != null ?
                    new SqlParameter("@OTROSINGRESOS", guardarInformacionSolicitante.otrosIngresos) :
                    new SqlParameter("@OTROSINGRESOS", DBNull.Value);
                SqlParameter numeroHijosParameter = guardarInformacionSolicitante.numeroHijos != null ?
                    new SqlParameter("@NUMEROHIJOS", guardarInformacionSolicitante.numeroHijos) :
                    new SqlParameter("@NUMEROHIJOS", DBNull.Value);
                SqlParameter cabezaHogarParameter = guardarInformacionSolicitante.cabezaHogar != null ?
                    new SqlParameter("@CABEZAHOGAR", guardarInformacionSolicitante.cabezaHogar) :
                    new SqlParameter("@CABEZAHOGAR", DBNull.Value);
                SqlParameter tipoContratoParameter = guardarInformacionSolicitante.tipoContrato != null ?
                    new SqlParameter("@TIPOCONTRATO", guardarInformacionSolicitante.tipoContrato) :
                    new SqlParameter("@TIPOCONTRATO", DBNull.Value);
                SqlParameter actividadPrincipalParameter = guardarInformacionSolicitante.actividadPrincipal != null ?
                    new SqlParameter("@ACTIVIDADPRINCIPAL", guardarInformacionSolicitante.actividadPrincipal) :
                    new SqlParameter("@ACTIVIDADPRINCIPAL", DBNull.Value);
                SqlParameter actividadSecundariaParameter = guardarInformacionSolicitante.actividadSecundaria != null ?
                    new SqlParameter("@ACTIVIDADSECUNDARIA", guardarInformacionSolicitante.actividadSecundaria) :
                    new SqlParameter("@ACTIVIDADSECUNDARIA", DBNull.Value);
                SqlParameter usuarioParameter = guardarInformacionSolicitante.usuario != null ?
                    new SqlParameter("@USUARIO", guardarInformacionSolicitante.usuario) :
                    new SqlParameter("@USUARIO", DBNull.Value);

                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("" + DB + ".dbo.PA_ACTUALIZAR_CRM @NITAFILIADO,@REGISTRO,@FECHANACIMIENTO,@DIRECCIONRESIDENCIA,@CIUDADRESIDENCIA,@TELEFONORESIDENCIA,@CELULAR,@CORREORESIDENCIA,@CORREOOFICINA,@BARRIO,@NOMINA,@PAPEL,@OCUPACION,@PROFESION,@CARGO,@DIRECCOFICINA,@CIUDADOFICINA,@TELEFONOOFICINA,@ESTADOCIVIL,@REGIMEN,@PERSONACARGO,@ESTRATO,@INGREMENSUALES,@EGREMENSUALES,@ACTIVOS,@PASIVOS,@SALARIOACTUAL,@OTROSINGRESOS,@NUMEROHIJOS,@CABEZAHOGAR,@TIPOCONTRATO,@ACTIVIDADPRINCIPAL,@ACTIVIDADSECUNDARIA,@USUARIO",
                     @nitAfiliadoParameter, @registroParameter, @fechaNacimientoParameter, @direccionResidenciaParameter, @ciudadResidenciaParameter, @telefonoResidenciaParameter, @celularParameter, @correoResidenciaParameter, @correoOficinaParameter, @barrioParameter, @nominaParameter, @papelParameter, @ocupacionParameter, @profesionParameter, @cargoParameter, @direccOficinaParameter, @ciudadOficinaParameter, @telefonoOficinaParameter, @estadoCivilParameter, @regimenParameter, @personaCargoParameter, @estratoParameter, @ingreMensualesParameter, @egreMensualesParameter, @activosParameter, @pasivosParameter, @salarioActualParameter, @otrosIngresosParameter, @numeroHijosParameter, @cabezaHogarParameter, @tipoContratoParameter, @actividadPrincipalParameter, @actividadSecundariaParameter, @usuarioParameter);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public virtual ObjectResult<Aportes> ObtenerAportes(string tipoDocumento, string numeroDocumento, short IdTipoRol)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@TipoDocumento", tipoDocumento);
                SqlParameter param2 = new SqlParameter("@NumeroDocumento", numeroDocumento);
                SqlParameter param3 = new SqlParameter("@IdPapelCavipetrol", IdTipoRol);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Aportes>("dbo.PA_CONSULTAR_APORTES @TipoDocumento, @NumeroDocumento, @IdPapelCavipetrol", @param1, @param2, param3);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public virtual ObjectResult<Cartera> ObtenerCartera(string idTipoIdentificacion, string numeroDocumento)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@idTipoIdentificacion", idTipoIdentificacion);
                SqlParameter param2 = new SqlParameter("@NumeroIdentificacion", numeroDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Cartera>("dbo.PA_CONSULTAR_CARTERA @idTipoIdentificacion, @NumeroIdentificacion", @param1, @param2);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual ObjectResult<Garantia> ObtenerInformacionGarantia(string NumSolicitud, string TipoDoc, string NumeroDoc)
        {
            try
            {
                SqlParameter param1 = NumSolicitud == null ?
                new SqlParameter("@NumSolicitud", DBNull.Value) :
                new SqlParameter("@NumSolicitud", NumSolicitud);

                SqlParameter param2 = TipoDoc == null ?
                new SqlParameter("@TipoDoc", DBNull.Value) :
                new SqlParameter("@TipoDoc", TipoDoc);

                SqlParameter param3 = NumeroDoc == null ?
                new SqlParameter("@NumeroDoc", DBNull.Value) :
                new SqlParameter("@NumeroDoc", NumeroDoc);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Garantia>("dbo.PA_CONSULTAR_GARANTIA @NumSolicitud, @TipoDoc, @NumeroDoc", @param1, @param2, @param3);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public virtual ObjectResult<ViewModelFormaPagoFYC> ObtenerFormasPagoFYC(string norma)
        {
            try
            {
                SqlParameter param1 = norma == null ? new SqlParameter("@NORMA", DBNull.Value) : new SqlParameter("@NORMA", norma);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ViewModelFormaPagoFYC>("dbo.PA_CONSULTAR_FORMA_PAGO @NORMA", @param1);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public virtual ObjectResult<VMSaldoAsegurabilidad> ObtenerSaldoAsegurabilidad(string numeroIdentificacion)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@NumIdentificacion", numeroIdentificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<VMSaldoAsegurabilidad>("dbo.PA_CONSULTAR_SALDO_ASEGURABILIDAD @NumIdentificacion", @param1);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public virtual ObjectResult<RespuestaValidacion> ValidarCodeudor(string TipoDocumento)
        {
            try
            {
                SqlParameter param1 = TipoDocumento == null ? new SqlParameter("@numeroIdentificacion", DBNull.Value) : new SqlParameter("@numeroIdentificacion", TipoDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<RespuestaValidacion>("dbo.PA_Validar_Codeudor @numeroIdentificacion", @param1);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public virtual ObjectResult<TipoGarantiaPorProducto> ObtenerTipoGarantiaPorProducto(string Producto)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Producto", Producto);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<TipoGarantiaPorProducto>("PA_CONSULTAR_GARANTIAS_FYC @Producto", @param1);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual ObjectResult<Familiar> GuardarFamiliar(Familiar informacionFamiliar)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@TipoIdentificacion", informacionFamiliar.TipoIdentificacion);
                SqlParameter param2 = new SqlParameter("@NumeroIdentificacion", informacionFamiliar.NumeroIdentificacion);
                SqlParameter param3 = new SqlParameter("@NombresApellidos", informacionFamiliar.NombresApellidos);
                SqlParameter param4 = new SqlParameter("@DireccionResidencia", informacionFamiliar.DireccionResidencia);
                SqlParameter param5 = new SqlParameter("@Telefono", informacionFamiliar.Telefono);
                SqlParameter param6 = new SqlParameter("@CorreoElectronico", informacionFamiliar.CorreoElectronico);
                SqlParameter param7 = new SqlParameter("@Parentesco", informacionFamiliar.Parentesco);
                SqlParameter param8 = new SqlParameter("@TipoIdentificacionAsociado", informacionFamiliar.TipoIdentificacionAsociado);
                SqlParameter param9 = new SqlParameter("@NumeroIdentificacionAsociado", informacionFamiliar.NumeroIdentificacionAsociado);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Familiar>("dbo.PA_Guardar_Familiar @TipoIdentificacion, @NumeroIdentificacion,@NombresApellidos,@DireccionResidencia,@Telefono,@CorreoElectronico,@Parentesco,@TipoIdentificacionAsociado,@NumeroIdentificacionAsociado", @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public virtual ObjectResult<FormaPagoCredito> ObtenerFormaDePagoSolicitud(string idSolicitud)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@IdSolicitud", idSolicitud);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<FormaPagoCredito>("" + DB + ".dbo.PA_FormaPago_Credito @IdSolicitud", @param1);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /*
         * Metodo para mapear los parametros e invocar al sp PA_GUARDAR_SOLICITUD_CREDITO .
         */
        public virtual ObjectResult<ObjectInt> guardarSolicitudCreditoFyc(SolicitudCreditoFyc solicitudCreditoFyc)
        {
            try
            {
                SqlParameter fscNumsolicitudParameter = solicitudCreditoFyc.fscNumsolicitud != 0 || solicitudCreditoFyc.fscNumsolicitud != null ?
                    new SqlParameter("@fscNumsolicitud", solicitudCreditoFyc.fscNumsolicitud) :
                    new SqlParameter("@fscNumsolicitud", DBNull.Value);
                SqlParameter fscCiudadParameter = solicitudCreditoFyc.fscCiudad != null ?
                    new SqlParameter("@fscCiudad", solicitudCreditoFyc.fscCiudad) :
                    new SqlParameter("@fscCiudad", DBNull.Value);
                SqlParameter fscOficinaParameter = solicitudCreditoFyc.fscOficina != null ?
                    new SqlParameter("@fscOficina", solicitudCreditoFyc.fscOficina) :
                    new SqlParameter("@fscOficina", DBNull.Value);
                SqlParameter fscTnitafliadoParameter = solicitudCreditoFyc.fscTnitafliado != null ?
                    new SqlParameter("@fscTnitafliado", solicitudCreditoFyc.fscTnitafliado) :
                    new SqlParameter("@fscTnitafliado", DBNull.Value);
                SqlParameter fscNitafiliadoParameter = solicitudCreditoFyc.fscNitafiliado != null ?
                    new SqlParameter("@fscNitafiliado", solicitudCreditoFyc.fscNitafiliado) :
                    new SqlParameter("@fscNitafiliado", DBNull.Value);
                SqlParameter fscNomafiliadoParameter = solicitudCreditoFyc.fscNomafiliado != null ?
                    new SqlParameter("@fscNomafiliado", solicitudCreditoFyc.fscNomafiliado) :
                    new SqlParameter("@fscNomafiliado", DBNull.Value);
                SqlParameter fscProductoParameter = solicitudCreditoFyc.fscProducto != null ?
                    new SqlParameter("@fscProducto", solicitudCreditoFyc.fscProducto) :
                    new SqlParameter("@fscProducto", DBNull.Value);
                SqlParameter fscDocumentoParameter = solicitudCreditoFyc.fscDocumento != null ?
                    new SqlParameter("@fscDocumento", solicitudCreditoFyc.fscDocumento) :
                    new SqlParameter("@fscDocumento", DBNull.Value);
                SqlParameter fscNormaParameter = solicitudCreditoFyc.fscNorma != null ?
                    new SqlParameter("@fscNorma", solicitudCreditoFyc.fscNorma) :
                    new SqlParameter("@fscNorma", DBNull.Value);
                SqlParameter fscTipogarantiaParameter = solicitudCreditoFyc.fscTipogarantia != null ?
                    new SqlParameter("@fscTipogarantia", solicitudCreditoFyc.fscTipogarantia) :
                    new SqlParameter("@fscTipogarantia", DBNull.Value);
                SqlParameter fscFechaParameter = solicitudCreditoFyc.fscFecha > DateTime.Parse("1900-01-01") ?
                    new SqlParameter("@fscFecha", solicitudCreditoFyc.fscFecha) :
                    new SqlParameter("@fscFecha", DateTime.Now);
                SqlParameter fscMquincenalParameter = solicitudCreditoFyc.fscMquincenal != 0 || solicitudCreditoFyc.fscMquincenal != null ?
                    new SqlParameter("@fscMquincenal", solicitudCreditoFyc.fscMquincenal) :
                    new SqlParameter("@fscMquincenal", 0);
                SqlParameter fscMmensualParameter = solicitudCreditoFyc.fscMmensual != 0 || solicitudCreditoFyc.fscMmensual != null ?
                    new SqlParameter("@fscMmensual", solicitudCreditoFyc.fscMmensual) :
                    new SqlParameter("@fscMmensual", 0);
                SqlParameter fscMsemestralParameter = solicitudCreditoFyc.fscMsemestral != 0 || solicitudCreditoFyc.fscMsemestral != null ?
                    new SqlParameter("@fscMsemestral", solicitudCreditoFyc.fscMsemestral) :
                    new SqlParameter("@fscMsemestral", 0);
                SqlParameter fscMtrimestralParameter = solicitudCreditoFyc.fscMtrimestral != 0 || solicitudCreditoFyc.fscMtrimestral != null ?
                    new SqlParameter("@fscMtrimestral", solicitudCreditoFyc.fscMtrimestral) :
                    new SqlParameter("@fscMtrimestral", 0);
                SqlParameter fscManualParameter = solicitudCreditoFyc.fscManual != 0 || solicitudCreditoFyc.fscManual != null ?
                    new SqlParameter("@fscManual", solicitudCreditoFyc.fscManual) :
                    new SqlParameter("@fscManual", 0);
                SqlParameter fscSaldoParameter = solicitudCreditoFyc.fscSaldo != 0 || solicitudCreditoFyc.fscSaldo != null ?
                    new SqlParameter("@fscSaldo", solicitudCreditoFyc.fscSaldo) :
                    new SqlParameter("@fscSaldo", DBNull.Value);
                SqlParameter fscSaldonormalizadoParameter = solicitudCreditoFyc.fscSaldonormalizado != 0 || solicitudCreditoFyc.fscSaldonormalizado != null ?
                    new SqlParameter("@fscSaldonormalizado", solicitudCreditoFyc.fscSaldonormalizado) :
                    new SqlParameter("@fscSaldonormalizado", 0);
                SqlParameter fscPlazoanhosParameter = solicitudCreditoFyc.fscPlazoanhos != 0 || solicitudCreditoFyc.fscPlazoanhos != null ?
                    new SqlParameter("@fscPlazoanhos", solicitudCreditoFyc.fscPlazoanhos) :
                    new SqlParameter("@fscPlazoanhos", 0);
                SqlParameter fscClavequincenalParameter = solicitudCreditoFyc.fscClavequincenal != 0 || solicitudCreditoFyc.fscClavequincenal != null ?
                    new SqlParameter("@fscClavequincenal", solicitudCreditoFyc.fscClavequincenal) :
                    new SqlParameter("@fscClavequincenal", 0);
                SqlParameter fscClavemensualParameter = solicitudCreditoFyc.fscClavemensual != 0 || solicitudCreditoFyc.fscClavemensual != null ?
                    new SqlParameter("@fscClavemensual", solicitudCreditoFyc.fscClavemensual) :
                    new SqlParameter("@fscClavemensual", 0);
                SqlParameter fscClavesemestralParameter = solicitudCreditoFyc.fscClavesemestral != 0 || solicitudCreditoFyc.fscClavesemestral != null ?
                    new SqlParameter("@fscClavesemestral", solicitudCreditoFyc.fscClavesemestral) :
                    new SqlParameter("@fscClavesemestral", 0);
                SqlParameter fscClavetrimestralParameter = solicitudCreditoFyc.fscClavetrimestral != 0 || solicitudCreditoFyc.fscClavetrimestral != null ?
                    new SqlParameter("@fscClavetrimestral", solicitudCreditoFyc.fscClavetrimestral) :
                    new SqlParameter("@fscClavetrimestral", 0);
                SqlParameter fscClaveanualParameter = solicitudCreditoFyc.fscClaveanual != 0 || solicitudCreditoFyc.fscClaveanual != null ?
                    new SqlParameter("@fscClaveanual", solicitudCreditoFyc.fscClaveanual) :
                    new SqlParameter("@fscClaveanual", 0);
                SqlParameter fscDesembolsofaiParameter = solicitudCreditoFyc.fscDesembolsofai != 0 || solicitudCreditoFyc.fscDesembolsofai != null ?
                    new SqlParameter("@fscDesembolsofai", solicitudCreditoFyc.fscDesembolsofai) :
                    new SqlParameter("@fscDesembolsofai", DBNull.Value);
                SqlParameter fscDesembolsochequeParameter = solicitudCreditoFyc.fscDesembolsocheque != 0 || solicitudCreditoFyc.fscDesembolsocheque != null ?
                    new SqlParameter("@fscDesembolsocheque", solicitudCreditoFyc.fscDesembolsocheque) :
                    new SqlParameter("@fscDesembolsocheque", DBNull.Value);
                SqlParameter fscDesembolsocruzeParameter = solicitudCreditoFyc.fscDesembolsocruze != 0 || solicitudCreditoFyc.fscDesembolsocruze != null ?
                    new SqlParameter("@fscDesembolsocruze", solicitudCreditoFyc.fscDesembolsocruze) :
                    new SqlParameter("@fscDesembolsocruze", DBNull.Value);
                SqlParameter fscUsuarioParameter = solicitudCreditoFyc.fscUsuario != null ?
                    new SqlParameter("@fscUsuario", solicitudCreditoFyc.fscUsuario) :
                    new SqlParameter("@fscUsuario", DBNull.Value);
                SqlParameter fscEstadoParameter = solicitudCreditoFyc.fscEstado != null ?
                    new SqlParameter("@fscEstado", solicitudCreditoFyc.fscEstado) :
                    new SqlParameter("@fscEstado", DBNull.Value);
                SqlParameter fscDescripcionParameter = solicitudCreditoFyc.fscDescripcion != null ?
                    new SqlParameter("@fscDescripcion", solicitudCreditoFyc.fscDescripcion) :
                    new SqlParameter("@fscDescripcion", DBNull.Value);
                SqlParameter fscUsuariocontrolParameter = solicitudCreditoFyc.fscUsuariocontrol != null ?
                    new SqlParameter("@fscUsuariocontrol", solicitudCreditoFyc.fscUsuariocontrol) :
                    new SqlParameter("@fscUsuariocontrol", DBNull.Value);
                SqlParameter fscValorcuentafaiParameter = solicitudCreditoFyc.fscValorcuentafai != 0 || solicitudCreditoFyc.fscValorcuentafai != null ?
                    new SqlParameter("@fscValorcuentafai", solicitudCreditoFyc.fscValorcuentafai) :
                    new SqlParameter("@fscValorcuentafai", DBNull.Value);
                SqlParameter fscValorchequeParameter = solicitudCreditoFyc.fscValorcheque != 0 || solicitudCreditoFyc.fscValorcheque != null ?
                    new SqlParameter("@fscValorcheque", solicitudCreditoFyc.fscValorcheque) :
                    new SqlParameter("@fscValorcheque", DBNull.Value);
                SqlParameter fscValorinterproducParameter = solicitudCreditoFyc.fscValorinterproduc != 0 || solicitudCreditoFyc.fscValorinterproduc != null ?
                    new SqlParameter("@fscValorinterproduc", solicitudCreditoFyc.fscValorinterproduc) :
                    new SqlParameter("@fscValorinterproduc", DBNull.Value);
                SqlParameter fscTasaParameter = solicitudCreditoFyc.fscTasa != 0 || solicitudCreditoFyc.fscTasa != null ?
                    new SqlParameter("@fscTasa", solicitudCreditoFyc.fscTasa) :
                    new SqlParameter("@fscTasa", DBNull.Value);
                SqlParameter fscMesadaParameter = solicitudCreditoFyc.fscMesada != 0 || solicitudCreditoFyc.fscMesada != null ?
                    new SqlParameter("@fscMesada", solicitudCreditoFyc.fscMesada) :
                    new SqlParameter("@fscMesada", 0);
                SqlParameter fscObservacionParameter = solicitudCreditoFyc.fscObservacion != null ?
                    new SqlParameter("@fscObservacion", solicitudCreditoFyc.fscObservacion) :
                    new SqlParameter("@fscObservacion", DBNull.Value);
                SqlParameter fscPlazoquincenalParameter = solicitudCreditoFyc.fscPlazoquincenal != 0 || solicitudCreditoFyc.fscPlazoquincenal != null ?
                    new SqlParameter("@fscPlazoquincenal", solicitudCreditoFyc.fscPlazoquincenal) :
                    new SqlParameter("@fscPlazoquincenal", 0);
                SqlParameter fscPlazomensualParameter = solicitudCreditoFyc.fscPlazomensual != 0 || solicitudCreditoFyc.fscPlazomensual != null ?
                    new SqlParameter("@fscPlazomensual", solicitudCreditoFyc.fscPlazomensual) :
                    new SqlParameter("@fscPlazomensual", 0);
                SqlParameter fscPlazosemestralParameter = solicitudCreditoFyc.fscPlazosemestral != 0 || solicitudCreditoFyc.fscPlazosemestral != null ?
                    new SqlParameter("@fscPlazosemestral", solicitudCreditoFyc.fscPlazosemestral) :
                    new SqlParameter("@fscPlazosemestral", 0);
                SqlParameter fscPlazotrimestralParameter = solicitudCreditoFyc.fscPlazotrimestral != 0 || solicitudCreditoFyc.fscPlazotrimestral != null ?
                    new SqlParameter("@fscPlazotrimestral", solicitudCreditoFyc.fscPlazotrimestral) :
                    new SqlParameter("@fscPlazotrimestral", 0);
                SqlParameter fscPlazoanualParameter = solicitudCreditoFyc.fscPlazoanual != 0 || solicitudCreditoFyc.fscPlazoanual != null ?
                    new SqlParameter("@fscPlazoanual", solicitudCreditoFyc.fscPlazoanual) :
                    new SqlParameter("@fscPlazoanual", 0);
                SqlParameter fscVcuotaquincenalParameter = solicitudCreditoFyc.fscVcuotaquincenal != 0 || solicitudCreditoFyc.fscVcuotaquincenal != null ?
                    new SqlParameter("@fscVcuotaquincenal", solicitudCreditoFyc.fscVcuotaquincenal) :
                    new SqlParameter("@fscVcuotaquincenal", 0);
                SqlParameter fscVcuotamensualParameter = solicitudCreditoFyc.fscVcuotamensual != 0 || solicitudCreditoFyc.fscVcuotamensual != null ?
                    new SqlParameter("@fscVcuotamensual", solicitudCreditoFyc.fscVcuotamensual) :
                    new SqlParameter("@fscVcuotamensual", 0);
                SqlParameter fscVcuotasemestralParameter = solicitudCreditoFyc.fscVcuotasemestral != 0 || solicitudCreditoFyc.fscVcuotasemestral != null ?
                    new SqlParameter("@fscVcuotasemestral", solicitudCreditoFyc.fscVcuotasemestral) :
                    new SqlParameter("@fscVcuotasemestral", 0);
                SqlParameter fscVcuotatrimestralParameter = solicitudCreditoFyc.fscVcuotatrimestral != 0 || solicitudCreditoFyc.fscVcuotatrimestral != null ?
                    new SqlParameter("@fscVcuotatrimestral", solicitudCreditoFyc.fscVcuotatrimestral) :
                    new SqlParameter("@fscVcuotatrimestral", 0);
                SqlParameter fscVcuotanualParameter = solicitudCreditoFyc.fscVcuotanual != 0 || solicitudCreditoFyc.fscVcuotanual != null ?
                    new SqlParameter("@fscVcuotanual", solicitudCreditoFyc.fscVcuotanual) :
                    new SqlParameter("@fscVcuotanual", 0);
                SqlParameter fscPagaremayorParameter = solicitudCreditoFyc.fscPagaremayor != 0 || solicitudCreditoFyc.fscPagaremayor != null ?
                    new SqlParameter("@fscPagaremayor", solicitudCreditoFyc.fscPagaremayor) :
                    new SqlParameter("@fscPagaremayor", 0);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ObjectInt>("" + DB + ".dbo.PA_GUARDAR_SOLICITUD_CREDITO @fscNumsolicitud, @fscCiudad, @fscOficina, @fscTnitafliado, @fscNitafiliado, @fscNomafiliado, @fscProducto, @fscDocumento, @fscNorma, @fscTipogarantia, @fscFecha, @fscMquincenal, @fscMmensual, @fscMsemestral, @fscMtrimestral, @fscManual, @fscSaldo, @fscSaldonormalizado, @fscPlazoanhos, @fscClavequincenal, @fscClavemensual, @fscClavesemestral, @fscClavetrimestral, @fscClaveanual, @fscDesembolsofai, @fscDesembolsocheque, @fscDesembolsocruze, @fscUsuario, @fscEstado, @fscDescripcion, @fscUsuariocontrol, @fscValorcuentafai, @fscValorcheque, @fscValorinterproduc, @fscTasa, @fscMesada, @fscObservacion, @fscPlazoquincenal, @fscPlazomensual, @fscPlazosemestral, @fscPlazotrimestral, @fscPlazoanual, @fscVcuotaquincenal, @fscVcuotamensual, @fscVcuotasemestral, @fscVcuotatrimestral, @fscVcuotanual, @fscPagaremayor",
                @fscNumsolicitudParameter, @fscCiudadParameter, @fscOficinaParameter, @fscTnitafliadoParameter, @fscNitafiliadoParameter, @fscNomafiliadoParameter, @fscProductoParameter, @fscDocumentoParameter, @fscNormaParameter, @fscTipogarantiaParameter, @fscFechaParameter, @fscMquincenalParameter, @fscMmensualParameter, @fscMsemestralParameter, @fscMtrimestralParameter, @fscManualParameter, @fscSaldoParameter, @fscSaldonormalizadoParameter, @fscPlazoanhosParameter, @fscClavequincenalParameter, @fscClavemensualParameter, @fscClavesemestralParameter, @fscClavetrimestralParameter, @fscClaveanualParameter, @fscDesembolsofaiParameter, @fscDesembolsochequeParameter, @fscDesembolsocruzeParameter, @fscUsuarioParameter, @fscEstadoParameter, @fscDescripcionParameter, @fscUsuariocontrolParameter, @fscValorcuentafaiParameter, @fscValorchequeParameter, @fscValorinterproducParameter, @fscTasaParameter, @fscMesadaParameter, @fscObservacionParameter, @fscPlazoquincenalParameter, @fscPlazomensualParameter, @fscPlazosemestralParameter, @fscPlazotrimestralParameter, @fscPlazoanualParameter, @fscVcuotaquincenalParameter, @fscVcuotamensualParameter, @fscVcuotasemestralParameter, @fscVcuotatrimestralParameter, @fscVcuotanualParameter, @fscPagaremayorParameter);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// guardado solicitud credito fyc isynet ECP
        /// </summary>
        /// <param name="solicitudCreditoFyc"></param>
        /// <returns></returns>

        public virtual ObjectResult<ObjectInt> GuardarSolicitudCreditoFycIsynetECP(SolicitudCreditoFyc solicitudCreditoFyc)
        {
            try
            {
                SqlParameter fscNumsolicitudParameter = solicitudCreditoFyc.fscNumsolicitud != 0 || solicitudCreditoFyc.fscNumsolicitud != null ?
                    new SqlParameter("@fscNumsolicitud", solicitudCreditoFyc.fscNumsolicitud) :
                    new SqlParameter("@fscNumsolicitud", DBNull.Value);
                SqlParameter fscCiudadParameter = solicitudCreditoFyc.fscCiudad != null ?
                    new SqlParameter("@fscCiudad", solicitudCreditoFyc.fscCiudad) :
                    new SqlParameter("@fscCiudad", DBNull.Value);
                SqlParameter fscOficinaParameter = solicitudCreditoFyc.fscOficina != null ?
                    new SqlParameter("@fscOficina", solicitudCreditoFyc.fscOficina) :
                    new SqlParameter("@fscOficina", DBNull.Value);
                SqlParameter fscTnitafliadoParameter = solicitudCreditoFyc.fscTnitafliado != null ?
                    new SqlParameter("@fscTnitafliado", solicitudCreditoFyc.fscTnitafliado) :
                    new SqlParameter("@fscTnitafliado", DBNull.Value);
                SqlParameter fscNitafiliadoParameter = solicitudCreditoFyc.fscNitafiliado != null ?
                    new SqlParameter("@fscNitafiliado", solicitudCreditoFyc.fscNitafiliado) :
                    new SqlParameter("@fscNitafiliado", DBNull.Value);
                SqlParameter fscNomafiliadoParameter = solicitudCreditoFyc.fscNomafiliado != null ?
                    new SqlParameter("@fscNomafiliado", solicitudCreditoFyc.fscNomafiliado) :
                    new SqlParameter("@fscNomafiliado", DBNull.Value);
                SqlParameter fscProductoParameter = solicitudCreditoFyc.fscProducto != null ?
                    new SqlParameter("@fscProducto", solicitudCreditoFyc.fscProducto) :
                    new SqlParameter("@fscProducto", DBNull.Value);
                SqlParameter fscDocumentoParameter = solicitudCreditoFyc.fscDocumento != null ?
                    new SqlParameter("@fscDocumento", solicitudCreditoFyc.fscDocumento) :
                    new SqlParameter("@fscDocumento", DBNull.Value);
                SqlParameter fscNormaParameter = solicitudCreditoFyc.fscNorma != null ?
                    new SqlParameter("@fscNorma", solicitudCreditoFyc.fscNorma) :
                    new SqlParameter("@fscNorma", DBNull.Value);
                SqlParameter fscTipogarantiaParameter = solicitudCreditoFyc.fscTipogarantia != null ?
                    new SqlParameter("@fscTipogarantia", solicitudCreditoFyc.fscTipogarantia) :
                    new SqlParameter("@fscTipogarantia", DBNull.Value);
                SqlParameter fscFechaParameter = solicitudCreditoFyc.fscFecha > DateTime.Parse("1900-01-01") ?
                    new SqlParameter("@fscFecha", solicitudCreditoFyc.fscFecha) :
                    new SqlParameter("@fscFecha", DateTime.Now);
                SqlParameter fscMquincenalParameter = solicitudCreditoFyc.fscMquincenal != 0 || solicitudCreditoFyc.fscMquincenal != null ?
                    new SqlParameter("@fscMquincenal", solicitudCreditoFyc.fscMquincenal) :
                    new SqlParameter("@fscMquincenal", 0);
                SqlParameter fscMmensualParameter = solicitudCreditoFyc.fscMmensual != 0 || solicitudCreditoFyc.fscMmensual != null ?
                    new SqlParameter("@fscMmensual", solicitudCreditoFyc.fscMmensual) :
                    new SqlParameter("@fscMmensual", 0);
                SqlParameter fscMsemestralParameter = solicitudCreditoFyc.fscMsemestral != 0 || solicitudCreditoFyc.fscMsemestral != null ?
                    new SqlParameter("@fscMsemestral", solicitudCreditoFyc.fscMsemestral) :
                    new SqlParameter("@fscMsemestral", 0);
                SqlParameter fscMtrimestralParameter = solicitudCreditoFyc.fscMtrimestral != 0 || solicitudCreditoFyc.fscMtrimestral != null ?
                    new SqlParameter("@fscMtrimestral", solicitudCreditoFyc.fscMtrimestral) :
                    new SqlParameter("@fscMtrimestral", 0);
                SqlParameter fscManualParameter = solicitudCreditoFyc.fscManual != 0 || solicitudCreditoFyc.fscManual != null ?
                    new SqlParameter("@fscManual", solicitudCreditoFyc.fscManual) :
                    new SqlParameter("@fscManual", 0);
                SqlParameter fscSaldoParameter = solicitudCreditoFyc.fscSaldo != 0 || solicitudCreditoFyc.fscSaldo != null ?
                    new SqlParameter("@fscSaldo", solicitudCreditoFyc.fscSaldo) :
                    new SqlParameter("@fscSaldo", DBNull.Value);
                SqlParameter fscSaldonormalizadoParameter = solicitudCreditoFyc.fscSaldonormalizado != 0 || solicitudCreditoFyc.fscSaldonormalizado != null ?
                    new SqlParameter("@fscSaldonormalizado", solicitudCreditoFyc.fscSaldonormalizado) :
                    new SqlParameter("@fscSaldonormalizado", 0);
                SqlParameter fscPlazoanhosParameter = solicitudCreditoFyc.fscPlazoanhos != 0 || solicitudCreditoFyc.fscPlazoanhos != null ?
                    new SqlParameter("@fscPlazoanhos", solicitudCreditoFyc.fscPlazoanhos) :
                    new SqlParameter("@fscPlazoanhos", 0);
                SqlParameter fscClavequincenalParameter = solicitudCreditoFyc.fscClavequincenal != 0 || solicitudCreditoFyc.fscClavequincenal != null ?
                    new SqlParameter("@fscClavequincenal", solicitudCreditoFyc.fscClavequincenal) :
                    new SqlParameter("@fscClavequincenal", 0);
                SqlParameter fscClavemensualParameter = solicitudCreditoFyc.fscClavemensual != 0 || solicitudCreditoFyc.fscClavemensual != null ?
                    new SqlParameter("@fscClavemensual", solicitudCreditoFyc.fscClavemensual) :
                    new SqlParameter("@fscClavemensual", 0);
                SqlParameter fscClavesemestralParameter = solicitudCreditoFyc.fscClavesemestral != 0 || solicitudCreditoFyc.fscClavesemestral != null ?
                    new SqlParameter("@fscClavesemestral", solicitudCreditoFyc.fscClavesemestral) :
                    new SqlParameter("@fscClavesemestral", 0);
                SqlParameter fscClavetrimestralParameter = solicitudCreditoFyc.fscClavetrimestral != 0 || solicitudCreditoFyc.fscClavetrimestral != null ?
                    new SqlParameter("@fscClavetrimestral", solicitudCreditoFyc.fscClavetrimestral) :
                    new SqlParameter("@fscClavetrimestral", 0);
                SqlParameter fscClaveanualParameter = solicitudCreditoFyc.fscClaveanual != 0 || solicitudCreditoFyc.fscClaveanual != null ?
                    new SqlParameter("@fscClaveanual", solicitudCreditoFyc.fscClaveanual) :
                    new SqlParameter("@fscClaveanual", 0);
                SqlParameter fscDesembolsofaiParameter = solicitudCreditoFyc.fscDesembolsofai != 0 || solicitudCreditoFyc.fscDesembolsofai != null ?
                    new SqlParameter("@fscDesembolsofai", solicitudCreditoFyc.fscDesembolsofai) :
                    new SqlParameter("@fscDesembolsofai", DBNull.Value);
                SqlParameter fscDesembolsochequeParameter = solicitudCreditoFyc.fscDesembolsocheque != 0 || solicitudCreditoFyc.fscDesembolsocheque != null ?
                    new SqlParameter("@fscDesembolsocheque", solicitudCreditoFyc.fscDesembolsocheque) :
                    new SqlParameter("@fscDesembolsocheque", DBNull.Value);
                SqlParameter fscDesembolsocruzeParameter = solicitudCreditoFyc.fscDesembolsocruze != 0 || solicitudCreditoFyc.fscDesembolsocruze != null ?
                    new SqlParameter("@fscDesembolsocruze", solicitudCreditoFyc.fscDesembolsocruze) :
                    new SqlParameter("@fscDesembolsocruze", DBNull.Value);
                SqlParameter fscUsuarioParameter = solicitudCreditoFyc.fscUsuario != null ?
                    new SqlParameter("@fscUsuario", solicitudCreditoFyc.fscUsuario) :
                    new SqlParameter("@fscUsuario", DBNull.Value);
                SqlParameter fscEstadoParameter = solicitudCreditoFyc.fscEstado != null ?
                    new SqlParameter("@fscEstado", solicitudCreditoFyc.fscEstado) :
                    new SqlParameter("@fscEstado", DBNull.Value);
                SqlParameter fscDescripcionParameter = solicitudCreditoFyc.fscDescripcion != null ?
                    new SqlParameter("@fscDescripcion", solicitudCreditoFyc.fscDescripcion) :
                    new SqlParameter("@fscDescripcion", DBNull.Value);
                SqlParameter fscUsuariocontrolParameter = solicitudCreditoFyc.fscUsuariocontrol != null ?
                    new SqlParameter("@fscUsuariocontrol", solicitudCreditoFyc.fscUsuariocontrol) :
                    new SqlParameter("@fscUsuariocontrol", DBNull.Value);
                SqlParameter fscValorcuentafaiParameter = solicitudCreditoFyc.fscValorcuentafai != 0 || solicitudCreditoFyc.fscValorcuentafai != null ?
                    new SqlParameter("@fscValorcuentafai", solicitudCreditoFyc.fscValorcuentafai) :
                    new SqlParameter("@fscValorcuentafai", DBNull.Value);
                SqlParameter fscValorchequeParameter = solicitudCreditoFyc.fscValorcheque != 0 || solicitudCreditoFyc.fscValorcheque != null ?
                    new SqlParameter("@fscValorcheque", solicitudCreditoFyc.fscValorcheque) :
                    new SqlParameter("@fscValorcheque", DBNull.Value);
                SqlParameter fscValorinterproducParameter = solicitudCreditoFyc.fscValorinterproduc != 0 || solicitudCreditoFyc.fscValorinterproduc != null ?
                    new SqlParameter("@fscValorinterproduc", solicitudCreditoFyc.fscValorinterproduc) :
                    new SqlParameter("@fscValorinterproduc", DBNull.Value);
                SqlParameter fscTasaParameter = solicitudCreditoFyc.fscTasa != 0 || solicitudCreditoFyc.fscTasa != null ?
                    new SqlParameter("@fscTasa", solicitudCreditoFyc.fscTasa) :
                    new SqlParameter("@fscTasa", DBNull.Value);
                SqlParameter fscMesadaParameter = solicitudCreditoFyc.fscMesada != 0 || solicitudCreditoFyc.fscMesada != null ?
                    new SqlParameter("@fscMesada", solicitudCreditoFyc.fscMesada) :
                    new SqlParameter("@fscMesada", 0);
                SqlParameter fscObservacionParameter = solicitudCreditoFyc.fscObservacion != null ?
                    new SqlParameter("@fscObservacion", solicitudCreditoFyc.fscObservacion) :
                    new SqlParameter("@fscObservacion", DBNull.Value);
                SqlParameter fscPlazoquincenalParameter = solicitudCreditoFyc.fscPlazoquincenal != 0 || solicitudCreditoFyc.fscPlazoquincenal != null ?
                    new SqlParameter("@fscPlazoquincenal", solicitudCreditoFyc.fscPlazoquincenal) :
                    new SqlParameter("@fscPlazoquincenal", 0);
                SqlParameter fscPlazomensualParameter = solicitudCreditoFyc.fscPlazomensual != 0 || solicitudCreditoFyc.fscPlazomensual != null ?
                    new SqlParameter("@fscPlazomensual", solicitudCreditoFyc.fscPlazomensual) :
                    new SqlParameter("@fscPlazomensual", 0);
                SqlParameter fscPlazosemestralParameter = solicitudCreditoFyc.fscPlazosemestral != 0 || solicitudCreditoFyc.fscPlazosemestral != null ?
                    new SqlParameter("@fscPlazosemestral", solicitudCreditoFyc.fscPlazosemestral) :
                    new SqlParameter("@fscPlazosemestral", 0);
                SqlParameter fscPlazotrimestralParameter = solicitudCreditoFyc.fscPlazotrimestral != 0 || solicitudCreditoFyc.fscPlazotrimestral != null ?
                    new SqlParameter("@fscPlazotrimestral", solicitudCreditoFyc.fscPlazotrimestral) :
                    new SqlParameter("@fscPlazotrimestral", 0);
                SqlParameter fscPlazoanualParameter = solicitudCreditoFyc.fscPlazoanual != 0 || solicitudCreditoFyc.fscPlazoanual != null ?
                    new SqlParameter("@fscPlazoanual", solicitudCreditoFyc.fscPlazoanual) :
                    new SqlParameter("@fscPlazoanual", 0);
                SqlParameter fscVcuotaquincenalParameter = solicitudCreditoFyc.fscVcuotaquincenal != 0 || solicitudCreditoFyc.fscVcuotaquincenal != null ?
                    new SqlParameter("@fscVcuotaquincenal", solicitudCreditoFyc.fscVcuotaquincenal) :
                    new SqlParameter("@fscVcuotaquincenal", 0);
                SqlParameter fscVcuotamensualParameter = solicitudCreditoFyc.fscVcuotamensual != 0 || solicitudCreditoFyc.fscVcuotamensual != null ?
                    new SqlParameter("@fscVcuotamensual", solicitudCreditoFyc.fscVcuotamensual) :
                    new SqlParameter("@fscVcuotamensual", 0);
                SqlParameter fscVcuotasemestralParameter = solicitudCreditoFyc.fscVcuotasemestral != 0 || solicitudCreditoFyc.fscVcuotasemestral != null ?
                    new SqlParameter("@fscVcuotasemestral", solicitudCreditoFyc.fscVcuotasemestral) :
                    new SqlParameter("@fscVcuotasemestral", 0);
                SqlParameter fscVcuotatrimestralParameter = solicitudCreditoFyc.fscVcuotatrimestral != 0 || solicitudCreditoFyc.fscVcuotatrimestral != null ?
                    new SqlParameter("@fscVcuotatrimestral", solicitudCreditoFyc.fscVcuotatrimestral) :
                    new SqlParameter("@fscVcuotatrimestral", 0);
                SqlParameter fscVcuotanualParameter = solicitudCreditoFyc.fscVcuotanual != 0 || solicitudCreditoFyc.fscVcuotanual != null ?
                    new SqlParameter("@fscVcuotanual", solicitudCreditoFyc.fscVcuotanual) :
                    new SqlParameter("@fscVcuotanual", 0);
                SqlParameter fscPagaremayorParameter = solicitudCreditoFyc.fscPagaremayor != 0 || solicitudCreditoFyc.fscPagaremayor != null ?
                    new SqlParameter("@fscPagaremayor", solicitudCreditoFyc.fscPagaremayor) :
                    new SqlParameter("@fscPagaremayor", 0);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ObjectInt>("" + DB + ".dbo.PA_GUARDAR_SOLICITUD_CREDITO_FYC_ISYNET @fscNumsolicitud, @fscCiudad, @fscOficina, @fscTnitafliado, @fscNitafiliado, @fscNomafiliado, @fscProducto, @fscDocumento, @fscNorma, @fscTipogarantia, @fscFecha, @fscMquincenal, @fscMmensual, @fscMsemestral, @fscMtrimestral, @fscManual, @fscSaldo, @fscSaldonormalizado, @fscPlazoanhos, @fscClavequincenal, @fscClavemensual, @fscClavesemestral, @fscClavetrimestral, @fscClaveanual, @fscDesembolsofai, @fscDesembolsocheque, @fscDesembolsocruze, @fscUsuario, @fscEstado, @fscDescripcion, @fscUsuariocontrol, @fscValorcuentafai, @fscValorcheque, @fscValorinterproduc, @fscTasa, @fscMesada, @fscObservacion, @fscPlazoquincenal, @fscPlazomensual, @fscPlazosemestral, @fscPlazotrimestral, @fscPlazoanual, @fscVcuotaquincenal, @fscVcuotamensual, @fscVcuotasemestral, @fscVcuotatrimestral, @fscVcuotanual, @fscPagaremayor",
                @fscNumsolicitudParameter, @fscCiudadParameter, @fscOficinaParameter, @fscTnitafliadoParameter, @fscNitafiliadoParameter, @fscNomafiliadoParameter, @fscProductoParameter, @fscDocumentoParameter, @fscNormaParameter, @fscTipogarantiaParameter, @fscFechaParameter, @fscMquincenalParameter, @fscMmensualParameter, @fscMsemestralParameter, @fscMtrimestralParameter, @fscManualParameter, @fscSaldoParameter, @fscSaldonormalizadoParameter, @fscPlazoanhosParameter, @fscClavequincenalParameter, @fscClavemensualParameter, @fscClavesemestralParameter, @fscClavetrimestralParameter, @fscClaveanualParameter, @fscDesembolsofaiParameter, @fscDesembolsochequeParameter, @fscDesembolsocruzeParameter, @fscUsuarioParameter, @fscEstadoParameter, @fscDescripcionParameter, @fscUsuariocontrolParameter, @fscValorcuentafaiParameter, @fscValorchequeParameter, @fscValorinterproducParameter, @fscTasaParameter, @fscMesadaParameter, @fscObservacionParameter, @fscPlazoquincenalParameter, @fscPlazomensualParameter, @fscPlazosemestralParameter, @fscPlazotrimestralParameter, @fscPlazoanualParameter, @fscVcuotaquincenalParameter, @fscVcuotamensualParameter, @fscVcuotasemestralParameter, @fscVcuotatrimestralParameter, @fscVcuotanualParameter, @fscPagaremayorParameter);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /**
         * Metodo que genera y retorna el numero de pagare de la solicitud de credito.
         *
         */
        public virtual ObjectResult<ObjectInt> generarNumeroPagare(SolicitudCreditoPagare solicitudCreditoPagare)
        {
            try
            {
                SqlParameter variableParameter = solicitudCreditoPagare.variable != null ?
                    new SqlParameter("@variable", solicitudCreditoPagare.variable) :
                    new SqlParameter("@variable", DBNull.Value);
                SqlParameter ciudadParameter = solicitudCreditoPagare.ciudad != null ?
                    new SqlParameter("@ciudad", solicitudCreditoPagare.ciudad) :
                    new SqlParameter("@ciudad", DBNull.Value);
                SqlParameter ciudadAdjuntoParameter = solicitudCreditoPagare.ciudadAdjunto != null ?
                    new SqlParameter("@ciudadAdjunto", solicitudCreditoPagare.ciudadAdjunto) :
                    new SqlParameter("@ciudadAdjunto", DBNull.Value);
                SqlParameter tipoNitBeneficiarioParameter = solicitudCreditoPagare.tipoNitBeneficiario != null ?
                    new SqlParameter("@tipoNitBeneficiario", solicitudCreditoPagare.tipoNitBeneficiario) :
                    new SqlParameter("@tipoNitBeneficiario", DBNull.Value);
                SqlParameter nitBeneficiarioParameter = solicitudCreditoPagare.nitBeneficiario != null ?
                    new SqlParameter("@nitBeneficiario", solicitudCreditoPagare.nitBeneficiario) :
                    new SqlParameter("@nitBeneficiario", DBNull.Value);
                SqlParameter fechaParameter = solicitudCreditoPagare.fecha != null ?
                    new SqlParameter("@fecha", solicitudCreditoPagare.fecha) :
                    new SqlParameter("@fecha", DBNull.Value);
                SqlParameter @idSolicitud = solicitudCreditoPagare.idSolicitud != null ?
                    new SqlParameter("@idSolicitud", solicitudCreditoPagare.idSolicitud) :
                    new SqlParameter("@idSolicitud", DBNull.Value);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ObjectInt>("" + DB + ".dbo.PA_GENERAR_NUMERO_PAGARE @variable,@ciudad,@ciudadAdjunto,@tipoNitBeneficiario,@nitBeneficiario,@fecha,@idSolicitud",
                    @variableParameter, @ciudadParameter, @ciudadAdjuntoParameter, @tipoNitBeneficiarioParameter, @nitBeneficiarioParameter, @fechaParameter, @idSolicitud);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void guardarSolicitudCreditoPagare(SolicitudCreditoPagare solicitudCreditoPagare)
        {
            try
            {
                SqlParameter idSolicitudParameter = solicitudCreditoPagare.idSolicitud != null ?
                    new SqlParameter("@idSolicitud", solicitudCreditoPagare.idSolicitud) :
                    new SqlParameter("@idSolicitud", DBNull.Value);
                SqlParameter idPagareParameter = solicitudCreditoPagare.idPagare != null ?
                    new SqlParameter("@idPagare", solicitudCreditoPagare.idPagare) :
                    new SqlParameter("@idPagare", DBNull.Value);
                SqlParameter numeroPagareParameter = solicitudCreditoPagare.numeroPagare != null ?
                    new SqlParameter("@numeroPagare", solicitudCreditoPagare.numeroPagare) :
                    new SqlParameter("@numeroPagare", DBNull.Value);
                SqlParameter fechaCreacionParameter = solicitudCreditoPagare.fechaCreacion != null ?
                    new SqlParameter("@fechaCreacion", solicitudCreditoPagare.fechaCreacion) :
                    new SqlParameter("@fechaCreacion", DBNull.Value);
                SqlParameter fechaModificacionParameter = solicitudCreditoPagare.fechaModificacion != null ?
                    new SqlParameter("@fechaModificacion", solicitudCreditoPagare.fechaModificacion) :
                    new SqlParameter("@fechaModificacion", DBNull.Value);
                SqlParameter usuarioCreacionParameter = solicitudCreditoPagare.usuarioCreacion != null ?
                    new SqlParameter("@usuarioCreacion", solicitudCreditoPagare.usuarioCreacion) :
                    new SqlParameter("@usuarioCreacion", DBNull.Value);
                SqlParameter usuarioModificacionParameter = solicitudCreditoPagare.usuarioModificacion != null ?
                    new SqlParameter("@usuarioModificacion", solicitudCreditoPagare.usuarioModificacion) : new SqlParameter("@usuarioModificacion", DBNull.Value);
                SqlParameter activoParameter = solicitudCreditoPagare.activo != null ?
                    new SqlParameter("@activo", solicitudCreditoPagare.activo) : new SqlParameter("@activo", DBNull.Value);
                SqlParameter jsonConceptosParameter = solicitudCreditoPagare.jsonConceptos != null ?
                    new SqlParameter("@jsonConceptos", solicitudCreditoPagare.jsonConceptos) : new SqlParameter("@jsonConceptos", DBNull.Value);

                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("" + DB + ".dbo.PA_GUARDAR_SOLICITUD_CREDITO_PAGARE @idSolicitud,@idPagare,@numeroPagare,@fechaCreacion,@fechaModificacion,@usuarioCreacion,@usuarioModificacion,@activo,@jsonConceptos",
                    @idSolicitudParameter, @idPagareParameter, @numeroPagareParameter, @fechaCreacionParameter, @fechaModificacionParameter, @usuarioCreacionParameter, @usuarioModificacionParameter, @activoParameter, @jsonConceptosParameter);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /**
         * Metodo que mapea los parametros para invocar al SP YSADFGDSA.
         */
        public void guardarPagareGarantia(VMConceptoGarantiaCredito vMConceptoGarantiaCredito)
        {
            try
            {
                SqlParameter nombrePagareParameter = vMConceptoGarantiaCredito.pagare != null ?
                    new SqlParameter("@nombrePagare", vMConceptoGarantiaCredito.pagare) :
                    new SqlParameter("@nombrePagare", DBNull.Value);

                SqlParameter numeroPagareParameter = vMConceptoGarantiaCredito.IdPagare != 0 ?
                    new SqlParameter("@numeroPagare", vMConceptoGarantiaCredito.IdPagare) :
                    new SqlParameter("@numeroPagare", 0);

                SqlParameter columnParameter = vMConceptoGarantiaCredito.pkFycItemDocumento != null ?
                new SqlParameter("@column", vMConceptoGarantiaCredito.pkFycItemDocumento) :
                new SqlParameter("@column", DBNull.Value);

                SqlParameter tipoVariableParameter = vMConceptoGarantiaCredito.tipoVariable != null ?
                    new SqlParameter("@tipoVariable", vMConceptoGarantiaCredito.tipoVariable) :
                    new SqlParameter("@tipoVariable", DBNull.Value);

                SqlParameter datoCadenaParameter = vMConceptoGarantiaCredito.Valor != null ?
                            new SqlParameter("@datoCadena", vMConceptoGarantiaCredito.Valor) :
                            new SqlParameter("@datoCadena", DBNull.Value);

                SqlParameter datoFechaParameter = vMConceptoGarantiaCredito.Valor != null ?
                            new SqlParameter("@datoFecha", vMConceptoGarantiaCredito.Valor) :
                            new SqlParameter("@datoFecha", DBNull.Value);

                SqlParameter datoValorParameter = vMConceptoGarantiaCredito.Valor != null ?
                            new SqlParameter("@datoValor", vMConceptoGarantiaCredito.Valor) :
                            new SqlParameter("@datoValor", DBNull.Value);

                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("" + DB + ".dbo.PA_GUARDAR_PAGARE_FYC @nombrePagare,@numeroPagare,@column,@tipoVariable,@datoCadena,@datoFecha,@datoValor",
                @nombrePagareParameter, @numeroPagareParameter, @columnParameter, @tipoVariableParameter, @datoCadenaParameter, @datoFechaParameter, @datoValorParameter);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual ObjectResult<SolicitudesCredito> ObtenerSolicitudes(int numeroSolicitud, string tipoDocumento, int numeroDocumento, int tipoCredito)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@NumeroSolicitud", numeroSolicitud);
                SqlParameter param2 = tipoDocumento != null ?
                    new SqlParameter("@TipoIdentificación", tipoDocumento) :
                    new SqlParameter("@TipoIdentificación", DBNull.Value);
                SqlParameter param3 = new SqlParameter("@NumeroIdentificación", numeroDocumento);
                SqlParameter param4 = new SqlParameter("@TipoCrédito", tipoCredito);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SolicitudesCredito>("" + DB + ".dbo.PA_ObtnerSolicitudesCredito @NumeroSolicitud, @TipoIdentificación, @NumeroIdentificación, @TipoCrédito", @param1, param2, @param3, @param4);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual ObjectResult<AsegurabilidadOpcion> ObtenerOpcionesAsegurabilidad(bool IdAsegurable, string idCredito, string idPapel)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@IdAsegurable", IdAsegurable);
                SqlParameter param2 = idCredito != null ? new SqlParameter("@idCredito", idCredito) : new SqlParameter("@idCredito", DBNull.Value);
                SqlParameter param3 = new SqlParameter("@idPapel", idPapel);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<AsegurabilidadOpcion>("" + DB + ".dbo.ObtenerOpcionAsegurabilidad @IdAsegurable, @idCredito, @idPapel", @param1, param2, @param3);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual ObjectResult<int> ValidarITP(string cedula, int bandera)
        {
            try
            {
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<int>("FYC.dbo.SOLICITUD_CREDITOS @NITAFILIADO = '" + cedula + "', @BANDERA = " + bandera);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public virtual ObjectResult<int> ValidarEmbargo(string cedula, int bandera)
        {
            try
            {
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<int>("PA_VALIDAR_CONTROLES @NIT = '" + cedula + "', @BANDERA = " + bandera);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public virtual ObjectResult<int> ValidarExisExtraprima(string cedula, string tipoIdentificacion, int bandera)
        {
            try
            {
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<int>("PA_VALIDAR_CONTROLES @BANDERA = " + bandera + ", @NIT = '" + cedula + "', @TIPONIT = '" + tipoIdentificacion + "'");
            }
            catch (Exception)
            {
                throw;
            }
        }
        public virtual ObjectResult<ExtraPrima> TraeExtraprima(string cedula, string tipoIdentificacion, int bandera)
        {
            try
            {

                SqlParameter param1 = new SqlParameter("@BANDERA", bandera);
                SqlParameter param2 = new SqlParameter("@NIT", cedula);
                SqlParameter param3 = new SqlParameter("@TIPONIT", tipoIdentificacion);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ExtraPrima>("PA_VALIDAR_CONTROLES @BANDERA, @NIT, @TIPONIT", @param1, @param2, @param3);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void actualizarEstado(string NumeroSolicitud, string Estado)
        {
            try
            {
                //SqlParameter param1 = new SqlParameter("@NumeroSolicitud", NumeroSolicitud);
                //SqlParameter param2 = new SqlParameter("@Estado", Estado);

                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<object>("DECLARE @IdSolicitudFYC AS VARCHAR(20) " +
                                                                                   "SELECT @IdSolicitudFYC = FSC_NUMSOLICITUD " +
                                                                                   "FROM " + DB + ".dbo.SOLICITUD_CREDITO " +
                                                                                   "WHERE ConsecutivoSolicitud = '" + NumeroSolicitud + "'" +
                                                                                   "UPDATE CAVCRM.[CAVIPETROL].[dbo].[FYC_SOLICITUD_CREDITO] " +
                                                                                   "SET[FSC_ESTADO] = '" + Estado + "'" +
                                                                                   "WHERE[FSC_NUMSOLICITUD] = @IdSolicitudFYC");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ObjectResult<ArbolAmortizacion> ObtenerArbolAmortizacion(string idtipocredito, string idpapelcavipetrol)
        {
            try
            {

                SqlParameter param1 = new SqlParameter("@idtipocredito", idtipocredito);
                SqlParameter param2 = new SqlParameter("@idpapelcavipetrol", idpapelcavipetrol);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ArbolAmortizacion>("" + DB + ".dbo.PA_OBTENER_ARBOL_AMORTIZACION @idtipocredito,@idpapelcavipetrol", @param1, @param2);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual void GuardarSolicitudInterproductos(int IdSolicitud, string IdProducto, decimal Valor, string DocumentoTipo, string DocumentoNumero)
        {
            SqlParameter param1 = new SqlParameter("@IdSolicitud", IdSolicitud);
            SqlParameter param2 = new SqlParameter("@IdProducto", IdProducto);
            SqlParameter param3 = new SqlParameter("@Valor", Valor);
            SqlParameter param4 = new SqlParameter("@DocumentoTipo", DocumentoTipo);
            SqlParameter param5 = new SqlParameter("@DocumentoNumero", DocumentoNumero);

            ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("[dbo].[INSERTAR_SOLICITUD_INTERPRODUCTOS] @IdSolicitud,@IdProducto,@Valor,@DocumentoTipo,@DocumentoNumero", @param1, @param2, @param3, @param4, @param5);
        }

        //HACE LA EJECUCION DE LA FUNCIONALIDAD SQL BULK COPY PARA LA INSERCION MASIVA DE DATOS EN LA BD </jarp>
        public void GuardaDatosCartasCrViviendaECPTmp(DataTable DatosCartas)
        {
            try
            {
                string conString = string.Empty;
                conString = ConfigurationManager.ConnectionStrings["STAGE"].ConnectionString;
                SqlConnection con = new SqlConnection(conString);
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con);
                con.Open();
                SqlCommand limpiarTabla = new SqlCommand("DELETE FROM dbo.TMP_CARTAS_ADJUDICACION_CR_VIVIENDA_ECP;", con);
                limpiarTabla.ExecuteNonQuery();
                sqlBulkCopy.DestinationTableName = "dbo.TMP_CARTAS_ADJUDICACION_CR_VIVIENDA_ECP";
                sqlBulkCopy.WriteToServer(DatosCartas);
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // SELECCIONA DATOS CON BASE A UNAS CONDICIONES DISPUESTA EN EL SP OPERACION 1 </jarp>
        public virtual ObjectResult<DatosCartasCrViviendaEcopetrolConsultaErrores> ValidaObtieneGuardaDatosCartasCrViviendaECP()
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 1);
                SqlParameter param2 = new SqlParameter("@FechaCargue", "");

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosCartasCrViviendaEcopetrolConsultaErrores>("" + DB + ".dbo.PA_GESTION_CARTAS_CR_VIVIENDA_ECP @Operacion, @FechaCargue", @param1, param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // SELECCIONA DATOS CON BASE A UNAS CONDICIONES DISPUESTA EN EL SP OPERACION 2 </jarp>
        public virtual ObjectResult<DatosCartasCrViviendaEcopetrolConsulta> ObtenerDatosCartasECP(string fechaCargue)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 2);
                SqlParameter param2 = new SqlParameter("@FechaCargue", fechaCargue);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosCartasCrViviendaEcopetrolConsulta>("" + DB + ".dbo.PA_GESTION_CARTAS_CR_VIVIENDA_ECP @Operacion, @FechaCargue", @param1, param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //HACE LA EJECUCION DE LA FUNCIONALIDAD SQL BULK COPY PARA LA INSERCION MASIVA DE DATOS QUE SE ACTUALIZARAN EN LA BD </jarp>
        public void GuardarDatosCartasActualizadasCrViviendaECPTmp(DataTable DatosCartas)
        {
            try
            {
                string conString = string.Empty;
                conString = ConfigurationManager.ConnectionStrings["STAGE"].ConnectionString;
                SqlConnection con = new SqlConnection(conString);
                SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con);
                con.Open();
                SqlCommand limpiarTabla = new SqlCommand("DELETE FROM dbo.TMP_ACTUALIZAR_CARTAS_ADJUDICACION_CR_VIVIENDA_ECP;", con);
                limpiarTabla.ExecuteNonQuery();
                sqlBulkCopy.DestinationTableName = "dbo.TMP_ACTUALIZAR_CARTAS_ADJUDICACION_CR_VIVIENDA_ECP";
                sqlBulkCopy.WriteToServer(DatosCartas);
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // SELECCIONA DATOS CON BASE A UNAS CONDICIONES DISPUESTA EN EL SP OPERACION 3 </jarp>
        public virtual ObjectResult<DatosCartasActualizarCrViviendaEcopetrolConsultaErrores> ValidaObtieneActualizaDatosCartasCrViviendaECP()
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 3);
                SqlParameter param2 = new SqlParameter("@FechaCargue", "".ToString());
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosCartasActualizarCrViviendaEcopetrolConsultaErrores>("" + DB + ".dbo.PA_GESTION_CARTAS_CR_VIVIENDA_ECP @Operacion, @FechaCargue", @param1, param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GENERA LA INICIACION DEL PROCESO DE CREDITO</jarp>
        public virtual object IniciarProcesoCreditoECP(int PI_Id_Formulario, int PI_Numero_de_Identificacion, int PI_Comodin, string PO_Url, int PO_Bandera, string PO_Proceso)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@PI_Id_Formulario", PI_Id_Formulario);
                SqlParameter param2 = new SqlParameter("@PI_Numero_de_Identificacion", PI_Numero_de_Identificacion);
                SqlParameter param3 = new SqlParameter("@PI_Comodin", PI_Comodin);
                SqlParameter param4 = new SqlParameter("@PO_Url", PO_Url);
                SqlParameter param5 = new SqlParameter("@PO_Bandera", PO_Bandera);
                SqlParameter param6 = new SqlParameter("@PO_Proceso", PO_Proceso);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("" + DB + ".dbo.PA_CONSULTA_PROCESO_ECP @PI_Id_Formulario, @PI_Numero_de_Identificacion, @PI_Comodin, @PO_Url, @PO_Bandera, @PO_Proceso ", @param1, param2, param3, param4, param5, param6);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene los usuos de credito
        /// </summary>
        /// <returns>Lista con los usos del credito</returns>
        public virtual ObjectResult<DatosUsosCreditos> ObtenerUsosSolicitudCreditoECP()
        {
            try
            {
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosUsosCreditos>("[" + DB + "].[dbo].[PA_CONSULTAR_USOS_ECP]");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene los usuos de credito
        /// </summary>
        /// <returns>Lista con los usos del credito</returns>
        public virtual ObjectResult<DatosUsosCreditos> ObtenerInfoUsosSolicitudCreditoECP(string NumIdetificacion)
        {
            try
            {
                SqlParameter param = new SqlParameter("@Operacion", 2);
                SqlParameter param1 = new SqlParameter("@NumIdetificacion", NumIdetificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosUsosCreditos>("[" + DB + "].[dbo].[PA_OBTENER_INFO_SOLICITUD_CREDITO_ECP] @Operacion, @NumIdetificacion", param, param1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene los datos de la hipoteca
        /// </summary>
        /// <returns>Lista con los usos del credito</returns>
        public virtual ObjectResult<DatosHipotecaPersistencia> ObtenerInfoHipotecaSolicitudECP(string NumIdentificacion)
        {
            try
            {
                SqlParameter param = new SqlParameter("@Operacion", 4);
                SqlParameter param1 = new SqlParameter("@NumIdetificacion", NumIdentificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosHipotecaPersistencia>("[" + DB + "].[dbo].[PA_OBTENER_INFO_SOLICITUD_CREDITO_ECP] @Operacion, @NumIdetificacion", param, param1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene las formas de pago segun la norma
        /// </summary>
        /// <param name="norma">Norma que se adjudicó</param>
        /// <returns>Lista con las formas de pago segun la norma adjudicada</returns>
        public virtual ObjectResult<DatosFormaPago> ObtenerFormasPago(string norma)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Norma", norma);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosFormaPago>("[" + DB + "].[dbo].[PA_CONSULTAR_FORMA_PAGO_ECP] @Norma", @param1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene los datos de la carta de adjudicación
        /// </summary>
        /// <param name="nIdentificacion">Número de identificacion</param>
        /// <returns>Los datos de la carta adjudicada</returns>
        public virtual ObjectResult<DatosCartasCrViviendaEcopetrolConsulta> ObtenerDatosCartaECP(string nIdentificacion)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@PI_Cedula", nIdentificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosCartasCrViviendaEcopetrolConsulta>("[" + DB + "].[dbo].[PA_CONSULTA_CARGA_ADJUDICACION_ECP] @PI_Cedula", @param1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public virtual ObjectResult<int> ValidarSolicitud(int IdConsecutivo)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@IdConsecutivo", IdConsecutivo);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<int>("[" + DB + "].[dbo].[PA_CONSULTA_VALIDA_SOLICITUD_ECP] @IdConsecutivo", @param1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void GuardarUsosCreditoECP(int IdUsoCredito, int IdConsecutivo, decimal Valor, Boolean Activo)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@IdUsoCredito", IdUsoCredito);
                SqlParameter param2 = new SqlParameter("@IdConsecutivo", IdConsecutivo);
                SqlParameter param3 = new SqlParameter("@Valor", Valor);
                SqlParameter param4 = new SqlParameter("@Activo", Activo);
                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("[" + DB + "].[dbo].[PA_INSERTAR_SOLICITUD_USOS_CREDITO_ECP] @IdUsoCredito,@IdConsecutivo,@Valor,@Activo", @param1, @param2, @param3, @param4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void GuardarFormasPagoECP(int IdProductoNOrma, int IdConsecutivo, int Plazo, decimal Valor, decimal Cuota)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@IdProductoNOrma", IdProductoNOrma);
                SqlParameter param2 = new SqlParameter("@IdConsecutivo", IdConsecutivo);
                SqlParameter param3 = new SqlParameter("@Plazo", Plazo);
                SqlParameter param4 = new SqlParameter("@Valor", Valor);
                SqlParameter param5 = new SqlParameter("@Cuota", Cuota);
                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("[" + DB + "].[dbo].[PA_INSERTAR_FORMA_PAGO_SOLICITUD_ECP] @IdProductoNOrma,@IdConsecutivo,@Plazo,@Valor,@Cuota", @param1, @param2, @param3, @param4, @param5);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ObjectResult<string> GuardarSolicitudCreditoISYNETECP(int IdConsecutivo, decimal MontoSolicitado, decimal MontoCheque, decimal MontoFAI, int IdSolicitudFYC)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@IdConsecutivo", IdConsecutivo);
                SqlParameter param2 = new SqlParameter("@MontoSolicitado", MontoSolicitado);
                SqlParameter param3 = new SqlParameter("@MontoCheque", MontoCheque);
                SqlParameter param4 = new SqlParameter("@MontoFAI", MontoFAI);
                SqlParameter param5 = new SqlParameter("@IdSolicitudFYC", IdSolicitudFYC);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<string>("[" + DB + "].[dbo].[PA_INSERTAR_SOLICITUD_ISYNET_ECP] @IdConsecutivo,@MontoSolicitado,@MontoCheque,@MontoFAI,@IdSolicitudFYC", @param1, @param2, @param3, @param4, @param5);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<DatosHipoteca> ObtenerHipotecaECP(string nIdentificacion, string sIdConsecutivo)
        {
            int IdConsecutivo = Int32.Parse(sIdConsecutivo);
            try
            {
                SqlParameter param1 = new SqlParameter("@nIdentificacion", nIdentificacion);
                SqlParameter param2 = new SqlParameter("@IdConsecutivo", IdConsecutivo);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosHipoteca>("[" + DB + "].[dbo].[PA_CONSULTAR_HIPOTECA_ECP] @nIdentificacion,@IdConsecutivo", @param1, @param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<DatosGeneralesECP> ValidaNumSolicitudFYC(string Consecutivo)
        {

            int IdConsecutivo = Int32.Parse(Consecutivo);
            try
            {
                SqlParameter param = new SqlParameter("@Consecutivo", IdConsecutivo);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosGeneralesECP>("[" + DB + "].[dbo].[PA_ECP_VALIDAR_EXISTE_NUMSOLICITUD] @Consecutivo", @param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void GuardarHipotecaECP(
                    string HDireccion, decimal HAvaluoComercial, decimal H80AvaluoComercial, decimal HValorConstruccion, string HAvaluador,
                    string HIdentificacion, string HMatriculaInmobiliaria, string HCiudad, string HNumeroEscritura, string HNotaria, string HFecha,
                    string HCiudadNotaria, string HClaseHipoteca, string HFechaSeguros, string HFechaCobroECP, string HOtroCredito, string HSeguroVida,
                    string HSeguroIncendio, int IdConsecutivo)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@HDireccion", HDireccion);
                SqlParameter param2 = new SqlParameter("@HAvaluoComercial", HAvaluoComercial);
                SqlParameter param3 = new SqlParameter("@H80AvaluoComercial", H80AvaluoComercial);
                SqlParameter param4 = new SqlParameter("@HValorConstruccion", HValorConstruccion);
                SqlParameter param5 = new SqlParameter("@HAvaluador", HAvaluador);
                SqlParameter param6 = new SqlParameter("@HIdentificacion", HIdentificacion);
                SqlParameter param7 = new SqlParameter("@HMatriculaInmobiliaria", HMatriculaInmobiliaria);
                SqlParameter param8 = new SqlParameter("@HCiudad", HCiudad);
                SqlParameter param9 = new SqlParameter("@HNumeroEscritura", HNumeroEscritura);
                SqlParameter param10 = new SqlParameter("@HNotaria", HNotaria);
                SqlParameter param11 = new SqlParameter("@HFecha", HFecha);
                SqlParameter param12 = new SqlParameter("@HCiudadNotaria", HCiudadNotaria);
                SqlParameter param13 = new SqlParameter("@HClaseHipoteca", HClaseHipoteca);
                SqlParameter param14 = new SqlParameter("@HFechaSeguros", HFechaSeguros);
                SqlParameter param15 = new SqlParameter("@HFechaCobroECP", HFechaCobroECP);
                SqlParameter param16 = new SqlParameter("@HOtroCredito", HOtroCredito);
                SqlParameter param17 = new SqlParameter("@HSeguroVida", HSeguroVida);
                SqlParameter param18 = new SqlParameter("@HSeguroIncendio", HSeguroIncendio);
                SqlParameter param19 = new SqlParameter("@IdConsecutivo", IdConsecutivo);

                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand(
                        "[" + DB + "].[dbo].[PA_INSERTAR_HIPOTECA_ECP]   @HDireccion,@HAvaluoComercial,@H80AvaluoComercial,@HValorConstruccion,@HAvaluador," +
                        "@HIdentificacion,@HMatriculaInmobiliaria,@HCiudad,@HNumeroEscritura,@HNotaria,@HFecha,@HCiudadNotaria,@HClaseHipoteca," +
                        "@HFechaSeguros,@HFechaCobroECP,@HOtroCredito,@HSeguroVida,@HSeguroIncendio,@IdConsecutivo", @param1, @param2, @param3, @param4,
                        @param5, @param6, @param7, @param8, @param9, @param10, @param11, @param12, @param13, @param14, @param15, @param16, @param17, @param18, @param19);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public virtual void GuardarChequesECP(
                    string Tipo_Registro, Boolean Activo, Boolean Habilitar_Eliminacion, string Tipo_Id_Destinatario, string Id_Destinatario,
                    string Nombre_Destinatario, string Tipo_Id_Quien_Recibe, string Id_Quien_Recibe, string Nombre_Quien_Recibe, string Fecha_Desembolso,
                    decimal Monto, string Fecha_Proceso, int IdConsecutivo)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Tipo_Registro", Tipo_Registro);
                SqlParameter param2 = new SqlParameter("@Activo", Activo);
                SqlParameter param3 = new SqlParameter("@Habilitar_Eliminacion", Habilitar_Eliminacion);
                SqlParameter param4 = new SqlParameter("@Tipo_Id_Destinatario", Tipo_Id_Destinatario);
                SqlParameter param5 = new SqlParameter("@Id_Destinatario", Id_Destinatario);
                SqlParameter param6 = new SqlParameter("@Nombre_Destinatario", Nombre_Destinatario);
                SqlParameter param7 = new SqlParameter("@Tipo_Id_Quien_Recibe", Tipo_Id_Quien_Recibe);
                SqlParameter param8 = new SqlParameter("@Id_Quien_Recibe", Id_Quien_Recibe);
                SqlParameter param9 = new SqlParameter("@Nombre_Quien_Recibe", Nombre_Quien_Recibe);
                SqlParameter param10 = new SqlParameter("@Fecha_Desembolso", Fecha_Desembolso);
                SqlParameter param11 = new SqlParameter("@Monto", Monto);
                SqlParameter param12 = new SqlParameter("@Fecha_Proceso", Fecha_Proceso);
                SqlParameter param13 = new SqlParameter("@IdConsecutivo", IdConsecutivo);


                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand(
                        "[" + DB + "].[dbo].[PA_INSERTAR_CHEQUES_ECP]   @Tipo_Registro,@Activo,@Habilitar_Eliminacion,@Tipo_Id_Destinatario,@Id_Destinatario," +
                        "@Nombre_Destinatario,@Tipo_Id_Quien_Recibe,@Id_Quien_Recibe,@Nombre_Quien_Recibe,@Fecha_Desembolso,@Monto,@Fecha_Proceso,@IdConsecutivo",
                        @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10, @param11, @param12, @param13);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // SELECCIONA DATOS DE LA CARGA FILTRANDO POR CEDULA </jarp>
        public virtual ObjectResult<DatosCartasCrViviendaEcopetrolConsultaCarga> ObtenerDatosCargaAdjudicacion(string numeroIdentificacion, string tipoDocumento)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@PI_Cedula", numeroIdentificacion);
                SqlParameter param2 = new SqlParameter("@PI_TipoDocumento", tipoDocumento);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosCartasCrViviendaEcopetrolConsultaCarga>("" + DB + ".dbo.PA_CONSULTA_CARGA_ADJUDICACION_ECP @PI_Cedula, @PI_TipoDocumento", @param1, param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // SELECCIONA DATOS DE LA CARGA FILTRANDO POR CEDULA </jarp>
        public virtual ObjectResult<DatosCartasCrViviendaEcopetrolConsultaHistoricoBitacora> ObtenerDatosHistoricoGestion(string numeroIdentificacion)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@PI_Numero_De_Identificacion", numeroIdentificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosCartasCrViviendaEcopetrolConsultaHistoricoBitacora>("" + DB + ".dbo.PA_CONSULTA_HISTORICO_BITACORA_ECP @PI_Numero_De_Identificacion", @param1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<DatosCartasCrViviendaEcopetrolConsultaREPORTES_SES> ObtenerDatosAsegurabilidad_REPORTES_SES(string numeroIdentificacion, string tipoidentificacion)
        {
            try
            {

                SqlParameter param1 = new SqlParameter("@DOCUMENTO_IDENTIFICACION", numeroIdentificacion);
                SqlParameter param2 = new SqlParameter("@TIPO_IDENTIFICACION", tipoidentificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosCartasCrViviendaEcopetrolConsultaREPORTES_SES>("dbo.PA_CONSULTAR_INFORMACION_USUARIO_SOLICITANTE_CREDITO @DOCUMENTO_IDENTIFICACION, @TIPO_IDENTIFICACION", @param1, param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<DatosCartasCrViviendaEcopetrolConsultaTipoProducto> ObtenerTipoProducto(string Comodin, string Filtro)
        {
            try
            {

                SqlParameter param1 = new SqlParameter("@PI_Comodin", Comodin);
                SqlParameter param2 = new SqlParameter("@PI_Filtro", Filtro);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosCartasCrViviendaEcopetrolConsultaTipoProducto>("" + DB + ".dbo.PA_CONSULTA_PRODUCTO_ECP @PI_Comodin, @PI_Filtro", @param1, param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<DatosCartasCrViviendaEcopetrolConsultaTipoNorma> ObtenerTipoNorma(string Comodin, string Filtro)
        {
            try
            {

                SqlParameter param1 = new SqlParameter("@PI_Comodin", Comodin);
                SqlParameter param2 = new SqlParameter("@PI_Filtro", Filtro);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosCartasCrViviendaEcopetrolConsultaTipoNorma>("" + DB + ".dbo.PA_CONSULTA_NORMA_ECP @PI_Comodin, @PI_Filtro", @param1, param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<DatosCartasCrViviendaEcopetrolConsultaTipoGestion> ObtenerTipoGestion()
        {
            try
            {
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosCartasCrViviendaEcopetrolConsultaTipoGestion>("" + DB + ".dbo.PA_CONSULTA_TIPO_GESTION_ECP");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<DatosCartasCrViviendaEcopetrolConsultaMenu> ObtenerMenu(string numeroIdentificacion, string comodin)
        {
            try
            {
                /*
                 Comodin 1 = Obtiene todos los menus cuyo proceso de gestion son 1 y no transversales
                 Comodin 2 = Obtiene el IdMenu filtrando por el nombre.
                 */
                SqlParameter param1 = new SqlParameter("@PI_Numero_De_Identificacion", numeroIdentificacion);
                SqlParameter param2 = new SqlParameter("@PI_Comodin1", Convert.ToInt16(comodin));
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosCartasCrViviendaEcopetrolConsultaMenu>("" + DB + ".dbo.PA_CONSULTA_MENU_ECP @PI_Numero_De_Identificacion,@PI_Comodin1", @param1, param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<ObjectInt> GuardarGestionBitacora(CreditoEcopetrolBitacora BitacoraModel)
        {
            try
            {
                SqlParameter PI_Consecutivo = new SqlParameter("@PI_Consecutivo", BitacoraModel.BitConsecutivo);
                SqlParameter PI_Id_TipoGestion = new SqlParameter("@PI_Id_TipoGestion", BitacoraModel.BitIdTipoGestion);
                SqlParameter PI_IdUsuario = new SqlParameter("@PI_IdUsuario", BitacoraModel.BitIdUsuario);
                SqlParameter PI_Numero_De_Identificacion = new SqlParameter("@PI_Numero_De_Identificacion", BitacoraModel.BitNumeroIdentificacion);
                SqlParameter PI_Observaciones_HistoricoGestion = new SqlParameter("@PI_Observaciones_HistoricoGestion", BitacoraModel.BitObservaciones);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ObjectInt>
                    ("" + DB + ".dbo.PA_GESTION_HISTORICO_ECP @PI_Consecutivo, @PI_Id_TipoGestion, @PI_IdUsuario, @PI_Numero_De_Identificacion, @PI_Observaciones_HistoricoGestion",
               @PI_Consecutivo, @PI_Id_TipoGestion, @PI_IdUsuario, @PI_Numero_De_Identificacion, @PI_Observaciones_HistoricoGestion);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public virtual ObjectResult<ObjectInt> GuardarGestionAsegurabilidad(CreditoEcopetrolAsegurabilidad AsegurabilidadModel)
        {
            try
            {
                if (AsegurabilidadModel.AsegEstado_Asegurabilidad == null)
                {
                    AsegurabilidadModel.AsegEstado_Asegurabilidad = "0";
                }
                if (AsegurabilidadModel.AsegVigencia_Asegurabilidad == null)
                {
                    AsegurabilidadModel.AsegVigencia_Asegurabilidad = "0";
                }
                if (AsegurabilidadModel.AsegExtraPrima_Asegurabilidad == null)
                {
                    AsegurabilidadModel.AsegExtraPrima_Asegurabilidad = "0";
                }
                if (AsegurabilidadModel.AsegRechazoAplazo_Asegurabilidad == null)
                {
                    AsegurabilidadModel.AsegRechazoAplazo_Asegurabilidad = "0";
                }
                if (AsegurabilidadModel.AsegObservaciones_Asegurabilidad == null)
                {
                    AsegurabilidadModel.AsegObservaciones_Asegurabilidad = "0";
                }


                SqlParameter PI_Consecutivo = new SqlParameter("@PI_Consecutivo", AsegurabilidadModel.AsegConsecutivo);
                SqlParameter PI_IdUsuario = new SqlParameter("@PI_IdUsuario", AsegurabilidadModel.AsegIdUsuario);
                SqlParameter PI_Numero_De_Identificacion = new SqlParameter("@PI_Numero_De_Identificacion", AsegurabilidadModel.AsegNumeroIdentificacion);
                SqlParameter PI_Estado_Asegurabilidad = new SqlParameter("@PI_Estado_Asegurabilidad", AsegurabilidadModel.AsegEstado_Asegurabilidad);
                SqlParameter PI_Vigencia_Asegurabilidad = new SqlParameter("@PI_Vigencia_Asegurabilidad", AsegurabilidadModel.AsegVigencia_Asegurabilidad);
                SqlParameter PI_ExtraPrima_Asegurabilidad = new SqlParameter("@PI_ExtraPrima_Asegurabilidad", AsegurabilidadModel.AsegExtraPrima_Asegurabilidad);
                SqlParameter PI_RechazoAplazo_Asegurabilidad = new SqlParameter("@PI_RechazoAplazo_Asegurabilidad", AsegurabilidadModel.AsegRechazoAplazo_Asegurabilidad);
                SqlParameter PI_Observaciones_Asegurabilidad = new SqlParameter("@PI_Observaciones_Asegurabilidad", AsegurabilidadModel.AsegObservaciones_Asegurabilidad);
                SqlParameter PI_Observaciones_Gestion_Asegurabilidad = new SqlParameter("@PI_Observaciones_Gestion_Asegurabilidad", AsegurabilidadModel.AsegObservaciones_Gestion_Asegurabilidad);




                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ObjectInt>
                    ("" + DB + ".dbo.PA_GESTION_ASEGURABILIDAD_ECP" +
                    " @PI_Consecutivo," +
                     " @PI_IdUsuario," +
                     " @PI_Numero_De_Identificacion," +
                     " @PI_Estado_Asegurabilidad," +
                     " @PI_Vigencia_Asegurabilidad," +
                     " @PI_ExtraPrima_Asegurabilidad," +
                     " @PI_RechazoAplazo_Asegurabilidad," +
                     " @PI_Observaciones_Asegurabilidad," +
                     " @PI_Observaciones_Gestion_Asegurabilidad",
               @PI_Consecutivo,
               @PI_IdUsuario,
               @PI_Numero_De_Identificacion,
               @PI_Estado_Asegurabilidad,
               @PI_Vigencia_Asegurabilidad,
               @PI_ExtraPrima_Asegurabilidad,
               @PI_RechazoAplazo_Asegurabilidad,
               @PI_Observaciones_Asegurabilidad,
               @PI_Observaciones_Gestion_Asegurabilidad
               );

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public virtual ObjectResult<int> GuardarAdjudicacion(CreditoEcopetrolAdjudicacion AdjudicacionModel)
        {
            try
            {


                SqlParameter PI_Consecutivo = new SqlParameter("@PI_Consecutivo", AdjudicacionModel.AdjConsecutivo);
                SqlParameter PI_IdUsuario = new SqlParameter("@PI_IdUsuario", AdjudicacionModel.AdjIdUsuario);
                SqlParameter PI_Id_Producto = new SqlParameter("@PI_Id_Producto", AdjudicacionModel.AdjId_Producto);
                SqlParameter PI_Id_Norma = new SqlParameter("@PI_Id_Norma", AdjudicacionModel.AdjId_Norma);
                SqlParameter PI_Numero_Identificacion = new SqlParameter("@PI_Numero_Identificacion", AdjudicacionModel.AdjNumero_Identificacion);
                SqlParameter PI_Papel_Adjudicacion = new SqlParameter("@PI_Papel_Adjudicacion", AdjudicacionModel.AdjPapel_Adjudicacion);
                SqlParameter PI_Registro_Adjudicacion = new SqlParameter("@PI_Registro_Adjudicacion", AdjudicacionModel.AdjRegistro_Adjudicacion);
                SqlParameter PI_Acta_Adjudicacion = new SqlParameter("@PI_Acta_Adjudicacion", AdjudicacionModel.AdjActa_Adjudicacion);
                SqlParameter PI_Tipo_Adjudicacion = new SqlParameter("@PI_Tipo_Adjudicacion", AdjudicacionModel.AdjTipo_Adjudicacion);
                SqlParameter PI_Valor_Adjudicacion = new SqlParameter("@PI_Valor_Adjudicacion", AdjudicacionModel.AdjValor_Adjudicacion);
                SqlParameter PI_Fecha_Generacion = new SqlParameter("@PI_Fecha_Generacion", AdjudicacionModel.AdjFecha_Generacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<int>("" + DB + ".dbo.PA_GESTION_ADJUDICACION_ECP @PI_Consecutivo,@PI_IdUsuario,@PI_Id_Producto,@PI_Id_Norma,@PI_Numero_Identificacion,@PI_Papel_Adjudicacion,@PI_Registro_Adjudicacion,@PI_Acta_Adjudicacion,@PI_Tipo_Adjudicacion,@PI_Valor_Adjudicacion, @PI_Fecha_Generacion", @PI_Consecutivo, @PI_IdUsuario, @PI_Id_Producto, @PI_Id_Norma, @PI_Numero_Identificacion, @PI_Papel_Adjudicacion, @PI_Registro_Adjudicacion, @PI_Acta_Adjudicacion, @PI_Tipo_Adjudicacion, @PI_Valor_Adjudicacion, @PI_Fecha_Generacion);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void CrearEstudioTitulo(EstudioTitulos InformacionTitulo)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@ECPT_CIUDADEXPEDICIONCEDULA", InformacionTitulo.ECPT_CiudadExpedicionCedula);
                SqlParameter param2 = new SqlParameter("@ECPT_CIUDADINMUEBLE", InformacionTitulo.ECPT_CiudadInmueble);
                SqlParameter param3 = new SqlParameter("@ECPT_CIUDADFIRMAESCRITURA", InformacionTitulo.ECPT_CiudadFirmaEscritura);
                SqlParameter param4 = new SqlParameter("@ECPT_NOMBREVENDEDOR", InformacionTitulo.ECPT_NombreVendedor);
                SqlParameter param5 = new SqlParameter("@ECPT_CEDULADEUDOR", InformacionTitulo.ECPT_CedulaDeudor);
                SqlParameter param6 = new SqlParameter("@ECPT_NOMBREDEUDOR", InformacionTitulo.ECPT_NombreDeudor);
                SqlParameter param7 = new SqlParameter("@ECPT_REGISTRODEUDOR", InformacionTitulo.ECPT_RegistroDeudor);
                SqlParameter param8 = new SqlParameter("@ECPT_DIRECCIONINMUEBLE", InformacionTitulo.ECPT_DireccionInmueble);
                SqlParameter param9 = new SqlParameter("@ECPT_FECHAGENERACION", InformacionTitulo.ECPT_FechaGeneracion);
                SqlParameter param10 = new SqlParameter("@ECPT_FECHAADJUDICACION", InformacionTitulo.ECPT_FechaAdjudicacion);
                SqlParameter param11 = new SqlParameter("@ECPT_CIUDADDEUDOR", InformacionTitulo.ECPT_CiudadDeudor);
                SqlParameter param12 = new SqlParameter("@ECPT_MATRICULAINMOBILIARIA", InformacionTitulo.ECPT_MatriculaInmobiliaria);
                SqlParameter param13 = new SqlParameter("@ECPT_DESCRIPCIONMATRICULA", InformacionTitulo.ECPT_DescripcionMatricula);
                SqlParameter param14 = new SqlParameter("@ECPT_NOTARIA", InformacionTitulo.ECPT_Notaria);
                SqlParameter param15 = new SqlParameter("@ECPT_NUMEROESCRITURA", InformacionTitulo.ECPT_NumeroEscritura);
                SqlParameter param16 = new SqlParameter("@ECPT_OFICINAPUBLICA", InformacionTitulo.ECPT_OficinaPublica);
                SqlParameter param17 = new SqlParameter("@ECPT_PLAZOHIPOTECA", InformacionTitulo.ECPT_PlazoHipoteca);
                SqlParameter param18 = new SqlParameter("@ECPT_PRODUCTO", InformacionTitulo.ECPT_Producto);
                SqlParameter param19 = new SqlParameter("@ECPT_TASAEA", InformacionTitulo.ECPT_TasaEA);
                SqlParameter param20 = new SqlParameter("@ECPT_TIEMPOPRESTAMO", InformacionTitulo.ECPT_TiempoPrestamo);
                SqlParameter param21 = new SqlParameter("@ECPT_TIPO_HIPOTECA", InformacionTitulo.ECPT_Tipo_Hipoteca);
                SqlParameter param22 = new SqlParameter("@ECPT_USUARIO", InformacionTitulo.ECPT_Usuario);
                SqlParameter param23 = new SqlParameter("@ECPT_VALORPRESTADO", InformacionTitulo.ECPT_ValorPrestado);
                SqlParameter param24 = new SqlParameter("@ECPT_OBSERVACIONES", InformacionTitulo.ECPT_Observaciones);
                SqlParameter param25 = new SqlParameter("@FK_ECPE_ESTADO", InformacionTitulo.FK_ECPE_ESTADO);
                SqlParameter param26 = new SqlParameter("@FK_CONSECUTIVO", InformacionTitulo.FK_Consecutivo);
                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DatosUsosCreditos>("[" + DB + "].[dbo].[PA_CREAR_TITULO_ECP]" +
                "@ECPT_CIUDADEXPEDICIONCEDULA," +
                "@ECPT_CIUDADINMUEBLE," +
                "@ECPT_CIUDADFIRMAESCRITURA," +
                "@ECPT_NOMBREVENDEDOR," +
                "@ECPT_CEDULADEUDOR," +
                "@ECPT_NOMBREDEUDOR," +
                "@ECPT_REGISTRODEUDOR," +
                "@ECPT_DIRECCIONINMUEBLE," +
                "@ECPT_FECHAGENERACION," +
                "@ECPT_FECHAADJUDICACION," +
                "@ECPT_CIUDADDEUDOR," +
                "@ECPT_MATRICULAINMOBILIARIA," +
                "@ECPT_DESCRIPCIONMATRICULA," +
                "@ECPT_NOTARIA," +
                "@ECPT_NUMEROESCRITURA," +
                "@ECPT_OFICINAPUBLICA," +
                "@ECPT_PLAZOHIPOTECA," +
                "@ECPT_PRODUCTO," +
                "@ECPT_TASAEA," +
                "@ECPT_TIEMPOPRESTAMO," +
                "@ECPT_TIPO_HIPOTECA," +
                "@ECPT_USUARIO," +
                "@ECPT_VALORPRESTADO," +
                "@ECPT_OBSERVACIONES," +
                "@FK_ECPE_ESTADO," +
                "@FK_CONSECUTIVO", param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15, param16, param17, param18, param19, param20, param21, param22, param23, param24, param25, param26);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<ModeloCartas> ObtenerCartasECP(int cedula, int estado)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@CEDULA", cedula);
                SqlParameter param2 = new SqlParameter("@ESTADO", estado);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ModeloCartas>("" + DB + ".dbo.PA_CONSULTAR_CARTAS_ECP @CEDULA, @ESTADO", @param1, @param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void ActualizarEstudioTitulo(int concecutivo, int estado)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@CONSECUTIVO", concecutivo);
                SqlParameter param2 = new SqlParameter("@ESTADO", estado);
                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("" + DB + ".dbo.PA_ACTUALIZAR_TITULO @CONSECUTIVO, @ESTADO", @param1, @param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<EstudioTitulos> ObtenerInformacionEstudio(int concecutivo)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@CONCECUTIVO", concecutivo);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<EstudioTitulos>("" + DB + ".dbo.PA_OBTENER_INFORMACION_ESTUDIO @CONCECUTIVO", @param1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GUARDAR DATOS DE LOS DOCUMENTO ENTREGADOS POR EL ASOCIADO
        /// </summary>>
        /// <returns></returns>
        public virtual ObjectResult<DocumentosAsociadosCrViviendaEcopetrol> GuardarDocumentoAsociado(int IdUso, int IdDocumento, string NumeroIdentificacion, string Observacion)
        {
            try
            {
                SqlParameter param = new SqlParameter("@Operacion", 1);
                SqlParameter param1 = new SqlParameter("@IdUso", IdUso);
                SqlParameter param2 = new SqlParameter("@IdDocumento", IdDocumento);
                SqlParameter param3 = new SqlParameter("@NumeroIdentificacion", NumeroIdentificacion);
                SqlParameter param4 = new SqlParameter("@Observacion", Observacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DocumentosAsociadosCrViviendaEcopetrol>("" + DB + ".dbo.PA_GESTION_USOS_DOCUMENTOS_ASOCIADOS @Operacion,@IdUso, @IdDocumento, @NumeroIdentificacion, @Observacion", @param, param1, param2, param3, param4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GUARDAR DATOS DE LOS DOCUMENTO QUE NO APLICAN POR EL ASOCIADO
        /// </summary>>
        /// <returns></returns>
        public virtual ObjectResult<DocumentosAsociadosCrViviendaEcopetrol> GuardarDocumentoNoAplicaAsociado(int IdUso, int IdDocumento, string NumeroIdentificacion, string Observacion)
        {
            try
            {
                SqlParameter param = new SqlParameter("@Operacion", 2);
                SqlParameter param1 = new SqlParameter("@IdUso", IdUso);
                SqlParameter param2 = new SqlParameter("@IdDocumento", IdDocumento);
                SqlParameter param3 = new SqlParameter("@NumeroIdentificacion", NumeroIdentificacion);
                SqlParameter param4 = new SqlParameter("@Observacion", Observacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DocumentosAsociadosCrViviendaEcopetrol>("" + DB + ".dbo.PA_GESTION_USOS_DOCUMENTOS_ASOCIADOS @Operacion,@IdUso, @IdDocumento, @NumeroIdentificacion, @Observacion", @param, param1, param2, param3, param4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public virtual ObjectResult<ObjectInt> GuardarEnvioEstudio(CreditoEcopetrolEstudio EstudioModel)
        {
            try
            {
                SqlParameter PI_Consecutivo = new SqlParameter("@ECPR_Consecutivo", EstudioModel.EstConsecutivo);
                SqlParameter PI_NumeroIdentificacion = new SqlParameter("@ECPR_NumeroIdentificacion", EstudioModel.EstDocumento);
                SqlParameter PI_Estado = new SqlParameter("@FK_Estado", EstudioModel.EstEstado);


                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ObjectInt>
                    ("" + DB + ".dbo.PA_CREAR_TITULO @ECPR_Consecutivo, @ECPR_NumeroIdentificacion, @FK_Estado",
               @PI_Consecutivo, @PI_NumeroIdentificacion, @PI_Estado);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // OBTIENE EL VALOR DE LA ADJUDICACION DEL CREDITO </jarp>
        public virtual ObjectResult<CreditoEcopetrolValorAdjudicacion> ObtenerInfoSolicitudCredito(string NumIdetificacion)
        {
            try
            {
                SqlParameter param = new SqlParameter("@Operacion", 1);
                SqlParameter param1 = new SqlParameter("@NumIdetificacion", NumIdetificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CreditoEcopetrolValorAdjudicacion>("[" + DB + "].[dbo].[PA_OBTENER_INFO_SOLICITUD_CREDITO_ECP] @Operacion, @NumIdetificacion", param, @param1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// devuelve el valor total de los cheques generados para comprarlos con el valor de la adjudicacion y controlar su cantidad
        /// </summary>
        /// <param name="Consecutivo"></param>
        /// <returns></returns>
        public virtual ObjectResult<CreditoEcopetrolValorCheques> ObtenerValorTotalChequesGenerados(string Identificacion)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Identificacion", Identificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CreditoEcopetrolValorCheques>("[" + DB + "].[dbo].[PA_VALIDAR_MONTO_RESTANTE_CHEQUE] @Identificacion", @param1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<ProcesoECP> ObtenerProcesoECP(int identificacion)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@NUMERO_IDENTIFICACION", identificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ProcesoECP>("" + DB + ".dbo.PA_CONSULTAR_PROCESOS_SOLICITUDES_ECP @NUMERO_IDENTIFICACION", @param1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		public virtual void ReiniciarProceso(int identificacion, int formulario)
		{
			try
			{
				SqlParameter param1 = new SqlParameter("@NUMERO_IDENTIFICACION", identificacion);
				SqlParameter param2 = new SqlParameter("@FORMULARIO", formulario);
				((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("[" + DB + "].[dbo].[PA_CABMIO_ESTADO_PROCESO_ECP] @NUMERO_IDENTIFICACION, @FORMULARIO", @param1, @param2);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public virtual ObjectResult<CreditoEcopetrolNotaContable> ObtenerDatosNotaContable(string NumeroIdentificacion, string Consecutivo)
        {
            try
            {

                SqlParameter param1 = new SqlParameter("@PI_Numero_Identificacion", NumeroIdentificacion);
                SqlParameter param2 = new SqlParameter("@PI_Consecutivo", Consecutivo);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CreditoEcopetrolNotaContable>("" + DB + ".dbo.PA_CONSULTA_NOTA_CONTABLE_ECP @PI_Numero_Identificacion, @PI_Consecutivo", @param1, param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		public virtual void AceptacionProcesoAdjudicacion(string tipoIdentificacion, string identificacion, string producto, string numeroSolicitud, string autorizacion)
		{
			try
			{
				SqlParameter param1 = new SqlParameter("@TIPO_NIT", tipoIdentificacion);
				SqlParameter param2 = new SqlParameter("@NIT", identificacion);
				SqlParameter param3 = new SqlParameter("@PRODUCTO", producto);
				SqlParameter param4 = new SqlParameter("@NUMERO_SOLICITUD", numeroSolicitud);
				SqlParameter param5 = new SqlParameter("@AUTORIZACION", autorizacion);
				((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("[" + DB + "].[dbo].[PA_ACEPTACION_ADJUDICACION] @TIPO_NIT, @NIT, @PRODUCTO, @NUMERO_SOLICITUD, @AUTORIZACION", param1, param2, param3, param4, param5);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public virtual ObjectResult<InformacionReporteECP> ObtenerInformacionReporteSolicitud(string identificacion, string consecutivo)
		{
			try
			{

				SqlParameter param1 = new SqlParameter("@NUMERO_IDENTIFICACION", identificacion);
				SqlParameter param2 = new SqlParameter("@CONSECUTIVO", consecutivo);
				return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<InformacionReporteECP>("" + DB + ".dbo.PA_OBTENER_INFORMACION_SOLICITUD @NUMERO_IDENTIFICACION, @CONSECUTIVO", @param1, param2);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public virtual ObjectResult<FormasPagoECP> ObtenerFormasPagoECP(string identificacion, string consecutivo)
		{
			try
			{

				SqlParameter param1 = new SqlParameter("@CONCECUTIVO", consecutivo);
				SqlParameter param2 = new SqlParameter("@NUMERO_IDENTIFICACION", identificacion);
				return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<FormasPagoECP>("" + DB + ".dbo.PA_OBTENER_FORMAS_PAGO_SOLICITUD_ECP @CONCECUTIVO, @NUMERO_IDENTIFICACION", @param1, param2);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public virtual ObjectResult<string> GestionPagare(CreditoEcopetrolPagare PagareModel)
        {
            try
            {


                SqlParameter PI_Comodin = new SqlParameter("@PI_Comodin", PagareModel.Comodin);
                SqlParameter PI_Numero_Identificacion = new SqlParameter("@PI_Numero_Identificacion", PagareModel.Numero_Identificacion);
                SqlParameter PI_IdUsuario = new SqlParameter("@PI_IdUsuario", PagareModel.IdUsuario);
                SqlParameter PI_Id_TipoPagare = new SqlParameter("@PI_Id_TipoPagare", PagareModel.Id_TipoPagare);
                SqlParameter PI_Numero_Solicitud = new SqlParameter("@PI_Numero_Solicitud", PagareModel.Numero_Solicitud);
                SqlParameter PI_Id_Pagare = new SqlParameter("@PI_Id_Pagare", PagareModel.Id_Pagare);
                SqlParameter PI_Campo1_PagDes = new SqlParameter("@PI_Campo1_PagDes", PagareModel.Campo1_PagDes);
                SqlParameter PI_Campo2_PagDes = new SqlParameter("@PI_Campo2_PagDes", PagareModel.Campo2_PagDes);
                SqlParameter PI_Campo3_PagDes = new SqlParameter("@PI_Campo3_PagDes", PagareModel.Campo3_PagDes);
                SqlParameter PI_Campo4_PagDes = new SqlParameter("@PI_Campo4_PagDes", PagareModel.Campo4_PagDes);
                SqlParameter PI_Campo5_PagDes = new SqlParameter("@PI_Campo5_PagDes", PagareModel.Campo5_PagDes);
                SqlParameter PI_Campo6_PagDes = new SqlParameter("@PI_Campo6_PagDes", PagareModel.Campo6_PagDes);
                SqlParameter PI_Campo7_PagDes = new SqlParameter("@PI_Campo7_PagDes", PagareModel.Campo7_PagDes);
                SqlParameter PI_Campo8_PagDes = new SqlParameter("@PI_Campo8_PagDes", PagareModel.Campo8_PagDes);
                SqlParameter PI_Campo9_PagDes = new SqlParameter("@PI_Campo9_PagDes", PagareModel.Campo9_PagDes);
                SqlParameter PI_Campo10_PagDes = new SqlParameter("@PI_Campo10_PagDes", PagareModel.Campo10_PagDes);
                SqlParameter PI_Campo11_PagDes = new SqlParameter("@PI_Campo11_PagDes", PagareModel.Campo11_PagDes);
                SqlParameter PI_Campo12_PagDes = new SqlParameter("@PI_Campo12_PagDes", PagareModel.Campo12_PagDes);
                SqlParameter PI_Campo13_PagDes = new SqlParameter("@PI_Campo13_PagDes", PagareModel.Campo13_PagDes);
                SqlParameter PI_Campo14_PagDes = new SqlParameter("@PI_Campo14_PagDes", PagareModel.Campo14_PagDes);
                SqlParameter PI_Campo15_PagDes = new SqlParameter("@PI_Campo15_PagDes", PagareModel.Campo15_PagDes);
                SqlParameter PI_Campo16_PagDes = new SqlParameter("@PI_Campo16_PagDes", PagareModel.Campo16_PagDes);
                SqlParameter PI_Campo17_PagDes = new SqlParameter("@PI_Campo17_PagDes", PagareModel.Campo17_PagDes);
                SqlParameter PI_Campo18_PagDes = new SqlParameter("@PI_Campo18_PagDes", PagareModel.Campo18_PagDes);
                SqlParameter PI_Campo19_PagDes = new SqlParameter("@PI_Campo19_PagDes", PagareModel.Campo19_PagDes);
                SqlParameter PI_Campo20_PagDes = new SqlParameter("@PI_Campo20_PagDes", PagareModel.Campo20_PagDes);
                SqlParameter PI_Campo21_PagDes = new SqlParameter("@PI_Campo21_PagDes", PagareModel.Campo21_PagDes);
                SqlParameter PI_Campo22_PagDes = new SqlParameter("@PI_Campo22_PagDes", PagareModel.Campo22_PagDes);
                SqlParameter PI_Campo23_PagDes = new SqlParameter("@PI_Campo23_PagDes", PagareModel.Campo23_PagDes);
                SqlParameter PI_Campo24_PagDes = new SqlParameter("@PI_Campo24_PagDes", PagareModel.Campo24_PagDes);
                SqlParameter PI_Campo25_PagDes = new SqlParameter("@PI_Campo25_PagDes", PagareModel.Campo25_PagDes);
                SqlParameter PI_Campo26_PagDes = new SqlParameter("@PI_Campo26_PagDes", PagareModel.Campo26_PagDes);
                SqlParameter PI_Campo27_PagDes = new SqlParameter("@PI_Campo27_PagDes", PagareModel.Campo27_PagDes);
                SqlParameter PI_Campo28_PagDes = new SqlParameter("@PI_Campo28_PagDes", PagareModel.Campo28_PagDes);
                SqlParameter PI_Campo29_PagDes = new SqlParameter("@PI_Campo29_PagDes", PagareModel.Campo29_PagDes);
                SqlParameter PI_Campo30_PagDes = new SqlParameter("@PI_Campo30_PagDes", PagareModel.Campo30_PagDes);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<string>("" + DB + ".dbo.PA_GESTION_PAGARE @PI_Comodin,@PI_Numero_Identificacion,@PI_IdUsuario,@PI_Id_TipoPagare,@PI_Numero_Solicitud,@PI_Id_Pagare,@PI_Campo1_PagDes,@PI_Campo2_PagDes,@PI_Campo3_PagDes,@PI_Campo4_PagDes,@PI_Campo5_PagDes,@PI_Campo6_PagDes,@PI_Campo7_PagDes,@PI_Campo8_PagDes,@PI_Campo9_PagDes,@PI_Campo10_PagDes,@PI_Campo11_PagDes,@PI_Campo12_PagDes,@PI_Campo13_PagDes,@PI_Campo14_PagDes,@PI_Campo15_PagDes,@PI_Campo16_PagDes,@PI_Campo17_PagDes,@PI_Campo18_PagDes,@PI_Campo19_PagDes,@PI_Campo20_PagDes,@PI_Campo21_PagDes,@PI_Campo22_PagDes,@PI_Campo23_PagDes,@PI_Campo24_PagDes,@PI_Campo25_PagDes,@PI_Campo26_PagDes,@PI_Campo27_PagDes,@PI_Campo28_PagDes,@PI_Campo29_PagDes,@PI_Campo30_PagDes", PI_Comodin, PI_Numero_Identificacion, PI_IdUsuario, PI_Id_TipoPagare, PI_Numero_Solicitud, PI_Id_Pagare, PI_Campo1_PagDes, PI_Campo2_PagDes, PI_Campo3_PagDes, PI_Campo4_PagDes, PI_Campo5_PagDes,PI_Campo6_PagDes,PI_Campo7_PagDes,PI_Campo8_PagDes,PI_Campo9_PagDes,PI_Campo10_PagDes,PI_Campo11_PagDes,PI_Campo12_PagDes,PI_Campo13_PagDes,PI_Campo14_PagDes,PI_Campo15_PagDes,PI_Campo16_PagDes,PI_Campo17_PagDes,PI_Campo18_PagDes,PI_Campo19_PagDes,PI_Campo20_PagDes,PI_Campo21_PagDes,PI_Campo22_PagDes,PI_Campo23_PagDes,PI_Campo24_PagDes,PI_Campo25_PagDes,PI_Campo26_PagDes,PI_Campo27_PagDes,PI_Campo28_PagDes,PI_Campo29_PagDes,PI_Campo30_PagDes);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public virtual ObjectResult<CreditoEcopetrolOrdenGiro> ConsultarProgramacionGiros(string FechaInicial, string FechaFinal)
        {
            try
            {

                SqlParameter param1 = new SqlParameter("@DESDE", FechaInicial);
                SqlParameter param2 = new SqlParameter("@HASTA", FechaFinal);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CreditoEcopetrolOrdenGiro>("" + DB + ".dbo.PA_REPORTE_ECP_PAGOS_CLIENTES @DESDE, @HASTA", @param1, param2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public virtual ObjectResult<string> GenerarNumeroOrgen(CreditoEcopetrolCreacionOrden OrgenGiroModel)
        {
            try
            {


                SqlParameter PI_Fk_Usuario = new SqlParameter("@PI_Fk_Usuario", OrgenGiroModel.PI_Fk_Usuario);
                SqlParameter PI_Fecha_Inicial = new SqlParameter("@PI_Fecha_Inicial", OrgenGiroModel.PI_Fecha_Inicial);
                SqlParameter PI_Fecha_Final = new SqlParameter("@PI_Fecha_Final", OrgenGiroModel.PI_Fecha_Final);
                SqlParameter PI_Total_Orden = new SqlParameter("@PI_Total_Orden", OrgenGiroModel.PI_Total_Orden);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<string>("" + DB + ".dbo.PA_GESTION_ORGEN_GIRO @PI_Fk_Usuario,@PI_Fecha_Inicial,@PI_Fecha_Final,@PI_Total_Orden", PI_Fk_Usuario, PI_Fecha_Inicial, PI_Fecha_Final, PI_Total_Orden);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public virtual ObjectResult<CreditoEcopetrolReferenciaPago> ConsultaReferenciaPagos()
        {
            try
            {
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CreditoEcopetrolReferenciaPago>("" + DB + ".dbo.PA_GENERA_ARCHIVO_REFERENCIAS_PAGO");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

