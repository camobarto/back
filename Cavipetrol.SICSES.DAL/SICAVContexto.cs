﻿using Cavipetrol.SICSES.DAL.Mapeo.Sicav;
using Cavipetrol.SICSES.DAL.Mapeo.Garantias;
using Cavipetrol.SICSES.DAL.Mapeo;
using Cavipetrol.SICSES.Infraestructura.Historico;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.Seguridad;
using Cavipetrol.SICSES.Infraestructura.Model.PreaprobacionVehiculo;
using Cavipetrol.SICSES.Infraestructura.Model.Garantias;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP;
using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.fyc.Riesgos;
using System.Configuration;
using System.Collections.Generic;
using System.Security;
using System.Security.Permissions;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;

namespace Cavipetrol.SICSES.DAL
{
	public partial class SICAVContexto : DbContext
	{
        string DB = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;
        public SICAVContexto() : base("name=SICAV")
        {

        }
        public DbSet<CapacidadPago> CapacidadPago { get; set; }
        public DbSet<CapacidadPagoEstados> CapacidadPagoEstados { get; set; }
        public DbSet<SolicitudCredito> SolicitudCredito { get; set; }
        public DbSet<CapacidadPagoHorasExtras> CapacidadPagoHorasExtras { get; set; }
        public DbSet<Historico> Historico { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<PerfilMenuRelacion> PerfilMenuRelacion { get; set; }
        public DbSet<AsegurabilidadOpcion> AsegurabilidadOpcion { get; set; }
        public DbSet<SolicitudCreditoCausalesNegacion> SolicitudCreditoCausalesNegacion { get; set; }
        public DbSet<SolicitudCreditoNegado> SolicitudCreditoNegado { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Perfil> Perfil { get; set; }
        public DbSet<Moras> Moras { get; set; }
        public DbSet<ReferenciasAsociado> ReferenciasAsociado { get; set; }
        public DbSet<ConsolidadoAprobacion> ConsolidadoAprobacion { get; set; }
        public DbSet<SolicitudCreditoFormaPago> SolicitudCreditoFormaPago { get; set; }
        public DbSet<SolicitudCreditoIntentosLog> SolicitudCreditoIntentosLog { get; set; }
        public DbSet<CapacidadPagoCodeudores> CapacidadPagoCodeudores { get; set; }
        public DbSet<SolicitudCreditoAdjunto> SolicitudCreditoAdjuntos { get; set; }
        public DbSet<CapacidadPagoTerceroCodeudor> CapacidadPagoTerceroCodeudor { get; set; }
        public DbSet<SolicitudCreditoHipoteca> SolicitudCreditoHipoteca { get; set; }
        public DbSet<DetalleAsociado> DetalleAsociado { get; set; }
        public DbSet<SolicitudCreditoHistorico> SolicitudCreditoHistorico { get; set; }
		public DbSet<PerfilUsuario> PerfilUsuario { get; set; }
        public DbSet<PrendaVehicular> PrendaVehicular { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new CapacidadPagoMapa());
			modelBuilder.Configurations.Add(new CapacidadPagoEstadosMapa());
			modelBuilder.Configurations.Add(new SolicitudCreditoMapa());
			modelBuilder.Configurations.Add(new CapacidadPagoHorasExtrasMapa());
			modelBuilder.Configurations.Add(new HistoricoMapa());
			modelBuilder.Configurations.Add(new MenuMapa());
			modelBuilder.Configurations.Add(new PerfilMenuRelacionMapa());
			modelBuilder.Configurations.Add(new AsegurabilidadOpcionMapa());
			modelBuilder.Configurations.Add(new SolicitudCreditoCausalesNegacionMapa());
			modelBuilder.Configurations.Add(new SolicitudCreditoNegadoMapa());
			modelBuilder.Configurations.Add(new ReferenciasAsociadoMapa());
			modelBuilder.Configurations.Add(new UsuarioMapa());
			modelBuilder.Configurations.Add(new PerfilMapa());
			modelBuilder.Configurations.Add(new MorasMapa());
			modelBuilder.Configurations.Add(new ConsolidadoAprobacionMapa());
			modelBuilder.Configurations.Add(new SolicitudCreditoFormaPagoMapa());
			modelBuilder.Configurations.Add(new SolicitudCreditoIntentosLogMapa());
			modelBuilder.Configurations.Add(new CapacidadPagoCodeudoresMapa());
			modelBuilder.Configurations.Add(new GarantiaVariableRelacionMapa());
			modelBuilder.Configurations.Add(new CapacidadPagoTerceroCodeudorMapa());
			modelBuilder.Configurations.Add(new SolicitudCreditoAdjuntoMapa());
			modelBuilder.Configurations.Add(new SolicitudCreditoHipotecaMapa());
			modelBuilder.Configurations.Add(new DetalleAsociadoMapa());
			modelBuilder.Configurations.Add(new SolicitudCreditoHistoricoMapa());
            modelBuilder.Configurations.Add(new PrendaVehicularMapa());
        }
        public virtual ObjectResult<Infraestructura.Model.SolicitudCreditos.InformacionSolicitanteCampanaEspecial> ObtenerDatosCampaniaVehiculo(string numeroIdentificacion, string tipoIdentificacion, int TipoVehiculo)
        {
            SqlParameter param1 = new SqlParameter("@DOCUMENTO_IDENTIFICACION", numeroIdentificacion);
            SqlParameter param2 = new SqlParameter("@TIPO_IDENTIFICACION", tipoIdentificacion);
            SqlParameter param3 = new SqlParameter("@TIPO_VEHICULO", TipoVehiculo);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Infraestructura.Model.SolicitudCreditos.InformacionSolicitanteCampanaEspecial>("dbo.PA_CONSULTAR_INFORMACION_USUARIO_SOLICITANTE_CAMPANIA_VEHICULO @DOCUMENTO_IDENTIFICACION, @TIPO_IDENTIFICACION, @TIPO_VEHICULO", @param1, @param2, @param3);
        }
        public virtual ObjectResult<PreaprobadoCarta> GuardarDatosPreaprobacion(PreaprobadoCarta datos)
        {
            SqlParameter param1 = new SqlParameter("@NumeroConsecutivo", datos.NumeroConsecutivo);
            SqlParameter param2 = new SqlParameter("@Cupo", datos.Cupo);
            SqlParameter param3 = datos.DeudorNombre == null ? new SqlParameter("@DeudorNombre", 'X') : new SqlParameter("@DeudorNombre", datos.DeudorNombre);
            SqlParameter param4 = new SqlParameter("@Tasa", datos.Tasa);
            SqlParameter param5 = datos.DeudorCedula == null ? new SqlParameter("@DeudorCedula", 'X') : new SqlParameter("@DeudorCedula", datos.DeudorCedula);
            SqlParameter param6 = datos.Ciudad == null ? new SqlParameter("@Ciudad", 'X') : new SqlParameter("@Ciudad", datos.Ciudad);
            SqlParameter param7 = datos.Periodicidad == null ? new SqlParameter("@Periodicidad", 'X') : new SqlParameter("@Periodicidad", datos.Periodicidad);
            SqlParameter param8 = new SqlParameter("@FechaGeneracion", datos.FechaGeneracion);
            SqlParameter param9 = new SqlParameter("@Tarea", datos.Tarea);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<PreaprobadoCarta>("" + DB + ".dbo.PA_GUARDAR_DATOS_PREAPROBACION @NumeroConsecutivo, @Cupo, @DeudorNombre, @Tasa, @DeudorCedula, @Ciudad, @Periodicidad, @FechaGeneracion, @Tarea", @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9);            
        }
        public virtual ObjectResult<Solicitud> AdministrarPagares(PagaresModel datos)
        {
            SqlParameter param1 = new SqlParameter("@IdNit", datos.IdNit);
            SqlParameter param2 = new SqlParameter("@NumNit", datos.NumNit);
            SqlParameter param3 = new SqlParameter("@Proceso", datos.Proceso);
            SqlParameter param4 = datos.TipoPagare == null ? new SqlParameter("@TipoPagare", ' ') : new SqlParameter("@TipoPagare", datos.TipoPagare);
            SqlParameter param5 = new SqlParameter("@Ciudad", datos.Ciudad);
            SqlParameter param6 = datos.Producto == null ? new SqlParameter("@Producto", ' ') : new SqlParameter("@Producto", datos.Producto);
            SqlParameter param7 = new SqlParameter("@Usuario", datos.Usuario);
            SqlParameter param8 = new SqlParameter("@NumeroSolicitud", datos.NumeroSolicitud);
            SqlParameter param9 = datos.NumeroMatricula == null ? new SqlParameter("@NumeroMatricula", ' ') : new SqlParameter("@NumeroMatricula", datos.NumeroMatricula);
            SqlParameter param10 = datos.OficinaMatricula == null ? new SqlParameter("@OficinaMatricula", ' ') : new SqlParameter("@OficinaMatricula", datos.OficinaMatricula);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Solicitud>("dbo.PA_ADMINISTRAR_PAGARES @idNit, @numNit, @Proceso, @TipoPagare, @Ciudad, @Producto, @Usuario, @NumeroSolicitud, @NumeroMatricula, @OficinaMatricula", @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10);
        }
        public virtual ObjectResult<CreditoYPagares> GestionNormalizado(NormalizadoConsulta datos)
        {
            SqlParameter param1 = new SqlParameter("@IdNit", datos.IdNit);
            SqlParameter param2 = new SqlParameter("@NumNit", datos.NumNit);
            SqlParameter param3 = new SqlParameter("@Proceso", datos.Proceso);
            SqlParameter param4 = datos.TipoPagare == null ? new SqlParameter("@TipoPagare", ' ') : new SqlParameter("@TipoPagare", datos.TipoPagare);
            SqlParameter param5 = new SqlParameter("@Ciudad", datos.Ciudad);
            SqlParameter param6 = datos.Producto == null ? new SqlParameter("@Producto", ' ') : new SqlParameter("@Producto", datos.Producto);
            SqlParameter param7 = datos.DocumentoTipo == null ? new SqlParameter("@DocumentoTipo", ' ') : new SqlParameter("@DocumentoTipo", datos.DocumentoTipo);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CreditoYPagares>("dbo.PA_GESTION_NORMALIZADO @idNit, @numNit, @Proceso, @TipoPagare, @Ciudad, @Producto,@DocumentoTipo", @param1, @param2, @param3, @param4, @param5, @param6, @param7);
        }
        public virtual ObjectResult<ViewModelSolicitudCredito> ObtenerSolicitudCredito(int? idSolicitud = null,
																					   int? idTipoCredito = null,
																					   string idTipoIdentificacion = null,
																					   string numeroIdentificacionAsociado = null,
																					   short? idAseguabilidadOpcion = null,
																					   string idEstadoSolicitudCredito = null,
																					   string usuarioCrea = null,
																					   string usuarioModifica = null,
																					   DateTime? fechaInicial = null,
																					   DateTime? fechaFinal = null,
																					   long? consecutivoSolicitud = null,
																					   int? idPerfil = null)
		{
			SqlParameter param1 = idSolicitud == null ?
				new SqlParameter("@idSolicitud", DBNull.Value) :
				new SqlParameter("@idSolicitud", idSolicitud);

			SqlParameter param2 = idTipoCredito == null ?
				new SqlParameter("@idTipoCredito", DBNull.Value) :
				new SqlParameter("@idTipoCredito", idTipoCredito);

			SqlParameter param3 = idTipoIdentificacion == null ?
				new SqlParameter("@idTipoIdentificacion", DBNull.Value) :
				new SqlParameter("@idTipoIdentificacion", idTipoIdentificacion);

			SqlParameter param4 = numeroIdentificacionAsociado == null ?
				new SqlParameter("@numeroIdentificacionAsociado", DBNull.Value) :
				new SqlParameter("@numeroIdentificacionAsociado", numeroIdentificacionAsociado);

			SqlParameter param5 = idAseguabilidadOpcion == null ?
				new SqlParameter("@idAseguabilidadOpcion", DBNull.Value) :
				new SqlParameter("@idAseguabilidadOpcion", idAseguabilidadOpcion);

			SqlParameter param6 = idEstadoSolicitudCredito == null ?
				new SqlParameter("@idEstadoSolicitudCredito", DBNull.Value) :
				new SqlParameter("@idEstadoSolicitudCredito", idEstadoSolicitudCredito);

			SqlParameter param7 = usuarioCrea == null ?
				new SqlParameter("@usuarioCrea", DBNull.Value) :
				new SqlParameter("@usuarioCrea", usuarioCrea);

			SqlParameter param8 = usuarioModifica == null ?
				new SqlParameter("@usuarioModifica", DBNull.Value) :
				new SqlParameter("@usuarioModifica", usuarioModifica);

			SqlParameter param9 = fechaInicial == null ?
				new SqlParameter("@fechaInicial", DBNull.Value) :
				new SqlParameter("@fechaInicial", fechaInicial);

			SqlParameter param10 = fechaFinal == null ?
				new SqlParameter("@fechaFinal", DBNull.Value) :
				new SqlParameter("@fechaFinal", SolicitudCredito);

			SqlParameter param11 = consecutivoSolicitud == null ?
			   new SqlParameter("@consecutivoSolicitud", DBNull.Value) :
			   new SqlParameter("@consecutivoSolicitud", consecutivoSolicitud);

			SqlParameter param12 = idPerfil == null ?
			   new SqlParameter("@IdPerfil", DBNull.Value) :
			   new SqlParameter("@IdPerfil", idPerfil);



			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ViewModelSolicitudCredito>("dbo.PA_CONSULTAR_SOLICITUD_CREDITO @idSolicitud, @idTipoCredito, @idTipoIdentificacion, @numeroIdentificacionAsociado, @idAseguabilidadOpcion, @idEstadoSolicitudCredito, @usuarioCrea, @usuarioModifica, @fechaInicial, @fechaFinal, @consecutivoSolicitud, @IdPerfil",
																											 @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10, @param11, @param12);
		}
		public virtual ObjectResult<ViewModelConsolidadoAtribuciones> ObtenerConsolidadoAtribuciones(string idTipoIdentificacion, string numeroIdentificacionAsociado,
																int? idPerfil, int? valorcredito)
		{
			SqlParameter param1 = idTipoIdentificacion == null ?
				new SqlParameter("@TIPO_IDENTIFICACION", DBNull.Value) :
				new SqlParameter("@TIPO_IDENTIFICACION", idTipoIdentificacion);

			SqlParameter param2 = numeroIdentificacionAsociado == null ?
				new SqlParameter("@NUM_IDENTIFICACION", DBNull.Value) :
				new SqlParameter("@NUM_IDENTIFICACION", numeroIdentificacionAsociado);

			SqlParameter param3 = idPerfil == null ?
				new SqlParameter("@PERFIL", DBNull.Value) :
				new SqlParameter("@PERFIL", idPerfil);

			SqlParameter param4 = valorcredito == null ?
				new SqlParameter("@NUEVO_VALOR_CRED", DBNull.Value) :
				new SqlParameter("@NUEVO_VALOR_CRED", valorcredito);

			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ViewModelConsolidadoAtribuciones>("dbo.PA_CONSULTA_CONSOLIDADO_ATRIBUCIONES @TIPO_IDENTIFICACION,@NUM_IDENTIFICACION,@PERFIL,@NUEVO_VALOR_CRED",
																													@param1, @param2, @param3, @param4);
		}

		public virtual void GuardarUsuarioTemporal(string Usuario, string Contrasena)
		{
			SqlParameter param1 = Usuario == null ? new SqlParameter("@Usuario", DBNull.Value) : new SqlParameter("@Usuario", Usuario);
			SqlParameter param2 = Contrasena == null ? new SqlParameter("@Contrasena", DBNull.Value) : new SqlParameter("@Contrasena", Contrasena);

			((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("dbo.PA_InsertarClaveTemporal @Usuario,@Contrasena", @param1, @param2);
		}

		public virtual ObjectResult<RespuestaValidacion> ValidarInicioUsuario(string usuario)
		{
			SqlParameter param1 = usuario == null ? new SqlParameter("@Usuario", DBNull.Value) : new SqlParameter("@Usuario", usuario);
			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<RespuestaValidacion>("dbo.PA_ValidarUsuario @Usuario", @param1);
		}

		public virtual ObjectResult<UsuarioLogin> AdministrarInicioSesion(string usuario, string contrasena, string NuevaContrasena, int bandera)
		{
			SqlParameter param1 = Usuario == null ? new SqlParameter("@Usuario", DBNull.Value) : new SqlParameter("@Usuario", usuario);
			SqlParameter param2 = contrasena == null ? new SqlParameter("@Contrasena", DBNull.Value) : new SqlParameter("@Contrasena", contrasena);
			SqlParameter param3 = NuevaContrasena == null ? new SqlParameter("@NuevaContrasena", DBNull.Value) : new SqlParameter("@NuevaContrasena", NuevaContrasena);
			SqlParameter param4 = bandera == 0 ? new SqlParameter("@bandera", DBNull.Value) : new SqlParameter("@bandera", bandera);

			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<UsuarioLogin>("dbo.AdministrarInicioSesion @Usuario,@Contrasena,@NuevaContrasena,@bandera", @param1, @param2, @param3, @param4);
		}
		public virtual ObjectResult<CapacidadPagoTerceroCodeudor> GuardarCapacidadPagoTerceroCodeudor(string TipoIdentificacion, string NumeroIdentificacion, string Nombre, string FechaNacimiento, string FormCapacidad)
		{
			SqlParameter param1 = TipoIdentificacion == null ? new SqlParameter("@Usuario", DBNull.Value) : new SqlParameter("@Usuario", TipoIdentificacion);
			SqlParameter param2 = NumeroIdentificacion == null ? new SqlParameter("@Contrasena", DBNull.Value) : new SqlParameter("@Contrasena", NumeroIdentificacion);
			SqlParameter param3 = Nombre == null ? new SqlParameter("@NuevaContrasena", DBNull.Value) : new SqlParameter("@NuevaContrasena", Nombre);
			SqlParameter param4 = FechaNacimiento == null ? new SqlParameter("@bandera", DBNull.Value) : new SqlParameter("@bandera", FechaNacimiento);

			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CapacidadPagoTerceroCodeudor>("dbo.AdministrarInicioSesion @Usuario,@Contrasena,@NuevaContrasena,@bandera", @param1, @param2, @param3, @param4);
		}

		public virtual ObjectResult<double> ObtenerCupoMaximoCredito(string TipoIdentificacion, string NumeroIdentificacion)
		{
			SqlParameter param1 = TipoIdentificacion == null ? new SqlParameter("@TipoIdentificacion", DBNull.Value) : new SqlParameter("@TipoIdentificacion", TipoIdentificacion);
			SqlParameter param2 = NumeroIdentificacion == null ? new SqlParameter("@NumeroIdentificacion", DBNull.Value) : new SqlParameter("@NumeroIdentificacion", NumeroIdentificacion);

			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<double>("dbo.PA_CONSULTAR_SALDO_PIGNORACION_APORTES @TipoIdentificacion,@NumeroIdentificacion", @param1, @param2);
		}

		public virtual ObjectResult<RespuestaValidacion> ObtenerSolicitudCreditoAdjunto(int idSolicitud, int idTipoAdjunto)
		{
			SqlParameter param1 = new SqlParameter("@idSolicitud", idSolicitud);
			SqlParameter param2 = new SqlParameter("@idTipoAdjunto", idTipoAdjunto);

			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<RespuestaValidacion>("dbo.PA_CONSULTA_SOLICITUD_CREDITO_ADJUNTO @idSolicitud, @idTipoAdjunto", @param1, @param2);
		}

		public virtual ObjectResult<HipotecaSolicitud> ObtenerHipotecaSolicitudCredito(int idSolicitud)
		{
			SqlParameter param1 = new SqlParameter("@idSolicitudCredito", idSolicitud);

			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<HipotecaSolicitud>("dbo.PA_OBTENER_HIPOTECA_CREDITO @idSolicitudCredito", param1);
		}

		public virtual ObjectResult<RespuestaValidacion> GenerarHipotecaGuardarMinuta(SolicitudMinutaFYC solicitudMinutaFYC)
		{
			SqlParameter param1 = solicitudMinutaFYC.Ciudad == null ? new SqlParameter("@DCTCiudad", DBNull.Value) : new SqlParameter("@DCTCiudad", solicitudMinutaFYC.Ciudad);
			SqlParameter param2 = solicitudMinutaFYC.CiudadInmueble == null ? new SqlParameter("@DCTCiudadInmueble", DBNull.Value) : new SqlParameter("@DCTCiudadInmueble", solicitudMinutaFYC.CiudadInmueble);
			SqlParameter param3 = solicitudMinutaFYC.CiudadNotaria == null ? new SqlParameter("@DCTCiudadNotaria", DBNull.Value) : new SqlParameter("@DCTCiudadNotaria", solicitudMinutaFYC.CiudadNotaria);
			SqlParameter param4 = solicitudMinutaFYC.Compra == null ? new SqlParameter("@DCTCompra", DBNull.Value) : new SqlParameter("@DCTCompra", solicitudMinutaFYC.Compra);
			SqlParameter param5 = solicitudMinutaFYC.DeudorTipoCedula == null ? new SqlParameter("@DCTDeudorTipoCedula", DBNull.Value) : new SqlParameter("@DCTDeudorTipoCedula", solicitudMinutaFYC.DeudorTipoCedula);
			SqlParameter param6 = solicitudMinutaFYC.DeudorCedula == null ? new SqlParameter("@DCTDeudorCedula", DBNull.Value) : new SqlParameter("@DCTDeudorCedula", solicitudMinutaFYC.DeudorCedula);
			SqlParameter param7 = solicitudMinutaFYC.DeudorNombre == null ? new SqlParameter("@DCTDeudorNombre", DBNull.Value) : new SqlParameter("@DCTDeudorNombre", solicitudMinutaFYC.DeudorNombre);
			SqlParameter param8 = solicitudMinutaFYC.DeudorRegistro == null ? new SqlParameter("@DCTDeudorRegistro", DBNull.Value) : new SqlParameter("@DCTDeudorRegistro", solicitudMinutaFYC.DeudorRegistro);
			SqlParameter param9 = solicitudMinutaFYC.DeudorDireccion == null ? new SqlParameter("@DCTDeudorDireccion", DBNull.Value) : new SqlParameter("@DCTDeudorDireccion", solicitudMinutaFYC.DeudorDireccion);
			SqlParameter param10 = solicitudMinutaFYC.DireccionInmueble == null ? new SqlParameter("@DCTDireccionInmueble", DBNull.Value) : new SqlParameter("@DCTDireccionInmueble", solicitudMinutaFYC.DireccionInmueble);
			SqlParameter param11 = solicitudMinutaFYC.Matricula == null ? new SqlParameter("@DCTMatricula", DBNull.Value) : new SqlParameter("@DCTMatricula", solicitudMinutaFYC.Matricula);
			SqlParameter param12 = solicitudMinutaFYC.MatriculaDescripcion == null ? new SqlParameter("@DCTMatriculaDescipcion", DBNull.Value) : new SqlParameter("@DCTMatriculaDescipcion", solicitudMinutaFYC.MatriculaDescripcion);
			SqlParameter param13 = solicitudMinutaFYC.Notaria == null ? new SqlParameter("@DCTNotaria", DBNull.Value) : new SqlParameter("@DCTNotaria", solicitudMinutaFYC.Notaria);
			SqlParameter param14 = solicitudMinutaFYC.NumEscritura == null ? new SqlParameter("@DCTNumEscritura", DBNull.Value) : new SqlParameter("@DCTNumEscritura", solicitudMinutaFYC.NumEscritura);
			SqlParameter param15 = solicitudMinutaFYC.Oficina == null ? new SqlParameter("@DCTOficina", DBNull.Value) : new SqlParameter("@DCTOficina", solicitudMinutaFYC.Oficina);
			SqlParameter param16 = solicitudMinutaFYC.Plazo == null ? new SqlParameter("@DCTPlazo", DBNull.Value) : new SqlParameter("@DCTPlazo", solicitudMinutaFYC.Plazo);
			SqlParameter param17 = solicitudMinutaFYC.Producto == null ? new SqlParameter("@DCTProducto", DBNull.Value) : new SqlParameter("@DCTProducto", solicitudMinutaFYC.Producto);
			SqlParameter param18 = solicitudMinutaFYC.TiempoECP == null ? new SqlParameter("@DCTTiempoECP", DBNull.Value) : new SqlParameter("@DCTTiempoECP", solicitudMinutaFYC.TiempoECP);
			SqlParameter param19 = solicitudMinutaFYC.TipoHipoteca == null ? new SqlParameter("@DCTTipoHipoteca", DBNull.Value) : new SqlParameter("@DCTTipoHipoteca", solicitudMinutaFYC.TipoHipoteca);
			SqlParameter param20 = solicitudMinutaFYC.Usuario == null ? new SqlParameter("@DCTUsuario", DBNull.Value) : new SqlParameter("@DCTUsuario", solicitudMinutaFYC.Usuario);
			SqlParameter param21 = !solicitudMinutaFYC.Tasa.HasValue ? new SqlParameter("@DCTTasa", DBNull.Value) : new SqlParameter("@DCTTasa", solicitudMinutaFYC.Tasa);
			SqlParameter param22 = !solicitudMinutaFYC.ValorPrestamo.HasValue ? new SqlParameter("@DCTValorPrestamo", DBNull.Value) : new SqlParameter("@DCTValorPrestamo", solicitudMinutaFYC.ValorPrestamo);
			SqlParameter param23 = solicitudMinutaFYC.Fecha == null ? new SqlParameter("@DCTFecha", DBNull.Value) : new SqlParameter("@DCTFecha", solicitudMinutaFYC.Fecha);
			SqlParameter param24 = solicitudMinutaFYC.FechaFirma == null ? new SqlParameter("@DCTFechaFirma", DBNull.Value) : new SqlParameter("@DCTFechaFirma", solicitudMinutaFYC.FechaFirma);
			SqlParameter param25 = !solicitudMinutaFYC.IdSolicitud.HasValue ? new SqlParameter("@DCTIdSolicitud", DBNull.Value) : new SqlParameter("@DCTIdSolicitud", solicitudMinutaFYC.IdSolicitud);
			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<RespuestaValidacion>("dbo.PA_GENERAR_HIPOTECA @DCTCiudad, @DCTCiudadInmueble, @DCTCiudadNotaria, @DCTCompra, @DCTDeudorTipoCedula, @DCTDeudorCedula, @DCTDeudorNombre, @DCTDeudorRegistro, @DCTDeudorDireccion, @DCTDireccionInmueble, @DCTMatricula, @DCTMatriculaDescipcion, @DCTNotaria, @DCTNumEscritura, @DCTOficina, @DCTPlazo, @DCTProducto, @DCTTiempoECP, @DCTTipoHipoteca, @DCTUsuario, @DCTTasa, @DCTValorPrestamo, @DCTFecha, @DCTFechaFirma, @DCTIdSolicitud", @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10, @param11, @param12, @param13, @param14, @param15, @param16, @param17, @param18, @param19, @param20, @param21, @param22, @param23, @param24, @param25);
		}
		public virtual ObjectResult<Factura> ObtenerNumeroFactura(string servidor)
		{
			switch (servidor) {
				case "FYCCAR":
					servidor = "FYCCAR";
					break;
				default:
					servidor = "FYCBOG";
					break;
			}
			SqlParameter param1 = new SqlParameter("@servidor", servidor);

			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Factura>("dbo.PA_GENERAR_NUMERO_FACTURA @servidor", param1);
		}

		public virtual ObjectResult<string> ObtenerCodeudores(int Identificacion)
		{
			SqlParameter param1 = new SqlParameter("@Identificacion", Identificacion);

			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<string>("dbo.[PA_CONSULTAR_CODEUDORES] @Identificacion", param1);
		}

		public virtual ObjectResult<SolicitudOperacion> SolicitudOperacion(string TipoIdentificacion, string NumeroIdentificacion, string NumeroOperacion)
		{
			SqlParameter param1 = new SqlParameter("@IdTipoNit", TipoIdentificacion);
			SqlParameter param2 = new SqlParameter("@Nit", NumeroIdentificacion);
			SqlParameter param3 = new SqlParameter("@Operacion", NumeroOperacion);

			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<SolicitudOperacion>("[dbo].[PA_CONSULTA_ULTIMA_OPERACION] @IdTipoNit, @Nit,@Operacion", @param1, @param2, @param3);
		}

		public virtual void GuardarDatosPersonaTransaccionEfectivo(int Operacion, string Tipoidentificacion, string Numeroidentificacion, string Primernombre, string Segundonombre,
									string Primerapellido, string Segundoapellido, string Direccion, string Telefono, string Consulta, string Tipopersona)
		{
			SqlParameter param1 = new SqlParameter("@Operacion", Operacion);
			SqlParameter param2 = new SqlParameter("@Tipoidentificacion", Tipoidentificacion);
			SqlParameter param3 = new SqlParameter("@Numeroidentificacion", Numeroidentificacion);
			SqlParameter param4 = new SqlParameter("@Primernombre", Primernombre);
			SqlParameter param5 = new SqlParameter("@Segundonombre", Segundonombre);
			SqlParameter param6 = new SqlParameter("@Primerapellido", Primerapellido);
			SqlParameter param7 = new SqlParameter("@Segundoapellido", Segundoapellido);
			SqlParameter param8 = new SqlParameter("@Direccion", Direccion);
			SqlParameter param9 = new SqlParameter("@Telefono", Telefono);
			SqlParameter param10 = new SqlParameter("@Consulta", Consulta);
			SqlParameter param11 = new SqlParameter("@Tipopersona", Tipopersona);


			((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("[dbo].[PA_INSERTAR_DATOS_PERSONA] @Operacion,@Tipoidentificacion,@Numeroidentificacion,@Primernombre,@Segundonombre, @Primerapellido,@Segundoapellido,@Direccion,@Telefono,@Consulta,@Tipopersona",
																			@param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10, @param11);
		}

		public virtual void GuardarDatosOrigenFondos(int Operacion, int Opcion, string Consulta, string Detalle)
		{
			SqlParameter param1 = new SqlParameter("@Operacion", Operacion);
			SqlParameter param2 = new SqlParameter("@IdOpcion", Opcion);
			SqlParameter param3 = new SqlParameter("@Consulta", Consulta);
			SqlParameter param4 = new SqlParameter("@Detalle", Detalle);

			((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("[dbo].[PA_INSERTAR_DATOS_ORIGEN_FONDOS] @Operacion,@IdOpcion,@Consulta,@Detalle", @param1, @param2, @param3, @param4);
		}

		public virtual ObjectResult<CartaCondicionesCreditoModel> ObtenerDatosCartaCondiciones(int idSolicitud)
		{
			SqlParameter param1 = new SqlParameter("@idSolicitud", idSolicitud);
			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CartaCondicionesCreditoModel>("PA_CONSULTAR_INFO_CARTA_CONDICIONES_CREDITO @idSolicitud", @param1);
		}

		public virtual void GuardarDatosCodeudorFYC(int IdSolicitud, string TipoIdentificacion, string IdIdentificacion, string Nombre)
		{
			SqlParameter param1 = new SqlParameter("@IdSolicitud", IdSolicitud);
			SqlParameter param2 = new SqlParameter("@TipoIdentificacion", TipoIdentificacion);
			SqlParameter param3 = new SqlParameter("@IdIdentificacion", IdIdentificacion);
			SqlParameter param4 = new SqlParameter("@Nombre", Nombre);

			((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("[" + DB + "].[dbo].[PA_GUARDAR_CODEUDOR_FYC] @IdSolicitud,@TipoIdentificacion,@IdIdentificacion,@Nombre", @param1, @param2, @param3, @param4);
		}

		public ObjectResult<DateTime> ConsultarFechaFinalizacionContratoPorEmpleado(string TipoIdentificacion, string NumeroIdentificacion)
		{
			SqlParameter param1 = new SqlParameter("@tipo_id", TipoIdentificacion);
			SqlParameter param2 = new SqlParameter("@num_id", NumeroIdentificacion);
			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<DateTime>("[dbo].pa_consultar_fecha_finalizacion_contrato_ecp_temporal @tipo_id, @num_id", @param1, @param2);
		}
		public virtual ObjectResult<PorcentajesTasas> ObtenerTasas(string nomPorcentaje)
		{
			SqlParameter param1 = new SqlParameter("@nombreTasa", nomPorcentaje);
			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<PorcentajesTasas>("[dbo].PA_OBTENER_TASAS @nombreTasa", @param1);
		}
		public virtual void CrearNuevaTasa(string nomPorcentaje, string porcentaje)
		{
			SqlParameter param1 = new SqlParameter("@PROCESO", 1);
			SqlParameter param2 = new SqlParameter("@NOMBRE_TASA", nomPorcentaje);
			SqlParameter param3 = new SqlParameter("@PORCENTAJE_TASA", porcentaje);
			
			((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("[dbo].PA_EJECUCION_PROCESOS_FODES PROCESO, @NOMBRE_TASA,  @PORCENTAJE_TASA", @param1, @param2, @param3);
		}
		public virtual ObjectResult<FomentoEmpresarial> ObtenerFomentoEmresarial()
		{
			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<FomentoEmpresarial>("[dbo].PA_OBTENER_CREDITOS_FOMENTO_EMPRESARIAL");
		}
		public virtual void MarcarCreditoFodes(string documentoTipo, string documentoNumero, int numeroUnico, string tipoDocumento, string numeroDocumento, string descripcion)
		{
			SqlParameter param1 = new SqlParameter("@PROCESO", 2);
			SqlParameter param2 = new SqlParameter("@ACCION", "CREAR");
			SqlParameter param3 = new SqlParameter("@NOMBRE_TASA", "NO");
			SqlParameter param4 = new SqlParameter("@PORCENTAJE_TASA", "NO");
			SqlParameter param5 = new SqlParameter("@ADN_DOCUMENTOTIPO", documentoTipo);
			SqlParameter param6 = new SqlParameter("@ADN_DOCUMENTONÚMERO", documentoNumero);
			SqlParameter param7 = new SqlParameter("@NUMERO_UNICO", numeroUnico);
			SqlParameter param8 = new SqlParameter("@TIPO_DOCUMENTO", tipoDocumento);
			SqlParameter param9 = new SqlParameter("@NUM_DOCUMENTO", numeroDocumento);
			SqlParameter param10 = new SqlParameter("@DESCRIPCION", descripcion);
			((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("dbo.PA_EJECUCION_PROCESOS_FODES @PROCESO, @ACCION,  @NOMBRE_TASA, @PORCENTAJE_TASA, @ADN_DOCUMENTOTIPO,  @ADN_DOCUMENTONÚMERO, @NUMERO_UNICO, @TIPO_DOCUMENTO, @NUM_DOCUMENTO, @DESCRIPCION", @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, param10);
		}
		public virtual ObjectResult<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMarcados()
		{
			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<FomentoEmpresarial>("[dbo].[PA_OBTENER_CREDITOS_FOMENTO_EMPRESARIAL_MARCADOS]");
		}
		public virtual void DesmarcarCreditoFodes(string documentoTipo, string documentoNumero, int numeroUnico, string tipoDocumento, string numeroDocumento, string descripcion)
		{
			SqlParameter param1 = new SqlParameter("@PROCESO", 3);
			SqlParameter param2 = new SqlParameter("@ACCION", "ELIMINAR");
			SqlParameter param3 = new SqlParameter("@NOMBRE_TASA", "NO");
			SqlParameter param4 = new SqlParameter("@PORCENTAJE_TASA", "NO");
			SqlParameter param5 = new SqlParameter("@ADN_DOCUMENTOTIPO", documentoTipo);
			SqlParameter param6 = new SqlParameter("@ADN_DOCUMENTONÚMERO", documentoNumero);
			SqlParameter param7 = new SqlParameter("@NUMERO_UNICO", numeroUnico);
			SqlParameter param8 = new SqlParameter("@TIPO_DOCUMENTO", tipoDocumento);
			SqlParameter param9 = new SqlParameter("@NUM_DOCUMENTO", numeroDocumento);
			SqlParameter param10 = new SqlParameter("@DESCRIPCION", descripcion);
			((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("dbo.PA_EJECUCION_PROCESOS_FODES @PROCESO, @ACCION, @NOMBRE_TASA, @PORCENTAJE_TASA, @ADN_DOCUMENTOTIPO,  @ADN_DOCUMENTONÚMERO, @NUMERO_UNICO, @TIPO_DOCUMENTO, @NUM_DOCUMENTO, @DESCRIPCION", @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10);
		}
		public virtual ObjectResult<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMora()
		{
			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<FomentoEmpresarial>("[dbo].[PA_OBTENER_CREDITOS_FOMENTO_MOROSIDAD]");
		}
		public virtual ObjectResult<InfoFodes> InformacionFodes()
		{
			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<InfoFodes>("[dbo].[PA_OBTENER_INFORMACION_FODES]");
		}

        // SELECCIONA DATOS DE LAS SOLICITUDES PARA LLENAR EL COMBO -- OPERACION 1 </jarp>
        public virtual ObjectResult<AvaluosCrViviendaEcopetrolObtener> ConsultaSolicitudesAvaluos(AvaluosCrViviendaEcopetrolObtener Obj)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 1);
                SqlParameter param2 = new SqlParameter("@Identificacion", Obj.Identificacion);
                SqlParameter param3 = new SqlParameter("@IdSolicitud", Obj.IdSolicitud);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<AvaluosCrViviendaEcopetrolObtener>("" + DB + ".dbo.PA_GESTION_AVALUOS @Operacion, @Identificacion, @IdSolicitud", @param1, param2, param3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // SELECCIONA DATOS DE LAS SOLICITUDES PARA LLENAR EL FORMULARIO, INFO POR SOLICITUD -- OPERACION 2 </jarp>
        public virtual ObjectResult<AvaluosCrViviendaEcopetrol> ConsultaInformacionSolicitudAvaluos(AvaluosCrViviendaEcopetrol Obj)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@Operacion", 2);
                SqlParameter param2 = new SqlParameter("@Identificacion", "".ToString());
                SqlParameter param3 = new SqlParameter("@IdSolicitud", Obj.AVA_NumeroServicio);

                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<AvaluosCrViviendaEcopetrol>("" + DB + ".dbo.PA_GESTION_AVALUOS @Operacion, @Identificacion, @IdSolicitud", @param1, param2, param3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GENERA LA FINALIZACION DEL PROCESO CORRECTAMENTE</jarp>
        public virtual void FinalizarProceso(string PI_Id_Formulario, string PI_Numero_de_Identificacion, string PI_Comodin, string PO_Url, string PO_Bandera, string PO_Proceso)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@PI_Id_Formulario", PI_Id_Formulario);
                SqlParameter param2 = new SqlParameter("@PI_Numero_de_Identificacion", PI_Numero_de_Identificacion);
                SqlParameter param3 = new SqlParameter("@PI_Comodin", PI_Comodin);
                SqlParameter param4 = new SqlParameter("@PO_Url", PO_Url);
                SqlParameter param5 = new SqlParameter("@PO_Bandera", PO_Bandera);
                SqlParameter param6 = new SqlParameter("@PO_Proceso", PO_Proceso);

                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("" + DB + ".dbo.PA_CONSULTA_PROCESO_ECP @PI_Id_Formulario, @PI_Numero_de_Identificacion, @PI_Comodin, @PO_Url, @PO_Bandera, @PO_Proceso ", @param1, param2, param3, param4, param5, param6);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        // SELECCIONA DATOS CON BASE A UNAS CONDICIONES DISPUESTA EN EL SP OPERACION 3 </jarp>
        public virtual ObjectResult<LogProcesosCrViviendaEcopetrol> GuardarLogAprobacionProceso(LogProcesosCrViviendaEcopetrol Obj)
        {
            try
            {
                SqlParameter param1 = new SqlParameter("@PI_IdUsuario", Obj.PI_IdUsuario);
                SqlParameter param2 = new SqlParameter("@PI_Id_Formulario", Obj.PI_Id_Formulario);
                SqlParameter param3 = new SqlParameter("@PI_Numero_De_Identificacion", Obj.PI_Numero_De_Identificacion);
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<LogProcesosCrViviendaEcopetrol>("" + DB + ".dbo.PA_GESTION_LOG_APROBACION_PROCESO @PI_IdUsuario,@PI_Id_Formulario, @PI_Numero_De_Identificacion", @param1, param2, param3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public virtual void GuardarLogAvaluos(int IdConsecutivo, string NitAfiliado, int NumServicio, string UsuarioSolicitud)
        {
            try
            {
                SqlParameter param = new SqlParameter("@Operacion", 1);
                SqlParameter param1 = new SqlParameter("@IdConsecutivo", IdConsecutivo);
                SqlParameter param2 = new SqlParameter("@NitAfiliado", NitAfiliado);
                SqlParameter param3 = new SqlParameter("@NumServicio", NumServicio);
                SqlParameter param4 = new SqlParameter("@UsuarioSolicitud", UsuarioSolicitud);
                ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("" + DB + ".dbo.PA_GUARDAR_LOG_AVALUOS @Operacion, @IdConsecutivo, @NitAfiliado, @NumServicio, @UsuarioSolicitud", @param, @param1, @param2, @param3, @param4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual ObjectResult<int>ObtenerNumSolicitudFechaAvaluo(string NitAfiliado)
        {
            try
            {
                AvaluosLog ObjAvaluosLog = new AvaluosLog();

                SqlParameter param = new SqlParameter("@Operacion", 2);
                SqlParameter param1 = new SqlParameter("@IdConsecutivo", ObjAvaluosLog.IdConsecutivo);
                SqlParameter param2 = new SqlParameter("@NitAfiliado", NitAfiliado);
                SqlParameter param3 = new SqlParameter("@NumServicio", ObjAvaluosLog.NumServicio);
                SqlParameter param4 = new SqlParameter("@UsuarioSolicitud", "".ToString());
                return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<int>("" + DB + ".dbo.PA_GUARDAR_LOG_AVALUOS @Operacion, @IdConsecutivo, @NitAfiliado, @NumServicio, @UsuarioSolicitud", @param, @param1, @param2, @param3, @param4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}