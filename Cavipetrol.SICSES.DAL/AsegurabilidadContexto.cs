﻿using Cavipetrol.SICSES.DAL.Mapeo.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL
{
    public partial class AsegurabilidadContexto : DbContext
    {
        public AsegurabilidadContexto() : base("name=REPORTES_SICSES")
        {

        }
        public DbSet<AsegurabilidadModel> TiposTasas { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AsegurabilidadMapa());
        }
        public virtual ObjectResult<VMPolizasVigentes> ObtenerPolizasVigentes(string tipo, string numero)
        {
            SqlParameter param1 = new SqlParameter("@TipoDocumento", tipo);
            SqlParameter param2 = new SqlParameter("@NumeroDocumento", numero);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<VMPolizasVigentes>("dbo.PA_CONSULTAR_POLIZAS_VIGENTES @TipoDocumento, @NumeroDocumento", @param1, @param2);
        }

        
    }
}
