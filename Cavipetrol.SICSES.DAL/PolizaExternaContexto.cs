﻿using Cavipetrol.SICSES.DAL.Mapeo.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.DAL
{
    public partial class PolizaExternaContexto : DbContext
    {
        public PolizaExternaContexto() : base("name=Asegurabilidad")
        {

        }

        public DbSet<Aseguradoras> Aseguradoras { get; set; }
        public DbSet<PolizasExternas> PolizasExternas { get; set; }

        public DbSet<TipoPoliza> TipoPoliza { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PolizasExternasMapa());
            modelBuilder.Configurations.Add(new AseguradorasMapa());
            modelBuilder.Configurations.Add(new TipoPolizaMapa());

        }

        public virtual ObjectResult<ViewModelPolizasExternas> ConsultarPolizasExternas(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza, int DiasVencimiento,string EstadoPoliza)
        {
            SqlParameter param1 = TipoIdentificacion == null ? new SqlParameter("@TipoIdentificacion", DBNull.Value ) : new SqlParameter("@TipoIdentificacion", TipoIdentificacion );
            SqlParameter param2 = NumeroDocumento == null ? new SqlParameter("@NumeroDocumento", DBNull.Value) : new SqlParameter("@NumeroDocumento", NumeroDocumento);
            SqlParameter param3 = Nombres == null ? new SqlParameter("@Nombres", DBNull.Value) : new SqlParameter("@Nombres", Nombres);
            SqlParameter param4 = NumeroPoliza == null ? new SqlParameter("@NumeroPoliza", DBNull.Value) : new SqlParameter("@NumeroPoliza", NumeroPoliza);
            SqlParameter param5 = DiasVencimiento == 0 ? new SqlParameter("@DiasVencimiento", DBNull.Value) : new SqlParameter("@DiasVencimiento", DiasVencimiento);
            SqlParameter param6 = EstadoPoliza == null ? new SqlParameter("@EstadoPoliza", DBNull.Value) : new SqlParameter("@EstadoPoliza", EstadoPoliza);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ViewModelPolizasExternas>("dbo.PA_CONSULTAR_INFORMACION_POLIZAS_EXTERNAS @TipoIdentificacion, @NumeroDocumento,@Nombres,@NumeroPoliza,@DiasVencimiento,@EstadoPoliza", @param1, @param2, @param3, @param4, @param5, @param6);
        }

        public virtual ObjectResult<ViewModelPolizasExternas> AdministrarPolizasExternas(ViewModelPolizasExternas modelo)
        {
            SqlParameter param1 = modelo.tipoIdentificacion == null ? new SqlParameter("@TSE_TipoNit", DBNull.Value) : new SqlParameter("@TSE_TipoNit", modelo.tipoIdentificacion);
            SqlParameter param2 = modelo.NumeroDocumento == null ? new SqlParameter("@TSE_NumNit", DBNull.Value) : new SqlParameter("@TSE_NumNit", modelo.NumeroDocumento);
            SqlParameter param3 = modelo.Nombre == null ? new SqlParameter("@TSE_Nombre_Apellidos", DBNull.Value) : new SqlParameter("@TSE_Nombre_Apellidos", modelo.Nombre);
            SqlParameter param4 = new SqlParameter("@TSE_Aseguradora", modelo.Aseguradora);
            SqlParameter param5 = modelo.NumeroPoliza == null ? new SqlParameter("@TSE_NumPoliza", DBNull.Value) : new SqlParameter("@TSE_NumPoliza", modelo.NumeroPoliza);
            SqlParameter param6 = new SqlParameter("@TSE_ValorAsegura", modelo.ValorAsegurado);
            SqlParameter param7 = new SqlParameter("@TSE_TipoPoliza", modelo.TipoPoliza);
            SqlParameter param8 = modelo.FechaDesde == null ? new SqlParameter("@TSE_VigenteDesde", DBNull.Value) : new SqlParameter("@TSE_VigenteDesde", modelo.FechaDesde);
            SqlParameter param9 = modelo.FechaHasta == null ? new SqlParameter("@TSE_VigenteHasta", DBNull.Value) : new SqlParameter("@TSE_VigenteHasta", modelo.FechaHasta);
            SqlParameter param10 = new SqlParameter("@TSE_AplicarColectiva", modelo.TSE_AplicarColectiva);
            SqlParameter param11 = modelo.nitTomador == null ? new SqlParameter("@TSE_TipoNitTomador", DBNull.Value) : new SqlParameter("@TSE_TipoNitTomador", modelo.nitTomador);
            SqlParameter param12 = modelo.NumeroTomador == null ? new SqlParameter("@TSE_NumNitTomador", DBNull.Value) : new SqlParameter("@TSE_NumNitTomador", modelo.NumeroTomador);
            SqlParameter param13 = modelo.NombreTomaPoliza == null ? new SqlParameter("@TSE_NombreTomaPoliza", DBNull.Value) : new SqlParameter("@TSE_NombreTomaPoliza", modelo.NombreTomaPoliza);
            SqlParameter param14 = modelo.Observaciones == null ? new SqlParameter("@TSE_Observaciones", DBNull.Value) : new SqlParameter("@TSE_Observaciones", modelo.Observaciones);
            SqlParameter param15 = modelo.Direcciones == null ? new SqlParameter("@TSE_Direccion", DBNull.Value) : new SqlParameter("@TSE_Direccion", modelo.Direcciones);
            SqlParameter param16 = modelo.Matricula == null ? new SqlParameter("@TSE_Matricula", DBNull.Value) : new SqlParameter("@TSE_Matricula", modelo.Matricula);
            SqlParameter param17 = modelo.oficina == null ? new SqlParameter("@TSE_Oficina", DBNull.Value) : new SqlParameter("@TSE_Oficina", modelo.oficina);
            SqlParameter param18 = modelo.EstadoPoliza == null ? new SqlParameter("@TSE_ACTIVO", DBNull.Value) : new SqlParameter("@TSE_ACTIVO", modelo.EstadoPoliza);
            SqlParameter param19 = modelo.Producto == null ? new SqlParameter("@producto", DBNull.Value) : new SqlParameter("@producto", modelo.Producto);
            SqlParameter param20 = modelo.NumeroDocu == null ? new SqlParameter("@NumeroDocu", DBNull.Value) : new SqlParameter("@NumeroDocu", modelo.NumeroDocu);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ViewModelPolizasExternas>("dbo.PA_Administrar_Polizas_Externas @TSE_TipoNit, @TSE_NumNit,@TSE_Nombre_Apellidos,@TSE_Aseguradora,@TSE_NumPoliza,@TSE_ValorAsegura,@TSE_TipoPoliza,@TSE_VigenteDesde,@TSE_VigenteHasta,@TSE_AplicarColectiva,@TSE_TipoNitTomador,@TSE_NumNitTomador,@TSE_NombreTomaPoliza,@TSE_Observaciones,@TSE_Direccion,@TSE_Matricula,@TSE_Oficina,@producto,@NumeroDocu,@TSE_ACTIVO", @param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10, @param11, @param12, @param13, @param14, @param15, @param16, @param17, @param18, param19, param20);
        }

        public virtual ObjectResult<ViewModelPolizasExternas> EliminarPolizaExterna(string TipoIdentificacion, string NumeroIdentificacion, string Aseguradora, string NumeroPoliza)
        {
            SqlParameter param1 = TipoIdentificacion == null ? new SqlParameter("@TSE_TipoNit", DBNull.Value) : new SqlParameter("@TSE_TipoNit", TipoIdentificacion);
            SqlParameter param2 = NumeroIdentificacion == null ? new SqlParameter("@TSE_NumNit", DBNull.Value) : new SqlParameter("@TSE_NumNit", NumeroIdentificacion);
            SqlParameter param3 = Aseguradora == null ? new SqlParameter("@TSE_Aseguradora", DBNull.Value) : new SqlParameter("@TSE_Aseguradora", Aseguradora);
            SqlParameter param4 = NumeroPoliza == null ? new SqlParameter("@TSE_NumPoliza", DBNull.Value) : new SqlParameter("@TSE_NumPoliza", NumeroPoliza);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ViewModelPolizasExternas>("dbo.PA_EliminarPolizasExternas @TSE_TipoNit, @TSE_NumNit,@TSE_Aseguradora,@TSE_NumPoliza", @param1, @param2, @param3, @param4);
        }

        public virtual ObjectResult<HistoricoPolizasExternas> ConsultarHistoricoPoliza(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza)
        {
            SqlParameter param1 = TipoIdentificacion == null ? new SqlParameter("@TipoIdentificacion", DBNull.Value) : new SqlParameter("@TipoIdentificacion", TipoIdentificacion);
            SqlParameter param2 = NumeroDocumento == null ? new SqlParameter("@NumeroDocumento", DBNull.Value) : new SqlParameter("@NumeroDocumento", NumeroDocumento);
            SqlParameter param3 = Nombres == null ? new SqlParameter("@Nombres", DBNull.Value) : new SqlParameter("@Nombres", Nombres);
            SqlParameter param4 = NumeroPoliza == null ? new SqlParameter("@NumeroPoliza", DBNull.Value) : new SqlParameter("@NumeroPoliza", NumeroPoliza);  
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<HistoricoPolizasExternas>("dbo.PA_CONSULTAR_HISTORICO_POLIZAS_EXTERNAS @TipoIdentificacion, @NumeroDocumento,@Nombres,@NumeroPoliza", @param1, @param2, @param3, @param4);
        }

        public virtual ObjectResult<RespuestaValidacion> ValidarPolizasExternas(string aseguradora, string tipopoliza, string producto, string documento, string nit)
        {
            SqlParameter param1 = aseguradora == null ? new SqlParameter("@aseguradora", DBNull.Value) : new SqlParameter("@aseguradora", aseguradora);
            SqlParameter param2 = tipopoliza == null ? new SqlParameter("@tipopoliza", DBNull.Value) : new SqlParameter("@tipopoliza", tipopoliza);
            SqlParameter param3 = producto == null ? new SqlParameter("@producto", DBNull.Value) : new SqlParameter("@producto", producto);
            SqlParameter param4 = documento == null ? new SqlParameter("@documento", DBNull.Value) : new SqlParameter("@documento", documento);
            SqlParameter param5 = nit == null ? new SqlParameter("@nit", DBNull.Value) : new SqlParameter("@nit", nit);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<RespuestaValidacion>("dbo.PA_VALIDAR_POLIZAS_EXTERNAS @aseguradora, @tipopoliza,@producto,@documento,@nit", @param1, @param2, @param3, @param4, param5);
        }

        public virtual ObjectResult<ViewModelPolizasExternas> GuardarPolizaExternaCredito(string TipoNit, string NumNit, string Aseguradora, string NumeroPoliza, string Documento, string Producto)
        {
            SqlParameter param1 = TipoNit == null ? new SqlParameter("@TSC_TipoNit", DBNull.Value) : new SqlParameter("@TSC_TipoNit", TipoNit);
            SqlParameter param2 = NumNit == null ? new SqlParameter("@TSC_NumNit", DBNull.Value) : new SqlParameter("@TSC_NumNit", NumNit);
            SqlParameter param3 = Aseguradora == null ? new SqlParameter("@TSC_Aseguradora", DBNull.Value) : new SqlParameter("@TSC_Aseguradora", Aseguradora);
            SqlParameter param4 = NumeroPoliza == null ? new SqlParameter("@TSC_NumPoliza", DBNull.Value) : new SqlParameter("@TSC_NumPoliza", NumeroPoliza);
            SqlParameter param5 = Documento == null ? new SqlParameter("@Numero_Docu", DBNull.Value) : new SqlParameter("@Numero_Docu", Documento);
            SqlParameter param6 = Producto == null ? new SqlParameter("@Producto", DBNull.Value) : new SqlParameter("@Producto", Producto);
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ViewModelPolizasExternas>("dbo.SP_GuardarPolizaExternaCreditos @TSC_TipoNit, @TSC_NumNit,@TSC_Aseguradora,@TSC_NumPoliza,@Numero_Docu,@Producto", @param1, @param2, @param3, @param4, param5, param6);
        }

        public virtual ObjectResult<Infraestructura.Model.ProductoPorAsociado> ObtenerProductoPorAsociado(string Cedula)
        {
            SqlParameter param1 = Cedula == null ? new SqlParameter("@Cedula", DBNull.Value) : new SqlParameter("@Cedula", Cedula);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Infraestructura.Model.ProductoPorAsociado>("dbo.PA_CONSULTAR_PRODUCTOASOCIADO @Cedula", @param1);

        }


    }
}