﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.Model.ReportesUIAF;
using System;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace Cavipetrol.SICSES.DAL
{
    public partial class SICSESContexto
    {
        public virtual ObjectResult<ReporteInformacionEstadistica> ObtenerReporteInformacionEstadisticaInicial(DateTime fecha_perido)
        {
            var @params = new[]{
               new SqlParameter("fecha_perido", fecha_perido)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteInformacionEstadistica>("dbo.PA_REPORTE_INFORMACION_ESTADISTICA @fecha_perido = @fecha_perido", @params);
        }

        public virtual ObjectResult<ReporteOrganosDireccionyControl> ObtenerReporteOrganosDirreccionControl()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteOrganosDireccionyControl>("dbo.PA_REPORTE_GENERALIDADES_DIRECTIVOS");
        }

        public virtual ObjectResult<ReporteIndividualDeCarteraDeCredito> ObtenerReporteindividualDeCarteraDeCredito(int Mes, int Anio)
        {
            var @params = new[]{
               new SqlParameter("ANIO", Anio),
               new SqlParameter("MES", Mes)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteIndividualDeCarteraDeCredito>("dbo.PA_CONSULTAR_INFORMACION_INDIVIDUAL_CARTERA_CREDITO @ANIO = @ANIO, @MES = @MES", @params);
        }
        public virtual ObjectResult<ReporteUsuarios> ObtenerReporteUsuarios(DateTime fechaCorte)
        {
            SqlParameter param1 = new SqlParameter("@FECHA_CORTE", fechaCorte);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteUsuarios>("dbo.PA_REPORTE_USUARIOS_INTERNOS_EXTERNOS @FECHA_CORTE", @param1);
        }


        public virtual ObjectResult<ReporteRedOficinasYCorresponsalesNoBancarios> ObtenerRedOficinasYCorresponsalesNoBancarios()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteRedOficinasYCorresponsalesNoBancarios>("dbo.PA_REPORTE_RED_OFICINAS_CORRESPONSALES");
        }

        public virtual ObjectResult<ReporteHistorico> ConsultarHistorico()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteHistorico>("dbo.PA_CONSULTAR_HISTORICO_REPORTES");
        }

        public virtual ObjectResult<ReporteParentescos> ObtenerInformeParentescos()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteParentescos>("dbo.PA_REPORTE_INDIVIDUAL_PARENTESCOS_OTROS_VINCULOS");
        }

        public virtual ObjectResult<ReporteRelacionBienesRecibosPago> ObtenerRelacionBienesPagoRecibidos()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteRelacionBienesRecibosPago>("dbo.PA_RELACION_BIENES_RECIBIDOS_ENPAGO");
        }
        public virtual ObjectResult<ReporteIndividualCarteraCredito> ObtenerInformacionCarteraCredito(string anho, string mes)
        {
            DateTime FechaConsulta = DateTime.Now;
            DateTime.TryParse(anho + "/" + mes + "/01", out FechaConsulta);

            var @params = new[]{
               new SqlParameter("FechaConsulta", FechaConsulta)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteIndividualCarteraCredito>("dbo.PA_CONSULTA_INFORMACION_INDIVIDUAL_CARTERA_CREDITO @FechaConsulta = @FechaConsulta", @params);
        }
        public virtual ObjectResult<ReporteInformeIndividualCaptaciones> ObtenerReporteInformeIndividualCaptaciones(DateTime fechaCorte)
        {


            var @params = new[]{
               new SqlParameter("fecha_perido", fechaCorte)
            };
            
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteInformeIndividualCaptaciones>("dbo.PA_INFORME_INDIVIDUAL_CAPTACIONES @fecha_perido = @fecha_perido", @params);
        }



        public virtual ObjectResult<ReportePUC> ObtenerReportePUC(DateTime fechaCorte,int periodo)
        {
           
            var @params = new Object[]{
               new SqlParameter("Fecha_Corte", fechaCorte),
               new SqlParameter("Parametro", periodo)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReportePUC>
                ("dbo.PA_PLAN_UNICO_CUENTAS @Fecha_Corte, @Parametro", @params);
        }

        public virtual ObjectResult<ReporteAportesContribuciones> ObtenerReporteAportesContribuciones(DateTime fecha_periodo)
        {
            SqlParameter param1 = new SqlParameter("@FECHA_PERIODO", fecha_periodo);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteAportesContribuciones>("dbo.PA_INFORME_INDIVIDUAL_APORTES_CONTRIBUCIONES @FECHA_PERIODO", @param1);
        }


        public virtual ObjectResult<ReporteRelacionPropiedadesPlantaEquipo> ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(DateTime fechaCorte)
        {

            var @params = new[]{
               new SqlParameter("fecha_perido", fechaCorte)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteRelacionPropiedadesPlantaEquipo>("dbo.PA_RELACION_PROPIEDADES_PLANTA_EQUIPO @fecha_perido = @fecha_perido", @params);
        }

        public virtual ObjectResult<ReporteRelacionInversiones> ObtenerReporteInformeRelacionInversiones(DateTime fecha_perido)
        {
            var @params = new[]{
               new SqlParameter("fecha_perido", fecha_perido)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteRelacionInversiones>("dbo.PA_RELACION_INVERSIONES @fecha_perido = @fecha_perido", @params);
        }

        //Reportes UIAF
        public virtual ObjectResult<ReporteProductosOfrecidos> ObtenerReporte4Uiaf()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteProductosOfrecidos>("dbo.PA_RELACION_BIENES_RECIBIDOS_ENPAGO");
        }

        public virtual ObjectResult<ReporteTransaccionesEnEfectivo> ObtenerReporteTransaccionesEnEfectivo(DateTime fechaInicio, DateTime fechaFin)
        {
            var @params = new Object[]{
               new SqlParameter("fecha_inicio", fechaInicio),
               new SqlParameter("fecha_FIN", fechaFin)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteTransaccionesEnEfectivo>
                ("dbo.PA_TRANSACCIONES_EFECTIVO @fecha_inicio, @fecha_FIN", @params);
        }

        public virtual ObjectResult<ReporteTransaccionesEnEfectivoRiesgos> ObtenerReporteTransaccionesEnEfectivoRiesgos(DateTime fechaInicio, DateTime fechaFin)
        {
            var @params = new Object[]{
               new SqlParameter("fecha_inicio", fechaInicio),
               new SqlParameter("fecha_FIN", fechaFin)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteTransaccionesEnEfectivoRiesgos>
                ("dbo.PA_TRANSACCIONES_EFECTIVO_FCT_006 @fecha_inicio, @fecha_FIN", @params);
        }

        public virtual ObjectResult<ReporteProductosEconomiaSolidaria> ObtenerReporteProductosEconomiaSolidaria(DateTime fechaCorte)
        {
            var @params = new[]{
               new SqlParameter("fecha_perido", fechaCorte)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteProductosEconomiaSolidaria>("dbo.PA_PRODUCTOS_ECONOMIA_SOLIDARIA @fecha_perido = @fecha_perido", @params);
        }

        public virtual ObjectResult<ClasificacionExcedentes> ObtenerAplicacionExcedentes(DateTime fechaCorte)
        {

            var @params = new[]{
               new SqlParameter("fecha_perido", fechaCorte)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ClasificacionExcedentes>("dbo.PA_RELACION_PROPIEDADES_PLANTA_EQUIPO @fecha_perido = @fecha_perido", @params);
        }

        public virtual ObjectResult<ReporteRetiroeIngresoAsociados>ObtenerInformeRetiroeIngresoAsociados(DateTime fechaCorte)
        {

            var @params = new[]{
               new SqlParameter("fecha_perido", fechaCorte)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteRetiroeIngresoAsociados>("dbo.PA_INFORME_RETIROEINGRESO_ASOCIADOS @fecha_perido = @fecha_perido", @params);
        }

        public virtual ObjectResult<ReporteRetiroeIngresoAsociados> ObtenerInformeIndividualCaptaciones(DateTime fechaCorte)
        {

            var @params = new[]{
               new SqlParameter("fecha_perido", fechaCorte)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteRetiroeIngresoAsociados>("dbo.PA_INFORME_RETIROEINGRESO_ASOCIADOS @fecha_perido = @fecha_perido", @params);
        }        

        public virtual ObjectResult<ReporteDetalleInformacionEstadistica> ObtenerReporteDetalleInformacionEstadistica(DateTime fecha_perido)
        {
            var @params = new[]{
               new SqlParameter("fecha_perido", fecha_perido)
            };

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ReporteDetalleInformacionEstadistica>("dbo.PA_REPORTE_DETALLE_INFORMACION_ESTADISTICA @fecha_perido = @fecha_perido", @params);
        }

    }
}

