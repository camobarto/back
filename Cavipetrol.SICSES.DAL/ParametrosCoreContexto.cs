﻿using Cavipetrol.SICSES.DAL.Mapeo.Ubicacion;
using Cavipetrol.SICSES.Infraestructura.Model;
using System.Data.Entity;
using Cavipetrol.SICSES.DAL.Mapeo.Parametros;
using Cavipetrol.SICSES.DAL.Mapeo;
using Cavipetrol.SICSES.DAL.Mapeo.Reportes;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System;
using System.Data.Entity.Infrastructure;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.DAL.Mapeo.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;

namespace Cavipetrol.SICSES.DAL
{
    public partial class ParametrosCoreContexto : DbContext
    {
        public ParametrosCoreContexto() : base("name=PARAMETROS_SICSES")
        {

        }

        public DbSet<Municipio> Municipios { get; set; }
        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<AsociacionEntidadSolidaria> AsociacionesEntidadSolidaria { get; set; }
        public DbSet<FormatoVigenteSICSES> FormatosVigentesSICSES { get; set; }
        public DbSet<TipoContrato> TiposContratos { get; set; }
        public DbSet<TipoEstadoCivil> TiposEstadosCiviles { get; set; }
        public DbSet<TipoEstrato> TiposEstratos { get; set; }
        public DbSet<TipoIdentificacion> TiposIdentificaciones { get; set; }
        public DbSet<TipoJornadaLaboral> TiposJornadasLaborales { get; set; }
        public DbSet<TipoNivelEscolaridad> TiposNivelesEscolaridad { get; set; }
        public DbSet<TipoOcupacion> TiposOcupaciones { get; set; }
        public DbSet<TipoOrganoDirectivoControl> TiposOrganosDirectivosControl { get; set; }
        public DbSet<TipoParentescoGradoConsanguinidad> TiposParentescoGradoConsanguinidad { get; set; }
        public DbSet<TipoRol> TiposRoles { get; set; }

        public DbSet<TipoEntidad> TiposEntidades { get; set; }
        public DbSet<TipoSectorEconomico> TiposSectoresEconomicos { get; set; }
        public DbSet<Meses> Meses { get; set; }

        public DbSet<ClasificacionCIIU> TiposClasificacionCIIU { get; set; }

        public DbSet<Anos> Ano { get; set; }

        public DbSet<ClasificacionExcedentes> Excedentes { get; set; }

        public DbSet<TiposTitulos> TiposTitulos { get; set; }

        public DbSet<ClaseGarantia> ClaseGarantia { get; set; }
        public DbSet<DestinoCredito> DestinoCredito { get; set; }
        public DbSet<ClaseVivienda> ClaseVivienda { get; set; }
        public DbSet<CategoriaReestructurado> CategoriaRestructurado { get; set; }

        public DbSet<ClaseNaturaleza> ClaseNaturaleza { get; set; }
        public DbSet<ClaseEstadoProcesoActual> ClaseEstadoProcesoActual { get; set; }
        public DbSet<ClaseConcepto> ClaseConcepto { get; set; }

        public DbSet<FormCapacidadPago> FormCapacidadPago { get; set; }

        public DbSet<FormCapacidadPagoTipoCreditoTipoRolRelacion> FormCapacidadPagoTipoCreditoTipoRolRelacion { get; set; }
        public DbSet<FormCapacidadPagoGrupos> FormCapacidadPagoGrupos { get; set; }

        public DbSet<EvaluacionRiesgoLiquidez> RiesgoLiquidez { get; set; }

        public DbSet<TipoObligacion> TiposObligaciones { get; set; }

        public DbSet<TipoCuota> Tiposcuotas { get; set; }

        public DbSet<TipoClaseGarantia> TiposClaseGarantia { get; set; }

        public DbSet<TipoEntidadBeneficiaria> TipoEntidadBeneficiaria { get; set; }

        public DbSet<TipoModalidad> TipoModalidad { get; set; }

        public DbSet<TipoDestino> TipoDestino { get; set; }

        public DbSet<NivelEducacion> NivelEducacion { get; set; }

        public DbSet<TipoBeneficiarios> TipoBeneficiarios { get; set; }

        public DbSet<AlternativasDecreto2880> AlternativasDecreto2880 { get; set; }
        public DbSet<TipoUsuarioTieneAporte> TipoUsuarioTieneAporte { get; set; }
        public DbSet<ClaseDeActivo> ClaseDeActivo { get; set; }
        public DbSet<TipoLineaCredito> TipoLineaCredito { get; set; }
        public DbSet<TipoCredito> TipoCredito { get; set; }

        public DbSet<InformacionRelacionadaGrupoInteres> GrupoInteres { get; set; }

        public DbSet<TipoTitulo> TipoDetitulo { get; set; }

        public DbSet<TipoTasa> TiposTasas { get; set; }

        public DbSet<TipoModalidadContratacion> TipoModalidadContratacion { get; set; }
        public DbSet<TipoProductoServicioContratado> TipoProductoServicioContratado { get; set; }

        public DbSet<TipoCreditoTipoRolRelacion> TiposCreditoTiposRolRelacion { get; set; }
        public DbSet<Etiquetas> Etiquetas { get; set; }
        public DbSet<Parametros> Parametros { get; set; }
        public DbSet<TiposDocumentoRequisito> TiposDocumentoRequisito { get; set; }
        public DbSet<Plantilla> Plantilla { get; set; }
        public DbSet<Cupo> Cupo { get; set; }
        public DbSet<TiposCupo> TipoCupo { get; set; }
        public DbSet<TiposCreditoTipoSeguroRelacion> TiposCreditoTipoSeguroRelacion { get; set; }
        public DbSet<TiposCreditoTiposGarantiaRelacion> TiposCreditoTiposGarantiaRelacion { get; set; }
        public DbSet<TiposGarantia> TiposGarantia { get; set; }

        public DbSet<Bimestre> Bimestre { get; set; }
        public DbSet<TiposGarantiasPagares> TiposGarantiasPagares { get; set; }

        public DbSet<UsosCredito> UsosCredito { get; set; }
        public DbSet<AtribucionesAprobacionCredito> AtribucionesAprobacionCredito { get; set; }
        public DbSet<CuposAsegurabilidad> CuposAsegurabilidad { get; set; }

        public DbSet<PapelCavipetrol> PapelCavipetrol { get; set; }
        public DbSet<FormaPago> FormaPago { get; set; }
        public DbSet<FormaPagoRelacion> FormaPagoRelacion { get; set; }
        public DbSet<SeguroTasas> SeguroTasas { get; set; }
        public DbSet<TiposPagareTiposConceptoRelacion> TiposPagareTiposConceptoRelacion { get; set; }
        public DbSet<TiposConceptosGarantia> TiposConceptosGarantia { get; set; }
        public DbSet<TipoConceptoAdjuntoRelacion> TipoConceptoAdjuntoRelacion { get; set; }
        public DbSet<TipoConceptoAdjunto> TipoConceptoAdjunto { get; set; }

        public DbSet<GarantiaVariableRelacion> GarantiaVariableRelacion { get; set; }

        public DbSet<Oficina> Oficina { get; set; }

        public DbSet<TiposContrato> TiposContratoSolicitud { get; set; }

        public DbSet<SolicitudDescripcionFondos> SolicitudDescripcionFondos { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DepartamentoMapa());
            modelBuilder.Configurations.Add(new MunicipioMapa());
            modelBuilder.Configurations.Add(new AsociacionEntidadSolidariaMapa());
            modelBuilder.Configurations.Add(new FormatoVigentesSICSESMapa());
            modelBuilder.Configurations.Add(new TipoContratoMapa());
            modelBuilder.Configurations.Add(new TipoEstadoCivilMapa());
            modelBuilder.Configurations.Add(new TipoEstratoMapa());
            modelBuilder.Configurations.Add(new TipoIdentificacionMapa());
            modelBuilder.Configurations.Add(new TipoJornadaLaboralMapa());
            modelBuilder.Configurations.Add(new TipoNivelEscolaridadMapa());
            modelBuilder.Configurations.Add(new TipoOcupacionMapa());
            modelBuilder.Configurations.Add(new TipoOrganoDirectivoControlMapa());
            modelBuilder.Configurations.Add(new TipoParentescoGradoConsanguinidadMapa());
            modelBuilder.Configurations.Add(new TipoRolMapa());
            modelBuilder.Configurations.Add(new TipoSectorEconomicoMapa());
            modelBuilder.Configurations.Add(new MesesMapa());
            modelBuilder.Configurations.Add(new TipoEntidadMapa());
            modelBuilder.Configurations.Add(new TipoClasificacionCIIU());
            modelBuilder.Configurations.Add(new AnosMapa());
            modelBuilder.Configurations.Add(new ReporteClasificacionExcedentes());
            modelBuilder.Configurations.Add(new FormCapacidadPagoMapa());
            modelBuilder.Configurations.Add(new FormCapacidadPagoTipoCreditoTipoRolRelacionMapa());
            modelBuilder.Configurations.Add(new FormCapacidadPagoGruposMapa());
            modelBuilder.Configurations.Add(new ReporteRiesgoLiquidez());
            modelBuilder.Configurations.Add(new TiposTitulosMapa());
            modelBuilder.Configurations.Add(new ClaseGarantiaMapa());
            modelBuilder.Configurations.Add(new DestinoCreditoMapa());
            modelBuilder.Configurations.Add(new ClaseViviendaMapa());
            modelBuilder.Configurations.Add(new CategoriaRestructuradoMapa());
            modelBuilder.Configurations.Add(new TipoObligacionMapa());
            modelBuilder.Configurations.Add(new TipoCuotaMapa());
            modelBuilder.Configurations.Add(new TipoEntidadBeneficiariaMapa());
            modelBuilder.Configurations.Add(new TipoModalidadMapa());
            modelBuilder.Configurations.Add(new ClaseNaturalezaMapa());
            modelBuilder.Configurations.Add(new ClaseEstadoProcesoActualMapa());
            modelBuilder.Configurations.Add(new ClaseConceptoMapa());
            modelBuilder.Configurations.Add(new TipoDestinoMapa());
            modelBuilder.Configurations.Add(new NivelEducacionMapa());
            modelBuilder.Configurations.Add(new TipoBeneficiariosMapa());
            modelBuilder.Configurations.Add(new AlternativasDecreto2880Mapa());
            modelBuilder.Configurations.Add(new TipoUsuarioTieneAporteMapa());
            modelBuilder.Configurations.Add(new ClaseDeActivoMapa());
            modelBuilder.Configurations.Add(new InfoGrupoInteresMapa());
            modelBuilder.Configurations.Add(new TipoTituloMapa());
            modelBuilder.Configurations.Add(new TipoTasaMapa());

            modelBuilder.Configurations.Add(new TipoLineaCreditoMapa());
            modelBuilder.Configurations.Add(new TipoCreditoMapa());
            modelBuilder.Configurations.Add(new TipoModalidadContratacionMapa());
            modelBuilder.Configurations.Add(new TipoProductoServicioContratadoMapa());

            modelBuilder.Configurations.Add(new TiposCreditoTiposRolRelacionMapa());
            modelBuilder.Configurations.Add(new EtiquetasMapa());
            modelBuilder.Configurations.Add(new ParametrosMapa());
            modelBuilder.Configurations.Add(new TiposDocumentoRequisitoMapa());
            modelBuilder.Configurations.Add(new PlantillaMapa());
            modelBuilder.Configurations.Add(new CupoMapa());
            modelBuilder.Configurations.Add(new TipoCupoMapa());
            modelBuilder.Configurations.Add(new TiposCreditoTiposSeguroRelacionMapa());
            modelBuilder.Configurations.Add(new TiposCreditoTiposGarantiaRelacionMapa());
            modelBuilder.Configurations.Add(new TiposGarantiaMapa());
            modelBuilder.Configurations.Add(new BimestreMapa());
            modelBuilder.Configurations.Add(new TiposGarantiasPagaresMapa());
            modelBuilder.Configurations.Add(new UsosCreditoMapa());
            modelBuilder.Configurations.Add(new AtribucionesAprobacionCreditoMapa());
            modelBuilder.Configurations.Add(new CuposAsegurabilidadMapa());
            modelBuilder.Configurations.Add(new PapelCavipetrolMapa());
            modelBuilder.Configurations.Add(new FormaPagoMapa());
            modelBuilder.Configurations.Add(new FormaPagoRelacionMapa());
            modelBuilder.Configurations.Add(new SeguroTasasMapa());
            modelBuilder.Configurations.Add(new TiposConceptosGarantiaMapa());
            modelBuilder.Configurations.Add(new TiposPagareTiposConceptoRelacionMapa());
            modelBuilder.Configurations.Add(new GarantiaVariableRelacionMapa());
            modelBuilder.Configurations.Add(new OficinaMapa());

            modelBuilder.Configurations.Add(new TipoConceptoAdjuntoRelacionMapa());
            modelBuilder.Configurations.Add(new TipoConceptoAdjuntoMapa());
            modelBuilder.Configurations.Add(new TiposContratoMapa());
            modelBuilder.Configurations.Add(new SolicitudDescripcionFondosMapa());
          
        }
        public virtual ObjectResult<TipoGarantiaCredito> ObtenerTiposGarantiasCredito(int? idTipoGarantiaCredito = null)
        {
            SqlParameter param1 = idTipoGarantiaCredito == null ?
            new SqlParameter("@ID_TIPO_CREDITO", DBNull.Value) :
            new SqlParameter("@ID_TIPO_CREDITO", idTipoGarantiaCredito);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<TipoGarantiaCredito>("dbo.PA_CONSULTAR_TIPOS_GARANTIA_TIPO_CREDITO @ID_TIPO_CREDITO", @param1);

        }

        public virtual ObjectResult<string> ValidarUsuarioCavipetrol(string NumeroIdentificacion)
        {
            SqlParameter param1 = NumeroIdentificacion == null ? new SqlParameter("@NumeroIdentificacion", DBNull.Value) :   new SqlParameter("@NumeroIdentificacion", NumeroIdentificacion);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<string>("CAVIPETROL.dbo.PA_ValidarUsuarioCavipetrol @NumeroIdentificacion", @param1);

        }
        public virtual ObjectResult<TiposConceptosAdjunto> ObtenerConceptosAdjuntos(string idConcepto)
        {
            SqlParameter param1 = idConcepto == null ? new SqlParameter("@idConcepto", DBNull.Value) : new SqlParameter("@idConcepto", idConcepto);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<TiposConceptosAdjunto>("CORE_PARAMETROS.dbo.PA_ObtenerConceptosTipoAdjunto @idConcepto", @param1);

        }
        public virtual ObjectResult<CiudadesServidor> ObtenerCiudadesServidor ()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CiudadesServidor>("CORE_PARAMETROS.dbo.PA_CIUDADES_SERVIDOR");
        }
        public virtual ObjectResult<Usuarios>  ObtenerUsuariosPorPerfil(string perfil)
        {
            SqlParameter param1 = perfil == null ? new SqlParameter("@perfil", DBNull.Value) : new SqlParameter("@perfil", perfil);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Usuarios>("SICAV.dbo.PA_ObtenerUsuarioPorPerfil @perfil", @param1);

        }
        public virtual ObjectResult<string> AsignarUsuarioSolicitud(int idSolicitud, int idUsuario)
        {
            SqlParameter param1 = idSolicitud == 0 ? new SqlParameter("@IdCredito", DBNull.Value) : new SqlParameter("@IdCredito", idSolicitud);
            SqlParameter param2 = idUsuario == 0 ? new SqlParameter("@idUsuario", DBNull.Value) : new SqlParameter("@idUsuario", idUsuario);

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<string>("SICAV.dbo.PA_RelacionCreditoUsuario @IdCredito,@idUsuario ", @param1, param2);

        }

        public virtual ObjectResult<ModelCartaSolicitudCredito> ObtenerCartaSolicitud(string idSolicitud)
        {
            SqlParameter param1 = Convert.ToInt32(idSolicitud) == 0 ? new SqlParameter("@idSolicitud", DBNull.Value) : new SqlParameter("@idSolicitud", Convert.ToInt32(idSolicitud));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<ModelCartaSolicitudCredito>("SICAV.dbo.PA_CONSULTAR_MODELO_SOLICITUD_CREDITO @idSolicitud ", @param1);

        }

        public virtual ObjectResult<Interproductos> ObtenerModelInterproductos(string idSolicitud)
        {
            SqlParameter param1 = Convert.ToInt32(idSolicitud) == 0 ? new SqlParameter("@idSolicitud", DBNull.Value) : new SqlParameter("@idSolicitud", Convert.ToInt32(idSolicitud));

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<Interproductos>("SICAV.dbo.PA_CONSULTAR_MODELO_SOLICITUD_INTERPRODUCTOS @idSolicitud ", @param1);

        }
		public virtual ObjectResult<CausalesFodes> ObtenerCausalesFodes()
		{
			return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<CausalesFodes>("CORE_PARAMETROS.[dbo].[PA_OBTENER_CAUSALES_FODES]");
		}

	}
}
