﻿using Cavipetrol.SICSES.Infraestructura.Model.fyc.Riesgos;
using System;
using System.Collections.Generic;


namespace Cavipetrol.SICSES.Facade
{
    public interface IRiesgosFacade
    {
        IEnumerable<SolicitudOperacion> ObtenerOperacion(string TipoIdentificacion, string NumeroIdentificacion, string NumeroOperacion);
        void GuardarDatosPersonaTransaccionEfectivo(int Operacion, string Tipoidentificacion, string Numeroidentificacion, string Primernombre,
                                                        string Segundonombre, string Primerapellido, string Segundoapellido, string Direccion, string Telefono,
                                                        string Consulta, string Tipopersona);
        void GuardarDatosOrigenFondos(int Operacion, int Opcion, string Consulta, string Detalle);
    }
}
