﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Sap;

namespace Cavipetrol.SICSES.Facade
{
    public class SapFacade: ISapFacade
    {
        private readonly Sap  _SapFacade;
        public SapFacade()
        {
            _SapFacade = new Sap();
        }

        public IEnumerable<AsiSoporte> ObtenerAsiSoporte()
        {
            return _SapFacade.ObtenerAsiSoporte();
        }

        public IEnumerable<Asientos> ConsultarAsientos(string AsiSoporte, string AsiConsecutivo, string Asioperacion, string AsiOpeConsecutivo, string Fecha, string Ciudad, string MensajeError)
        {
            return _SapFacade.ConsultarAsientos(AsiSoporte,  AsiConsecutivo, Asioperacion, AsiOpeConsecutivo,  Fecha, Ciudad, MensajeError);

        }
        public IEnumerable<string> ActualizarAsiento()
        {
            return _SapFacade.ActualizarAsiento();
        }

        public IEnumerable<Proveedores> ConsultaProveedores(string CardCode, string CardName)
        {
            return _SapFacade.ConsultaProveedores( CardCode,  CardName);
        }


        public IEnumerable<string> ActualizarEstadoProveedores()
        {
            return _SapFacade.ActualizarEstadoProveedores();
        }

        public IEnumerable<MensajeErrorAsientos> ConsultarMensajeError()
        {
            return _SapFacade.ConsultarMensajeError();
        }


    }
}
