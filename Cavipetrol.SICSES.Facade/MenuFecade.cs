﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Menu;

namespace Cavipetrol.SICSES.Facade
{
    public class MenuFecade : IMenuFecade
    {
        private readonly BLL.Controllers.Menu _menu;
        public MenuFecade()
        {
            _menu = new BLL.Controllers.Menu();
        }

        public List<ViewModelMenu> ObtenerMenu(List<Perfil> listaPerfiles)
        {
            return _menu.ObtenerMenu(listaPerfiles);
        }
        public List<ViewModelMenu> MenuCompleto()
        {
            return _menu.MenuCompleto().ToList();
        }
        public bool ActualizarMenuPerfil(Cavipetrol.SICSES.Infraestructura.Model.Sicav.ListaMenuPerfil perfilMenuRelacion)
        {
            return _menu.ActualizarMenuPerfil(perfilMenuRelacion);
        }
    }
}
