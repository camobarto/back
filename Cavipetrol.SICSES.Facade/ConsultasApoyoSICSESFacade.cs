﻿using System.Collections.Generic;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Menu;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System.Linq;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;

namespace Cavipetrol.SICSES.Facade
{
    public class ConsultasApoyoSICSESFacade : IConsultasApoyoSICSESFacade
    {
        private readonly ConsultasApoyo _consultasApoyo;
        public ConsultasApoyoSICSESFacade()
        {
            _consultasApoyo = new ConsultasApoyo();
        }
        public IEnumerable<AsociacionEntidadSolidaria> ObtenerAsociacionesEntidadesSolidarias()
        {
            return _consultasApoyo.ObtenerAsociacionesEntidadesSolidarias();
        }

        public IEnumerable<Departamento> ObtenerDepartamentos()
        {
            return _consultasApoyo.ObtenerDepartamentos();
        }

        public IEnumerable<FormatoVigenteSICSES> ObtenerFormatosVigentesSICSES()
        {
            return _consultasApoyo.ObtenerFormatosVigentesSICSES();
        }

        public IEnumerable<Municipio> ObtenerMunicipiosPorIdDepartamento(string idDepartamento)
        {
            return _consultasApoyo.ObtenerMunicipiosPorIdDepartamento(idDepartamento);
        }

        public IEnumerable<TipoNivelEscolaridad> ObtenerNivelesEscolaridad()
        {
            return _consultasApoyo.ObtenerNivelesEscolaridad();
        }

        public IEnumerable<TipoContrato> ObtenerTiposContratos()
        {
            return _consultasApoyo.ObtenerTiposContratos();
        }

        public IEnumerable<TipoEstadoCivil> ObtenerTiposEstadosCiviles()
        {
            return _consultasApoyo.ObtenerTiposEstadosCiviles();
        }

        public IEnumerable<TipoEstrato> ObtenerTiposEstratos()
        {
            return _consultasApoyo.ObtenerTiposEstratos();
        }

        public IEnumerable<TipoIdentificacion> ObtenerTiposIdentificaciones()
        {
            return _consultasApoyo.ObtenerTiposIdentificaciones();
        }

        public IEnumerable<TipoJornadaLaboral> ObtenerTiposJornadasLaborales()
        {
            return _consultasApoyo.ObtenerTiposJornadasLaborales();
        }

        public IEnumerable<TipoOcupacion> ObtenerTiposOcupacion()
        {
            return _consultasApoyo.ObtenerTiposOcupacion();
        }

        public IEnumerable<TipoOrganoDirectivoControl> ObtenerTiposOrganosDirectivosControl()
        {
            return _consultasApoyo.ObtenerTiposOrganosDirectivosControl();
        }

        public IEnumerable<TipoRol> ObtenerTiposRoles()
        {
            return _consultasApoyo.ObtenerTiposRoles();
        }

        public IEnumerable<TipoSectorEconomico> ObtenerTiposSectorEconomico()
        {
            return _consultasApoyo.ObtenerTiposSectorEconomico();
        }
        public IEnumerable<TipoEntidad> ObtenerEntidades()
        {
            return _consultasApoyo.ObtenerEntidades();
        }

        public IEnumerable<ClasificacionCIIU> ObtenerClasificacionCUU()
        {
            return _consultasApoyo.ObtenerClasificacionCUU();
        }

        public IEnumerable<Meses> ObtenerMeses()
        {
            return _consultasApoyo.ObtenerMeses();
        }

        public IEnumerable<Anos> ObtenerAnos()
        {
            return _consultasApoyo.ObtenerAnos();
        }


        public IEnumerable<ClasificacionExcedentes> ObtenerInfoExcedentes()
        {
            return _consultasApoyo.ObtenerInfoExcedentes();
        }
        public IEnumerable<FormCapacidadPago> ObtenerFormCapacidadPago(int? idLineaCredito = null) {
            return _consultasApoyo.ObtenerFormCapacidadPago(idLineaCredito);
        }
        public IEnumerable<ViewModelFormCapacidadPagoGrupos> ObtenerFormCapacidadPagoGrupos(int? idTipoCreditoTipoRolRelacion = null)
        {
            return _consultasApoyo.ObtenerFormCapacidadPagoGrupos(idTipoCreditoTipoRolRelacion);
        }

        public IEnumerable<EvaluacionRiesgoLiquidez> ObtenerInfoEvaluacionRiesgoLiquidez()
        {
            return _consultasApoyo.ObtenerInfoEvaluacionRiesgoLiquidez();
        }

        public IEnumerable<TiposTitulos> ObtenerTiposTitulos()
        {
            return _consultasApoyo.ObtenerTiposTitulos();
        }

        public IEnumerable<TipoObligacion> ObtenerTipoOligacion()
        {
            return _consultasApoyo.ObtenerTipoOligacion();
        }

        public IEnumerable<TipoCuota> ObtenerTipoCuota()
        {
            return _consultasApoyo.ObtenerTipoCuota();
        }

        public IEnumerable<ClaseGarantia> ObtenerClaseGarantia()
        {
            return _consultasApoyo.ObtenerClaseGarantia();
        }
        public IEnumerable<DestinoCredito> ObtenerDestinoCredito()
        {
            return _consultasApoyo.ObtenerDestinoCredito();
        }
        public IEnumerable<ClaseVivienda> ObtenerClaseVivienda()
        {
            return _consultasApoyo.ObtenerClaseVivienda();
        }
        public IEnumerable<CategoriaReestructurado> ObtenerCategoriaRestructurado()
        {
            return _consultasApoyo.ObtenerCategoriaRestructurado();
        }

        public IEnumerable<TipoEntidadBeneficiaria> ObtenerTipoEntidadBeneficiaria()
        {
            return _consultasApoyo.ObtenerTipoEntidadBeneficiaria();
        }

        public IEnumerable<TipoModalidad> ObtenerTipoModalidad()
        {
            return _consultasApoyo.ObtenerTipoModalidad();
        }
        public IEnumerable<ClaseNaturaleza> ObtenerClaseNaturaleza()
        {
            return _consultasApoyo.ObtenerClaseNaturaleza();
        }
        public IEnumerable<ClaseEstadoProcesoActual> ObtenerClaseEstadoProcesoActual()
        {
            return _consultasApoyo.ObtenerClaseEstadoProcesoActual();
        }
        public IEnumerable<ClaseConcepto> ObtenerClaseConcepto()
        {
            return _consultasApoyo.ObtenerClaseConcepto();
        }

        public IEnumerable<TipoDestino> ObtenerTipoDestino()
        {
            return _consultasApoyo.ObtenerTipoDestino();
        }

        public IEnumerable<NivelEducacion> ObtenerNivelEducacion()
        {
            return _consultasApoyo.ObtenerNivelEducacion();
        }

        public IEnumerable<TipoBeneficiarios> ObtenerTiposBeneficiarios()
        {
            return _consultasApoyo.ObtenerTiposBeneficiarios();
        }

        public IEnumerable<AlternativasDecreto2880> ObtenerAlternativasDecreto2880()
        {
            return _consultasApoyo.ObtenerAlternativasDecreto2880();
        }
        public IEnumerable<TipoUsuarioTieneAporte> ObtenerTipoUsuarioTieneAporte()
        {
            return _consultasApoyo.ObtenerTipoUsuarioTieneAporte();
        }
        public IEnumerable<ClaseDeActivo> ObtenerClaseDeActivo()
        {
            return _consultasApoyo.ObtenerClaseDeActivo();
        }

        public IEnumerable<TipoTitulo> ObtenerTipoTitulo()
        {
            return _consultasApoyo.ObtenerTipoTitulo();
        }

        public IEnumerable<InformacionRelacionadaGrupoInteres> ObtenerInfoReporteGrupoDeInteres()
        {
            return _consultasApoyo.ObtenerInfoReporteGrupoDeInteres();
        }

        public IEnumerable<TipoTasa> ObtenerTipoTasa()
        {
            return _consultasApoyo.ObtenerTipoTasa();
        }



        public IEnumerable<TipoLineaCredito> ObtenerTipoLineaCredito(int idTipoRol = 0)
        {
            return _consultasApoyo.ObtenerTipoLineaCredito(idTipoRol);
        }

        public IEnumerable<TipoCredito> ObtenerTipoCredito(int? idLineaCredito = null) {
            return _consultasApoyo.ObtenerTipoCredito(idLineaCredito);
        }
        public IEnumerable<TipoModalidadContratacion> ObtenerTipoModalidadContratacion()
        {
            return _consultasApoyo.ObtenerTipoModalidadContratacion();
        }
        public IEnumerable<TipoProductoServicioContratado> ObtenerTipoProductoServicioContratado()
        {
            return _consultasApoyo.ObtenerTipoProductoServicioContratado();
        }
        public IEnumerable<ViewModelTiposCreditoTiposRol> ObtenerTiposCreditoTiposRolRelacion(short idTipoRol, int idTipoLineaCredito, DateTime? fechaAfiliacion, string tipoDocumento, string numeroDocumento, double tiempoAfiliacion, double salario)
        {
            return _consultasApoyo.ObtenerTiposCreditoTiposRol(idTipoRol, idTipoLineaCredito, fechaAfiliacion, tipoDocumento, numeroDocumento, tiempoAfiliacion, salario);
        }
        public IEnumerable<Etiquetas> ObtenerEtiquetas(int idTipoEtiqueta) {
            return _consultasApoyo.ObtenerEtiquetas(idTipoEtiqueta);
        }

        public IEnumerable<TiposDocumentoRequisito> ObtenerTiposDocumentoRequisito()
        {
            return _consultasApoyo.ObtenerTiposDocumentoRequisito();
        }

        public string ObtenerPlantillaHtml(short idPlantilla, List<Parametros> parametros)
        {
            return _consultasApoyo.ObtenerPlantillaHtml(idPlantilla, parametros);
        }

        public IEnumerable<AsegurabilidadOpcion> ObtenerAsegurabilidadOpciones(bool? noAsegurable)
        {
           return _consultasApoyo.ObtenerAsegurabilidadOpciones(noAsegurable);
        }

        public IEnumerable<Bimestre> ObtenerBimestre()
        {
            return _consultasApoyo.ObtenerBimestre();
        }
        public IEnumerable<TiposGarantiasPagares> ObtenerTiposGarantiasPagares()
        {
            return _consultasApoyo.ObtenerTiposGarantiasPagares();
        }
        

        public IEnumerable<SolicitudCreditoCausalesNegacion> ObtenerCausalesNegacion()
        {
            return _consultasApoyo.ObtenerCausalesNegacion();
        }

        public IEnumerable<UsosCredito> ObtenerUsosCredito(int idTipoLinea)
        {
            return _consultasApoyo.ObtenerUsosCredito(idTipoLinea);
        }

        public IEnumerable<TipoGarantiaCredito> ObtenerTiposGarantiasCredito(int? idTipoGarantiaCredito = null)
        {
            return _consultasApoyo.ObtenerTiposGarantiasCredito(idTipoGarantiaCredito);
        }

        public IEnumerable<VMConceptoGarantiaCredito> ObtenerTiposGarantiaTiposConceptoRelacion(int idPagare)
        {
            return _consultasApoyo.ObtenerTiposGarantiaTiposConceptoRelacion(idPagare);
        }

        public IEnumerable<TiposCreditoTiposGarantiaRelacion> ObtenerTiposCreditoTiposGarantiaRelacion(short idTipoCredito)
        {
            return _consultasApoyo.ObtenerTiposCreditoTiposGarantiaRelacion(idTipoCredito);
        }
        
        public int ObtenerCodeudorPorGarantia(string idTipoGarantia, string idTipoCodeudor, string valor)
        {
            return _consultasApoyo.ObtenerCodeudorPorGarantia(idTipoGarantia, idTipoCodeudor, valor);
        }

        public string ValidarUsuarioCavipetrol(string NumeroIdentificacion)
        {
            return _consultasApoyo.ValidarUsuarioCavipetrol(NumeroIdentificacion);
        }
        public IEnumerable<TiposConceptosAdjunto> ObtenerConceptosAdjuntos(string idConcepto)
        {
            return _consultasApoyo.ObtenerConceptosAdjuntos(idConcepto);
        }
        public IEnumerable<VMTipoConceptoAdjunto> ObtenerTiposConceptosTipoAdjunto(int idTipoAdjunto) => _consultasApoyo.ObtenerTiposConceptosTipoAdjunto(idTipoAdjunto);

        public IEnumerable<TiposContrato> ObtenerTiposContrato()
        {
            return _consultasApoyo.ObtenerTiposContrato();
        }
        public List<CiudadesServidor> ObtenerCiudadesServidor()
        {
            return _consultasApoyo.ObtenerCiudadesServidor().ToList();
        }
        public IEnumerable<Parametros> ObtenerParametros(string nombre)
        {
            return _consultasApoyo.ObtenerParametros(nombre).ToList();
        }
        public IEnumerable<Usuarios> ObtenerUsuariosPorPerfil(string perfil)
        {
            return _consultasApoyo.ObtenerUsuariosPorPerfil(perfil).ToList();
        }
        public string AsignarUsuarioSolicitud(int idSolicitud, int idUsuario)
        {
            return _consultasApoyo.AsignarUsuarioSolicitud(idSolicitud, idUsuario).ToString();
        }
        public IEnumerable<SolicitudDescripcionFondos> SolicitudDescripcionFondos()
        {
            return _consultasApoyo.SolicitudDescripcionFondos();
        }

        public ViewModelSegurosTasas ObtenerSegurosTasas(int edad, short IdTipoCredito)
        {
            return _consultasApoyo.ObtenerSegurosTasas(edad, IdTipoCredito);
        }

        public IEnumerable<ModelCartaSolicitudCredito> ObtenerCartaSolicitud(string IdSolicitud)
        {
            return _consultasApoyo.ObtenerCartaSolicitud(IdSolicitud);
        }

        public IEnumerable<Interproductos> ObtenerModelInterproductos(string IdSolicitud)
        {
            return _consultasApoyo.ObtenerModelInterproductos(IdSolicitud);
        }

        public List<CartaCondicionesCreditoModel> ObtenerDatosCartaCondiciones(int idSolicitud)
        {
            return _consultasApoyo.ObtenerDatosCartaCondiciones(idSolicitud);
        }
    }
}
