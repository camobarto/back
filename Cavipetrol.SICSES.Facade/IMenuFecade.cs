﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public interface IMenuFecade
    {
        List<ViewModelMenu> ObtenerMenu(List<Perfil> listaPerfiles);
        List<ViewModelMenu> MenuCompleto();
        bool ActualizarMenuPerfil(Cavipetrol.SICSES.Infraestructura.Model.Sicav.ListaMenuPerfil perfilMenuRelacion);

    }

}
