﻿using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System.Collections.Generic;
using System.Linq;
using System;
using Cavipetrol.SICSES.Infraestructura.Model.ReportesUIAF;

namespace Cavipetrol.SICSES.Facade
{
    public class ReportsUIAFFacade : IReportsUIAFFacade
    {
        private readonly ReportesUIAF reportesSICSES;
        public ReportsUIAFFacade()
        {
            reportesSICSES = new ReportesUIAF();
        }

        public RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivo>> ObtenerReporteTransaccionesEnEfectivo(int idReporte, string fechaInicio, string fechaFin)
        {
            return reportesSICSES.ObtenerReporteTransaccionesEnEfectivo(idReporte, fechaInicio, fechaFin);
        }

        public RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivoRiesgos>> ObtenerReporteTransaccionesEnEfectivoRiesgos(int idReporte, string fechaInicio, string fechaFin)
        {
            return reportesSICSES.ObtenerReporteTransaccionesEnEfectivoRiesgos(idReporte, fechaInicio, fechaFin);
        }

        public RespuestaReporte<IEnumerable<ReporteProductosEconomiaSolidaria>> ObtenerReporteProductosEconomiaSolidaria(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerReporteProductosEconomiaSolidaria(idReporte, mes, ano);
        }
    }
}
