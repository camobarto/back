﻿using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Sicav;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public interface IAsociadosFacade
    {
        RespuestaNegocio<string> GuardarReferencias(List<ReferenciasAsociado> referenciasAsociado);
        RespuestaNegocio<List<ViewModelReferenciasAsociado>> ObtenerReferencias(string tipo, string numero);
    }
}
