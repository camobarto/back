﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public interface IFuncionesFacade
    {
        bool EnviarSMS(DatosSMS datossms);

    }
}
