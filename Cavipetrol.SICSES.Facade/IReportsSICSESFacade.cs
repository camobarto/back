﻿using Cavipetrol.SICSES.Infraestructura;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public interface IReportsSICSESFacade
    {

        bool ActulizarParametrosCavipetrol(ParametroUsuarioCavipetrol registroParametrosCavipetrol);
        bool InsertarHistoricoReporte(ReporteHistorico reporteHistorico);
        IEnumerable<ParametroUsuarioCavipetrol> ObtenerParametrosRegistroCavipetrol();
        RespuestaReporte<IEnumerable<ReporteInformacionEstadistica>> ObtenerReporteInfromacionEstadisticas(int idReporte, int mes, int ano);
        RespuestaReporte<IEnumerable<ReporteOrganosDireccionyControl>> ObtenerReporteOrganosDirreccionControl(int idReporte, int mes, int ano);
        RespuestaReporte<IEnumerable<ReporteIndividualDeCarteraDeCredito>> ObtenerReporteindividualDeCarteraDeCredito(int idReporte, int mes, int ano);
        RespuestaReporte<IEnumerable<ReporteUsuarios>>  ObtenerReporteUsuarios(int idReporte, int mes, int ano);
        RespuestaReporte<IEnumerable<ReporteRedOficinasYCorresponsalesNoBancarios>> ObtenerReporteRedOficinasYCorresponsalesNoBancarios(int idReporte, int mes, int ano);
        ReporteHistorico ConsultarHistorico(int idReporte, int mes, int ano);
        RespuestaReporte<IEnumerable<ReporteParentescos>>   ObtenerInformeParentescos(int idReporte, int mes, int ano);

        RespuestaReporte<IEnumerable<ReporteRelacionBienesRecibosPago>> ObtenerRelacionBienesPagoRecibidos(int idReporte, int mes, int ano);

        RespuestaReporte<IEnumerable<ReporteInformeIndividualCaptaciones>> ObtenerInformeIndividualCaptacioness(int idReporte, int mes, int ano);

        RespuestaReporte<IEnumerable<ReportePUC>> ObtenerInformePUC(int idReporte, int mes, int ano, int periodo);

        RespuestaReporte<IEnumerable<ReporteAportesContribuciones>> ObtenerReporteAportesContribuciones(int idReporte, int mes, int ano);

        RespuestaReporte<IEnumerable<ReporteRelacionPropiedadesPlantaEquipo>> ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(int idReporte, int mes, int ano);

        RespuestaReporte<IEnumerable<ReporteRelacionInversiones>> ObtenerReporteInformeRelacionInversiones(int idReporte, int mes, int ano);

        RespuestaReporte<IEnumerable<ReporteProductosOfrecidos>> ObtenerReporte4Uiaf(int idReporte, int mes, int ano);

        RespuestaReporte<IEnumerable<ClasificacionExcedentes>> ObtenerAplicacionExcedentes(int idReporte, int mes, int ano);

        RespuestaReporte<IEnumerable<ReporteEducacionFormal>> ObtenerEducacionFormal(int idReporte, int mes, int ano);

        RespuestaReporte<IEnumerable<ReporteCreditosBancosFinancieros>> ObtenerCreditosBancariosyFinan(int idReporte, int mes, int ano);

        RespuestaReporte<IEnumerable<ReporteProcesosJudiciales>> ObtenerProcesosJudiciales(int idReporte, int mes, int ano);        
        RespuestaReporte<IEnumerable<T>> ObtenerHistoricoInformesSICSES<T>(int idReporte, int mes, int ano);

        RespuestaReporte<IEnumerable<ReporteRetiroeIngresoAsociados>> ObtenerInformeRetiroeIngresoAsociados(int idReporte, int mes, int ano);
        RespuestaReporte<IEnumerable<ReporteIndividualCarteraCredito>> ObtenerInformacionCarteraCredito(string anho, string mes);
        RespuestaReporte<IEnumerable<ReporteDetalleInformacionEstadistica>> ObtenerReporteDetalleInformacionEstadistica(int mes, int ano);
    }
}
