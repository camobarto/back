﻿using Cavipetrol.SICSES.Infraestructura;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.Model.ReportesUIAF;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public interface IReportsUIAFFacade
    {
        RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivo>> ObtenerReporteTransaccionesEnEfectivo(int idReporte, string fechaInicio, string fechaFin);
        RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivoRiesgos>> ObtenerReporteTransaccionesEnEfectivoRiesgos(int idReporte, string fechaInicio, string fechaFin);
        RespuestaReporte<IEnumerable<ReporteProductosEconomiaSolidaria>> ObtenerReporteProductosEconomiaSolidaria(int idReporte, int mes, int ano);


    }
}
