﻿using System;
using System.Collections.Generic;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;

namespace Cavipetrol.SICSES.Facade
{
	public interface IFodesFacade
	{
		IEnumerable<PorcentajesTasas> ObtenerTasas(string nomPorcentaje);
		void CrearNuevaTasa(string nomPorcentaje, string porcemtaje);
		IEnumerable<FomentoEmpresarial> ObtenerFomentoEmresarial();
		bool MarcarCreditoFodes(string documentoTipo, string documentoNumero, int numeroUnico, string tipoDocumento, string numeroDocumento, string descripcion);
		IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMarcados();
		bool DesmarcarCreditoFodes(List<FodesGenerico> listaDesmarcacion);
		IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMora();
		IEnumerable<CausalesFodes> ObtenerCausalesFodes();
		IEnumerable<InfoFodes> InformacionFodes();
	}
}
