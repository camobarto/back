﻿using Cavipetrol.SICSES.BLL.Controllers;
using System.Collections.Generic;
using Cavipetrol.SICSES.Infraestructura.Model.fyc.Riesgos;

namespace Cavipetrol.SICSES.Facade
{
    public class RiesgosFacade : IRiesgosFacade
    {
        Riesgos _ConsultasRiesgos = new Riesgos();
        public IEnumerable<SolicitudOperacion> ObtenerOperacion(string TipoIdentificacion, string NumeroIdentificacion, string NumeroOperacion)
        {
            return _ConsultasRiesgos.ObtenerOperacion(TipoIdentificacion, NumeroIdentificacion, NumeroOperacion);
        }
        public void GuardarDatosPersonaTransaccionEfectivo(int Operacion, string Tipoidentificacion, string Numeroidentificacion, string Primernombre,
                                                        string Segundonombre, string Primerapellido, string Segundoapellido, string Direccion, string Telefono,
                                                        string Consulta, string Tipopersona)
        {
            _ConsultasRiesgos.GuardarDatosPersonaTransaccionEfectivo(Operacion, Tipoidentificacion, Numeroidentificacion, Primernombre, Segundonombre, Primerapellido,
                                                                    Segundoapellido, Direccion, Telefono, Consulta, Tipopersona);
        }
        public void GuardarDatosOrigenFondos(int Operacion, int Opcion, string Consulta, string Detalle)
        {
            _ConsultasRiesgos.GuardarDatosOrigenFondos(Operacion, Opcion, Consulta, Detalle); ;
        }
    }
}
