﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using System;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Facade
{
    public interface ISolicitudCreditoFacade
    {
        RespuestaNegocio<InformacionSolicitante> ObtenerInformacionEmpleado(string numeroIdentificacion, string tipoIdentificacion);
        RespuestaNegocio<InformacionSolicitanteCampanaEspecial> ObtenerInformacionSolicitanteCampanaEspecial(string numeroIdentificacion, string tipoIdentificacion);
        RespuestaNegocio<InformacionAsegurabilidad> ObtenerInformacionAsegurabilidad(string numeroIdentificacion, string tipoIdentificacion);
        IEnumerable<SaldosColocacion> ObtenerCreditosDisponiblesSaldo(string numeroIdentificacion, string tipoIdentificacion, string papel);
        RespuestaNegocio<ValidacionesSolicitudCreditoProducto> ValidacionesReestriccionesCreditos(string numeroIdentificacion, string tipoIdentificacion, string papel, string producto);
        IEnumerable<NormaProductos> ObtenerNormas(string producto, string papel, short idTipoCredito);
        Modalidades ObtenerModalidades(string producto, string norma);
        //CapacidadPago ObtenerCapacidadPago(string numeroIdentificacion, string tipoIdentificacion, string tipoCredito);
        RespuestaNegocio<FormCapacidadPagoGruposAmortizacion> CalcularCapacidadPago(List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, string idTipoCreditoTipoRolRelacion, ref List<Amortizacion> tablaAmortizacion, double valorInmueble, short idTipoPoliza, double valorNegociado, ref List<TiposSeguro> tablaSeguros, byte? edad, double PorcenExtraPrima, string tipoCapacidadPago);
        RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago> GuardarCapacidadPago(Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago);
        RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> GuardarSolicitudCredito(Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito solicitudCredito,
                                                                                                      Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago,
                                                                                                      Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras horasExtras,
                                                                                                      List<FormaPago> listaFormaPago,
                                                                                                      List<FormCapacidadPagoCodeudoresGrupos> listaFormCapacidadPagoCodeudoresGrupos,
                                                                                                      List<FormCapacidadPagoTerceroCodeudor> listaFormCapacidadPagoTerceroCodeudor);
        RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras> GuardarCapacidadPagoHorasExtras(Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras horasExtras);
        RespuestaNegocio<IEnumerable<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras>> ConsultarCapacidadPagoHorasExtras(int idCapacidadPago = 0);
        RespuestaNegocio<object> GuardarAsociado(GuardarInformacionSolicitante guardarInformacionSolicitante);
        RespuestaNegocio<Cartera> ObtenerCartera(string idTipoIdentificacion, string numeroDocumento);
        RespuestaNegocio<IEnumerable<ViewModelSolicitudCredito>> ObtenerSolicitudCredito(string numeroIdentificacion, string idTipoIdentificacion, string numeroSolicitud, int? idTipoCredito, string idEstadoSolicitudCredito, int? idPerfil, string Usuario);
        RespuestaNegocio<Infraestructura.Model.Sicav.SolicitudCredito> CambiarEstadoSolicitud(int idSolicitud);
        RespuestaNegocio<ViewModelSolicitudCreditoDetalle> ObtenerSolicitudCreditoDetalle(int idSolicitud);
        RespuestaNegocio<SICSES.Infraestructura.Model.Sicav.SolicitudCredito> ActuaizarSolicitudCredito(List<Parametros> causalesNegacion, int idSolicitud, short idEstadoSolicitudCredito, string observaciones, string UsuRegistra, int IdMenu, int IdProceso, Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago);
        IEnumerable<Garantia> ObtenerInformacionGarantia(string NumSolicitud, string TipoDoc, string NumeroDoc);
        RespuestaNegocio<CuposAsegurabilidad> ObtenerCupoAsegurabilidad(int edad);
        RespuestaNegocio<IEnumerable<FormaPago>> ObtenerFormasPago(string idTipoCreditoTipoRolRelacion, bool mesada14);
        RespuestaNegocio<IEnumerable<ViewModelConsolidadoAtribuciones>> ObtenerConsolidadoAtribuciones(string idTipoIdentificacion, string numeroIdentificacionAsociado, int? idPerfil, int? valorcredito);

        RespuestaNegocio<VMSaldoAsegurabilidad> ObtenerSaldoAseurabilidad(string numeroIdentificacion);

        RespuestaValidacion ValidarCodeudor(string NumeroDocumento);
        IEnumerable<TipoGarantiaPorProducto> ObtenerTipoGarantiaPorProducto(string Producto);

        RespuestaNegocio<SolicitudCreditoIntentosLog> GuardarSolicitudCreditoIntentoLog(SolicitudCreditoIntentosLog model);

        RespuestaNegocio<Familiar> GuardarFamiliar(Familiar informacionFamiliar);
        RespuestaNegocio<List<Amortizacion>> GenerarTablaAmortizacion(int idLinea,
                                                                      int idTipoCreditoTipoRolRelacion,
                                                                      int periodos,
                                                                      double tasaNominal,
                                                                      double tasaEA,
                                                                      double valorCredito,
                                                                      double? valorTotalInmueble,
                                                                      short idTipoPoliza,
                                                                      double porcentajeExtraprima,
                                                                      short idPapelCavipetrol,
                                                                      byte edad,
                                                                      int idFormaPago,
                                                                    int gracia,
                                                                    int idTipoGarantia);

        IEnumerable<FormaPagoCredito> ObtenerFormaDePagoSolicitud(string idSolicitud);
        List<CapacidadPagoCodeudores> ObtenerCoincidenciaCodeudorCreditos(string numeroIdentificacion, string tipoIdentificacion);
        List<CapacidadPagoCodeudores> ObtenerSolicitudCreditoDetalleCodeudores(int idSolicitud);
        List<FormaPago> ObtenerFormaPagoPorNorma(string norma);

        //RespuestaNegocio<CapacidadPagoTerceroCodeudor> GuardarCapacidadPagoTerceroCodeudor(string TipoIdentificacion, string NumeroIdentificacion, string Nombre, string FechaNacimiento, string FormCapacidad);


        int guardarSolicitudCreditoFyc(SolicitudCreditoFyc solicitudCreditoFyc);
        RespuestaNegocio<object> guardarPagareGarantia(SolicitudCreditoPagare solicitudCreditoPagare);
        double ObtenerCupoMaximoCredito(string TipoIdentificacion, string NumeroIdentificacion);
        int generarNumeroConsecutivoPagare(SolicitudCreditoPagare solicitudCreditoPagare);
        RespuestaValidacion ObtenerSolicitudCreditoAdjunto(int idSolicitud, int idTipoAdjunto);
        SolicitudCreditoAdjunto GuardarSolicitudCreditoAdjunto(SolicitudCreditoAdjunto solicitudCreditoAdjunto);
        RespuestaNegocio<System.Object> ObtenerHipotecaSolicitudCredito(int idSolicitud);
        bool GuardarSolicitudCreditoHipoteca(SolicitudCreditoHipoteca solicitudCreditoHipoteca);
        RespuestaNegocio<object> GenerarHipotecaGuardarMinuta(SolicitudMinutaFYC solicitudMinutaFYC);
        Aportes ObtenerAportes(string tipoDocumento, string numeroDocumento, short IdTipoRol);
        IEnumerable<SolicitudesCredito> ObtenerSolicitudes(int numeroSolicitud, string tipoDocumento, int numeroDocumento, int tipoCredito);
        string ObtenerCodeudores(int Identificacion);
        IEnumerable<AsegurabilidadOpcion> ObtenerOpcionesAsegurabilidad(bool IdAsegurable, string idCredito, string idPapel);
        string GuardarJubilado(DetalleAsociado asociado);
        bool ConfirmarMesada14(string Cedula);
        int ValidarITP(string cedula, int bandera);
        int ValidarEmbargo(string cedula, int bandera);
        int ValidarExisExtraprima(string cedula, string tipoIdentificacion, int bandera);
        IEnumerable<ExtraPrima> TraeExtraprima(string cedula, string tipoIdentificacion, int bandera);
        void actualizarEstado(string NumeroSolicitud, string Estado);
        IEnumerable<ArbolAmortizacion> ObtenerArbolAmortizacion(string idtipocredito, string idpapelcavipetrol);
        void GuardarSolicitudInterproductos(List<InformacionProductosCreditos> datos);
    }

}
