﻿using Cavipetrol.SICSES.Infraestructura.Model.Administracion;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Administracion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public interface  IAdministracionFacade
    {
        IEnumerable<LineaCredito> ObtenerLineaCredito();

        IEnumerable<ViewTiposCreditos> ObtenerTiposCredito();

        string GuardarLineaCredito(int id,string nombre, string Descripcion,int bandera);
        string AdministrarTiposCredito(int id, string nombre, int idLinea, int bandera);

        string EliminarLineaCredito(int id);

        string EliminarTiposCredito(int id);

        IEnumerable<ViewPapelCavipetrol> ObtenerPapelCavipetrol();

        string AdministrarPapelCavipetrol(int id, string nombre, string Descripcion, int idcontrato, int bandera);

        string EliminarPapelCavipetrol(int id);

        IEnumerable<ViewTipoCreditoTipoRelacion> ObtenerTipoCreditoTipoRolRelacion();

        string AdministrarTipoCreditoTipoRol(TipoCreditoTipoRol datos);
    }
}
