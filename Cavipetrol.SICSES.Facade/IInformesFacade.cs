﻿using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public interface IInformesFacade
    {
        IEnumerable<RetencionProveedores> ObtenerInformacionGarantia(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "");
        IEnumerable<RetencionProveedores_ICA> ObtenerInformacionGarantia2(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "");
        IEnumerable<RetencionProveedores_IVA> ObtenerInformacionGarantia3(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "");

        bool InsertarDatosGarantias(List<DatosIva> datosExcel);
        bool ActualizaInfoProveedores(int bandera);
        IEnumerable<ProveedoresFaltantes> GeneraProveedoresFaltantes(int bandera);
        IEnumerable<CuentaContable> ObtenerCuentaContable(int bandera);
        IEnumerable<AuxTercero> ConsultaAuxTercero(string mes, string anio, string cuenta);
        IEnumerable<AuditaCuenta> ConsultaAuditaCuenta(string Fc_Ini, string Fc_Fin, string Cuenta);
        IEnumerable<GMF_BUno> ConsutaGMF_BUno(string Fc_Ini, string Fc_Fin, int Bandera);
        IEnumerable<GMF_BDos> ConsutaGMF_BDos(string Fc_Ini, string Fc_Fin, int Bandera);
        IEnumerable<GMF_BTres> ConsutaGMF_BTres(string Fc_Ini, string Fc_Fin, int Bandera);
        IEnumerable<SaldoInicialCarteraEcopetrol> ObtenerSaldoInicialInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera);
        IEnumerable<EncabezadoCarteraEcopetrol> ObtenerEncabezadoInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera);

        IEnumerable<PlanPagosCarteraEcopetrol> ObtenerPlanPagosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera);

        IEnumerable<MovimientosCarteraEcopetrol> ObtenerMovimientosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera);

        IEnumerable<TipoDocumentoPorCedula> ObtenerTipoDocumentoPorCedula(string TipoIdentificacion, int Cedula);

        IEnumerable<FacturaElectronica> ObtenerFacturaElectronica(string Factura, string Servidor);
        RespuestaNegocio<string> GuardarNota(Nota nota);
        IEnumerable<Certificaciones_Ingresos_Retenciones> Certificaciones(string NumeroIdentificacio, int ano, int mes);
    }
}
