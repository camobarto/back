﻿using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public class FuncionesFacade : IFuncionesFacade
    {
        private readonly Funciones _consultasApoyo;
        public FuncionesFacade()
        {
            _consultasApoyo = new Funciones();
        }

        public bool EnviarSMS(DatosSMS DatosSms)
        {
            return _consultasApoyo.EnviarSMS(DatosSms);
        }

    }
}
