﻿using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Cavipetrol.SICSES.Facade
{
    public class ReportsSICSESFacade : IReportsSICSESFacade
    {
        private readonly ReportesSICSES reportesSICSES;
        public ReportsSICSESFacade()
        {
            reportesSICSES = new ReportesSICSES();
        }

        public ParametroUsuarioCavipetrol ObtenerReporteRegistro()
        {
            return reportesSICSES.ObtenerParametrosRegistroCavipetrol().FirstOrDefault();
        }

        public bool ActulizarParametrosCavipetrol(ParametroUsuarioCavipetrol parametroCavipetrol)
        {
            return reportesSICSES.ActulizarParametrosCavipetrol(parametroCavipetrol);
        }

        public bool InsertarHistoricoReporte(ReporteHistorico reporteHistorico)
        {
            return reportesSICSES.InsertarHistoricoReporte(reporteHistorico);
        }

        public RespuestaReporte<IEnumerable<ReporteOrganosDireccionyControl>> ObtenerReporteOrganosDirreccionControl(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerReporteOrganosDirreccionControl(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteIndividualDeCarteraDeCredito>> ObtenerReporteindividualDeCarteraDeCredito(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerReporteindividualDeCarteraDeCredito(idReporte, mes, ano);
        }
        public RespuestaReporte<IEnumerable<ReporteUsuarios>> ObtenerReporteUsuarios(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerReporteUsuarios(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteRedOficinasYCorresponsalesNoBancarios>> ObtenerReporteRedOficinasYCorresponsalesNoBancarios(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerReporteRedOficinasYCorresponsalesNoBancarios(idReporte, mes, ano);
        }

        public ReporteHistorico ConsultarHistorico(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ConsultarHistorico(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteParentescos>> ObtenerInformeParentescos(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerInformeParentescos(idReporte, mes, ano);
        }

        public IEnumerable<ParametroUsuarioCavipetrol> ObtenerParametrosRegistroCavipetrol()
        {
            return reportesSICSES.ObtenerParametrosRegistroCavipetrol();
        }

        public RespuestaReporte<IEnumerable<ReporteInformacionEstadistica>> ObtenerReporteInfromacionEstadisticas(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerReporteInfromacionEstadisticas(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteRelacionBienesRecibosPago>> ObtenerRelacionBienesPagoRecibidos(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerRelacionBienesPagoRecibidos(idReporte, mes, ano);
        }
        public RespuestaReporte<IEnumerable<ReporteIndividualCarteraCredito>> ObtenerInformacionCarteraCredito(string anho, string mes)
        {
            return reportesSICSES.ObtenerInformacionCarteraCredito(anho, mes);
        }
        public RespuestaReporte<IEnumerable<ReporteInformeIndividualCaptaciones>> ObtenerInformeIndividualCaptacioness(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerInformeIndividualCaptacioness(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReportePUC>> ObtenerInformePUC(int idReporte, int mes, int ano, int periodo)
        {
            return reportesSICSES.ObtenerInformePUC(idReporte, mes, ano, periodo);
        }

        public RespuestaReporte<IEnumerable<ReporteAportesContribuciones>> ObtenerReporteAportesContribuciones(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerReporteAportesContribuciones(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteRelacionPropiedadesPlantaEquipo>> ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteRelacionInversiones>> ObtenerReporteInformeRelacionInversiones(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerReporteInformeRelacionInversiones(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteProductosOfrecidos>> ObtenerReporte4Uiaf(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerReporte4Uiaf(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ClasificacionExcedentes>> ObtenerAplicacionExcedentes(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerAplicacionExcedentes(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteEducacionFormal>> ObtenerEducacionFormal(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerEducacionFormal(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteCreditosBancosFinancieros>> ObtenerCreditosBancariosyFinan(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerCreditosBancariosyFinan(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteProcesosJudiciales>> ObtenerProcesosJudiciales(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerProcesosJudiciales(idReporte, mes, ano);
        }
        public RespuestaReporte<IEnumerable<T>> ObtenerHistoricoInformesSICSES<T>(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerHistoricoInformesSICSES<T>(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteRetiroeIngresoAsociados>>ObtenerInformeRetiroeIngresoAsociados(int idReporte, int mes, int ano)
        {
            return reportesSICSES.ObtenerInformeRetiroeIngresoAsociados(idReporte, mes, ano);
        }       

        public RespuestaReporte<IEnumerable<ReporteDetalleInformacionEstadistica>> ObtenerReporteDetalleInformacionEstadistica(int mes, int ano)
        {
            return reportesSICSES.ObtenerReporteDetalleInformacionEstadistica(mes, ano);
        }
    }
}
