﻿using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model.Administracion;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Administracion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public class AdministracionFacade : IAdministracionFacade
    {
        private readonly Administracion _consultasApoyo;
        public AdministracionFacade()
        {
            _consultasApoyo = new Administracion();
        }

        public IEnumerable<LineaCredito> ObtenerLineaCredito()
        {
            return _consultasApoyo.ObtenerLineaCredito();
        }

        public string GuardarLineaCredito(int id,string nombre, string Descripcion,int bandera)
        {
            return _consultasApoyo.GuardarLineaCredito(id,nombre, Descripcion, bandera);
        }
        public string EliminarLineaCredito(int id)
        {
            return _consultasApoyo.EliminarLineaCredito(id);
        }

        public string EliminarTiposCredito(int id)
        {
            return _consultasApoyo.EliminarTiposCredito(id);
        }

        public IEnumerable<ViewTiposCreditos> ObtenerTiposCredito()
        {
            return _consultasApoyo.ObtenerTiposCredito();
        }
        public string AdministrarTiposCredito(int id, string nombre, int idLinea, int bandera)
        {
            return _consultasApoyo.AdministrarTiposCredito(id, nombre, idLinea, bandera);
        }
        public IEnumerable<ViewPapelCavipetrol> ObtenerPapelCavipetrol()
        {
            return _consultasApoyo.ObtenerPapelCavipetrol();
        }

        public string AdministrarPapelCavipetrol(int id, string nombre, string Descripcion, int idcontrato, int bandera)
        {
            return _consultasApoyo.AdministrarPapelCavipetrol(id, nombre, Descripcion, idcontrato, bandera);
        }
        public string EliminarPapelCavipetrol(int id)
        {
            return _consultasApoyo.EliminarPapelCavipetrol(id);
        }
        public IEnumerable<ViewTipoCreditoTipoRelacion> ObtenerTipoCreditoTipoRolRelacion()
        {
            return _consultasApoyo.ObtenerTipoCreditoTipoRolRelacion();
        }
        public string AdministrarTipoCreditoTipoRol(TipoCreditoTipoRol datos)
        {
            return _consultasApoyo.AdministrarTipoCreditoTipoRol(datos);
        }
    }
}
