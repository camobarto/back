﻿using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.Seguridad;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Facade
{
    public class SeguridadFacade : ISeguridadFacade
    {
        private readonly SeguridadNegocio _seguridadNegocio;
        public SeguridadFacade()
        {
            _seguridadNegocio = new SeguridadNegocio();
        }
        public UsuarioDTO ObtenerPerfilUsuario(string uniqueName)
        {
            //Data Transfer para adicionarle los perfiles a un usuario
            UsuarioDTO usuario = new UsuarioDTO();
            
            //Se consulta el usuario por su correo electrónico
            var user = _seguridadNegocio.ObtenerPerfilUsuario(uniqueName);
            //Se consultan los perfiles del usuario
            var perfilesUsuario = _seguridadNegocio.ObtenerPerfilesUsuario(user.IdUsuario);

            usuario.Usuario = user;
            usuario.Perfiles = perfilesUsuario;

            return usuario;
        }

        public void GuardarUsuarioTemporal(string Usuario , string Contrasena)
        {
             _seguridadNegocio.GuardarUsuarioTemporal(Usuario, Contrasena);
        }

        public RespuestaValidacion ValidarInicioUsuario(string usuario)
        {
            return _seguridadNegocio.ValidarInicioUsuario(usuario);
        }

        public UsuarioLogin AdministrarInicioSesion(string usuario, string contrasena, string NuevaContrasena, int bandera)
        {
             return _seguridadNegocio.AdministrarInicioSesion(usuario, contrasena, NuevaContrasena, bandera);
        }
        public List<Usuario> ObtenerUsuario()
        {
            return _seguridadNegocio.ObtenerUsuario();
        }
        public List<Perfil> ObtenerPerfil()
        {
            return _seguridadNegocio.ObtenerPerfil();
        }
        public Usuario GuardarUsuario(Usuario usuario)
        {
            return _seguridadNegocio.GuardarUsuario(usuario);
        }
        public Perfil GuardarPerfil(Perfil perfil)
        {
            return _seguridadNegocio.GuardarPerfil(perfil);
        }
        public List<Oficina> ObtenerOficinas()
        {
            return _seguridadNegocio.ObtenerOficinas();
        }
    }
}
