﻿using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model.Hipotecas;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Hipotecas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public class HipotecasFacade : IHipotecasFacade
    {

        private readonly Hipotecas _InformesFacade;
        public HipotecasFacade()
        {
            _InformesFacade = new Hipotecas();
        }

        public IEnumerable<Hipoteca> ObtenerHipotecas(string NumeroHipoteca, string TipoIdentificacion, string NumeroIdentificacion, string NumeroSolicitud)
        {
            return _InformesFacade.ObtenerHipotecas(NumeroHipoteca, TipoIdentificacion, NumeroIdentificacion, NumeroSolicitud);
        }

        public bool ActualizaHipotecas(int IdHipoteca, string AudUsuarioModificacion, string FechaEscrituracion, string NumeroEscritura, string ValorAvaluo, string VigenciaInicial, string VigenciaFinal, string NumeroNotaria = "", string Oficina = "")
        {
            return _InformesFacade.ActualizaHipotecas(IdHipoteca, AudUsuarioModificacion, FechaEscrituracion, NumeroEscritura,  ValorAvaluo,  VigenciaInicial,  VigenciaFinal, NumeroNotaria,Oficina);
        }
        public List<VMHipotecas> ObtenerHipotecasXCedula(string TipoIdentificacion, string NumeroIdentificacion, double ValorSolicitado) => _InformesFacade.ObtenerHipotecasXCedula(TipoIdentificacion, NumeroIdentificacion, ValorSolicitado);
    }
}
