﻿using System.Collections.Generic;
using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.BLL.Controllers.SolicitudCreditosValidaciones;
using System;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP;

namespace Cavipetrol.SICSES.Facade
{
    public class SolicitudCreditosFacade : ISolicitudCreditoFacade
    {
        private readonly SolicitudCreditoNegocio _solicitudCredito;
        public SolicitudCreditosFacade()
        {
            _solicitudCredito = new SolicitudCreditoNegocio();
        }
        public IEnumerable<SaldosColocacion> ObtenerCreditosDisponiblesSaldo(string numeroIdentificacion, string tipoIdentificacion, string papel)
        {
            return _solicitudCredito.ObtenerCreditosDisponiblesSaldo(numeroIdentificacion, tipoIdentificacion, papel);
        }
        public IEnumerable<NormaProductos> ObtenerNormas(string producto, string papel, short idTipoCredito)
        {
            var a   = _solicitudCredito.ObtenerNormas(producto, papel, idTipoCredito);
            return a;
        }

        public RespuestaNegocio<InformacionAsegurabilidad> ObtenerInformacionAsegurabilidad(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _solicitudCredito.ObtenerInformacionAsegurabilidad(numeroIdentificacion, tipoIdentificacion);
        }

        public RespuestaNegocio<InformacionSolicitante> ObtenerInformacionEmpleado(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _solicitudCredito.ObtenerInformacionEmpleado(numeroIdentificacion, tipoIdentificacion);
        }
       

        public RespuestaNegocio<InformacionSolicitanteCampanaEspecial> ObtenerInformacionSolicitanteCampanaEspecial(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _solicitudCredito.ObtenerInformacionSolicitanteCampanaEspecial(numeroIdentificacion, tipoIdentificacion);
        }

        public RespuestaNegocio<InformacionSolicitante> ObtenerDatosPersona(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _solicitudCredito.ObtenerInformacionEmpleado(numeroIdentificacion, tipoIdentificacion);
        }

        public RespuestaNegocio<ValidacionesSolicitudCreditoProducto> ValidacionesReestriccionesCreditos(string numeroIdentificacion, string tipoIdentificacion, string papel, string producto)
        {
            IReglasSolicitudCreditoProductos reglaSolicitudNegocioCreditos;
            switch (producto)
            {
                case "CavOrdVivEmplea": reglaSolicitudNegocioCreditos = new ReglaSolicitudCreditoViviendaOrdinario(); break;
                default: throw new Exception("Producto no encontrado");
            }

            return _solicitudCredito.ValidacionesReestriccionProductos(numeroIdentificacion, tipoIdentificacion, papel, reglaSolicitudNegocioCreditos);
        }

        public Modalidades ObtenerModalidades(string producto, string norma)
        {
            return _solicitudCredito.ObtenerModalidades(producto, norma);
        }

        //public CapacidadPago ObtenerCapacidadPago(string numeroIdentificacion, string tipoIdentificacion, string tipoCredito)
        //{
        //    return null; //_solicitudCredito.ObtenerCapacidadPago(numeroIdentificacion, numeroIdentificacion, tipoCredito);
        //}
        public RespuestaNegocio<FormCapacidadPagoGruposAmortizacion> CalcularCapacidadPago(List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, string idTipoCreditoTipoRolRelacion, ref List<Amortizacion> tablaAmortizacion, double valorInmueble, short idTipoPoliza, double valorNegociado, ref List<TiposSeguro> tablaSeguros, byte? edad, double PorcenExtraPrima, string tipoCapacidadPago)
        {
            return _solicitudCredito.CalcularCapacidadPago(listaFormCapacidadPagoGrupos, idTipoCreditoTipoRolRelacion, ref tablaAmortizacion, valorInmueble, idTipoPoliza, valorNegociado, (byte)(edad == null ? 0 : edad), PorcenExtraPrima, tipoCapacidadPago);
        }
        public RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago> GuardarCapacidadPago(Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago)
        {
            return _solicitudCredito.GuardarCapacidadPago(capacidadPago);
        }
        public RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> GuardarSolicitudCredito(Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito solicitudCredito,
                                                                                                      Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago,
                                                                                                      Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras horasExtras,
                                                                                                      List<FormaPago> listaFormaPago,
                                                                                                      List<FormCapacidadPagoCodeudoresGrupos> listaFormCapacidadPagoCodeudoresGrupos,
                                                                                                       List<FormCapacidadPagoTerceroCodeudor> listaFormCapacidadPagoTerceroCodeudor)
        {
            return _solicitudCredito.GuardarSolicitudCredito(solicitudCredito, capacidadPago, horasExtras, listaFormaPago, listaFormCapacidadPagoCodeudoresGrupos, listaFormCapacidadPagoTerceroCodeudor);
        }
        public RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras> GuardarCapacidadPagoHorasExtras(Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras horasExtras)
        {
            return _solicitudCredito.GuardarCapacidadPagoHorasExtras(horasExtras);
        }
        public RespuestaNegocio<IEnumerable<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras>> ConsultarCapacidadPagoHorasExtras(int idCapacidadPago = 0)
        {
            return _solicitudCredito.ConsultarCapacidadPagoHorasExtras(idCapacidadPago);
        }
        public RespuestaNegocio<object> GuardarAsociado(GuardarInformacionSolicitante guardarInformacionSolicitante)
        {
            return _solicitudCredito.GuardarAsociado(guardarInformacionSolicitante);
        }

        public RespuestaNegocio<Cartera> ObtenerCartera(string idTipoIdentificacion, string numeroDocumento)
        {
            return _solicitudCredito.ObtenerCartera(idTipoIdentificacion, numeroDocumento);
        }

        public RespuestaNegocio<IEnumerable<ViewModelSolicitudCredito>> ObtenerSolicitudCredito(string numeroIdentificacion, string idTipoIdentificacion, string numeroSolicitud, int? idTipoCredito, string idEstadoSolicitudCredito, int? idPerfil, string Usuario)
        {
            return _solicitudCredito.ObtenerSolicitudCredito(numeroIdentificacion, idTipoIdentificacion, numeroSolicitud, idTipoCredito, idEstadoSolicitudCredito, idPerfil, Usuario);
        }
        public RespuestaNegocio<Infraestructura.Model.Sicav.SolicitudCredito> CambiarEstadoSolicitud(int idSolicitud)
        {
            return _solicitudCredito.CambiarEstadoSolicitud(idSolicitud);
        }
        public RespuestaNegocio<ViewModelSolicitudCreditoDetalle> ObtenerSolicitudCreditoDetalle(int idSolicitud)
        {
            return _solicitudCredito.ObtenerSolicitudCreditoDetalle(idSolicitud);
        }
        public RespuestaNegocio<SICSES.Infraestructura.Model.Sicav.SolicitudCredito> ActuaizarSolicitudCredito(List<Parametros> causalesNegacion, int idSolicitud, short idEstadoSolicitudCredito, string observaciones, string UsuRegistra, int IdMenu, int IdProceso, Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago)
        {
            return _solicitudCredito.ActuaizarSolicitudCredio(causalesNegacion, idSolicitud, idEstadoSolicitudCredito, observaciones, UsuRegistra, IdMenu, IdProceso, capacidadPago);
        }
        public IEnumerable<Garantia> ObtenerInformacionGarantia(string NumSolicitud, string TipoDoc, string NumeroDoc)
        {
            return _solicitudCredito.ObtenerInformacionGarantia(NumSolicitud, TipoDoc, NumeroDoc);
        }

        public RespuestaNegocio<CuposAsegurabilidad> ObtenerCupoAsegurabilidad(int edad)
        {
            return _solicitudCredito.ObtenerCupoAsegurabilidad(edad);
        }
        public RespuestaNegocio<IEnumerable<FormaPago>> ObtenerFormasPago(string idTipoCreditoTipoRolRelacion, bool mesada14)
        {
            return _solicitudCredito.ObtenerFormasPagoFYC(idTipoCreditoTipoRolRelacion, mesada14);
        }
        public RespuestaNegocio<IEnumerable<ViewModelConsolidadoAtribuciones>> ObtenerConsolidadoAtribuciones(string idTipoIdentificacion, string numeroIdentificacionAsociado, int? idPerfil, int? valorcredito)
        {
            return _solicitudCredito.ObtenerConsolidadoAtribuciones(idTipoIdentificacion, numeroIdentificacionAsociado, idPerfil, valorcredito);
        }

        public RespuestaNegocio<VMSaldoAsegurabilidad> ObtenerSaldoAseurabilidad(string numeroIdentificacion)
        {
            return _solicitudCredito.ObtenerSaldoAsegurabilidad(numeroIdentificacion);
        }
        public RespuestaValidacion ValidarCodeudor(string numeroIdentificacion)
        {
            return _solicitudCredito.ValidarCodeudor(numeroIdentificacion);
        }
        public IEnumerable<TipoGarantiaPorProducto> ObtenerTipoGarantiaPorProducto(string Producto)
        {
            return _solicitudCredito.ObtenerTipoGarantiaPorProducto(Producto);
        }
        public RespuestaNegocio<Familiar> GuardarFamiliar(Familiar informacionFamiliar)
        {
            return _solicitudCredito.GuardarFamiliar(informacionFamiliar);
        }

        public RespuestaNegocio<SolicitudCreditoIntentosLog> GuardarSolicitudCreditoIntentoLog(SolicitudCreditoIntentosLog modelo)
        {
            return _solicitudCredito.GuardarSolicitudCreditoIntentoLog(modelo);
        }

        public RespuestaNegocio<List<Amortizacion>> GenerarTablaAmortizacion(int idLinea, int idTipoCreditoTipoRolRelacion, int periodos, double tasaNominal, double tasaEA, double valorCredito, double? valorTotalInmueble, short idTipoPoliza, double porcentajeExtraprima, short idPapelCavipetrol, byte edad, int idFormaPago, int gracia, int idTipoGarantia)
        {
            return _solicitudCredito.GenerarTablaAmortizacion(idLinea,
                                                                idTipoCreditoTipoRolRelacion,
                                                                periodos,
                                                                tasaNominal,
                                                                tasaEA,
                                                                valorCredito,
                                                                valorTotalInmueble,
                                                                idTipoPoliza,
                                                                porcentajeExtraprima,
                                                                idPapelCavipetrol,
                                                                edad,
                                                                idFormaPago,
                                                                gracia,
                                                                idTipoGarantia);
        }

        public IEnumerable<FormaPagoCredito> ObtenerFormaDePagoSolicitud(string idSolicitud)
        {
            return _solicitudCredito.ObtenerFormaDePagoSolicitud(idSolicitud);
        }

        public List<CapacidadPagoCodeudores> ObtenerCoincidenciaCodeudorCreditos(string numeroIdentificacion, string tipoIdentificacion)
        {
            return _solicitudCredito.ObtenerCoincidenciaCodeudorCreditos(numeroIdentificacion, tipoIdentificacion);
        }
        public List<CapacidadPagoCodeudores> ObtenerSolicitudCreditoDetalleCodeudores(int idSolicitud)
        {
            return _solicitudCredito.ObtenerSolicitudCreditoDetalleCodeudores(idSolicitud);
        }
        public double ObtenerCupoMaximoCredito(string TipoIdentificacion, string NumeroIdentificacion)
        {
            return _solicitudCredito.ObtenerCupoMaximoCredito(TipoIdentificacion, NumeroIdentificacion);
        }

        public List<FormaPago> ObtenerFormaPagoPorNorma(string norma)
        {
            return _solicitudCredito.ObtenerFormaPagoPorNorma(norma);
        }

        public int guardarSolicitudCreditoFyc(SolicitudCreditoFyc solicitudCreditoFyc)
        {
            return _solicitudCredito.guardarSolicitudCreditoFyc(solicitudCreditoFyc);
        }

        public RespuestaNegocio<object> guardarPagareGarantia(SolicitudCreditoPagare solicitudCreditoPagare)
        {
            return _solicitudCredito.guardarPagareGarantia(solicitudCreditoPagare);
        }

        public int generarNumeroConsecutivoPagare(SolicitudCreditoPagare solicitudCreditoPagare)
        {
            return _solicitudCredito.generarNumeroConsecutivoPagare(solicitudCreditoPagare);
        }

        public RespuestaValidacion ObtenerSolicitudCreditoAdjunto(int idSolicitud, int idTipoAdjunto) => _solicitudCredito.ObtenerSolicitudCreditoAdjunto(idSolicitud, idTipoAdjunto);
        public SolicitudCreditoAdjunto GuardarSolicitudCreditoAdjunto(SolicitudCreditoAdjunto solicitudCreditoAdjunto) => _solicitudCredito.GuardarSolicitudCreditoAdjunto(solicitudCreditoAdjunto);
        public RespuestaNegocio<Object> ObtenerHipotecaSolicitudCredito(int idSolicitud) => _solicitudCredito.ObtenerHipotecaSolicitudCredito(idSolicitud);
        public bool GuardarSolicitudCreditoHipoteca(SolicitudCreditoHipoteca solicitudCreditoHipoteca) => _solicitudCredito.GuardarSolicitudCreditoHipoteca(solicitudCreditoHipoteca);
        public RespuestaNegocio<object> GenerarHipotecaGuardarMinuta(SolicitudMinutaFYC solicitudMinutaFYC)
        {
            return _solicitudCredito.GenerarHipotecaGuardarMinuta(solicitudMinutaFYC);
        }
        public Aportes ObtenerAportes(string numeroIdentificacion, string tipoIdentificacion, short IdTipoRol)
        {
            return _solicitudCredito.ObtenerAportes(numeroIdentificacion, tipoIdentificacion, IdTipoRol);
        }

        public IEnumerable<SolicitudesCredito> ObtenerSolicitudes(int numeroSolicitud, string tipoDocumento, int numeroDocumento, int tipoCredito)
        {
            return _solicitudCredito.ObtenerSolicitudes(numeroSolicitud, tipoDocumento, numeroDocumento, tipoCredito);
        }

        public string ObtenerCodeudores(int Identificacion)
        {
            return _solicitudCredito.ObtenerCodeudores(Identificacion);
        }

        public IEnumerable<AsegurabilidadOpcion> ObtenerOpcionesAsegurabilidad(bool IdAsegurable, string idCredito, string idPapel)
        {
            return _solicitudCredito.ObtenerOpcionesAsegurabilidad(IdAsegurable, idCredito, idPapel);
        }
        public string GuardarJubilado(DetalleAsociado asociado)
        {
            return _solicitudCredito.GuardarJubilado(asociado);
        }

        public bool ConfirmarMesada14(string Cedula)
        {
            return _solicitudCredito.ConfirmarMesada14(Cedula);
        }

        public int ValidarITP(string cedula, int bandera)
        {
            return _solicitudCredito.ValidarITP(cedula, bandera);
        }

        public int ValidarEmbargo(string cedula, int bandera)
        {
            return _solicitudCredito.ValidarEmbargo(cedula, bandera);
        }

        public int ValidarExisExtraprima(string cedula, string tipoIdentificacion, int bandera)
        {
            return _solicitudCredito.ValidarExisExtraprima(cedula, tipoIdentificacion, bandera);
        }

        public IEnumerable<ExtraPrima> TraeExtraprima(string cedula, string tipoIdentificacion, int bandera)
        {
            return _solicitudCredito.TraeExtraprima(cedula, tipoIdentificacion, bandera);
        }

        public void actualizarEstado(string NumeroSolicitud, string Estado)
        {         
                _solicitudCredito.actualizarEstado(NumeroSolicitud, Estado);          
        }

        public IEnumerable<ArbolAmortizacion> ObtenerArbolAmortizacion(string idtipocredito, string idpapelcavipetrol)
        {
            return _solicitudCredito.ObtenerArbolAmortizacion(idtipocredito, idpapelcavipetrol);
        }

        public virtual void GuardarSolicitudInterproductos(List<InformacionProductosCreditos> datos)
        {
            _solicitudCredito.GuardarSolicitudInterproductos(datos);
        }
    }
}
