﻿using Cavipetrol.SICSES.Infraestructura.Model.Hipotecas;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Hipotecas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public interface IHipotecasFacade
    {
        IEnumerable<Hipoteca> ObtenerHipotecas(string NumeroHipoteca, string TipoIdentificacion, string NumeroIdentificacion, string NumeroSolicitud);
        bool ActualizaHipotecas(int IdHipoteca, string AudUsuarioModificacion, string FechaEscrituracion, string NumeroEscritura, string ValorAvaluo, string VigenciaInicial, string VigenciaFinal, string NumeroNotaria , string Oficina);
        List<VMHipotecas> ObtenerHipotecasXCedula(string TipoIdentificacion, string NumeroIdentificacion, double ValorSolicitado);
    }
}
