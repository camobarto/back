﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.Seguridad;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Facade
{
    public interface ISeguridadFacade
    {
        UsuarioDTO ObtenerPerfilUsuario(string uniqueName);

        void GuardarUsuarioTemporal(string Usuario, string Contrasena);

        RespuestaValidacion ValidarInicioUsuario(string usuario);

        UsuarioLogin AdministrarInicioSesion(string usuario, string contrasena, string NuevaContrasena, int bandera);

        List<Usuario> ObtenerUsuario();
        List<Perfil> ObtenerPerfil();

        Usuario GuardarUsuario(Usuario usuario);
        Perfil GuardarPerfil(Perfil perfil);
        List<Oficina> ObtenerOficinas();
    }
}
