﻿using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Facade
{
    public class InformesFacade : IInformesFacade
    {
        private readonly Informes _InformesFacade;
        public InformesFacade()
        {
            _InformesFacade = new Informes();
        }

        public IEnumerable<RetencionProveedores> ObtenerInformacionGarantia(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "")
        {
            return _InformesFacade.ObtenerInformacionGarantia(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta);
        }
        public IEnumerable<RetencionProveedores_ICA> ObtenerInformacionGarantia2(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "")
        {
            return _InformesFacade.ObtenerInformacionGarantia2(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta, Bimestre);
        }
        public IEnumerable<RetencionProveedores_IVA> ObtenerInformacionGarantia3(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "")
        {
            return _InformesFacade.ObtenerInformacionGarantia3(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta, Bimestre);
        }
        public bool InsertarDatosGarantias(List<DatosIva> DatosGarantia)
        {
            return _InformesFacade.InsertarDatosGarantias(DatosGarantia);
        }
        public bool ActualizaInfoProveedores(int bandera)
        {
            return _InformesFacade.ActualizaInfoProveedores(bandera);
        }
        public IEnumerable<ProveedoresFaltantes> GeneraProveedoresFaltantes(int bandera)
        {
            return _InformesFacade.GeneraProveedoresFaltantes(bandera);
        }
        public IEnumerable<CuentaContable> ObtenerCuentaContable(int bandera)
        {
            return _InformesFacade.ObtenerCuentaContable(bandera);
        }
        public IEnumerable<AuxTercero> ConsultaAuxTercero(string mes, string anio, string cuenta)
        {
            return _InformesFacade.ConsultaAuxTercero(mes, anio, cuenta);
        }
        public IEnumerable<AuditaCuenta> ConsultaAuditaCuenta(string Fc_Ini, string Fc_Fin, string Cuenta)
        {
            return _InformesFacade.ConsultaAuditaCuenta(Fc_Ini, Fc_Fin, Cuenta);
        }

        public IEnumerable<GMF_BUno> ConsutaGMF_BUno(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            return _InformesFacade.ConsutaGMF_BUno(Fc_Ini, Fc_Fin, Bandera);
        }
        public IEnumerable<GMF_BDos> ConsutaGMF_BDos(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            return _InformesFacade.ConsutaGMF_BDos(Fc_Ini, Fc_Fin, Bandera);
        }
        public IEnumerable<GMF_BTres> ConsutaGMF_BTres(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            return _InformesFacade.ConsutaGMF_BTres(Fc_Ini, Fc_Fin, Bandera);
        }
        public IEnumerable<SaldoInicialCarteraEcopetrol> ObtenerSaldoInicialInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta,int Bandera)
        {
            return _InformesFacade.ObtenerSaldoInicialInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }
        public IEnumerable<EncabezadoCarteraEcopetrol> ObtenerEncabezadoInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _InformesFacade.ObtenerEncabezadoInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }
        public IEnumerable<PlanPagosCarteraEcopetrol> ObtenerPlanPagosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _InformesFacade.ObtenerPlanPagosInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }
        public IEnumerable<MovimientosCarteraEcopetrol> ObtenerMovimientosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _InformesFacade.ObtenerMovimientosInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }

        public IEnumerable<TipoDocumentoPorCedula> ObtenerTipoDocumentoPorCedula(string TipoIdentificacion, int Cedula)
        {
            return _InformesFacade.ObtenerTipoDocumentoPorCedula(TipoIdentificacion, Cedula);
        }
        public IEnumerable<FacturaElectronica> ObtenerFacturaElectronica(string Factura, string Servidor)
        {
            return _InformesFacade.ObtenerFacturaElectronica(Factura, Servidor);
        }

        public RespuestaNegocio<string> GuardarNota(Nota nota)
        {
            return _InformesFacade.GenerarFactura(nota);
        }

        public IEnumerable<Certificaciones_Ingresos_Retenciones> Certificaciones(string NumeroIdentificacio, int ano, int mes)
        {
            return _InformesFacade.Certificaciones(NumeroIdentificacio, ano, mes);
        }
    }
}
