﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Menu;
using System;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Facade
{
    public interface IConsultasApoyoSICSESFacade
    {
        IEnumerable<Municipio> ObtenerMunicipiosPorIdDepartamento(string idDepartamento);
        IEnumerable<Departamento> ObtenerDepartamentos();
        IEnumerable<TipoContrato> ObtenerTiposContratos();
        IEnumerable<TipoEstadoCivil> ObtenerTiposEstadosCiviles();
        IEnumerable<TipoEstrato> ObtenerTiposEstratos();
        IEnumerable<TipoIdentificacion> ObtenerTiposIdentificaciones();
        IEnumerable<TipoJornadaLaboral> ObtenerTiposJornadasLaborales();
        IEnumerable<TipoNivelEscolaridad> ObtenerNivelesEscolaridad();
        IEnumerable<TipoOcupacion> ObtenerTiposOcupacion();
        IEnumerable<TipoOrganoDirectivoControl> ObtenerTiposOrganosDirectivosControl();
        IEnumerable<TipoRol> ObtenerTiposRoles();
        IEnumerable<TipoSectorEconomico> ObtenerTiposSectorEconomico();
        IEnumerable<AsociacionEntidadSolidaria> ObtenerAsociacionesEntidadesSolidarias();
        IEnumerable<FormatoVigenteSICSES> ObtenerFormatosVigentesSICSES();

        IEnumerable<TipoEntidad> ObtenerEntidades();

        IEnumerable<ClasificacionCIIU> ObtenerClasificacionCUU();

        IEnumerable<Meses> ObtenerMeses();
        IEnumerable<Anos> ObtenerAnos();

        IEnumerable<ClasificacionExcedentes> ObtenerInfoExcedentes();

        IEnumerable<FormCapacidadPago> ObtenerFormCapacidadPago(int? idLineaCredito = null);
        IEnumerable<ViewModelFormCapacidadPagoGrupos> ObtenerFormCapacidadPagoGrupos(int? idTipoCreditoTipoRolRelacion = null);

        IEnumerable<EvaluacionRiesgoLiquidez> ObtenerInfoEvaluacionRiesgoLiquidez();

        IEnumerable<TiposTitulos> ObtenerTiposTitulos();

        IEnumerable<TipoObligacion> ObtenerTipoOligacion();

        IEnumerable<TipoCuota> ObtenerTipoCuota();

        IEnumerable<ClaseGarantia> ObtenerClaseGarantia();
        
        IEnumerable<DestinoCredito> ObtenerDestinoCredito();
        IEnumerable<ClaseVivienda> ObtenerClaseVivienda();
        IEnumerable<CategoriaReestructurado> ObtenerCategoriaRestructurado();

        IEnumerable<TipoEntidadBeneficiaria> ObtenerTipoEntidadBeneficiaria();

        IEnumerable<TipoModalidad> ObtenerTipoModalidad();

        IEnumerable<TipoDestino> ObtenerTipoDestino();
        IEnumerable<NivelEducacion> ObtenerNivelEducacion();

        IEnumerable<TipoBeneficiarios> ObtenerTiposBeneficiarios();

        IEnumerable<AlternativasDecreto2880> ObtenerAlternativasDecreto2880();

        IEnumerable<ClaseNaturaleza> ObtenerClaseNaturaleza();
        IEnumerable<ClaseEstadoProcesoActual> ObtenerClaseEstadoProcesoActual();
        IEnumerable<ClaseConcepto> ObtenerClaseConcepto();
        IEnumerable<TipoUsuarioTieneAporte> ObtenerTipoUsuarioTieneAporte();
        IEnumerable<ClaseDeActivo> ObtenerClaseDeActivo();

        IEnumerable<InformacionRelacionadaGrupoInteres> ObtenerInfoReporteGrupoDeInteres();

        IEnumerable<TipoLineaCredito> ObtenerTipoLineaCredito(int idTipoRol = 0);
        

        IEnumerable<TipoTitulo> ObtenerTipoTitulo();

        IEnumerable<TipoTasa> ObtenerTipoTasa();
        IEnumerable<TipoCredito> ObtenerTipoCredito(int? idLineaCredito = null);
        
        IEnumerable<TipoModalidadContratacion> ObtenerTipoModalidadContratacion();
        IEnumerable<TipoProductoServicioContratado> ObtenerTipoProductoServicioContratado();

        IEnumerable<ViewModelTiposCreditoTiposRol> ObtenerTiposCreditoTiposRolRelacion(short idTipoRol, int idTipoLineaCredito, DateTime? fechaAfiliacion, string tipoDocumento, string numeroDocumento, double tiempoAfiliacion, double salario);
        IEnumerable<Etiquetas> ObtenerEtiquetas(int idTipoEtiqueta);
        IEnumerable<TiposDocumentoRequisito> ObtenerTiposDocumentoRequisito();
        string ObtenerPlantillaHtml(short idPlantilla, List<Parametros> parametros);
        IEnumerable<AsegurabilidadOpcion> ObtenerAsegurabilidadOpciones(bool? noAsegurable);
        IEnumerable<Bimestre> ObtenerBimestre();
        IEnumerable<TiposGarantiasPagares> ObtenerTiposGarantiasPagares();

        IEnumerable<SolicitudCreditoCausalesNegacion> ObtenerCausalesNegacion();
        IEnumerable<UsosCredito> ObtenerUsosCredito(int idTipoLinea);

        IEnumerable<TipoGarantiaCredito> ObtenerTiposGarantiasCredito(int? idTipoGarantiaCredito = null);
        IEnumerable<VMConceptoGarantiaCredito> ObtenerTiposGarantiaTiposConceptoRelacion(int idPagare);

        IEnumerable<TiposCreditoTiposGarantiaRelacion> ObtenerTiposCreditoTiposGarantiaRelacion(short idTipoCredito);
        IEnumerable<VMTipoConceptoAdjunto> ObtenerTiposConceptosTipoAdjunto(int idTipoAdjunto);

        int ObtenerCodeudorPorGarantia(string idTipoGarantia, string idTipoCodeudor, string valor);

        string ValidarUsuarioCavipetrol(string NumeroIdentificacion);

        IEnumerable<TiposConceptosAdjunto> ObtenerConceptosAdjuntos(string idConcepto);

        IEnumerable<TiposContrato> ObtenerTiposContrato();
        List<CiudadesServidor> ObtenerCiudadesServidor();
       IEnumerable<Parametros> ObtenerParametros(string nombre);

        IEnumerable<Usuarios> ObtenerUsuariosPorPerfil(string perfil);

        string AsignarUsuarioSolicitud(int idSolicitud, int idUsuario);
        IEnumerable<SolicitudDescripcionFondos> SolicitudDescripcionFondos();
        ViewModelSegurosTasas ObtenerSegurosTasas(int edad, short IdTipoCredito);
        IEnumerable<ModelCartaSolicitudCredito> ObtenerCartaSolicitud(string IdSolicitud);
        IEnumerable<Interproductos> ObtenerModelInterproductos(string IdSolicitud);
        List<CartaCondicionesCreditoModel> ObtenerDatosCartaCondiciones(int idSolicitud);
    }
}
