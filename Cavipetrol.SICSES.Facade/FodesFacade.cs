﻿using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Facade
{
	public class FodesFacade : IFodesFacade
	{
		Fodes _fodes = new Fodes();
		ConsultasApoyo _consultasApoyo = new ConsultasApoyo();

		public void CrearNuevaTasa(string nomPorcentaje, string porcemtaje)
		{
			_fodes.CrearNuevaTasa(nomPorcentaje, porcemtaje);
		}
		public IEnumerable<PorcentajesTasas> ObtenerTasas(string nomPorcentaje)
		{
			return _fodes.ObtenerTasas(nomPorcentaje);
		}
		public IEnumerable<FomentoEmpresarial> ObtenerFomentoEmresarial()
		{
			return _fodes.ObtenerFomentoEmresarial();
		}

		public bool MarcarCreditoFodes(string documentoTipo, string documentoNumero, int numeroUnico, string tipoDocumento, string numeroDocumento, string descripcion)
		{
			_fodes.MarcarCreditoFodes(documentoTipo, documentoNumero, numeroUnico, tipoDocumento, numeroDocumento, descripcion);
			return true;
		}

		public IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMarcados()
		{
			return _fodes.ObtenerCreditosFomentoEmresarialMarcados();
		}

		public bool DesmarcarCreditoFodes(List<FodesGenerico> listaDesmarcacion)
		{
			_fodes.DesmarcarCreditoFodes(listaDesmarcacion);
			return true;
		}

		public IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMora()
		{
			return _fodes.ObtenerCreditosFomentoEmresarialMora();
		}

		public IEnumerable<CausalesFodes> ObtenerCausalesFodes()
		{
			return _fodes.ObtenerCausalesFodes();
		}

		public IEnumerable<InfoFodes> InformacionFodes()
		{
			return _fodes.InformacionFodes();
		}
	}
}
