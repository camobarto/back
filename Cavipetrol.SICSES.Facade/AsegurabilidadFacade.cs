﻿using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Facade
{
    public class AsegurabilidadFacade :   IAsegurabilidadFacade
    {
        private readonly Asegurabilidad _consultasApoyo;
        public AsegurabilidadFacade()
        {
            _consultasApoyo = new Asegurabilidad();
        }

        public bool InsertarDatosAsegurabilidad(List<AsegurabilidadModel> DatosAsegurabilidad)
        {
            return _consultasApoyo.InsertarDatosAsegurabilidad(DatosAsegurabilidad);
        }

        public bool InsertarDatosPolizasExternas(List<PolizasExternas> DatosPolizasExternas)
        {
            return _consultasApoyo.InsertarDatosPolizasExternas(DatosPolizasExternas);
        }

        public bool AdministrarPolizasExternas(ViewModelPolizasExternas DatosPolizasExternas)
        {
            return _consultasApoyo.AdministrarPolizasExternas(DatosPolizasExternas);
        }

        public bool ConsultarHistorico(string NumeroDocumento, string NumeroPoliza, string ProductoAmparado, string DocumentoProducto)
        {
            return _consultasApoyo.ConsultarHistorico(NumeroDocumento,  NumeroPoliza,  ProductoAmparado,  DocumentoProducto);
        }


        public IEnumerable<ViewModelPolizasExternas> ConsultarPolizasExternas(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza, int DiasVencimiento, string EstadoPoliza)
        {
            return _consultasApoyo.ConsultarPolizasExternas(TipoIdentificacion, NumeroDocumento, Nombres, NumeroPoliza, DiasVencimiento , EstadoPoliza);
        }

        public IEnumerable<Aseguradoras> ObtenerAseguradoras()
        {
            return _consultasApoyo.ObtenerAseguradoras();
        }

        public IEnumerable<TipoPoliza> ObtenerTipoPoliza()
        {
            return _consultasApoyo.ObtenerTipoPoliza();
        }

        public bool EliminarPolizaExterna(string TipoIdentificacion, string NumeroIdentificacion, string Aseguradora, string NumeroPoliza)
        {
            return _consultasApoyo.EliminarPolizaExterna(TipoIdentificacion, NumeroIdentificacion, Aseguradora, NumeroPoliza);
        }


        public IEnumerable<HistoricoPolizasExternas> ConsultarHistoricoPoliza(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza)
        {
            return _consultasApoyo.ConsultarHistoricoPoliza(TipoIdentificacion, NumeroDocumento, Nombres, NumeroPoliza);
        }

       public RespuestaValidacion ValidarPolizasExternas(string aseguradora, string tipopoliza, string producto, string documento, string nit)
        {
            return _consultasApoyo.ValidarPolizasExternas(aseguradora, tipopoliza, producto, documento,nit);
        }
        public RespuestaNegocio<IEnumerable<VMPolizasVigentes>> ObtenerPolizasVigentes(string tipo, string numero)
        {
            return _consultasApoyo.ObtenerPolizasVigentes(tipo, numero);
        }

        public IEnumerable<Infraestructura.Model.ProductoPorAsociado> ObtenerProductoPorAsociado(string Cedula)
        {
            return _consultasApoyo.ObtenerProductoPorAsociado(Cedula);
        }
    }
}
