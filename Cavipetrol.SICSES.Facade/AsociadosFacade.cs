﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Sicav;

namespace Cavipetrol.SICSES.Facade
{
    public class AsociadosFacade : IAsociadosFacade
    {
        private readonly Asociados _asociados;

        public AsociadosFacade()
        {
            _asociados = new Asociados();
        }
        public RespuestaNegocio<string> GuardarReferencias(List<ReferenciasAsociado> referenciasAsociado)
        {
            return _asociados.GuardarReferenciasAsociado(referenciasAsociado);
        }

        public RespuestaNegocio<List<ViewModelReferenciasAsociado>> ObtenerReferencias(string tipo, string numero)
        {
            return _asociados.ObtenerReferencias(tipo, numero);
        }
    }
}
