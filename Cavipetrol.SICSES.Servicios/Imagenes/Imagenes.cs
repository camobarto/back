﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace Cavipetrol.SICSES.Servicios.Imagenes
{
    public static class Imagenes
    {
        public enum enumImagenes {
            LogoCavipetrol,
            CiudadesCavipetrol,
            CiudadesCavipetrolVertical
        }
        public static List<Parametros> ObtenerImagenIndividual(List<Parametros> lstParametros, short idPlantilla) {
            switch (idPlantilla) {
                case 1:
                    break;
                default:
                    foreach (enumImagenes enumImagenes in new enumImagenes[] { enumImagenes.LogoCavipetrol, enumImagenes.CiudadesCavipetrol, enumImagenes.CiudadesCavipetrolVertical })
                    {
                        string path = ObtenerRuta(enumImagenes);
                        string base64String = string.Empty;
                        using (Image imagen = Image.FromFile(path))
                        {
                            using (MemoryStream m = new MemoryStream())
                            {
                                imagen.Save(m, imagen.RawFormat);
                                byte[] imageBytes = m.ToArray();
                                base64String = Convert.ToBase64String(imageBytes);
                            }
                        }
                        if (enumImagenes.LogoCavipetrol == enumImagenes) {
                            lstParametros.Add(new Parametros { Nombre = "@ImagenLogoCavipetrol", Descripcion = base64String });
                        }
                        else if (enumImagenes.CiudadesCavipetrol == enumImagenes) {
                            lstParametros.Add(new Parametros { Nombre = "@ImagenCiudadesCavipetrol", Descripcion = base64String });
                        }
                        else if (enumImagenes.CiudadesCavipetrolVertical == enumImagenes)
                        {
                            lstParametros.Add(new Parametros { Nombre = "@ImagenCiudadesCavipetrolV", Descripcion = base64String });
                        }
                    }
                    break;
            }
            return lstParametros;
        }
        public static string ObtenerImagen(string plantilla, enumImagenes[] ListaEnumImagenes)
        {
            foreach (enumImagenes enumImagenes in ListaEnumImagenes.Distinct()) {
                string path = ObtenerRuta(enumImagenes);
                string base64String = string.Empty;
                using (Image imagen = Image.FromFile(path))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        imagen.Save(m, imagen.RawFormat);
                        byte[] imageBytes = m.ToArray();
                        base64String = Convert.ToBase64String(imageBytes);
                    }
                }
                plantilla = ColocarImagen(plantilla, enumImagenes, base64String);
            }
            return plantilla;
        }

        private static string ColocarImagen(string plantilla, enumImagenes enumImagenes , string base64String)
        {
            switch (enumImagenes)
            {
                case enumImagenes.LogoCavipetrol:
                    return plantilla.Replace("@ImagenLogoCavipetrol", base64String); 
                case enumImagenes.CiudadesCavipetrol:
                    return plantilla.Replace("@ImagenCiudadesCavipetrol", base64String);
                case enumImagenes.CiudadesCavipetrolVertical:
                    return plantilla.Replace("@ImagenCiudadesCavipetrolV", base64String);
                default:
                    return plantilla;
            }
           
        }
        private static string ObtenerRuta(enumImagenes enumImagenes)
        {
            switch (enumImagenes)
            {
                case enumImagenes.LogoCavipetrol:
                    return System.Web.HttpContext.Current.Server.MapPath("~") + "Imagenes\\LogoCavipetro.jpg";
                case enumImagenes.CiudadesCavipetrol:
                    return System.Web.HttpContext.Current.Server.MapPath("~") + "Imagenes\\CiudadesCavipetrol.png";
                case enumImagenes.CiudadesCavipetrolVertical:
                    return System.Web.HttpContext.Current.Server.MapPath("~") + "Imagenes\\CiudadesCavipetrolVertical.png";
                default:
                    return string.Empty;                
            }
        }
    }
}