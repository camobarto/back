﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Net.Http;
using Cavipetrol.SICSES.Servicios.Sevicios;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System.Globalization;
using System.Threading;

[assembly: OwinStartup(typeof(Cavipetrol.SICSES.Servicios.Startup))]

namespace Cavipetrol.SICSES.Servicios
{
    public class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {

            var OAuthOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(480),
                Provider = new SimpleAuthorizationServerProvider()
                
            };

            app.UseOAuthBearerTokens(OAuthOptions);
            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);


        }

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
