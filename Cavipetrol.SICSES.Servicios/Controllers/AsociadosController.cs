﻿using Cavipetrol.SICSES.Facade;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.Utilidades;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Sicav;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Cavipetrol.SICSES.BLL.Controllers;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Cavipetrol.SICSES.Servicios.Controllers
{


    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AsociadosController : ApiController
    {
        private readonly IAsociadosFacade _asociadosFacade;

        //CLASE PARA CIFRADO DE PARAMETROS
        private Cifrado.CifradoDescifrado _cifradoDesifrado;

        private static readonly log4net.ILog logger =
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";
        public AsociadosController()
        {
            _asociadosFacade = new AsociadosFacade();
            _cifradoDesifrado = new Cifrado.CifradoDescifrado();
            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }
        [HttpPost]
        [Route("api/Asociados/GuardarReferencias")]
        //public HttpResponseMessage GuardarReferencias(List<ReferenciasAsociado> referenciasAsociado)
        public HttpResponseMessage GuardarReferencias(ModeloString modelo)
        {
            HttpResponseMessage response;
            try
            {
                modelo.Valor = this._cifradoDesifrado.decryptString(modelo.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarReferencias(ModeloString: " + modelo.Valor + ")");

                List<ReferenciasAsociado> listReferenciasAsociado = JsonConvert.DeserializeObject<List<ReferenciasAsociado>>(modelo.Valor);

                RespuestaNegocio<string> respuestaNegocio = _asociadosFacade.GuardarReferencias(listReferenciasAsociado);

                string json = JsonConvert.SerializeObject(respuestaNegocio);

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
                //response = Request.CreateResponse(HttpStatusCode.OK, _asociadosFacade.GuardarReferencias(referenciasAsociado));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarReferencias() -> " + ex.Message);
            }

            return response;
        }
        [HttpGet]
        [Route("api/Asociados/ObtenerReferencias")]
        public HttpResponseMessage ObtenerReferencias(string idTipoIdentificacion, string numeroIdentificacion)
        {
            HttpResponseMessage response;
            try
            {
                numeroIdentificacion = _cifradoDesifrado.decryptString(numeroIdentificacion);
                idTipoIdentificacion = _cifradoDesifrado.decryptString(idTipoIdentificacion);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerReferencias(idTipoIdentificacion: " + idTipoIdentificacion + ", numeroIdentificacion: " + numeroIdentificacion + ")");

                RespuestaNegocio<List<ViewModelReferenciasAsociado>> respuestaNegocio = _asociadosFacade.ObtenerReferencias(idTipoIdentificacion, numeroIdentificacion);

                string json = JsonConvert.SerializeObject(respuestaNegocio);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));

                //response = Request.CreateResponse(HttpStatusCode.OK, _asociadosFacade.ObtenerReferencias(idTipoIdentificacion, numeroIdentificacion));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerReferencias() -> " + ex.Message);
            }

            return response;
        }

        [HttpGet]
        [Route("api/Asociados/ObtenerContratosPorAsociado")]
        public IHttpActionResult ObtenerContratosPorAsociado(string TipoIdentificacion, string NumeroIdentificacion)
        {   
            Asociados asociados = new Asociados();
            DateTime fechaFin = asociados.ConsultarFechaFinalizacionContratoPorEmpleado(_cifradoDesifrado.decryptString(TipoIdentificacion),
                _cifradoDesifrado.decryptString(NumeroIdentificacion));
            return Ok(JsonConvert.SerializeObject(fechaFin.ToString("MM/dd/yyyy")));
        }
    }
}
