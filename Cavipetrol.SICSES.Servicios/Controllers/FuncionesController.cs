﻿using Cavipetrol.SICSES.Facade;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Utilidades;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Cors;


namespace Cavipetrol.SICSES.Servicios.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FuncionesController : ApiController
    {
        private readonly IFuncionesFacade _funcionesFacade;

        //CLASE PARA CIFRADO DE PARAMETROS
        private Cifrado.CifradoDescifrado _cifradoDesifrado;

        private static readonly log4net.ILog logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";

        public FuncionesController()
        {
            _funcionesFacade = new FuncionesFacade();
            _cifradoDesifrado = new Cifrado.CifradoDescifrado();
            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }


        [HttpPost]
        [Route("api/Funciones/EnviarEmail")]
        public HttpResponseMessage EnviarCorreo(ModeloString modeloString) // DatosEmail
        {
            HttpResponseMessage response;
            DatosEmail dtoDatosEmail = new DatosEmail();
            try
            {
                //   byte[] bytes = Convert.FromBase64String(adjunto.adjunto);
                // MemoryStream Archivo = new MemoryStream(bytes);

                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio EnviarCorreo(ModeloString: " + modeloString.Valor + ")");

                dtoDatosEmail = JsonConvert.DeserializeObject<DatosEmail>(modeloString.Valor);

                MailMessage mail = new MailMessage("sicav@cavipetrol.com",(dtoDatosEmail.correo ?? "javier.diaz@cavipetrol.com"));
                SmtpClient client = new SmtpClient();
                client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = "128.51.3.115";
                mail.Subject = dtoDatosEmail.Asunto ?? "Asunto";
                mail.Body = dtoDatosEmail.Cuerpo ?? "Body";
                //mail.Attachments.Add(new Attachment(Archivo, "amortizacion.pdf", System.Net.Mime.MediaTypeNames.Application.Pdf));
                client.Send(mail);

                response = Request.CreateResponse(HttpStatusCode.OK, "Correo enviado correctamente");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error EnviarCorreo() -> " + ex.Message);
            }

            return response;
        }

        [HttpPost]
        [Route("api/Funciones/EnviarSMS")]
        public HttpResponseMessage EnviarSMS(ModeloString modeloString)
        {
            HttpResponseMessage response;
            DatosSMS dtoDatosSMS = new DatosSMS();
            try
            {    DateTime moment = DateTime.Now;

                int ano = moment.Year;
                int mes = moment.Month;               
                int dia = moment.Day;               
                int hora = moment.Hour;
                int minuto = moment.Minute;               
                int segundo = moment.Second;               
                int milisegundo = moment.Millisecond;

                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio EnviarSMS(ModeloString: " + modeloString.Valor + ")");

                dtoDatosSMS = JsonConvert.DeserializeObject<DatosSMS>(modeloString.Valor);

                dtoDatosSMS.LMOL_LLAVE = dtoDatosSMS.LMOL_AFINIT + ';' + ano + '.' + mes + '.' + dia + ';' + hora + ':' + minuto + ':' + segundo + ':' + milisegundo;

                bool success = _funcionesFacade.EnviarSMS(dtoDatosSMS);
                string json = JsonConvert.SerializeObject(success);
                response = Request.CreateResponse(HttpStatusCode.OK, json);

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error EnviarSMS() -> " + ex.Message);
            }

            return response;
        }

        [HttpPost]
        [Route("api/Funciones/EnviarSMStandard")]
        public HttpResponseMessage EnviarSMStandard(ModeloString modeloString)
        {
            HttpResponseMessage response;
            DatosSMS dtoDatosSMS = new DatosSMS();
            try
            {
                DateTime moment = DateTime.Now;

                int ano = moment.Year;
                int mes = moment.Month;
                int dia = moment.Day;
                int hora = moment.Hour;
                int minuto = moment.Minute;
                int segundo = moment.Second;
                int milisegundo = moment.Millisecond;

                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio EnviarSMS(ModeloString: " + modeloString.Valor + ")");

                dtoDatosSMS = JsonConvert.DeserializeObject<DatosSMS>(modeloString.Valor);

                dtoDatosSMS.LMOL_LLAVE = dtoDatosSMS.LMOL_AFINIT + ';' + ano + '.' + mes + '.' + dia + ';' + hora + ':' + minuto + ':' + segundo + ':' + milisegundo;

                bool success = _funcionesFacade.EnviarSMS(dtoDatosSMS);
                string json = JsonConvert.SerializeObject(success);
                response = Request.CreateResponse(HttpStatusCode.OK, json);

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error EnviarSMS() -> " + ex.Message);
            }

            return response;
        }

    }
}