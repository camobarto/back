﻿using Cavipetrol.SICSES.Facade;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SapController : ApiController
    {

        private readonly ISapFacade _SapFacade;
        private Cifrado.CifradoDescifrado _cifradoDescifrado;

        private static readonly log4net.ILog logger =
         log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";

      
        public SapController()
        {
            _SapFacade = new SapFacade();
            _cifradoDescifrado = new Cifrado.CifradoDescifrado();
            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }

        [HttpGet]
        [Route("api/Sap/ObtenerAsiSoporte")]
        public HttpResponseMessage ObtenerAsiSoporte()
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerAsiSoporte()");


                var DtaDefault = _SapFacade.ObtenerAsiSoporte();
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DtaDefault)));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAsiSoporte() -> " + ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("api/Sap/ConsultarAsientos")]
        public HttpResponseMessage ConsultarAsientos(string AsiSoporte, string AsiConsecutivo,string Asioperacion,string AsiOpeConsecutivo,string Fecha,string Ciudad,string MensajeError)
        {
            HttpResponseMessage response;
            try
            {
                AsiSoporte = _cifradoDescifrado.decryptString(AsiSoporte);
                AsiConsecutivo = _cifradoDescifrado.decryptString(AsiConsecutivo);
                Asioperacion = _cifradoDescifrado.decryptString(Asioperacion);
                AsiOpeConsecutivo = _cifradoDescifrado.decryptString(AsiOpeConsecutivo);
                Fecha = _cifradoDescifrado.decryptString(Fecha);
                Ciudad = _cifradoDescifrado.decryptString(Ciudad);
                MensajeError = _cifradoDescifrado.decryptString(MensajeError);


                var DtaDefault = _SapFacade.ConsultarAsientos(AsiSoporte, AsiConsecutivo, Asioperacion, AsiOpeConsecutivo, Fecha, Ciudad, MensajeError);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DtaDefault)));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAsiSoporte() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Sap/ActualizarAsientos")]
        public HttpResponseMessage ActualizarAsientos()
        {
            HttpResponseMessage response;
            try
            {
               

                var DtaDefault = _SapFacade.ActualizarAsiento();
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DtaDefault)));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAsiSoporte() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Sap/ConsultaProveedores")]
        public HttpResponseMessage ConsultaProveedores(string CardCode, string CardName)
        {
            HttpResponseMessage response;
            try
            {


                CardCode = _cifradoDescifrado.decryptString(CardCode);
                CardName = _cifradoDescifrado.decryptString(CardName);


                var DtaDefault = _SapFacade.ConsultaProveedores(CardCode, CardName);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DtaDefault)));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ConsultaProveedores() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Sap/ActualizarEstadoProveedores")]
        public HttpResponseMessage ActualizarEstadoProveedores()
        {
            HttpResponseMessage response;
            try
            {
                var DtaDefault = _SapFacade.ActualizarEstadoProveedores();
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DtaDefault)));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ConsultaProveedores() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Sap/ConsultarMensajeError")]
        public HttpResponseMessage ConsultarMensajeError()
        {
            HttpResponseMessage response;
            try
            {

                


                var DtaDefault = _SapFacade.ConsultarMensajeError();
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DtaDefault)));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ConsultaProveedores() -> " + ex.Message);
            }
            return response;
        }

    }




    }
 