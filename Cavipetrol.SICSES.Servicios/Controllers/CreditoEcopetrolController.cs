﻿using Cavipetrol.SICSES.Facade;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.Utilidades;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.BLL.Controllers.CreditoViviendaEcopetrol;
using Cavipetrol.SICSES.Servicios.Cifrado;

namespace Cavipetrol.SICSES.Servicios.Controllers
{

    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "*")]

    public class CreditoEcopetrolController : ApiController
    {
        CifradoDescifrado _cifradoDescifrado = new CifradoDescifrado();
        private static readonly log4net.ILog logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string usuario = "";

        //ENVIA LOS DATOS A LA BASE TEMPORAL PARA SU VALIDACION </jarp>
        [HttpPost]
        [Route("api/CreditoEcopetrol/ValidaGuardaDatosCrViviendaECPTmp")]
        public HttpResponseMessage GuardaDatosCartasCrViviendaECPTmp(ModeloCifrado StringCifrado)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            DatosDocumento JsonObject = new DatosDocumento();

            try
            {
                JsonObject = JsonConvert.DeserializeObject<DatosDocumento>(_cifradoDescifrado.decryptString(StringCifrado.Valor));
                List<DatosCartasCrViviendaEcopetrol> lstDatos = new List<DatosCartasCrViviendaEcopetrol>();
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ValidacionGuardadoDatos(modelosString.Valor: " + StringCifrado.Valor + ")");

                foreach (var item in JsonObject.DatosExcel)
                {
                    DatosCartasCrViviendaEcopetrol DatosExcel = new DatosCartasCrViviendaEcopetrol();
                    DatosExcel.Registro = item[0];
                    DatosExcel.Numero_De_Identificacion = item[1];
                    DatosExcel.Nombre = item[2];
                    DatosExcel.Linea_Prestamo = item[3];
                    DatosExcel.Linea_Credito_Cavipetrol = item[4];
                    DatosExcel.Fecha_Adjudicacion = item[5];
                    DatosExcel.Fecha_Vigencia = item[6];
                    DatosExcel.Tiempo_Amortizacion_Carta = Convert.ToInt32(item[7]);
                    DatosExcel.Monto_Autorizado = Convert.ToDecimal(item[8]);
                    DatosExcel.Tasa = item[9];

                    lstDatos.Add(DatosExcel);
                }
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ObjCreditoViviendaEcopetrol.GuardaDatosCartasCrViviendaECPTmp(lstDatos))));
            }

            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                List<string> MensajeDuplicado = new List<string>();
                MensajeDuplicado.Add("Error de Datos en el Archivo Excel Cargado, Tiene Datos Repetidos, Por Favor Verifique ");
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(MensajeDuplicado)));
            }
        }


        //VALIDA, OBTIENE ERRORES Y GUARDA LOS DATOS SI NO HAY ERRORES </jarp>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ValidaObtieneGuardaDatosCrViviendaECP")]
        public HttpResponseMessage ValidaObtieneGuardaDatosCartasCrViviendaECP()
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                var datosCartas = ObjCreditoViviendaEcopetrol.ValidaObtieneGuardaDatosCartasCrViviendaECP();
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(datosCartas)));
            }

            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                List<string> MensajeDuplicado = new List<string>();
                MensajeDuplicado.Add("Error de Datos en el Archivo Excel Cargado, Tiene Datos Repetidos, Por Favor Verifique ");
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(MensajeDuplicado)));
            }
            return response;
        }


        //OBTIENE LOS DATOS DE LAS CARTAS CON O SIN FILTRO DE FECHA DE CARGUE </jarp>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerDatosCartasECP")]
        public HttpResponseMessage ObtenerDatosCartasECP(string fechaCargue)
        {

            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                if (fechaCargue == null)
                {
                    fechaCargue = "";
                }
                
                var datosCartas = ObjCreditoViviendaEcopetrol.ObtenerDatosCartasECP(fechaCargue);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(datosCartas)));
            }

            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                List<string> MensajeDuplicado = new List<string>();
                MensajeDuplicado.Add("Error de Datos en el Archivo Excel Cargado, Tiene Datos Repetidos, Por Favor Verifique ");
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(MensajeDuplicado)));
            }
            return response;
        }


        //ENVIA LOS DATOS DE LAS CARTAS QUE SE ACTUALIZARAN A LA BASE TEMPORAL PARA SU VALIDACION </jarp>
        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarDatosCartasActualizadasCrViviendaECPTmp")]
        public HttpResponseMessage GuardarDatosCartasActualizadasCrViviendaECPTmp(ModeloCifrado StringCifrado)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            DatosDocumento JsonObject = new DatosDocumento();

            try
            {
                JsonObject = JsonConvert.DeserializeObject<DatosDocumento>(_cifradoDescifrado.decryptString(StringCifrado.Valor));
                List<DatosCartasActualizarCrViviendaEcopetrol> lstDatos = new List<DatosCartasActualizarCrViviendaEcopetrol>();
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ValidacionGuardadoDatos(modelosString.Valor: " + StringCifrado.Valor + ")");

                foreach (var item in JsonObject.DatosExcel)
                {
                    DatosCartasActualizarCrViviendaEcopetrol DatosExcel = new DatosCartasActualizarCrViviendaEcopetrol();
                    DatosExcel.Consecutivo = Convert.ToInt32(item[0]);
                    DatosExcel.Fecha_Fin_Prorroga = item[1];

                    lstDatos.Add(DatosExcel);
                }
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ObjCreditoViviendaEcopetrol.GuardarDatosCartasActualizadasCrViviendaECPTmp(lstDatos))));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                List<string> MensajeDuplicado = new List<string>();
                MensajeDuplicado.Add("Error de Datos en el Archivo Excel Cargado, Tiene Datos Repetidos, Por Favor Verifique ");
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(MensajeDuplicado)));
            }
        }

        //VALIDA, OBTIENE ERRORES Y ACTUALIZA LOS DATOS SI NO HAY ERRORES </jarp>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ValidaObtieneActualizaDatosCartasCrViviendaECP")]
        public HttpResponseMessage ValidaObtieneActualizaDatosCartasCrViviendaECP()
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                var datosCartas = ObjCreditoViviendaEcopetrol.ValidaObtieneActualizaDatosCartasCrViviendaECP();
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(datosCartas)));
            }

            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                List<string> MensajeDuplicado = new List<string>();
                MensajeDuplicado.Add("Error de Datos en el Archivo Excel Cargado, Tiene Datos Repetidos, Por Favor Verifique ");
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(MensajeDuplicado)));
            }
            return response;
        }

        /// <summary>
        /// Obtiene los usuos de credito
        /// </summary>
        /// <returns>Lista con los usos del credito</returns>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerUsosSolicitudCreditoECP")]
        public IHttpActionResult ObtenerUsosSolicitudCreditoECP()
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerUsosCreditoECP()");

                var Lista = ObjCreditoViviendaEcopetrol.ObtenerUsosSolicitudCreditoECP(); 
                string json = JsonConvert.SerializeObject(Lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerUsosCreditoECP() -> " + ex.Message);
            }
            return Ok();
        }

        /// <summary>
        /// Obtiene las formas de pago segun la norma
        /// </summary>
        /// <param name="norma">Norma que se adjudicó</param>
        ///// <returns>Lista con las formas de pago segun la norma adjudicada</returns>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerFormasPagoECP")]
        public IHttpActionResult ObtenerFormasPagoECP(string norma)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            norma = _cifradoDescifrado.decryptString(norma);
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerFormaPagoECP()");

                var Lista = ObjCreditoViviendaEcopetrol.ObtenerFormasPago(norma);
                string json = JsonConvert.SerializeObject(Lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormaPagoECP() -> " + ex.Message);
            }
            return Ok();
        }

        /// <summary>
        /// Obtiene los datos de la carta de adjudicación
        /// </summary>
        /// <param name="nIdentificacion">Número de identificacion</param>
        /// <returns>Los datos de la carta adjudicada</returns>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerDatosCartaECP")]
        public IHttpActionResult ObtenerDatosCartaECP(string nIdentificacion)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            nIdentificacion = _cifradoDescifrado.decryptString(nIdentificacion);
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerFormaPagoECP()");

                var Lista = ObjCreditoViviendaEcopetrol.ObtenerDatosCartaECP(nIdentificacion);
                string json = JsonConvert.SerializeObject(Lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormaPagoECP() -> " + ex.Message);
            }
            return Ok();
        }


        [HttpGet]
        [Route("api/CreditoEcopetrol/ValidarSolicitud")]
        public IHttpActionResult ValidarSolicitud(string IdConsecutivo)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            IdConsecutivo = _cifradoDescifrado.decryptString(IdConsecutivo);
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerFormaPagoECP()");

                var Lista = ObjCreditoViviendaEcopetrol.ValidarSolicitud(Int32.Parse(IdConsecutivo));
                string json = JsonConvert.SerializeObject(Lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormaPagoECP() -> " + ex.Message);
            }
            return Ok();
        }


        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarUsosCreditoECP")]
        public HttpResponseMessage GuardarUsosCreditoECP(ModeloString datosString)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {

                datosString.Valor = _cifradoDescifrado.decryptString(datosString.Valor);              
                
                List<DatosUsosCreditos> Usos = JsonConvert.DeserializeObject<List<DatosUsosCreditos>>(datosString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarSolicitudInterproductos (usuario: " + usuario + ", contrasena:" + usuario + ")");

                ObjCreditoViviendaEcopetrol.GuardarUsosCreditoECP(Usos);

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(usuario));

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarSolicitudInterproductos() -> " + ex.Message);
                throw;
            }
            return response;
        }


        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarFormasPagoECP")]
        public HttpResponseMessage GuardarFormasPagoECP(ModeloString datosString)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {

                datosString.Valor = _cifradoDescifrado.decryptString(datosString.Valor);

                List<DatosFormaPago> FormaPago = JsonConvert.DeserializeObject<List<DatosFormaPago>>(datosString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarSolicitudInterproductos (usuario: " + usuario + ", contrasena:" + usuario + ")");

                ObjCreditoViviendaEcopetrol.GuardarFormasPagoECP(FormaPago);

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(usuario));

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarSolicitudInterproductos() -> " + ex.Message);
                throw;
            }
            return response;
        }

        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarSolicitudCreditoISYNETECP")]
        public HttpResponseMessage GuardarSolicitudCreditoISYNETECP(ModeloString datosString)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {

                datosString.Valor = _cifradoDescifrado.decryptString(datosString.Valor);

                List<DatosSolicitudISYNET> SolicitudISYNETECP = JsonConvert.DeserializeObject<List<DatosSolicitudISYNET>>(datosString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarSolicitudInterproductos (usuario: " + usuario + ", contrasena:" + usuario + ")");

                var respuestaNegocio = ObjCreditoViviendaEcopetrol.GuardarSolicitudCreditoISYNETECP(SolicitudISYNETECP);

                string json = JsonConvert.SerializeObject(respuestaNegocio);                
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarSolicitudInterproductos() -> " + ex.Message);
                throw;
            }
            return response;
        }

        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerHipotecaECP")]
        public IHttpActionResult ObtenerHipotecaECP(string nIdentificacion, string IdConsecutivo)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            nIdentificacion = _cifradoDescifrado.decryptString(nIdentificacion);
            IdConsecutivo = _cifradoDescifrado.decryptString(IdConsecutivo);
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerFormaPagoECP()");

                var Lista = ObjCreditoViviendaEcopetrol.ObtenerHipotecaECP(nIdentificacion, IdConsecutivo);
                string json = JsonConvert.SerializeObject(Lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormaPagoECP() -> " + ex.Message);
            }
            return Ok();
        }
        /// <summary>
        /// devuelve el nuemro de solicitud para mostrar los botones de impresion de los formatos
        /// </summary>
        /// <param name="nIdentificacion"></param>
        /// <param name="IdConsecutivo"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ValidaNumSolicitudFYC")]
        public IHttpActionResult ValidaNumSolicitudFYC(string Consecutivo)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            Consecutivo = _cifradoDescifrado.decryptString(Consecutivo);
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerFormaPagoECP()");

                var Lista = ObjCreditoViviendaEcopetrol.ValidaNumSolicitudFYC(Consecutivo);
                string json = JsonConvert.SerializeObject(Lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormaPagoECP() -> " + ex.Message);
            }
            return Ok();
        }


        //CREA EL TITULO CORRESPONDIENTE A LA CARTA DE ECP VALIDADA POR VISTA </jarp>
        [HttpPost]
		[Route("api/CreditoEcopetrol/CrearEstudioTitulo")]
		public IHttpActionResult CrearEstudioTitulo(ModeloCifrado StringCifrado)
		{
			try
			{
				EstudioTitulos ObjEstudioTitulo = new EstudioTitulos();
				CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();

				ObjEstudioTitulo = JsonConvert.DeserializeObject<EstudioTitulos>(_cifradoDescifrado.decryptString(StringCifrado.Valor));
				
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposDocumentoRequisito()");

				ObjCreditoViviendaEcopetrol.CrearEstudioTitulo(ObjEstudioTitulo);
				
				return Ok(_cifradoDescifrado.encryptString("true"));
			}
			catch (Exception ex)
			{
				Debug.Print(ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposDocumentoRequisito() -> " + ex.Message);
			}
			return Ok();
		}

		[HttpGet]
		[Route("api/CreditoEcopetrol/ObtenerCartasECP")]
		public IHttpActionResult ConsultarCartas(string cedula, string estado)
		{
			CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
			try
			{
				int Cedula = 0;
				int Estado = 0;

				cedula = _cifradoDescifrado.decryptString(cedula);
				estado = _cifradoDescifrado.decryptString(estado);
				int.TryParse(cedula, out Cedula);
				int.TryParse(estado, out Estado);
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsultarCartas(cedula: " + cedula + "estadp:"+ estado + ")");

				var Lista = ObjCreditoViviendaEcopetrol.ObtenerCartasECP(Cedula, Estado);
				string json = JsonConvert.SerializeObject(Lista);
				return Ok(_cifradoDescifrado.encryptString(json));
			}
			catch (Exception ex)
			{
				Debug.Print(ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposDocumentoRequisito() -> " + ex.Message);
			}
			return Ok();
		}

		[HttpPost]
		[Route("api/CreditoEcopetrol/ActualizarTitulo")]
		public IHttpActionResult ActualizarEstudioTitulo(string consecutivo, string estado)
		{
			CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
			try
			{
				int Estado = 0;
				int Consecutivo = 0;
				
				consecutivo = _cifradoDescifrado.decryptString(consecutivo);
				estado = _cifradoDescifrado.decryptString(estado);
				int.TryParse(consecutivo, out Consecutivo);
				int.TryParse(estado, out Estado);

				logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposDocumentoRequisito()");

				ObjCreditoViviendaEcopetrol.ActualizarEstudioTitulo(Consecutivo, Estado);

				return Ok(_cifradoDescifrado.encryptString("true"));
			}
			catch (Exception ex)
			{
				Debug.Print(ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposDocumentoRequisito() -> " + ex.Message);
			}
			return Ok();
		}

		[HttpGet]
		[Route("api/CreditoEcopetrol/ObtenerInformacionEstudio")]
		public IHttpActionResult ObtenerInformacionEstudio(string concecutivo)
		{
			CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
			try
			{
				int Concecutivo = 0;

				concecutivo = _cifradoDescifrado.decryptString(concecutivo);
				int.TryParse(concecutivo, out Concecutivo);
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsultarCartas(concecutivo: " + concecutivo + ")");

				var Lista = ObjCreditoViviendaEcopetrol.ObtenerInformacionEstudio(Concecutivo);
				string json = JsonConvert.SerializeObject(Lista);
				return Ok(_cifradoDescifrado.encryptString(json));
			}
			catch (Exception ex)
			{
				Debug.Print(ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposDocumentoRequisito() -> " + ex.Message);
			}
			return Ok();
		}

        //OBTIENE LOS DATOS DE LA CARGA, FILTRANDO POR CEDULA y TIPO </jarp>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerDatosCargaAdjudicacion")]
        public HttpResponseMessage ObtenerDatosCargaAdjudicacion([FromUri] string numeroIdentificacion, [FromUri] string tipoIdentificacion)
        {

            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                string snumeroIdentificacion, stipoIdentificacion;
                snumeroIdentificacion = _cifradoDescifrado.decryptString(numeroIdentificacion);
                stipoIdentificacion = _cifradoDescifrado.decryptString(tipoIdentificacion);

                var datosCarga = ObjCreditoViviendaEcopetrol.ObtenerDatosCargaAdjudicacion(snumeroIdentificacion, stipoIdentificacion);
                int rows = datosCarga.Count();
                if (rows >= 1)
                {

                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(datosCarga)));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar");
                }

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error Consultando Datos Carga() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar");
            }
            return response;
        }

        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerTipoGestion")]
        public HttpResponseMessage ObtenerTipoGestion()
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {


                var convertidor = new ConvertirAListaSeleccionable();
                //var datosTipoGestion = ObjCreditoViviendaEcopetrol.ObtenerTipoGestion().ToList();
                var lista = ObjCreditoViviendaEcopetrol.ObtenerTipoGestion().ToList();
                int rows = lista.Count();
                if (rows >= 1)
                {

                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los tipos de gestion.");
                }

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error Consultando Tipos de Gestión() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los tipos de gestion");
            }
            return response;
        }

        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerMenu")]
        public HttpResponseMessage ObtenerMenu([FromUri] string numeroIdentificacion)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                var convertidor = new ConvertirAListaSeleccionable();
                numeroIdentificacion = _cifradoDescifrado.decryptString(numeroIdentificacion);
                //var datosTipoGestion = ObjCreditoViviendaEcopetrol.ObtenerTipoGestion().ToList();

                /*
                 Comodin 1 = Obtiene todos los menus cuyo proceso de gestion son 1 y no transversales
                 Comodin 2 = Obtiene el IdMenu filtrando por el nombre.
                 */

                var lista = ObjCreditoViviendaEcopetrol.ObtenerMenu(numeroIdentificacion, "1").ToList();
                int rows = lista.Count();
                if (rows >= 1)
                {

                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los menús.");
                }

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error Consultando Menus de ObtenerMenu() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los menús");
            }
            return response;
        }

        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerIdMenu")]
        public HttpResponseMessage ObtenerIdMenu([FromUri] string numeroIdentificacion)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {


                var convertidor = new ConvertirAListaSeleccionable();
                numeroIdentificacion = _cifradoDescifrado.decryptString(numeroIdentificacion);
                //var datosTipoGestion = ObjCreditoViviendaEcopetrol.ObtenerTipoGestion().ToList();

                /*
                Comodin 1 = Obtiene todos los menus cuyo proceso de gestion son 1 y no transversales
                Comodin 2 = Obtiene el IdMenu filtrando por el nombre.
                */
                var lista = ObjCreditoViviendaEcopetrol.ObtenerIdMenu(numeroIdentificacion, "2").ToList();
                int rows = lista.Count();
                if (rows >= 1)
                {

                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar el Id Menú.-ObtenerIdMenu()");
                }

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error Consultando IdMenú de ObtenerIdMenu() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los IdMenú");
            }
            return response;
        }



        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerDatosHistoricoGestion")]
        public HttpResponseMessage ObtenerDatosHistoricoGestion([FromUri] string numeroIdentificacion)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                numeroIdentificacion = _cifradoDescifrado.decryptString(numeroIdentificacion);
                //var datosTipoGestion = ObjCreditoViviendaEcopetrol.ObtenerTipoGestion().ToList();
                var lista = ObjCreditoViviendaEcopetrol.ObtenerDatosHistoricoGestion(numeroIdentificacion).ToList();
                int rows = lista.Count();
                if (rows >= 1)
                {

                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los tipos de gestion.");
                }

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error Consultando Tipos de Gestión() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los tipos de gestion");
            }
            return response;
        }

        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerDatosAsegurabilidad_REPORTES_SES")]
        public HttpResponseMessage ObtenerDatosAsegurabilidad_REPORTES_SES([FromUri] string numeroIdentificacion, [FromUri] string tipoIdentificacion)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                numeroIdentificacion = _cifradoDescifrado.decryptString(numeroIdentificacion);
                tipoIdentificacion = _cifradoDescifrado.decryptString(tipoIdentificacion);
                //var datosTipoGestion = ObjCreditoViviendaEcopetrol.ObtenerTipoGestion().ToList();
                var lista = ObjCreditoViviendaEcopetrol.ObtenerDatosAsegurabilidad_REPORTES_SES(numeroIdentificacion, tipoIdentificacion).ToList();
                int rows = lista.Count();
                if (rows >= 1)
                {

                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los tipos de gestion.");
                }

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error Consultando Tipos de Gestión() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los tipos de gestion");
            }
            return response;
        }

        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerTipoProducto")]
        public HttpResponseMessage ObtenerTipoProducto([FromUri] string Comodin, [FromUri] string Filtro)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                Comodin = _cifradoDescifrado.decryptString(Comodin);
                Filtro = _cifradoDescifrado.decryptString(Filtro);
                //var datosTipoGestion = ObjCreditoViviendaEcopetrol.ObtenerTipoGestion().ToList();
                var lista = ObjCreditoViviendaEcopetrol.ObtenerTipoProducto(Comodin, Filtro).ToList();
                int rows = lista.Count();
                if (rows >= 1)
                {

                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los tipos de gestion.");
                }

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error Consultando Tipos de Producto -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Error Consultando Tipos de Producto");
            }
            return response;
        }


        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerTipoNorma")]
        public HttpResponseMessage ObtenerTipoNorma([FromUri] string Comodin, [FromUri] string Filtro)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                Comodin = _cifradoDescifrado.decryptString(Comodin);
                Filtro = _cifradoDescifrado.decryptString(Filtro);
                //var datosTipoGestion = ObjCreditoViviendaEcopetrol.ObtenerTipoGestion().ToList();
                var lista = ObjCreditoViviendaEcopetrol.ObtenerTipoNorma(Comodin, Filtro).ToList();
                int rows = lista.Count();
                if (rows >= 1)
                {

                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los tipos de gestion.");
                }

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error Consultando Tipos de Producto -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Error Consultando Tipos de Producto");
            }
            return response;
        }

        //GUARDA EL NUEVO USO DE CREDITO </jarp>
        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarNuevoUso")]
        public HttpResponseMessage GuardarNuevoUso(ModeloString modeloString)
        {
            HttpResponseMessage response;
            UsosCreditosEcopetrolGuardar datos = new UsosCreditosEcopetrolGuardar();
            UsosCreditosEcopetrol ObjBll = new UsosCreditosEcopetrol();
            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                datos = JsonConvert.DeserializeObject<UsosCreditosEcopetrolGuardar>(modeloString.Valor);

                string json = JsonConvert.SerializeObject(ObjBll.GuardarNuevoUso(datos));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw;
            }
            return response;
        }

        //OBTIENE LOS USOS DE CREDITOS ECP </jarp>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerUsosCreditoECP")]
        public HttpResponseMessage ObtenerUsosCreditoECP()
        {
            UsosCreditosEcopetrol ObjBll = new UsosCreditosEcopetrol();
            HttpResponseMessage response;
            try
            {
                var datosUsos = ObjBll.ObtenerUsosCreditoECP();
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(datosUsos)));
            }

            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
            }
            return response;
        }

        //ACTUALIZA EL USO DE CREDITO </jarp>
        [HttpPost]
        [Route("api/CreditoEcopetrol/ActualizarUso")]
        public HttpResponseMessage ActualizarUso(ModeloString modeloString)
        {
            HttpResponseMessage response;
            UsosCreditosEcopetrolObtener datos = new UsosCreditosEcopetrolObtener();
            UsosCreditosEcopetrol ObjBll = new UsosCreditosEcopetrol();
            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                datos = JsonConvert.DeserializeObject<UsosCreditosEcopetrolObtener>(modeloString.Valor);

                string json = JsonConvert.SerializeObject(ObjBll.ActualizarUso(datos));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }

        /// <summary>
        /// OBTIENE LOS DOCUMENTOS DE CREDITOS ECP </jarp>
        /// </summary>
        /// <returns>Retorna los Documentos creados</returns>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerDocumentosCreditoECP")]
        public HttpResponseMessage ObtenerDocumentosCreditoECP()
        {
            DocumentosCreditosEcopetrol ObjBllDocumentos = new DocumentosCreditosEcopetrol();
            HttpResponseMessage response;
            try
            {
                var datosUsos = ObjBllDocumentos.ObtenerDocumentosCreditoECP();
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(datosUsos)));
            }

            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
            }
            return response;
        }

        //GUARDA EL NUEVO USO DE CREDITO </jarp>
        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarNuevoDocumento")]
        public HttpResponseMessage GuardarNuevoDocumento(ModeloString modeloString)
        {
            HttpResponseMessage response;
            DocumentosCreditosEcopetrolGuardar datosDocs = new DocumentosCreditosEcopetrolGuardar();
            DocumentosCreditosEcopetrol ObjBllDocumentos = new DocumentosCreditosEcopetrol();
            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                datosDocs = JsonConvert.DeserializeObject<DocumentosCreditosEcopetrolGuardar>(modeloString.Valor);

                string json = JsonConvert.SerializeObject(ObjBllDocumentos.GuardarNuevoDocumento(datosDocs));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw;
            }
            return response;
        }

        //ACTUALIZA EL USO DE CREDITO </jarp>
        [HttpPost]
        [Route("api/CreditoEcopetrol/ActualizarDocumento")]
        public HttpResponseMessage ActualizarDocumento(ModeloString modeloString)
        {
            HttpResponseMessage response;
            DocumentosCreditosEcopetrolObtener datos = new DocumentosCreditosEcopetrolObtener();
            DocumentosCreditosEcopetrol ObjBll = new DocumentosCreditosEcopetrol();
            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                datos = JsonConvert.DeserializeObject<DocumentosCreditosEcopetrolObtener>(modeloString.Valor);

                string json = JsonConvert.SerializeObject(ObjBll.ActualizarDocumento(datos));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }

        //GUARDA EL NUEVO USO DE CREDITO </jarp>
        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarRelacionUsoDocumento")]
        public HttpResponseMessage GuardarRelacionUsoDocumento(ModeloString modeloString)
        {
            HttpResponseMessage response;
            UsosDocumentosCreditosEcopoetrolGuardar datosUsosDocs = new UsosDocumentosCreditosEcopoetrolGuardar();
            DocumentosCreditosEcopetrol ObjBllDocumentos = new DocumentosCreditosEcopetrol();
            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                datosUsosDocs = JsonConvert.DeserializeObject<UsosDocumentosCreditosEcopoetrolGuardar>(modeloString.Valor);

                string json = JsonConvert.SerializeObject(ObjBllDocumentos.GuardarRelacionUsoDocumento(datosUsosDocs));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw;
            }
            return response;
        }
        /// <summary>
        /// Obtiene la relacion de los usos y documentos
        /// </summary>
        /// <returns>datos de las relaciones</returns>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerUsoDocumentoCreditoECP")]
        public HttpResponseMessage ObtenerUsoDocumentoCreditoECP()
        {
            DocumentosCreditosEcopetrol ObjBllDocumentos = new DocumentosCreditosEcopetrol();
            HttpResponseMessage response;
            try
            {
                var datosUsos = ObjBllDocumentos.ObtenerUsoDocumentoCreditoECP();
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(datosUsos)));
            }

            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
            }
            return response;
        }

        //ACTUALIZA EL ESTADO DEL USO - DOCUMENTO</jarp>
        [HttpPost]
        [Route("api/CreditoEcopetrol/ActualizarEstadoUsoDocumentoHabilitar")]
        public HttpResponseMessage ActualizarEstadoUsoDocumentoHabilitar(ModeloString modeloString)
        {
            HttpResponseMessage response;
            UsosDocumentosCreditosEcopoetrolObtener datos = new UsosDocumentosCreditosEcopoetrolObtener();
            DocumentosCreditosEcopetrol ObjBll = new DocumentosCreditosEcopetrol();
            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                datos = JsonConvert.DeserializeObject<UsosDocumentosCreditosEcopoetrolObtener>(modeloString.Valor);

                string json = JsonConvert.SerializeObject(ObjBll.ActualizarEstadoUsoDocumentoHabilitar(datos));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }

        //ACTUALIZA EL ESTADO DEL USO - DOCUMENTO</jarp>
        [HttpPost]
        [Route("api/CreditoEcopetrol/ActualizarEstadoUsoDocumentoDeshabilitar")]
        public HttpResponseMessage ActualizarEstadoUsoDocumentoDeshabilitar(ModeloString modeloString)
        {
            HttpResponseMessage response;
            UsosDocumentosCreditosEcopoetrolObtener datos = new UsosDocumentosCreditosEcopoetrolObtener();
            DocumentosCreditosEcopetrol ObjBll = new DocumentosCreditosEcopetrol();
            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                datos = JsonConvert.DeserializeObject<UsosDocumentosCreditosEcopoetrolObtener>(modeloString.Valor);

                string json = JsonConvert.SerializeObject(ObjBll.ActualizarEstadoUsoDocumentoDeshabilitar(datos));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }

        //ACTUALIZA EL ESTADO DEL USO - DOCUMENTO</jarp>
        [HttpPost]
        [Route("api/CreditoEcopetrol/ActualizarUsoDocumento")]
        public HttpResponseMessage ActualizarUsoDocumento(ModeloString modeloString)
        {
            HttpResponseMessage response;
            UsosDocumentosCreditosEcopoetrolObtener datos = new UsosDocumentosCreditosEcopoetrolObtener();
            DocumentosCreditosEcopetrol ObjBll = new DocumentosCreditosEcopetrol();
            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                datos = JsonConvert.DeserializeObject<UsosDocumentosCreditosEcopoetrolObtener>(modeloString.Valor);

                string json = JsonConvert.SerializeObject(ObjBll.ActualizarUsoDocumento(datos));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }

        //CONSULTA LAS SOLICITUDES POR CEDULA PARA CARGAR EL COMBO</jarp>
        [HttpPost]
        [Route("api/Avaluos/ConsultaSolicitudesAvaluos")]
        public HttpResponseMessage ConsultaSolicitudesAvaluos(ModeloString modeloString)
        {
            HttpResponseMessage response;
            AvaluosCrViviendaEcopetrolObtener datos = new AvaluosCrViviendaEcopetrolObtener();
            AvaluosCreditoEcopetrol ObjBllAvaluos = new AvaluosCreditoEcopetrol();
            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                datos = JsonConvert.DeserializeObject<AvaluosCrViviendaEcopetrolObtener>(modeloString.Valor);

                string json = JsonConvert.SerializeObject(ObjBllAvaluos.ConsultaSolicitudesAvaluos(datos));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }

        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarGestionBitacora")]
        public HttpResponseMessage GuardarGestionBitacora(ModeloString BitacoraModel)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                var convertidor = new ConvertirAListaSeleccionable();
                BitacoraModel.Valor = _cifradoDescifrado.decryptString(BitacoraModel.Valor);
                CreditoEcopetrolBitacora Bitacora = new CreditoEcopetrolBitacora();
                Bitacora = JsonConvert.DeserializeObject<CreditoEcopetrolBitacora>(BitacoraModel.Valor);
                var lista = ObjCreditoViviendaEcopetrol.GuardarGestionBitacora(Bitacora);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error al insertar la gestion -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al insertar la gestion");
            }
            return response;
        }

        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarGestionAsegurabilidad")]
        public HttpResponseMessage GuardarGestionAsegurabilidad(ModeloString AsegurabilidadModel)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                var convertidor = new ConvertirAListaSeleccionable();
                AsegurabilidadModel.Valor = _cifradoDescifrado.decryptString(AsegurabilidadModel.Valor);
                CreditoEcopetrolAsegurabilidad Asegurabilidad = new CreditoEcopetrolAsegurabilidad();
                Asegurabilidad = JsonConvert.DeserializeObject<CreditoEcopetrolAsegurabilidad>(AsegurabilidadModel.Valor);
                var lista = ObjCreditoViviendaEcopetrol.GuardarGestionAsegurabilidad(Asegurabilidad);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error al insertar la gestion de asegurabilidad -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al insertar la gestion de asegurabilidad");
            }
            return response;
        }

        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarAdjudicacion")]
        public HttpResponseMessage GuardarAdjudicacion(ModeloString AdjudicacionModel)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                var convertidor = new ConvertirAListaSeleccionable();
                AdjudicacionModel.Valor = _cifradoDescifrado.decryptString(AdjudicacionModel.Valor);
                CreditoEcopetrolAdjudicacion Adjudicacion = new CreditoEcopetrolAdjudicacion();
                Adjudicacion = JsonConvert.DeserializeObject<CreditoEcopetrolAdjudicacion>(AdjudicacionModel.Valor);
                var lista = ObjCreditoViviendaEcopetrol.GuardarAdjudicacion(Adjudicacion);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error al insertar la gestion de asegurabilidad -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al insertar la gestion de asegurabilidad");
            }
            return response;
        }

        //GENERA LA CONFIRMACION DEL PROCESO GENERADO CORRECTAMENTE</jarp>
        [HttpPost]
        [Route("api/proceso/FinalizarProceso")]
        public HttpResponseMessage FinalizarProceso(string PI_Id_Formulario, string PI_Numero_de_Identificacion, string PI_Comodin, string PO_Url, string PO_Bandera, string PO_Proceso)
        {
            HttpResponseMessage response;
            AvaluosCreditoEcopetrol ObjBllAvaluos = new AvaluosCreditoEcopetrol();
            try
            {
                PI_Id_Formulario = _cifradoDescifrado.decryptString(PI_Id_Formulario);
                PI_Numero_de_Identificacion = _cifradoDescifrado.decryptString(PI_Numero_de_Identificacion);
                PI_Comodin = _cifradoDescifrado.decryptString(PI_Comodin);
                PO_Url = _cifradoDescifrado.decryptString(PO_Url);
                PO_Bandera = _cifradoDescifrado.decryptString(PO_Bandera);
                PO_Proceso = _cifradoDescifrado.decryptString(PO_Proceso);

                ObjBllAvaluos.FinalizarProceso(PI_Id_Formulario, PI_Numero_de_Identificacion, PI_Comodin, PO_Url, PO_Bandera, PO_Proceso);
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }

        //INICIA EL PROCESOS DEL CREDITO VIVIENDA ECOPETROL</jarp>
        [HttpPost]
        [Route("api/proceso/IniciarProceso")]
        public HttpResponseMessage IniciarProcesoCreditoECP(string PI_Id_Formulario, string PI_Numero_de_Identificacion, string PI_Comodin, string PO_Url, string PO_Bandera, string PO_Proceso)
        {
            HttpResponseMessage response;
            CreditoViviendaEcopetrol ObjBll = new CreditoViviendaEcopetrol();
            try
            {
                PI_Id_Formulario = _cifradoDescifrado.decryptString(PI_Id_Formulario);
                PI_Numero_de_Identificacion = _cifradoDescifrado.decryptString(PI_Numero_de_Identificacion);
                PI_Comodin = _cifradoDescifrado.decryptString(PI_Comodin);
                PO_Url = _cifradoDescifrado.decryptString(PO_Url);
                PO_Bandera = _cifradoDescifrado.decryptString(PO_Bandera);
                PO_Proceso = _cifradoDescifrado.decryptString(PO_Proceso);
                int IdFrm = Convert.ToInt32(PI_Id_Formulario.Replace('"', ' '));
                int NumIdentificacion = Convert.ToInt32(PI_Numero_de_Identificacion.Replace('"', ' '));
                int Comodin = Convert.ToInt32(PI_Comodin.Replace('"', ' '));
                int Bandera = Convert.ToInt32(PO_Bandera.Replace('"', ' '));

                string json = JsonConvert.SerializeObject(ObjBll.IniciarProcesoCreditoECP(IdFrm, NumIdentificacion, Comodin, PO_Url, Bandera, PO_Proceso));
                //ObjBll.IniciarProcesoCreditoECP(IdFrm, NumIdentificacion, Comodin, PO_Url, Bandera, PO_Proceso);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }

        //GUARDA EL LOG DE LOS DATOS RELEVANTES DE QUIEN O QUE APROBO EL PROCESO </jarp>
        [HttpPost]
        [Route("api/proceso/GuardarLogAprobacionProceso")]
        public HttpResponseMessage GuardarLogAprobacionProceso(ModeloString modeloString)
        {
            HttpResponseMessage response;
            LogProcesosCrViviendaEcopetrol datos = new LogProcesosCrViviendaEcopetrol();
            AvaluosCreditoEcopetrol ObjBllAvaluos = new AvaluosCreditoEcopetrol();
            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                datos = JsonConvert.DeserializeObject<LogProcesosCrViviendaEcopetrol>(modeloString.Valor);

                string json = JsonConvert.SerializeObject(ObjBllAvaluos.GuardarLogAprobacionProceso(datos));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }

        [HttpPost]
        [Route("api/CreditoEcopetrol/ObtenerDocumentoxUsoECP")]
        public HttpResponseMessage ObtenerDocumentoxUsoECP(string IdUso)
        {
            DocumentosCreditosEcopetrol ObjBllDocumentos = new DocumentosCreditosEcopetrol();
            HttpResponseMessage response;
            try
            {
                var idUso = 0;
                IdUso = _cifradoDescifrado.decryptString(IdUso);
                int.TryParse(IdUso, out idUso);
                string json = JsonConvert.SerializeObject(ObjBllDocumentos.ObtenerDocumentoxUsoECP(idUso));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
            }
            return response;
        }

        //CONSULTA LAS SOLICITUDES POR NUMERO DE SOLICITUD PARA CARGAR EL FORMULARIO</jarp>
        [HttpPost]
        [Route("api/Avaluos/ConsultaInformacionSolicitudAvaluos")]
        public HttpResponseMessage ConsultaInformacionSolicitudAvaluos(ModeloString modeloString)
        {
            HttpResponseMessage response;
            AvaluosCrViviendaEcopetrol datos = new AvaluosCrViviendaEcopetrol();
            AvaluosCreditoEcopetrol ObjBllAvaluos = new AvaluosCreditoEcopetrol();
            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                datos = JsonConvert.DeserializeObject<AvaluosCrViviendaEcopetrol>(modeloString.Valor);

                string json = JsonConvert.SerializeObject(ObjBllAvaluos.ConsultaInformacionSolicitudAvaluos(datos));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }

        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarHipotecaECP")]
        public HttpResponseMessage GuardarHipotecaECP(ModeloString datosString)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {

                datosString.Valor = _cifradoDescifrado.decryptString(datosString.Valor);

                List<DatosHipoteca> FormaPago = JsonConvert.DeserializeObject<List<DatosHipoteca>>(datosString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarSolicitudInterproductos (usuario: " + usuario + ", contrasena:" + usuario + ")");

                ObjCreditoViviendaEcopetrol.GuardarHipotecaECP(FormaPago);

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(usuario));

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarSolicitudInterproductos() -> " + ex.Message);
                throw;
            }
            return response;
        }


        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarChequesECP")]
        public HttpResponseMessage GuardarChequesECP(ModeloString datosString)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {

                datosString.Valor = _cifradoDescifrado.decryptString(datosString.Valor);

                List<DatosCheque> DatosCheque = JsonConvert.DeserializeObject<List<DatosCheque>>(datosString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarSolicitudInterproductos (usuario: " + usuario + ", contrasena:" + usuario + ")");

                ObjCreditoViviendaEcopetrol.GuardarChequesECP(DatosCheque);

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(usuario));

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarSolicitudInterproductos() -> " + ex.Message);
                throw;
            }
            return response;
        }
        /// <summary>
        /// GUARDA DOCUMENTO QUE APLICA PARA EL ASOCIADO
        /// </summary>
        /// <param name="IdUso"></param>
        /// <param name="IdDocumento"></param>
        /// <param name="NumeroIdentificacion"></param>
        /// <param name="Observacion"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarDocumentoAsociado")]
        public HttpResponseMessage GuardarDocumentoAsociado(string IdUso, string IdDocumento, string NumeroIdentificacion, string Observacion)
        {
            HttpResponseMessage response;
            CreditoViviendaEcopetrol ObjBll = new CreditoViviendaEcopetrol();
            try
            {
                IdUso = _cifradoDescifrado.decryptString(IdUso);
                IdDocumento = _cifradoDescifrado.decryptString(IdDocumento);
                NumeroIdentificacion = _cifradoDescifrado.decryptString(NumeroIdentificacion);
                Observacion = _cifradoDescifrado.decryptString(Observacion);
                int Iduso = Convert.ToInt32(IdUso.Replace('"', ' '));
                int IdDocs = Convert.ToInt32(IdDocumento.Replace('"', ' '));

                string json = JsonConvert.SerializeObject(ObjBll.GuardarDocumentoAsociado(Iduso, IdDocs, NumeroIdentificacion, Observacion));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }
        /// <summary>
        /// GUARDA EL DOCUMENTO QUE NO APLICA PARA EL ASOCIADO
        /// </summary>
        /// <param name="IdUso"></param>
        /// <param name="IdDocumento"></param>
        /// <param name="NumeroIdentificacion"></param>
        /// <param name="Observacion"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarDocumentoNoAplicaAsociado")]
        public HttpResponseMessage GuardarDocumentoNoAplicaAsociado(string IdUso, string IdDocumento, string NumeroIdentificacion, string Observacion)
        {
            HttpResponseMessage response;
            CreditoViviendaEcopetrol ObjBll = new CreditoViviendaEcopetrol();
            try
            {
                IdUso = _cifradoDescifrado.decryptString(IdUso);
                IdDocumento = _cifradoDescifrado.decryptString(IdDocumento);
                NumeroIdentificacion = _cifradoDescifrado.decryptString(NumeroIdentificacion);
                Observacion = _cifradoDescifrado.decryptString(Observacion);
                int Iduso = Convert.ToInt32(IdUso.Replace('"', ' '));
                int IdDocs = Convert.ToInt32(IdDocumento.Replace('"', ' '));

                string json = JsonConvert.SerializeObject(ObjBll.GuardarDocumentoNoAplicaAsociado(Iduso, IdDocs, NumeroIdentificacion, Observacion));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw ex;
            }
            return response;
        }

        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarEnvioEstudio")]
        public HttpResponseMessage GuardarEnvioEstudio(ModeloString EstudioModel)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                var convertidor = new ConvertirAListaSeleccionable();
                EstudioModel.Valor = _cifradoDescifrado.decryptString(EstudioModel.Valor);
                CreditoEcopetrolEstudio Estudio = new CreditoEcopetrolEstudio();
                Estudio = JsonConvert.DeserializeObject<CreditoEcopetrolEstudio>(EstudioModel.Valor);
                var lista = ObjCreditoViviendaEcopetrol.GuardarEnvioEstudio(Estudio);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error al insertar la gestion -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al insertar el envio al abogado");
            }
            return response;
        }
        /// <summary>
        /// OBTIENE EL VALOR DE LA ADJUDICACION DEL CREDITO
        /// </summary>
        /// <param name="IdUso"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerInfoSolicitudCredito")]
        public HttpResponseMessage ObtenerInfoSolicitudCredito(string NumIdetificacion)
        {
            CreditoViviendaEcopetrol ObjBllCreditoViviendaECP = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                NumIdetificacion = _cifradoDescifrado.decryptString(NumIdetificacion);
                string json = JsonConvert.SerializeObject(ObjBllCreditoViviendaECP.ObtenerInfoSolicitudCredito(NumIdetificacion));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
            }
            return response;
        }

        /// <summary>
        /// OBTIENE EL VALOR DE LOS CHEQUES GENERADOS EN LA SOLICITUD
        /// </summary>
        /// <param name="IdUso"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerValorTotalChequesGenerados")]
        public HttpResponseMessage ObtenerValorTotalChequesGenerados(string Identificacion)
        {
            CreditoViviendaEcopetrol ObjBllCreditoViviendaECP = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                Identificacion = _cifradoDescifrado.decryptString(Identificacion);
                string json = JsonConvert.SerializeObject(ObjBllCreditoViviendaECP.ObtenerValorTotalChequesGenerados(Identificacion));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
            }
            return response;
        }

        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerInfoUsosSolicitudCreditoECP")]
        public IHttpActionResult ObtenerInfoUsosSolicitudCreditoECP(string NumIdetificacion)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerUsosCreditoECP()");
                NumIdetificacion = _cifradoDescifrado.decryptString(NumIdetificacion);
                var Lista = ObjCreditoViviendaEcopetrol.ObtenerInfoUsosSolicitudCreditoECP(NumIdetificacion);
                string json = JsonConvert.SerializeObject(Lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerUsosCreditoECP() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerInfoHipotecaSolicitudECP")]
        public IHttpActionResult ObtenerInfoHipotecaSolicitudECP(string NumIdentificacion)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerUsosCreditoECP()");
                NumIdentificacion = _cifradoDescifrado.decryptString(NumIdentificacion);
                var Lista = ObjCreditoViviendaEcopetrol.ObtenerInfoHipotecaSolicitudECP(NumIdentificacion);
                string json = JsonConvert.SerializeObject(Lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerUsosCreditoECP() -> " + ex.Message);
            }
            return Ok();
        }

        //OBTIENE EL PROCESO DE CADA PERSONA </jarp>
        [HttpGet]
		[Route("api/CreditoEcopetrol/ObtenerProcesoPersonaECP")]
		public HttpResponseMessage ObtenerProcesoECP(string identificacion)
		{
			CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
			HttpResponseMessage response;
			try
			{
				int Identificacion = 0;

				identificacion = _cifradoDescifrado.decryptString(identificacion);
				int.TryParse(identificacion, out Identificacion);
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerProcesoECP(identificacion: " + identificacion + ")");

				var Lista = ObjCreditoViviendaEcopetrol.ObtenerProcesoECP(Identificacion);
				string json = JsonConvert.SerializeObject(Lista);
				response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
			}

			catch (Exception ex)
			{
				logger.Error("Usuario: " + this.usuario + " -> Error ObtenerProcesoECP() -> " + ex.Message);
				return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
			}
			return response;
		}

		[HttpPost]
		[Route("api/CreditoEcopetrol/CambioEstadoFormulario")]
		public HttpResponseMessage ReiniciarProceso(string identificacion, string formulario)
		{
			CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
			HttpResponseMessage response;
			try
			{

				int Identificacion = 0;
				int Formulario = 0;

				identificacion = _cifradoDescifrado.decryptString(identificacion);
				formulario = _cifradoDescifrado.decryptString(formulario);
				int.TryParse(identificacion, out Identificacion);
				int.TryParse(formulario, out Formulario);
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ReiniciarProceso(identificacion: " + identificacion + " formulario: "+ formulario + ")");

				ObjCreditoViviendaEcopetrol.ReiniciarProceso(Identificacion, Formulario);
				response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString("true"));
			}
			catch (Exception ex)
			{
				logger.Error("Usuario: " + this.usuario + " -> Error ReiniciarProceso() -> " + ex.Message);
				return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
			}
			return response;
		}

        [HttpGet]
        [Route("api/CreditoEcopetrol/ObtenerDatosNotaContable")]
        public HttpResponseMessage ObtenerDatosNotaContable([FromUri] string NumeroIdentificacion, [FromUri] string Consecutivo)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                NumeroIdentificacion = _cifradoDescifrado.decryptString(NumeroIdentificacion);
                Consecutivo = _cifradoDescifrado.decryptString(Consecutivo);
                //var datosTipoGestion = ObjCreditoViviendaEcopetrol.ObtenerTipoGestion().ToList();
                var lista = ObjCreditoViviendaEcopetrol.ObtenerDatosNotaContable(NumeroIdentificacion, Consecutivo).ToList();
                int rows = lista.Count();
                if (rows >= 1)
                {

                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar los tipos de gestion.");
                }

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error Consultando Tipos de Gestión() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al consultar la nota contable");
            }
            return response;
        }

		[HttpPost]
		[Route("api/CreditoEcopetrol/AceptacionProcesoAdjudicacion")]
		public HttpResponseMessage AceptacionProcesoAdjudicacion(string tipoIdentificacion, string identificacion, string producto, string numeroSolicitud, string autorizacion)
		{
			CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
			HttpResponseMessage response;
			try
			{

				tipoIdentificacion = _cifradoDescifrado.decryptString(tipoIdentificacion);
				identificacion = _cifradoDescifrado.decryptString(identificacion);
				producto = _cifradoDescifrado.decryptString(producto);
				numeroSolicitud = _cifradoDescifrado.decryptString(numeroSolicitud);
				autorizacion = _cifradoDescifrado.decryptString(autorizacion);

				logger.Debug("Usuario: " + this.usuario + " -> Inicio AceptacionProcesoAdjudicacion(tipoIdentificacion: " + tipoIdentificacion + " identificacion: " + identificacion + "producto"+ producto + "numeroSolicitud"+ numeroSolicitud + "autorizacion"+ autorizacion + ")");

				ObjCreditoViviendaEcopetrol.AceptacionProcesoAdjudicacion(tipoIdentificacion, identificacion, producto, numeroSolicitud, autorizacion);
				response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString("true"));
			}
			catch (Exception ex)
			{
				logger.Error("Usuario: " + this.usuario + " -> Error AceptacionProcesoAdjudicacion() -> " + ex.Message);
				return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
			}
			return response;
		}

		[HttpGet]
		[Route("api/CreditoEcopetrol/ObtenerInformacionReporteSolicitud")]
		public HttpResponseMessage ObtenerInformacionReporteSolicitud(string identificacion, string concecutivo)
		{
			CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
			HttpResponseMessage response;
			try
			{

				identificacion = _cifradoDescifrado.decryptString(identificacion);
				concecutivo = _cifradoDescifrado.decryptString(concecutivo);
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerInformacionReporteSolicitud(identificacion: " + identificacion + "concecutivo:" + concecutivo +")");

				var Lista = ObjCreditoViviendaEcopetrol.ObtenerInformacionReporteSolicitud(identificacion, concecutivo);
				string json = JsonConvert.SerializeObject(Lista);
				response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
			}

			catch (Exception ex)
			{
				logger.Error("Usuario: " + this.usuario + " -> Error ObtenerInformacionReporteSolicitud() -> " + ex.Message);
				return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
			}
			return response;
		}

		[HttpGet]
		[Route("api/CreditoEcopetrol/ObtenerFormasPagoECP")]
		public HttpResponseMessage ObtenerFormasPagoECP(string identificacion, string concecutivo)
		{
			CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
			HttpResponseMessage response;
			try
			{

				identificacion = _cifradoDescifrado.decryptString(identificacion);
				concecutivo = _cifradoDescifrado.decryptString(concecutivo);
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerFormasPagoECP(identificacion: " + identificacion + "concecutivo:" + concecutivo + ")");

				var Lista = ObjCreditoViviendaEcopetrol.ObtenerFormasPagoECP(identificacion, concecutivo);
				string json = JsonConvert.SerializeObject(Lista);
				response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
			}

			catch (Exception ex)
			{
				logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormasPagoECP() -> " + ex.Message);
				return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
			}
			return response;
		}


        [HttpPost]
        [Route("api/CreditoEcopetrol/GestionPagare")]
        public HttpResponseMessage GestionPagare(ModeloString PagareModel)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                var convertidor = new ConvertirAListaSeleccionable();
                PagareModel.Valor = _cifradoDescifrado.decryptString(PagareModel.Valor);
                CreditoEcopetrolPagare Pagare = new CreditoEcopetrolPagare();
                Pagare = JsonConvert.DeserializeObject<CreditoEcopetrolPagare>(PagareModel.Valor);
                var lista = ObjCreditoViviendaEcopetrol.GestionPagare(Pagare);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error al gestionar el pagaré -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al insertar el pagare");
            }
            return response;
        }

        [HttpPost]
        [Route("api/CreditoEcopetrol/GuardarLogAvaluos")]
        public IHttpActionResult GuardarLogAvaluos(string IdConsecutivo, string NitAfiliado, string NumServicio, string UsuarioSolicitud)
        {
            AvaluosCreditoEcopetrol ObjAvaluosCreditoEcopetrol = new AvaluosCreditoEcopetrol();
            try
            {
                int numServicio = 0;
                int Consecutivo = 0;

                IdConsecutivo = _cifradoDescifrado.decryptString(IdConsecutivo);
                NitAfiliado = _cifradoDescifrado.decryptString(NitAfiliado);
                NumServicio = _cifradoDescifrado.decryptString(NumServicio);
                UsuarioSolicitud = _cifradoDescifrado.decryptString(UsuarioSolicitud);
                int.TryParse(IdConsecutivo, out Consecutivo);
                int.TryParse(NumServicio, out numServicio);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposDocumentoRequisito()");

                ObjAvaluosCreditoEcopetrol.GuardarLogAvaluos(Consecutivo, NitAfiliado, numServicio, UsuarioSolicitud);

                return Ok(_cifradoDescifrado.encryptString("true"));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposDocumentoRequisito() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpPost]
        [Route("api/CreditoEcopetrol/ObtenerNumSolicitudFechaAvaluo")]
        public HttpResponseMessage ObtenerNumSolicitudFechaAvaluo(string NitAfiliado)
        {
            AvaluosCreditoEcopetrol ObjAvaluosCreditoEcopetrol = new AvaluosCreditoEcopetrol();
            HttpResponseMessage response;
            try
            {

                NitAfiliado = _cifradoDescifrado.decryptString(NitAfiliado);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerFormasPagoECP(identificacion: " + NitAfiliado + ")");

                var Lista = ObjAvaluosCreditoEcopetrol.ObtenerNumSolicitudFechaAvaluo(NitAfiliado);
                string json = JsonConvert.SerializeObject(Lista);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }

            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormasPagoECP() -> " + ex.Message);
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
            }
            return response;
        }

        [HttpGet]
        [Route("api/CreditoEcopetrol/ConsultarProgramacionGiros")]
        public HttpResponseMessage ConsultarProgramacionGiros(string FechaInicial, string FechaFinal)
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {

                FechaInicial = _cifradoDescifrado.decryptString(FechaInicial);
                FechaFinal = _cifradoDescifrado.decryptString(FechaFinal);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsultarProgramacionGiros(FechaInicial: " + FechaInicial + "FechaFinal:" + FechaFinal + ")");

                var Lista = ObjCreditoViviendaEcopetrol.ConsultarProgramacionGiros(FechaInicial, FechaFinal);
                string json = JsonConvert.SerializeObject(Lista);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }

            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormasPagoECP() -> " + ex.Message);
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
            }
            return response;
        }

        [HttpPost]
        [Route("api/CreditoEcopetrol/GenerarNumeroOrden")]
        public HttpResponseMessage GenerarNumeroOrgen(ModeloString OrgenGiroModel)
        {
            //fechaCargue = _cifradoDescifrado.decryptString(fechaCargue);
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {
                var convertidor = new ConvertirAListaSeleccionable();
                OrgenGiroModel.Valor = _cifradoDescifrado.decryptString(OrgenGiroModel.Valor);
                CreditoEcopetrolCreacionOrden Pagare = new CreditoEcopetrolCreacionOrden();
                Pagare = JsonConvert.DeserializeObject<CreditoEcopetrolCreacionOrden>(OrgenGiroModel.Valor);
                var lista = ObjCreditoViviendaEcopetrol.GenerarNumeroOrgen(Pagare);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error al gestionar el pagaré -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, "Algo sucedio al insertar el pagare");
            }
            return response;
        }

        [HttpGet]
        [Route("api/CreditoEcopetrol/ConsultaReferenciaPagos")]
        public HttpResponseMessage ConsultaReferenciaPagos()
        {
            CreditoViviendaEcopetrol ObjCreditoViviendaEcopetrol = new CreditoViviendaEcopetrol();
            HttpResponseMessage response;
            try
            {

              
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsultaReferenciaPagos()");

                var Lista = ObjCreditoViviendaEcopetrol.ConsultaReferenciaPagos();
                string json = JsonConvert.SerializeObject(Lista);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }

            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormasPagoECP() -> " + ex.Message);
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(ex)));
            }
            return response;
        }
    }
}