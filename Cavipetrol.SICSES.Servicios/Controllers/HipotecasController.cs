﻿using Cavipetrol.SICSES.Facade;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HipotecasController : ApiController
    {
        private readonly IHipotecasFacade _HipotecasFacade;
        private Cifrado.CifradoDescifrado  _cifradoDescifrado;

        private static readonly log4net.ILog logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";
        public HipotecasController()
        {
            _HipotecasFacade = new HipotecasFacade();
            _cifradoDescifrado = new Cifrado.CifradoDescifrado();
            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }

        [HttpGet]
        [Route("api/Hipotecas/ObtenerHipotecas")]
        public HttpResponseMessage ObtenerHipotecas(string NumeroHipoteca = "",
                                                    string TipoIdentificacion = "",
                                                    string NumeroIdentificacion = "",
                                                    string NumeroSolicitud = "")
        {
            HttpResponseMessage response;
            try
            {  
                NumeroHipoteca = _cifradoDescifrado.decryptString(NumeroHipoteca);
                TipoIdentificacion = _cifradoDescifrado.decryptString(TipoIdentificacion);
                NumeroIdentificacion = _cifradoDescifrado.decryptString(NumeroIdentificacion);
                NumeroSolicitud = _cifradoDescifrado.decryptString(NumeroSolicitud);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerHipotecas("
                + "NumeroHipoteca: " + NumeroHipoteca + ", TipoIdentificacion: " + TipoIdentificacion
                + ", NumeroIdentificacion: " + NumeroIdentificacion + ", NumeroSolicitud: " + NumeroSolicitud + ")");


                var DtaDefault = _HipotecasFacade.ObtenerHipotecas(NumeroHipoteca, TipoIdentificacion, NumeroIdentificacion, NumeroSolicitud);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DtaDefault)));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerHipotecas() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Hipotecas/ActualizaHipotecas")]
        public HttpResponseMessage ActualizaHipotecas(int IdHipoteca,
                                                      string AudUsuarioModificacion,
                                                      string FechaEscrituracion = "",
                                                      string NumeroEscritura = "",
                                                      string ValorAvaluo = "",
                                                      string VigenciaInicial = "",
                                                      string VigenciaFinal = "",
                                                      string NumeroNotaria = "",
                                                      string Oficina = "")
        {
            HttpResponseMessage response;
            try
            {
                //IdHipoteca = _cifradoDescifrado.decryptString(IdHipoteca);
                AudUsuarioModificacion = _cifradoDescifrado.decryptString(AudUsuarioModificacion);
                FechaEscrituracion = _cifradoDescifrado.decryptString(FechaEscrituracion);
                NumeroEscritura = _cifradoDescifrado.decryptString(NumeroEscritura);
                ValorAvaluo = _cifradoDescifrado.decryptString(ValorAvaluo);
                VigenciaInicial = _cifradoDescifrado.decryptString(VigenciaInicial);
                VigenciaFinal = _cifradoDescifrado.decryptString(VigenciaFinal);
                NumeroNotaria = _cifradoDescifrado.decryptString(NumeroNotaria);
                Oficina = _cifradoDescifrado.decryptString(Oficina);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ActualizaHipotecas("
                + "IdHipoteca: " + IdHipoteca + ", AudUsuarioModificacion: " + AudUsuarioModificacion
                + ", FechaEscrituracion: " + FechaEscrituracion + ", NumeroEscritura: " + NumeroEscritura
                + ", ValorAvaluo: " + ValorAvaluo + ", VigenciaInicial: " + VigenciaInicial
                + ", VigenciaFinal: " + VigenciaFinal + ", NumeroNotaria: " + NumeroNotaria
                + ", Oficina: " + Oficina + ")");

                var datosProveedores = _HipotecasFacade.ActualizaHipotecas(IdHipoteca, AudUsuarioModificacion, FechaEscrituracion, NumeroEscritura, ValorAvaluo, VigenciaInicial, VigenciaFinal, NumeroNotaria, Oficina);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(datosProveedores)));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ActualizaHipotecas() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Hipotecas/ObtenerHipotecasCedula")]
        public HttpResponseMessage ObtenerHipotecasCedula(string TipoIdentificacion = "", string NumeroIdentificacion = "", string ValorSolicitado = "")
        {
            HttpResponseMessage response;
            try
            {
                TipoIdentificacion = _cifradoDescifrado.decryptString(TipoIdentificacion);
                NumeroIdentificacion = _cifradoDescifrado.decryptString(NumeroIdentificacion);
                ValorSolicitado = _cifradoDescifrado.decryptString(ValorSolicitado);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerHipotecasCedula("
                + "TipoIdentificacion: " + TipoIdentificacion + ", NumeroIdentificacion: " + NumeroIdentificacion
                + ", ValorSolicitado: " + ValorSolicitado + ")");

                double doubleValorSolicitado = double.Parse(ValorSolicitado);
                var listaHipotecas = _HipotecasFacade.ObtenerHipotecasXCedula(TipoIdentificacion, NumeroIdentificacion, doubleValorSolicitado);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(listaHipotecas)));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerHipotecasCedula() -> " + ex.Message);
            }
            return response;
        }
    }
}
