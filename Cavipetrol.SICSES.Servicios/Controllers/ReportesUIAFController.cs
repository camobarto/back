﻿using Cavipetrol.SICSES.Facade;
using Cavipetrol.SICSES.Infraestructura.Model.ReportesUIAF;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReportesUIAFController : ApiController
    {
        private readonly IReportsUIAFFacade IReportsUIAFFacade;

        //CLASE PARA CIFRADO DE PARAMETROS
        private Cifrado.CifradoDescifrado _cifradoDesifrado;

        private static readonly log4net.ILog logger =
        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";

        public ReportesUIAFController()
        {
            IReportsUIAFFacade = new ReportsUIAFFacade();
            _cifradoDesifrado = new Cifrado.CifradoDescifrado();
            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }

        [HttpGet]
        [Route("api/ReportesUIAF/TransaccionesEnEfectivo")]
        public HttpResponseMessage ObtenerTransaccionesEnEfectivo(string idReporte, //int 
                                                                string fechaInicio, 
                                                                string fechaFin)
        {
            HttpResponseMessage response;
            int intIdReporte = 0;

            idReporte = _cifradoDesifrado.decryptString(idReporte);
            fechaInicio = _cifradoDesifrado.decryptString(fechaInicio);
            fechaFin = _cifradoDesifrado.decryptString(fechaFin);
            int.TryParse(idReporte, out intIdReporte);

            try
             {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTransaccionesEnEfectivo(idReporte: " + idReporte + ", fechaInicio: " + fechaInicio + ", fechaFin: " + fechaFin + ")");

                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivo>> respuesta = 
                    IReportsUIAFFacade.ObtenerReporteTransaccionesEnEfectivo(intIdReporte, fechaInicio, fechaFin);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTransaccionesEnEfectivo() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportesUIAF/TransaccionesEnEfectivoRiesgos")]
        public HttpResponseMessage ObtenerTransaccionesEnEfectivoRiesgos(string idReporte, //int 
                                                                string fechaInicio,
                                                                string fechaFin)
        {
            HttpResponseMessage response;
            int intIdReporte = 0;

            idReporte = _cifradoDesifrado.decryptString(idReporte);
            fechaInicio = _cifradoDesifrado.decryptString(fechaInicio);
            fechaFin = _cifradoDesifrado.decryptString(fechaFin);
            int.TryParse(idReporte, out intIdReporte);

            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTransaccionesEnEfectivoRiesgos(idReporte: " + idReporte + ", fechaInicio: " + fechaInicio + ", fechaFin: " + fechaFin + ")");
                                
                RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivoRiesgos>> respuesta =
                    IReportsUIAFFacade.ObtenerReporteTransaccionesEnEfectivoRiesgos(intIdReporte, fechaInicio, fechaFin);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTransaccionesEnEfectivoRiesgos() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportesUIAF/ProductosOfrecidos")]
        public HttpResponseMessage ObtenerReporteProductosEconomiaSolidaria(string idReporte, //int  
                                                                            string mes, //int  
                                                                            string ano) //int 
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            

            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);
                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerReporteProductosEconomiaSolidaria(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteProductosEconomiaSolidaria>> respuesta = IReportsUIAFFacade.ObtenerReporteProductosEconomiaSolidaria(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerReporteProductosEconomiaSolidaria() -> " + ex.Message);
            }
            return response;
        }


    }
}
