﻿using Cavipetrol.SICSES.Facade;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Administracion;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdministracionController : ApiController   
    {
        private readonly IAdministracionFacade _administracionFacade;

        //CLASE PARA CIFRADO DE PARAMETROS
        private Cifrado.CifradoDescifrado _cifradoDescifrado;

        private static readonly log4net.ILog logger =
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";
        public AdministracionController()
        {
            _administracionFacade = new AdministracionFacade();
            _cifradoDescifrado = new Cifrado.CifradoDescifrado();

            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }

        [HttpGet]
        [Route("api/Administracion/ObtenerLineaCredito")]
        public HttpResponseMessage ObtenerLineaCredito()
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerLineaCredito()");
                response = Request.CreateResponse(HttpStatusCode.OK, _administracionFacade.ObtenerLineaCredito());
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerLineaCredito() -> " + ex.Message);
            }
            return response;
        }

        [HttpPost]
        [Route("api/Administracion/GuardarLineaCredito")]
        public HttpResponseMessage GuardarLineaCredtio(string id,string nombre,string Descripcion, string bandera )
        {
            HttpResponseMessage response;
            try
            {
                nombre = _cifradoDescifrado.decryptString(nombre);
                Descripcion = _cifradoDescifrado.decryptString(Descripcion);
                int bande = Convert.ToInt32(_cifradoDescifrado.decryptString(bandera));
                int idcre = Convert.ToInt32(_cifradoDescifrado.decryptString(id));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarLineaCredtio( id: " + id + ", nombre: " + nombre + ", Descripcion: " + Descripcion + ", bandera: " + bandera + ")");
                response = Request.CreateResponse(HttpStatusCode.OK, _administracionFacade.GuardarLineaCredito(idcre, nombre, Descripcion, bande));            
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarLineaCredtio() -> " + ex.Message);
            }
            return response;
        }
        [HttpDelete]
        [Route("api/Administracion/EliminarLineaCredito")]
        public HttpResponseMessage EliminarLineaCredito(string id)
        {
            HttpResponseMessage response;
            try
            {
                int idcre = Convert.ToInt32(_cifradoDescifrado.decryptString(id));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio EliminarLineaCredito( id: " + id + ")");

                response = Request.CreateResponse(HttpStatusCode.OK, _administracionFacade.EliminarLineaCredito(idcre));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error EliminarLineaCredito() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Administracion/ObtenerTiposCredito")]
        public HttpResponseMessage ObtenerTiposCredito()
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposCredito()");
                response = Request.CreateResponse(HttpStatusCode.OK, _administracionFacade.ObtenerTiposCredito());
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposCredito() -> " + ex.Message);
            }
            return response;
        }
        [HttpPost]
        [Route("api/Administracion/AdministrarTipoCredito")]
        public HttpResponseMessage AdministrarTiposCredito(string id, string nombre, string idLinea, string bandera)
        {
            HttpResponseMessage response;
            try
            {
                int idTipoCredito = Convert.ToInt32(_cifradoDescifrado.decryptString(id));
                nombre = _cifradoDescifrado.decryptString(nombre);
                int idLineaCredito = Convert.ToInt32(_cifradoDescifrado.decryptString(idLinea));
                int bande = Convert.ToInt32(_cifradoDescifrado.decryptString(bandera));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio AdministrarTiposCredito( id: " + id + ", nombre: " + nombre + ", idLinea: " + idLinea + ", bandera: " + bandera + ")");

                response = Request.CreateResponse(HttpStatusCode.OK, _administracionFacade.AdministrarTiposCredito(idTipoCredito,  nombre, idLineaCredito, bande));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error AdministrarTiposCredito() -> " + ex.Message);
            }
            return response;
        }
        [HttpDelete]
        [Route("api/Administracion/EliminarTiposCredito")]
        public HttpResponseMessage EliminarTiposCredito(string id)
        {
            HttpResponseMessage response;
            try
            {
                int idcre = Convert.ToInt32(_cifradoDescifrado.decryptString(id));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio EliminarTiposCredito( id: " + id + ")");

                response = Request.CreateResponse(HttpStatusCode.OK, _administracionFacade.EliminarTiposCredito(idcre));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error EliminarTiposCredito() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Administracion/ObtenerPapelCavipetrol")]
        public HttpResponseMessage ObtenerPapelCavipetrol()
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerPapelCavipetrol()");
                response = Request.CreateResponse(HttpStatusCode.OK, _administracionFacade.ObtenerPapelCavipetrol());
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerPapelCavipetrol() -> " + ex.Message);
            }
            return response;
        }
        [HttpPost]
        [Route("api/Administracion/AdministrarPapelCavipetrol")]
        public HttpResponseMessage AdministrarPapelCavipetrol(string id, 
                                                              string nombre, 
                                                              string Descripcion,
                                                              string idcontrato, 
                                                              string bandera)
        {
            HttpResponseMessage response;
            try
            {
                int idPapel = Convert.ToInt32(_cifradoDescifrado.decryptString(id));
                nombre = _cifradoDescifrado.decryptString(nombre);
                Descripcion = _cifradoDescifrado.decryptString(Descripcion);
                int idContrac = Convert.ToInt32(_cifradoDescifrado.decryptString(idcontrato));
                int bande = Convert.ToInt32(_cifradoDescifrado.decryptString(bandera));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio AdministrarPapelCavipetrol( id: " + id + 
                ", nombre: " + nombre + ", Descripcion: " + Descripcion + ", idcontrato: " + idcontrato + ", bandera: " + bandera + ")");

                response = Request.CreateResponse(HttpStatusCode.OK, _administracionFacade.AdministrarPapelCavipetrol(idPapel, nombre, Descripcion,idContrac, bande));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error AdministrarPapelCavipetrol() -> " + ex.Message);
            }
            return response;
        }
        [HttpDelete]
        [Route("api/Administracion/EliminarPapelCavipetrol")]
        public HttpResponseMessage EliminarPapelCavipetrol(string id)
        {
            HttpResponseMessage response;
            try
            {
                int idPapel = Convert.ToInt32(_cifradoDescifrado.decryptString(id));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio EliminarPapelCavipetrol( id: " + id + ")");

                response = Request.CreateResponse(HttpStatusCode.OK, _administracionFacade.EliminarPapelCavipetrol(idPapel));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error EliminarPapelCavipetrol() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Administracion/ObtenerTipoCreditoTipoRolRelacion")]
        public HttpResponseMessage ObtenerTipoCreditoTipoRolRelacion()
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoCreditoTipoRolRelacion()");
                response = Request.CreateResponse(HttpStatusCode.OK, _administracionFacade.ObtenerTipoCreditoTipoRolRelacion());
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoCreditoTipoRolRelacion() -> " + ex.Message);
            }
            return response;
        }

        [HttpPost]
        [Route("api/Administracion/AdministrarTipoCreditoTipoRol")]
        public HttpResponseMessage AdministrarTipoCreditoTipoRol(ModeloCifrado StringCifrado)
        {
            HttpResponseMessage response;
            TipoCreditoTipoRol JsonObject = new TipoCreditoTipoRol();
            try
            {
                StringCifrado.Valor = _cifradoDescifrado.decryptString(StringCifrado.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio AdministrarTipoCreditoTipoRol( ModeloCifrado: " + StringCifrado.Valor + ")");

                JsonObject = JsonConvert.DeserializeObject<TipoCreditoTipoRol>(StringCifrado.Valor);

                response = Request.CreateResponse(HttpStatusCode.OK, _administracionFacade.AdministrarTipoCreditoTipoRol(JsonObject));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error AdministrarTipoCreditoTipoRol() -> " + ex.Message);
            }
            return response;
        }

    }
}