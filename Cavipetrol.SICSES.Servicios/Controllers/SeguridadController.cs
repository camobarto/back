﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cavipetrol.SICSES.Facade;
using Cavipetrol.SICSES.Infraestructura.Model.Seguridad;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Utilidades;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SeguridadController : ApiController 
    {

        //CLASE PARA CIFRADO DE PARAMETROS
        private Cifrado.CifradoDescifrado _cifradoDesifrado;

        private static readonly log4net.ILog logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";

        private readonly ISeguridadFacade _seguridadFacade;
        public SeguridadController()
        {
            _seguridadFacade = new SeguridadFacade();
            _cifradoDesifrado = new Cifrado.CifradoDescifrado();
            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
               .Identity).NameClaimType;
        }

        [HttpPost]
        [Route("api/Seguridad/ObtenerPerfilUsuario")]
        public HttpResponseMessage ObtenerPerfilUsuario([FromUri] string uniqueName)
        {
            HttpResponseMessage response;
            try
            {
                uniqueName = _cifradoDesifrado.decryptString(uniqueName);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerPerfilUsuario ( uniqueName: " + uniqueName + ")");

                UsuarioDTO usuario = _seguridadFacade.ObtenerPerfilUsuario(uniqueName);

                string json = JsonConvert.SerializeObject(usuario);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerPerfilUsuario() -> " + ex.Message);
                throw;
            }
            return response;
        }

        // GET: api/Seguridad/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Seguridad
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Seguridad/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Seguridad/5
        public void Delete(int id)
        {
        }

        [HttpPost]
        [Route("api/Seguridad/GuardarUser")]
        public HttpResponseMessage GuardarClaveTemporal(string usuario, string contrasena)
        {
            HttpResponseMessage response;
            try
            {
                usuario = _cifradoDesifrado.decryptString(usuario);
                contrasena = _cifradoDesifrado.decryptString(contrasena);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarClaveTemporal (usuario: " + usuario + ", contrasena:" + usuario +  ")");

                _seguridadFacade.GuardarUsuarioTemporal(usuario,contrasena);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(usuario));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarClaveTemporal() -> " + ex.Message);
                throw;
            }
            return response;
        }

        [HttpPost]
        [Route("api/Seguridad/ValidarInicioUsuario")]
        public HttpResponseMessage ValidarInicioUsuario(string usuario)
        {
            HttpResponseMessage response;
            try
            {
                usuario = _cifradoDesifrado.decryptString(usuario);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ValidarInicioUsuario (usuario: " + usuario + ")");

                var a = _seguridadFacade.ValidarInicioUsuario(usuario);
                string json = JsonConvert.SerializeObject(a.Respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ValidarInicioUsuario() -> " + ex.Message);
                throw;
            }
            return response;
        }
        [HttpPost]
        [Route("api/Seguridad/InicioUsuario")]
        public HttpResponseMessage AdministrarInicioSesion(string usuario, string contrasena, string NuevaContrasena, string bandera)
        {
            int intBandera = 0;
            HttpResponseMessage response;
            try
            {
                usuario = _cifradoDesifrado.decryptString(usuario);
                contrasena = _cifradoDesifrado.decryptString(contrasena);
                NuevaContrasena = _cifradoDesifrado.decryptString(NuevaContrasena);
                bandera = _cifradoDesifrado.decryptString(bandera);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio AdministrarInicioSesion (usuario: " + usuario + ", contrasena:" + contrasena + ", NuevaContrasena:" + NuevaContrasena + ", bandera: " + bandera + ")");

                int.TryParse(bandera, out intBandera);
                UsuarioLogin respuesta = _seguridadFacade.AdministrarInicioSesion(usuario, contrasena, NuevaContrasena, intBandera);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error AdministrarInicioSesion() -> " + ex.Message);
                throw;
            }
            return response;
        }
        [HttpGet]
        [Route("api/Seguridad/ObtenerUsuario")]
        public HttpResponseMessage ObtenerUsuario()
        {
            HttpResponseMessage response;

            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerUsuario ()");
                string json = JsonConvert.SerializeObject(_seguridadFacade.ObtenerUsuario());
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerUsuario() -> " + ex.Message);
                throw;
            }
            return response;
        }
        [HttpGet]
        [Route("api/Seguridad/ObtenerPerfil")]
        public HttpResponseMessage ObtenerPerfil()
        {
            HttpResponseMessage response;

            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerPerfil ()");
                string json = JsonConvert.SerializeObject(_seguridadFacade.ObtenerPerfil());
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerPerfil() -> " + ex.Message);
                throw;
            }
            return response;
        }
        [HttpGet]
        [Route("api/Seguridad/ObtenerOficinas")]
        public HttpResponseMessage ObtenerOficinas()
        {
            HttpResponseMessage response;

            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerOficinas ()");
                string json = JsonConvert.SerializeObject(_seguridadFacade.ObtenerOficinas());
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerOficinas() -> " + ex.Message);
                throw;
            }
            return response;
        }
        [HttpPost]
        [Route("api/Seguridad/GuardarUsuario")]
        public HttpResponseMessage GuardarUsuario(ModeloString modeloString)
        {
            HttpResponseMessage response;
            Usuario datos = new Usuario();

            try
            {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                datos = JsonConvert.DeserializeObject<Usuario>(modeloString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> GuardarUsuario (usuario: " + modeloString.Valor + ")");
                string json = JsonConvert.SerializeObject(_seguridadFacade.GuardarUsuario(datos));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarUsuario() -> " + ex.Message);
                throw;
            }
            return response;
        }
        [HttpPost]
        [Route("api/Seguridad/GuardarPerfil")]
        public HttpResponseMessage GuardarPerfil(ModeloString modeloString)
        {
            HttpResponseMessage response;
            Perfil perfil = new Perfil();

            try
            {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                perfil = JsonConvert.DeserializeObject<Perfil>(modeloString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarPerfil (usuario: " + modeloString.Valor + ")");

                string json = JsonConvert.SerializeObject(_seguridadFacade.GuardarPerfil(perfil));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarPerfil() -> " + ex.Message);
                throw;
            }
            return response;
        }
    }
    }
