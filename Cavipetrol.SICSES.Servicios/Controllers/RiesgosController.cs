﻿using Cavipetrol.SICSES.Facade;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RiesgosController : ApiController
    {
        private readonly IRiesgosFacade _IRiesgosFacade;
        //CLASE PARA CIFRADO DE PARAMETROS
        private Cifrado.CifradoDescifrado _cifradoDescifrado;

        private static readonly log4net.ILog logger =
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";

        public RiesgosController()
        {
            _IRiesgosFacade = new RiesgosFacade();
            _cifradoDescifrado = new Cifrado.CifradoDescifrado();

            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }

        [HttpGet]
        [Route("api/Riesgos/ObtenerOperacion")]

        public HttpResponseMessage ObtenerOperacion(string TipoIdentificacion, string NumeroIdentificacion, string NumeroOperacion)
        {
            HttpResponseMessage response;

            try
            {
                TipoIdentificacion = _cifradoDescifrado.decryptString(TipoIdentificacion);
                NumeroIdentificacion = _cifradoDescifrado.decryptString(NumeroIdentificacion);
                NumeroOperacion = _cifradoDescifrado.decryptString(NumeroOperacion);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerOperacion( Tipo de Identificacion: " + TipoIdentificacion + ", Número: " + NumeroIdentificacion + ", Operacion: " + NumeroOperacion + ")");                
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(_IRiesgosFacade.ObtenerOperacion(TipoIdentificacion, NumeroIdentificacion, NumeroOperacion))));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerOperacion( Tipo de Identificacion: " + TipoIdentificacion + ", Número: " + NumeroIdentificacion + ") -> " + ex.Message);
            }

            return response;
        }

        [HttpPost]
        [Route("api/Riesgos/GuardarDatosPersonaTransaccionEfectivo")]
        public HttpResponseMessage GuardarDatosPersonaTransaccionEfectivo(string Operacion, string Tipoidentificacion, string Numeroidentificacion, string Primernombre,
                                                        string Segundonombre, string Primerapellido, string Segundoapellido, string Direccion, string Telefono,
                                                        string Consulta, string Tipopersona)
        {
            HttpResponseMessage response;
            try
            {
                Operacion = _cifradoDescifrado.decryptString(Operacion);
                Tipoidentificacion = _cifradoDescifrado.decryptString(Tipoidentificacion);
                Numeroidentificacion = _cifradoDescifrado.decryptString(Numeroidentificacion);
                Primernombre = _cifradoDescifrado.decryptString(Primernombre);
                Segundonombre = _cifradoDescifrado.decryptString(Segundonombre);
                Primerapellido = _cifradoDescifrado.decryptString(Primerapellido);
                Segundoapellido = _cifradoDescifrado.decryptString(Segundoapellido);
                Direccion = _cifradoDescifrado.decryptString(Direccion);
                Telefono = _cifradoDescifrado.decryptString(Telefono);
                Consulta = _cifradoDescifrado.decryptString(Consulta);
                Tipopersona = _cifradoDescifrado.decryptString(Tipopersona);


                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarDatosPersonaTransaccionEfectivo (usuario: " + usuario + ", contrasena:" + usuario + ")");

                _IRiesgosFacade.GuardarDatosPersonaTransaccionEfectivo(Convert.ToInt32(Operacion), Tipoidentificacion, Numeroidentificacion, Primernombre, Segundonombre, Primerapellido,
                                                                    Segundoapellido, Direccion, Telefono, Consulta, Tipopersona);

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(usuario));

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarDatosPersonaTransaccionEfectivo() -> " + ex.Message);
                throw;
            }
            return response;
        }

        [HttpPost]
        [Route("api/Riesgos/GuardarDatosOrigenFondos")]
        public HttpResponseMessage GuardarDatosOrigenFondos(string Operacion, string Opcion, string Consulta, string Detalle)
        {
            HttpResponseMessage response;
            try
            {
                Operacion = _cifradoDescifrado.decryptString(Operacion);
                Opcion = _cifradoDescifrado.decryptString(Opcion);
                Consulta = _cifradoDescifrado.decryptString(Consulta);
                Detalle = _cifradoDescifrado.decryptString(Detalle);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarDatosPersonaTransaccionEfectivo (usuario: " + usuario + ", contrasena:" + usuario + ")");

                _IRiesgosFacade.GuardarDatosOrigenFondos(Convert.ToInt32(Operacion), Convert.ToInt32(Opcion), Consulta, Detalle);

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(usuario));

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarDatosPersonaTransaccionEfectivo() -> " + ex.Message);
                throw;
            }
            return response;
        }

    }
}