﻿using Cavipetrol.SICSES.Facade;
using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using Cavipetrol.SICSES.Servicios.Imagenes;
using static Cavipetrol.SICSES.Servicios.Imagenes.Imagenes;
using Cavipetrol.SICSES.Servicios.Autenticacion;
using System.Net.Http;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;
using Cavipetrol.SICSES.Infraestructura.Utilidades;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ConsultasController : ApiController
    {        
        private readonly IConsultasApoyoSICSESFacade consultasSICSESFacade;
        private readonly IMenuFecade menuFecade;
        private readonly ISolicitudCreditoFacade _solcitudCreditoFacade;

        private Cifrado.CifradoDescifrado _cifradoDescifrado;

        private static readonly log4net.ILog logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";
        public ConsultasController()
        {
            consultasSICSESFacade = new ConsultasApoyoSICSESFacade();
            menuFecade = new MenuFecade();
            _solcitudCreditoFacade = new SolicitudCreditosFacade();

            _cifradoDescifrado = new Cifrado.CifradoDescifrado();

            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }

        [HttpGet]
        [Route("api/DDL")]        
        public IHttpActionResult TestDLL()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio TestDLL()");

                var convertidor = new ConvertirAListaSeleccionable();
                var tipo1 = consultasSICSESFacade.ObtenerAsociacionesEntidadesSolidarias();
                var tipo2 = convertidor.Convertir(consultasSICSESFacade.ObtenerFormatosVigentesSICSES().ToList(), text: "CodigoSicses");
                var tipo3 = consultasSICSESFacade.ObtenerDepartamentos();
                var tipo4 = consultasSICSESFacade.ObtenerMunicipiosPorIdDepartamento("19");
                var tipo5 = consultasSICSESFacade.ObtenerNivelesEscolaridad();
                var tipo6 = consultasSICSESFacade.ObtenerTiposContratos();
                var tipo7 = consultasSICSESFacade.ObtenerTiposEstadosCiviles();
                var tipo8 = consultasSICSESFacade.ObtenerTiposEstratos();
                var tipo9 = consultasSICSESFacade.ObtenerTiposIdentificaciones();
                var tipo10 = consultasSICSESFacade.ObtenerTiposJornadasLaborales();
                var tipo11 = consultasSICSESFacade.ObtenerTiposOcupacion();
                var tipo12 = consultasSICSESFacade.ObtenerTiposOrganosDirectivosControl();
                var tipo13 = consultasSICSESFacade.ObtenerTiposRoles();
                var tipo14 = consultasSICSESFacade.ObtenerTiposSectorEconomico();
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                logger.Error("Usuario: " + this.usuario + " -> Error TestDLL() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/NiveldeEducacion")]
        public IHttpActionResult ObtenerNiveldeEducacion()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerNiveldeEducacion()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerNivelesEscolaridad().ToList());
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerNiveldeEducacion() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerDepartamentos")]
        public IHttpActionResult ObtenerDepartamentos()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerDepartamentos()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerDepartamentos().ToList(), id: "Id", text: "Nombre");

                string json = JsonConvert.SerializeObject(lista);

                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerDepartamentos() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerCiudadesPorIdDepto")]
        public IHttpActionResult ObtenerCiudadesPorIdDepto([FromUri] string IdDepartamento)
        {
            try
            {
                IdDepartamento = _cifradoDescifrado.decryptString(IdDepartamento);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCiudadesPorIdDepto(IdDepartamento: " + IdDepartamento + ")");
                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerMunicipiosPorIdDepartamento(IdDepartamento).ToList(), id: "Id", text: "Nombre");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCiudadesPorIdDepto() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTiposDocumento")]
        public IHttpActionResult ObtenerTiposDocumento()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposDocumento()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTiposIdentificaciones().ToList(), id: "Id", text: "Descripcion", idDos: "IdCavipetrol");

                return Ok(_cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCiudadesPorIdDepto() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerEntidades")]
        public IHttpActionResult ObtenerEntidades()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerEntidades()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerEntidades().ToList());
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerEntidades() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/SectorEconomico")]
        public IHttpActionResult ObtenerSectorEconomico()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerSectorEconomico()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTiposSectorEconomico().ToList(), id: "Id", text: "Nombre");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerSectorEconomico() -> " + ex.Message);
            }
            return Ok();
        }


        [HttpGet]
        [Route("api/Consultas/ClasificacionCiiu")]
        public IHttpActionResult ObtenerClasificacionCIUU()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerClasificacionCIUU()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerClasificacionCUU().OrderBy(x => x.Nombre).ToList(), id: "Id", text: "Nombre");

                string json = JsonConvert.SerializeObject(lista);
                //response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));


                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerClasificacionCIUU() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/Meses")]
        public IHttpActionResult ObtenerMeses()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerMeses()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerMeses().ToList(), id: "Id", text: "Mes");

                return Ok(_cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerMeses() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/Anos")]
        public IHttpActionResult ObtenerAnos()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerAnos()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerAnos().ToList(), id: "Id");

                return Ok(_cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAnos() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/InfoExcedentes")]
        public IHttpActionResult ObtenerInfoExcedentes()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerInfoExcedentes()");

                var lista = consultasSICSESFacade.ObtenerInfoExcedentes().ToList();
                string json = JsonConvert.SerializeObject(lista);

                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerInfoExcedentes() -> " + ex.Message);
            }
            return Ok();
        }


        [HttpGet]
        [Route("api/Consultas/InfoEvaluacionRiesgoLiquidez")]
        public IHttpActionResult ObtenerInfoEvaluacionRiesgoLiquidez()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerInfoEvaluacionRiesgoLiquidez()");

                var lista = consultasSICSESFacade.ObtenerInfoEvaluacionRiesgoLiquidez().ToList();
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerInfoEvaluacionRiesgoLiquidez() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTiposTitulos")]
        public IHttpActionResult ObtenerTiposTitulos()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposTitulos()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTiposTitulos().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposTitulos() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerTipoObligacion")]
        public IHttpActionResult ObtenerTipoObligacion()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoObligacion()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTipoOligacion().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoObligacion() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerTipoCuota")]
        public IHttpActionResult ObtenerTipoCuota()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoCuota()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTipoCuota().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoCuota() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerClaseGarantia")]
        public IHttpActionResult ObtenerClaseGarantia()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerClaseGarantia()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerClaseGarantia().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerClaseGarantia() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerDestinoCredito")]
        public IHttpActionResult ObtenerDestinoCredito()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerDestinoCredito()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerDestinoCredito().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerDestinoCredito() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerClaseVivienda")]
        public IHttpActionResult ObtenerClaseVivienda()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerClaseVivienda()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerClaseVivienda().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerClaseVivienda() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerCategoriaRestructurado")]
        public IHttpActionResult ObtenerCategoriaRestructurado()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCategoriaRestructurado()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerCategoriaRestructurado().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCategoriaRestructurado() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTipoEntidadBeneficiaria")]
        public IHttpActionResult ObtenerTipoEntidadBeneficiaria()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoEntidadBeneficiaria()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTipoEntidadBeneficiaria().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoEntidadBeneficiaria() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerTipoModalidad")]
        public IHttpActionResult ObtenerTipoModalidad()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoModalidad()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTipoModalidad().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoModalidad() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerTipoDestino")]
        public IHttpActionResult ObtenerTipoDestino()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoDestino()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTipoDestino().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoDestino() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerNivelEducacion")]
        public IHttpActionResult ObtenerNivelEducacion()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerNivelEducacion()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerNivelEducacion().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerNivelEducacion() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTiposBeneficiarios")]
        public IHttpActionResult ObtenerTiposBeneficiarios()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposBeneficiarios()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTiposBeneficiarios().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposBeneficiarios() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerAlternativasDecreto2880")]
        public IHttpActionResult ObtenerAlternativasDecreto2880()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerAlternativasDecreto2880()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerAlternativasDecreto2880().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAlternativasDecreto2880() -> " + ex.Message);
            }
            return Ok();
        }


        [HttpGet]
        [Route("api/Consultas/ObtenerClaseNaturaleza")]
        public IHttpActionResult ObtenerClaseNaturaleza()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerClaseNaturaleza()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerClaseNaturaleza().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerClaseNaturaleza() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerClaseEstadoProcesoActual")]
        public IHttpActionResult ObtenerClaseEstadoProcesoActual()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerClaseEstadoProcesoActual()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerClaseEstadoProcesoActual().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerClaseEstadoProcesoActual() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerClaseConcepto")]
        public IHttpActionResult ObtenerClaseConcepto()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerClaseConcepto()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerClaseConcepto().ToList(), id: "Id", text: "Descripcion");

                return Ok(_cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerClaseConcepto() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTipoUsuarioTieneAporte")]
        public IHttpActionResult ObtenerTipoUsuarioTieneAporte()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoUsuarioTieneAporte()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTipoUsuarioTieneAporte().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoUsuarioTieneAporte() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerClaseDeActivo")]
        public IHttpActionResult ObtenerClaseDeActivo()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerClaseDeActivo()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerClaseDeActivo().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerClaseDeActivo() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTiposLineaCredito")]
        public IHttpActionResult ObtenerTipoLineaCredito(string idTipoRol = null)
        {
            try
            {
                int tipoRol = 0;

                idTipoRol = _cifradoDescifrado.decryptString(idTipoRol);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoLineaCredito(idTipoRol: " + idTipoRol + ")");

                int.TryParse(idTipoRol, out tipoRol);

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTipoLineaCredito(tipoRol).ToList()
                                                  , id: "IdTipoLineaCredito", text: "Nombre");



                string json = JsonConvert.SerializeObject(lista);
                //response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));

                return Ok(_cifradoDescifrado.encryptString(json));
                //return Ok(lista);
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoLineaCredito() -> " + ex.Message);
            }
            return Ok();
        }


        [HttpGet]
        [Route("api/Consultas/ObtenerTiposCreditoTiposRol")]
        public IHttpActionResult ObtenerTiposCreditoTiposRol(string idPapelCavipetrol,
                                                             string idTipoLineaCredito, 
                                                             string fechaAfiliacion, 
                                                             string tipoDocumento, 
                                                             string numeroDocumento, 
                                                             string tiempoAfiliacion,
                                                             string salario)
        {
            try
            {
                double converTiempoAfiliacion = 0, converSalario = 0;

                idPapelCavipetrol = _cifradoDescifrado.decryptString(idPapelCavipetrol);
                idTipoLineaCredito = _cifradoDescifrado.decryptString(idTipoLineaCredito);
                fechaAfiliacion = _cifradoDescifrado.decryptString(fechaAfiliacion);
                tipoDocumento = _cifradoDescifrado.decryptString(tipoDocumento);
                numeroDocumento = _cifradoDescifrado.decryptString(numeroDocumento);
                tiempoAfiliacion = _cifradoDescifrado.decryptString(tiempoAfiliacion);
                salario = _cifradoDescifrado.decryptString(salario);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposCreditoTiposRol(idPapelCavipetrol: " + idPapelCavipetrol 
                    + ", idTipoLineaCredito: " + idTipoLineaCredito + ", fechaAfiliacion: " + fechaAfiliacion + ", tipoDocumento: " + tipoDocumento 
                    + ", numeroDocumento: " + numeroDocumento + ", tiempoAfiliacion: " + tiempoAfiliacion + ", salario: " + salario + ")");

                double.TryParse(tiempoAfiliacion, out converTiempoAfiliacion);
                var convertidor = new ConvertirAListaSeleccionable();
                short tipoRol = 0;
                int tipoLineaCredito = 0;
                short.TryParse(idPapelCavipetrol, out tipoRol);
                int.TryParse(idTipoLineaCredito, out tipoLineaCredito);
                DateTime dtFechaAfilicion;
                DateTime.TryParse(fechaAfiliacion, out dtFechaAfilicion);
                double.TryParse(salario, out converSalario);

                var lista = consultasSICSESFacade.ObtenerTiposCreditoTiposRolRelacion(tipoRol, tipoLineaCredito, dtFechaAfilicion, tipoDocumento, numeroDocumento, converTiempoAfiliacion, converSalario)
                                                  .ToList();

                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposCreditoTiposRol() -> " + ex.Message);
            }
            return Ok();

        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTiposCredito")]
        //public IHttpActionResult ObtenerTiposCredito(int idLineaCredito)
        public IHttpActionResult ObtenerTiposCredito(string idLineaCredito)
        {
            try
            {
                var convertidor = new ConvertirAListaSeleccionable();

                idLineaCredito = _cifradoDescifrado.decryptString(idLineaCredito);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposCredito(idLineaCredito: " + idLineaCredito + ")");

                int intIdLineaCredito = 0;
                int.TryParse(idLineaCredito, out intIdLineaCredito);

                var lista = convertidor.Convertir(consultasSICSESFacade
                    .ObtenerTipoCredito(intIdLineaCredito == 0? (int?)null: intIdLineaCredito)
                    .ToList(), id: "IdTipoCredito", text: "Nombre");

                string json = JsonConvert.SerializeObject(lista);

                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposCredito() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTipoTitulo")]
        public IHttpActionResult ObtenerTipoTitulo()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoTitulo()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTipoTitulo().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoTitulo() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/InfoReporteGrupoDeInteres")]
        public IHttpActionResult ObtenerInfoReporteGrupoDeInteres()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerInfoReporteGrupoDeInteres()");

                var lista = consultasSICSESFacade.ObtenerInfoReporteGrupoDeInteres().ToList();
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerInfoReporteGrupoDeInteres() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/FormCapacidadPagoGrupos")]
        //public IHttpActionResult ObtenerFormCapacidadPagoGrupos(int idTipoCreditoTipoRolRelacion)
        public IHttpActionResult ObtenerFormCapacidadPagoGrupos(string idTipoCreditoTipoRolRelacion)
        {
            try
            {
                idTipoCreditoTipoRolRelacion = _cifradoDescifrado.decryptString(idTipoCreditoTipoRolRelacion);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerFormCapacidadPagoGrupos(idTipoCreditoTipoRolRelacion: " + idTipoCreditoTipoRolRelacion + ")");

                int intIdTipoCreditoTipoRolRelacion = 0;
                int.TryParse(idTipoCreditoTipoRolRelacion, out intIdTipoCreditoTipoRolRelacion);

                var lista = consultasSICSESFacade.ObtenerFormCapacidadPagoGrupos(intIdTipoCreditoTipoRolRelacion).ToList();

                string json = JsonConvert.SerializeObject(lista);

                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormCapacidadPagoGrupos() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTipoTasa")]
        public IHttpActionResult ObtenerTipoTasa()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoTasa()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTipoTasa().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoTasa() -> " + ex.Message);
            }
            return Ok();
        }


        [HttpGet]
        [Route("api/Consultas/ObtenerTipoModalidadContratacion")]
        public IHttpActionResult ObtenerTipoModalidadContratacion()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoModalidadContratacion()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTipoModalidadContratacion().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoModalidadContratacion() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTipoProductoServicioContratado")]
        public IHttpActionResult ObtenerTipoProductoServicioContratado()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoProductoServicioContratado()");

                var convertidor = new ConvertirAListaSeleccionable();
                var Lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTipoProductoServicioContratado().ToList(), id: "Id", text: "Descripcion");
                string json = JsonConvert.SerializeObject(Lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoProductoServicioContratado() -> " + ex.Message);
            }
            return Ok();
        }        
        [HttpGet]
        [Route("api/Consultas/ObtenerEtiquetas")]
        public IHttpActionResult ObtenerEtiquetas(string idTipoEtiqueta)
        {
            try
            {
                idTipoEtiqueta = _cifradoDescifrado.decryptString(idTipoEtiqueta);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerEtiquetas(idTipoEtiqueta: " + idTipoEtiqueta + ")");

                int intIdTipoEtiqueta = 0;
                int.TryParse(idTipoEtiqueta, out intIdTipoEtiqueta);

                var lista = consultasSICSESFacade.ObtenerEtiquetas(intIdTipoEtiqueta).ToList();
                string json = JsonConvert.SerializeObject(lista);

                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerEtiquetas() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerTiposDocumentoRequisito")]
        public IHttpActionResult ObtenerTiposDocumentoRequisito()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposDocumentoRequisito()");

                var Lista = consultasSICSESFacade.ObtenerTiposDocumentoRequisito();
                string json = JsonConvert.SerializeObject(Lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposDocumentoRequisito() -> " + ex.Message);
            }
            return Ok();
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("api/Consultas/ObtenerPlantilla")]
        public IHttpActionResult ObtenerPlantilla(short idPlantilla, 
                                                  string datos, 
                                                  string tipo)
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerPlantilla(idPlantilla: " + idPlantilla + ", datos: " + datos + ", tipo: " + tipo + ")");

                JavaScriptSerializer serializador = new JavaScriptSerializer();
                List<Parametros> listaParametros = serializador.Deserialize<List<Parametros>>(datos ?? "");
                switch (tipo)
                {
                    case "plantilla":
                        string plantilla = consultasSICSESFacade.ObtenerPlantillaHtml(idPlantilla, (listaParametros ?? new List<Parametros>()));
                        return Ok(plantilla);
                    case "imagen":
                        List<Parametros> lstParametros = new List<Parametros>();
                        listaParametros = ObtenerImagenIndividual(lstParametros, idPlantilla);
                        return Ok(listaParametros);
                    default:
                        string planti = consultasSICSESFacade.ObtenerPlantillaHtml(idPlantilla, listaParametros);
                        planti = ObtenerImagen(planti, new enumImagenes[] { enumImagenes.LogoCavipetrol, enumImagenes.CiudadesCavipetrolVertical });
                        return Ok(planti);
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerPlantilla() -> " + ex.Message);
            }
            return Ok();
        }
        //[Authorize(Roles = "1")]
        [HttpGet]
        [Route("api/Consultas/ObtenerAsegurabilidadOpciones")]
        public IHttpActionResult ObtenerAsegurabilidadOpciones(string noEsAsegurable)
        {
            try
            {

                noEsAsegurable = _cifradoDescifrado.decryptString(noEsAsegurable);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerAsegurabilidadOpciones(noEsAsegurable: " + noEsAsegurable + ")");


                bool noAsegurable = true;
                bool.TryParse(noEsAsegurable, out noAsegurable);
                 var menu = consultasSICSESFacade.ObtenerAsegurabilidadOpciones(noEsAsegurable == null ? (bool?)null : noAsegurable);

                string json = JsonConvert.SerializeObject(menu);
                //response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));

                return Ok(_cifradoDescifrado.encryptString(json));
                //return Ok(menu);
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAsegurabilidadOpciones() -> " + ex.Message);
            }
            return Ok();
        }
        //[Authorize]
        [AllowAnonymous]
        [HttpGet]
        [Route("api/Consultas/ObtenerMenu")]
        public IHttpActionResult ObtenerMenu([FromUri] string strListaPerfiles) //int
        {
            try
            {
                strListaPerfiles = _cifradoDescifrado.decryptString(strListaPerfiles);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerMenu(idPerfil: " + strListaPerfiles + ")");
                
                List<Perfil> listaPerfiles = JsonConvert.DeserializeObject<List<Perfil>>(strListaPerfiles);
                
                var menu = menuFecade.ObtenerMenu(listaPerfiles);
                
                
                string json = JsonConvert.SerializeObject(menu);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerMenu() -> " + ex.Message);
            }
            return Ok();
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("api/Consultas/MenuCompleto")]
        public IHttpActionResult MenuCompleto()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio MenuCompleto()");

                var menu = menuFecade.MenuCompleto();
                string json = JsonConvert.SerializeObject(menu);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error MenuCompleto() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpPost]
        [Route("api/Consultas/ActualizarMenuPerfil")]
        public IHttpActionResult ActualizarMenuPerfil(ModeloString modeloString)
        {
            HttpResponseMessage response;
            ListaMenuPerfil perfilMenuRelacion = new ListaMenuPerfil();

            try
            {
                modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);
                perfilMenuRelacion = JsonConvert.DeserializeObject<ListaMenuPerfil>(modeloString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ActualizarMenuPerfil (usuario: " + modeloString.Valor + ")");

                string json = JsonConvert.SerializeObject(menuFecade.ActualizarMenuPerfil(perfilMenuRelacion));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ActualizarMenuPerfil() -> " + ex.Message);
                throw;
            }
            return null;
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerBimestre")]
        public IHttpActionResult ObtenerBimestre()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerBimestre()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerBimestre().ToList());
                return Ok(_cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerBimestre() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTiposGarantiasPagares")]
        public IHttpActionResult ObtenerTiposGarantiasPagares()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposGarantiasPagares()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTiposGarantiasPagares().ToList());
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposGarantiasPagares() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerCausaleNegacion")]
        public IHttpActionResult ObtenerCausalesNegacion()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCausalesNegacion()");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerCausalesNegacion().ToList(), "IdCausalNegacion", "Nombre");
                string json = JsonConvert.SerializeObject(lista);

                return Ok(_cifradoDescifrado.encryptString(json));                
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCausalesNegacion() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerUsosCredito")]
        public IHttpActionResult ObtenerUsosCredito(string idTipoLinea)
        {
            int idLinea = 0;
            try
            {
                idTipoLinea = _cifradoDescifrado.decryptString(idTipoLinea);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerUsosCredito(idTipoLinea: " + idTipoLinea + ")");

                int.TryParse(idTipoLinea, out idLinea);
                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerUsosCredito(idLinea).ToList(), "IdUsoCredito", "Nombre");
                string json = JsonConvert.SerializeObject(lista);

                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerUsosCredito() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerTiposGarantiasCredito")]
        public IHttpActionResult ObtenerTiposGarantiasCredito(string idTipoGarantiaCredito) //int
        {
            int intIdTipoGarantiaCredito = 0;
            try
            {
                idTipoGarantiaCredito = _cifradoDescifrado.decryptString(idTipoGarantiaCredito);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposGarantiasCredito(idTipoGarantiaCredito: " + idTipoGarantiaCredito + ")");

                int.TryParse(idTipoGarantiaCredito, out intIdTipoGarantiaCredito);

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(consultasSICSESFacade.ObtenerTiposGarantiasCredito(intIdTipoGarantiaCredito == 0 ? (int?)null : intIdTipoGarantiaCredito).ToList(), id: "IDTIPOGARANTIA", text: "NOMBRE_GARANTIA");
                string json = JsonConvert.SerializeObject(lista);
                return Ok(_cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposGarantiasCredito() -> " + ex.Message);
            }
            return Ok();
        }      
        [HttpGet]
        [Route("api/Consultas/ObtenerFormasPago")]
        public HttpResponseMessage ObtenerFormasPago(string idTipoCreditoTipoRolRelacion, string mesada)
        {
            HttpResponseMessage response;
            try
            {
                    
                mesada = _cifradoDescifrado.decryptString(mesada);
                idTipoCreditoTipoRolRelacion = _cifradoDescifrado.decryptString(idTipoCreditoTipoRolRelacion);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerFormasPago(idTipoCreditoTipoRolRelacion: " + idTipoCreditoTipoRolRelacion + ")");

                var formasPago = _solcitudCreditoFacade.ObtenerFormasPago(idTipoCreditoTipoRolRelacion, Boolean.Parse(mesada));
                string json = JsonConvert.SerializeObject(formasPago);

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormasPago() -> " + ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerSaldoAsegurabilidad")]
        public HttpResponseMessage ObtenerSaldoAsegurabilidad(string numeroIdentificacion)
        {
            HttpResponseMessage response;
            try
            {
                numeroIdentificacion = _cifradoDescifrado.decryptString(numeroIdentificacion);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerSaldoAsegurabilidad(numeroIdentificacion: " + numeroIdentificacion + ")");

                var formasPago = _solcitudCreditoFacade.ObtenerSaldoAseurabilidad(numeroIdentificacion);
                string json = JsonConvert.SerializeObject(formasPago);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerSaldoAsegurabilidad() -> " + ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerConceptosGarantia")]
        public HttpResponseMessage ObtenerConceptosGarantia(string idTipoGarantia) //int
        {
            HttpResponseMessage response;
            int intIdTipoGarantia = 0;
            try
            {
                idTipoGarantia = _cifradoDescifrado.decryptString(idTipoGarantia);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerConceptosGarantia(idTipoGarantia: " + idTipoGarantia + ")");

                int.TryParse(idTipoGarantia, out intIdTipoGarantia);

                var lstTiposGarantiaTiposConcepto = consultasSICSESFacade.ObtenerTiposGarantiaTiposConceptoRelacion(intIdTipoGarantia);
                string json = JsonConvert.SerializeObject(lstTiposGarantiaTiposConcepto);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerConceptosGarantia() -> " + ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerTiposCreditoTiposGarantiaRelacion")]
        public HttpResponseMessage ObtenerTiposCreditoTiposGarantiaRelacion(string idTipoCredito)
        {
            HttpResponseMessage response;
            try
            {
                idTipoCredito = _cifradoDescifrado.decryptString(idTipoCredito);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposCreditoTiposGarantiaRelacion(idTipoCredito: " + idTipoCredito + ")");

                short shortIdTipoCredito = 0;
                short.TryParse(idTipoCredito, out shortIdTipoCredito);
 
                var lst = consultasSICSESFacade.ObtenerTiposCreditoTiposGarantiaRelacion(shortIdTipoCredito);
                string json = JsonConvert.SerializeObject(lst);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposCreditoTiposGarantiaRelacion() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerCodeudorPorGarantia")]
        public HttpResponseMessage ObtenerCodeudorPorGarantia(string idTipoGarantia, string idTipoCodeudor, string valor)
        {
            HttpResponseMessage response;
            try
            {
                idTipoGarantia = _cifradoDescifrado.decryptString(idTipoGarantia);
                idTipoCodeudor = _cifradoDescifrado.decryptString(idTipoCodeudor);
                valor = _cifradoDescifrado.decryptString(valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCodeudorPorGarantia(idTipoGarantia: " + idTipoGarantia + ", idTipoCodeudor" + idTipoCodeudor + ", valor" + valor + ")");

                int respuesta = consultasSICSESFacade.ObtenerCodeudorPorGarantia(idTipoGarantia, idTipoCodeudor, valor);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCodeudorPorGarantia() -> " + ex.Message);
            }
            return response;
        }
        [HttpPost]
        [Route("api/Consultas/ValidarUsuarioCavipetrol")]
        public HttpResponseMessage ValidarUsuarioCavipetrol(string NumeroIdentificacion)
        {
            HttpResponseMessage reponse;
            try
            {
                NumeroIdentificacion = _cifradoDescifrado.decryptString(NumeroIdentificacion);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ValidarUsuarioCavipetrol(NumeroIdentificacion: " + NumeroIdentificacion + ")");

                reponse = Request.CreateResponse(HttpStatusCode.OK, consultasSICSESFacade.ValidarUsuarioCavipetrol(NumeroIdentificacion));
            }
            catch (Exception ex)
            {
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ValidarUsuarioCavipetrol() -> " + ex.Message);
            }
            return reponse;
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerConceptosAdjuntos")]
        public HttpResponseMessage ObtenerConceptosAdjuntos(string idconcepto)
        {
            HttpResponseMessage reponse;
            try
            {
                idconcepto = _cifradoDescifrado.decryptString(idconcepto);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerConceptosAdjuntos(idconcepto: " + idconcepto + ")");

                reponse = Request.CreateResponse(HttpStatusCode.OK, consultasSICSESFacade.ObtenerConceptosAdjuntos(idconcepto));
            }
            catch (Exception ex)
            {
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerConceptosAdjuntos() -> " + ex.Message);
            }
            return reponse;
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerConceptosTipoAdjunto")]
        public HttpResponseMessage ObtenerConceptosTipoAdjunto(string idTipoAdjunto)
        {
            HttpResponseMessage response;
            try
            {
                idTipoAdjunto = _cifradoDescifrado.decryptString(idTipoAdjunto);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerConceptosTipoAdjunto(idTipoAdjunto: " + idTipoAdjunto + ")");

                int intIdTipoAdjunto = 0;
                int.TryParse(idTipoAdjunto, out intIdTipoAdjunto);

                var lst = consultasSICSESFacade.ObtenerTiposConceptosTipoAdjunto(intIdTipoAdjunto);
                //string json = JsonConvert.SerializeObject(lst);
                //response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
                response = Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerConceptosTipoAdjunto() -> " + ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerTiposContratoSolictud")]
        public HttpResponseMessage ObtenerTiposContrato()
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTiposContrato()");

                var lst = consultasSICSESFacade.ObtenerTiposContrato();
                response = Request.CreateResponse(HttpStatusCode.OK, lst);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTiposContrato() -> " + ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerCiudadesServidor")]
        public HttpResponseMessage ObtenerCiudadesServidor()
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCiudadesServidor()");

                var lst = consultasSICSESFacade.ObtenerCiudadesServidor();
                string json = JsonConvert.SerializeObject(lst);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCiudadesServidor() -> " + ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerParametros")]
        public IHttpActionResult ObtenerParametros(string nombre)
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerSalarioMinimo()");

                nombre = _cifradoDescifrado.decryptString(nombre);
                var convertidor = new ConvertirAListaSeleccionable();
              
                
                return Ok(consultasSICSESFacade.ObtenerParametros(nombre).ToList());
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerEntidades() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerUsuariosPorPerfil")]
        public IHttpActionResult ObtenerUsuariosPorPerfil(string perfil)
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerUsuariosPorPerfil()");

                perfil = _cifradoDescifrado.decryptString(perfil);


                return Ok(consultasSICSESFacade.ObtenerUsuariosPorPerfil(perfil).ToList());
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerEntidades() -> " + ex.Message);
            }
            return Ok();
        }
        [HttpPost]
        [Route("api/Consultas/AsignarUsuarioSolicitud")]
        public HttpResponseMessage AsignarSolicitudUsuario(string idSolicitud,string idUsuario)
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio AsignarSolicitudUsuario()");

                int idSol = Convert.ToInt32(_cifradoDescifrado.decryptString(idSolicitud));
                int idUsu = Convert.ToInt32(_cifradoDescifrado.decryptString(idUsuario));


                response = Request.CreateResponse(HttpStatusCode.OK, consultasSICSESFacade.AsignarUsuarioSolicitud(idSol, idUsu));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                logger.Error("Usuario: " + this.usuario + " -> Error AsignarSolicitudUsuario() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Consultas/SolicitudDescripcionFondos")]

        public HttpResponseMessage SolicitudDescripcionFondos()
        {
            HttpResponseMessage response;

            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio SolicitudDescripcionFondos()");
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(consultasSICSESFacade.SolicitudDescripcionFondos())));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error SolicitudDescripcionFondos() -> " + ex.Message);
            }

            return response;
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerTasasSeguros")]

        public HttpResponseMessage ObtenerTasasSeguros(string edad, string idCredito)
        {
            HttpResponseMessage response;

            try
            {
                edad = _cifradoDescifrado.decryptString(edad);
                idCredito = _cifradoDescifrado.decryptString(idCredito);

                int intEdad = 0;
                int.TryParse(edad, out intEdad);

                short shortIdTipoCredito = 0;
                short.TryParse(idCredito, out shortIdTipoCredito);

                logger.Debug("ObtenerTasasSeguros: " + this.usuario + " -> Inicio ObtenerTasasSeguros()");
                var algo = consultasSICSESFacade.ObtenerSegurosTasas(intEdad, shortIdTipoCredito);
                response = Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(algo));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error SolicitudDescripcionFondos() -> " + ex.Message);
            }

            return response;
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerModelSolicitudCredito")]

        public HttpResponseMessage ObtenerModelSolicitudCredito(string idSolicitud)
        {
            HttpResponseMessage response;

            try
            {
                idSolicitud = _cifradoDescifrado.decryptString(idSolicitud);

           

                logger.Debug("ObtenerModelSolicitudCredito: " + this.usuario + " -> Inicio ObtenerModelSolicitudCredito()");
                var res = consultasSICSESFacade.ObtenerCartaSolicitud(idSolicitud);
                response = Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(res));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error SolicitudDescripcionFondos() -> " + ex.Message);
            }

            return response;
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerModelInterproductos")]

        public HttpResponseMessage ObtenerModelInterproductos(string idSolicitud)
        {
            HttpResponseMessage response;

            try
            {
                idSolicitud = _cifradoDescifrado.decryptString(idSolicitud);



                logger.Debug("ObtenerModelSolicitudCredito: " + this.usuario + " -> Inicio ObtenerModelSolicitudCredito()");
                var res = consultasSICSESFacade.ObtenerModelInterproductos(idSolicitud);
                response = Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(res));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error SolicitudDescripcionFondos() -> " + ex.Message);
            }

            return response;
        
        }
        
        [HttpGet]
        [Route("api/Consultas/ObtenerDatosCartaCondiciones")]
        public HttpResponseMessage ObtenerDatosCartaCondiciones(string idSolicitud)
        {
            HttpResponseMessage response;
            int intIdSolicitud;

            idSolicitud = _cifradoDescifrado.decryptString(idSolicitud);
            int.TryParse(idSolicitud, out intIdSolicitud);

            response = Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(consultasSICSESFacade.ObtenerDatosCartaCondiciones(intIdSolicitud)));
            return response;
        }
    }
}
