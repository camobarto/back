﻿using Cavipetrol.SICSES.Facade;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.Utilidades;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Cavipetrol.SICSES.BLL.Controllers;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class InformesController : ApiController
    {

        private readonly IInformesFacade _InformesFacade;
        private Cifrado.CifradoDescifrado _cifradoDescifrado;

        private static readonly log4net.ILog logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";
        public InformesController()
        {
            _InformesFacade = new InformesFacade();
            _cifradoDescifrado = new Cifrado.CifradoDescifrado();
            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }

        [HttpGet]
        [Route("api/Informes/ObtenerRetencionProveedore")]
        public HttpResponseMessage ObtenerRetencionProveedore(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "")
        {
            HttpResponseMessage response;
            try
            {
                IdOpcionInfo = _cifradoDescifrado.decryptString(IdOpcionInfo);
                Nit = _cifradoDescifrado.decryptString(Nit);
                Fc_Desde = _cifradoDescifrado.decryptString(Fc_Desde);
                Fc_Hasta = _cifradoDescifrado.decryptString(Fc_Hasta);
                Bimestre = _cifradoDescifrado.decryptString(Bimestre);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerRetencionProveedore(IdOpcionInfo: " + IdOpcionInfo + ", Nit: " + Nit + ", Fc_Desde: " + Fc_Desde + ", Fc_Hasta: " + Fc_Hasta + ", Bimestre: " + Bimestre + ")");

                switch (IdOpcionInfo)
                {
                    case "1":
                        var datosGarantia = _InformesFacade.ObtenerInformacionGarantia(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(datosGarantia)));
                        break;
                    case "2":
                        var Ica = _InformesFacade.ObtenerInformacionGarantia2(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta, Bimestre);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(Ica)));
                        break;
                    case "3":
                        var Iva = _InformesFacade.ObtenerInformacionGarantia3(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta, Bimestre);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(Iva)));
                        break;
                    default:
                        var DtaDefault = _InformesFacade.ObtenerInformacionGarantia("0", Nit, Fc_Desde, Fc_Hasta);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DtaDefault)));
                        break;
                }
                
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerRetencionProveedore() -> " + ex.Message);
            }
            return response;
        }


        [HttpPost]
        [Route("api/Informes/GuardarDatosGarantias")]
        //public HttpResponseMessage GuardaDatosPolizasExternas(DatosDocumento JsonObject)
        public HttpResponseMessage GuardaDatosPolizasExternas(ModeloCifrado StringCifrado)
         {
            Informes ObjInformesBLL = new Informes();
            HttpResponseMessage response;
            DatosDocumento JsonObject = new DatosDocumento();
            
            try
            {
                JsonObject = JsonConvert.DeserializeObject<DatosDocumento>(_cifradoDescifrado.decryptString(StringCifrado.Valor));
                List<DatosIva> lstDatos = new List<DatosIva>();

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardaDatosPolizasExternas(modelosString.Valor: " + StringCifrado.Valor + ")");

                foreach (var item in JsonObject.DatosExcel)
                {
                    if (!ObjInformesBLL.ValidaDatosExcel(item))
                    {
                        List<string> Mensaje = new List<string>();
                        Mensaje.Add("Error de Datos en el Archivo Excel Cargado, Tiene Celdas en Blanco, Por Favor Verifique");
                        return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(Mensaje)));
                    }
                    else
                    {
                        if (item[0].Length > 0)
                        {
                            DatosIva DatosExcel = new DatosIva();
                            DatosExcel.RET_TIPO_SOPORTE = item[0].Trim();
                            DatosExcel.RET_NUM_SOPORTE = Convert.ToInt32(item[1]);
                            DatosExcel.ASI_FECHA = Convert.ToDateTime(item[2].Trim());
                            DatosExcel.RET_CIUDAD = item[3];
                            DatosExcel.RET_CUENTA = item[4];
                            DatosExcel.NOMBRE_CUENTA = item[5];
                            DatosExcel.MVC_VALOR_DEBITO = Convert.ToDouble(item[6].Replace(",", ".").Replace("'", ".").Replace("$", "").Replace(" ", "").Trim());
                            DatosExcel.MVC_VALOR_CREDITO = Convert.ToDouble(item[7].Replace(",", ".").Replace("'", ".").Replace("$", "").Replace(" ", "").Trim());
                            DatosExcel.RET_TIPO_NIT = item[8];
                            DatosExcel.RET_NUM_NIT = item[9];

                            if (11 == item.Length)
                            {
                                DatosExcel.RET_PORCENTAJE = Convert.ToDouble(item[10].Replace(",", ".").Replace("'", ".").Replace("$", "").Replace(" ", "").Trim() == null ? 0 : Convert.ToDouble(item[10].Replace(",", ".").Replace("'", ".").Replace("$", "").Replace(" ", "").Trim()));

                            }
                            else if (12 == item.Length)
                            {
                                DatosExcel.RET_PORCENTAJE = Convert.ToDouble(item[10].Replace(",", ".").Replace("'", ".").Replace("$", "").Replace(" ", "").Trim() == null ? 0 : Convert.ToDouble(item[10].Replace(",", ".").Replace("'", ".").Replace("$", "").Replace(" ", "").Trim()));
                                DatosExcel.RET_BASE = Convert.ToDouble(item[11].Replace(",", ".").Replace("'", ".").Replace("$", "").Replace(" ", "").Trim() == null ? 0 : Convert.ToDouble(item[11].Replace(",", ".").Replace("'", ".").Replace("$", "").Replace(" ", "").Trim()));
                            }

                            if (JsonObject.tipo == "1")
                            {
                                DatosExcel.TIPO = "Retención en la fuente";
                            }
                            else if (JsonObject.tipo == "2")
                            {
                                DatosExcel.TIPO = "ICA";
                            }
                            else if (JsonObject.tipo == "3")
                            {
                                DatosExcel.TIPO = "IVA";
                            }

                            lstDatos.Add(DatosExcel);
                        }
                    }
                }
                 return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(_InformesFacade.InsertarDatosGarantias(lstDatos))));
            }

            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
                List<string> MensajeDuplicado = new List<string>();
                MensajeDuplicado.Add("Error de Datos en el Archivo Excel Cargado, Tiene Datos Repetidos, Por Favor Verifique ");
                return response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(MensajeDuplicado)));
            }
        }


        [HttpGet]
        [Route("api/Informes/ActualizaInfoProveedores")]
        public HttpResponseMessage ActualizaInfoProveedores()
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ActualizaInfoProveedores()");

                var datosProveedores = _InformesFacade.ActualizaInfoProveedores(5);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(datosProveedores)));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ActualizaInfoProveedores() -> " + ex.Message);
            }
            return response;
        }


        [HttpGet]
        [Route("api/Informes/GeneraProveedoresFaltantes")]
        public HttpResponseMessage GeneraProveedoresFaltantes()
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio GeneraProveedoresFaltantes()");

                var datosProveedores = _InformesFacade.GeneraProveedoresFaltantes(4);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(datosProveedores)));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error GeneraProveedoresFaltantes() -> " + ex.Message);
            }
            return response;
        }
        
        [HttpGet]
        [Route("api/Informes/ObtenerCuentaContable")]
        //public IHttpActionResult ObtenerCuentaContable(int bandera)
        public IHttpActionResult ObtenerCuentaContable(string bandera)
        {
            try
            {
                int intBandera = int.Parse(_cifradoDescifrado.decryptString(bandera));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCuentaContable(bandera: " + bandera + ")");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = convertidor.Convertir(_InformesFacade.ObtenerCuentaContable(intBandera == 0 ? 1 : intBandera).ToList(), id: "cuc_codigo", text: "cuc_descripcion");
                return Ok(_cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch(Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCuentaContable() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Informes/ConsultaAuxTercero")]
        public HttpResponseMessage ConsultaAuxTercero(string mes, string anio, string cuenta)
        {
            HttpResponseMessage response;
            try
            {
                mes = _cifradoDescifrado.decryptString(mes);
                anio = _cifradoDescifrado.decryptString(anio);
                cuenta = _cifradoDescifrado.decryptString(cuenta);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsultaAuxTercero(mes: " + mes + ", año: " + anio + ", cuenta: " + cuenta + ")");

                var DatosAuxTercero = _InformesFacade.ConsultaAuxTercero(mes, anio, cuenta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DatosAuxTercero)));
            }
            catch(Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ConsultaAuxTercero() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Informes/ConsultaAuditaCuenta")]
        public HttpResponseMessage ConsultaAuditaCuenta(string Fc_Ini, string Fc_Fin, string Cuenta)
        {
            HttpResponseMessage response;
            try
            {
                Fc_Ini = _cifradoDescifrado.decryptString(Fc_Ini);
                Fc_Fin = _cifradoDescifrado.decryptString(Fc_Fin);
                Cuenta = _cifradoDescifrado.decryptString(Cuenta);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsultaAuditaCuenta(Fc_Ini: " + Fc_Ini + ", Fc_Fin: " + Fc_Fin + ", cuenta: " + Cuenta + ")");

                var DatosAditaCuenta = _InformesFacade.ConsultaAuditaCuenta(Fc_Ini, Fc_Fin, Cuenta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DatosAditaCuenta)));
            }
            catch(Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ConsultaAuditaCuenta() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Informes/ConsutaGMF")]
        public HttpResponseMessage ConsutaGMF(string Fc_Ini, string Fc_Fin, string Bandera)
        {
            
            HttpResponseMessage response;
            try
            {
                Fc_Ini = _cifradoDescifrado.decryptString(Fc_Ini);
                Fc_Fin = _cifradoDescifrado.decryptString(Fc_Fin);
                int intBandera = int.Parse(_cifradoDescifrado.decryptString(Bandera));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsutaGMF(Fc_Ini: " + Fc_Ini + ", Fc_Fin: " + Fc_Fin + ", Bandera: " + Bandera + ")");

                switch (intBandera)
                {
                    case 1:
                        var DatosGMF_BUno = _InformesFacade.ConsutaGMF_BUno(Fc_Ini, Fc_Fin, intBandera);
                        
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DatosGMF_BUno)));
                        break;
                    case 2:
                        var DatosGMF_BDos = _InformesFacade.ConsutaGMF_BDos(Fc_Ini, Fc_Fin, intBandera);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DatosGMF_BDos)));
                        break;
                    case 3:
                        var DatosGMF_BTres = _InformesFacade.ConsutaGMF_BTres(Fc_Ini, Fc_Fin, intBandera);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(DatosGMF_BTres)));
                        break;
                    default:
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString("No se selecciono una opcion de informe"));
                        break;
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ConsutaGMF() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Informes/InformeCarteraEcopetrol")]
        //public HttpResponseMessage ObtenerInformeOpcion19Ecopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        public HttpResponseMessage ObtenerInformeOpcion19Ecopetrol(string TipoNit, string NumeroIdentificacion,
                                                                    string producto, string Doctipo, string Docunumero,
                                                                    string FechaDesde, string FechaHasta, string Bandera)
        {
            HttpResponseMessage response;
            try
            {
                TipoNit = _cifradoDescifrado.decryptString(TipoNit);
                NumeroIdentificacion = _cifradoDescifrado.decryptString(NumeroIdentificacion);
                producto = _cifradoDescifrado.decryptString(producto);
                Doctipo = _cifradoDescifrado.decryptString(Doctipo);
                int intDocunumero = int.Parse(_cifradoDescifrado.decryptString(Docunumero));
                FechaDesde = _cifradoDescifrado.decryptString(FechaDesde);
                FechaHasta = _cifradoDescifrado.decryptString(FechaHasta);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerInformeOpcion19Ecopetrol(" +
                "TipoNit: " + TipoNit + ", NumeroIdentificacion: " + NumeroIdentificacion + 
                ", producto: " + producto + ", Doctipo: " + Doctipo + ", Docunumero: " + Docunumero + 
                ", FechaDesde: " + FechaDesde + ", FechaHasta: " + FechaHasta + ", Bandera: " + Bandera + ")");

                var SaldoInicial = _InformesFacade.ObtenerSaldoInicialInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, intDocunumero, FechaDesde, FechaHasta, 1);
                var Encabezado = _InformesFacade.ObtenerEncabezadoInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, intDocunumero, FechaDesde, FechaHasta, 2);
                var PlanPagos = _InformesFacade.ObtenerPlanPagosInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, intDocunumero, FechaDesde, FechaHasta, 3);
                var Movimientos = _InformesFacade.ObtenerMovimientosInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, intDocunumero, FechaDesde, FechaHasta, 4);

                var informe = new InformeCarteraEcopetrol();

                informe.SaldoInicial = SaldoInicial.ToList();
                informe.Encabezado = Encabezado.ToList();
                informe.Planpagos = PlanPagos.ToList();
                informe.Movimientos = Movimientos.ToList();

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(informe)));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerInformeOpcion19Ecopetrol() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Informes/TipoDocumentoPorCedula")]
        //public HttpResponseMessage ObtenerTipoDocumentoPorCedula(string TipoIdentificacion, int Cedula)
        public HttpResponseMessage ObtenerTipoDocumentoPorCedula(string TipoIdentificacion, string Cedula)
        {
            HttpResponseMessage response;
            try
            {
                //TODO: Obtener parámetro para adquirir el informe histórico
                TipoIdentificacion = _cifradoDescifrado.decryptString(TipoIdentificacion);
                int intCedula = int.Parse(_cifradoDescifrado.decryptString(Cedula));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoDocumentoPorCedula(TipoIdentificacion: " + TipoIdentificacion + ", Cedula: " + Cedula + ")");

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(_InformesFacade.ObtenerTipoDocumentoPorCedula(TipoIdentificacion, intCedula))));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoDocumentoPorCedula() -> " + ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("api/Informes/ObtenerFacturaelectronica")]
        //public HttpResponseMessage ObtenerTipoDocumentoPorCedula(string TipoIdentificacion, int Cedula)
        public HttpResponseMessage ObtenerFacturaElectronica(string Factura, string Servidor)
        {
            HttpResponseMessage response;
            try
            {
                //TODO: Obtener parámetro para adquirir el informe histórico
                Factura = _cifradoDescifrado.decryptString(Factura);
                Servidor = _cifradoDescifrado.decryptString(Servidor);

                logger.Debug("Factura: " + this.usuario + " -> Inicio ObtenerFacturaelectronica(TipoIdentificacion: " + Factura + ", Servidor: " + Servidor + ")");

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(_InformesFacade.ObtenerFacturaElectronica(Factura, Servidor))));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFacturaelectronica() -> " + ex.Message);
            }
            return response;
        }

        [HttpPost]
        [Route("api/Informes/GenerarNota")]
        //public HttpResponseMessage ObtenerTipoDocumentoPorCedula(string TipoIdentificacion, int Cedula)
        public HttpResponseMessage GenerarNota(ModeloString modelosString)
        {
            HttpResponseMessage response;
            try
            {
                //TODO: Obtener parámetro para adquirir el informe histórico                
                modelosString.Valor = _cifradoDescifrado.decryptString(modelosString.Valor);
                logger.Debug("Factura: " + this.usuario + " -> Inicio GenerarNota(nota: " + modelosString.Valor + ")");
                Nota notaRecibida = new Nota();
                notaRecibida = JsonConvert.DeserializeObject<Nota>(modelosString.Valor);                
                var rta = _InformesFacade.GuardarNota(notaRecibida);
                rta.Respuesta = rta.Respuesta;                               
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(rta)));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFacturaelectronica() -> " + ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("api/Informes/ObtenerIngresosyRetenciones")]
        //public HttpResponseMessage ObtenerTipoDocumentoPorCedula(string TipoIdentificacion, int Cedula)
        public HttpResponseMessage ObtenerIngresosyRetenciones(string NumeroIdentificacion, string ano, string mes)
        {
            HttpResponseMessage response;
            try
            {
                int intMes = 0;
                int intAno = 0;
                //TODO: Obtener parámetro para adquirir el informe histórico
                NumeroIdentificacion = _cifradoDescifrado.decryptString(NumeroIdentificacion);
                ano = _cifradoDescifrado.decryptString(ano);
                mes = _cifradoDescifrado.decryptString(mes);

                int.TryParse(ano, out intAno);
                int.TryParse(mes, out intMes);

                logger.Debug("Factura: " + this.usuario + " -> Inicio Certificaciones(NumeroIdentificacion: " + NumeroIdentificacion + ", ano: " + ano + ",mes: "+ mes + ")");

				var certificacion = _InformesFacade.Certificaciones(NumeroIdentificacion, intAno, intMes);
				var respuesta = JsonConvert.SerializeObject(certificacion);
				response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(respuesta));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerIngresosyRetenciones() -> " + ex.Message);
            }
            return response;
        }

    }


}
