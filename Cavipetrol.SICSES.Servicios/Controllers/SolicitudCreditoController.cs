﻿using Cavipetrol.SICSES.DAL;
using Cavipetrol.SICSES.Facade;
using Cavipetrol.SICSES.BLL.Controllers;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.PreaprobacionVehiculo;
using Cavipetrol.SICSES.Infraestructura.Model.Garantias;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.Utilidades;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using Cavipetrol.SICSES.BLL;
using Cavipetrol.SICSES.Servicios.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.Creditos;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
    //[Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SolicitudCreditoController : ApiController
    {
        // prueba
        private readonly ISolicitudCreditoFacade _solcitudCreditoFacade;
        private readonly SolicitudCreditoNegocio _consultasApoyo;

        private static readonly log4net.ILog logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";

        //CLASE PARA CIFRADO DE PARAMETROS
        private Cifrado.CifradoDescifrado _cifradoDesifrado;

        public SolicitudCreditoController()
        {
            _consultasApoyo = new SolicitudCreditoNegocio();
            _solcitudCreditoFacade = new SolicitudCreditosFacade(); 
            _cifradoDesifrado = new Cifrado.CifradoDescifrado();
            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }

        [HttpPost]
        [Route("api/SolicitudCreditos/GestionPrendaVehicular")]
        public HttpResponseMessage GestionPrendaVehicular(ModeloCifrado StringCifrado)
        {
            HttpResponseMessage response;
            PrendaVehicular[] JsonObject;
            try
            {
                string des = _cifradoDesifrado.decryptString(StringCifrado.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GestionPrendaVehicular(ModeloString: " + StringCifrado.Valor + " )");

                JsonObject = JsonConvert.DeserializeObject<PrendaVehicular[]>(des, new JsonSerializerSettings());
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(JsonConvert.SerializeObject(_consultasApoyo.GestionPrendaVehicular(JsonObject))));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDesifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error GestionPrendaVehicular() -> " + ex.Message);
            }

            return response;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerDatosCampaniaVehiculo")]
        public HttpResponseMessage ObtenerDatosCampaniaVehiculo(string numeroIdentificacion, string tipoIdentificacion, string TipoVehiculo)
        {
            HttpResponseMessage response;
            try
            {
                numeroIdentificacion = _cifradoDesifrado.decryptString(numeroIdentificacion);
                tipoIdentificacion = _cifradoDesifrado.decryptString(tipoIdentificacion);
                TipoVehiculo = _cifradoDesifrado.decryptString(TipoVehiculo);
                int TipoVeh = int.Parse(TipoVehiculo);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerDatosCampaniaVehiculo (numeroIdentificacion:" + numeroIdentificacion
                    + ", tipoIdentificacion: " + tipoIdentificacion + ")");

                response = Request.CreateResponse(HttpStatusCode.OK, _consultasApoyo.ObtenerDatosCampaniaVehiculo(numeroIdentificacion, tipoIdentificacion, TipoVeh));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerDatosCampaniaVehiculo() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }

            return response;
        }
        [HttpPost]
        [Route("api/SolicitudCreditos/AdministrarPagares")]
        public HttpResponseMessage AdministrarPagares(ModeloCifrado StringCifrado)
        {
            HttpResponseMessage response;
            PagaresModel JsonObject = new PagaresModel();
            try
            {
                string des = _cifradoDesifrado.decryptString(StringCifrado.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio AdministrarPagares(ModeloString: " + StringCifrado.Valor + " )");

                JsonObject = JsonConvert.DeserializeObject<PagaresModel>(des, new JsonSerializerSettings());
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(JsonConvert.SerializeObject(_consultasApoyo.AdministrarPagares(JsonObject))));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDesifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error AdministrarPagares() -> " + ex.Message);
            }

            return response;
        }
        [HttpPost]
        [Route("api/SolicitudCreditos/GestionNormalizado")]
        public HttpResponseMessage GestionNormalizado(ModeloCifrado StringCifrado)
        {
            HttpResponseMessage response;
            NormalizadoConsulta JsonObject = new NormalizadoConsulta();
            try
            {
                string des = _cifradoDesifrado.decryptString(StringCifrado.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GestionNormalizado(ModeloString: " + StringCifrado.Valor + " )");

                JsonObject = JsonConvert.DeserializeObject<NormalizadoConsulta>(des, new JsonSerializerSettings());
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(JsonConvert.SerializeObject(_consultasApoyo.GestionNormalizado(JsonObject))));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDesifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error GestionNormalizado() -> " + ex.Message);
            }

            return response;
        }
        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarDatosPreaprobacion")]
        public HttpResponseMessage GuardarDatosPreaprobacion(ModeloCifrado StringCifrado)
        {
            HttpResponseMessage response;
            PreaprobadoCarta JsonObject = new PreaprobadoCarta();
            try
            {
                string des = _cifradoDesifrado.decryptString(StringCifrado.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarDatosPreaprobacion(ModeloString: " + StringCifrado.Valor + ")");

                JsonObject = JsonConvert.DeserializeObject<PreaprobadoCarta>(des, new JsonSerializerSettings { DateFormatString = "yyyy-MM-ddTH:mm:ss.fffZ", DateTimeZoneHandling = DateTimeZoneHandling.Utc });
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(JsonConvert.SerializeObject(_consultasApoyo.GuardarDatosPreaprobacion(JsonObject))));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDesifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarDatosPreaprobacion() -> " + ex.Message);
            }

            return response;
        }
        //Reporte Saldos Diarios
        [HttpGet]
        [Route("api/SolicitudCredito/ReporteSaldosDiarios")]
        public HttpResponseMessage ReporteSaldosDiarios(string selectedFechaInicio, string selectedFechaFin )
        {
            HttpResponseMessage response;
            try
            {
                selectedFechaInicio = _cifradoDesifrado.decryptString(selectedFechaInicio);
                selectedFechaFin = _cifradoDesifrado.decryptString(selectedFechaFin);


                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerReportes (selectedFechaInicio:" + selectedFechaInicio
                + ", selectedFechaFin: " + selectedFechaFin + ")");

                var Reportes = _consultasApoyo.ReporteSaldosDiarios(selectedFechaInicio, selectedFechaFin);

                string json = new JavaScriptSerializer().Serialize(Reportes);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));              
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerReporte() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }

            return response;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerInformacionEmpleado")]
        public HttpResponseMessage ConsultaInformacionEmpleado([FromUri] string numeroIdentificacion, [FromUri] string tipoIdentificacion)
        {
            //var usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User).Identity).NameClaimType;
            HttpResponseMessage response;
            try
            {
                numeroIdentificacion = _cifradoDesifrado.decryptString(numeroIdentificacion);
                tipoIdentificacion = _cifradoDesifrado.decryptString(tipoIdentificacion);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsultaInformacionEmpleado (numeroIdentificacion:" + numeroIdentificacion
                    + ", tipoIdentificacion: " + tipoIdentificacion + ")");

                var respuestaNegocio = _solcitudCreditoFacade.ObtenerInformacionEmpleado(numeroIdentificacion, tipoIdentificacion);
                if (respuestaNegocio.Estado)
                {
                    InformacionSolicitanteGeneralAsegurabilidad viewModel = new InformacionSolicitanteGeneralAsegurabilidad();
                    viewModel.MappingInformacionSolicitante(respuestaNegocio.Respuesta);
                    //var respuestaAsegurabilidad = _solcitudCreditoFacade.ObtenerInformacionAsegurabilidad(numeroIdentificacion, tipoIdentificacion);
                    //if (respuestaAsegurabilidad.Estado)
                    //{
                    //viewModel.MappingInformacionAsegurabilidad(respuestaAsegurabilidad.Respuesta);
                    string json = JsonConvert.SerializeObject(viewModel);
                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
                    //}
                    //else
                    //{
                    //    response = Request.CreateResponse(HttpStatusCode.BadRequest, respuestaAsegurabilidad.MensajesError);
                    //}
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, respuestaNegocio.MensajesError);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ConsultaInformacionEmpleado() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }

            return response;
        }       
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerSaldoProductos")]
        public HttpResponseMessage ObtenerSaldoProductos([FromUri] string numeroIdentificacion, [FromUri] string tipoIdentificacion, [FromUri] string papel)
        {
            HttpResponseMessage response;
            try
            {
                numeroIdentificacion = _cifradoDesifrado.decryptString(numeroIdentificacion);
                tipoIdentificacion = _cifradoDesifrado.decryptString(tipoIdentificacion);
                papel = _cifradoDesifrado.decryptString(papel);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerProductosDisponibles (numeroIdentificacion:" + numeroIdentificacion
                + ", tipoIdentificacion: " + tipoIdentificacion + ", papel: " + papel + ")");

                var listaProductos = _solcitudCreditoFacade.ObtenerCreditosDisponiblesSaldo(numeroIdentificacion, tipoIdentificacion, papel).ToList();
                
                string json = new JavaScriptSerializer().Serialize(listaProductos);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));

                //response = Request.CreateResponse(HttpStatusCode.OK, listaSeleccionable);<
            }
            catch (Exception ex) 
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerProductosDisponibles() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }

            return response;
        }

        //[HttpGet]
        //[Route("api/SolicitudCreditos/ValidacionProductos")]
        //public HttpResponseMessage ValidacionProducto([FromUri] string numeroIdentificacion, [FromUri] string tipoIdentificacion, [FromUri] string papel, [FromUri] string producto)
        //{
        //    logger.Debug("Inicio ValidacionProducto(numeroIdentificacion:" + numeroIdentificacion
        //        + ", tipoIdentificacion: " + tipoIdentificacion + ", papel: " + papel + ", producto: " + producto + ")");
        //    HttpResponseMessage response;
        //    try
        //    {
        //        var validaciones = _solcitudCreditoFacade.ValidacionesReestriccionesCreditos(numeroIdentificacion, tipoIdentificacion, papel, producto);
        //        if (validaciones.Estado)
        //            response = Request.CreateResponse(HttpStatusCode.OK, validaciones.Estado);
        //        else
        //            response = Request.CreateResponse(HttpStatusCode.BadRequest, validaciones.MensajesError);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error("Error ValidacionProducto() -> " + ex.Message);
        //        response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
        //    }

        //    return response;
        //}

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerNormasTipoCredito")]
        public HttpResponseMessage ObtenerNormas(string papel, string producto, string idTipoCredito)
        {
            HttpResponseMessage response;
            try
            {
                papel = _cifradoDesifrado.decryptString(papel);
                producto = _cifradoDesifrado.decryptString(producto);
                idTipoCredito = _cifradoDesifrado.decryptString(idTipoCredito);

                short shortIdTipoCredito = 0;
                short.TryParse(idTipoCredito, out shortIdTipoCredito);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerNormas(papel: " + papel + ", producto: " + producto + ")");

                IEnumerable<NormaProductos> respuesta = _solcitudCreditoFacade.ObtenerNormas(producto, papel, shortIdTipoCredito).ToList();
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerNormas() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }

            return response;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerDetalleNorma")]
        public HttpResponseMessage ObtenerDetalleNorma([FromUri] string producto, [FromUri] string norma)
        {
            HttpResponseMessage response;
            producto = _cifradoDesifrado.decryptString(producto);
            norma = _cifradoDesifrado.decryptString(norma);
            try
            {
                producto = _cifradoDesifrado.decryptString(producto);
                norma = _cifradoDesifrado.decryptString(norma);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerDetalleNorma("
                    + "producto: " + producto
                    + ", norma: " + norma + ")");

                var modalidades = _solcitudCreditoFacade.ObtenerModalidades(producto, norma);
                string json = JsonConvert.SerializeObject(modalidades);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerDetalleNorma() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }

            return response;
        }
        [HttpPost]
        [Route("api/SolicitudCreditos/CalcularCapacidadPagoGrupos")]
        public HttpResponseMessage CalcularCapacidadPagoGrupos(ModeloString modelosString, //List<ViewModelFormCapacidadPagoGrupos>
                                                               string idTipoCreditoTipoRolRelacion,
                                                               string idTipoIdentificacion,
                                                               string numeroIdentificacion,
                                                               string valorNegociado,
                                                               string valorTotalInmueble,
                                                               string idTipoPolizaSeleccionada,
                                                               string edad, //byte
                                                               string PorcenExtraPrima, //double 
                                                               string tipoCapacidadPago )
        {
            HttpResponseMessage response;
            byte byteEdad = 0;
            double doublePorcenExtraPrima = 0;
            List<ViewModelFormCapacidadPagoGrupos> dtoViewModellistaFormCapacidadPagoGrupos = new List<ViewModelFormCapacidadPagoGrupos>();

            try
            {
                modelosString.Valor = _cifradoDesifrado.decryptString(modelosString.Valor); //ViewModellistaFormCapacidadPagoGrupos
                idTipoCreditoTipoRolRelacion = _cifradoDesifrado.decryptString(idTipoCreditoTipoRolRelacion);
                idTipoIdentificacion = _cifradoDesifrado.decryptString(idTipoIdentificacion);
                numeroIdentificacion = _cifradoDesifrado.decryptString(numeroIdentificacion);
                valorNegociado = _cifradoDesifrado.decryptString(valorNegociado);
                valorTotalInmueble = _cifradoDesifrado.decryptString(valorTotalInmueble);
                idTipoPolizaSeleccionada = _cifradoDesifrado.decryptString(idTipoPolizaSeleccionada);
                edad = _cifradoDesifrado.decryptString(edad); //byte
                PorcenExtraPrima = _cifradoDesifrado.decryptString(PorcenExtraPrima); //double
                tipoCapacidadPago = _cifradoDesifrado.decryptString(tipoCapacidadPago);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio CalcularCapacidadPagoGrupos( " +
                    "modelosString.Valor: " + modelosString.Valor
                + ", idTipoCreditoTipoRolRelacion: " + idTipoCreditoTipoRolRelacion
                + ", idTipoIdentificacion: " + idTipoIdentificacion
                + ", numeroIdentificacion: " + numeroIdentificacion
                + ", valorNegociado: " + valorNegociado
                + ", valorTotalInmueble: " + valorTotalInmueble
                + ", idTipoPolizaSeleccionada: " + idTipoPolizaSeleccionada
                + ", edad: " + edad
                + ", PorcenExtraPrima: " + PorcenExtraPrima + ")");

                byteEdad = Byte.Parse(edad);
                doublePorcenExtraPrima = Double.Parse(PorcenExtraPrima);
                dtoViewModellistaFormCapacidadPagoGrupos = JsonConvert.DeserializeObject<List<ViewModelFormCapacidadPagoGrupos>>(modelosString.Valor.Replace("00nullnullnull", "0").Replace("0NaN0", "0"));


                List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos = new List<FormCapacidadPagoGrupos>();
                listaFormCapacidadPagoGrupos = dtoViewModellistaFormCapacidadPagoGrupos.Select(post => new FormCapacidadPagoGrupos {
                    IdGrupo = post.IdGrupo,
                    ListaFormCapacidadPago = post.ListaFormCapacidadPago,
                    Mostrar = post.Mostrar,
                    MostrarTotal = post.MostrarTotal,
                    Nombre = post.Nombre,
                    Orden = post.Orden,
                    Total = post.Total
                }).ToList();
                short idTipoPoliza = 0;
                short.TryParse(idTipoPolizaSeleccionada, out idTipoPoliza);
                double valorInmueble = 0;
                double.TryParse(valorTotalInmueble, out valorInmueble);
                double valorCredito = 0;
                double.TryParse(valorNegociado, out valorCredito);
                List<Amortizacion> tablaAmortizacion = new List<Amortizacion>();
                List<TiposSeguro> tablaSeguros = new List<TiposSeguro>();
                var calculoCapacidadPago = _solcitudCreditoFacade.CalcularCapacidadPago(listaFormCapacidadPagoGrupos,
                    idTipoCreditoTipoRolRelacion, ref tablaAmortizacion, valorInmueble, idTipoPoliza, valorCredito,
                    ref tablaSeguros, byteEdad, doublePorcenExtraPrima, tipoCapacidadPago);

                calculoCapacidadPago.Respuesta.ListaFormCapacidadPagoGrupos
                    .Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Ingresos)
                    .SingleOrDefault().HorasExtras = dtoViewModellistaFormCapacidadPagoGrupos
                    .Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Ingresos).SingleOrDefault().HorasExtras;
                calculoCapacidadPago.Respuesta.TablaSeguros = tablaSeguros;

                string json = JsonConvert.SerializeObject(calculoCapacidadPago);
                response = Request.CreateResponse(HttpStatusCode.OK, json);
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error CalcularCapacidadPagoGrupos() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return response;
        }


        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarSolicitudCredito")]
        public HttpResponseMessage GuardarSolicitudCredito(ModeloString modeloString, //List<FormCapacidadPagoGrupos>
                                                           string idTipoCreditoTipoRolRelacion,
                                                           string idTipoIdentificacion,
                                                           string numeroIdentificacion,
                                                           string valorNegociado,
                                                           string cuentaFai,
                                                           string girarCheque,
                                                           string cruceInterproductos,
                                                           string saldoAnterior,
                                                           string idAsegurabilidadOpcion,
                                                           string idUsoCredito,
                                                           string idOficina,
                                                           string UserLog,
                                                           string valorInmueble,
                                                           string idTipoGarantia,
                                                           string fscNumSolicitud,
                                                           string IdCiuuFomento,
                                                           string CursoFodes,
                                                           string observacionesSolicitud,
                                                           string tipoCapacidadPago)
        {

            HttpResponseMessage response;
            try
            {

                JavaScriptSerializer serializador = new JavaScriptSerializer();

                //DESCIFRADOS
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                idTipoCreditoTipoRolRelacion = _cifradoDesifrado.decryptString(idTipoCreditoTipoRolRelacion);
                idTipoIdentificacion = _cifradoDesifrado.decryptString(idTipoIdentificacion);
                numeroIdentificacion = _cifradoDesifrado.decryptString(numeroIdentificacion);
                valorNegociado = _cifradoDesifrado.decryptString(valorNegociado);
                cuentaFai = _cifradoDesifrado.decryptString(cuentaFai);
                girarCheque = _cifradoDesifrado.decryptString(girarCheque);
                cruceInterproductos = _cifradoDesifrado.decryptString(cruceInterproductos);
                saldoAnterior = _cifradoDesifrado.decryptString(saldoAnterior);
                idAsegurabilidadOpcion = _cifradoDesifrado.decryptString(idAsegurabilidadOpcion);
                idUsoCredito = _cifradoDesifrado.decryptString(idUsoCredito);
                idOficina = _cifradoDesifrado.decryptString(idOficina);
                UserLog = _cifradoDesifrado.decryptString(UserLog);
                valorInmueble = _cifradoDesifrado.decryptString(valorInmueble);
                idTipoGarantia = _cifradoDesifrado.decryptString(idTipoGarantia);
                fscNumSolicitud = _cifradoDesifrado.decryptString(fscNumSolicitud);
                IdCiuuFomento = _cifradoDesifrado.decryptString(IdCiuuFomento);
                CursoFodes = _cifradoDesifrado.decryptString(CursoFodes);
                observacionesSolicitud = _cifradoDesifrado.decryptString(observacionesSolicitud);
                tipoCapacidadPago = _cifradoDesifrado.decryptString(tipoCapacidadPago);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarSolicitudCredito("
                + "modeloString.Valor: " + modeloString.Valor
                + ", idTipoCreditoTipoRolRelacion: " + idTipoCreditoTipoRolRelacion
                + ", idTipoIdentificacion: " + idTipoIdentificacion
                + ", numeroIdentificacion: " + numeroIdentificacion
                + ", valorNegociado: " + valorNegociado
                + ", cuentaFai: " + cuentaFai
                + ", girarCheque: " + girarCheque
                + ", cruceInterproductos: " + cruceInterproductos
                + ", saldoAnterior: " + saldoAnterior
                + ", idAsegurabilidadOpcion: " + idAsegurabilidadOpcion
                + ", idUsoCredito: " + idUsoCredito
                + ", idOficina: " + idOficina
                + ", UserLog: " + UserLog
                + ", valorInmueble: " + valorInmueble
                + ", idTipoGarantia: " + idTipoGarantia
                + ", fscNumSolicitud: " + fscNumSolicitud
                + ", IdCiuuFomento: " + IdCiuuFomento
                + ", CursoFodes: " + CursoFodes
                + ", observacionesSolicitud: " + observacionesSolicitud
                + ", tipoCapacidadPago: " + tipoCapacidadPago
                + ")");

                VMSolicitudCreditoCodeudores vmSolicitudCreditoCodeudores = new VMSolicitudCreditoCodeudores();
                List<FormCapacidadPagoGrupos> listFormCapacidadPagoGrupos = new List<FormCapacidadPagoGrupos>();
                vmSolicitudCreditoCodeudores = JsonConvert.DeserializeObject<VMSolicitudCreditoCodeudores>(modeloString.Valor);



                ///// Guardar solicitud ============================
                decimal valorSolicitado = 0;
                decimal valorCuentaFai = 0;
                decimal valorGirarCheque = 0;
                decimal valorCruceInterproductos = 0;
                decimal valorSaldoAnterior = 0;
                decimal valorInmuebleDecimal = 0;
                short IdAsegurabilidadOpcion = 0;
                short IdUsoCredito = 0;
                int IdOficina = 0;
                short shortIdTipoGarantia = 0;
                int intFscNumSolicitud = 0;
                int intIdCiuuFomento = 0;
                Boolean booleanCurso = false;
                decimal.TryParse(valorNegociado, out valorSolicitado);
                decimal.TryParse(cuentaFai, out valorCuentaFai);
                decimal.TryParse(girarCheque, out valorGirarCheque);
                decimal.TryParse(cruceInterproductos, out valorCruceInterproductos);
                decimal.TryParse(saldoAnterior, out valorSaldoAnterior);
                decimal.TryParse(valorInmueble, out valorInmuebleDecimal);
                short.TryParse(idAsegurabilidadOpcion, out IdAsegurabilidadOpcion);
                short.TryParse(idUsoCredito, out IdUsoCredito);
                int.TryParse(idOficina, out IdOficina);
                short.TryParse(idTipoGarantia, out shortIdTipoGarantia);
                int.TryParse(fscNumSolicitud, out intFscNumSolicitud);
                int.TryParse(IdCiuuFomento, out intIdCiuuFomento);
                Boolean.TryParse(CursoFodes, out booleanCurso);
                int enteroIdTipoCreditoTipoRolRelacion = 0;
                int.TryParse(idTipoCreditoTipoRolRelacion, out enteroIdTipoCreditoTipoRolRelacion);
                Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito solicitudCredito = new Infraestructura.Model.Sicav.SolicitudCredito();
                solicitudCredito.IdTipoCreditoTipoRolRelacion = enteroIdTipoCreditoTipoRolRelacion;
                solicitudCredito.IdTipoIdentificacionAsociado = idTipoIdentificacion ?? "";
                solicitudCredito.NumeroIdentificacionAsociado = numeroIdentificacion ?? "";
                solicitudCredito.AudFechaCreacion = DateTime.Now;
                solicitudCredito.AudFechaModificacion = DateTime.Now;
                solicitudCredito.AudUsuarioCreacion = UserLog;
                solicitudCredito.AudUsuarioModificacion = UserLog;
                solicitudCredito.ValorSolicitado = valorSolicitado;
                solicitudCredito.IdEstadoSolicitudCredito = (short)enumEstadosSolicitudCredito.ANALISIS;
                solicitudCredito.ValorCruceInterproductos = valorCruceInterproductos;
                solicitudCredito.ValorCuentaFai = valorCuentaFai;
                solicitudCredito.ValorGirarCheque = valorGirarCheque;
                solicitudCredito.ValorSaldoAnterior = valorSaldoAnterior;
                solicitudCredito.IdAseguabilidadOpcion = IdAsegurabilidadOpcion;
                solicitudCredito.IdUsoCredito = IdUsoCredito;
                solicitudCredito.IdOficina = IdOficina;
                solicitudCredito.ValorInmueble = valorInmuebleDecimal;
                solicitudCredito.IdTipoGarantia = shortIdTipoGarantia;
                solicitudCredito.fscNumSolicitud = intFscNumSolicitud;
                solicitudCredito.IdCiuuFomento = intIdCiuuFomento;
                solicitudCredito.CursoFodes = booleanCurso;
                solicitudCredito.Observaciones = observacionesSolicitud;
                solicitudCredito.TipoCapacidadPago = tipoCapacidadPago;

                ///// =============================================

                ///// Guardar capacidad pago ==========
                Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago = new Infraestructura.Model.Sicav.CapacidadPago();
                capacidadPago.IdEstadoCapacidadPago = 1; // Abierto                
                capacidadPago.JsonCapacidadPago = serializador.Serialize(vmSolicitudCreditoCodeudores.listaFormCapacidadPagoGrupos);
                capacidadPago.AudFechaCreacion = DateTime.Now;
                capacidadPago.AudFechaModificacion = DateTime.Now;
                capacidadPago.AudUsuarioCreacion = UserLog;
                capacidadPago.AudUsuarioModificacion = UserLog;
                ///// ===================================

                ///// Guardar horas extras ==========
                Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras capacidadPagoHorasExtra = new Infraestructura.Model.Sicav.CapacidadPagoHorasExtras();
                capacidadPagoHorasExtra.JsonHorasExtras = serializador.Serialize(vmSolicitudCreditoCodeudores.listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Ingresos).SingleOrDefault().HorasExtras);
                ///// ====================================

                /// Guardar forma pago =================
                List<FormaPago> listaFormaPago = new List<FormaPago>();
                listaFormaPago = vmSolicitudCreditoCodeudores.listaFormCapacidadPagoGrupos[0].FormaPago;
                /// ========================================

                RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> respuestaNegocio = _solcitudCreditoFacade.GuardarSolicitudCredito(solicitudCredito, capacidadPago, capacidadPagoHorasExtra, listaFormaPago, vmSolicitudCreditoCodeudores.listaFormCapacidadPagoCodeudoresGrupos, vmSolicitudCreditoCodeudores.listaFormCapacidadPagoTerceroCodeudor);


                string json = JsonConvert.SerializeObject(respuestaNegocio);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarSolicitudCredito() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return response;
        }


        [HttpPost]
        [Route("api/SolicitudCreditos/EnviarEmail")]
        public HttpResponseMessage EnviarCorreo(ModeloString modeloString)
        {
            HttpResponseMessage response;
            DatosEmail adjunto = new DatosEmail();
            try
            {
                adjunto = JsonConvert.DeserializeObject<DatosEmail>(_cifradoDesifrado.decryptString(modeloString.Valor));
                byte[] bytes = Convert.FromBase64String(adjunto.adjunto);
                MemoryStream Archivo = new MemoryStream(bytes);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio EnviarCorreo("
                + "adjunto.correo: " + adjunto.correo
                + ", adjunto.Asunto: " + adjunto.Asunto + ")");

                MailMessage mail = new MailMessage("sicav@cavipetrol.com", adjunto.correo)
                {
                    Subject = "Tabla Amortizacion",
                    Body = "Señor usuario esta es la tabla de amortización de el credito"
                };
                mail.Attachments.Add(new Attachment(Archivo, "amortizacion.pdf", System.Net.Mime.MediaTypeNames.Application.Pdf));
                SmtpClient client = new SmtpClient
                {
                    Port = 25,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Host = "128.51.3.115"
                };
                
                client.Send(mail);               
                mail.Dispose();
                response = Request.CreateResponse(HttpStatusCode.OK, "Correo enviado correctamente");
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error EnviarCorreo() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }

            return response;
        }

        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;
            logger.Debug("Usuario: " + this.usuario + " -> Inicio IsFileLocked("
                + "file.Name: " + file.Name + ")");

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error IsFileLocked() -> " + ex.Message);
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarAsociado")]
        public HttpResponseMessage GuardarAsociado(ModeloString modeloString)
        {
            HttpResponseMessage response;
            GuardarInformacionSolicitante guardarInformacionSolicitante = new GuardarInformacionSolicitante();

            try
            {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarAsociado("
                + "modeloString.Valor: " + modeloString.Valor + ")");

                guardarInformacionSolicitante = JsonConvert.DeserializeObject<GuardarInformacionSolicitante>(modeloString.Valor);
                RespuestaNegocio<object> respuestaNegocio = _solcitudCreditoFacade.GuardarAsociado(guardarInformacionSolicitante);

                response = Request.CreateResponse(HttpStatusCode.OK, "Asociado o Empleado actualizado correctamente.");

            } catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarAsociado() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return response;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerCartera")]
        public HttpResponseMessage ObtenerCartera(string idTipoIdentificacion, string numeroIdentificacion)
        {
            HttpResponseMessage response;
            try
            {
                numeroIdentificacion = _cifradoDesifrado.decryptString(numeroIdentificacion);
                idTipoIdentificacion = _cifradoDesifrado.decryptString(idTipoIdentificacion);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCartera("
                + "numeroIdentificacion: " + numeroIdentificacion
                + ", idTipoIdentificacion: " + idTipoIdentificacion + ")");

                RespuestaNegocio<Cartera> respuestaNegocio = _solcitudCreditoFacade.ObtenerCartera(idTipoIdentificacion, numeroIdentificacion);
                
                string json = JsonConvert.SerializeObject(respuestaNegocio);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
               
            } catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCartera() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.OK, ex.ToString());
            }
            return response;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerSolicitudCredito")]
        public HttpResponseMessage ObtenerSolicitudCredito(string numeroIdentificacion,
                                                    string idTipoIdentificacion,
                                                    string numeroSolicitud,
                                                    string tipoCredito,
                                                    string estadoSolicitudCredito,
                                                    string idPerfil,
                                                    string Usuario)
        {
            HttpResponseMessage response;
            int perfil = 0;
            int idTipoCredito = 0;
            RespuestaNegocio<IEnumerable<ViewModelSolicitudCredito>> respuestaNegocio =
                new RespuestaNegocio<IEnumerable<ViewModelSolicitudCredito>>();
            try
            {
                numeroIdentificacion = _cifradoDesifrado.decryptString(numeroIdentificacion);
                idTipoIdentificacion = _cifradoDesifrado.decryptString(idTipoIdentificacion);
                numeroSolicitud = _cifradoDesifrado.decryptString(numeroSolicitud);
                tipoCredito = _cifradoDesifrado.decryptString(tipoCredito);
                estadoSolicitudCredito = _cifradoDesifrado.decryptString(estadoSolicitudCredito);
                idPerfil = _cifradoDesifrado.decryptString(idPerfil);
                Usuario = _cifradoDesifrado.decryptString(Usuario);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerSolicitudCredito("
                + "numeroIdentificacion: " + numeroIdentificacion
                + ", idTipoIdentificacion: " + idTipoIdentificacion
                + ", numeroSolicitud: " + numeroSolicitud
                + ", tipoCredito: " + tipoCredito
                + ", estadoSolicitudCredito: " + estadoSolicitudCredito
                + ", idPerfil: " + idPerfil + ")");

                int.TryParse(idPerfil, out perfil);
                int.TryParse(tipoCredito, out idTipoCredito);
                respuestaNegocio =
                    _solcitudCreditoFacade.ObtenerSolicitudCredito(
                        (numeroIdentificacion == string.Empty ? null : numeroIdentificacion),
                        (idTipoIdentificacion == string.Empty ? null : idTipoIdentificacion),
                        (numeroSolicitud == string.Empty ? null : numeroSolicitud),
                        (idTipoCredito == 0 ? null : (int?)idTipoCredito),
                        (estadoSolicitudCredito == string.Empty ? null : estadoSolicitudCredito),
                        (perfil == 0 ? (int?)null : perfil),
                         (Usuario == string.Empty ? null : Usuario));
               
                string json = JsonConvert.SerializeObject(respuestaNegocio);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));


            } catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerSolicitudCredito() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, respuestaNegocio.MensajesError);
            }

            return response;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/CambiarEstadoSolicitud")]
        public HttpResponseMessage CambiarEstadoSolicitud(string idSolicitud)
        {
            HttpResponseMessage response;
            int intIdSolicitud = 0;
            try
            {
                idSolicitud = _cifradoDesifrado.decryptString(idSolicitud);                
                logger.Debug("Usuario: " + this.usuario + " -> CambiarEstadoSolicitud (id: " + idSolicitud + ")");

                int.TryParse(idSolicitud, out intIdSolicitud);
                string json = JsonConvert.SerializeObject(_solcitudCreditoFacade.CambiarEstadoSolicitud(intIdSolicitud));
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error CambiarEstadoSolicitud() -> " + ex.Message);
                throw;
            }
            return response;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerSolicitudCreditoDetalle")]
        public HttpResponseMessage ObtenerSolicitudCreditoDetalle(string idSolicitud)
        {
            HttpResponseMessage response;
            int intIdSolicitud = 0;
            RespuestaNegocio<ViewModelSolicitudCreditoDetalle> respuestaNegocio =
                new RespuestaNegocio<ViewModelSolicitudCreditoDetalle>();

            try
            {
                idSolicitud = _cifradoDesifrado.decryptString(idSolicitud);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerSolicitudCreditoDetalle("
                + "idSolicitud: " + idSolicitud + ")");

                int.TryParse(idSolicitud, out intIdSolicitud);
                respuestaNegocio = _solcitudCreditoFacade.ObtenerSolicitudCreditoDetalle(intIdSolicitud);
                //if (respuestaNegocio.Estado)
                //{
                string json = JsonConvert.SerializeObject(respuestaNegocio);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));

                //response = Request.CreateResponse(HttpStatusCode.OK, respuestaNegocio);
                //}
                //else
                //{
                //    response = Request.CreateResponse(HttpStatusCode.InternalServerError, respuestaNegocio.MensajesError);
                //}
            } catch (Exception ex) {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerSolicitudCreditoDetalle() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, respuestaNegocio.MensajesError);
            }

            return response;
        }
        [HttpPost]
        [Route("api/SolicitudCreditos/ActuaizarSolicitudCredito")]
        public HttpResponseMessage ActuaizarSolicitudCredito(ModeloString modeloString, //VMActualizarSolicitudCredito 
                                                            string idSolicitud,
                                                            string idEstadoSolicitudCredito,
                                                            string observaciones,
                                                            string usuRegistra,
                                                            string idMenu, //int
                                                            string idProceso, //int
                                                            string valorNegociado) //decimal
        {
            HttpResponseMessage response;
            VMActualizarSolicitudCredito dtoVMActualizarSolicitudCredito = new VMActualizarSolicitudCredito();
            int intIdMenu = 0;
            int intIdProceso = 0;
            decimal decimalValorNegociado = 0;
            int intIdSolicitud = 0;
            short shortIdEstado = 0;
            RespuestaNegocio<SICSES.Infraestructura.Model.Sicav.SolicitudCredito> respuestaNegocio =
                new RespuestaNegocio<Infraestructura.Model.Sicav.SolicitudCredito>();

            try {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                idSolicitud = _cifradoDesifrado.decryptString(idSolicitud);
                idEstadoSolicitudCredito = _cifradoDesifrado.decryptString(idEstadoSolicitudCredito);
                observaciones = _cifradoDesifrado.decryptString(observaciones);
                usuRegistra = _cifradoDesifrado.decryptString(usuRegistra);
                idMenu = _cifradoDesifrado.decryptString(idMenu);
                idProceso = _cifradoDesifrado.decryptString(idProceso);
                valorNegociado = _cifradoDesifrado.decryptString(valorNegociado);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ActuaizarSolicitudCredito("
                + "modeloString.Valor: " + modeloString.Valor
                + ", idSolicitud: " + idSolicitud
                + ", idEstadoSolicitudCredito: " + idEstadoSolicitudCredito
                + ", observaciones: " + observaciones
                + ", usuRegistra: " + usuRegistra
                + ", idMenu: " + idMenu
                + ", idProceso: " + idProceso
                + ", valorNegociado: " + valorNegociado + ")");

                dtoVMActualizarSolicitudCredito = JsonConvert.DeserializeObject<VMActualizarSolicitudCredito>(modeloString.Valor);
                int.TryParse(idMenu, out intIdMenu);
                int.TryParse(idProceso, out intIdProceso);
                decimal.TryParse(valorNegociado, out decimalValorNegociado);
                int.TryParse(idSolicitud, out intIdSolicitud);
                short.TryParse(idEstadoSolicitudCredito, out shortIdEstado);
                List<Parametros> listaCausalesNegacion = dtoVMActualizarSolicitudCredito.causalesNegacion == null ? new List<Parametros>() : dtoVMActualizarSolicitudCredito.causalesNegacion.Select(x => new Parametros { Nombre = x.Text, Valor = x.Value }).ToList();
                JavaScriptSerializer serializador = new JavaScriptSerializer();

                ///// Guardar capacidad pago ==========
                Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago = new Infraestructura.Model.Sicav.CapacidadPago();
                if (shortIdEstado == (short)enumEstadosSolicitudCredito.MENOR_VALOR)
                {
                    capacidadPago.IdEstadoCapacidadPago = (short)enumEstadosSolicitudCredito.MENOR_VALOR;
                    capacidadPago.JsonCapacidadPago = serializador.Serialize(dtoVMActualizarSolicitudCredito.FormCapacidadPagoGrupos);
                    capacidadPago.AudFechaCreacion = DateTime.Now;
                    capacidadPago.AudFechaModificacion = DateTime.Now;
                    capacidadPago.AudUsuarioCreacion = usuRegistra;
                    capacidadPago.AudUsuarioModificacion = usuRegistra;
                    capacidadPago.IdSolicitud = intIdSolicitud;
                    capacidadPago.ValorNegociado = decimalValorNegociado;
                }
                else {
                    capacidadPago = null;
                }
                ///// =====================================

                respuestaNegocio = _solcitudCreditoFacade.ActuaizarSolicitudCredito(
                    listaCausalesNegacion, intIdSolicitud,
                    shortIdEstado, observaciones, usuRegistra,
                    intIdMenu, intIdProceso, capacidadPago);

                //if (respuestaNegocio.Estado)
                //{
                string json = JsonConvert.SerializeObject(respuestaNegocio);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
                //}
                //else
                //{
                //    response = Request.CreateResponse(HttpStatusCode.InternalServerError, respuestaNegocio.MensajesError);
                //}

            }
            catch (Exception ex) {
                logger.Error("Usuario: " + this.usuario + " -> Error ActuaizarSolicitudCredito() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, respuestaNegocio.MensajesError);
            }

            return response;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerInformacionGarantia")]
        public HttpResponseMessage ObtenerDatosGarantia(string NumSolicitud = "", string TipoDoc = "", string NumeroDoc = "")
        {
            HttpResponseMessage response;
            NumSolicitud = _cifradoDesifrado.decryptString(NumSolicitud);
            TipoDoc = _cifradoDesifrado.decryptString(TipoDoc);
            NumeroDoc = _cifradoDesifrado.decryptString(NumeroDoc);
            try
            {
                NumSolicitud = _cifradoDesifrado.decryptString(NumSolicitud);
                TipoDoc = _cifradoDesifrado.decryptString(TipoDoc);
                NumeroDoc = _cifradoDesifrado.decryptString(NumeroDoc);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerDatosGarantia("
                + "NumSolicitud: " + NumSolicitud
                + ", TipoDoc: " + TipoDoc
                + ", NumeroDoc: " + NumeroDoc + ")");

                var datosGarantia = _solcitudCreditoFacade.ObtenerInformacionGarantia(NumSolicitud, TipoDoc, NumeroDoc);
                string json = JsonConvert.SerializeObject(datosGarantia);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            } catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerDatosGarantia() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return response;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerCupoAsegurabilidad")]
        //public HttpResponseMessage ObtenerCupoAsegurabilidad(int edad, string numeroIdentificacion)
        public HttpResponseMessage ObtenerCupoAsegurabilidad(string edad, string numeroIdentificacion)
        {
            HttpResponseMessage response;
            try
            {
                numeroIdentificacion = _cifradoDesifrado.decryptString(numeroIdentificacion);
                edad = _cifradoDesifrado.decryptString(edad);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCupoAsegurabilidad("
                + "numeroIdentificacion: " + numeroIdentificacion
                + ", edad: " + edad + ")");

                int intEdad = 0;
                int.TryParse(edad, out intEdad);

                var aseurabilidad = _solcitudCreditoFacade.ObtenerCupoAsegurabilidad(intEdad);
                var saldoAsegurado = _solcitudCreditoFacade.ObtenerSaldoAseurabilidad(numeroIdentificacion);

                decimal saldo = 0;
                decimal.TryParse((saldoAsegurado.Respuesta == null ? "0" : saldoAsegurado.Respuesta.Saldo.ToString()), out saldo);
                aseurabilidad.Respuesta.Valor = (aseurabilidad.Respuesta.Valor - saldo);
                aseurabilidad.Respuesta.Valor = aseurabilidad.Respuesta.Valor < 0 ? 0 : aseurabilidad.Respuesta.Valor;

                string json = JsonConvert.SerializeObject(aseurabilidad);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));

                //response = Request.CreateResponse(HttpStatusCode.OK, aseurabilidad);
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCupoAsegurabilidad() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return response;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerConsolidadoAtribuciones")]
        public HttpResponseMessage ObtenerConsolidadoAtribuciones(string idTipoIdentificacion,
                                                                 string numeroIdentificacionAsociado,
                                                                 string idPerfil, //int
                                                                 string valorcredito) //int
        {
            HttpResponseMessage response;
            int intIdPerfil = 0;
            int intValorCredito = 0;

            try
            {
                idTipoIdentificacion = _cifradoDesifrado.decryptString(idTipoIdentificacion);
                numeroIdentificacionAsociado = _cifradoDesifrado.decryptString(numeroIdentificacionAsociado);
                idPerfil = _cifradoDesifrado.decryptString(idPerfil);
                valorcredito = _cifradoDesifrado.decryptString(valorcredito);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerConsolidadoAtribuciones("
                + "idTipoIdentificacion: " + idTipoIdentificacion
                + ", numeroIdentificacionAsociado: " + numeroIdentificacionAsociado
                + ", idPerfil: " + idPerfil
                + ", valorcredito: " + valorcredito + ")");

                int.TryParse(idPerfil, out intIdPerfil);
                int.TryParse(valorcredito, out intValorCredito);

                var consolidado = _solcitudCreditoFacade.ObtenerConsolidadoAtribuciones(idTipoIdentificacion, numeroIdentificacionAsociado, intIdPerfil, intValorCredito);
                string json = JsonConvert.SerializeObject(consolidado);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerConsolidadoAtribuciones() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return response;
        }


        [HttpGet]
        [Route("api/SolicitudCreditos/ValidarCodeudor")]
        public HttpResponseMessage ValidarCodeudor(string NumeroDocumento)
        {
            HttpResponseMessage response;
            NumeroDocumento = _cifradoDesifrado.decryptString(NumeroDocumento);
            try
            {
                NumeroDocumento = _cifradoDesifrado.decryptString(NumeroDocumento);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ValidarCodeudor("
                + "NumeroDocumento: " + NumeroDocumento + ")");

                var respuesta = _solcitudCreditoFacade.ValidarCodeudor(NumeroDocumento);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ValidarCodeudor() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return response;
        }
        [HttpGet]
        [Route("api/Consultas/ObtenerTipoGarantiaPorProducto")]
        public HttpResponseMessage ObtenerTipoGarantiaPorProducto(string Producto)
        {

            HttpResponseMessage response;
            Producto = _cifradoDesifrado.decryptString(Producto);
            try
            {
                Producto = _cifradoDesifrado.decryptString(Producto);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoGarantiaPorProducto("
                + "Producto: " + Producto + ")");

                var TipoGarantiaPorProducto = _solcitudCreditoFacade.ObtenerTipoGarantiaPorProducto(Producto).ToList();
                var listaSeleccionable = new ConvertirAListaSeleccionable().Convertir<TipoGarantiaPorProducto>(TipoGarantiaPorProducto, id: "Codeudores", text: "TipoGarantia");
                string json = JsonConvert.SerializeObject(listaSeleccionable);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
                //response = Request.CreateResponse(HttpStatusCode.OK, listaSeleccionable);
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoGarantiaPorProducto() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }

            return response;
        }
        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarIntentoLog")]
        public HttpResponseMessage GuardarIntentoLog(ModeloString modeloString)
        {
            HttpResponseMessage response;
            RespuestaNegocio<SolicitudCreditoIntentosLog> rta = new RespuestaNegocio<SolicitudCreditoIntentosLog>();
            SolicitudCreditoIntentosLog solicitudCreditoIntentosLog = new SolicitudCreditoIntentosLog();
            try
            {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarIntentoLog("
                + "modeloString.Valor: " + modeloString.Valor + ")");
                solicitudCreditoIntentosLog = JsonConvert.DeserializeObject<SolicitudCreditoIntentosLog>(modeloString.Valor);
                rta = _solcitudCreditoFacade.GuardarSolicitudCreditoIntentoLog(solicitudCreditoIntentosLog);

                string json = JsonConvert.SerializeObject(rta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
                
            }
            catch (Exception ex) {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarIntentoLog() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, rta.MensajesError);
            }
            return response;
        }

        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarFamiliar")]
        public HttpResponseMessage GuardarFamiliar(ModeloString modeloString)
        {
            HttpResponseMessage response;
            RespuestaNegocio<Familiar> respuestaNegocio = new RespuestaNegocio<Familiar>();
            Familiar familiar = new Familiar();
            try {

                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarFamiliar("
                + "modeloString.Valor: " + modeloString.Valor + ")");

                familiar = JsonConvert.DeserializeObject<Familiar>(modeloString.Valor);

                respuestaNegocio = _solcitudCreditoFacade.GuardarFamiliar(familiar);
                if (respuestaNegocio.Estado)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, "Familiar guardado correctamente.");
                }
                else {
                    response = Request.CreateResponse(HttpStatusCode.OK, "Familiar no se guardado correctamente.");
                }
                
            }
            catch (Exception ex) {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarFamiliar() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, respuestaNegocio.MensajesError);
            }

            return response;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/GenerarTablaAmortizacion")]
        public HttpResponseMessage GenerarTablaAmortizacion(string idLinea, //int
                                                            string idTipoCreditoTipoRolRelacion, //int
                                                            string periodos, //int
                                                            string tasaNominal, //double
                                                            string tasaEA, //double
                                                            string valorCredito, //double
                                                            string valorTotalInmueble,
                                                            string idTipoPoliza, //short
                                                            string porcentajeExtraprima,
                                                            string idPapelCavipetrol, //short
                                                            string idFormaPago, //int
                                                            string edad,
                                                            string Gracia,
                                                            string stringIdTipoGarantia) //byte

        {

            HttpResponseMessage response;
            double valorTotalInmuebleDouble = 0;
            double porcentajeExtraprimaDouble = 0;
            int intIdLinea = 0;
            int intIdTipoCreditoTipoRolRelacion = 0;
            int intPeriodos = 0;
            double doubleTasaNominal = 0;
            double doubleTasaEA = 0;
            double doubleValorCredito = 0;
            short shortIdTipoPoliza = 0;
            short shortIdPapelCavipetrol = 0;
            int intIdFormaPago = 0;
            byte byteEdad = 0;
            int ValorGracia = 0;
            int idTipoGarantia = 0;
            RespuestaNegocio<List<Amortizacion>> respuestaNegocio = new RespuestaNegocio<List<Amortizacion>>();

            try {
                idLinea = _cifradoDesifrado.decryptString(idLinea);
                idTipoCreditoTipoRolRelacion = _cifradoDesifrado.decryptString(idTipoCreditoTipoRolRelacion);
                periodos = _cifradoDesifrado.decryptString(periodos);
                tasaNominal = _cifradoDesifrado.decryptString(tasaNominal);
                tasaEA = _cifradoDesifrado.decryptString(tasaEA);
                valorCredito = _cifradoDesifrado.decryptString(valorCredito);
                valorTotalInmueble = _cifradoDesifrado.decryptString(valorTotalInmueble);
                idTipoPoliza = _cifradoDesifrado.decryptString(idTipoPoliza);
                porcentajeExtraprima = _cifradoDesifrado.decryptString(porcentajeExtraprima);
                idPapelCavipetrol = _cifradoDesifrado.decryptString(idPapelCavipetrol);
                idFormaPago = _cifradoDesifrado.decryptString(idFormaPago);
                edad = _cifradoDesifrado.decryptString(edad);
                Gracia = _cifradoDesifrado.decryptString(Gracia);
                stringIdTipoGarantia = _cifradoDesifrado.decryptString(stringIdTipoGarantia);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GenerarTablaAmortizacion("
                + "idLinea: " + idLinea
                + ", idTipoCreditoTipoRolRelacion: " + idTipoCreditoTipoRolRelacion
                + ", periodos: " + periodos
                + ", tasaNominal: " + tasaNominal
                + ", tasaEA: " + tasaEA
                + ", valorCredito: " + valorCredito
                + ", valorTotalInmueble: " + valorTotalInmueble
                + ", idTipoPoliza: " + idTipoPoliza
                + ", porcentajeExtraprima: " + porcentajeExtraprima
                + ", idPapelCavipetrol: " + idPapelCavipetrol
                + ", idFormaPago: " + idFormaPago
                + ", edad: " + edad 
                + ", gracia: " + Gracia + ")");

                int.TryParse(idLinea, out intIdLinea);
                int.TryParse(idTipoCreditoTipoRolRelacion, out intIdTipoCreditoTipoRolRelacion);
                int.TryParse(periodos, out intPeriodos);
                double.TryParse(tasaNominal, out doubleTasaNominal);
                double.TryParse(tasaEA, out doubleTasaEA);
                double.TryParse(valorCredito, out doubleValorCredito);
                short.TryParse(idTipoPoliza, out shortIdTipoPoliza);
                short.TryParse(idPapelCavipetrol, out shortIdPapelCavipetrol);
                int.TryParse(idFormaPago, out intIdFormaPago);
                byte.TryParse(edad, out byteEdad);

                double.TryParse(valorTotalInmueble, out valorTotalInmuebleDouble);
                double.TryParse(porcentajeExtraprima, out porcentajeExtraprimaDouble);
                int.TryParse(Gracia, out ValorGracia);
                int.TryParse(stringIdTipoGarantia, out idTipoGarantia);

                respuestaNegocio =
                    _solcitudCreditoFacade.GenerarTablaAmortizacion(
                        intIdLinea, intIdTipoCreditoTipoRolRelacion, intPeriodos,
                        doubleTasaNominal, doubleTasaEA, doubleValorCredito, valorTotalInmuebleDouble,
                        shortIdTipoPoliza, porcentajeExtraprimaDouble, shortIdPapelCavipetrol,
                        byteEdad, intIdFormaPago,ValorGracia, idTipoGarantia);
                string json = JsonConvert.SerializeObject(respuestaNegocio);
                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));

            }catch (Exception ex) {
                logger.Error("Usuario: " + this.usuario + " -> Error GenerarTablaAmortizacion() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, respuestaNegocio.MensajesError);
            }
            return response;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerFormaPago")]
        public HttpResponseMessage ObtenerFormaDePagoSolicitud(string idSolicitud)
        {
            HttpResponseMessage response;
            try
            {
                idSolicitud = _cifradoDesifrado.decryptString(idSolicitud);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerFormaDePagoSolicitud("
                + "idSolicitud: " + idSolicitud + ")");

                IEnumerable<FormaPagoCredito> respuesta = _solcitudCreditoFacade.ObtenerFormaDePagoSolicitud(idSolicitud);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormaDePagoSolicitud() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return response;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerCoincidenciaCodeudorCreditos")]
        public HttpResponseMessage ObtenerCoincidenciaCodeudorCreditos(string numeroIdentificacion, string tipoIdentificacion)
        {
            HttpResponseMessage reponse;
            numeroIdentificacion = _cifradoDesifrado.decryptString(numeroIdentificacion);
            tipoIdentificacion = _cifradoDesifrado.decryptString(tipoIdentificacion);
            try
            {

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCoincidenciaCodeudorCreditos("
                + "numeroIdentificacion: " + numeroIdentificacion
                + "tipoIdentificacion: " + tipoIdentificacion + ")");

                List<CapacidadPagoCodeudores> respuesta = _solcitudCreditoFacade.ObtenerCoincidenciaCodeudorCreditos(numeroIdentificacion, tipoIdentificacion);
                string json = JsonConvert.SerializeObject(respuesta);
                reponse = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCoincidenciaCodeudorCreditos() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        //IEnumerable<ViewModelFormaPagoFYC> ObtenerFormaPagoRelacionNorma(string norma);

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerFormaPagoPorNorma")]
        public HttpResponseMessage ObtenerFormaPagoRelacionPorNorma(string norma)
        {
            HttpResponseMessage response;
            try
            {
                norma = _cifradoDesifrado.decryptString(norma);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerFormaPagoRelacionPorNorma("
                + "norma: " + norma + ")");

                var respuesta = _solcitudCreditoFacade.ObtenerFormaPagoPorNorma(norma);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
                
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerFormaPagoRelacionPorNorma() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
            return response;

        }

        /**
         * Método para guardar datos de fyc solicitud servicio.
         */
        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarSolicitudCreditoFyc")]
        public HttpResponseMessage guardarSolicitudCreditoFyc(ModeloString modeloString)
        {
            HttpResponseMessage response;
            try {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio guardarSolicitudCreditoFyc("
                + "modeloString.Valor: " + modeloString.Valor + ")");

                SolicitudCreditoFyc solicitudCreditoFyc = new SolicitudCreditoFyc();
                solicitudCreditoFyc = JsonConvert.DeserializeObject<SolicitudCreditoFyc>(modeloString.Valor);

                //RespuestaNegocio<object> respuestaNegocio = _solcitudCreditoFacade.guardarSolicitudCreditoFyc(solicitudCreditoFyc);
                var respuestaNegocio = _solcitudCreditoFacade.guardarSolicitudCreditoFyc(solicitudCreditoFyc);

                string json = JsonConvert.SerializeObject(respuestaNegocio);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error guardarSolicitudCreditoFyc() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
            return response;
        }
        /// <summary>
        /// guarda solicitud de credito fyc e isynet
        /// </summary>
        /// <param name="modeloString"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarSolicitudCreditoFycIsynetECP")]
        public HttpResponseMessage GuardarSolicitudCreditoFycIsynetECP(ModeloString modeloString)
        {
            HttpResponseMessage response;
            try
            {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio guardarSolicitudCreditoFyc("
                + "modeloString.Valor: " + modeloString.Valor + ")");

                SolicitudCreditoFyc solicitudCreditoFyc = new SolicitudCreditoFyc();
                solicitudCreditoFyc = JsonConvert.DeserializeObject<SolicitudCreditoFyc>(modeloString.Valor);
                SolicitudCreditoNegocio ObjBllSolicitudCreditoFYCIsynetECP = new SolicitudCreditoNegocio();
                //RespuestaNegocio<object> respuestaNegocio = _solcitudCreditoFacade.guardarSolicitudCreditoFyc(solicitudCreditoFyc);
                var respuestaNegocio = ObjBllSolicitudCreditoFYCIsynetECP.GuardarSolicitudCreditoFycIsynetECP(solicitudCreditoFyc);

                string json = JsonConvert.SerializeObject(respuestaNegocio);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error guardarSolicitudCreditoFyc() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
            }
            return response;
        }

        /**
         * Método para guardar los pagares de garantia.
         */
        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarPagareGarantia")]
        public HttpResponseMessage guardarPagareGarantia(ModeloString modeloString)
        {
            HttpResponseMessage response;
            RespuestaNegocio<object> respuestaNegocio = new RespuestaNegocio<object>();

            try {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio guardarPagareGarantia("
                + "modeloString.Valor: " + modeloString.Valor + ")");

                SolicitudCreditoPagare solicitudCreditoPagare = new SolicitudCreditoPagare();
                solicitudCreditoPagare = JsonConvert.DeserializeObject<SolicitudCreditoPagare>(modeloString.Valor);
                respuestaNegocio = _solcitudCreditoFacade.guardarPagareGarantia(solicitudCreditoPagare);
                
                response = Request.CreateResponse(HttpStatusCode.OK, "Pagare Garantia Guardada Correctamente.");
            }
            catch (Exception ex) {
                logger.Error("Usuario: " + this.usuario + " -> Error guardarPagareGarantia() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, respuestaNegocio.MensajesError);
            }

            return response;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerSolicitudCreditoDetalleCodeudores")]
        public HttpResponseMessage ObtenerSolicitudCreditoDetalleCodeudores(string idSolicitud)
        {
            HttpResponseMessage reponse;
            int intIdSolicitud = 0;
            try
            {
                idSolicitud = _cifradoDesifrado.decryptString(idSolicitud);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerSolicitudCreditoDetalleCodeudores("
                + "idSolicitud: " + idSolicitud + ")");

                int.TryParse(idSolicitud, out intIdSolicitud);
                var solicitudCodeudores = _solcitudCreditoFacade.ObtenerSolicitudCreditoDetalleCodeudores(intIdSolicitud);
                solicitudCodeudores.ForEach(codeudor => {
                    var infoCodeudor = _solcitudCreditoFacade.ObtenerInformacionEmpleado(codeudor.NumeroIdentificacionCodeudor, codeudor.IdTipoIdentificacionCodeudor);
                    if (infoCodeudor.Estado) codeudor.DireccionCodeudor = infoCodeudor.Respuesta.DireccionResidencia;
                });
                reponse = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(JsonConvert.SerializeObject(solicitudCodeudores)));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerSolicitudCreditoDetalleCodeudores() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerSolicitudCreditoAdjunto")]
        public HttpResponseMessage ObtenerSolicitudCreditoAdjunto(string idSolicitud, string idTipoAdjunto)
        {
            HttpResponseMessage reponse;
            try
            {
                idSolicitud = _cifradoDesifrado.decryptString(idSolicitud);
                idTipoAdjunto = _cifradoDesifrado.decryptString(idTipoAdjunto);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerSolicitudCreditoAdjunto("
                + "idSolicitud: " + idSolicitud
                + "idTipoAdjunto: " + idTipoAdjunto + ")");

                int intIdSolicitud = 0, intIdTipoAdjunto = 0;
                int.TryParse(idSolicitud, out intIdSolicitud);
                int.TryParse(idTipoAdjunto, out intIdTipoAdjunto);
                var cantidadSolicitudCreditoAdjunto = _solcitudCreditoFacade.ObtenerSolicitudCreditoAdjunto(intIdSolicitud, intIdTipoAdjunto);

                if (cantidadSolicitudCreditoAdjunto.Respuesta.Equals("1"))
                {
                    reponse = Request.CreateResponse(HttpStatusCode.OK, true);
                } else
                {
                    reponse = Request.CreateResponse(HttpStatusCode.OK, false);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerSolicitudCreditoAdjunto() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarSolicitudCreditoAdjunto")]
        public HttpResponseMessage GuardarSolicitudCreditoAdjunto(ModeloCifrado StringCifrado)
        {
            HttpResponseMessage response;
            SolicitudCreditoAdjunto solicitudCreditoAdjunto = new SolicitudCreditoAdjunto();
            try
            {
                string des = _cifradoDesifrado.decryptString(StringCifrado.Valor);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarSolicitudCreditoAdjunto("
                + "StringCifrado.Valor: " + StringCifrado.Valor + ")");

                solicitudCreditoAdjunto = JsonConvert.DeserializeObject<SolicitudCreditoAdjunto>(des, new JsonSerializerSettings { DateFormatString = "yyyy-MM-ddTHH:mm:ss.sssZ", DateTimeZoneHandling = DateTimeZoneHandling.Local });
                SolicitudCreditoAdjunto respuesta = _solcitudCreditoFacade.GuardarSolicitudCreditoAdjunto(solicitudCreditoAdjunto);
             
                response = Request.CreateResponse(HttpStatusCode.OK, "Solicitud guardada correctamente.");
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerSolicitudCreditoAdjunto() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error guardando la solicitud");
            }
            return response;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerHipotecaSolicitudCredito")]
        public HttpResponseMessage ObtenerHipotecaSolicitudCredito(string idSolicitud)
        {
            HttpResponseMessage reponse;
            int intIdSolicitud = 0;
            try
            {
                idSolicitud = _cifradoDesifrado.decryptString(idSolicitud);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerHipotecaSolicitudCredito("
                + "idSolicitud: " + idSolicitud + ")");

                int.TryParse(idSolicitud, out intIdSolicitud);
                RespuestaNegocio<Object> respuesta = _solcitudCreditoFacade.ObtenerHipotecaSolicitudCredito(intIdSolicitud);
                reponse = Request.CreateResponse(HttpStatusCode.OK, respuesta);
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerHipotecaSolicitudCredito() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerCupoMaximoCredito")]
        public HttpResponseMessage ObtenerCupoMaximoCredito(string TipoIdentificacion, string NumeroIdentificacion)
        {
            HttpResponseMessage reponse;
            try
            {
                TipoIdentificacion = _cifradoDesifrado.decryptString(TipoIdentificacion);
                NumeroIdentificacion = _cifradoDesifrado.decryptString(NumeroIdentificacion);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCupoMaximoCredito("
                + "TipoIdentificacion: " + TipoIdentificacion
                + "NumeroIdentificacion: " + NumeroIdentificacion + ")");

                reponse = Request.CreateResponse(HttpStatusCode.OK, _solcitudCreditoFacade.ObtenerCupoMaximoCredito(TipoIdentificacion, NumeroIdentificacion));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCupoMaximoCredito() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }
        /**
         * Metodo para generar el numero de consecutivo de un pagare.
         */
        [HttpPost]
        [Route("api/SolicitudCreditos/GeneraraNumeroConsecutivoPagare")]
        public HttpResponseMessage GeneraraNumeroConsecutivoPagare(ModeloString modeloString)
        {
            HttpResponseMessage response;
            try
            {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCupoMaximoCredito("
                + "modeloString.Valor: " + modeloString.Valor + ")");

                SolicitudCreditoPagare solicitudCreditoPagare = new SolicitudCreditoPagare();
                solicitudCreditoPagare = JsonConvert.DeserializeObject<SolicitudCreditoPagare>(modeloString.Valor);

                int respuestaNegocio = _solcitudCreditoFacade.generarNumeroConsecutivoPagare(solicitudCreditoPagare);
                response = Request.CreateResponse(HttpStatusCode.OK, respuestaNegocio);
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCupoMaximoCredito() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return response;
        }

        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarSolicitudCreditoHipoteca")]
        public HttpResponseMessage GuardarSolicitudCreditoHipoteca(ModeloCifrado StringCifrado)
        {
            HttpResponseMessage response;
            SolicitudCreditoHipoteca solicitudCreditoHipoteca = new SolicitudCreditoHipoteca();
            solicitudCreditoHipoteca = JsonConvert.DeserializeObject<SolicitudCreditoHipoteca>(_cifradoDesifrado.decryptString(StringCifrado.Valor), new JsonSerializerSettings { DateFormatString = "yyyy-MM-ddTHH:mm:ss.sssZ", DateTimeZoneHandling = DateTimeZoneHandling.Local });
            bool respuesta = _solcitudCreditoFacade.GuardarSolicitudCreditoHipoteca(solicitudCreditoHipoteca);
            if (respuesta)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, "Solicitud guardada correctamente");
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error guardando la solicitud");
            }
            return response;
        }

        [HttpPost]
        [Route("api/SolicitudCreditos/GenerarHipotecaGuardarMinuta")]
        public HttpResponseMessage GenerarHipotecaGuardarMinuta(ModeloCifrado StringCifrado)
        {

            HttpResponseMessage response;
            SolicitudMinutaFYC solicitudMinutaFYC = new SolicitudMinutaFYC();

            try
            {
                string des = _cifradoDesifrado.decryptString(StringCifrado.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GenerarHipotecaGuardarMinuta("
                + "ModeloCifrado: " + StringCifrado.Valor + ")");

                solicitudMinutaFYC = JsonConvert.DeserializeObject<SolicitudMinutaFYC>(des, new JsonSerializerSettings { DateFormatString = "yyyy-MM-ddTHH:mm:ss.sssZ", DateTimeZoneHandling = DateTimeZoneHandling.Local, FloatParseHandling = FloatParseHandling.Decimal });
                RespuestaNegocio<object> respuesta = _solcitudCreditoFacade.GenerarHipotecaGuardarMinuta(solicitudMinutaFYC);

                if (respuesta.Estado)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, respuesta.Respuesta);
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error guardando la solicitud");
                }
            }
            catch (Exception ex)
            {

                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error GenerarHipotecaGuardarMinuta() -> " + ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerAportes")]
        public HttpResponseMessage ObtenerAportes(string tipoDocumento, string numeroDocumento, string IdTipoRol)
        {
            HttpResponseMessage reponse;
            try
            {
                tipoDocumento = _cifradoDesifrado.decryptString(tipoDocumento);
                numeroDocumento = _cifradoDesifrado.decryptString(numeroDocumento);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerAportes("
                + "tipoDocumento: " + tipoDocumento
                + "numeroDocumento: " + numeroDocumento + ")");

                reponse = Request.CreateResponse(HttpStatusCode.OK, _solcitudCreditoFacade.ObtenerAportes(tipoDocumento, numeroDocumento,1));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAportes() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerSolicitudes")]
        public HttpResponseMessage ObtenerSolicitudes(string numeroSolicitud, string tipoDocumento, string numeroDocumento, string tipoCredito)
        {
            HttpResponseMessage reponse;
            try
            {
                numeroSolicitud = _cifradoDesifrado.decryptString(numeroSolicitud);
                tipoDocumento = _cifradoDesifrado.decryptString(tipoDocumento);
                numeroDocumento = _cifradoDesifrado.decryptString(numeroDocumento);
                tipoCredito = _cifradoDesifrado.decryptString(tipoCredito);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerSolicitudes("
                + "tipoDocumento: " + numeroSolicitud
                + "numeroDocumento: " + tipoDocumento
                + "tipoDocumento: " + numeroDocumento
                + "tipoDocumento: " + tipoCredito + ")");
                int Solicitud;
                int.TryParse(numeroSolicitud, out Solicitud);
                int Documento;
                int.TryParse(numeroDocumento, out Documento);
                int Credito;
                int.TryParse(tipoCredito, out Credito);
                if(tipoDocumento == "null")
                {
                    tipoDocumento = null;
                }
                var resultado = _solcitudCreditoFacade.ObtenerSolicitudes(Solicitud, tipoDocumento, Documento, Credito);
                reponse = Request.CreateResponse(HttpStatusCode.OK, resultado);
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerSolicitudes() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ConsultarSolicitudesPorFecha")]
        public IHttpActionResult ConsultarSolicitudesPorFecha(string FechaInicio, string FechaFin)
        {
            SICSESContexto reportesSESContext = new SICSESContexto();
            DateTime DateFechaInicio = Convert.ToDateTime(FechaInicio);
            DateTime DateFechaFin = Convert.ToDateTime(FechaFin);
            
            var Solicitudes = 
                reportesSESContext.ConsultarSolicitudesCreditoPorFecha(DateFechaInicio,
                DateFechaFin).ToList();
            return Ok(Solicitudes);
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerCodeudores")]
        public HttpResponseMessage ObtenerCodeudores(string Identificacion)
        {
            HttpResponseMessage reponse;
            try
            {

                Identificacion = _cifradoDesifrado.decryptString(Identificacion);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCodeudores("
                + "Identificacion: " + Identificacion + ")");
                var resultado = _solcitudCreditoFacade.ObtenerCodeudores(Convert.ToInt32(Identificacion));
                reponse = Request.CreateResponse(HttpStatusCode.OK, resultado);
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerCodeudores() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerOpcionesAsegurabilidad")]
        public HttpResponseMessage ObtenerOpcionesAsegurabilidad(string IdAsegurable, string idCredito, string idPapel)
        {
            HttpResponseMessage reponse;
            try
            {
                IdAsegurable = _cifradoDesifrado.decryptString(IdAsegurable);
                idCredito = _cifradoDesifrado.decryptString(idCredito);
                idPapel = _cifradoDesifrado.decryptString(idPapel);

                bool Asegurable = Convert.ToBoolean(IdAsegurable);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerOpcionesAsegurabilidad("
                + "IdAsegurable: " + IdAsegurable
                + "idCredito: " + idCredito 
                + "idPapel: " + idPapel + ")");

                reponse = Request.CreateResponse(HttpStatusCode.OK, _solcitudCreditoFacade.ObtenerOpcionesAsegurabilidad(Asegurable, idCredito, idPapel));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAportes() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarJubilado")]
        public HttpResponseMessage GuardarJubilado(string TipoIdentificacion, string NumeroIdentificacion, string NombreAsociado)
        {
            HttpResponseMessage reponse;
            try
            {
                TipoIdentificacion = _cifradoDesifrado.decryptString(TipoIdentificacion);
                NumeroIdentificacion = _cifradoDesifrado.decryptString(NumeroIdentificacion);
                NombreAsociado = _cifradoDesifrado.decryptString(NombreAsociado);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarJubilado("
                + "TipoIdentificacion: " + TipoIdentificacion
                + "NumeroIdentificacion: " + NumeroIdentificacion
                + "NombreAsociado: " + NombreAsociado + ")");

                DetalleAsociado asociado = new DetalleAsociado();
                asociado.TipoIdentificacion = TipoIdentificacion;
                asociado.NumeroIdentificacion = NumeroIdentificacion;
                asociado.NombreAsociado = NombreAsociado;
                reponse = Request.CreateResponse(HttpStatusCode.OK, _solcitudCreditoFacade.GuardarJubilado(asociado));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAportes() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ConfirmarMesada14")]
        public HttpResponseMessage ConfirmarMesada14(string Cedula)
        {
            HttpResponseMessage reponse;
            try
            {
                Cedula = _cifradoDesifrado.decryptString(Cedula);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerOpcionesAsegurabilidad("
                + "Cedula: " + Cedula + ")");



                reponse = Request.CreateResponse(HttpStatusCode.OK, _solcitudCreditoFacade.ConfirmarMesada14(Cedula));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAportes() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/ValidarControles")]
        public HttpResponseMessage ValidarITP(string cedula, string tipoIdentificacion, string bandera, string control)
        {
            HttpResponseMessage reponse;
            try
            {
                cedula = _cifradoDesifrado.decryptString(cedula);
                tipoIdentificacion = _cifradoDesifrado.decryptString(tipoIdentificacion);
                bandera = _cifradoDesifrado.decryptString(bandera);
                control = _cifradoDesifrado.decryptString(control);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ValidarControles("
                + "cedula: " + cedula 
                + "cedula: " + tipoIdentificacion
                + "cedula: " + bandera
                + "bandera: " + control + ")");
                object respuesta = null;

                List<ExtraPrima> TraeExtraprima = new List<ExtraPrima>();

                switch (control)
                {
                    case "ITP":
                        respuesta = _solcitudCreditoFacade.ValidarITP(cedula, Convert.ToInt32(bandera));
                        break;
                    case "EMBARGOS":
                        respuesta = _solcitudCreditoFacade.ValidarEmbargo(cedula, Convert.ToInt32(bandera));
                        break;
                    case "EXTRAPRIMA":
                        var ExisteExtraprima = _solcitudCreditoFacade.ValidarExisExtraprima(cedula, tipoIdentificacion, Convert.ToInt32(bandera));
                        TraeExtraprima = _solcitudCreditoFacade.TraeExtraprima(cedula, tipoIdentificacion, 4).ToList();
                        if(ExisteExtraprima > 0)
                        {
                            if(TraeExtraprima.Count > 0)
                            {
                                respuesta = TraeExtraprima[0];
                            }
                        } else {
                            respuesta = 0;
                        }
                        break;
                    default:
                        respuesta = "Verifique tipo de control";
                        break;

                }
                reponse = Request.CreateResponse(HttpStatusCode.OK, respuesta);
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ValidarControles() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/actualizarEstado")]
        public HttpResponseMessage ActualizarEstado(string NumeroSolicitud, string Estado)
        {
            HttpResponseMessage reponse;
            try
            {
                NumeroSolicitud = _cifradoDesifrado.decryptString(NumeroSolicitud);
                Estado = _cifradoDesifrado.decryptString(Estado);
                
                logger.Debug("Usuario: " + this.usuario + " -> Inicio Actualizar Estado("
                + "NumeroSolicitud: " + NumeroSolicitud + "Estado: " + Estado + ")");

                _solcitudCreditoFacade.actualizarEstado(NumeroSolicitud, Estado);

                reponse = Request.CreateResponse(HttpStatusCode.OK,"OK");

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error Actualizar Estado() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        [HttpGet]
        [Route("api/SolicitudCreditos/AmorAmortizacion")]
        public HttpResponseMessage ObtenerArbolAmortizacion(string idtipocredito, string idpapelcavipetrol)
        {
            HttpResponseMessage reponse;
            try
            {
                idtipocredito = _cifradoDesifrado.decryptString(idtipocredito);
                idpapelcavipetrol = _cifradoDesifrado.decryptString(idpapelcavipetrol);

                reponse = Request.CreateResponse(HttpStatusCode.OK, _solcitudCreditoFacade.ObtenerArbolAmortizacion(idtipocredito, idpapelcavipetrol));

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error Actualizar Estado() -> " + ex.Message);
                reponse = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }
            return reponse;
        }

        [HttpPost]
        [Route("api/SolicitudCreditos/GuardarSolicitudInterproductos")]
        public HttpResponseMessage GuardarSolicitudInterproductos(ModeloString datosString )
        {
            HttpResponseMessage response;
            try
            {
                
                datosString.Valor = _cifradoDesifrado.decryptString(datosString.Valor);
                //IdProducto = _cifradoDesifrado.decryptString(IdProducto);
                //Valor = _cifradoDesifrado.decryptString(Valor);
                List< InformacionProductosCreditos >  lista = JsonConvert.DeserializeObject<List<InformacionProductosCreditos>>(datosString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarSolicitudInterproductos (usuario: " + usuario + ", contrasena:" + usuario + ")");

                _solcitudCreditoFacade.GuardarSolicitudInterproductos(lista);

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(usuario));

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarSolicitudInterproductos() -> " + ex.Message);
                throw;
            }
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numeroIdentificacion"></param>
        /// <param name="tipoIdentificacion"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/SolicitudCreditos/ObtenerInformacionCampanaEspecial")]
        public HttpResponseMessage ConsultaInformacionCampanaEspecial([FromUri] string numeroIdentificacion, [FromUri] string tipoIdentificacion)
        {            
            HttpResponseMessage response;
            try
            {
                numeroIdentificacion = _cifradoDesifrado.decryptString(numeroIdentificacion);
                tipoIdentificacion = _cifradoDesifrado.decryptString(tipoIdentificacion);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsultaInformacionCampanaespecial (numeroIdentificacion:" + numeroIdentificacion
                    + ", tipoIdentificacion: " + tipoIdentificacion + ")");
                               
                response = Request.CreateResponse(HttpStatusCode.OK, _solcitudCreditoFacade.ObtenerInformacionSolicitanteCampanaEspecial(numeroIdentificacion, tipoIdentificacion));                
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ConsultaInformacionCampanaespecial() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }

            return response;
        }

    }
}
