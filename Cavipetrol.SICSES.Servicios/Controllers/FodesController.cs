﻿using Cavipetrol.SICSES.Facade;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Cors;
using Cavipetrol.SICSES.Infraestructura.Utilidades;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
	
	[EnableCors(origins: "*", headers: "*", methods: "*")]
	public class FodesController : ApiController
	{
		private readonly IFodesFacade _IFodesFacade;
		//CLASE PARA CIFRADO DE PARAMETROS
		private Cifrado.CifradoDescifrado _cifradoDescifrado;

		private static readonly log4net.ILog logger =
		   log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		private string usuario = "";

		public FodesController()
		{
			_IFodesFacade = new FodesFacade();
			_cifradoDescifrado = new Cifrado.CifradoDescifrado();

			this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
				.Identity).NameClaimType;
		}

		[HttpGet]
		[Route("api/Fodes/ProcentajesSubsidio")]
		public HttpResponseMessage ObtenerPorcentaje(string nomPorcentaje)
		{
			HttpResponseMessage response;

			try
			{
				nomPorcentaje = _cifradoDescifrado.decryptString(nomPorcentaje);
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerPorcentaje(nomPorcentaje: "+ nomPorcentaje + ")");

				response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(_IFodesFacade.ObtenerTasas(nomPorcentaje))));
			}
			catch (Exception ex)
			{
				response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error  ObtenerPorcentaje(nomPorcentaje: " + nomPorcentaje + ")" + ex.Message);
			}

			return response;
		}
		[HttpPost]
		[Route("api/Fodes/CrearTasa")]
		public HttpResponseMessage CrearNuevaTasa(string nombreTasa, string porcentaje)
		{
			HttpResponseMessage response;
			try
			{
				nombreTasa = _cifradoDescifrado.decryptString(nombreTasa);
				porcentaje = _cifradoDescifrado.decryptString(porcentaje);

				logger.Debug("Usuario: " + this.usuario + " -> Inicio CrearNuevaTasa( nombreTasa:"+ nombreTasa+ ", porcentaje:"+ porcentaje + ")");

				_IFodesFacade.CrearNuevaTasa(nombreTasa, porcentaje);
				response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(usuario));
			}
			catch (Exception ex)
			{
				response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error GuardarLineaCredtio() -> " + ex.Message);
			}
			return response;
		}
		[HttpGet]
		[Route("api/Fodes/ObtenerCreditosFomentoEmresarial")]
		public HttpResponseMessage ObtenerCreditosFomentoEmresarial()
		{
			HttpResponseMessage response;

			try
			{
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCreditosFomentoEmresarial()");

				object servicio = _IFodesFacade.ObtenerFomentoEmresarial();
				string cifrado  = _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(servicio));
				response = Request.CreateResponse(HttpStatusCode.OK, cifrado);
			}
			catch (Exception ex)
			{
				response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error  ObtenerCreditosFomentoEmresarial()" + ex.Message);
			}

			return response;
		}
		[HttpPost]
		[Route("api/Fodes/MarcarCreditoFodes")]
		public HttpResponseMessage MarcarCreditoFodes(	string documentoTipo,
														string documentoNumero,
														string numeroUnico,
														string tipoDocumento,
														string numeroDocumento,
														string descripcion
													 )
		{
			HttpResponseMessage response;
			int NumeroUnico = 0;
			try
			{
				documentoTipo = _cifradoDescifrado.decryptString(documentoTipo);
				documentoNumero = _cifradoDescifrado.decryptString(documentoNumero);
				numeroUnico = _cifradoDescifrado.decryptString(numeroUnico);
				tipoDocumento = _cifradoDescifrado.decryptString(tipoDocumento);
				numeroDocumento = _cifradoDescifrado.decryptString(numeroDocumento);
				descripcion = _cifradoDescifrado.decryptString(descripcion);

				int.TryParse(numeroUnico, out NumeroUnico);

				logger.Debug("Usuario: " + this.usuario + " -> Inicio MarcarCreditoFodes( documentoTipo:" + documentoTipo + ", documentoNumero:" + documentoNumero + ", numeroUnico:" + numeroUnico + ", tipoDocumento:" + tipoDocumento + ", numeroDocumento:" + numeroDocumento + ", descripcion:" + descripcion + ")");

				bool respuesta = _IFodesFacade.MarcarCreditoFodes(documentoTipo, documentoNumero, NumeroUnico, tipoDocumento, numeroDocumento, descripcion);

				string json = JsonConvert.SerializeObject(respuesta);

				response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
			}
			catch (Exception ex)
			{
				response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error GuardarLineaCredtio() -> " + ex.Message);
			}
			return response;
		}
		[HttpGet]
		[Route("api/Fodes/ObtenerCreditosFomentoEmresarialMarcados")]
		public HttpResponseMessage ObtenerCreditosFomentoEmresarialMarcados()
		{
			HttpResponseMessage response;

			try
			{
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCreditosFomentoEmresarialMarcados()");

				object servicio = _IFodesFacade.ObtenerCreditosFomentoEmresarialMarcados();
				string cifrado = _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(servicio));
				response = Request.CreateResponse(HttpStatusCode.OK, cifrado);
			}
			catch (Exception ex)
			{
				response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error  ObtenerCreditosFomentoEmresarialMarcados()" + ex.Message);
			}

			return response;
		}
		[HttpPost]
		[Route("api/Fodes/DesmarcarCreditosFodes")]
		public HttpResponseMessage DesmarcarCreditoFodes(ModeloString modeloString)
		{
			HttpResponseMessage response;
			try
			{
				modeloString.Valor = _cifradoDescifrado.decryptString(modeloString.Valor);

				logger.Debug("Usuario: " + this.usuario + " -> Inicio DesmarcarCreditoFodes( modeloString:" + modeloString +")");

				List<FodesGenerico> listaDesmarcacion = new List<FodesGenerico>();
				listaDesmarcacion = JsonConvert.DeserializeObject<List<FodesGenerico>>(modeloString.Valor);

				bool respuesta = _IFodesFacade.DesmarcarCreditoFodes(listaDesmarcacion);
				string json = JsonConvert.SerializeObject(respuesta);
				response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));
			}
			catch (Exception ex)
			{
				response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error GuardarLineaCredtio() -> " + ex.Message);
			}
			return response;
		}
		[HttpGet]
		[Route("api/Fodes/ObtenerCreditosFomentoEmresarialMora")]
		public HttpResponseMessage ObtenerCreditosFomentoEmresarialMora()
		{
			HttpResponseMessage response;

			try
			{
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCreditosFomentoEmresarialMora()");

				object servicio = _IFodesFacade.ObtenerCreditosFomentoEmresarialMora();
				string cifrado = _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(servicio));
				response = Request.CreateResponse(HttpStatusCode.OK, cifrado);
			}
			catch (Exception ex)
			{
				response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error  ObtenerCreditosFomentoEmresarialMarcados()" + ex.Message);
			}

			return response;
		}
		[HttpGet]
		[Route("api/Fodes/ObtenerCausalesFodes")]
		public HttpResponseMessage ObtenerCausalesFodes()
		{
			HttpResponseMessage response;

			try
			{
				logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerCausalesFodes()");

				object servicio = _IFodesFacade.ObtenerCausalesFodes();
				string cifrado = _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(servicio));
				response = Request.CreateResponse(HttpStatusCode.OK, cifrado);
			}
			catch (Exception ex)
			{
				response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error  ObtenerCausalesFodes()" + ex.Message);
			}

			return response;
		}
		[HttpGet]
		[Route("api/Fodes/InformacionFodes")]
		public HttpResponseMessage InformacionFodes()
		{
			HttpResponseMessage response;

			try
			{
				logger.Debug("Usuario: " + this.usuario + " -> Inicio InformacionFodes()");

				object servicio = _IFodesFacade.InformacionFodes();
				string cifrado = _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(servicio));
				response = Request.CreateResponse(HttpStatusCode.OK, cifrado);
			}
			catch (Exception ex)
			{
				response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
				logger.Error("Usuario: " + this.usuario + " -> Error  ObtenerCausalesFodes()" + ex.Message);
			}

			return response;
		}
	}
}