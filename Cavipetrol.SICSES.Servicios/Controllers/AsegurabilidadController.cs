﻿using Cavipetrol.SICSES.Facade;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
    [Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AsegurabilidadController : ApiController
    {
        private readonly IAsegurabilidadFacade asegurabilidadFacade;
        private Cifrado.CifradoDescifrado _cifradoDescifrado;

        private static readonly log4net.ILog logger =
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";
        public AsegurabilidadController()
        {
            asegurabilidadFacade = new AsegurabilidadFacade();
            _cifradoDescifrado = new Cifrado.CifradoDescifrado();

            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }
        [HttpPost]
        [Route("api/Asegurabilidad/GuardarDatosAsegurabilidad")]
        public HttpResponseMessage GuardaDatosAsegurabilidad(ModeloCifrado StringCifrado)
        //public HttpResponseMessage GuardaDatosAsegurabilidad(DatosDocumento JsonObject)
        {
            HttpResponseMessage response;
            DatosDocumento JsonObject = new DatosDocumento();
            try
            {
                JsonObject = JsonConvert.DeserializeObject<DatosDocumento>(_cifradoDescifrado.decryptString(StringCifrado.Valor));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardaDatosAsegurabilidad( ModeloString: " + StringCifrado.Valor + ")");

                List<AsegurabilidadModel> lstDatos = new List<AsegurabilidadModel>();

                foreach (var item in JsonObject.DatosExcel)
                {

                    if (item[0].Length > 0)
                    {

                        AsegurabilidadModel model = new AsegurabilidadModel();
                        model.Reporte_deudores = String.IsNullOrEmpty(item[0]) ? 0 : Convert.ToInt32(item[0]);
                        model.nombre = item[1];
                        model.nit = item[2];
                        model.Ciudad_Residencia = item[3];
                        model.Dirreccion_Residencia = item[4];
                        model.Telefono_Residencia = item[5];
                        model.Ciudad_oficina = item[6];
                        model.Dirreccion_oficina = item[7];
                        model.TelefonoOficina = item[8];
                        model.Correo_Electronico_oficina = item[9];
                        model.CorreoElectronico_residencia = item[10];
                        model.Fecha_nacimiento = item[11];
                        model.Documento_ciudad = item[12];
                        model.Saldo = Convert.ToDouble(item[13].Trim().Replace(",",""));
                        model.Control_inclusiones_cavipetrol = String.IsNullOrEmpty(item[14]) ? 0 : Convert.ToInt32(item[14]);                        
                        model.Cedula = item[15];
                        model.Mes_inc = item[16];
                        model.UltimoMes_reporte = DateTime.ParseExact(item[17], "dd/MM/yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(item[17], "dd/MM/yyyy");
                        model.interrupcion = String.IsNullOrEmpty(item[18]) ? 0 : Convert.ToInt32(item[18]);
                        model.mes_inclusion = DateTime.ParseExact(item[19], "dd/MM/yyyy", CultureInfo.InvariantCulture);//Convert.ToDateTime(item[19]);
                        model.Va_por_Refinanciacion = item[20];
                        model.Cumulo_Refinan = item[21];
                        model.Etiquetas_de_fila = String.IsNullOrEmpty(item[22]) ? 0 : Convert.ToDouble(item[22].Trim().Replace(",", ""));
                        model.Continuidad = String.IsNullOrEmpty(item[23]) ? 0 :  Convert.ToDouble(item[23].Trim().Replace(",", ""));
                        model.Di_17 = String.IsNullOrEmpty(item[24]) ? 0 : Convert.ToDouble(item[24].Trim().Replace(",", ""));
                        model.Rechazo_Aseguradora_anterior = item[25];
                        model.Observacion = item[26];
                        model.Tipologia = item[27];
                        model.Declaracion = String.IsNullOrEmpty(item[28]) ? 0 : Convert.ToInt32(item[28]);
                        model.Vr_asegurado_mes_ant = String.IsNullOrEmpty(item[29]) ? 0 : Convert.ToDouble(item[29].Trim().Replace(",", ""));
                        model.Observacion_mes_anterior = item[30];
                        model.Porcentaje_extraprima = item[31];
                        model.Concepto_Extraprima = item[32];
                        model.Calculo = item[33];
                        model.Edad_al_ingreso = String.IsNullOrEmpty(item[34]) ? 0 : Convert.ToInt32(item[34]);
                        model.Tasa_por_mil_anual = item[35];
                        model.Limite_asegurado_segun_edad = String.IsNullOrEmpty(item[36]) ? 0 : Convert.ToDouble(item[36].Trim().Replace(",", ""));
                        model.Aplica_cumulo = item[37];
                        model.Valor_asegurado = String.IsNullOrEmpty(item[38]) ? 0 : Convert.ToDouble(item[38].Trim().Replace(",", ""));
                        model.Valor_sin_cobertura = item[39];
                        model.Prima_mensual = item[40];
                        model.Valor_extraprima = item[41];
                        model.Prima_mensual_Extraprima = item[42];
                        model.Retorno = item[43];

                        lstDatos.Add(model);
                        }
                    
                }
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(asegurabilidadFacade.InsertarDatosAsegurabilidad(lstDatos))));//asegurabilidadFacade.InsertarDatosAsegurabilidad(lstDatos));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));//ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosAsegurabilidad() -> " + ex.Message);
            }

            return response;
        }


        [HttpPost]
        [Route("api/Asegurabilidad/GuardarDatosPolizasExternas")]
        public HttpResponseMessage GuardaDatosPolizasExternas(ModeloCifrado StringCifrado)
        {
            HttpResponseMessage response;
            DatosDocumento JsonObject = new DatosDocumento();
            try
            {
                JsonObject = JsonConvert.DeserializeObject<DatosDocumento>(_cifradoDescifrado.decryptString(StringCifrado.Valor));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardaDatosPolizasExternas(ModeloString: " + StringCifrado.Valor + ")");

                List<PolizasExternas> lstDatos = new List<PolizasExternas>();

                foreach (var item in JsonObject.DatosExcel)
                {
                    if (item[2].Length > 0)
                    {
                        PolizasExternas Poliza = new PolizasExternas();

                        Poliza.Oficina = item[1];
                        Poliza.Tipo_documento = item[2];
                        Poliza.Cedula = item[3].Trim();
                        Poliza.Nombre_Apellidos = item[4];

                        Poliza.Aseguradora = item[5];
                        Poliza.Nombre_poliza = item[6];
                        Poliza.Numero_Poliza = item[7];
                        Poliza.Valor_Asegurado = Convert.ToDouble(item[8].Replace(",", "").Replace("'", "").Replace("$", "").Replace(".", "").Replace(" ", "").Trim());
                        Poliza.Tomador = item[9];
                        Poliza.Nit_Tomador = item[10];

                        Poliza.Vigencia_Desde = DateTime.ParseExact(item[14].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        Poliza.Vigencia_Hasta = DateTime.ParseExact(item[15].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        Poliza.Producto_Amparado = item[16];

                        Poliza.TipoNitTomador = "CC";

                        Poliza.Aplicar_Colectiva = 0;

                        Poliza.Activo = "1";


                        if (Poliza.Nombre_poliza.Contains("incendio"))
                        {
                            if (19 == item.Length)          {

                                Poliza.Direccion = item[17];
                                Poliza.Matricula_Inmobiliaria = item[18];
                            }
                            else if (20 == item.Length)
                            {
                                Poliza.Direccion = item[17];
                                Poliza.Matricula_Inmobiliaria = item[18];
                                Poliza.Observaciones = item[19];
                            }
                        }
                        else  if (18 <= item.Length)
                        {
                            
                            if (18 == item.Length)
                            {
                                Poliza.Documento_No = item[17];
                                var partes = Poliza.Documento_No.Split('-');
                                Poliza.Documento_No = partes[0];
                                
                            }                            
                            else
                            {
                                Poliza.Documento_No = item[17];
                                var partes = Poliza.Documento_No.Split('-');
                                Poliza.Documento_No = partes[0];
                                Poliza.Observaciones = item[18];
                            }
                           
                        }
                        
                        lstDatos.Add(Poliza);
                    }
                  
                }
                
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(asegurabilidadFacade.InsertarDatosPolizasExternas(lstDatos))));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error GuardaDatosPolizasExternas() -> " + ex.Message);
            }

            return response;
        }
        [HttpPost]
        [Route("api/Asegurabilidad/AdministrarPolizasExternas")]
        public HttpResponseMessage AdministrarDatosPolizasExternas(ModeloCifrado StringCifrado)
        {
            HttpResponseMessage response;
            ViewModelPolizasExternas JsonObject = new ViewModelPolizasExternas();
            try
            {
                string des = _cifradoDescifrado.decryptString(StringCifrado.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio AdministrarDatosPolizasExternas(ModeloString: " + StringCifrado.Valor + ")");

                JsonObject = JsonConvert.DeserializeObject<ViewModelPolizasExternas>(des, new JsonSerializerSettings { DateFormatString = "yyyy-MM-ddTH:mm:ss.fffZ", DateTimeZoneHandling = DateTimeZoneHandling.Utc });
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(asegurabilidadFacade.AdministrarPolizasExternas(JsonObject))));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error AdministrarDatosPolizasExternas() -> " + ex.Message);
            }

            return response;
        }

        [HttpGet]
        [Route("api/Asegurabilidad/ConsultarInformacionPoliza")]
        //public HttpResponseMessage ConsultarInformacionPoliza(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza, int? DiasVencimiento, string EstadoPoliza)
        public HttpResponseMessage ConsultarInformacionPoliza(string TipoIdentificacion, 
                                                              string NumeroDocumento, 
                                                              string Nombres, 
                                                              string NumeroPoliza, 
                                                              string DiasVencimiento, 
                                                              string EstadoPoliza)
        {
            HttpResponseMessage response;
            try
            {
                TipoIdentificacion = _cifradoDescifrado.decryptString(TipoIdentificacion);
                NumeroDocumento = _cifradoDescifrado.decryptString(NumeroDocumento);
                Nombres = _cifradoDescifrado.decryptString(Nombres);
                NumeroPoliza = _cifradoDescifrado.decryptString(NumeroPoliza);
                DiasVencimiento = _cifradoDescifrado.decryptString(DiasVencimiento);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsultarInformacionPoliza( TipoIdentificacion: " + TipoIdentificacion +
                    ", NumeroDocumento: " + NumeroDocumento + ", Nombres: " + Nombres + ", NumeroPoliza: " + NumeroPoliza + ", DiasVencimiento: " + DiasVencimiento +
                    ", EstadoPoliza: " + EstadoPoliza + ")");

                int intDiasVencimiento = String.IsNullOrEmpty(DiasVencimiento) ? 0 : int.Parse(DiasVencimiento);
                EstadoPoliza = _cifradoDescifrado.decryptString(EstadoPoliza);
                //TODO: Obtener parámetro para adquirir el informe histórico
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(asegurabilidadFacade.ConsultarPolizasExternas(TipoIdentificacion,NumeroDocumento, Nombres, NumeroPoliza, intDiasVencimiento, EstadoPoliza))));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ConsultarInformacionPoliza() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Asegurabilidad/ObtenerAseguradoras")]
        public IHttpActionResult ObtenerAseguradoras()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerAseguradoras()");

                var lista = asegurabilidadFacade.ObtenerAseguradoras().ToList();

                return Ok(_cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAseguradoras() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Asegurabilidad/ObtenerTipoPoliza")]
        public IHttpActionResult ObtenerTipoPoliza()
        {
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerTipoPoliza()");

                var lista = asegurabilidadFacade.ObtenerTipoPoliza().ToList();

                return Ok(_cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerTipoPoliza() -> " + ex.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("api/Asegurabilidad/EliminarPolizasExternas")]
        public HttpResponseMessage EliminarPolizasExternas(string TipoIdentificacion, 
                                                           string NumeroIdentificacion, 
                                                           string Aseguradora, 
                                                           string NumeroPoliza)
        {
            HttpResponseMessage response;
            try
            {
                TipoIdentificacion = _cifradoDescifrado.decryptString(TipoIdentificacion);
                NumeroIdentificacion = _cifradoDescifrado.decryptString(NumeroIdentificacion);
                Aseguradora = _cifradoDescifrado.decryptString(Aseguradora);
                NumeroPoliza = _cifradoDescifrado.decryptString(NumeroPoliza);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio EliminarPolizasExternas( TipoIdentificacion: " + TipoIdentificacion +
                ", NumeroIdentificacion: " + NumeroIdentificacion + ", Aseguradora: " + Aseguradora + ", NumeroPoliza: " + NumeroPoliza + ")");

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(asegurabilidadFacade.EliminarPolizaExterna(TipoIdentificacion, NumeroIdentificacion, Aseguradora, NumeroPoliza))));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error EliminarPolizasExternas() -> " + ex.Message);
            }

            return response;
        }
        [HttpGet]
        [Route("api/Asegurabilidad/ConsultarHistoricoPoliza")]
        public HttpResponseMessage ConsultarHistoricoPoliza(string TipoIdentificacion, 
                                                            string NumeroDocumento, 
                                                            string Nombres, 
                                                            string NumeroPoliza)
        {
            HttpResponseMessage response;
            try
            {
                //TO DO: Obtener parámetro para adquirir el informe histórico
                TipoIdentificacion = _cifradoDescifrado.decryptString(TipoIdentificacion);
                NumeroDocumento = _cifradoDescifrado.decryptString(NumeroDocumento);
                Nombres = _cifradoDescifrado.decryptString(Nombres);
                NumeroPoliza = _cifradoDescifrado.decryptString(NumeroPoliza);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsultarHistoricoPoliza( TipoIdentificacion: " + TipoIdentificacion +
                ", NumeroDocumento: " + NumeroDocumento + ", Nombres: " + Nombres + ", NumeroPoliza: " + NumeroPoliza + ")");

                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(asegurabilidadFacade.ConsultarHistoricoPoliza(TipoIdentificacion, NumeroDocumento, Nombres, NumeroPoliza))));
            }
            catch (Exception ex)

            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ConsultarHistoricoPoliza() -> " + ex.Message);
            }
            return response;
        }


        [HttpPost]
        [Route("api/Asegurabilidad/ValidarPolizasExternas")]
        //public HttpResponseMessage ValidarPolizasExternas(DatosDocumento JsonObject)
        public HttpResponseMessage ValidarPolizasExternas(ModeloCifrado StringCifrado)
        {
            HttpResponseMessage response;
            DatosDocumento JsonObject = new DatosDocumento();
            try
            {
                JsonObject = JsonConvert.DeserializeObject<DatosDocumento>(_cifradoDescifrado.decryptString(StringCifrado.Valor));

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ValidarPolizasExternas( ModeloString: " + StringCifrado.Valor + ")");

                List <PolizasExternas> lstDatos = new List<PolizasExternas>();
                var i = 1;

                foreach (var item in JsonObject.DatosExcel)
                {


                    if (item[2].Length > 0)
                    {

                        PolizasExternas Poliza = new PolizasExternas();

                        Poliza.Oficina = item[1];
                        Poliza.Tipo_documento = item[2];
                        Poliza.Cedula = item[3].Trim() ;
                        Poliza.Nombre_Apellidos = item[4];

                        Poliza.Aseguradora = item[5];
                        Poliza.Nombre_poliza = item[6];
                        Poliza.Numero_Poliza = item[7];
                        Poliza.Valor_Asegurado = Convert.ToDouble(item[8].Replace(",", "").Replace("'", "").Replace("$", "").Replace(".", "").Replace(" ", "").Trim());
                        Poliza.Tomador = item[9];
                        Poliza.Nit_Tomador = item[10];

                        Poliza.Vigencia_Desde = DateTime.ParseExact(item[14].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture); 


                        Poliza.Vigencia_Hasta = DateTime.ParseExact(item[15].Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        Poliza.Producto_Amparado = item[16];


                        if (Poliza.Nombre_poliza.Contains("incendio"))
                        {
                            if (19 == item.Length)
                            {

                                Poliza.Direccion = item[17];
                                Poliza.Matricula_Inmobiliaria = item[18];
                            }
                            else if (20 == item.Length)
                            {
                                Poliza.Direccion = item[17];
                                Poliza.Matricula_Inmobiliaria = item[18];
                                Poliza.Observaciones = item[19];
                            }
                        }
                        else if (18 <= item.Length)
                        {

                            if (18 == item.Length)
                            {
                               


                                Poliza.Documento_No = item[17];
                                var partes = Poliza.Documento_No.Split('-');

                                Poliza.Documento_No = partes[0];

                            }
                            else
                            {
                                Poliza.Documento_No = item[17];

                                var partes = Poliza.Documento_No.Split('-');

                                Poliza.Documento_No = partes[0];
                                Poliza.Observaciones = item[18];
                            }

                        }
                        PolizasExternas PolizaValidacion = new PolizasExternas();
                        
                        var Respuesta = asegurabilidadFacade.ValidarPolizasExternas(Poliza.Aseguradora, Poliza.Nombre_poliza, Poliza.Producto_Amparado, Poliza.Documento_No, Poliza.Cedula);
                        if (!String.IsNullOrEmpty(Respuesta.Respuesta))
                           
                        {
                            PolizaValidacion.Validacion = Respuesta.Respuesta;
                           
                        }
                        PolizaValidacion.id = i;
                        lstDatos.Add(PolizaValidacion);
                        i++;
                    }                    
                }
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lstDatos))); 
            }
            catch (Exception ex)
            {
                 response = Request.CreateResponse(HttpStatusCode.BadRequest, _cifradoDescifrado.encryptString(ex.ToString()));
                logger.Error("Usuario: " + this.usuario + " -> Error ValidarPolizasExternas() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Asegurabilidad/ObtenerPolizasVigentes")]
        public HttpResponseMessage ObtenerPolizasVigentes(string tipo, string numero)
        {
            HttpResponseMessage response;
            try
            {

                tipo = _cifradoDescifrado.decryptString(tipo);
                numero = _cifradoDescifrado.decryptString(numero);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerPolizasVigentes( tipo: " + tipo + ", numero" + numero + ")");

                RespuestaNegocio<IEnumerable<VMPolizasVigentes>> respuestaNegocio = asegurabilidadFacade.ObtenerPolizasVigentes(tipo, numero);
                string json = JsonConvert.SerializeObject(respuestaNegocio);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDescifrado.encryptString(json));

                //response = Request.CreateResponse(HttpStatusCode.OK, asegurabilidadFacade.ObtenerPolizasVigentes(tipo, numero));
            }
            catch (Exception ex)

            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerPolizasVigentes() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/Consultas/ObtenerProductoPorAsociado")]
        public IHttpActionResult ObtenerProductoPorAsociado(string Cedula)
        {
            try
            {
                Cedula = _cifradoDescifrado.decryptString(Cedula);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerProductoPorAsociado( Cedula: " + Cedula + ")");

                var convertidor = new ConvertirAListaSeleccionable();
                var lista = asegurabilidadFacade.ObtenerProductoPorAsociado(Cedula).ToList();
                return Ok(_cifradoDescifrado.encryptString(JsonConvert.SerializeObject(lista)));
            }
            catch (Exception ex)
            {
                Debug.Print(ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerProductoPorAsociado() -> " + ex.Message);
            }
            return Ok();
        }

    }
}