﻿using Cavipetrol.SICSES.Facade;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Cavipetrol.SICSES.Infraestructura.Model;
using Newtonsoft.Json.Linq;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.ReportesSICSES;
using Cavipetrol.SICSES.Infraestructura.Utilidades;

namespace Cavipetrol.SICSES.Servicios.Controllers
{
    //[Authorize]
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class ReportsSICSESController : ApiController
    {
        private readonly IReportsSICSESFacade reportSICSESFacade;

        //CLASE PARA CIFRADO DE PARAMETROS
        private Cifrado.CifradoDescifrado _cifradoDesifrado;

        private static readonly log4net.ILog logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string usuario = "";

        public ReportsSICSESController()
        {
            reportSICSESFacade = new ReportsSICSESFacade();
            _cifradoDesifrado = new Cifrado.CifradoDescifrado();
            this.usuario = ((System.Security.Claims.ClaimsIdentity)((System.Security.Claims.ClaimsPrincipal)this.User)
                .Identity).NameClaimType;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformacionEstadistica/")]
        public HttpResponseMessage getInformacionEstadistica(string idReporte, //int
                                                             string mes, //int 
                                                             string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;  
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio getInformacionEstadistica(" 
                    + "idReporte: " + idReporte
                    + ", mes: " + mes
                    + ", ano: " + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);

                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteInformacionEstadistica>> respuesta = reportSICSESFacade.ObtenerReporteInfromacionEstadisticas(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));    

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error getInformacionEstadistica() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformacionRegistroCavipetrol")]
        public HttpResponseMessage ObtenerInformacionCavipetrol()
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerInformacionCavipetrol()");

                IEnumerable<ParametroUsuarioCavipetrol> respuesta = reportSICSESFacade.ObtenerParametrosRegistroCavipetrol();
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerInformacionCavipetrol() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
            }

            return response;
        }

        [HttpPost]
        [Route("api/ReportsSICSES/InsertarInformacionRegistro")]
        public HttpResponseMessage InsertarInformacionRegistroCavipetrol([FromBody]ModeloString modeloString) //ParametroUsuarioCavipetrol 
        {
            HttpResponseMessage response;
            ParametroUsuarioCavipetrol dtoParametroUsuarioCavipetrol = new ParametroUsuarioCavipetrol();
            try
            {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio InsertarInformacionRegistroCavipetrol(ModeloString: " + modeloString.Valor + ")");

                dtoParametroUsuarioCavipetrol = JsonConvert.DeserializeObject<ParametroUsuarioCavipetrol>(modeloString.Valor);

                bool estadoOperacion = reportSICSESFacade.ActulizarParametrosCavipetrol(dtoParametroUsuarioCavipetrol);
                if (estadoOperacion)
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString("Se ha actualizado exitosamente."));
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.NotModified, _cifradoDesifrado.encryptString("No se ha actualizado ningún parametro."));
                }
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error InsertarInformacionRegistroCavipetrol() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
            }

            return response;
        }

        [HttpPost]
        [Route("api/ReportsSICSES/InsertarListadoHistorial")]
        public HttpResponseMessage InsertarListadoHistorial([FromBody]ModeloString modeloString) //IEnumerable<JObject>
        {
            HttpResponseMessage response;
            try
            {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                logger.Debug("Usuario: " + this.usuario + " -> Inicio InsertarListadoHistorial(ModeloString: " + modeloString.Valor + ")");
                IEnumerable<JObject> JsonObject = JsonConvert.DeserializeObject<IEnumerable<JObject>>(modeloString.Valor);

                bool estadoOperacion = reportSICSESFacade.InsertarHistoricoReporte(
                    new ReporteHistorico()
                    {
                        NumeroReporte = 10,
                        DatosReporte = JsonObject.ToString(),
                        FechaReporte = DateTime.Now
                    });

                string json = JsonConvert.SerializeObject(estadoOperacion);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error InsertarListadoHistorial() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
            }

            return response;
        }

        [HttpPost]
        [Route("api/ReportsSICSES/InsertarObjetoHistorial")]
        public HttpResponseMessage InsertarObjetoHistorial([FromBody]JObject JsonObject)
        {
            HttpResponseMessage response;
            try
            {
                logger.Debug("Usuario: " + this.usuario + " -> Inicio InsertarObjetoHistorial(JObject: " + JsonObject + ")");
                response = Request.CreateResponse(HttpStatusCode.OK, JsonObject);

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error InsertarObjetoHistorial() -> " + ex.Message);
            }

            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeOrganosDirreccionControl")]
        public HttpResponseMessage GetInformeOrganosDireccionControl(string idReporte, //int 
                                                                     string mes, //int 
                                                                     string ano) //int 
        {
            HttpResponseMessage response;
            int intIdReporte = 0; //int 
            int intMes = 0; //int 
            int intAno = 0; //int 

            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GetInformeOrganosDireccionControl(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);

                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteOrganosDireccionyControl>> respuesta = reportSICSESFacade.ObtenerReporteOrganosDirreccionControl(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
                
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error GetInformeOrganosDireccionControl() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeUsuarios")]
        public HttpResponseMessage ObtenerInformeUsuarios(string idReporte, //int
                                                          string mes, //int
                                                          string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio InformeUsuarios(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);

                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteUsuarios>> respuesta = reportSICSESFacade.ObtenerReporteUsuarios(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error InformeUsuarios() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeRedOficinasCorresponsalesNoBancarios")]
        public HttpResponseMessage ObtenerInformeRedOficinasCorresponsalesNoBancarios(string idReporte, //int
                                                                                      string mes, //int
                                                                                      string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio InformeRedOficinasCorresponsalesNoBancarios(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteRedOficinasYCorresponsalesNoBancarios>> respuesta = reportSICSESFacade.ObtenerReporteRedOficinasYCorresponsalesNoBancarios(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error InformeRedOficinasCorresponsalesNoBancarios() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/ConsultaHistorico")]
        public HttpResponseMessage ConsultaReporteHistorico([FromUri] string idreport, //int
                                                            [FromUri] string mes, //int
                                                            [FromUri] string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idreport = _cifradoDesifrado.decryptString(idreport);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ConsultaReporteHistorico(idReporte: " + idreport + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idreport, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                ReporteHistorico respuesta = reportSICSESFacade.ConsultarHistorico(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ConsultaReporteHistorico() -> " + ex.Message);
            }
            return response;
        }


        [HttpGet]
        [Route("api/ReportsSICSES/InformeParentescos")]
        public HttpResponseMessage ObtenerInformeParentescos(string idReporte, //int
                                                             string mes, //int
                                                             string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerInformeParentescos(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteParentescos>> respuesta = reportSICSESFacade.ObtenerInformeParentescos(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerInformeParentescos() -> " + ex.Message);
            }
            return response;
        }

        [HttpPost]
        [Route("api/ReportsSICSES/GuardarHistorial")]
        public HttpResponseMessage GuardarHistorial(ModeloString modeloString) //ReporteHistorico
        {
            HttpResponseMessage response;
            ReporteHistorico reporteHistorico = new ReporteHistorico();
            try
            {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                reporteHistorico = JsonConvert.DeserializeObject<ReporteHistorico>(modeloString.Valor);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio GuardarHistorial(ModeloString: " + modeloString.Valor + ")");

                bool respuesta = reportSICSESFacade.InsertarHistoricoReporte(reporteHistorico);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
                //response = Request.CreateResponse(HttpStatusCode.OK, respuesta);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error GuardarHistorial() -> " + ex.Message);
            }

            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeRelacionBienesPagoRecibidos")]
        public HttpResponseMessage ObtenerRelacionBienesPAgoRecibidos(string idReporte, //int
                                                                      string mes, //int
                                                                      string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerRelacionBienesPAgoRecibidos(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteRelacionBienesRecibosPago>> respuesta = reportSICSESFacade.ObtenerRelacionBienesPagoRecibidos(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerRelacionBienesPAgoRecibidos() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeIndividualCarteraCredito")]
        public HttpResponseMessage InformeIndividualCarteraCredito(string Anho, string mes)
        {
            HttpResponseMessage response;
            try
            {
                Anho = _cifradoDesifrado.decryptString(Anho);
                mes = _cifradoDesifrado.decryptString(mes);
                //TO DO: Obtener parámetro para adquirir el informe histórico

                logger.Debug("Usuario: " + this.usuario + " -> Inicio EliminarHistoricoInformes(Anho: " + Anho + ", mes: " + mes + ")");
                RespuestaReporte<IEnumerable<ReporteIndividualCarteraCredito>> rta = reportSICSESFacade.ObtenerInformacionCarteraCredito(Anho, mes);
                response = Request.CreateResponse(HttpStatusCode.OK, rta);

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error EliminarHistoricoInformes() -> " + ex.Message);

            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeInformeIndividualCaptaciones")]
        public HttpResponseMessage ObtenerInformeIndividualCaptaciones(string idReporte, //int
                                                                       string mes, //int
                                                                       string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerInformeIndividualCaptaciones(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteInformeIndividualCaptaciones>> respuesta = reportSICSESFacade.ObtenerInformeIndividualCaptacioness(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerInformeIndividualCaptaciones() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeInformePUC")]
        public HttpResponseMessage ObtenerInformePlanUnicoCuenta(string idReporte, //int
                                                                 string mes, //int
                                                                 string ano, //int
                                                                 string periodo) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            int intPeriodo = 0; 
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);
                periodo = _cifradoDesifrado.decryptString(periodo);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerInformePlanUnicoCuenta(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ", periodo" + periodo + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                int.TryParse(periodo, out intPeriodo);

                //TO DO: Obtener parámetro para adquirir el informe histórico

                RespuestaReporte<IEnumerable<ReportePUC>> respuesta = reportSICSESFacade.ObtenerInformePUC(intIdReporte, intMes, intAno, intPeriodo);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerInformePlanUnicoCuenta() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeAportesContribuciones")]
        public HttpResponseMessage ObtenerReporteAportesContribuciones(string idReporte, //int
                                                                       string mes, //int
                                                                       string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerReporteAportesContribuciones(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteAportesContribuciones>> respuesta = reportSICSESFacade.ObtenerReporteAportesContribuciones(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerReporteAportesContribuciones() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeRelacionPropiedadesPlantaEquipo")]
        public HttpResponseMessage ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(string idReporte, //int
                                                                                        string mes, //int
                                                                                        string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteRelacionPropiedadesPlantaEquipo>> respuesta = 
                    reportSICSESFacade.ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerReporteInformeRelacionPropiedadesPlantaEquipo() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeRelacionInversiones")]
        public HttpResponseMessage ObtenerReporteInformeRelacionInversiones(string idReporte, //int 
                                                                            string mes, //int 
                                                                            string ano) //int 
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerReporteInformeRelacionInversiones(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteRelacionInversiones>> respuesta = reportSICSESFacade.ObtenerReporteInformeRelacionInversiones(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerReporteInformeRelacionInversiones() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeProductosOfrecidos")]
        public HttpResponseMessage ObtenerReporte4Uiaf(string idReporte, //int 
                                                       string mes, //int 
                                                       string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerReporte4Uiaf(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);

                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteProductosOfrecidos>> respuesta = reportSICSESFacade.ObtenerReporte4Uiaf(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerReporte4Uiaf() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeAplicacionExcedentes")]
        public HttpResponseMessage ObtenerAplicacionExcedentes(string idReporte, //int
                                                               string mes, //int
                                                               string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerAplicacionExcedentes(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ClasificacionExcedentes>> respuesta = reportSICSESFacade.ObtenerAplicacionExcedentes(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerAplicacionExcedentes() -> " + ex.Message);
            }
            return response;
        }


        [HttpGet]
        [Route("api/ReportsSICSES/ObtenerEducacionFormal/")]
        public HttpResponseMessage getEducacionFormal(string idReporte, //int 
                                                      string mes, //int
                                                      string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio getEducacionFormal(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteEducacionFormal>> respuesta = reportSICSESFacade.ObtenerEducacionFormal(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error getEducacionFormal() -> " + ex.Message);
            }
            return response;
        }



        [HttpGet]
        [Route("api/ReportsSICSES/ObtenerCreditosBancariosyFinan/")]
        public HttpResponseMessage getCreditosBancariosyFinan(string idReporte, //int
                                                              string mes, //int 
                                                              string ano) //int 
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio getCreditosBancariosyFinan(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteCreditosBancosFinancieros>> respuesta = reportSICSESFacade.ObtenerCreditosBancariosyFinan(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error getCreditosBancariosyFinan() -> " + ex.Message);
            }
            return response;
        }


        [HttpGet]
        [Route("api/ReportsSICSES/ProcesosJudiciales/")]
        public HttpResponseMessage getProcesosJudiciales(string idReporte, //int
                                                         string mes, //int
                                                         string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio getProcesosJudiciales(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteProcesosJudiciales>> respuesta = reportSICSESFacade.ObtenerProcesosJudiciales(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error getProcesosJudiciales() -> " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/ObtenerHistoricoInformes/")]
        public HttpResponseMessage obtenerHistoricoInformes(string idReporte, //int
                                                            string mes, //int
                                                            string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0; //int
            int intMes = 0;//int
            int intAno = 0; //int

            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio obtenerHistoricoInformes(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);

                string jsonResponse = "";

                //TO DO: Obtener parámetro para adquirir el informe histórico
                switch (intIdReporte) {
                    case (int)enumReportesSICSES.ReporteProcesosJudiciales:
                        RespuestaReporte<IEnumerable<ReporteProcesosJudiciales>> respuestaJudiciales = reportSICSESFacade.ObtenerProcesosJudiciales(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaJudiciales);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteIngresosRecibidosParaTerceros:
                        RespuestaReporte<IEnumerable<ReporteIngresosRecibidosParaTerceros>> respuestaTerceros = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteIngresosRecibidosParaTerceros>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaTerceros);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteInversionEducacionFormal:
                        RespuestaReporte<IEnumerable<ReporteEducacionFormal>> respuestaFormal = reportSICSESFacade.ObtenerEducacionFormal(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaFormal);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteFondoDeLiquidez:
                        RespuestaReporte<IEnumerable<ReporteFondoDeLiquidez>> respuestaLiquidez = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteFondoDeLiquidez>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaLiquidez);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteEvaluacionRiesgoLiquidez:
                        RespuestaReporte<IEnumerable<EvaluacionRiesgoLiquidez>> respuestaRiesgoLiquidez = reportSICSESFacade.ObtenerHistoricoInformesSICSES<EvaluacionRiesgoLiquidez>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaRiesgoLiquidez);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteInformacionNoReportada:
                        RespuestaReporte<IEnumerable<ReporteInformacionNoReportada>> respuestaNoReportada = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteInformacionNoReportada>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaNoReportada);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteindividualDeCarteraDeCredito:
                        RespuestaReporte<IEnumerable<ReporteIndividualDeCarteraDeCredito>> respuestaCarteraCredito = reportSICSESFacade.ObtenerReporteindividualDeCarteraDeCredito(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaCarteraCredito);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteAplicacionExcedentes:
                        RespuestaReporte<IEnumerable<ReporteAplicacionExcedentes>> respuestaExcedentes = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteAplicacionExcedentes>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaExcedentes);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteInformeErogacionesOrganosDeAdministracionyControl:
                        RespuestaReporte<IEnumerable<ReporteInformeErogacionesOrganosDeAdministracionyControl>> respuestaAdminControl = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteInformeErogacionesOrganosDeAdministracionyControl>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaAdminControl);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.CreditosBancosyFinanciero:
                        RespuestaReporte<IEnumerable<ReporteCreditosBancosFinancieros>> respuestaFinanciero = reportSICSESFacade.ObtenerCreditosBancariosyFinan(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaFinanciero);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteRetiroeingresoAsociados:
                        RespuestaReporte<IEnumerable<ReporteIngresoyRetiro>> respuestaIngresoAsociado = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteIngresoyRetiro>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaIngresoAsociado);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteDeudoresPorVenta:
                        RespuestaReporte<IEnumerable<DeudoresPorVenta>> respuestaDeudoresPorVenta = reportSICSESFacade.ObtenerHistoricoInformesSICSES<DeudoresPorVenta>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaDeudoresPorVenta);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteInformeCuentasPorPagarOtras:
                        RespuestaReporte<IEnumerable<ReporteInformeCuentasPorPagarOtras>> respuestaCuentasPorVenta = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteInformeCuentasPorPagarOtras>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaCuentasPorVenta);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteActivosCastigados:
                        RespuestaReporte<IEnumerable<ReporteActivosCastigados>> respuestaActivosCastigados = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteActivosCastigados>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaActivosCastigados);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteActivosReferidos:
                        RespuestaReporte<IEnumerable<ReporteActivosReferidos>> respuestaReferidos = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteActivosReferidos>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaReferidos);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteInfoGrupoInteres:
                        RespuestaReporte<IEnumerable<InformacionRelacionadaGrupoInteres>> respuestaGrupoInteres = reportSICSESFacade.ObtenerHistoricoInformesSICSES<InformacionRelacionadaGrupoInteres>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaGrupoInteres);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteInformeDeudoresPatronalesyEmpresas:
                        RespuestaReporte<IEnumerable<ReporteInformeDeudoresPatronalesyEmpresas>> respuestaPatronalesEmpresas = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteInformeDeudoresPatronalesyEmpresas>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaPatronalesEmpresas);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteContratacion:
                        RespuestaReporte<IEnumerable<ReporteContratacion>> respuestaContratacion = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteContratacion>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaContratacion);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;                    
                    case (int)enumReportesSICSES.ReporteRelacionDeInversion:
                        RespuestaReporte<IEnumerable<ReporteRelacionInversiones>> respuestaRelacionDeInversion = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteRelacionInversiones>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaRelacionDeInversion);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;
                    case (int)enumReportesSICSES.ReporteCompraVentaCredito:
                        RespuestaReporte<IEnumerable<ReporteCompraVentaCreditos>> respuestaCompraVentaCredito = reportSICSESFacade.ObtenerHistoricoInformesSICSES<ReporteCompraVentaCreditos>(intIdReporte, intMes, intAno);
                        jsonResponse = JsonConvert.SerializeObject(respuestaCompraVentaCredito);
                        response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(jsonResponse));
                        break;

                    default:
                        response = null;
                        break;
                }
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error obtenerHistoricoInformes() -> " + ex.Message);
            }
            return response;
        }
        [HttpPost]
        [Route("api/ReportsSICSES/EliminarHistoricoInformes/")]
        public HttpResponseMessage EliminarHistoricoInformes(ModeloString modeloString)
        {
            HttpResponseMessage response;
            try
            {
                modeloString.Valor = _cifradoDesifrado.decryptString(modeloString.Valor);
                //TO DO: Obtener parámetro para adquirir el informe histórico

                logger.Debug("Usuario: " + this.usuario + " -> Inicio EliminarHistoricoInformes(ModeloString: " + modeloString.Valor + ")");

                response = null;                                        
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error EliminarHistoricoInformes() -> " + ex.Message);

            }
            return response;
        }

        [HttpGet]
        [Route("api/ReportsSICSES/InformeRetiroeIngresoAsociados")]
        public HttpResponseMessage ObtenerInformeRetiroeIngresoAsociados(string idReporte, //int
                                                                         string mes, //int
                                                                         string ano) //int
        {
            HttpResponseMessage response;
            int intIdReporte = 0;
            int intMes = 0;
            int intAno = 0;
            try
            {
                idReporte = _cifradoDesifrado.decryptString(idReporte);
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio ObtenerInformeRetiroeIngresoAsociados(idReporte: " + idReporte + ", mes: " + mes + ", año" + ano + ")");

                int.TryParse(idReporte, out intIdReporte);
                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);
                //TO DO: Obtener parámetro para adquirir el informe histórico
                RespuestaReporte<IEnumerable<ReporteRetiroeIngresoAsociados>> respuesta = reportSICSESFacade.ObtenerInformeRetiroeIngresoAsociados(intIdReporte, intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
                logger.Error("Usuario: " + this.usuario + " -> Error ObtenerInformeRetiroeIngresoAsociados() -> " + ex.Message);
            }
            return response; 
        }
        
        [HttpGet]
        [Route("api/ReportsSICSES/DetalleInformacionEstadistica/")]
        public HttpResponseMessage getDetalleInformacionEstadistica(string mes, 
                                                                    string ano) 
        {
            HttpResponseMessage response;
            int intMes = 0;
            int intAno = 0;
            try
            {
                mes = _cifradoDesifrado.decryptString(mes);
                ano = _cifradoDesifrado.decryptString(ano);

                logger.Debug("Usuario: " + this.usuario + " -> Inicio getDetalleInformacionEstadistica("
                    + ", mes: " + mes
                    + ", ano: " + ano + ")");

                int.TryParse(mes, out intMes);
                int.TryParse(ano, out intAno);

                RespuestaReporte<IEnumerable<ReporteDetalleInformacionEstadistica>> respuesta = reportSICSESFacade.ObtenerReporteDetalleInformacionEstadistica(intMes, intAno);
                string json = JsonConvert.SerializeObject(respuesta);
                response = Request.CreateResponse(HttpStatusCode.OK, _cifradoDesifrado.encryptString(json));

            }
            catch (Exception ex)
            {
                logger.Error("Usuario: " + this.usuario + " -> Error getDetalleInformacionEstadistica() -> " + ex.Message);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
            return response;
        }

    }
}