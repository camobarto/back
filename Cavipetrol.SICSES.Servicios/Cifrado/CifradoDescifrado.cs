﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;

namespace Cavipetrol.SICSES.Servicios.Cifrado
{
    public class CifradoDescifrado
    {
        private byte[] key;
        private byte[] iv; 

        private int keySize;
        private int ivSize;

        public CifradoDescifrado() {
            key = UTF8Encoding.UTF8.GetBytes(WebConfigurationManager.AppSettings["KEY"]);
            iv = UTF8Encoding.UTF8.GetBytes(WebConfigurationManager.AppSettings["KEY"]);
            keySize = 32;
            ivSize = 16;

            Array.Resize(ref key, keySize);
            Array.Resize(ref iv, ivSize);
        }

        /**
         * Descifra una cadena texto con el algoritmo de Rijndael
         *
         * @param	encryptedMessage	mensaje cifrado
         * @param	Key			clave del cifrado para Rijndael
         * @param	IV			vector de inicio para Rijndael
         * @return	string			texto descifrado (plano)
         */
        public string decryptString(String encryptedMessage)
        {
            encryptedMessage = encryptedMessage.Replace("-","+").Replace(".", "=").Replace("_", "/");

            // Obtener la representación en bytes del texto 
            byte[] cipherTextBytes = Convert.FromBase64String(encryptedMessage);

            // Crear un arreglo de bytes para almacenar los datos descifrados
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            // Crear una instancia del algoritmo de 
            Rijndael RijndaelAlg = Rijndael.Create();

            // Crear un flujo en memoria con la representación de bytes de la información cifrada
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

            // Crear un flujo de descifrado basado en el flujo de los datos
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                         RijndaelAlg.CreateDecryptor(key, iv),
                                                         CryptoStreamMode.Read);

            // Obtener los datos descifrados obteniéndolos del flujo de descifrado
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

            // Cerrar los flujos utilizados
            memoryStream.Close();
            cryptoStream.Close();

            // Retornar la representación de texto de los datos descifrados
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }

        /**
         * Cifra una cadena texto con el algoritmo de Rijndael
         *
         * @param	plainMessage	mensaje plano (sin cifrar)
         * @param	Key		        clave del cifrado para Rijndael
         * @param	IV		        vector de inicio para Rijndael
         * @return	string		        texto cifrado
         */
        public string encryptString(String plainMessage)
        {
            // Crear una instancia del algoritmo de Rijndael
            Rijndael RijndaelAlg = Rijndael.Create();

            // Establecer un flujo en memoria para el cifrado
            MemoryStream memoryStream = new MemoryStream();

            // Crear un flujo de cifrado basado en el flujo de los datos
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                         RijndaelAlg.CreateEncryptor(key, iv),
                                                         CryptoStreamMode.Write);

            // Obtener la representación en bytes de la información a cifrar
            byte[] plainMessageBytes = UTF8Encoding.UTF8.GetBytes(plainMessage);

            // Cifrar los datos enviándolos al flujo de cifrado
            cryptoStream.Write(plainMessageBytes, 0, plainMessageBytes.Length);
            cryptoStream.FlushFinalBlock();

            // Obtener los datos datos cifrados como un arreglo de bytes
            byte[] cipherMessageBytes = memoryStream.ToArray();

            // Cerrar los flujos utilizados
            memoryStream.Close();
            cryptoStream.Close();

            // Retornar la representación de texto de los datos cifrados
            return Convert.ToBase64String(cipherMessageBytes);
        }
    }
}