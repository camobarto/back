﻿using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cavipetrol.SICSES.Servicios.ViewModels
{
    public class InformacionSolicitanteGeneralAsegurabilidad
    {
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string NombreSolicitante { get; set; }
        public string TipoSolicitante { get; set; }
        public string Papel { get; set; }
        public string Registro { get; set; }        
        public string FechaIngreso { get; set; }
        public string FechaNacimiento { get; set; }
        public string Ciudad { get; set; }
        public string EstadoCivil { get; set; }
        public string Regimen { get; set; } //TODO
        public string Cargo { get; set; }
        public string DireccionResidencia { get; set; }
        public string TelefonoResidencia { get; set; }
        public int? Estrato { get; set; } //TODO
        public string DireccionOficina { get; set; }
        public string TelefonoOficina { get; set; }
        public string CorreoCorporativo { get; set; }
        public string DistritoTrabajo { get; set; } //TODO??
        public string Profesion { get; set; }
        public string Ocupacion { get; set; }
        public string NumeroMovil { get; set; } //TODO
        public string Barrio { get; set; }
        public int? NumeroHijos { get; set; } //TODO
        public int? PersonasACargo { get; set; } //TODO
        public string CorreoPersonal { get; set; }
        public decimal? IngresosMensuales { get; set; }
        public double? EgresosMensuales { get; set; }
        public decimal? TotalActivos { get; set; }
        public double? TotalPasivos { get; set; }
        public int? SalarioActual { get; set; }
        public int? OtrosIngresos { get; set; } //TODO
        public string FechaUltimaActualizacion { get; set; } //TODO
        public double? TasaExtraprima { get; set; }
        public string Observacion { get; set; }
        public bool EsAsegurable { get; set; }
        public bool EsPago { get; set; }
        public List<InformacionProductosCreditos> ProductosCreditos { get; set; }
        public double? AsegurabilidadPorcentajeExtraprima { get; set; }
        public DateTime? AsegurabilidadVigencia { get; set; }
        public string AsegurabilidadObservaciones { get; set; }
        public int? AsegurabilidadPagoAfiliado { get; set; }
        public string AsegurabilidadEstado { get; set; }
        public int? AsegurabilidadRechazado { get; set; }
        //public short? IdTipoRol { get; set; }
        public short? IdPapelCavipetrol { get; set; }
        
        public string TipoContrato { get; set; }
        public string Nomina { get; set; }
        public string CiudadOficina { get; set; }
        public string Ciiu { get; set; }
        public int? Edad { get; set; }
        public int? Antiguedad { get; set; }
        public string Ciiu2 { get; set; }
        public string FechaFinalizacionContrato { get; set; }
        public string LugarExpedicionCedula { get; set; }
        public void MappingInformacionSolicitante(InformacionSolicitante solicitante)
        {
            TipoIdentificacion = solicitante.TipoIdentificacion;
            NumeroIdentificacion = solicitante.NumeroIdentificacion;
            NombreSolicitante = solicitante.NombreSolicitante;
            TipoSolicitante = solicitante.TipoSolicitante;
            Papel = solicitante.Papel;
            LugarExpedicionCedula = solicitante.LugarExpedicionCedula;
            Registro = solicitante.Registro;
            FechaIngreso = solicitante.FechaIngreso.HasValue ? solicitante.FechaIngreso.Value.ToString("MM/dd/yyyy") : string.Empty;
            FechaNacimiento = solicitante.FechaNacimiento.HasValue ? solicitante.FechaNacimiento.Value.ToString("MM/dd/yyyy") : string.Empty;
            FechaFinalizacionContrato = solicitante.FechaFinalizacionContrato.HasValue ? solicitante.FechaFinalizacionContrato.Value.ToString("MM/dd/yyyy") : string.Empty;
            Ciudad = solicitante.Ciudad;
            EstadoCivil = solicitante.EstadoCivil;
            Regimen = solicitante.Regimen;
            Cargo = solicitante.Cargo;
            DireccionResidencia = solicitante.DireccionResidencia;
            TelefonoResidencia = solicitante.TelefonoResidencia;
            Estrato = solicitante.Estrato;
            DireccionOficina = solicitante.DireccionOficina;
            TelefonoOficina = solicitante.TelefonoOficina;
            CorreoCorporativo = solicitante.CorreoCorporativo;
            DistritoTrabajo = solicitante.DistritoTrabajo;
            Barrio = solicitante.Barrio;
            Profesion = solicitante.Profesion;
            Ocupacion = solicitante.Ocupacion;
            NumeroMovil = solicitante.NumeroMovil;
            NumeroHijos = solicitante.NumeroHijos;
            PersonasACargo = solicitante.PersonasACargo;
            CorreoPersonal = solicitante.CorreoPersonal;
            IngresosMensuales = solicitante.IngresosMensuales;
            EgresosMensuales = Convert.ToDouble(solicitante.EgresosMensuales);
            TotalActivos = solicitante.TotalActivos;
            SalarioActual = solicitante.SalarioActual;
            OtrosIngresos = solicitante.OtrosIngresos;
            FechaUltimaActualizacion = solicitante.FechaUltimaActualizacion.HasValue ? solicitante.FechaUltimaActualizacion.Value.ToString("dd/MM/yyyy") : string.Empty;
            ProductosCreditos = solicitante.Productos;
            AsegurabilidadPorcentajeExtraprima = Convert.ToDouble(solicitante.AsegurabilidadPorcentajeExtraprima);
            AsegurabilidadVigencia = solicitante.AsegurabilidadVigencia;
            AsegurabilidadObservaciones = solicitante.AsegurabilidadObservaciones;
            AsegurabilidadPagoAfiliado = solicitante.AsegurabilidadPagoAfiliado;
            AsegurabilidadEstado = solicitante.AsegurabilidadEstado;
            AsegurabilidadRechazado = solicitante.AsegurabilidadRechazado;
            IdPapelCavipetrol = solicitante.IdPapelCavipetrol;
            TipoContrato = solicitante.TipoContrato;
            Nomina = solicitante.Nomina;
            CiudadOficina = solicitante.CiudadOficina;
            TotalPasivos = Convert.ToDouble(solicitante.TotalPasivos);
            Ciiu = solicitante.Ciiu;
            Edad = solicitante.Edad;
            Antiguedad = solicitante.Antiguedad;
            Ciiu2 = solicitante.Ciiu2;
            EsAsegurable = Convert.ToBoolean(AsegurabilidadRechazado);
        }

        public void MappingInformacionAsegurabilidad(InformacionAsegurabilidad asegurabilidad)
        {
            TasaExtraprima = asegurabilidad.TasaExtraprima;
            EsAsegurable = Convert.ToBoolean(asegurabilidad.Asegurabilidad);
        }
    }
}