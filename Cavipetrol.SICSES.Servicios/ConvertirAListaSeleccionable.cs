﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using Cavipetrol.SICSES.Infraestructura.Utilidades;

namespace Cavipetrol.SICSES.Servicios
{
    public class ConvertirAListaSeleccionable
    {
        public List<SelectListItem> Convertir<T>(List<T> lista, string id = "Id", string text = "Descripcion", string idDos = "IdCavipetrol") {
            List<SelectListItem> listaSeleccionable = new List<SelectListItem>();

            if (lista.Count <= 0)
                return listaSeleccionable;


            foreach (var objeto in lista)
            {
                var propiedades = objeto.GetType().GetProperties();
                var objetoSeleccionable = new SelectListaItems();
                foreach (var propiedad in propiedades)
                {                    
                    if (propiedad.Name == id)
                    {
                        objetoSeleccionable.Value = propiedad.GetValue(objeto, null).ToString();
                    }
                    if (propiedad.Name == text)
                    {
                        objetoSeleccionable.Text = propiedad.GetValue(objeto, null).ToString();
                    }
                    if (propiedad.Name == idDos)
                    {
                        objetoSeleccionable.ValueDos = propiedad.GetValue(objeto, null).ToString();
                    }
                }

                listaSeleccionable.Add(objetoSeleccionable);
            }

            return listaSeleccionable;
        }
    }
}