﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Cavipetrol.SICSES.DAL;


namespace Cavipetrol.SICSES.Servicios.Sevicios
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private Cifrado.CifradoDescifrado _cifradoDescifrado = new Cifrado.CifradoDescifrado();
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); //   
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var user = _cifradoDescifrado.decryptString(context.UserName);
            var identity = new ClaimsIdentity(authenticationType : context.Options.AuthenticationType, nameType : user, roleType : null);
           
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            using (var db = new SICAVContexto())
            {
                if (db != null)
                {
                    var usuario = db.Usuario.ToList();
                    var perfil = db.Perfil.ToList();
                    if (usuario != null)
                    {
                        //var user = _cifradoDescifrado.decryptString(context.UserName);
                        if (!string.IsNullOrEmpty(usuario.Where(u => u.UniqueName == user).FirstOrDefault().UniqueName))
                        {
                            identity.AddClaim(new Claim("Age", "16"));
                            identity.AddClaim(new Claim("userName", user));

                            identity.Label = context.UserName;
                            var props = new AuthenticationProperties(new Dictionary<string, string>
                            {
                                {
                                    "userdisplayname", user
                                },
                                {
                                     "role", "admin"
                                }
                             });
                            
                            var ticket = new AuthenticationTicket(identity, props);  
                            
                            context.Validated(ticket);
                        }
                        else
                        {
                            context.SetError("invalid_grant", "Error: Usuario incorrecto");
                            context.Rejected();
                        }
                    }
                }
                else
                {
                    context.SetError("invalid_grant", "Error: Usuario incorrecto");
                    context.Rejected();
                }
                return;
            }
            //identity.AddClaim(new Claim("Age", "16"));
            //var props = new AuthenticationProperties(new Dictionary<string, string>
            //                {
            //                    {
            //                        "userdisplayname", context.UserName
            //                    },
            //                    {
            //                         "role", "1"
            //                    }
            //                 });

            //var ticket = new AuthenticationTicket(identity, props);
            //context.Validated(ticket);
        }
    }
}