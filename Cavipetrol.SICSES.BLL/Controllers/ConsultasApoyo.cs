﻿using Cavipetrol.SICSES.DAL.Mapeo.Reportes;
using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Menu;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.Creditos;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class ConsultasApoyo
    {
        private readonly IConsultasApoyoUnidadTrabajo _consultasApoyoUnidadTrabajo;
        private readonly ICreditosUnidadTrabajo _creditosUnidadTrabajo;
        private readonly ISicavUnidadTrabajo _sicavUnidadTrabajo;
        public ConsultasApoyo()
        {
            _consultasApoyoUnidadTrabajo = new ConsultasApoyoUnidadTrabajo();
            _creditosUnidadTrabajo = new CreditosUnidadTrabajo();
            _sicavUnidadTrabajo = new SicavUnidadTrabajo();
        }

        public IEnumerable<AsociacionEntidadSolidaria> ObtenerAsociacionesEntidadesSolidarias()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerAsociacionesEntidadesSolidarias();
        }

        public IEnumerable<Departamento> ObtenerDepartamentos()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerDepartamentos();
        }

        public IEnumerable<FormatoVigenteSICSES> ObtenerFormatosVigentesSICSES()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerFormatosVigentesSICSES();
        }

        public IEnumerable<Municipio> ObtenerMunicipiosPorIdDepartamento(string idDepartamento)
        {
            return _consultasApoyoUnidadTrabajo.ObtenerMunicipiosPorIdDepartamento(idDepartamento);
        }

        public IEnumerable<TipoNivelEscolaridad> ObtenerNivelesEscolaridad()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerNivelesEscolaridad();
        }

        public IEnumerable<TipoContrato> ObtenerTiposContratos()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposContratos();
        }

        public IEnumerable<TipoEstadoCivil> ObtenerTiposEstadosCiviles()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposEstadosCiviles();
        }

        public IEnumerable<TipoEstrato> ObtenerTiposEstratos()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposEstratos();
        }

        public IEnumerable<TipoIdentificacion> ObtenerTiposIdentificaciones()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposIdentificaciones().OrderBy(x => x.Descripcion);
        }

        public IEnumerable<TipoJornadaLaboral> ObtenerTiposJornadasLaborales()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposJornadasLaborales();
        }

        public IEnumerable<TipoOcupacion> ObtenerTiposOcupacion()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposOcupacion();
        }

        public IEnumerable<TipoOrganoDirectivoControl> ObtenerTiposOrganosDirectivosControl()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposOrganosDirectivosControl();
        }

        public IEnumerable<TipoRol> ObtenerTiposRoles()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposRoles();
        }

        public IEnumerable<TipoSectorEconomico> ObtenerTiposSectorEconomico()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposSectorEconomico();
        }

        public IEnumerable<TipoEntidad> ObtenerEntidades()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerEntidades();
        }

        public IEnumerable<ClasificacionCIIU> ObtenerClasificacionCUU()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerClasificacionCUU();
        }
        public IEnumerable<Meses> ObtenerMeses()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerMeses();
        }

        public IEnumerable<Anos> ObtenerAnos()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerAnos();
        }

        public IEnumerable<ClasificacionExcedentes> ObtenerInfoExcedentes()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerInfoExcedentes();
        }

        public IEnumerable<FormCapacidadPago> ObtenerFormCapacidadPago(int? idTipoCreditoTipoRolRelacion = null)
        {
            IEnumerable<FormCapacidadPago> listaFormCapacidadPago = _consultasApoyoUnidadTrabajo.ObtenerFormCapacidadPago() ?? new List<FormCapacidadPago>();
            IEnumerable<FormCapacidadPagoTipoCreditoTipoRolRelacion> listaRelacion = _consultasApoyoUnidadTrabajo.ObtenerFormCapacidadPagoTipoCreditoTipoRolRelacion()
                                                                                                               .Where(x => x.IdTipoCreditoTipoRolRelacion == idTipoCreditoTipoRolRelacion)
                                                                                                               .ToList()
                                                                                                              ?? new List<FormCapacidadPagoTipoCreditoTipoRolRelacion>();
            
            listaFormCapacidadPago = listaRelacion.Join(listaFormCapacidadPago,
                                                                 post => post.IdCapacidadPago,
                                                                 meta => meta.IdCapacidadPago,
                                                                (post, meta) => new FormCapacidadPago()
                                                                {
                                                                    IdCapacidadPago = meta.IdCapacidadPago,
                                                                    IdGrupo = meta.IdGrupo,
                                                                    EsSoloLecturaAsociado = meta.EsSoloLecturaAsociado,
                                                                    EsSoloLecturaFamiliar = meta.EsSoloLecturaFamiliar,
                                                                    ValorAsociado = meta.ValorAsociado,
                                                                    ValorFamiliar = meta.ValorFamiliar,
                                                                    Texto = meta.Texto,
                                                                    Factor = meta.Factor,
                                                                    Tipo = meta.Tipo,
                                                                    TipoLista = meta.TipoLista
                                                                }).ToList();
            

            return listaFormCapacidadPago;
        }
        public IEnumerable<ViewModelFormCapacidadPagoGrupos> ObtenerFormCapacidadPagoGrupos(int? idTipoCreditoTipoRolRelacion = null)
        {
            IEnumerable<FormCapacidadPago> listaFormCapacidadPago = ObtenerFormCapacidadPago(idTipoCreditoTipoRolRelacion) ?? new List<FormCapacidadPago>();
            IEnumerable<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos = _consultasApoyoUnidadTrabajo.ObtenerFormCapacidadPagoGrupos() ?? new List<FormCapacidadPagoGrupos>();
            IEnumerable<ViewModelFormCapacidadPagoGrupos> listaViewlistaFormCapacidadPagoGrupos = new List<ViewModelFormCapacidadPagoGrupos>();
            foreach (FormCapacidadPagoGrupos grupo in listaFormCapacidadPagoGrupos)
            {
                listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == grupo.IdGrupo).SingleOrDefault().ListaFormCapacidadPago =
                                                         listaFormCapacidadPago.Where(x => x.IdGrupo == grupo.IdGrupo).ToList();

            }

            listaViewlistaFormCapacidadPagoGrupos = listaFormCapacidadPagoGrupos
                                                    .Select(post => new ViewModelFormCapacidadPagoGrupos
                                                    {
                                                        IdGrupo = post.IdGrupo,
                                                        ListaFormCapacidadPago = post.ListaFormCapacidadPago,
                                                        Mostrar = post.Mostrar,
                                                        MostrarTotal = post.MostrarTotal,
                                                        Nombre = post.Nombre,
                                                        Orden = post.Orden,
                                                        Total = post.Total,
                                                        HorasExtras = new HorasExtras()
                                                    });


            return listaViewlistaFormCapacidadPagoGrupos.OrderBy(x => x.Orden);
        }

        public IEnumerable<EvaluacionRiesgoLiquidez> ObtenerInfoEvaluacionRiesgoLiquidez()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerInfoEvaluacionRiesgoLiquidez();
        }

        public IEnumerable<TiposTitulos> ObtenerTiposTitulos()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposTitulos();
        }

        public IEnumerable<TipoObligacion> ObtenerTipoOligacion()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTipoOligacion();
        }

        public IEnumerable<TipoCuota> ObtenerTipoCuota()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTipoCuota();
        }

        public IEnumerable<ClaseGarantia> ObtenerClaseGarantia()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerClaseGarantia();
        }
        public IEnumerable<DestinoCredito> ObtenerDestinoCredito()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerDestinoCredito();
        }
        public IEnumerable<ClaseVivienda> ObtenerClaseVivienda()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerClaseVivienda();
        }
        public IEnumerable<CategoriaReestructurado> ObtenerCategoriaRestructurado()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerCategoriaRestructurado();
        }

        public IEnumerable<TipoEntidadBeneficiaria> ObtenerTipoEntidadBeneficiaria()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTipoEntidadBeneficiaria();
        }

        public IEnumerable<TipoModalidad> ObtenerTipoModalidad()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTipoModalidad();
        }
        public IEnumerable<ClaseNaturaleza> ObtenerClaseNaturaleza()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerClaseNaturaleza();
        }
        public IEnumerable<ClaseEstadoProcesoActual> ObtenerClaseEstadoProcesoActual()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerClaseEstadoProcesoActual();
        }
        public IEnumerable<ClaseConcepto> ObtenerClaseConcepto()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerClaseConcepto();
        }

        public IEnumerable<TipoDestino> ObtenerTipoDestino()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTipoDestino();
        }

        public IEnumerable<NivelEducacion> ObtenerNivelEducacion()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerNivelEducacion();
        }

        public IEnumerable<TipoBeneficiarios> ObtenerTiposBeneficiarios()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposBeneficiarios();
        }

        public IEnumerable<AlternativasDecreto2880> ObtenerAlternativasDecreto2880()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerAlternativasDecreto2880();
        }
        public IEnumerable<TipoUsuarioTieneAporte> ObtenerTipoUsuarioTieneAporte()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTipoUsuarioTieneAporte();
        }
        public IEnumerable<ClaseDeActivo> ObtenerClaseDeActivo()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerClaseDeActivo();
        }

        public IEnumerable<InformacionRelacionadaGrupoInteres> ObtenerInfoReporteGrupoDeInteres()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerInfoReporteGrupoDeInteres();
        }
        public IEnumerable<TipoLineaCredito> ObtenerTipoLineaCredito(int idTipoRol = 0)
        {
            IEnumerable<TipoCreditoTipoRolRelacion> listaTipoCreditoTipoRol = new List<TipoCreditoTipoRolRelacion>();
            IEnumerable<TipoLineaCredito> listaTipoLineaCredito = new List<TipoLineaCredito>();
            IEnumerable<TipoCredito> listaTipoCredito = new List<TipoCredito>();

            listaTipoCreditoTipoRol = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposRolRelacion(idTipoRol);
            listaTipoCredito = _consultasApoyoUnidadTrabajo.ObtenerTipoCredito();
            listaTipoLineaCredito = _consultasApoyoUnidadTrabajo.ObtenerTipoLineaCredito();


            listaTipoCredito = listaTipoCredito.Join(listaTipoCreditoTipoRol,
                                                                              post => post.IdTipoCredito,
                                                                              meta => meta.IdTipoCredito,
                                                                             (post, meta) => new TipoCredito()
                                                                             {
                                                                                 IdTipoLineaCredito = post.IdTipoLineaCredito,
                                                                             }).ToList();

            listaTipoLineaCredito = listaTipoCredito.GroupBy(x => x.IdTipoLineaCredito).Join(listaTipoLineaCredito,
                                                                              post => post.Key,
                                                                              meta => meta.IdTipoLineaCredito,
                                                                             (post, meta) => new TipoLineaCredito()
                                                                             {
                                                                                 IdTipoLineaCredito = meta.IdTipoLineaCredito,
                                                                                 Nombre = meta.Nombre,
                                                                                 Descripcion = meta.Descripcion
                                                                             }).Where(x => x.Nombre != "Especiales").OrderBy(x => x.Nombre).ToList();
          
            return listaTipoLineaCredito;
        }
        public IEnumerable<TipoCredito> ObtenerTipoCredito(int? idLineaCredito = null)
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTipoCredito()
                                               .Where(x => x.IdTipoLineaCredito == (idLineaCredito == null ? x.IdTipoLineaCredito : idLineaCredito)).OrderBy(x => x.Nombre);
        }

        public IEnumerable<TipoTitulo> ObtenerTipoTitulo()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTipoTitulo();
        }

        public IEnumerable<TipoTasa> ObtenerTipoTasa()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTipoTasa();
        }
        public IEnumerable<TipoModalidadContratacion> ObtenerTipoModalidadContratacion()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTipoModalidadContratacion();
        }
        public IEnumerable<TipoProductoServicioContratado> ObtenerTipoProductoServicioContratado()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTipoProductoServicioContratado();
        }
        public TipoCreditoTipoRolRelacion ObtenerTipoCreditoTipoRolRelacion(int idTipoCreditoTipoRolRelacion)
        {
            FormCapacidadPagoTipoCreditoTipoRolRelacion obj = new FormCapacidadPagoTipoCreditoTipoRolRelacion();
            return _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposRolRelacion().Where(x => x.IdTipoCreditoTipoRolRelacion == idTipoCreditoTipoRolRelacion).FirstOrDefault();
        }
        public IEnumerable<ViewModelTiposCreditoTiposRol> ObtenerTiposCreditoTiposRol(short idTipoRol, int idTipoLineaCredito, DateTime? fechaAfiliacion, string tipoDocumento, string numeroDocumento, double tiempoAfiliacion, double salario)
        {
            List<ViewModelTiposCreditoTiposRol> lstVmTiposCreditoTiposRol = new List<ViewModelTiposCreditoTiposRol>();

            IEnumerable<TipoCredito> lstTipoCredito = new List<TipoCredito>();
            lstTipoCredito = _consultasApoyoUnidadTrabajo.ObtenerTipoCredito(null);

            IEnumerable<TipoCreditoTipoRolRelacion> lstTiposCreditoTiposRolRelacion = new List<TipoCreditoTipoRolRelacion>();
            lstTiposCreditoTiposRolRelacion = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposRolRelacion(idTipoRol).ToList();

            DateTime fechaInicio = new DateTime();
            DateTime.TryParse("1900-01-01", out fechaInicio);


            TimeSpan diferencia = DateTime.Now - (fechaAfiliacion ?? DateTime.Now);
            short diferenciaAnhos = 0;            
            if (fechaInicio < fechaAfiliacion)
            {
                short.TryParse((diferencia.Days / 365).ToString(), out diferenciaAnhos);
            }
            else
            {
                diferenciaAnhos = 0;
            }
            Aportes aportes = _creditosUnidadTrabajo.ObtenerAportes(tipoDocumento, numeroDocumento, idTipoRol) ?? new Aportes();
            lstVmTiposCreditoTiposRol = lstTiposCreditoTiposRolRelacion.Join(lstTipoCredito,
                                                                              post => post.IdTipoCredito,
                                                                              meta => meta.IdTipoCredito,
                                                                             (post, meta) => new ViewModelTiposCreditoTiposRol()
                                                                             {
                                                                                 IdTipoCreditoTipoRolRelacion = post.IdTipoCreditoTipoRolRelacion,
                                                                                 IdTipoCredito = post.IdTipoCredito,
                                                                                 IdPapelCavipetrol = post.IdPapelCavipetrol,
                                                                                 Nombre = meta.Nombre,
                                                                                 IdTipoLineaCredito = meta.IdTipoLineaCredito,
                                                                                 TiempoMinimoDeAfiliacionAnhos = post.TiempoMinimoDeAfiliacionAnhos,
                                                                                 PlazoMaximo = post.PlazoMaximo,
                                                                                 PlazoMinimo = post.PlazoMinimo,
                                                                                 PorcentajeNovacion = post.PorcentajeNovacion,
                                                                                 CupoMaximo = (double)Math.Round((decimal)CalcularCupoMaximo(post.CupoMaximo, post.IdTipoCredito, (short)tiempoAfiliacion, aportes, salario), 0),
                                                                                 Aportes = (aportes.ValorAportes == null ? 0 : (double)Math.Round((decimal)aportes.ValorAportes, 0)),
                                                                                 AntiguedadAnhos = diferenciaAnhos,
                                                                                 IdProductoFYC = post.IdProductoFYC,
                                                                                 TasaEA = (double)Math.Round((decimal)(post.InteresEfectivoAnual * 100), 2),
                                                                                 Activo = post.Activo,
                                                                                 InteresNominal = (double)Math.Round(((decimal)post.InteresNominalQuincenaVendida * 100), 2),
                                                                                 TipoVehiculo = post.TipoVehiculo,
                                                                                 Norma = post.Norma
                                                                             }).Where(x => x.IdTipoLineaCredito == (idTipoLineaCredito == 0 ? x.IdTipoLineaCredito : idTipoLineaCredito) && x.Activo == true)
                                                                               .OrderBy(x => x.Nombre).ToList();
            return lstVmTiposCreditoTiposRol;
        }
        private double? CalcularCupoMaximo(double? cupoMaximo, short idTipoCredito, short diferenciaAnhos, Aportes aportes, double? salario)
        {
            if (cupoMaximo == null)
            {
                IEnumerable<Cupo> listaCupos = new List<Cupo>();
                listaCupos = _consultasApoyoUnidadTrabajo.ObtenerCupos(idTipoCredito);



                Cupo cupo = listaCupos.Where(x => diferenciaAnhos >= x.AntiguedadInicial && diferenciaAnhos <= (x.AntiguedadFinal ?? 150))
                                      .FirstOrDefault();

                if (cupo == null)
                {
                    return 0;
                }
                else
                {
                    switch (cupo.IdTipoCupo)
                    {
                        case (short)enumTiposCupo.SMMLV:
                            Parametros parametros = new Parametros();
                            parametros = _consultasApoyoUnidadTrabajo.ObtenerParametros("SMMLV").LastOrDefault();

                            double smmlv = 1;
                            double.TryParse(parametros.Valor, out smmlv);

                            return (((double)cupo.Valor) * smmlv);

                        case (short)enumTiposCupo.PORC_DE_CUPO_SOBRE_APORTES_DIRECTOS_SYP_RETORNO_INV_2010:

                            // COMO OBTENER LOS APORTES DE LOS ASOCIADOS, FYC
                            double aportesAsociado = (double)(aportes.ValorAportes == null ? 0 : aportes.ValorAportes);
                            return (aportesAsociado * cupo.Valor);
                        case (short)enumTiposCupo.SALARIO_EMPLEADO:

                            return (cupo.Valor * salario);
                        default:
                            return 0;
                    }
                }
            }
            else
            {
                Parametros parametros = new Parametros();
                parametros = _consultasApoyoUnidadTrabajo.ObtenerParametros("SMMLV").LastOrDefault();

                double smmlv = 1;
                double.TryParse(parametros.Valor, out smmlv);

                return cupoMaximo * smmlv;
            }
        }
        public IEnumerable<Etiquetas> ObtenerEtiquetas(int idTipoEtiqueta)
        {
            return _consultasApoyoUnidadTrabajo.ObtenerEtiquetas(idTipoEtiqueta).OrderBy(x => x.Nombre);
        }
        public IEnumerable<TiposDocumentoRequisito> ObtenerTiposDocumentoRequisito()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposDocumentoRequisito().OrderBy(x => x.Nombre);
        }
        public string ObtenerPlantillaHtml(short idPlantilla, List<Parametros> parametros)
        {
            Plantilla plantilla = new Plantilla();
            plantilla = _consultasApoyoUnidadTrabajo.ObtenerPlantilla(idPlantilla);

            foreach (Parametros parametro in parametros)
            {
                if (parametro.Nombre == "@ValorPesos")
                {
                    long numero = 0;
                    long.TryParse(parametro.Valor.Replace("$", "").Replace(",", ""), out numero);
                    var valorEnLetras = Conversiones.Conversiones.enletras(numero.ToString());
                    plantilla.Html = plantilla.Html.Replace("@ValorLetras", (valorEnLetras.Length > 0 ? valorEnLetras : "Valor no enviado"));
                }
                plantilla.Html = plantilla.Html.Replace(parametro.Nombre, (parametro.Valor.Length > 0 ? parametro.Valor : "Valor no enviado"));
            }

            return plantilla.Html;
        }
        public IEnumerable<AsegurabilidadOpcion> ObtenerAsegurabilidadOpciones(bool? noAsegurable)
        {
            return _sicavUnidadTrabajo.ObtenerAsegurabilidadOpciones(noAsegurable);
        }
        public IEnumerable<Bimestre> ObtenerBimestre()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerBimestre();
        }
        public IEnumerable<TiposGarantiasPagares> ObtenerTiposGarantiasPagares()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposGarantiasPagares();
        }

        public IEnumerable<SolicitudCreditoCausalesNegacion> ObtenerCausalesNegacion()
        {
            return _sicavUnidadTrabajo.ObtenerCausalesNegacion();
        }
        public IEnumerable<UsosCredito> ObtenerUsosCredito(int idTipoLinea)
        {
            return _consultasApoyoUnidadTrabajo.ObtenerUsosCredito(idTipoLinea);
        }
        public IEnumerable<TipoGarantiaCredito> ObtenerTiposGarantiasCredito(int? idTipoGarantiaCredito = null)
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposGarantiasCredito(idTipoGarantiaCredito);
        }
        public IEnumerable<VMConceptoGarantiaCredito> ObtenerTiposGarantiaTiposConceptoRelacion(int idPagare)
        {
            var lstTiposGarantiaTiposConceptoRelacion = _consultasApoyoUnidadTrabajo.ObtenerTiposGarantiaTiposConceptoRelacion(idPagare);
            var lstTiposConceptoGarantias = _consultasApoyoUnidadTrabajo.ObtenerTiposConceptoGarantia();

            return lstTiposGarantiaTiposConceptoRelacion.Join(lstTiposConceptoGarantias,
                                                                              post => post.IdConceptoGarantia,
                                                                              meta => meta.IdConceptoGarantia,
                                                                             (post, meta) => new VMConceptoGarantiaCredito()
                                                                             {
                                                                                IdConceptoGarantia = post.IdConceptoGarantia,
                                                                                IdPagare = post.IdPagare,
                                                                                Concepto = meta.Concepto,
                                                                                CampoRemplaza = meta.CampoRemplaza,
                                                                                tipoVariable = meta.tipoVariable,
                                                                                pkFycItemDocumento = post.PkFycItemDocumento,
                                                                                visible = meta.visible,
                                                                                editable = meta.editable,
                                                                                ciudad = meta.ciudad
                                                                             })
                                                                               .OrderBy(x => x.Concepto).ToList();
        }
        public IEnumerable<TiposCreditoTiposGarantiaRelacion> ObtenerTiposCreditoTiposGarantiaRelacion(short idTipoCredito) {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposGarantiaRelacion(idTipoCredito);
        }
        public IEnumerable<VMTipoConceptoAdjunto> ObtenerTiposConceptosTipoAdjunto(int idTipoAdjunto)
        {
            var lstTiposConceptosRelacion = _consultasApoyoUnidadTrabajo.ObtenerTiposConceptosTipoAdjuntoRelacion(idTipoAdjunto);
            var lstTiposConceptosAdjuntos = _consultasApoyoUnidadTrabajo.ObtenerTiposConceptos();

            return lstTiposConceptosRelacion.Join(lstTiposConceptosAdjuntos,
                                                                            post => post.IdConceptoAdjunto,
                                                                            meta => meta.IdConceptoAdjunto,
                                                                            (post, meta) => new VMTipoConceptoAdjunto()
                                                                            {
                                                                                IdConceptoAdjunto = post.IdConceptoAdjunto,
                                                                                IdTipoAdjunto = post.IdTipoAdjunto,
                                                                                Concepto = meta.Concepto,
                                                                                Descripcion = meta.Descripcion
                                                                            })
                                                                            .OrderBy(x => x.Concepto).ToList();
        }



        public int ObtenerCodeudorPorGarantia(string idTipoGarantia, string idTipoCodeudor, string valor)
        {
            return _consultasApoyoUnidadTrabajo.ObtenerCodeudorPorGarantia(idTipoGarantia, idTipoCodeudor, valor);
        }
        public string ValidarUsuarioCavipetrol(string NumeroIdentificacion)
        {
            return _consultasApoyoUnidadTrabajo.ValidarUsuarioCavipetrol(NumeroIdentificacion);
        }

        public IEnumerable<TiposConceptosAdjunto> ObtenerConceptosAdjuntos(string idConcepto)
        {
            return _consultasApoyoUnidadTrabajo.ObtenerConceptosAdjuntos(idConcepto);
        }

        public IEnumerable<TiposContrato> ObtenerTiposContrato()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerTiposContrato();
        }
        public List<CiudadesServidor> ObtenerCiudadesServidor()
        {
            return _consultasApoyoUnidadTrabajo.ObtenerCiudadesServidor().ToList();
        }
        public IEnumerable<Parametros> ObtenerParametros(string nombre)
        {
            return _consultasApoyoUnidadTrabajo.ObtenerParametros(nombre).ToList();
        }
        public IEnumerable<Usuarios> ObtenerUsuariosPorPerfil(string perfil)
        {
            return _consultasApoyoUnidadTrabajo.ObtenerUsuariosPorPerfil(perfil).ToList();
        }
        public string AsignarUsuarioSolicitud(int idSolicitud, int idUsuario)
        {
            return _consultasApoyoUnidadTrabajo.AsignarUsuarioSolicitud(idSolicitud, idUsuario).ToString();
        }
        public IEnumerable<SolicitudDescripcionFondos> SolicitudDescripcionFondos()
        {
            return _consultasApoyoUnidadTrabajo.SolicitudDescripcionFondos();
        }
        public ViewModelSegurosTasas ObtenerSegurosTasas(int edad, short idTipoCredito) {
            ViewModelSegurosTasas segurosTasas = new ViewModelSegurosTasas();
            var algo = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposSeguroRelacion((short)enumTiposCredito.Vivienda);
            segurosTasas.Vida = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposSeguroRelacion((short)enumTiposCredito.Vivienda)
                                                                                             .Where(y => y.RangoFinal >= edad && y.RangoInicial <= edad && y.IdTipoSeguro == (int)enumTipoSeguro.Seguro_de_vida).FirstOrDefault().TasaPorMil;
            segurosTasas.Incendio = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposSeguroRelacion((short)enumTiposCredito.Vivienda)
                                                                                             .Where(y => y.RangoFinal >= edad && y.RangoInicial <= edad && y.IdTipoSeguro == (int)enumTipoSeguro.Seguro_de_incendio_y_terremoto).FirstOrDefault().TasaPorMil;
            return segurosTasas;
        }
        public IEnumerable<ModelCartaSolicitudCredito> ObtenerCartaSolicitud(string IdSolicitud)
        {
            try
            {
                JavaScriptSerializer serializador = new JavaScriptSerializer();
                var CapacidadPago = _consultasApoyoUnidadTrabajo.ObtenerCartaSolicitud(IdSolicitud);

                List<FormCapacidadPagoGrupos> ListaFormCapacidadPagoGrupos = new List<FormCapacidadPagoGrupos>();

                ListaFormCapacidadPagoGrupos = serializador.Deserialize<List<FormCapacidadPagoGrupos>>(CapacidadPago.FirstOrDefault().FormCapacidad);
                var plazoMeses = ListaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Credito).FirstOrDefault().ListaFormCapacidadPago.
                     Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Plazo_meses_valorCredito).FirstOrDefault().ValorAsociado;

                var tasaNominal  = 0;

                switch (CapacidadPago.FirstOrDefault().idLinea)
                {
                    case (int)enumLineasCredito.Vivienda:
                        tasaNominal = (int)ListaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Credito).FirstOrDefault().ListaFormCapacidadPago.
                                        Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Tasa_Credito_EA_Consumo).FirstOrDefault().ValorAsociado;

                        break;
                    default:
                        tasaNominal = (int)ListaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Credito).FirstOrDefault().ListaFormCapacidadPago.
                                         Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Tasa_Credito_EA_Consumo).FirstOrDefault().ValorAsociado;
                        break;
                }

                var tasaEA = ListaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Credito).FirstOrDefault().ListaFormCapacidadPago.
                                       Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Tasa_Credito_N_M_V).FirstOrDefault().ValorAsociado;
                var plazoAnhos = plazoMeses / 12;

                List<ReferenciasPersonales> referenciasPersonal = new List<ReferenciasPersonales>();
                List<ReferenciasFamiliares> referenciasFamiliar = new List<ReferenciasFamiliares>();
                //ReferenciasFamiliares referenciaFamiliar = new ReferenciasFamiliares();
                //ReferenciasPersonales referenciaPersonal = new ReferenciasPersonales();

                foreach (var item in CapacidadPago)
                {
                    if (item.TipoReferencia == 1)
                    {
                        ReferenciasFamiliares referenciaFamiliar = new ReferenciasFamiliares();
                        referenciaFamiliar.NombreReferencia = item.NombreReferencia;
                        referenciaFamiliar.NumeroReferencia = item.NumeroReferencia;
                        referenciaFamiliar.parentesco = item.Relacion;

                        referenciasFamiliar.Add(referenciaFamiliar);
                    }
                    else if (item.TipoReferencia == 2)
                    {
                        ReferenciasPersonales referenciaPersonal = new ReferenciasPersonales();
                        referenciaPersonal.NombreReferencia = item.NombreReferencia;
                        referenciaPersonal.NumeroReferencia = item.NumeroReferencia;
                        referenciaPersonal.parentesco = item.Relacion;

                        referenciasPersonal.Add(referenciaPersonal);
                    }
                }

                CapacidadPago.FirstOrDefault().referenciaPersonal = referenciasPersonal;
                CapacidadPago.FirstOrDefault().referenciaFamiliar = referenciasFamiliar;
                CapacidadPago.FirstOrDefault().plazoMeses = (int)plazoMeses;
                CapacidadPago.FirstOrDefault().tasaEA = tasaNominal;
                CapacidadPago.FirstOrDefault().TEA = tasaNominal;
                CapacidadPago.FirstOrDefault().tNominal = Convert.ToDouble(tasaEA);
                CapacidadPago.FirstOrDefault().plazoAnhos = (int)plazoAnhos;
                CapacidadPago.FirstOrDefault().valorNegociado = CapacidadPago.FirstOrDefault().ValorSolicitado.ToString("C0");
                CapacidadPago.FirstOrDefault().cuentaFAI = CapacidadPago.FirstOrDefault().ValorFai.ToString("C0");
                CapacidadPago.FirstOrDefault().girarCheque = CapacidadPago.FirstOrDefault().ValorCheque.ToString("C0");
                CapacidadPago.FirstOrDefault().cruceInterproductos = CapacidadPago.FirstOrDefault().ValorInterproductos.ToString("C0");
                CapacidadPago.FirstOrDefault().ingresosMensuales = CapacidadPago.FirstOrDefault().ingresos.ToString("C0");
                CapacidadPago.FirstOrDefault().egresosMensuales = CapacidadPago.FirstOrDefault().egresos.ToString("C0");
                CapacidadPago.FirstOrDefault().totalActivos = CapacidadPago.FirstOrDefault().Activos.ToString("C0");
                CapacidadPago.FirstOrDefault().totalPasivos = CapacidadPago.FirstOrDefault().Pasivos.ToString("C0");
                CapacidadPago.FirstOrDefault().salarioActual = CapacidadPago.FirstOrDefault().salario.ToString("C0");
                return CapacidadPago;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<Interproductos> ObtenerModelInterproductos(string IdSolicitud)
        {
            var interproductos =_consultasApoyoUnidadTrabajo.ObtenerModelInterproductos(IdSolicitud);

            foreach (var item in interproductos)
            {
                item.ValorACruzar = item.Valor.ToString("C0");

            }

            return interproductos;
        }

        public List<CartaCondicionesCreditoModel> ObtenerDatosCartaCondiciones(int idSolicitud)
        {
            List<CartaCondicionesCreditoModel> ListaCartaCondiciones = new List<CartaCondicionesCreditoModel>();
            ListaCartaCondiciones = _sicavUnidadTrabajo.ObtenerDatosCartaCondiciones(idSolicitud);

            if (ListaCartaCondiciones.Count() <= 0)
                return ListaCartaCondiciones;

            CartaCondicionesCreditoModel DatosCartaCondicionesCredito = ListaCartaCondiciones.FirstOrDefault();

            List<FormCapacidadPagoGrupos> ListaFormCapacidadPagoGrupos = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FormCapacidadPagoGrupos>>(DatosCartaCondicionesCredito.JsonCapacidadPago);

            FormCapacidadPagoGrupos JsonCapacidadPago = ListaFormCapacidadPagoGrupos.FirstOrDefault();

            double tasaNominal = 0;

            var plazoMeses = ListaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Credito).FirstOrDefault().ListaFormCapacidadPago.
                 Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Plazo_meses_valorCredito).FirstOrDefault().ValorAsociado;


            switch (ListaCartaCondiciones.FirstOrDefault().idLinea)
            {
                case (int)enumLineasCredito.Vivienda:
                    tasaNominal = (double)ListaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Credito).FirstOrDefault().ListaFormCapacidadPago.
                                    Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Tasa_Credito_EA_Consumo).FirstOrDefault().ValorAsociado;

                    break;
                default:
                    tasaNominal = (double)ListaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Credito).FirstOrDefault().ListaFormCapacidadPago.
                                     Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Tasa_Credito_EA_Consumo).FirstOrDefault().ValorAsociado;
                    break;
            }

            var tasaEA  = ListaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Credito).FirstOrDefault().ListaFormCapacidadPago.
                                   Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Tasa_Credito_N_M_V).FirstOrDefault().ValorAsociado;
            var plazoAnhos = plazoMeses / 12;

            DatosCartaCondicionesCredito.plazoAnhos = plazoAnhos.ToString();
            DatosCartaCondicionesCredito.TEA = Math.Round(tasaNominal, 2);
            
            DatosCartaCondicionesCredito.tNominal = (float?)Math.Round(Convert.ToDouble(tasaEA), 2);


            return ListaCartaCondiciones;
        }
	}
}
