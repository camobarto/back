﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.ReportesUIAF;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Script.Serialization;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class ReportesUIAF
    {
        private readonly IReportesUIAFUnidadTrabajo _reportesUnidadTrabajo;
        public ReportesUIAF()
        {
            _reportesUnidadTrabajo = new ReportesUIAFUnidadTrabajo();
        }

        public RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivo>> ObtenerReporteTransaccionesEnEfectivo(int idReporte, string fechaInicio, string fechaFin)
        {
            RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivo>> respuesta = new RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivo>>();
            //var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            //if (consultarHistorico == null)
            //{

            DateTime d = DateTime.Now;
            string dd = d.ToString("yyyy-MM-ddTHH:mm:ss.000Z");

            DateTime fechaInicioDate = DateTime.ParseExact(fechaInicio, "yyyy-MM-ddTHH:mm:ss.000Z", CultureInfo.InvariantCulture);
            DateTime fechaFinDate = DateTime.ParseExact(fechaFin, "yyyy-MM-ddTHH:mm:ss.000Z", CultureInfo.InvariantCulture);

            respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteTransaccionesEnEfectivo(fechaInicioDate, fechaFinDate);
            return respuesta;
            //}

            //respuesta.EsHistorico = true;
            //JavaScriptSerializer serializador = new JavaScriptSerializer();
            //respuesta.InformacionReporte = serializador.Deserialize<List<ReporteTransaccionesEnEfectivo>>(consultarHistorico.DatosReporte);
            //return respuesta;
        }

        public RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivoRiesgos>> ObtenerReporteTransaccionesEnEfectivoRiesgos(int idReporte, string fechaInicio, string fechaFin)
        {
            RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivoRiesgos>> respuesta = new RespuestaReporte<IEnumerable<ReporteTransaccionesEnEfectivoRiesgos>>();
           
            DateTime d = DateTime.Now;
            string dd = d.ToString("yyyy-MM-ddTHH:mm:ss.000Z");

            DateTime fechaInicioDate = DateTime.ParseExact(fechaInicio, "yyyy-MM-ddTHH:mm:ss.000Z", CultureInfo.InvariantCulture);
            DateTime fechaFinDate = DateTime.ParseExact(fechaFin, "yyyy-MM-ddTHH:mm:ss.000Z", CultureInfo.InvariantCulture);

            respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteTransaccionesEnEfectivoRiesgos(fechaInicioDate, fechaFinDate);
            return respuesta;            
        }

        public RespuestaReporte<IEnumerable<ReporteProductosEconomiaSolidaria>> ObtenerReporteProductosEconomiaSolidaria(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteProductosEconomiaSolidaria>> respuesta = new RespuestaReporte<IEnumerable<ReporteProductosEconomiaSolidaria>>();
            //var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            //if (consultarHistorico == null)
            //{
            int diasMes = System.DateTime.DaysInMonth(ano, mes);
            DateTime fechaPeriodo = new DateTime(ano, mes, diasMes);

            respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteProductosEconomiaSolidaria(fechaPeriodo);
            return respuesta;
            //}

            //respuesta.EsHistorico = true;
            //JavaScriptSerializer serializador = new JavaScriptSerializer();
            //respuesta.InformacionReporte = serializador.Deserialize<List<ReporteTransaccionesEnEfectivo>>(consultarHistorico.DatosReporte);
            //return respuesta;
        }

    }
}
