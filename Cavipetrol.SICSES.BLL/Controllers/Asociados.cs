﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.Utilidades;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Sicav;
using System;
using System.Collections.Generic;
using System.Linq;
using Cavipetrol.SICSES.DAL;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class Asociados
    {
        private readonly ISicavUnidadTrabajo _SicavUnidadTrabajo;
        private readonly SICAVContexto _sicavContexto;
        public Asociados()
        {
            _SicavUnidadTrabajo = new SicavUnidadTrabajo();
            _sicavContexto = new SICAVContexto();
        }
        public RespuestaNegocio<string> GuardarReferenciasAsociado(List<ReferenciasAsociado> referencias) {
            RespuestaNegocio<string> rta = new RespuestaNegocio<string>();
            try
            {
                rta = _SicavUnidadTrabajo.GuardarReferenciasAsociado(referencias);            
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return rta;
        }
        public RespuestaNegocio<List<ViewModelReferenciasAsociado>> ObtenerReferencias(string tipo, string numero) {
            RespuestaNegocio<List<ViewModelReferenciasAsociado>> rta = new RespuestaNegocio<List<ViewModelReferenciasAsociado>>();
            try
            {
                var lista = _SicavUnidadTrabajo.ObtenerReferenciasAsociado(tipo, numero);
                rta.Respuesta = lista.Select( x => new ViewModelReferenciasAsociado {
                                                    IdReferenciaAsociado = x.IdReferenciaAsociado,
                                                    Nombre = x.Nombre,
                                                    Numero = x.Numero,
                                                    IdTipoReferencia = x.IdTipoReferencia,
                                                    Relacion = x.Relacion
                                                    }).ToList();
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                List<string> mensaje = new List<string>();
                LimpiarExcepciones.LimpiarExcepcion(ex, ref mensaje);
                rta.MensajesError = mensaje;
            }
            return rta;
        }

        public DateTime ConsultarFechaFinalizacionContratoPorEmpleado(string TipoIdentificacion, string NumeroIdentificacion)
        {
            return _sicavContexto.ConsultarFechaFinalizacionContratoPorEmpleado(TipoIdentificacion, NumeroIdentificacion).FirstOrDefault();
        }
    }
}
