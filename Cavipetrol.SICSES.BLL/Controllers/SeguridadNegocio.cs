﻿using Cavipetrol.SICSES.DAL.Repositorios;
using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.Seguridad;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class SeguridadNegocio
    {
        private readonly ISicavUnidadTrabajo _SicavUnidadTrabajo;
        private readonly IConsultasApoyoUnidadTrabajo _ICoreParametros;
        private readonly RepositorioSICAV _repositorioSICAV;
        public SeguridadNegocio()
        {
            _SicavUnidadTrabajo = new SicavUnidadTrabajo();
            _ICoreParametros = new ConsultasApoyoUnidadTrabajo();
            _repositorioSICAV = new RepositorioSICAV(new DAL.SICAVContexto());
        }

        public Usuario ObtenerPerfilUsuario(string uniqueName)
        {
            Usuario usuario = _SicavUnidadTrabajo.ObtenerUsuario(uniqueName);
            if (usuario.IdUsuario != 0)
            {   
                var oficina = _ICoreParametros.ObtenerOficinas(usuario.IdOficina);
                var municipio = _ICoreParametros.ObtenerMunicipio(oficina.MNC_ID, oficina.DPR_ID);

                usuario.NombreOficina = oficina.OFI_NOM_OFICINA;
                usuario.NombreMunicipio = oficina.OFI_NOM_CIUDAD;
            }

            return usuario;
        }

        public List<Perfil> ObtenerPerfilesUsuario(int idUsuario)
        {
            return _repositorioSICAV.ObtenerPerfilesUsuario(idUsuario);
        }

        public void GuardarUsuarioTemporal(string Usuario,string Contrasena)
        {
             _SicavUnidadTrabajo.GuardarUsuarioTemporal(Usuario, Contrasena);
        }


        public RespuestaValidacion ValidarInicioUsuario(string usuario)
        {
            return _SicavUnidadTrabajo.ValidarInicioUsuario(usuario);
        }

        public UsuarioLogin AdministrarInicioSesion(string usuario, string contrasena, string NuevaContrasena, int bandera)
        {
            return _SicavUnidadTrabajo.AdministrarInicioSesion(usuario, contrasena, NuevaContrasena, bandera);
        }
        public RespuestaNegocio<CapacidadPagoTerceroCodeudor> GuardarCapacidadPagoTerceroCodeudor(string TipoIdentificacion, string NumeroIdentificacion, string Nombre, string FechaNacimiento, string FormCapacidad)
        {
            return _SicavUnidadTrabajo.GuardarCapacidadPagoTerceroCodeudor(TipoIdentificacion, NumeroIdentificacion, Nombre, FechaNacimiento, FormCapacidad);
        }
        public List<Usuario> ObtenerUsuario()
        {
            var usuarios = _SicavUnidadTrabajo.ObtenerUsuario();
            usuarios.ForEach(u => u.NombreOficina = _ICoreParametros.ObtenerOficinas(u.IdOficina).OFI_NOM_OFICINA);
            return usuarios;
        }
        public List<Perfil> ObtenerPerfil()
        {
            return _SicavUnidadTrabajo.ObtenerPerfil();
        }
        public Usuario GuardarUsuario(Usuario usuario)
        {
            return _SicavUnidadTrabajo.GuardarUsuario(usuario);
        }
        public Perfil GuardarPerfil(Perfil perfil)
        {
            return _SicavUnidadTrabajo.GuardarPerfil(perfil);
        }
        public List<Oficina> ObtenerOficinas()
        {
            return _ICoreParametros.ObtenerOficinas();
        }
    }
}
