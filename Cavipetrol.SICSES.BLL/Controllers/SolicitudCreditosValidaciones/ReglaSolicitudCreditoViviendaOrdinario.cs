﻿using System;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.BLL.Mensajes;

namespace Cavipetrol.SICSES.BLL.Controllers.SolicitudCreditosValidaciones
{
    public class ReglaSolicitudCreditoViviendaOrdinario : IReglasSolicitudCreditoProductos
    {
        private readonly string NombreProducto = "CavOrdVivEmplea";
        private readonly int PlazoMaximo = 5; //Plazo en años
        private readonly double SaldoLimite = 0;

        private MensajesValidaciones mensajes = new MensajesValidaciones();
        public Func<InformacionProductosCreditos, bool> FiltrosProductosDisponibles()
        {
            Func<InformacionProductosCreditos, bool> rule = (info) => info.NombreProducto == NombreProducto;            
            return rule;
        }
        public RespuestaNegocio<ValidacionesSolicitudCreditoProducto> ValidacionRestriccionesProducto(string identificacion, string tipoIdentificacion, string papel, ICreditosUnidadTrabajo _unidadTrabajo) {
            var validaciones = new RespuestaNegocio<ValidacionesSolicitudCreditoProducto>();
            validaciones.Respuesta = _unidadTrabajo.ObtenerRestriccionesProducto(identificacion, tipoIdentificacion, papel, NombreProducto);
            
            if(validaciones.Respuesta.Saldo > SaldoLimite)
            {
                validaciones.Respuesta.CumpleSaldo = false;
                validaciones.MensajesError.Add(mensajes.SALDO_MAYOR_A.Replace("$1", string.Format("{0:C2}", SaldoLimite)));
                validaciones.Estado = false;
            }

            return validaciones;
        }
        public RespuestaNegocio<ValidacionesSolicitudCreditoCaviAlDia> ValidacionProductoCaviAlDia(string papel, string identificacion, string tipoIdentificacion, ICreditosUnidadTrabajo _unidadTrabajo) {
            var validacion = new RespuestaNegocio<ValidacionesSolicitudCreditoCaviAlDia>();
            if(papel == ConstantesSolicitud.EMPLEADO_CAVIPETROL || papel == ConstantesSolicitud.EMPLEADO_TEMP || papel == ConstantesSolicitud.EXEMPLEADO_CAVIPETROL)
            {
                validacion.Estado = true;
            }
            return validacion;
        }
    }
}
