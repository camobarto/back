﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;

namespace Cavipetrol.SICSES.BLL.Controllers.SolicitudCreditosValidaciones
{
    public interface IReglasSolicitudCreditoProductos
    {
        Func<InformacionProductosCreditos, bool> FiltrosProductosDisponibles();
        RespuestaNegocio<ValidacionesSolicitudCreditoProducto> ValidacionRestriccionesProducto(string papel, string identificacion, string tipoIdentificacion, ICreditosUnidadTrabajo _unidadTrabajo);      
        RespuestaNegocio<ValidacionesSolicitudCreditoCaviAlDia> ValidacionProductoCaviAlDia(string papel, string identificacion, string tipoIdentificacion, ICreditosUnidadTrabajo _unidadTrabajo);
    }
}