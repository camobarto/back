﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class Asegurabilidad
    {
        private readonly IAsegurabilidad _asegurabilidadUnidadTrabajo;
        private readonly IPolizasExternasUnidadTrabajo _polizasExternasUnidadTrabajo;

        public Asegurabilidad()
        {
            _asegurabilidadUnidadTrabajo = new AsegurabilidadUnidadTrabajo();
            _polizasExternasUnidadTrabajo = new PolizasExternasUnidadTrabajo();
        }

        public bool InsertarDatosAsegurabilidad(List<AsegurabilidadModel> reporteHistorico)
        {
            return _asegurabilidadUnidadTrabajo.InsertarDatosAsegurabilidad(reporteHistorico);
        }

        public bool InsertarDatosPolizasExternas(List<PolizasExternas> reporteHistorico)
        {
            return _polizasExternasUnidadTrabajo.InsertarDatosPolizasExternas(reporteHistorico);
        }

        public bool AdministrarPolizasExternas(ViewModelPolizasExternas datosPolizasExternas)
        {
            return _polizasExternasUnidadTrabajo.AdministrarPolizasExternas(datosPolizasExternas);
        }

        public bool ConsultarHistorico(string NumeroDocumento, string NumeroPoliza, string ProductoAmparado, string DocumentoProducto)
        {
            return _polizasExternasUnidadTrabajo.ConsultarHistorico(NumeroDocumento, NumeroPoliza, ProductoAmparado, DocumentoProducto);
        }

        public IEnumerable<ViewModelPolizasExternas> ConsultarPolizasExternas(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza, int DiasVencimiento , string EstadoPoliza)
        {
            //RespuestaNegocio<PolizasExternas> informacionEmpleado = new RespuestaNegocio<PolizasExternas>();
            //return informacionEmpleado;
          return _polizasExternasUnidadTrabajo.ConsultarPolizasExternas(TipoIdentificacion, NumeroDocumento, Nombres, NumeroPoliza, DiasVencimiento ,  EstadoPoliza);

            
        }


        public IEnumerable<Aseguradoras> ObtenerAseguradoras()
        {
            return _polizasExternasUnidadTrabajo.ObtenerAseguradoras();
        }

        public IEnumerable<TipoPoliza> ObtenerTipoPoliza()
        {
            return _polizasExternasUnidadTrabajo.ObtenerTipoPoliza();
        }

        public bool EliminarPolizaExterna(string TipoIdentificacion, string NumeroIdentificacion, string Aseguradora, string NumeroPoliza)
        {
            return _polizasExternasUnidadTrabajo.EliminarPolizaExterna(TipoIdentificacion, NumeroIdentificacion, Aseguradora, NumeroPoliza);
        }


        public IEnumerable<HistoricoPolizasExternas> ConsultarHistoricoPoliza(string TipoIdentificacion, string NumeroDocumento, string Nombres, string NumeroPoliza)
        {
            //RespuestaNegocio<PolizasExternas> informacionEmpleado = new RespuestaNegocio<PolizasExternas>();
            //return informacionEmpleado;
            return _polizasExternasUnidadTrabajo.ConsultarHistoricoPoliza(TipoIdentificacion, NumeroDocumento, Nombres, NumeroPoliza);


        }

        public RespuestaValidacion ValidarPolizasExternas(string aseguradora, string tipopoliza, string producto, string documento,string nit)
        {
            {
                //RespuestaNegocio<PolizasExternas> informacionEmpleado = new RespuestaNegocio<PolizasExternas>();
                //return informacionEmpleado;
                return _polizasExternasUnidadTrabajo.ValidarPolizasExternas(aseguradora, tipopoliza, producto, documento,nit);
            }

        }

        public IEnumerable<Infraestructura.Model.ProductoPorAsociado> ObtenerProductoPorAsociado(string Cedula)
        {
            return _polizasExternasUnidadTrabajo.ObtenerProductoPorAsociado(Cedula);
        }

        public RespuestaNegocio<IEnumerable<VMPolizasVigentes>> ObtenerPolizasVigentes(string tipo, string numero) {
            RespuestaNegocio<IEnumerable<VMPolizasVigentes>> rta = new RespuestaNegocio<IEnumerable<VMPolizasVigentes>>();
            try
            {
                rta.Respuesta = _asegurabilidadUnidadTrabajo.ObtenerPolizasVigentes(tipo, numero);
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.MensajesError.Add(ex.InnerException.Message ?? ex.Message);               
                rta.MensajesError.Add(ex.Source);
                rta.MensajesError.Add(ex.StackTrace);
                rta.Estado = false;
            }
            return rta;
        }
    }
}
