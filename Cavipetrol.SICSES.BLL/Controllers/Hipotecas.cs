﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model.Hipotecas;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Hipotecas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class Hipotecas
    {
        private readonly IHipotecasUnidadTrabajo _creditosUnidadTrabajo;

        public Hipotecas()
        {
            _creditosUnidadTrabajo = new HipotecasUnidadTrabajo();
        }


        public IEnumerable<Hipoteca> ObtenerHipotecas(string NumeroHipoteca, string TipoIdentificacion, string NumeroIdentificacion, string NumeroSolicitud)
        {
            return _creditosUnidadTrabajo.ObtenerHipotecas(NumeroHipoteca, TipoIdentificacion, NumeroIdentificacion, NumeroSolicitud);
        }
        public bool ActualizaHipotecas(int IdHipoteca, string AudUsuarioModificacion, string FechaEscrituracion, string NumeroEscritura, string ValorAvaluo, string VigenciaInicial, string VigenciaFinal, string NumeroNotaria, string Oficina)
        {
            return _creditosUnidadTrabajo.ActualizaHipotecas(IdHipoteca, AudUsuarioModificacion, FechaEscrituracion, NumeroEscritura,  ValorAvaluo,  VigenciaInicial,  VigenciaFinal, NumeroNotaria, Oficina);
        }
        public List<VMHipotecas> ObtenerHipotecasXCedula(string TipoIdentificacion, string NumeroIdentificacion, double ValorSolicitado)
        {
            return _creditosUnidadTrabajo.ObtenerHipotecasXCedula(TipoIdentificacion, NumeroIdentificacion).Where(h => h.FechaEscrituracion.HasValue 
                                                                                                                    && h.NumeroEscritura != null).Select(post => new VMHipotecas {
                                                                                                                        IdHipoteca = post.IdHipoteca,
                                                                                                                        NumeroHipoteca = post.NumeroHipoteca,
                                                                                                                        Vigencia = post.VigenciaFinal,
                                                                                                                        ValorAvaluo = post.ValorAvaluo,
                                                                                                                        ValorCubierto = post.ValorCubierto,
                                                                                                                        ValorPosible = post.ValorAvaluo * 0.8,
                                                                                                                        ValorTotal = post.ValorCubierto + ValorSolicitado,
                                                                                                                        AplicaPagare = (post.ValorCubierto + ValorSolicitado) <= (post.ValorAvaluo * 0.8)
                                                                                                                    }).ToList();
        }
    }
}
