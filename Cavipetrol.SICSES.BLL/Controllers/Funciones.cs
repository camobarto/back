﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class Funciones
    {
        private readonly IFunciones _funcionesUnidadTrabajo;

        public Funciones()
        {
            _funcionesUnidadTrabajo = new FuncionesUnidadTrabajo();
        }

        public bool EnviarSMS(DatosSMS datossms)
        {
            return _funcionesUnidadTrabajo.EnviarSMS(datossms);
        }

    }
}
