﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.Creditos;
using Microsoft.VisualBasic;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.Globales;
using Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones;

namespace Cavipetrol.SICSES.BLL.Controllers.SolicitudCredito
{
    public abstract class CapacidadPagoCalculoCreditoAbstracta
    {
        protected float? totalIngresosAsociado = 0;
        protected float? totalIngresosFamiliar = 0;
        protected float? totalDescuentosLeyAsociado = 0;
        protected float? totalDescuentosLeyFamiliar = 0;
        protected float? totalOtrasDeduccionesAsociado = 0;
        protected float? totalOtrasDeduccionesFamiliar = 0;
        protected float? subtotalIngresosMenosDescuentosAsociado = 0;
        protected float? subtotalIngresosMenosDescuentosFamiliar = 0;
        protected float? otrosIngresosAsociado = 0;
        protected float? otrosIngresosFamiliar = 0;
        protected float? totalIngresosBrutosAsociado = 0;
        protected float? totalIngresosBrutosFamiliar = 0;
        protected float? factorGastosFamiliares = 0;
        protected float? gastosFamiliaresAsociado = 0;
        protected float? gastosFamiliaresFamiliar = 0;
        protected float? gastosFinancierosFamiliar = 0;
        protected float? gastosFinancierosAsociado = 0;
        protected float? totalGastosFamiliar = 0;
        protected float? SMMLV = 0;
        protected float? totalGastosAsociado = 0;
        protected float? ingresoFamiliarDisponibleTotal = 0;
        protected float? factorIngresoNetoVivienda = 0;
        protected float? ingresoNetoViviendaAsociado = 0;
        protected float? ingresoNetoViviendaFamiliar = 0;
        protected float? factorMaximoDescuentoNomina = 0;
        protected float? maximoDescuentoNomina = 0;
        protected float? factorMaximoValorPagoPorCaja = 0;
        protected float? maximoValorPagoPorCaja = 0;
        protected float? valorCuotaCierreFinanciero = 0;
        protected short anhosValorCredito = 0;
        protected short mesesValorCredito = 0;
        protected float? tasaCredito_E_A = 0;
        protected float? tasaNominal = 0;
        protected short plazoMeses = 0;
        protected float? totalValorCreditoVivienda = 0;
        protected float? factorValorMaximoViviendaNoVis = 0;
        protected float? factorValorMaximoViviendaVis = 0;
        public abstract void Calcular(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, 
                                      ref List<Amortizacion> tablaAmortizacion, 
                                      int idTipoCreditoTipoRolRelacion,
                                      List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion, 
                                      double valorTotalInmueble, 
                                      short idTipoPoliza, 
                                      double valorNegociado,
                                      double tasaMensualSeguroVida,
                                      double PorcenExtraPrima,
                                      int idPapelCavipetrol,
                                      string tipoCapacidadPago, int edad, int idTipoGarantia);
        protected void CalcularValorTotalGrupo(FormCapacidadPagoGrupos grupo, ref float? valorTotalAsociado, ref float? valorTotalFamiliar)
        {            
            foreach (FormCapacidadPago FormCapacidadPago in grupo.ListaFormCapacidadPago)
            {
                valorTotalAsociado += FormCapacidadPago.ValorAsociado ?? 0;
                valorTotalFamiliar += FormCapacidadPago.ValorFamiliar ?? 0;
            }            
        }
        public float? CalcularTasaNominal(double tasaEfectiva_Decimales, double numeroPeriodosAnho)
        {
            float? tasaNominal = 0;
            tasaNominal = (float?)((Math.Pow((1 + tasaEfectiva_Decimales), (1 / numeroPeriodosAnho)) - 1) * 100);
            return tasaNominal;
        }
        protected double CalcularValorActualPrestamoAPartirDeTasaInteresConstante_VA(double tasaNominal,
                                                                                  double plazoMeses,
                                                                                  double valorCuotaCierreFinanciero,
                                                                                  double valor)
        {
            return (Financial.PV(tasaNominal, plazoMeses, valorCuotaCierreFinanciero, 0) * -1);
        }
        protected void AsignarValorFormCapacidadPagoGrupos(ref List<FormCapacidadPagoGrupos> listaFormFormCapacidadPagoGrupos,
                                                           enumFormCapacidadPagoGrupos idGrupo,
                                                           enumFormCapacidadPago idCapacidadPago,
                                                           string propiedad,
                                                           float? valor)
        {
            switch (propiedad.ToUpper())
            {
                case "VALORASOCIADO":
                    listaFormFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)idGrupo)
                                        .SingleOrDefault()
                                        .ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)idCapacidadPago)
                                        .SingleOrDefault().ValorAsociado = valor;
                    break;
                case "VALORFAMILIAR":
                    listaFormFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)idGrupo)
                                        .SingleOrDefault()
                                        .ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)idCapacidadPago)
                                        .SingleOrDefault().ValorFamiliar = valor;
                    break;
            }
        }
        protected void AsignarValorTotalGrupo(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos,
                                                           enumFormCapacidadPagoGrupos idGrupo,
                                                           float? valor)
        {
            listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)idGrupo).SingleOrDefault().Total
                                 = valor;
        }

        protected void MostrarGrupo(ref List<FormCapacidadPagoGrupos> listaFormFormCapacidadPagoGrupos,
                                                           enumFormCapacidadPagoGrupos idGrupo,
                                                           bool mostrar)
        {
            listaFormFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)idGrupo)
                                        .SingleOrDefault()
                                        .Mostrar = mostrar;
        }

        protected abstract void CalcularGrupoEndeudamientoFinanciero(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, float? totalIngresosBrutosAsociado, float? totalIngresosBrutosFamiliar, float? valorCuotaCierreFinanciero, float? gastosFinancierosAsociado, string tipoCapacidadPago);
        protected abstract void CalcularGrupoValorCredito(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, short anhosValorCredito,ref float? tasaCredito_E_A,ref float? tasaNominal,ref short plazoMeses, FormCapacidadPagoGrupos grupoValorCreditoVivenda, float? valorCuotaCierreFinanciero,ref float? totalValorCredito, float? factorValorMaximoViviendaNoVis, float? factorValorMaximoViviendaVis, int idTipoCreditoTipoRolRelacion, int idPapelCavipetrol, string tipoCapacidadPago, float? valorSeguros);
        protected abstract void CalcularGrupoValorCuota(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos,
                                        ref float? valorCuotaCierreFinanciero,
                                        FormCapacidadPagoGrupos grupoMaximoDescuentoPorNomina,
                                        float? ingresoNetoViviendaFamiliar, string tipoCapacidadPago, List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion, double saldo, enumPapelCavipetrol papelCavipetrol, int edad, double valorTotalInmueble, double tasaMensualSeguroVida, double PorcenExtraPrima);
        protected abstract void CalcularGrupoMaximoDescuentoPorNominaYCaja(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, ref float? factorMaximoDescuentoNomina, ref FormCapacidadPagoGrupos grupoMaximoDescuentoPorNomina, ref float? maximoDescuentoNomina, float? subtotalIngresosMenosDescuentosAsociado, ref float? factorMaximoValorPagoPorCaja, ref float? maximoValorPagoPorCaja, string tipoCapacidadPago, float? otrosIngresos, FormCapacidadPagoGrupos grupoIngresoNetoDisponible);
        protected abstract void CalcularGrupoIngresoNetoDisponible(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, float? totalIngresosBrutosAsociado, float? totalGastosAsociado, float? totalIngresosBrutosFamiliar, float? totalGastosFamiliar, ref float? ingresoFamiliarDisponibleTotal, ref float? factorIngresoNetoVivienda, ref FormCapacidadPagoGrupos grupoIngresoNetoDisponible, ref float? ingresoNetoViviendaAsociado, ref float? ingresoNetoViviendaFamiliar,
            float? subtotalIngresos, float? otrosIngresos, string tipoCapacidadPago);
        protected abstract void CalcularGrupoGastos(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, ref FormCapacidadPagoGrupos grupoGastos, float? gastosFamiliaresAsociado, float? gastosFamiliaresFamiliar, ref float? totalGastosAsociado, float? totalGastosFamiliar, float? gastosFinancieros, string tipoCapacidadPago, float? gastosFinancierosFamiliar, float? smmlv);

        protected abstract void CalcularGrupoOtrosIngresos(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, float? totalIngresosBrutosAsociado, float? totalIngresosBrutosFamiliar);

        protected abstract void CalcularGrupoOtrasDeducciones(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, float? subtotalIngresosMenosDescuentosAsociado, float? subtotalIngresosMenosDescuentosFamiliar);
        /// <summary>
        /// Recorre ListaFormCapacidadPagoGrupos y Asigna null a todos los valores asociado y familiar que sean de soloLectura 
        /// </summary>
        /// <param name="listaFormCapacidadPagoGrupos"></param>
        protected void LimpiarListaFormCapacidadPagoGrupos(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos) {
            foreach (FormCapacidadPagoGrupos FormCapacidadPagoGrupos in listaFormCapacidadPagoGrupos)
            {
                foreach (FormCapacidadPago formCapacidadPago in FormCapacidadPagoGrupos.ListaFormCapacidadPago)
                {
                    if (formCapacidadPago.EsSoloLecturaAsociado == true)
                    {
                        formCapacidadPago.ValorAsociado = 0;
                    }
                    if (formCapacidadPago.EsSoloLecturaFamiliar == true)
                    {
                        formCapacidadPago.ValorFamiliar = 0;
                    }
                }
            }
        }
        public void GenerarTablaAmortizacion(int periodos, 
                                             double tasaNominal, 
                                             double tasaEA, 
                                             double valorCredito, 
                                             ref List<Amortizacion> tablaAmortizacion, 
                                             List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion, 
                                             double? valorTotalInmueble, 
                                             short idTipoPoliza,
                                             double tasaMensualSeguroVida,
                                             double PorcenExtraPrima,
                                             enumPapelCavipetrol papelCavipetrol,
                                             int gracia,
                                             int edad,
                                             int idFormaPago,
                                             ref float? ValorSeguros,
                                             int idTipoGarantia)
        {
            tablaAmortizacion = new List<Amortizacion>();          
            Amortizacion amortizacion;            
            double saldo = valorCredito;
            double cuota = 0;
            double cuotaConSeguros = 0;
            double abonos = 0;
            double intereses = 0;
            double capital = 0;
            double capitalAcumulado = 0;
            double cuotaDividendo = 0;
            double cuotaDivisor = 0;
            double seguro = 0;
            double seguroVida = 0;
            double seguroIncendio = 0;
            double saldoAnterior = 0;
            ValorSeguros = 0;

            for (int i = 0; i <= periodos; i++)
            {
                cuotaDividendo = 0;
                cuotaDivisor = 0;
                seguro = 0;
                if (i <= periodos && i != 0)
                {
                
                    intereses = saldo * tasaNominal;
                    cuotaDividendo = (tasaNominal * Math.Pow((1 + tasaNominal), (periodos - (i - 1))) * saldo);
                    cuotaDivisor = ((Math.Pow((1 + tasaNominal), (periodos - (i - 1))) - 1) + abonos);

                    if(cuota == 0) {
                        cuota = cuotaDividendo / cuotaDivisor;
                    }                    
                    //cuota =  gracia > 0 ? intereses : cuotaDividendo / cuotaDivisor;
                    //capital = gracia > 0 ? capital : cuota - intereses;
                    capital = cuota - intereses;
                    saldoAnterior = saldo;
                    saldo = saldo - capital;
                    //saldo = gracia > 0 ? saldo : saldo - capital;
                    capitalAcumulado += capital;
                    switch (idTipoPoliza)
                    {
                        case (short)enumTipoPoliza.Interna:
                            seguroVida = ObtenerTotalSeguros(listTipoCreditoTipoSeguroRelacion, saldoAnterior, valorTotalInmueble, tasaMensualSeguroVida, PorcenExtraPrima, papelCavipetrol, enumTipoSeguro.Seguro_de_vida, edad);
                            seguro += seguroVida;
                            seguroIncendio = ObtenerSeguroInsendioYterremoro(listTipoCreditoTipoSeguroRelacion, saldo, valorTotalInmueble, tasaMensualSeguroVida, PorcenExtraPrima, papelCavipetrol, enumTipoSeguro.Seguro_de_incendio_y_terremoto, edad, idTipoGarantia, idFormaPago, PorcenExtraPrima);                            
                            seguro += seguroIncendio;
                            break;                       
                        default:
                            seguroIncendio = ObtenerSeguroInsendioYterremoro(listTipoCreditoTipoSeguroRelacion, saldo, valorTotalInmueble, tasaMensualSeguroVida, PorcenExtraPrima, papelCavipetrol, enumTipoSeguro.Seguro_de_incendio_y_terremoto, edad, idTipoGarantia, idFormaPago, PorcenExtraPrima);
                            seguro += seguroIncendio;
                            break;
                    }
                    ValorSeguros = ValorSeguros + (float)seguro;
                    cuotaConSeguros = cuota + seguro;                                                                
                    gracia = gracia == 0 ? gracia : (gracia - 1);
                }
                else
                {
                    intereses = 0;
                    cuota = 0;
                    capital = 0;
                    capitalAcumulado = 0;
                    saldo = saldo - capital;
                    seguro = 0;                    
                }

                amortizacion = new Amortizacion();
                amortizacion.Periodo = i;
                amortizacion.Capital = ((decimal)capital).ToString("C0");
                amortizacion.Intereses = ((decimal)intereses).ToString("C0");
                amortizacion.Cuota = ((decimal)cuotaConSeguros).ToString("C0");
                amortizacion.Saldo = ((decimal)saldo).ToString("C0");
                amortizacion.CapitalAcumulado = ((decimal)capitalAcumulado).ToString("C0");
                amortizacion.Abonos = ((decimal)abonos).ToString("C0");
                amortizacion.Seguro = seguro.ToString("C0");
                amortizacion.SeguroVida = seguroVida.ToString("C0");
                amortizacion.SeguroIncendio = seguroIncendio.ToString("C0");

                //amortizacion.Capital = ((decimal)capital).ToString();
                //amortizacion.Intereses = ((decimal)intereses).ToString();
                //amortizacion.Cuota = ((decimal)cuotaConSeguros).ToString();
                //amortizacion.Saldo = ((decimal)saldo).ToString();
                //amortizacion.CapitalAcumulado = ((decimal)capitalAcumulado).ToString();
                //amortizacion.Abonos = ((decimal)abonos).ToString();
                //amortizacion.Seguro = seguro.ToString();
                //amortizacion.SeguroVida = seguroVida.ToString();
                //amortizacion.SeguroIncendio = seguroIncendio.ToString();

                tablaAmortizacion.Add(amortizacion);
                gracia--;
            }           
        }

        private double ObtenerSeguroInsendioYterremoro(List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion, double saldo, double? valorTotalInmueble, double tasaMensualSeguroVida, double porcenExtraPrima, enumPapelCavipetrol papelCavipetrol, enumTipoSeguro seguro_de_incendio_y_terremoto, int edad, int idTipoGarantia, int idFormaPago, double PorcenExtraPrima)
        {
            double seguroIncendio = 0;
            switch (idTipoGarantia)
            {
                case (int)enumTipoGarantia.Real:
                    seguroIncendio = ObtenerTotalSeguros(listTipoCreditoTipoSeguroRelacion, saldo, valorTotalInmueble, tasaMensualSeguroVida, PorcenExtraPrima, papelCavipetrol, enumTipoSeguro.Seguro_de_incendio_y_terremoto, edad);
                    switch (idFormaPago)
                    {
                        case (int)enumFormaPago.Quincenal:
                            seguroIncendio = seguroIncendio / 2;
                            break;
                        case (int)enumFormaPago.Semestral:
                            seguroIncendio = seguroIncendio * 6;
                            break;
                        case (int)enumFormaPago.Anual:
                            seguroIncendio = seguroIncendio * 12;
                            break;
                    }
                    break;
                case (int)enumTipoGarantia.Real2:
                    seguroIncendio = ObtenerTotalSeguros(listTipoCreditoTipoSeguroRelacion, saldo, valorTotalInmueble, tasaMensualSeguroVida, PorcenExtraPrima, papelCavipetrol, enumTipoSeguro.Seguro_de_incendio_y_terremoto, edad);
                    switch (idFormaPago)
                    {
                        case (int)enumFormaPago.Quincenal:
                            seguroIncendio = seguroIncendio / 2;
                            break;
                        case (int)enumFormaPago.Semestral:
                            seguroIncendio = seguroIncendio * 6;
                            break;
                        case (int)enumFormaPago.Anual:
                            seguroIncendio = seguroIncendio * 12;
                            break;
                    }
                    break;
                default:
                    seguroIncendio = 0;
                    break;
            }
            return seguroIncendio;
        }

        private double ObtenerTotalSeguros(List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion, 
                                           double saldo, 
                                           double? valorTotalInmueble,
                                           double tasaMensualSeguroVida,
                                           double PorcenExtraPrima,
                                           enumPapelCavipetrol papelCavipetrol,
                                           enumTipoSeguro tipoSeguro,
                                           int edad)
        {
            decimal totalSeguro = 0;
            /// Cavipetrol asume el valor de la extraprima para los siguientes papeles
            if (papelCavipetrol == enumPapelCavipetrol.Activo ||
               papelCavipetrol == enumPapelCavipetrol.Jub_Afil ||
               papelCavipetrol == enumPapelCavipetrol.Empleado_Temp) {
                PorcenExtraPrima = 0;
            }

            switch (tipoSeguro) {
                case enumTipoSeguro.Seguro_de_incendio_y_terremoto:
                    var factor = listTipoCreditoTipoSeguroRelacion.Where(x => x.IdTipoSeguro == (int)tipoSeguro)
                                                                  .Where(y => y.RangoInicial <= edad && y.RangoFinal >= edad).FirstOrDefault();
                    totalSeguro += (decimal)(valorTotalInmueble * (factor == null ? 0 : factor.Factor));
                    break;
                case enumTipoSeguro.Seguro_de_vida:
                    double? PorcenModeloFactor, SumatoriaModeloFactor;
                    if (PorcenExtraPrima != 0)
                    {
                        PorcenModeloFactor = tasaMensualSeguroVida * PorcenExtraPrima;
                        SumatoriaModeloFactor = tasaMensualSeguroVida + PorcenModeloFactor;
                    }
                    else
                    {
                        SumatoriaModeloFactor = tasaMensualSeguroVida;
                    }
                    totalSeguro += (decimal)(saldo * SumatoriaModeloFactor);
                    break;
            }

            return (double)totalSeguro;
            
            //foreach (TiposCreditoTipoSeguroRelacion modelo in listTipoCreditoTipoSeguroRelacion)
            //{
                
            //    double? PorcenModeloFactor, SumatoriaModeloFactor;
            //    switch (modelo.IdTipoSeuro)
            //    {
            //        case (int)enumTipoSeguro.Seguro_de_vida:
            //            if (PorcenExtraPrima != 0)
            //            {
            //                PorcenModeloFactor = tasaMensualSeguroVida * PorcenExtraPrima;
            //                SumatoriaModeloFactor = tasaMensualSeguroVida + PorcenModeloFactor;
            //            }
            //            else
            //            {
            //                SumatoriaModeloFactor = tasaMensualSeguroVida;
            //            }
            //            totalSeguro += (decimal)(saldo * SumatoriaModeloFactor);
            //            // totalSeguro += (decimal)(saldo * tasaMensualSeguroVida);
            //            break;
            //        case (int)enumTipoSeguro.Seguro_de_incendio_y_terremoto:
            //            totalSeguro += (decimal)(valorTotalInmueble * modelo.Factor);
            //            break;
            //        default:
            //            break;
            //    }

            //}
            //return (double)totalSeguro;
        }
    }           
}
