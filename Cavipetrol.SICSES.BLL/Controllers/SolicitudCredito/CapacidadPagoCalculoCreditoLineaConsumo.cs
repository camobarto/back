﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.Creditos;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.Globales;

namespace Cavipetrol.SICSES.BLL.Controllers.SolicitudCredito
{
    public class CapacidadPagoCalculoCreditoLineaConsumo : CapacidadPagoCalculoCreditoAbstracta
    {
        private readonly IConsultasApoyoUnidadTrabajo _consultasApoyoUnidadTrabajo;


        public CapacidadPagoCalculoCreditoLineaConsumo()
        {
            _consultasApoyoUnidadTrabajo = new ConsultasApoyoUnidadTrabajo();
        }
        
        public override void Calcular(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, 
                                      ref List<Amortizacion> tablaAmortizacion, 
                                      int idTipoCreditoTipoRolRelacion, 
                                      List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion, 
                                      double valorTotalInmueble,
                                      short idTipoPoliza,
                                      double valorNegociado,
                                      double tasaMensualSeguroVida,
                                      double PorcenExtraPrima,
                                      int idPapelCavipetrol,
                                      string tipoCapacidadPago,
                                      int edad,
                                      int idTipoGarantia
            )
        {            
            LimpiarListaFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos);
            FormCapacidadPagoGrupos grupoIngresos = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Ingresos).SingleOrDefault();
            FormCapacidadPagoGrupos grupoDescuentosLey = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Descuentos_por_ley).SingleOrDefault();
            FormCapacidadPagoGrupos grupoOtrasDeducciones = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Otras_Deducciones).SingleOrDefault();
            FormCapacidadPagoGrupos grupoOtrosIngresos = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Otros_Ingresos).SingleOrDefault();
            FormCapacidadPagoGrupos grupoGastos = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Gastos).SingleOrDefault();
            FormCapacidadPagoGrupos grupoIngresoNetoDisponible = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible).SingleOrDefault();
            FormCapacidadPagoGrupos grupoMaximoDescuentoPorNomina = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Maximo_Descuento_por_Nomina_y_Caja).SingleOrDefault();
            FormCapacidadPagoGrupos grupoValorCreditoVivenda = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Credito).SingleOrDefault();
            FormCapacidadPagoGrupos grupoSolvencia = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Solvencia).SingleOrDefault();
            float? ValorSeguros = 0;
            #region Grupo Ingresos             
            CalcularValorTotalGrupo(grupoIngresos, ref totalIngresosAsociado, ref totalIngresosFamiliar);
            AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingresos, (totalIngresosAsociado ?? 0) + (totalIngresosFamiliar ?? 0));
            #endregion

            #region Grupo Descuentos de Ley                        
            CalcularValorTotalGrupo(grupoDescuentosLey, ref totalDescuentosLeyAsociado, ref totalDescuentosLeyFamiliar);
            AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Descuentos_por_ley, (totalDescuentosLeyAsociado ?? 0) + (totalDescuentosLeyFamiliar ?? 0));
            #endregion

            #region Grupo Otras deducciones                        
            CalcularValorTotalGrupo(grupoOtrasDeducciones, ref totalOtrasDeduccionesAsociado, ref totalOtrasDeduccionesFamiliar);
            #endregion

            #region Grupo Ingresos decuentos            
            subtotalIngresosMenosDescuentosAsociado = (float?)(totalIngresosAsociado - (totalDescuentosLeyAsociado + totalOtrasDeduccionesAsociado));
            subtotalIngresosMenosDescuentosFamiliar = (float?)(totalIngresosFamiliar - (totalDescuentosLeyFamiliar + totalOtrasDeduccionesFamiliar));
            #endregion

            #region Obtener valores Grupo Otros ingresos                        
            ObtenerValorGrupoOtrosIngresos(grupoOtrosIngresos, ref otrosIngresosAsociado, ref otrosIngresosFamiliar, tipoCapacidadPago);
            #endregion  

            #region Grupo Otras deducciones
            switch (tipoCapacidadPago)
            {                
                case "A_Consumo":
                    totalIngresosBrutosAsociado = subtotalIngresosMenosDescuentosAsociado + (float?)((otrosIngresosAsociado) * 0.7);
                    break;
                case "B_Consumo":
                    totalIngresosBrutosAsociado = subtotalIngresosMenosDescuentosAsociado + (float?)((otrosIngresosAsociado) * 0.7);
                    break;
                case "E_Consumo":
                    totalIngresosBrutosAsociado = subtotalIngresosMenosDescuentosAsociado + (float)(otrosIngresosAsociado * 0.7);
                    break;
                default:
                    totalIngresosBrutosAsociado = subtotalIngresosMenosDescuentosAsociado + (float?)((otrosIngresosAsociado) * 0.7);
                    totalIngresosBrutosFamiliar = subtotalIngresosMenosDescuentosFamiliar + (otrosIngresosFamiliar / 2);
                    break;
            }                      
            CalcularGrupoOtrasDeducciones(ref listaFormCapacidadPagoGrupos, subtotalIngresosMenosDescuentosAsociado, subtotalIngresosMenosDescuentosFamiliar);
            #endregion

            #region Otros ingresos            
            CalcularGrupoOtrosIngresos(ref listaFormCapacidadPagoGrupos, totalIngresosBrutosAsociado, totalIngresosBrutosFamiliar);
            #endregion

            #region Grupo Gastos
            string indicadorGrupo = listaFormCapacidadPagoGrupos[5].ListaFormCapacidadPago[1].Texto;
            if (indicadorGrupo != "SMMLV")
            {
                factorGastosFamiliares = (float?)0.7;//((float?)grupoGastos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Gastos_Familiares).SingleOrDefault().Factor ?? 0);
                gastosFamiliaresAsociado = (totalIngresosAsociado * (factorGastosFamiliares));
                gastosFamiliaresFamiliar = (totalIngresosFamiliar * (factorGastosFamiliares));
                gastosFinancierosFamiliar = (grupoGastos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Gastos_financieros).SingleOrDefault().ValorFamiliar ?? 0);
            }
            gastosFinancierosAsociado = (grupoGastos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Gastos_financieros).SingleOrDefault().ValorAsociado ?? 0);
            totalGastosFamiliar = gastosFamiliaresFamiliar + gastosFinancierosFamiliar;
            Parametros parametros = new Parametros();
            parametros = _consultasApoyoUnidadTrabajo.ObtenerParametros("SMMLV").LastOrDefault();
            float smmlv = 1;
            float.TryParse(parametros.Valor, out smmlv);

            if (indicadorGrupo == "SMMLV")
                totalGastosAsociado = smmlv + gastosFinancierosAsociado;
            else
                totalGastosAsociado = gastosFamiliaresAsociado + gastosFinancierosAsociado;
            CalcularGrupoGastos(ref listaFormCapacidadPagoGrupos,
                                ref grupoGastos,
                                gastosFamiliaresAsociado,
                                gastosFamiliaresFamiliar,
                                ref totalGastosAsociado,
                                totalGastosFamiliar,
                                gastosFinancierosAsociado, 
                                tipoCapacidadPago, 0,
                                smmlv);
            #endregion

            #region Grupo Ingreso neto disponible                
            CalcularGrupoIngresoNetoDisponible(ref listaFormCapacidadPagoGrupos,
                                               totalIngresosBrutosAsociado,
                                               totalGastosAsociado,
                                               totalIngresosBrutosFamiliar,
                                               totalGastosFamiliar,
                                               ref ingresoFamiliarDisponibleTotal,
                                               ref factorIngresoNetoVivienda,
                                               ref grupoIngresoNetoDisponible,
                                               ref ingresoNetoViviendaAsociado,
                                               ref ingresoNetoViviendaFamiliar,
                                               (float?)(grupoIngresos.Total ?? 0),
                                               (float?)(grupoOtrosIngresos.Total ?? 0),
                                               tipoCapacidadPago);
            MostrarGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible, true);
            #endregion

            #region Grupo Maximo descuento por nomina                  
            CalcularGrupoMaximoDescuentoPorNominaYCaja(ref listaFormCapacidadPagoGrupos,
                                                       ref factorMaximoDescuentoNomina,
                                                       ref grupoMaximoDescuentoPorNomina,
                                                       ref maximoDescuentoNomina,
                                                       subtotalIngresosMenosDescuentosAsociado,
                                                       ref factorMaximoValorPagoPorCaja,
                                                       ref maximoValorPagoPorCaja,
                                                       tipoCapacidadPago, 
                                                       (otrosIngresosAsociado + otrosIngresosFamiliar), 
                                                       grupoIngresoNetoDisponible);
            MostrarGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Maximo_Descuento_por_Nomina_y_Caja, true);
            #endregion

            #region Grupo Valor cuota            

            CalcularGrupoValorCuota(ref listaFormCapacidadPagoGrupos,
                               ref valorCuotaCierreFinanciero,
                               grupoMaximoDescuentoPorNomina,
                               ingresoFamiliarDisponibleTotal,
                               tipoCapacidadPago, listTipoCreditoTipoSeguroRelacion, valorNegociado, (enumPapelCavipetrol)idPapelCavipetrol, edad, valorTotalInmueble, tasaMensualSeguroVida, PorcenExtraPrima);
            MostrarGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Cuota, true);
            #endregion
            
            #region Grupo Valor credito            
            CalcularGrupoValorCredito(ref listaFormCapacidadPagoGrupos,
                                      anhosValorCredito,
                                      ref tasaCredito_E_A,
                                      ref tasaNominal,
                                      ref plazoMeses,
                                      grupoValorCreditoVivenda,
                                      valorCuotaCierreFinanciero,
                                      ref totalValorCreditoVivienda,
                                      factorValorMaximoViviendaNoVis,
                                      factorValorMaximoViviendaVis,
                                      idTipoCreditoTipoRolRelacion,
                                      idPapelCavipetrol,
                                      tipoCapacidadPago,
                                      ValorSeguros);
            #endregion

            #region Grupo Endeudamiento financiero 
            CalcularGrupoEndeudamientoFinanciero(ref listaFormCapacidadPagoGrupos,
                                                 totalIngresosBrutosAsociado,
                                                 totalIngresosBrutosFamiliar,
                                                 valorCuotaCierreFinanciero,
                                                 gastosFinancierosAsociado,
                                                 tipoCapacidadPago);
            MostrarGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero, true);
            #endregion

            #region Grupo solvencia
            CalcularGrupoSolvencia(ref listaFormCapacidadPagoGrupos, grupoSolvencia);
            #endregion

            GenerarTablaAmortizacion(plazoMeses, (double)tasaNominal / 100, (double)tasaCredito_E_A, (double)totalValorCreditoVivienda, ref tablaAmortizacion, listTipoCreditoTipoSeguroRelacion, valorTotalInmueble, idTipoPoliza, tasaMensualSeguroVida, PorcenExtraPrima, (enumPapelCavipetrol)idPapelCavipetrol, 0, edad, 0, ref ValorSeguros, idTipoGarantia);

            totalValorCreditoVivienda = totalValorCreditoVivienda - ValorSeguros;
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Valor_maximo_por_capacidad_de_pago, "ValorAsociado",
                                               totalValorCreditoVivienda);
        }
        private void ObtenerValorGrupoOtrosIngresos(FormCapacidadPagoGrupos grupoOtrosIngresos, ref float? otrosIngresosAsociado, ref float? otrosIngresosFamiliar, string tipoCapacidadPago)
        {
            float? factor = 0;
            switch (tipoCapacidadPago) {
                case "A_Consumo":
                    factor = (float?)(grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().Factor ?? 1);
                    otrosIngresosAsociado = ((grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().ValorAsociado / 2)  ?? 0) * factor;
                    otrosIngresosFamiliar = ((grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().ValorFamiliar / 2) ?? 0) * factor;
                    break;
                //case "C_Consumo":
                //    factor = (float?)(grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().Factor ?? 1);
                //    otrosIngresosAsociado = ((grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().ValorAsociado / 2) ?? 0) * factor;
                //    otrosIngresosFamiliar = ((grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().ValorFamiliar / 2) ?? 0) * factor;
                //    break;
                default:
                    factor = (float?)(grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().Factor ?? 1);
                    otrosIngresosAsociado = ((grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().ValorAsociado) ?? 0) * factor;
                    otrosIngresosFamiliar = ((grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().ValorFamiliar) ?? 0) * factor;
                    break;
            }
            
        }
        
        protected override void CalcularGrupoEndeudamientoFinanciero(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, float? totalIngresosBrutosAsociado, float? totalIngresosBrutosFamiliar, float? valorCuotaCierreFinanciero, float? gastosFinancierosAsociado, string tipoCapacidadPago)
        {
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                     enumFormCapacidadPago.Total_Ingresos, "ValorAsociado",
                                     totalIngresosBrutosAsociado);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                                enumFormCapacidadPago.Total_Ingresos, "ValorFamiliar",
                                                totalIngresosBrutosFamiliar);

         

            switch (tipoCapacidadPago) {
                case "E_Consumo":
                    AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                             enumFormCapacidadPago.Total_Deuda_Financiera, "ValorAsociado",
                                             Math.Abs((float)valorCuotaCierreFinanciero) + gastosFinancierosAsociado);

                    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                  (((Math.Abs((float)valorCuotaCierreFinanciero) + gastosFinancierosAsociado) / totalIngresosBrutosAsociado) * 100));
                    break;
                //case "C_Consumo":
                //    AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                //                             enumFormCapacidadPago.Total_Deuda_Financiera, "ValorAsociado",
                //                             Math.Abs((float)valorCuotaCierreFinanciero) + gastosFinancierosAsociado);

                //    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                //                  (((Math.Abs((float)valorCuotaCierreFinanciero) + gastosFinancierosAsociado) / totalIngresosBrutosAsociado) * 100));
                //    break;
                default:
                    AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                             enumFormCapacidadPago.Total_Deuda_Financiera, "ValorAsociado",
                                             valorCuotaCierreFinanciero + gastosFinancierosAsociado);

                    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                  (((valorCuotaCierreFinanciero + gastosFinancierosAsociado) / totalIngresosBrutosAsociado) * 100));
                    break;
            }
          
        }

        protected override void CalcularGrupoValorCredito(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, short anhosValorCredito, ref float? tasaCredito_E_A, ref float? tasaNominal, ref short plazoMeses, FormCapacidadPagoGrupos grupoValorCreditoVivenda, float? valorCuotaCierreFinanciero, ref float? totalValorCreditoVivienda, float? factorValorMaximoViviendaNoVis, float? factorValorMaximoViviendaVis, int idTipoCreditoTipoRolRelacion, int idPapelCavipetrol, string tipoCapacidadPago, float? valorSeguros)
        {
            switch (tipoCapacidadPago) {
                case "A_VIVIENDA":
                    CalcularTipoCapacidadPago(ref listaFormCapacidadPagoGrupos, anhosValorCredito, ref tasaCredito_E_A, ref tasaNominal, ref plazoMeses, grupoValorCreditoVivenda, valorCuotaCierreFinanciero, ref totalValorCreditoVivienda, factorValorMaximoViviendaNoVis, factorValorMaximoViviendaVis, idTipoCreditoTipoRolRelacion, idPapelCavipetrol, tipoCapacidadPago);
                    break;
                default:
                    CalcularTipoCapacidadPago(ref listaFormCapacidadPagoGrupos, anhosValorCredito, ref tasaCredito_E_A, ref tasaNominal, ref plazoMeses, grupoValorCreditoVivenda, valorCuotaCierreFinanciero, ref totalValorCreditoVivienda, factorValorMaximoViviendaNoVis, factorValorMaximoViviendaVis, idTipoCreditoTipoRolRelacion, idPapelCavipetrol, tipoCapacidadPago);
                    break;
            }
         
        }


        public void CalcularTipoCapacidadPago(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, short anhosValorCredito, ref float? tasaCredito_E_A, ref float? tasaNominal, ref short plazoMeses, FormCapacidadPagoGrupos grupoValorCreditoVivenda, float? valorCuotaCierreFinanciero, ref float? totalValorCreditoVivienda, float? factorValorMaximoViviendaNoVis, float? factorValorMaximoViviendaVis, int idTipoCreditoTipoRolRelacion, int idPapelCavipetrol, string tipoCapacidadPago) {
            //anhosValorCredito = (short)(grupoValorCreditoVivenda.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Plazo_anos_valorCredito).SingleOrDefault().ValorAsociado ?? 0);
            mesesValorCredito = (short)(grupoValorCreditoVivenda.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Plazo_meses_valorCredito).SingleOrDefault().ValorAsociado ?? 0);
            //AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
            //                                    enumFormCapacidadPago.Plazo_meses_valorCredito, "ValorAsociado",
            //                                    anhosValorCredito * 12);
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Plazo_meses_valorCredito, "ValorAsociado",
                                                mesesValorCredito);

            TipoCreditoTipoRolRelacion tipoCreditoTipoRolRelacion = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposRolRelacion(0, idTipoCreditoTipoRolRelacion).SingleOrDefault();

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Tasa_Credito_EA_Consumo, "ValorAsociado",
                                                (float?)(tipoCreditoTipoRolRelacion.InteresEfectivoAnual * 100));

            tasaNominal = (float?)CalcularTasaNominal((double)(tipoCreditoTipoRolRelacion.InteresEfectivoAnual), (double)12);
            //plazoMeses = (short)(anhosValorCredito * 12);
            plazoMeses = mesesValorCredito;
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Tasa_Credito_N_M_V, "ValorAsociado",
                                                tasaNominal);

            var valorCuotaCierreFinal = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(y => y.IdCapacidadPago == (int)enumFormCapacidadPago.Cuota_Cierre_Final).SingleOrDefault().ValorAsociado;

            //totalValorCreditoVivienda = (float?)CalcularValorActualPrestamoAPartirDeTasaInteresConstante_VA((double)(tasaNominal / 100), plazoMeses, (double)valorCuotaCierreFinanciero, 0);
            //Se cambia Valor Cuota cierre financiero por totalValorCreditoVivienda a Peticion de Gerente de Riesgos 
            totalValorCreditoVivienda = (float?)CalcularValorActualPrestamoAPartirDeTasaInteresConstante_VA((double)(tasaNominal / 100), plazoMeses, (double)valorCuotaCierreFinal, 0);
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Valor_maximo_por_capacidad_de_pago, "ValorAsociado",
                                               totalValorCreditoVivienda);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Valor_maximo_por_capacidad_de_pago, "ValorAsociado",
                                               totalValorCreditoVivienda);
        }
        private double ObtenerTotalSeguros(List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion,
                                           double saldo,
                                           double? valorTotalInmueble,
                                           double tasaMensualSeguroVida,
                                           double PorcenExtraPrima,
                                           enumPapelCavipetrol papelCavipetrol,
                                           enumTipoSeguro tipoSeguro,
                                           int edad)
        {
            decimal totalSeguro = 0;
            /// Cavipetrol asume el valor de la extraprima para los siguientes papeles
            if (papelCavipetrol == enumPapelCavipetrol.Activo ||
               papelCavipetrol == enumPapelCavipetrol.Jub_Afil ||
               papelCavipetrol == enumPapelCavipetrol.Empleado_Temp)
            {
                PorcenExtraPrima = 0;
            }

            switch (tipoSeguro)
            {
                case enumTipoSeguro.Seguro_de_incendio_y_terremoto:
                    var factor = listTipoCreditoTipoSeguroRelacion.Where(x => x.IdTipoSeguro == (int)tipoSeguro)
                                                                  .Where(y => y.RangoInicial <= edad && y.RangoFinal >= edad).FirstOrDefault();
                    totalSeguro += (decimal)(valorTotalInmueble * (factor == null ? 0 : factor.Factor));
                    break;
                case enumTipoSeguro.Seguro_de_vida:
                    double? PorcenModeloFactor, SumatoriaModeloFactor;
                    if (PorcenExtraPrima != 0)
                    {
                        PorcenModeloFactor = tasaMensualSeguroVida * PorcenExtraPrima;
                        SumatoriaModeloFactor = tasaMensualSeguroVida + PorcenModeloFactor;
                    }
                    else
                    {
                        SumatoriaModeloFactor = tasaMensualSeguroVida;
                    }
                    totalSeguro += (decimal)(saldo * SumatoriaModeloFactor);
                    break;
            }

            return (double)totalSeguro;
         
        }
        protected override void CalcularGrupoValorCuota(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, ref float? valorCuotaCierreFinanciero, FormCapacidadPagoGrupos grupoMaximoDescuentoPorNomina, float? ingresoFamiliarDisponibleTotal, string tipoCapacidadPago, List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion, double saldo, enumPapelCavipetrol papelCavipetrol, int edad, double valorTotalInmueble, double tasaMensualSeguroVida, double PorcenExtraPrima)
        {

            //var seguroVida = ObtenerTotalSeguros(listTipoCreditoTipoSeguroRelacion, saldo, valorTotalInmueble, tasaMensualSeguroVida, PorcenExtraPrima, papelCavipetrol, enumTipoSeguro.Seguro_de_vida, edad);                    
            //var seguroIncendio = ObtenerTotalSeguros(listTipoCreditoTipoSeguroRelacion, saldo, valorTotalInmueble, tasaMensualSeguroVida, PorcenExtraPrima, papelCavipetrol, enumTipoSeguro.Seguro_de_incendio_y_terremoto, edad);
            valorCuotaCierreFinanciero = (float?)(grupoMaximoDescuentoPorNomina.Total < ingresoFamiliarDisponibleTotal ?
                         grupoMaximoDescuentoPorNomina.Total : ingresoFamiliarDisponibleTotal);
            //valorCuotaCierreFinanciero = valorCuotaCierreFinanciero - (float?)(seguroVida + seguroIncendio);

            //valorCuotaCierreFinanciero = ((float?)grupoMaximoDescuentoPorNomina.Total < ingresoNetoViviendaFamiliar ?
            //                           (float?)grupoMaximoDescuentoPorNomina.Total : ingresoNetoViviendaFamiliar);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Cuota,
                                                enumFormCapacidadPago.Valor_Cuota_cierre_Financiero, "ValorAsociado",
                                                valorCuotaCierreFinanciero);
            float? maximoDescuentoPorNomina = 0;
            float valorPorCaja = 0;
            maximoDescuentoPorNomina = grupoMaximoDescuentoPorNomina.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Máximo_descuento_de_nomina).SingleOrDefault().ValorAsociado;
            float? totalIngresoNetoDisponible = (float?)listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Ingreso_Familiar_Disponible_Total).SingleOrDefault().ValorAsociado;

            if (maximoDescuentoPorNomina < 0)
            {
                maximoDescuentoPorNomina = 0;
            }
            else if (Math.Min((float)ingresoFamiliarDisponibleTotal, (float)maximoDescuentoPorNomina) < 0)
            {
                maximoDescuentoPorNomina = 0;
            }
            else {
                maximoDescuentoPorNomina = Math.Min((float)ingresoFamiliarDisponibleTotal, (float)maximoDescuentoPorNomina);
            }

            listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Nomina).SingleOrDefault().ValorAsociado = maximoDescuentoPorNomina ;




            valorPorCaja = 0;
            if (maximoDescuentoPorNomina < 0 && ingresoFamiliarDisponibleTotal < 0) {
                valorPorCaja = 0;
            }if (maximoDescuentoPorNomina < 0)
            {
                valorPorCaja = (float)ingresoFamiliarDisponibleTotal;
            }
            else if ((ingresoFamiliarDisponibleTotal - maximoDescuentoPorNomina) < 0)
            {
                valorPorCaja = 0;
            }
            else {
                valorPorCaja = (float)(ingresoFamiliarDisponibleTotal - maximoDescuentoPorNomina);
            }

            listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Caja).SingleOrDefault().ValorAsociado = (valorPorCaja < 0 ? 0 : valorPorCaja);
            var cuota_Cierre_Final = valorPorCaja + maximoDescuentoPorNomina;

            if (tipoCapacidadPago == "A_Consumo")
                cuota_Cierre_Final = cuota_Cierre_Final * 2;
            switch (tipoCapacidadPago)
            {
                //case "C_Consumo":
                //    valorCuotaCierreFinanciero = (float?)(grupoMaximoDescuentoPorNomina.Total < ingresoFamiliarDisponibleTotal ?
                //                 grupoMaximoDescuentoPorNomina.Total : ingresoFamiliarDisponibleTotal);
                //    break;
                //case "A_Consumo":
                //    listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Cuota_Cierre_Final).SingleOrDefault().ValorAsociado = (cuota_Cierre_Final * 2);
                //    break;
                //case "C_Consumo":
                //    listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Cuota_Cierre_Final).SingleOrDefault().ValorAsociado = (cuota_Cierre_Final * 2);
                //    break;
                default:
                    listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Cuota_Cierre_Final).SingleOrDefault().ValorAsociado = cuota_Cierre_Final;
                    break;
            }                

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Cuota,
                                                enumFormCapacidadPago.Valor_Cuota_cierre_Financiero, "ValorAsociado",
                                                valorCuotaCierreFinanciero);
        }

        protected override void CalcularGrupoMaximoDescuentoPorNominaYCaja(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, ref float? factorMaximoDescuentoNomina, ref FormCapacidadPagoGrupos grupoMaximoDescuentoPorNomina, ref float? maximoDescuentoNomina, float? subtotalIngresosMenosDescuentosAsociado, ref float? factorMaximoValorPagoPorCaja, ref float? maximoValorPagoPorCaja, string tipoCapacidadPago, float? otrosIngresos, FormCapacidadPagoGrupos grupoIngresoNetoDisponible)
        {
            var subtotalIngresos = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (short)enumFormCapacidadPagoGrupos.Ingresos).FirstOrDefault().Total;
            var subtotalDescuentosLey = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (short)enumFormCapacidadPagoGrupos.Descuentos_por_ley).FirstOrDefault().Total;
            var otrasDeduccionesGrupo = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (short)enumFormCapacidadPagoGrupos.Otras_Deducciones).FirstOrDefault().ListaFormCapacidadPago.Where(y => y.IdCapacidadPago == (int)enumFormCapacidadPago.Otras_deducciones).FirstOrDefault();
            float? otrasDeducciones = 0;
            if (otrasDeduccionesGrupo != null)
            {
                otrasDeducciones = (otrasDeduccionesGrupo.ValorAsociado ?? 0) + (otrasDeduccionesGrupo.ValorFamiliar ?? 0);
            }


            factorMaximoDescuentoNomina = ((float?)grupoMaximoDescuentoPorNomina.ListaFormCapacidadPago
                                                                              .Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Máximo_descuento_de_nomina)
                                                                              .SingleOrDefault().Factor ?? 0);

            var ingresoDisponible = ((float?)grupoIngresoNetoDisponible.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Ingreso_Disponible)
                                                                                     .SingleOrDefault().Factor ?? 0);

            maximoDescuentoNomina = (float?)(((subtotalIngresos - subtotalDescuentosLey) * factorMaximoDescuentoNomina) - otrasDeducciones);//subtotalIngresosMenosDescuentosAsociado * factorMaximoDescuentoNomina;
            //maximoDescuentoNomina = subtotalIngresosMenosDescuentosAsociado * factorMaximoDescuentoNomina;
            


            factorMaximoValorPagoPorCaja = ((float?)grupoMaximoDescuentoPorNomina.ListaFormCapacidadPago
                                                                                 .Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Máximo_valor_pago_por_caja)
                                                                                 .SingleOrDefault().Factor ?? 0);
            //maximoValorPagoPorCaja = subtotalIngresosMenosDescuentosAsociado * factorMaximoValorPagoPorCaja;
            switch (tipoCapacidadPago) {                
                case "A_Consumo":
                    if (maximoDescuentoNomina < 0)
                    {
                        maximoValorPagoPorCaja = ingresoFamiliarDisponibleTotal;
                    }
                    else if ((ingresoFamiliarDisponibleTotal - maximoDescuentoNomina) < 0)
                    {
                        maximoValorPagoPorCaja = 0;
                    }
                    else {
                        maximoValorPagoPorCaja = (ingresoFamiliarDisponibleTotal - maximoDescuentoNomina);
                    }                    
                    break;
                case "B_Consumo":
                    // Cambio solicitado por Julian Patiño
                    // listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Maximo_Descuento_por_Nomina_y_Caja).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Máximo_valor_pago_por_caja).SingleOrDefault().Factor = 0.5;
                    //maximoValorPagoPorCaja = (float?)((((subtotalIngresos - subtotalDescuentosLey) / 2) + otrosIngresos) * factorMaximoValorPagoPorCaja); //(float?)((otrosIngresosAsociado + maximoDescuentoNomina) * 0.5);
                    // Cambio de capacidades de pago 2019-03-03
                    if (maximoDescuentoNomina < 0)
                    {
                        maximoValorPagoPorCaja = ingresoFamiliarDisponibleTotal;
                    }
                    else if ((ingresoFamiliarDisponibleTotal - maximoDescuentoNomina) < 0)
                    {
                        maximoValorPagoPorCaja = 0;
                    }
                    else
                    {
                        maximoValorPagoPorCaja = (ingresoFamiliarDisponibleTotal - maximoDescuentoNomina);
                    }                    
                    break;
                case "E_Consumo":
                    maximoValorPagoPorCaja = ingresoFamiliarDisponibleTotal;
                    maximoDescuentoNomina = 0;
                    break;
                //case "C_Consumo":
                //    maximoValorPagoPorCaja = 0;
                //    break;
                //case "D_Consumo":
                //    maximoValorPagoPorCaja = 0;
                //    break;       
                default:
                    if (maximoDescuentoNomina < 0)
                    {
                        maximoValorPagoPorCaja = ingresoFamiliarDisponibleTotal;
                    }
                    else if ((ingresoFamiliarDisponibleTotal - maximoDescuentoNomina) < 0)
                    {
                        maximoValorPagoPorCaja = 0;
                    }
                    else
                    {
                        maximoValorPagoPorCaja = (ingresoFamiliarDisponibleTotal - maximoDescuentoNomina);
                    }
                    break;
            }
           
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Maximo_Descuento_por_Nomina_y_Caja,
                                                enumFormCapacidadPago.Máximo_valor_pago_por_caja, "ValorAsociado",
                                                maximoValorPagoPorCaja);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Maximo_Descuento_por_Nomina_y_Caja,
                                                enumFormCapacidadPago.Máximo_descuento_de_nomina, "ValorAsociado",
                                                maximoDescuentoNomina);

            grupoMaximoDescuentoPorNomina.Total = maximoDescuentoNomina + maximoValorPagoPorCaja;
        }
        protected override void CalcularGrupoIngresoNetoDisponible(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, float? totalIngresosBrutosAsociado, float? totalGastosAsociado, float? totalIngresosBrutosFamiliar, float? totalGastosFamiliar, ref float? ingresoFamiliarDisponibleTotal, ref float? factorIngresoNetoVivienda, ref FormCapacidadPagoGrupos grupoIngresoNetoDisponible, ref float? ingresoNetoViviendaAsociado, ref float? ingresoNetoViviendaFamiliar, float? subtotalIngresos,
                                                                    float? otrosIngresos, string tipoCapacidadPago)
        {               
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible,
                                               enumFormCapacidadPago.Ingreso_Disponible, "ValorAsociado",
                                               (totalIngresosBrutosAsociado - totalGastosAsociado));

            ingresoFamiliarDisponibleTotal = ((totalIngresosBrutosAsociado - totalGastosAsociado) + (totalIngresosBrutosFamiliar - totalGastosFamiliar));

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible,
                                                enumFormCapacidadPago.Ingreso_Familiar_Disponible_Total, "ValorAsociado",
                                                ingresoFamiliarDisponibleTotal);
            switch (tipoCapacidadPago) {
                case "A_Vivienda":
                    AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible,
                                                enumFormCapacidadPago.Factor_ingreso_Disponible__Ingreso_Total, "ValorAsociado",
                                                ((((totalIngresosBrutosAsociado - totalGastosAsociado) + (totalIngresosBrutosFamiliar - totalGastosFamiliar)) / (totalIngresosBrutosAsociado + totalIngresosBrutosFamiliar))) * 100);
                    break;
                default:
                    AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible,
                                                enumFormCapacidadPago.Factor_ingreso_Disponible__Ingreso_Total, "ValorAsociado",
                                                ((((totalIngresosBrutosAsociado - totalGastosAsociado) + (totalIngresosBrutosFamiliar - totalGastosFamiliar)) / (totalIngresosBrutosAsociado + totalIngresosBrutosFamiliar))) * 100);
                    break;
            }
                
            

            /// Linea de consumo no necesita mostrar total
            listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible).SingleOrDefault().MostrarTotal = false;
        }

        protected override void CalcularGrupoGastos(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, ref FormCapacidadPagoGrupos grupoGastos, float? gastosFamiliaresAsociado, float? gastosFamiliaresFamiliar,ref float? totalGastosAsociado, float? totalGastosFamiliar, float? gastosFinancierosAsociado, string tipoCapacidadPago, float? gastosFinancierosFamiliar, float? smmlv)
        {
            string indicadorGrupo = listaFormCapacidadPagoGrupos[5].ListaFormCapacidadPago[1].Texto;
            float? OtrosGastos;
            if (indicadorGrupo == "SMMLV")
            {

                AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos,
                                                enumFormCapacidadPago.smmlv, "ValorAsociado",
                                                smmlv);
                OtrosGastos = smmlv;
            }
            else
            {

                AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos,
                                                    enumFormCapacidadPago.Gastos_Familiares, "ValorAsociado",
                                                    gastosFamiliaresAsociado);

                AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos,
                                                    enumFormCapacidadPago.Gastos_Familiares, "ValorFamiliar",
                                                    gastosFamiliaresFamiliar);
                OtrosGastos = gastosFamiliaresFamiliar;
            }

            grupoGastos.Total = totalGastosAsociado + totalGastosFamiliar;

            switch (tipoCapacidadPago)
            {
                case "A_Consumo":
                    totalGastosAsociado = (OtrosGastos + gastosFinancierosAsociado)/2;
                    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos, totalGastosAsociado);
                    break;
                //case "C_Consumo":
                //    totalGastosAsociado = gastosFamiliaresAsociado + (gastosFinancierosAsociado / 2);
                //    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos, gastosFamiliaresAsociado + (gastosFinancierosAsociado / 2));
                //    break;
                default:
                    totalGastosAsociado = OtrosGastos + (gastosFinancierosAsociado);
                    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos, smmlv + (gastosFinancierosAsociado));
                    break;
            }            
        }

        protected override void CalcularGrupoOtrosIngresos(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, float? totalIngresosBrutosAsociado, float? totalIngresosBrutosFamiliar)
        {
            AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Otros_Ingresos, totalIngresosBrutosAsociado + totalIngresosBrutosFamiliar);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Otros_Ingresos,
                                                enumFormCapacidadPago.Total_ingresos_brutos, "ValorAsociado",
                                                totalIngresosBrutosAsociado);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Otros_Ingresos,
                                                enumFormCapacidadPago.Total_ingresos_brutos, "ValorFamiliar",
                                                totalIngresosBrutosFamiliar);
        }

        protected override void CalcularGrupoOtrasDeducciones(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, float? subtotalIngresosMenosDescuentosAsociado, float? subtotalIngresosMenosDescuentosFamiliar)
        {
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Otras_Deducciones,
                                          enumFormCapacidadPago.Subtotal_Ingresos_menos_descuentos, "ValorAsociado",
                                          subtotalIngresosMenosDescuentosAsociado);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Otras_Deducciones,
                                                enumFormCapacidadPago.Subtotal_Ingresos_menos_descuentos, "ValorFamiliar",
                                                subtotalIngresosMenosDescuentosFamiliar);

            AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos,
                                   enumFormCapacidadPagoGrupos.Otras_Deducciones,
                                   subtotalIngresosMenosDescuentosAsociado + subtotalIngresosMenosDescuentosFamiliar);
        }
        protected void CalcularGrupoSolvencia(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, FormCapacidadPagoGrupos grupoSolvencia)
        {
            float? activos = grupoSolvencia.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Activo).FirstOrDefault().ValorAsociado;
            float? pasivos = grupoSolvencia.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Pasivo).FirstOrDefault().ValorAsociado;
            float? patrimonio = activos - pasivos; //grupoSolvencia.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Patrimonio).FirstOrDefault().ValorAsociado;

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Solvencia,
                                                enumFormCapacidadPago.Patrimonio, "ValorAsociado",
                                                ((patrimonio == 0 ? 1 : patrimonio)));

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Solvencia,
                                                enumFormCapacidadPago.Solvencia, "ValorAsociado",
                                                (activos / (patrimonio == 0 ? 1 : patrimonio)));
        }
    }

}
