﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.Creditos;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.Globales;

namespace Cavipetrol.SICSES.BLL.Controllers.SolicitudCredito
{
    public class CapacidadPagoCalculoCreditoLineaVivienda : CapacidadPagoCalculoCreditoAbstracta
    {
        private readonly IConsultasApoyoUnidadTrabajo _consultasApoyoUnidadTrabajo;
        public CapacidadPagoCalculoCreditoLineaVivienda()
        {
            _consultasApoyoUnidadTrabajo = new ConsultasApoyoUnidadTrabajo();
        }
        public override void Calcular(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos,
                                      ref List<Amortizacion> tablaAmortizacion,
                                      int idTipoCreditoTipoRolRelacion,
                                      List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion,
                                      double valorTotalInmueble ,
                                      short idTipoPoliza,
                                      double valorNegociado,
                                      double tasaMensualSeguroVida,
                                      double PorcenExtraPrima,
                                      int idPapelCavipetrol, 
                                      string tipoCapacidadPago,
                                      int edad,
                                      int idTipoGarantia)
        {
            LimpiarListaFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos);
            #region Variables
            FormCapacidadPagoGrupos grupoIngresos = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Ingresos).SingleOrDefault();
            FormCapacidadPagoGrupos grupoDescuentosLey = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Descuentos_por_ley).SingleOrDefault();
            FormCapacidadPagoGrupos grupoOtrasDeducciones = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Otras_Deducciones).SingleOrDefault();
            FormCapacidadPagoGrupos grupoOtrosIngresos = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Otros_Ingresos).SingleOrDefault();
            FormCapacidadPagoGrupos grupoGastos = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Gastos).SingleOrDefault();
            FormCapacidadPagoGrupos grupoIngresoNetoDisponible = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible).SingleOrDefault();
            FormCapacidadPagoGrupos grupoMaximoDescuentoPorNomina = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Maximo_Descuento_por_Nomina_y_Caja).SingleOrDefault();
            FormCapacidadPagoGrupos grupoValorCreditoVivenda = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Credito).SingleOrDefault();
            FormCapacidadPagoGrupos grupoSolvencia = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Solvencia).SingleOrDefault();
            float? ValorSeguros = 0;
            #endregion
            #region Grupo Ingresos             
            CalcularValorTotalGrupo(grupoIngresos, ref totalIngresosAsociado, ref totalIngresosFamiliar);
            AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingresos, (totalIngresosAsociado ?? 0) + (totalIngresosFamiliar ?? 0));
            #endregion
            #region Grupo Descuentos de Ley                        
            CalcularValorTotalGrupo(grupoDescuentosLey, ref totalDescuentosLeyAsociado, ref totalDescuentosLeyFamiliar);
            AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Descuentos_por_ley, (totalDescuentosLeyAsociado ?? 0) + (totalDescuentosLeyFamiliar ?? 0));
            #endregion
            #region Grupo Otras deducciones                        
            CalcularValorTotalGrupo(grupoOtrasDeducciones, ref totalOtrasDeduccionesAsociado, ref totalOtrasDeduccionesFamiliar);
            #endregion
            #region Grupo Ingresos decuentos            
            subtotalIngresosMenosDescuentosAsociado = (float?)(totalIngresosAsociado - (totalDescuentosLeyAsociado + totalOtrasDeduccionesAsociado));
            subtotalIngresosMenosDescuentosFamiliar = (float?)(totalIngresosFamiliar - (totalDescuentosLeyFamiliar));
            #endregion
            #region Obtener valores Grupo Otros ingresos                        
            ObtenerValorGrupoOtrosIngresos(grupoOtrosIngresos, ref otrosIngresosAsociado, ref otrosIngresosFamiliar, tipoCapacidadPago);
            #endregion  
            #region Grupo Otras deducciones                    
            var factor = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Otros_Ingresos).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Total_ingresos_brutos).SingleOrDefault().Factor;            
            switch (tipoCapacidadPago) {
                case "A_Vivienda":
                    totalIngresosBrutosAsociado = subtotalIngresosMenosDescuentosAsociado + ((otrosIngresosAsociado / 2) * (float?)factor);
                    totalIngresosBrutosFamiliar = subtotalIngresosMenosDescuentosFamiliar + ((otrosIngresosFamiliar / 2) * (float?)factor);
                    break;
                case "B_Vivienda":
                    totalIngresosBrutosAsociado = subtotalIngresosMenosDescuentosAsociado + (otrosIngresosAsociado * (float?)factor);
                    totalIngresosBrutosFamiliar = subtotalIngresosMenosDescuentosFamiliar + (otrosIngresosFamiliar * (float?)factor);
                    break;

                case "E_Vivienda":
                    totalIngresosBrutosAsociado = subtotalIngresosMenosDescuentosAsociado + (otrosIngresosAsociado * (float?)factor);
                    totalIngresosBrutosFamiliar = subtotalIngresosMenosDescuentosFamiliar + (otrosIngresosFamiliar * (float?)factor);
                    break;
                default:
                    totalIngresosBrutosAsociado = subtotalIngresosMenosDescuentosAsociado + ((otrosIngresosAsociado / 2) * (float?)factor);
                    totalIngresosBrutosFamiliar = subtotalIngresosMenosDescuentosFamiliar + ((otrosIngresosFamiliar / 2) * (float?)factor);
                    break;
            }            
            CalcularGrupoOtrasDeducciones(ref listaFormCapacidadPagoGrupos, subtotalIngresosMenosDescuentosAsociado, subtotalIngresosMenosDescuentosFamiliar);
            #endregion
            #region Otros ingresos            
            CalcularGrupoOtrosIngresos(ref listaFormCapacidadPagoGrupos, totalIngresosBrutosAsociado, totalIngresosBrutosFamiliar);
            #endregion
            #region Grupo Gastos
            string indicadorGrupo = listaFormCapacidadPagoGrupos[5].ListaFormCapacidadPago[1].Texto;
            if (indicadorGrupo != "SMMLV")
            {
                factorGastosFamiliares = (float?)0.7;//((float?)grupoGastos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Gastos_Familiares).SingleOrDefault().Factor ?? 0);
                gastosFamiliaresAsociado = (totalIngresosAsociado * (factorGastosFamiliares));
                gastosFamiliaresFamiliar = (totalIngresosFamiliar * (factorGastosFamiliares));
                gastosFinancierosFamiliar = (grupoGastos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Gastos_financieros).SingleOrDefault().ValorFamiliar ?? 0);
            }
            gastosFinancierosAsociado = (grupoGastos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Gastos_financieros).SingleOrDefault().ValorAsociado ?? 0);
            totalGastosFamiliar = gastosFamiliaresFamiliar + gastosFinancierosFamiliar;
            Parametros parametros = new Parametros();
            parametros = _consultasApoyoUnidadTrabajo.ObtenerParametros("SMMLV").LastOrDefault();
            float smmlv = 1;
            float.TryParse(parametros.Valor, out smmlv);

            if(indicadorGrupo == "SMMLV")
                totalGastosAsociado = smmlv + gastosFinancierosAsociado;
            else
                totalGastosAsociado = gastosFamiliaresAsociado + gastosFinancierosAsociado;

                CalcularGrupoGastos(ref listaFormCapacidadPagoGrupos,
                                ref grupoGastos,
                                gastosFamiliaresAsociado,
                                gastosFamiliaresFamiliar,
                                ref totalGastosAsociado,
                                totalGastosFamiliar, 
                                gastosFinancierosAsociado,
                                tipoCapacidadPago,
                                gastosFinancierosFamiliar,
                                smmlv);
            #endregion
            #region Grupo Ingreso neto disponible         
            
            CalcularGrupoIngresoNetoDisponible(ref listaFormCapacidadPagoGrupos,
                                               totalIngresosBrutosAsociado,
                                               totalGastosAsociado,
                                               totalIngresosBrutosFamiliar,
                                               totalGastosFamiliar,
                                               ref ingresoFamiliarDisponibleTotal,
                                               ref factorIngresoNetoVivienda,
                                               ref grupoIngresoNetoDisponible,
                                               ref ingresoNetoViviendaAsociado,
                                               ref ingresoNetoViviendaFamiliar,
                                               (float?)(grupoIngresos.Total ?? 0),
                                               (float?)(grupoOtrosIngresos.ListaFormCapacidadPago[0].ValorAsociado ?? 0) + (grupoOtrosIngresos.ListaFormCapacidadPago[0].ValorFamiliar ?? 0),
                                               tipoCapacidadPago);

            MostrarGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible, true);
            #endregion
            #region Grupo Maximo descuento por nomina                  
            CalcularGrupoMaximoDescuentoPorNominaYCaja(ref listaFormCapacidadPagoGrupos,
                                                       ref factorMaximoDescuentoNomina,
                                                       ref grupoMaximoDescuentoPorNomina,
                                                       ref maximoDescuentoNomina,
                                                       subtotalIngresosMenosDescuentosAsociado,
                                                       ref factorMaximoValorPagoPorCaja,
                                                       ref maximoValorPagoPorCaja,
                                                       tipoCapacidadPago,
                                                       (otrosIngresosAsociado + otrosIngresosFamiliar),
                                                       grupoIngresoNetoDisponible);
            MostrarGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Maximo_Descuento_por_Nomina_y_Caja, true);
            #endregion
            #region Grupo Valor cuota            
            CalcularGrupoValorCuota(ref listaFormCapacidadPagoGrupos,
                               ref valorCuotaCierreFinanciero,
                               grupoMaximoDescuentoPorNomina,
                               //ingresoNetoViviendaFamiliar);
                               (float?)grupoIngresoNetoDisponible.Total,
                               tipoCapacidadPago, listTipoCreditoTipoSeguroRelacion, valorNegociado, (enumPapelCavipetrol)idPapelCavipetrol, edad, valorTotalInmueble, tasaMensualSeguroVida, PorcenExtraPrima);
            
            MostrarGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Cuota, true);
            #endregion
            
            #region Grupo Valor credito            
            CalcularGrupoValorCredito(ref listaFormCapacidadPagoGrupos,
                                      anhosValorCredito,
                                      ref tasaCredito_E_A,
                                      ref tasaNominal,
                                      ref plazoMeses,
                                      grupoValorCreditoVivenda,
                                      valorCuotaCierreFinanciero,
                                      ref totalValorCreditoVivienda,
                                      factorValorMaximoViviendaNoVis,
                                      factorValorMaximoViviendaVis,
                                      idTipoCreditoTipoRolRelacion,
                                      idPapelCavipetrol,
                                      tipoCapacidadPago, 
                                      ValorSeguros);
            #endregion
            #region Grupo Endeudamiento financiero 
            CalcularGrupoEndeudamientoFinanciero(ref listaFormCapacidadPagoGrupos,
                                                 totalIngresosBrutosAsociado,
                                                 totalIngresosBrutosFamiliar,
                                                 valorCuotaCierreFinanciero,
                                                 gastosFinancierosAsociado,
                                                 tipoCapacidadPago);
            MostrarGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero, true);
            #endregion
            #region Grupo solvencia
            CalcularGrupoSolvencia(ref listaFormCapacidadPagoGrupos, grupoSolvencia);
            #endregion
            //int idPapelCavipetrol = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposRolRelacion(idTipoCreditoTipoRolRelacion: idTipoCreditoTipoRolRelacion).FirstOrDefault().IdPapelCavipetrol;
            GenerarTablaAmortizacion(plazoMeses, (double)tasaNominal / 100, (double)tasaCredito_E_A, (double)totalValorCreditoVivienda, ref tablaAmortizacion, listTipoCreditoTipoSeguroRelacion, valorTotalInmueble, idTipoPoliza, tasaMensualSeguroVida, PorcenExtraPrima, (enumPapelCavipetrol)idPapelCavipetrol, 0, edad, 0, ref ValorSeguros, idTipoGarantia);
            totalValorCreditoVivienda = totalValorCreditoVivienda - ValorSeguros;
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Valor_Credito_Vivienda, "ValorAsociado",
                                                totalValorCreditoVivienda);
            
        }

        private void ObtenerValorGrupoOtrosIngresos(FormCapacidadPagoGrupos grupoOtrosIngresos, ref float? otrosIngresosAsociado, ref float? otrosIngresosFamiliar, string tipoCapacidadPago)
        {
            float? factor = (float?)(grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().Factor ?? 1);
            switch (tipoCapacidadPago) {
                //case "C_Vivienda":
                //    var ingreso = ((grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().ValorAsociado) / 2);
                //    otrosIngresosAsociado = ingreso ?? 0 * factor;
                //    otrosIngresosFamiliar = (grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().ValorFamiliar ?? 0) * factor;
                //    break;
                default:                    
                    otrosIngresosAsociado = (grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().ValorAsociado ?? 0) * factor;
                    otrosIngresosFamiliar = (grupoOtrosIngresos.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().ValorFamiliar ?? 0) * factor;
                    break;
            }            
        }
        protected override void CalcularGrupoEndeudamientoFinanciero(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, float? totalIngresosBrutosAsociado, float? totalIngresosBrutosFamiliar, float? valorCuotaCierreFinanciero, float? gastosFinancierosAsociado, string tipoCapacidadPago)
        {
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                                enumFormCapacidadPago.Total_Ingresos, "ValorAsociado",
                                                totalIngresosBrutosAsociado);            
            switch (tipoCapacidadPago)
            {
                case "E_Vivienda":
                    AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                            enumFormCapacidadPago.Total_Ingresos, "ValorFamiliar",
                            totalIngresosBrutosFamiliar);

                    AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                             enumFormCapacidadPago.Total_Deuda_Financiera, "ValorAsociado",
                                             Math.Abs((float)valorCuotaCierreFinanciero) + gastosFinancierosAsociado);

                    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                  (((Math.Abs((float)valorCuotaCierreFinanciero) + gastosFinancierosAsociado) / totalIngresosBrutosAsociado) * 100));
                    break;
                case "B_Vivienda":

                    AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                                enumFormCapacidadPago.Total_Ingresos, "ValorFamiliar",
                                                0);
                    AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                             enumFormCapacidadPago.Total_Deuda_Financiera, "ValorAsociado",
                                             Math.Abs((float)valorCuotaCierreFinanciero) + gastosFinancierosAsociado);

                    
                    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                  (((Math.Abs((float)valorCuotaCierreFinanciero) + gastosFinancierosAsociado) / totalIngresosBrutosAsociado) * 100));
                    break;
                default:
                    AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                            enumFormCapacidadPago.Total_Ingresos, "ValorFamiliar",
                            totalIngresosBrutosFamiliar);

                    AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                                 enumFormCapacidadPago.Total_Deuda_Financiera, "ValorAsociado",
                                                 valorCuotaCierreFinanciero + gastosFinancierosAsociado);

                    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Endeudamiento_Financiero,
                                            (((valorCuotaCierreFinanciero + gastosFinancierosAsociado) / totalIngresosBrutosAsociado) * 100));
                    break;
            }            
        }

        protected override void CalcularGrupoGastos(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, ref FormCapacidadPagoGrupos grupoGastos, float? gastosFamiliaresAsociado, float? gastosFamiliaresFamiliar,ref float? totalGastosAsociado, float? totalGastosFamiliar, float? gastosFinancierosAsociado, string tipoCapacidadPago, float? gastosFinancierosFamiliar, float? smmlv)
        {
            string indicadorGrupo = listaFormCapacidadPagoGrupos[5].ListaFormCapacidadPago[1].Texto;
            float? OtrosGastos;
            if (indicadorGrupo == "SMMLV")
            {
                AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos,
                                                  enumFormCapacidadPago.smmlv, "ValorAsociado",
                                                  smmlv);
                OtrosGastos = smmlv;
            }
            else
            {

                AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos,
                                                   enumFormCapacidadPago.Gastos_Familiares, "ValorAsociado",
                                                   gastosFamiliaresAsociado);

                AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos,
                                                            enumFormCapacidadPago.Gastos_Familiares, "ValorFamiliar",
                                                            gastosFamiliaresFamiliar);

                OtrosGastos = gastosFamiliaresFamiliar;
            }
            switch (tipoCapacidadPago) {
                case "E_Vivienda":
                    totalGastosAsociado = gastosFinancierosAsociado + OtrosGastos;
                    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos, totalGastosAsociado);
                    break;
                //case "C_Vivienda":
                //    totalGastosAsociado = gastosFamiliaresAsociado + (gastosFinancierosAsociado / 2);
                //    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos, gastosFamiliaresAsociado + (gastosFinancierosAsociado / 2));
                //    break;
                //case "D_Vivienda":
                //    totalGastosAsociado = gastosFinancierosAsociado + gastosFamiliaresAsociado;
                //    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos, totalGastosAsociado);
                //    break;
                case "B_Vivienda":
                    totalGastosAsociado = gastosFinancierosAsociado + OtrosGastos;
                    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos, totalGastosAsociado);
                    break;
                default:
                    //totalGastosAsociado = (float?)grupoGastos.Total;
                    //AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos, (float?)grupoGastos.Total);
                    totalGastosAsociado = (gastosFinancierosAsociado + OtrosGastos) / 2;
                    AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Gastos, totalGastosAsociado);
                    break;
            }            
        }

        protected override void CalcularGrupoIngresoNetoDisponible(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, 
                                                                    float? totalIngresosBrutosAsociado, 
                                                                    float? totalGastosAsociado, 
                                                                    float? totalIngresosBrutosFamiliar, 
                                                                    float? totalGastosFamiliar, 
                                                                    ref float? ingresoFamiliarDisponibleTotal, 
                                                                    ref float? factorIngresoNetoVivienda, 
                                                                    ref FormCapacidadPagoGrupos grupoIngresoNetoDisponible, 
                                                                    ref float? ingresoNetoViviendaAsociado, 
                                                                    ref float? ingresoNetoViviendaFamiliar,
                                                                    float? subtotalIngresos,                                                                    
                                                                    float? otrosIngresos,
                                                                    string tipoCapacidadPago)
        {
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible,
                                                enumFormCapacidadPago.Ingreso_Disponible, "ValorAsociado",
                                                (totalIngresosBrutosAsociado - totalGastosAsociado));

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible,
                                                enumFormCapacidadPago.Ingreso_Disponible, "ValorFamiliar",
                                                (totalIngresosBrutosFamiliar - totalGastosFamiliar));
            
            var ingresoDisponible = (totalIngresosBrutosAsociado - totalGastosAsociado);
            ingresoFamiliarDisponibleTotal = (totalIngresosBrutosAsociado - totalGastosAsociado) + (totalIngresosBrutosFamiliar - totalGastosFamiliar);
            //ingresoFamiliarDisponibleTotal = (totalIngresosBrutosAsociado - totalGastosAsociado);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible,
                                                enumFormCapacidadPago.Ingreso_Familiar_Disponible_Total, "ValorFamiliar",
                                                ingresoFamiliarDisponibleTotal);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible,
                                                enumFormCapacidadPago.Factor_ingreso_Disponible__Ingreso_Total, "ValorFamiliar",
                                                ((((totalIngresosBrutosAsociado - totalGastosAsociado) + (totalIngresosBrutosFamiliar - totalGastosFamiliar)) / (totalIngresosBrutosAsociado + totalIngresosBrutosFamiliar))) * 100);

            factorIngresoNetoVivienda = ((float?)grupoIngresoNetoDisponible.ListaFormCapacidadPago
                                                                                 .Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Ingreso_Neto_Vivienda)
                                                                                 .SingleOrDefault().Factor ?? 0);

            

            
            //ingresoNetoViviendaAsociado = ((totalIngresosBrutosAsociado + totalIngresosBrutosFamiliar) * factorIngresoNetoVivienda);


            switch (tipoCapacidadPago) {
                case "A_Vivienda":                    
                    ingresoNetoViviendaAsociado = (subtotalIngresos + ((otrosIngresos / 2) * (float?)0.7)) * factorIngresoNetoVivienda;
                    ingresoNetoViviendaFamiliar = (totalIngresosBrutosFamiliar * factorIngresoNetoVivienda);
                    break;
                case "B_Vivienda":
                    ingresoNetoViviendaAsociado = (subtotalIngresos + (otrosIngresos * (float?)0.7)) * factorIngresoNetoVivienda;
                    ingresoNetoViviendaFamiliar = (totalIngresosBrutosFamiliar * factorIngresoNetoVivienda);
                    break;
                default:
                    ingresoNetoViviendaAsociado = (subtotalIngresos + ((otrosIngresos / 2) * (float?)0.7)) * factorIngresoNetoVivienda;
                    break;
            }
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible,
                                                enumFormCapacidadPago.Ingreso_Neto_Vivienda, "ValorAsociado",
                                                ingresoNetoViviendaAsociado);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible,
                                                enumFormCapacidadPago.Ingreso_Neto_Vivienda, "ValorFamiliar",
                                                ingresoNetoViviendaFamiliar);

            ingresoNetoViviendaFamiliar = ingresoDisponible < ingresoNetoViviendaAsociado ? ingresoDisponible : ingresoNetoViviendaAsociado;
            //ingresoNetoViviendaFamiliar = ((subtotalIngresos + otrosIngresos) * factorIngresoNetoVivienda);

            //grupoIngresoNetoDisponible.Total = ingresoNetoViviendaFamiliar <= ingresoFamiliarDisponibleTotal ? ingresoNetoViviendaFamiliar : ingresoFamiliarDisponibleTotal;
            grupoIngresoNetoDisponible.Total = ingresoNetoViviendaFamiliar <= (totalIngresosBrutosAsociado - totalGastosAsociado) ? ingresoNetoViviendaFamiliar : (totalIngresosBrutosAsociado - totalGastosAsociado);
            grupoIngresoNetoDisponible.Total = tipoCapacidadPago == "A_Vivienda" ? grupoIngresoNetoDisponible.Total * 2  : grupoIngresoNetoDisponible.Total;
            AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible, (float?)grupoIngresoNetoDisponible.Total);

        }

        protected override void CalcularGrupoMaximoDescuentoPorNominaYCaja(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, ref float? factorMaximoDescuentoNomina, ref FormCapacidadPagoGrupos grupoMaximoDescuentoPorNomina, ref float? maximoDescuentoNomina, float? subtotalIngresosMenosDescuentosAsociado, ref float? factorMaximoValorPagoPorCaja, ref float? maximoValorPagoPorCaja, string tipoCapacidadPago, float? otrosIngresos, FormCapacidadPagoGrupos grupoIngresoNetoDisponible)
        {

            var subtotalIngresos = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (short)enumFormCapacidadPagoGrupos.Ingresos).FirstOrDefault().Total;
            var subtotalDescuentosLey = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (short)enumFormCapacidadPagoGrupos.Descuentos_por_ley).FirstOrDefault().Total - totalDescuentosLeyFamiliar;
            var otrasDeduccionesGrupo = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (short)enumFormCapacidadPagoGrupos.Otras_Deducciones).FirstOrDefault().ListaFormCapacidadPago.Where(y => y.IdCapacidadPago == (int)enumFormCapacidadPago.Otras_deducciones).FirstOrDefault();
            var subtotalIngresosAsociado = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (short)enumFormCapacidadPagoGrupos.Ingresos).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Salario_Básico).SingleOrDefault().ValorAsociado;            
            var horasExtras = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (short)enumFormCapacidadPagoGrupos.Ingresos).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Horas_extras).SingleOrDefault();
            if (horasExtras != null) {
                subtotalIngresosAsociado = horasExtras.ValorAsociado + subtotalIngresosAsociado;
            }
            
            subtotalIngresosAsociado = subtotalIngresosAsociado + listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (short)enumFormCapacidadPagoGrupos.Ingresos).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Subsidio_de_Arriendo_).SingleOrDefault().ValorAsociado;
            float? otrasDeducciones = 0;
            if (otrasDeduccionesGrupo != null) {
                otrasDeducciones = (otrasDeduccionesGrupo.ValorAsociado ?? 0); //+ (otrasDeduccionesGrupo.ValorFamiliar ?? 0);
            }
           

            factorMaximoDescuentoNomina = ((float?)grupoMaximoDescuentoPorNomina.ListaFormCapacidadPago
                                                                               .Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Máximo_descuento_de_nomina)
                                                                               .SingleOrDefault().Factor ?? 0);
            maximoDescuentoNomina = (float?)(((subtotalIngresosAsociado - subtotalDescuentosLey) * factorMaximoDescuentoNomina) - otrasDeducciones);//subtotalIngresosMenosDescuentosAsociado * factorMaximoDescuentoNomina;
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Maximo_Descuento_por_Nomina_y_Caja,
                                                enumFormCapacidadPago.Máximo_descuento_de_nomina, "ValorAsociado",
                                                maximoDescuentoNomina);


            factorMaximoValorPagoPorCaja = ((float?)grupoMaximoDescuentoPorNomina.ListaFormCapacidadPago
                                                                                 .Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Máximo_valor_pago_por_caja)
                                                                                 .SingleOrDefault().Factor ?? 0);

            var ingresoDisponible = grupoIngresoNetoDisponible.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Ingreso_Disponible).SingleOrDefault().ValorAsociado;
            var ingresoDisponibleFamiliar = grupoIngresoNetoDisponible.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Ingreso_Disponible).SingleOrDefault().ValorFamiliar;
            var otrosIngresosAsociado = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Otros_Ingresos).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Otros_Ingresos).SingleOrDefault().ValorAsociado;
            var ingresoNetoDisponible = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible).SingleOrDefault().Total;

            switch (tipoCapacidadPago) {
                case "A_Vivienda":
                    if (maximoDescuentoNomina < 0) {
                        maximoValorPagoPorCaja = (float?)ingresoNetoDisponible;
                    }
                    else if (ingresoNetoDisponible - maximoDescuentoNomina < 0)
                    {
                        maximoValorPagoPorCaja = 0;
                    }
                    else {
                        maximoValorPagoPorCaja = (float?)(ingresoNetoDisponible - maximoDescuentoNomina);
                    }                                        
                    break;
                case "B_Vivienda":
                    if (maximoDescuentoNomina < 0)
                    {
                        maximoValorPagoPorCaja = (float?)ingresoNetoDisponible;
                    }
                    else if (ingresoNetoDisponible - maximoDescuentoNomina < 0)
                    {
                        maximoValorPagoPorCaja = 0;
                    }
                    else
                    {
                        maximoValorPagoPorCaja = (float?)(ingresoNetoDisponible - maximoDescuentoNomina);
                    }
                    break;
                //case "D_Vivienda":
                //    maximoValorPagoPorCaja = 0;
                //    break;
                //case "C_Vivienda":
                //    maximoValorPagoPorCaja = 0;
                //    break;                
                //case "B_Vivienda":                    
                //    //maximoValorPagoPorCaja = (maximoDescuentoNomina + otrosIngresos + ingresoDisponible) * factorMaximoValorPagoPorCaja;
                //    maximoValorPagoPorCaja = (float?)((((subtotalIngresos - subtotalDescuentosLey) / 2) + otrosIngresos) * factorMaximoValorPagoPorCaja); //(maximoDescuentoNomina + otrosIngresosAsociado) * factorMaximoValorPagoPorCaja;
                //    break;
                default:

                    if (maximoDescuentoNomina < 0)
                    {
                        maximoValorPagoPorCaja = (float?)ingresoNetoDisponible;
                    }
                    else if (ingresoNetoDisponible - maximoDescuentoNomina < 0)
                    {
                        maximoValorPagoPorCaja = 0;
                    }
                    else
                    {
                        maximoValorPagoPorCaja = (float?)(ingresoNetoDisponible - maximoDescuentoNomina);
                    }
                    break;
                    //maximoValorPagoPorCaja = subtotalIngresosMenosDescuentosAsociado * factorMaximoValorPagoPorCaja;
                    break;
            }            
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Maximo_Descuento_por_Nomina_y_Caja,
                                                enumFormCapacidadPago.Máximo_valor_pago_por_caja, "ValorAsociado",
                                                maximoValorPagoPorCaja);

            grupoMaximoDescuentoPorNomina.Total = maximoDescuentoNomina + maximoValorPagoPorCaja;
        }

        protected override void CalcularGrupoOtrasDeducciones(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, float? subtotalIngresosMenosDescuentosAsociado, float? subtotalIngresosMenosDescuentosFamiliar)
        {
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Otras_Deducciones,
                                                enumFormCapacidadPago.Subtotal_Ingresos_menos_descuentos, "ValorAsociado",
                                                subtotalIngresosMenosDescuentosAsociado);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Otras_Deducciones,
                                                enumFormCapacidadPago.Subtotal_Ingresos_menos_descuentos, "ValorFamiliar",
                                                subtotalIngresosMenosDescuentosFamiliar);

            AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos,
                                   enumFormCapacidadPagoGrupos.Otras_Deducciones,
                                   subtotalIngresosMenosDescuentosAsociado + subtotalIngresosMenosDescuentosFamiliar);
        }

        protected override void CalcularGrupoOtrosIngresos(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, float? totalIngresosBrutosAsociado, float? totalIngresosBrutosFamiliar)
        {
            

            AsignarValorTotalGrupo(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Otros_Ingresos, totalIngresosBrutosAsociado + totalIngresosBrutosFamiliar);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Otros_Ingresos,
                                                enumFormCapacidadPago.Total_ingresos_brutos, "ValorAsociado",
                                                totalIngresosBrutosAsociado);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Otros_Ingresos,
                                                enumFormCapacidadPago.Total_ingresos_brutos, "ValorFamiliar",
                                                totalIngresosBrutosFamiliar);
        }

        protected override void CalcularGrupoValorCredito(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, short anhosValorCredito, ref float? tasaCredito_E_A, ref float? tasaNominal, ref short plazoMeses, FormCapacidadPagoGrupos grupoValorCreditoVivenda, float? valorCuotaCierreFinanciero, ref float? totalValorCreditoVivienda, float? factorValorMaximoViviendaNoVis, float? factorValorMaximoViviendaVis, int idTipoCreditoTipoRolRelacion, int idPapelCavipetrol, string tipoCapacidadPago, float? ValorSeguros)
        {
            //anhosValorCredito = (short)(grupoValorCreditoVivenda.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Plazo_anos_valorCredito).SingleOrDefault().ValorAsociado ?? 0);
            mesesValorCredito = (short)(grupoValorCreditoVivenda.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Plazo_meses_valorCredito).SingleOrDefault().ValorAsociado ?? 0);
            //AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
            //                                    enumFormCapacidadPago.Plazo_meses_valorCredito, "ValorAsociado",
            //                                    anhosValorCredito * 12);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Plazo_meses_valorCredito, "ValorAsociado",
                                                mesesValorCredito);

            //TipoCredito tipoCredito = _consultasApoyoUnidadTrabajo.ObtenerTipoCredito().Where(x => x.IdTipoCredito == (int)enumTiposCredito.Vivienda).SingleOrDefault();
            TipoCreditoTipoRolRelacion tipoCreditoTipoRolRelacion = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposRolRelacion(0, idTipoCreditoTipoRolRelacion).SingleOrDefault();
            tasaCredito_E_A = (float?)(tipoCreditoTipoRolRelacion.InteresEfectivoAnual * 100);
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                              enumFormCapacidadPago.Tasa_Credito_EA_Consumo, "ValorAsociado",
                                              tasaCredito_E_A);


            tasaNominal = CalcularTasaNominal((double)(tasaCredito_E_A / 100), (double)12);
            //plazoMeses = (short)(anhosValorCredito * 12);
            plazoMeses = mesesValorCredito;
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Tasa_Credito_N_M_V, "ValorAsociado",
                                                tasaNominal);

            var valorCuotaCierreFinal = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(y => y.IdCapacidadPago == (int)enumFormCapacidadPago.Cuota_Cierre_Final).SingleOrDefault().ValorAsociado;
            //Se cambia Valor Cuota cierre financiero por totalValorCreditoVivienda a Peticion de Gerente de Riesgos 
            //switch (tipoCapacidadPago)
            //{
            //    case "C_Vivienda":
            //        totalValorCreditoVivienda = (float?)CalcularValorActualPrestamoAPartirDeTasaInteresConstante_VA((double)(tasaNominal / 100), plazoMeses, (double)valorCuotaCierreFinanciero, 0) - ValorSeguros;
            //        break;
            //    default:
            //        totalValorCreditoVivienda = (float?)CalcularValorActualPrestamoAPartirDeTasaInteresConstante_VA((double)(tasaNominal / 100), plazoMeses, (double)valorCuotaCierreFinanciero, 0) - ValorSeguros;
            //        break;
            //}
            totalValorCreditoVivienda = (float?)CalcularValorActualPrestamoAPartirDeTasaInteresConstante_VA((double)(tasaNominal / 100), plazoMeses, (double)valorCuotaCierreFinal, 0) - ValorSeguros;

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Valor_Credito_Vivienda, "ValorAsociado",
                                                totalValorCreditoVivienda);


            factorValorMaximoViviendaNoVis = ((float?)grupoValorCreditoVivenda.ListaFormCapacidadPago
                                                    .Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Valor_Maximo_Vivienda_No_VIS)
                                                    .SingleOrDefault().Factor ?? 0);
            factorValorMaximoViviendaVis = ((float?)grupoValorCreditoVivenda.ListaFormCapacidadPago
                                                    .Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Valor_Maximo_Vivienda_VIS)
                                                    .SingleOrDefault().Factor ?? 0);

            var valorNoVis = (int)(totalValorCreditoVivienda / factorValorMaximoViviendaNoVis);
            var valorVis = (int)(totalValorCreditoVivienda / factorValorMaximoViviendaVis);
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Valor_Maximo_Vivienda_No_VIS, "ValorAsociado",
                                                valorNoVis);
            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Credito,
                                                enumFormCapacidadPago.Valor_Maximo_Vivienda_VIS, "ValorAsociado",
                                                valorVis);
        }
        private double ObtenerTotalSeguros(List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion,
                                           double saldo,
                                           double? valorTotalInmueble,
                                           double tasaMensualSeguroVida,
                                           double PorcenExtraPrima,
                                           enumPapelCavipetrol papelCavipetrol,
                                           enumTipoSeguro tipoSeguro,
                                           int edad)
        {
            decimal totalSeguro = 0;
            /// Cavipetrol asume el valor de la extraprima para los siguientes papeles
            if (papelCavipetrol == enumPapelCavipetrol.Activo ||
               papelCavipetrol == enumPapelCavipetrol.Jub_Afil ||
               papelCavipetrol == enumPapelCavipetrol.Empleado_Temp)
            {
                PorcenExtraPrima = 0;
            }

            switch (tipoSeguro)
            {
                case enumTipoSeguro.Seguro_de_incendio_y_terremoto:
                    var factor = listTipoCreditoTipoSeguroRelacion.Where(x => x.IdTipoSeguro == (int)tipoSeguro)
                                                                  .Where(y => y.RangoInicial <= edad && y.RangoFinal >= edad).FirstOrDefault();
                    totalSeguro += (decimal)(valorTotalInmueble * (factor == null ? 0 : factor.Factor));
                    break;
                case enumTipoSeguro.Seguro_de_vida:
                    double? PorcenModeloFactor, SumatoriaModeloFactor;
                    if (PorcenExtraPrima != 0)
                    {
                        PorcenModeloFactor = tasaMensualSeguroVida * PorcenExtraPrima;
                        SumatoriaModeloFactor = tasaMensualSeguroVida + PorcenModeloFactor;
                    }
                    else
                    {
                        SumatoriaModeloFactor = tasaMensualSeguroVida;
                    }
                    totalSeguro += (decimal)(saldo * SumatoriaModeloFactor);
                    break;
            }

            return (double)totalSeguro;

        }
        protected override void CalcularGrupoValorCuota(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, ref float? valorCuotaCierreFinanciero, FormCapacidadPagoGrupos grupoMaximoDescuentoPorNomina, float? ingresoNetoViviendaFamiliar, string tipoCapacidadPago, List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion, double saldo, enumPapelCavipetrol papelCavipetrol, int edad, double valorTotalInmueble, double tasaMensualSeguroVida, double PorcenExtraPrima)
        {
            if (tipoCapacidadPago == "A_Vivienda")
                grupoMaximoDescuentoPorNomina.Total = grupoMaximoDescuentoPorNomina.Total / 2;
            //var seguroVida = ObtenerTotalSeguros(listTipoCreditoTipoSeguroRelacion, saldo, valorTotalInmueble, tasaMensualSeguroVida, PorcenExtraPrima, papelCavipetrol, enumTipoSeguro.Seguro_de_vida, edad);
            //var seguroIncendio = ObtenerTotalSeguros(listTipoCreditoTipoSeguroRelacion, saldo, valorTotalInmueble, tasaMensualSeguroVida, PorcenExtraPrima, papelCavipetrol, enumTipoSeguro.Seguro_de_incendio_y_terremoto, edad);
            valorCuotaCierreFinanciero = ((float?)grupoMaximoDescuentoPorNomina.Total < ingresoNetoViviendaFamiliar ?
                                               (float?)grupoMaximoDescuentoPorNomina.Total : ingresoNetoViviendaFamiliar);
            

            //valorCuotaCierreFinanciero = tipoCapacidadPago == "A_Vivienda" ? valorCuotaCierreFinanciero * 2: valorCuotaCierreFinanciero;
            //valorCuotaCierreFinanciero = valorCuotaCierreFinanciero - (float?)(seguroVida + seguroIncendio);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Valor_Cuota,
                                                enumFormCapacidadPago.Valor_Cuota_cierre_Financiero, "ValorAsociado",
                                                valorCuotaCierreFinanciero);
            float? maximoDescuentoPorNomina = 0;
            float valorPorCaja = 0;
            maximoDescuentoPorNomina = grupoMaximoDescuentoPorNomina.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Máximo_descuento_de_nomina).SingleOrDefault().ValorAsociado;
            var maximoDescuentoPorNominaInicial = maximoDescuentoPorNomina;
            var ingresoNetoDisponible = (float?)listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Ingreso_Neto_Disponible).SingleOrDefault().Total;

            float? totalIngresoNetoDisponible = tipoCapacidadPago == "A_Vivienda" ? ingresoNetoDisponible / 2 : ingresoNetoDisponible;

            if (maximoDescuentoPorNomina < 0)
            {
                maximoDescuentoPorNomina = 0;
            }
            else if (Math.Min((float)totalIngresoNetoDisponible, (float)maximoDescuentoPorNomina) < 0)
            {
                maximoDescuentoPorNomina = 0;
            }
            else {
                maximoDescuentoPorNomina = Math.Min((float)totalIngresoNetoDisponible, (float)maximoDescuentoPorNomina);
            }
            /////////////////////////////////////////////////////////////////////
            if (maximoDescuentoPorNomina < 0 && totalIngresoNetoDisponible < 0)
            {
                valorPorCaja = 0;
            }
            else if (maximoDescuentoPorNomina < 0) {
                valorPorCaja = (float)maximoValorPagoPorCaja;
            }
            else if (totalIngresoNetoDisponible - maximoDescuentoPorNominaInicial < 0) {
                valorPorCaja = 0;
            }
            else {
                valorPorCaja = (float)(totalIngresoNetoDisponible - maximoDescuentoPorNominaInicial);
            }           

            listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Nomina).SingleOrDefault().ValorAsociado = maximoDescuentoPorNomina;
            listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Caja).SingleOrDefault().ValorAsociado = valorPorCaja;

            var cuota_Cierre_Final = valorPorCaja + maximoDescuentoPorNomina;
            switch (tipoCapacidadPago) {
                case "A_Vivienda":
                    listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Cuota_Cierre_Final).SingleOrDefault().ValorAsociado = (cuota_Cierre_Final * 2);
                    break;
                //case "C_Vivienda":
                //    listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Cuota_Cierre_Final).SingleOrDefault().ValorAsociado = (cuota_Cierre_Final * 2);
                //    break;
                default:
                    listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Cuota).SingleOrDefault().ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Cuota_Cierre_Final).SingleOrDefault().ValorAsociado = cuota_Cierre_Final;
                    break;
            }
            


        }
        protected  void CalcularGrupoSolvencia(ref List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos, FormCapacidadPagoGrupos grupoSolvencia)
        {
            float? activos = grupoSolvencia.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Activo).FirstOrDefault().ValorAsociado;
            float? pasivos = grupoSolvencia.ListaFormCapacidadPago.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Pasivo).FirstOrDefault().ValorAsociado;
            float? patrimonio = activos - pasivos;

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Solvencia,
                                                enumFormCapacidadPago.Patrimonio, "ValorAsociado",
                                                patrimonio);

            AsignarValorFormCapacidadPagoGrupos(ref listaFormCapacidadPagoGrupos, enumFormCapacidadPagoGrupos.Solvencia,
                                                enumFormCapacidadPago.Solvencia, "ValorAsociado",
                                                (patrimonio / activos));
        }       
    }
}
