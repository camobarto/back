﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model.Administracion;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Administracion;
using System;
using System.Collections.Generic; 
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class Administracion
    {
        private readonly IAdministracion _administracionUnidadTrabajo;

        public Administracion()
        {
            _administracionUnidadTrabajo = new AdministracionUnidadTrabajo();
        }

        public IEnumerable<LineaCredito> ObtenerLineaCredito()
        {
            return _administracionUnidadTrabajo.ObtenerLineaCredito();
        }
        public string GuardarLineaCredito(int id,string nombre, string Descripcion, int bandera)
        {
            return _administracionUnidadTrabajo.GuardarLineaCredito(id,nombre, Descripcion, bandera);
        }

        public string EliminarLineaCredito(int id)
        {
            return _administracionUnidadTrabajo.EliminarLineaCredito(id);
        }
        public IEnumerable<ViewTiposCreditos> ObtenerTiposCredito()
        {
            return _administracionUnidadTrabajo.ObtenerTiposCredito();
        }
        public string AdministrarTiposCredito(int id, string nombre, int idLinea, int bandera)
        {
            return _administracionUnidadTrabajo.AdministrarTiposCredito(id, nombre, idLinea, bandera);
        }
        public string EliminarTiposCredito(int id)
        {
            return _administracionUnidadTrabajo.EliminarTiposCredito(id);
        }
        public IEnumerable<ViewPapelCavipetrol> ObtenerPapelCavipetrol()
        {
            return _administracionUnidadTrabajo.ObtenerPapelCavipetrol();
        }
        public string AdministrarPapelCavipetrol(int id, string nombre, string Descripcion, int idcontrato, int bandera)
        {
            return _administracionUnidadTrabajo.AdministrarPapelCavipetrol(id, nombre, Descripcion, idcontrato, bandera);
        }
        public string EliminarPapelCavipetrol(int id)
        {
            return _administracionUnidadTrabajo.EliminarPapelCavipetrol(id);
        }
        public IEnumerable<ViewTipoCreditoTipoRelacion> ObtenerTipoCreditoTipoRolRelacion()
        {
            return _administracionUnidadTrabajo.ObtenerTipoCreditoTipoRolRelacion();
        }
        public string AdministrarTipoCreditoTipoRol(TipoCreditoTipoRol datos)
        {
            return _administracionUnidadTrabajo.AdministrarTipoCreditoTipoRol(datos);
        }
    }
}
