﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using System.Collections.Generic;
using Cavipetrol.SICSES.Infraestructura.Model.Fodes;

namespace Cavipetrol.SICSES.BLL.Controllers
{
	public class Fodes

	{
		SicavUnidadTrabajo _sicavUnidadTrabajo = new SicavUnidadTrabajo();
		ConsultasApoyoUnidadTrabajo _consultasApoyoUnidadTrabajo = new ConsultasApoyoUnidadTrabajo();

		public IEnumerable<PorcentajesTasas> ObtenerTasas(string nomPorcentaje)
		{
			return _sicavUnidadTrabajo.ObtenerTasas(nomPorcentaje);
		}
		public void CrearNuevaTasa(string nomPorcentaje, string porcemtaje)
		{
			_sicavUnidadTrabajo.CrearNuevaTasa(nomPorcentaje, porcemtaje);
		}
		public IEnumerable<FomentoEmpresarial> ObtenerFomentoEmresarial()
		{
			return _sicavUnidadTrabajo.ObtenerFomentoEmresarial();
		}
		public void MarcarCreditoFodes(string documentoTipo, string documentoNumero, int numeroUnico, string tipoDocumento, string numeroDocumento, string descripcion)
		{
			_sicavUnidadTrabajo.MarcarCreditoFodes(documentoTipo, documentoNumero, numeroUnico, tipoDocumento, numeroDocumento, descripcion);
		}
		public IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMarcados()
		{
			return _sicavUnidadTrabajo.ObtenerCreditosFomentoEmresarialMarcados();
		}
		public void DesmarcarCreditoFodes(List<FodesGenerico> listaDesmarcacion)
		{
			_sicavUnidadTrabajo.DesmarcarCreditoFodes(listaDesmarcacion);
		}
		public IEnumerable<FomentoEmpresarial> ObtenerCreditosFomentoEmresarialMora()
		{
			return _sicavUnidadTrabajo.ObtenerCreditosFomentoEmresarialMora();
		}
		public IEnumerable<CausalesFodes> ObtenerCausalesFodes()
		{
			return _consultasApoyoUnidadTrabajo.ObtenerCausalesFodes();
		}
		public IEnumerable<InfoFodes> InformacionFodes()
		{
			return _sicavUnidadTrabajo.InformacionFodes();
		}
	}
}
