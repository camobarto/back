﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class ReportesSICSES
    {
        private readonly IReportesUnidadTrabajo _reportesUnidadTrabajo;
        public ReportesSICSES()
        {
            _reportesUnidadTrabajo = new ReportesUnidadTrabajo();
        }

        public bool ActulizarParametrosCavipetrol(ParametroUsuarioCavipetrol registroParametrosCavipetrol)
        {
            return _reportesUnidadTrabajo.ActulizarParametrosCavipetrol(registroParametrosCavipetrol);
        }

        public bool InsertarHistoricoReporte(ReporteHistorico reporteHistorico)
        {
            return _reportesUnidadTrabajo.InsertarHistoricoReporte(reporteHistorico);
        }

        public IEnumerable<ParametroUsuarioCavipetrol> ObtenerParametrosRegistroCavipetrol()
        {
            return _reportesUnidadTrabajo.ObtenerParametrosRegistroCavipetrol();
        }

        public RespuestaReporte<IEnumerable<ReporteInformacionEstadistica>> ObtenerReporteInfromacionEstadisticas(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteInformacionEstadistica>> respuesta = new RespuestaReporte<IEnumerable<ReporteInformacionEstadistica>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                int diasMes = System.DateTime.DaysInMonth(ano, mes);
                DateTime fechaPeriodo = new DateTime(ano, mes, diasMes);

                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteInfromacionEstadisticaInicial(fechaPeriodo);
                return respuesta;
            }

            //respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteInformacionEstadistica>>(consultarHistorico.DatosReporte);
            return respuesta;
        }

        public RespuestaReporte<IEnumerable<ReporteOrganosDireccionyControl>> ObtenerReporteOrganosDirreccionControl(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteOrganosDireccionyControl>> respuesta = new RespuestaReporte<IEnumerable<ReporteOrganosDireccionyControl>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteOrganosDirreccionControl();
                return respuesta;
            }

            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteOrganosDireccionyControl>>(consultarHistorico.DatosReporte);
            return respuesta;


        }

        public RespuestaReporte<IEnumerable<ReporteIndividualDeCarteraDeCredito>> ObtenerReporteindividualDeCarteraDeCredito(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteIndividualDeCarteraDeCredito>> respuesta = new RespuestaReporte<IEnumerable<ReporteIndividualDeCarteraDeCredito>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            //if (consultarHistorico == null)
            //{
            //respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteindividualDeCarteraDeCredito(mes, ano);
            //return respuesta;
            //}

            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteIndividualDeCarteraDeCredito>>(consultarHistorico.DatosReporte);
            return respuesta;


        }
        public RespuestaReporte<IEnumerable<ReporteUsuarios>> ObtenerReporteUsuarios(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteUsuarios>> respuesta = new RespuestaReporte<IEnumerable<ReporteUsuarios>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {

                int diasMes = System.DateTime.DaysInMonth(ano, mes);
                DateTime fechaCorte = new DateTime(ano, mes, diasMes);
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteUsuarios(fechaCorte);
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            serializador.MaxJsonLength = 500000000;
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteUsuarios>>(consultarHistorico.DatosReporte);
            return respuesta;

        }

        public RespuestaReporte<IEnumerable<ReporteRedOficinasYCorresponsalesNoBancarios>> ObtenerReporteRedOficinasYCorresponsalesNoBancarios(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteRedOficinasYCorresponsalesNoBancarios>> respuesta = new RespuestaReporte<IEnumerable<ReporteRedOficinasYCorresponsalesNoBancarios>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                //TO DO: Lógica de negocio para obtener reporte Red de oficicinas y corresponsales no bancarios.
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteRedOficinasYCorresponsalesNoBancarios();
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteRedOficinasYCorresponsalesNoBancarios>>(consultarHistorico.DatosReporte);
            return respuesta;
        }

        public ReporteHistorico ConsultarHistorico(int idReporte, int mes, int ano)
        {
            //TO DO: Lógica de negocio para obtener reporte usuarios
            return _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);
        }

        public RespuestaReporte<IEnumerable<ReporteParentescos>> ObtenerInformeParentescos(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteParentescos>> respuesta = new RespuestaReporte<IEnumerable<ReporteParentescos>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                //TO DO: Lógica de negocio para obtener reporte Red de oficicinas y corresponsales no bancarios.
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerInformeParentescos();
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteParentescos>>(consultarHistorico.DatosReporte);
            return respuesta;
        }

        public RespuestaReporte<IEnumerable<ReporteRelacionBienesRecibosPago>> ObtenerRelacionBienesPagoRecibidos(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteRelacionBienesRecibosPago>> respuesta = new RespuestaReporte<IEnumerable<ReporteRelacionBienesRecibosPago>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                //TO DO: Lógica de negocio para obtener reporte Red de oficicinas y corresponsales no bancarios.
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerRelacionBienesPagoRecibidos();
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteRelacionBienesRecibosPago>>(consultarHistorico.DatosReporte);
            return respuesta;
        }
        public RespuestaReporte<IEnumerable<ReporteIndividualCarteraCredito>> ObtenerInformacionCarteraCredito(string anho, string mes)
        {
            RespuestaReporte<IEnumerable<ReporteIndividualCarteraCredito>> respuesta = new RespuestaReporte<IEnumerable<ReporteIndividualCarteraCredito>>();
            respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerInformacionCarteraCredito(anho, mes);
            return respuesta;
        }

        public RespuestaReporte<IEnumerable<ReporteInformeIndividualCaptaciones>> ObtenerInformeIndividualCaptacioness(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteInformeIndividualCaptaciones>> respuesta = new RespuestaReporte<IEnumerable<ReporteInformeIndividualCaptaciones>>();
            
                int diasMes = System.DateTime.DaysInMonth(ano, mes);
                DateTime fechaCorte = new DateTime(ano, mes, diasMes);
                //TO DO: Lógica de negocio para obtener reporte Red de oficicinas y corresponsales no bancarios.
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerInformeIndividualCaptaciones(fechaCorte);
                return respuesta;           
        }

        public RespuestaReporte<IEnumerable<ReporteAportesContribuciones>> ObtenerReporteAportesContribuciones(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteAportesContribuciones>> respuesta = new RespuestaReporte<IEnumerable<ReporteAportesContribuciones>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                int diasMes = System.DateTime.DaysInMonth(ano, mes);
                DateTime fechaCorte = new DateTime(ano, mes, diasMes);
                //TO DO: Lógica de negocio para obtener reporte Informe puc.
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteAportesContribuciones(fechaCorte);
                return respuesta;


            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            serializador.MaxJsonLength = 500000000;
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteAportesContribuciones>>(consultarHistorico.DatosReporte);
            return respuesta;
        }


        public RespuestaReporte<IEnumerable<ReportePUC>> ObtenerInformePUC(int idReporte, int mes, int ano, int periodo)
        {
            RespuestaReporte<IEnumerable<ReportePUC>> respuesta = new RespuestaReporte<IEnumerable<ReportePUC>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                int diasMes = System.DateTime.DaysInMonth(ano, mes);
                DateTime fechaCorte = new DateTime(ano, mes, diasMes);
                //TO DO: Lógica de negocio para obtener reporte Informe puc.
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerInformePUC(fechaCorte, periodo);
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReportePUC>>(consultarHistorico.DatosReporte);
            return respuesta;
        }


        public RespuestaReporte<IEnumerable<ReporteRelacionPropiedadesPlantaEquipo>> ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteRelacionPropiedadesPlantaEquipo>> respuesta = new RespuestaReporte<IEnumerable<ReporteRelacionPropiedadesPlantaEquipo>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                int diasMes = System.DateTime.DaysInMonth(ano, mes);
                DateTime fechaCorte = new DateTime(ano, mes, diasMes);
                //TO DO: Lógica de negocio para obtener reporte Informe puc.
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteInformeRelacionPropiedadesPlantaEquipo(fechaCorte);
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteRelacionPropiedadesPlantaEquipo>>(consultarHistorico.DatosReporte);
            return respuesta;
        }

        public RespuestaReporte<IEnumerable<ReporteRelacionInversiones>> ObtenerReporteInformeRelacionInversiones(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteRelacionInversiones>> respuesta = new RespuestaReporte<IEnumerable<ReporteRelacionInversiones>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                int diasMes = System.DateTime.DaysInMonth(ano, mes);
                DateTime fechaCorte = new DateTime(ano, mes, diasMes);
                //TO DO: Lógica de negocio para obtener reporte Informe Relacion Inversiones.
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteInformeRelacionInversiones(fechaCorte);
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteRelacionInversiones>>(consultarHistorico.DatosReporte);
            return respuesta;
        }

        public RespuestaReporte<IEnumerable<ReporteProductosOfrecidos>> ObtenerReporte4Uiaf(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteProductosOfrecidos>> respuesta = new RespuestaReporte<IEnumerable<ReporteProductosOfrecidos>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                //TO DO: Lógica de negocio para obtener reporte Red de oficicinas y corresponsales no bancarios.
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporte4Uiaf();
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteProductosOfrecidos>>(consultarHistorico.DatosReporte);
            return respuesta;
        }

        public RespuestaReporte<IEnumerable<ClasificacionExcedentes>> ObtenerAplicacionExcedentes(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ClasificacionExcedentes>> respuesta = new RespuestaReporte<IEnumerable<ClasificacionExcedentes>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                int diasMes = System.DateTime.DaysInMonth(ano, mes);
                DateTime fechaCorte = new DateTime(ano, mes, diasMes);
                //TO DO: Lógica de negocio para obtener reporte Informe puc.
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerAplicacionExcedentes(fechaCorte);
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ClasificacionExcedentes>>(consultarHistorico.DatosReporte);
            return respuesta;
        }


        public RespuestaReporte<IEnumerable<ReporteEducacionFormal>> ObtenerEducacionFormal(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteEducacionFormal>> respuesta = new RespuestaReporte<IEnumerable<ReporteEducacionFormal>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                return respuesta;
            }


            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteEducacionFormal>>(consultarHistorico.DatosReporte);
            return respuesta;
        }

        public RespuestaReporte<IEnumerable<ReporteCreditosBancosFinancieros>> ObtenerCreditosBancariosyFinan(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteCreditosBancosFinancieros>> respuesta = new RespuestaReporte<IEnumerable<ReporteCreditosBancosFinancieros>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteCreditosBancosFinancieros>>(consultarHistorico.DatosReporte);
            return respuesta;
        }

        public RespuestaReporte<IEnumerable<ReporteProcesosJudiciales>> ObtenerProcesosJudiciales(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteProcesosJudiciales>> respuesta = new RespuestaReporte<IEnumerable<ReporteProcesosJudiciales>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteProcesosJudiciales>>(consultarHistorico.DatosReporte);
            return respuesta;
        }

        public RespuestaReporte<IEnumerable<T>> ObtenerHistoricoInformesSICSES<T>(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<T>> respuesta = new RespuestaReporte<IEnumerable<T>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            respuesta.InformacionReporte = serializador.Deserialize<List<T>>(consultarHistorico.DatosReporte);
            return respuesta;
        }

        public RespuestaReporte<IEnumerable<ReporteRetiroeIngresoAsociados>> ObtenerInformeRetiroeIngresoAsociados(int idReporte, int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteRetiroeIngresoAsociados>> respuesta = new RespuestaReporte<IEnumerable<ReporteRetiroeIngresoAsociados>>();
            var consultarHistorico = _reportesUnidadTrabajo.ConsultarHistorico(idReporte, mes, ano);

            if (consultarHistorico == null)
            {

                int diasMes = System.DateTime.DaysInMonth(ano, mes);
                DateTime fechaCorte = new DateTime(ano, mes, diasMes);
                //TO DO: Lógica de negocio para obtener reporte Red de oficicinas y corresponsales no bancarios.
                respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerInformeRetiroeIngresoAsociados(fechaCorte);
                return respuesta;
            }
            respuesta.EsHistorico = true;
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            serializador.MaxJsonLength = 500000000;
            respuesta.InformacionReporte = serializador.Deserialize<List<ReporteRetiroeIngresoAsociados>>(consultarHistorico.DatosReporte);
            return respuesta;
        }      

        public RespuestaReporte<IEnumerable<ReporteDetalleInformacionEstadistica>> ObtenerReporteDetalleInformacionEstadistica(int mes, int ano)
        {
            RespuestaReporte<IEnumerable<ReporteDetalleInformacionEstadistica>> respuesta = new RespuestaReporte<IEnumerable<ReporteDetalleInformacionEstadistica>>();

            int diasMes = System.DateTime.DaysInMonth(ano, mes);
            DateTime fechaPeriodo = new DateTime(ano, mes, diasMes);
            respuesta.InformacionReporte = _reportesUnidadTrabajo.ObtenerReporteDetalleInfromacionEstadistica(fechaPeriodo);
            JavaScriptSerializer serializador = new JavaScriptSerializer();
            return respuesta;
        }

    }
}
