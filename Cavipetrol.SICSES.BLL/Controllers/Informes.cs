﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model.Informes;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class Informes
    {
        private readonly IInformesUnidadTrabajo _creditosUnidadTrabajo;
        private readonly IConsultasApoyoUnidadTrabajo _consultasApoyoUnidadTrabajo;
        private readonly ISicavUnidadTrabajo _SicavUnidadTrabajo;

        // private MensajesValidaciones mensajes;
        public Informes()
        {
            // mensajes = new MensajesValidaciones();
            _creditosUnidadTrabajo = new InformesUnidadTrabajo();
            _consultasApoyoUnidadTrabajo = new ConsultasApoyoUnidadTrabajo();
            _SicavUnidadTrabajo = new SicavUnidadTrabajo();
        }
        

        public IEnumerable<RetencionProveedores> ObtenerInformacionGarantia(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "")
        {
            return _creditosUnidadTrabajo.ObtenerInformacionGarantia(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta);
        }
        public IEnumerable<RetencionProveedores_ICA> ObtenerInformacionGarantia2(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "")
        {
            return _creditosUnidadTrabajo.ObtenerInformacionGarantia2(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta, Bimestre);
        }
        public IEnumerable<RetencionProveedores_IVA> ObtenerInformacionGarantia3(string IdOpcionInfo = "", string Nit = "", string Fc_Desde = "", string Fc_Hasta = "", string Bimestre = "")
        {
            return _creditosUnidadTrabajo.ObtenerInformacionGarantia3(IdOpcionInfo, Nit, Fc_Desde, Fc_Hasta, Bimestre);
        }

        public bool InsertarDatosGarantias(List<DatosIva> reporteHistorico)
        {
            DataTable dt = new DataTable();
            dt = ListToDataTable(reporteHistorico);
            return _creditosUnidadTrabajo.InsertarDatosGarantias(dt);
        }

        public DataTable ListToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection propiedades = TypeDescriptor.GetProperties(typeof(T));
            DataTable dt = new DataTable();

            //crea las columnas de la datatable
            foreach(PropertyDescriptor propiedad in propiedades)
            {
                dt.Columns.Add(propiedad.Name, Nullable.GetUnderlyingType(propiedad.PropertyType) ?? propiedad.PropertyType);
            }
        
            //crea las filas de la datatable
            foreach(T item in data)
            {
                DataRow row = dt.NewRow();
                //llena las celdas con la ifno de la datatable
                foreach(PropertyDescriptor propiedad in propiedades)
                {
                    row[propiedad.Name] = propiedad.GetValue(item) ?? DBNull.Value;
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        public bool ValidaDatosExcel(string[] vDato)
        {
            foreach(var item in vDato)
            {
                if(String.IsNullOrEmpty(item))
                {
                    return false;
                }
            }
            return true;
        }

        //validacion de tipos de datos sin terminar
        //public bool ValidaDatosExcelTipoDato(string[] vTDato)
        //{
        //    foreach (var item in vTDato)
        //    {
        //        DatosIva DatosExcelTipoDato = new DatosIva();
        //        var RET_NUM_SOPORTE = DatosExcelTipoDato.RET_NUM_SOPORTE;
        //        if (RET_NUM_SOPORTE.GetType() == Type.GetType(item))
        //        {
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        public bool ActualizaInfoProveedores(int bandera)
        {
            return _creditosUnidadTrabajo.ActualizaInfoProveedores(bandera);
        }
        public IEnumerable<ProveedoresFaltantes> GeneraProveedoresFaltantes(int bandera)
        {
            return _creditosUnidadTrabajo.GeneraProveedoresFaltantes(bandera);
        }
        public IEnumerable<CuentaContable> ObtenerCuentaContable(int bandera)
        {
            return _creditosUnidadTrabajo.ObtenerCuentaContable(bandera);
        }
        public IEnumerable<AuxTercero> ConsultaAuxTercero(string mes, string anio, string cuenta)
        {
            return _creditosUnidadTrabajo.ConsultaAuxTercero(mes, anio,cuenta);
        }
        public IEnumerable<AuditaCuenta> ConsultaAuditaCuenta(string Fc_Ini, string Fc_Fin, string Cuenta)
        {
            return _creditosUnidadTrabajo.ConsultaAuditaCuenta(Fc_Ini, Fc_Fin, Cuenta);
        }
        public IEnumerable<GMF_BUno> ConsutaGMF_BUno(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            return _creditosUnidadTrabajo.ConsutaGMF_BUno(Fc_Ini, Fc_Fin, Bandera);
        }
        public IEnumerable<GMF_BDos> ConsutaGMF_BDos(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            return _creditosUnidadTrabajo.ConsutaGMF_BDos(Fc_Ini, Fc_Fin, Bandera);
        }
        public IEnumerable<GMF_BTres> ConsutaGMF_BTres(string Fc_Ini, string Fc_Fin, int Bandera)
        {
            return _creditosUnidadTrabajo.ConsutaGMF_BTres(Fc_Ini, Fc_Fin, Bandera);
        }

        public IEnumerable<SaldoInicialCarteraEcopetrol> ObtenerSaldoInicialInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta,int Bandera)
        {
            return _creditosUnidadTrabajo.ObtenerSaldoInicialInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }
        public IEnumerable<EncabezadoCarteraEcopetrol> ObtenerEncabezadoInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _creditosUnidadTrabajo.ObtenerEncabezadoInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }
        public IEnumerable<PlanPagosCarteraEcopetrol> ObtenerPlanPagosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _creditosUnidadTrabajo.ObtenerPlanPagosInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }
        public IEnumerable<MovimientosCarteraEcopetrol> ObtenerMovimientosInformeEcopetrol(string TipoNit, string NumeroIdentificacion, string producto, string Doctipo, int Docunumero, string FechaDesde, string FechaHasta, int Bandera)
        {
            return _creditosUnidadTrabajo.ObtenerMovimientosInformeEcopetrol(TipoNit, NumeroIdentificacion, producto, Doctipo, Docunumero, FechaDesde, FechaHasta, Bandera);
        }

        public IEnumerable<TipoDocumentoPorCedula> ObtenerTipoDocumentoPorCedula(string TipoIdentificacion, int Cedula)
        {
            return _creditosUnidadTrabajo.ObtenerTipoDocumentoPorCedula(TipoIdentificacion, Cedula);
        }

        public IEnumerable<FacturaElectronica> ObtenerFacturaElectronica(string Factura, string Servidor)
        {
            return _creditosUnidadTrabajo.ObtenerFacturaElectronica(Factura, Servidor);
        }
        public RespuestaNegocio<string> GenerarFactura(Nota nota) {
            RespuestaNegocio<string> respuestaNegocio = new RespuestaNegocio<string>();
            try
            {
                nota.IdFactura = _SicavUnidadTrabajo.ObtenerNumeroFactura(nota.Servidor).NumeroFactura.ToString();
                _creditosUnidadTrabajo.GuardarNota(nota);
                respuestaNegocio.Respuesta = nota.IdFactura;
                respuestaNegocio.Estado = true;
            }
            catch (Exception ex)
                {
                respuestaNegocio.Estado = false;
                respuestaNegocio.MensajesError.Add(ex.Message);
                respuestaNegocio.MensajesError.Add(ex.InnerException != null ? ex.InnerException.Message: string.Empty);
            }
            return respuestaNegocio;            
        }
        public IEnumerable<Certificaciones_Ingresos_Retenciones> Certificaciones(string NumeroIdentificacion, int ano, int mes)
        {
            DateTime FechaCorte = new DateTime(ano, mes, 1);
            return _creditosUnidadTrabajo.Certificaiones(NumeroIdentificacion, FechaCorte);
        }
    }
}
