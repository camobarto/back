﻿using Cavipetrol.SICSES.DAL;
using Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Cavipetrol.SICSES.Infraestructura.Respuesta;

namespace Cavipetrol.SICSES.BLL.Controllers.CreditoViviendaEcopetrol
{
    public class CreditoViviendaEcopetrol
    {
        private readonly SolicitudCreditoNegocio _solicitudCredito;
        CreditosContexto ObjCreditosEcpContexto = new CreditosContexto();
        //GUARDA LOS DATOS DEL EXCEL EN LA TMP </jarp>
        public bool GuardaDatosCartasCrViviendaECPTmp(List<DatosCartasCrViviendaEcopetrol> DatosCartas)
        {
            DataTable dtDatosCartas = new DataTable();
            dtDatosCartas = ListToDataTable(DatosCartas);
            ObjCreditosEcpContexto.GuardaDatosCartasCrViviendaECPTmp(dtDatosCartas);
            return true;

        }
        //VALIDA, OBTIENE ERRORES Y GUARDA LOS DATOS DE LAS CARTAS SI NO HAY ERRORES </jarp>
        public IEnumerable<DatosCartasCrViviendaEcopetrolConsultaErrores> ValidaObtieneGuardaDatosCartasCrViviendaECP()
        {
            try
            {
                return ObjCreditosEcpContexto.ValidaObtieneGuardaDatosCartasCrViviendaECP();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //CONSULTA LOS DATOS DE LAS CARTAS ADJUDICADAS </jarp>
        public IEnumerable<DatosCartasCrViviendaEcopetrolConsulta> ObtenerDatosCartasECP(string fechaCargue)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerDatosCartasECP(fechaCargue).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //GUARDA LOS DATOS PARA ACTUALIZAR DEL EXCEL EN LA TMP </jarp>
        public bool GuardarDatosCartasActualizadasCrViviendaECPTmp(List<DatosCartasActualizarCrViviendaEcopetrol> DatosCartas)
        {
            try
            {
                DataTable dtDatosCartas = new DataTable();
                dtDatosCartas = ListToDataTable(DatosCartas);
                ObjCreditosEcpContexto.GuardarDatosCartasActualizadasCrViviendaECPTmp(dtDatosCartas);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //VALIDA, OBTIENE ERRORES Y ACTUALIZA LOS DATOS DE LAS CARTAS SI NO HAY ERRORES </jarp>
        public IEnumerable<DatosCartasActualizarCrViviendaEcopetrolConsultaErrores> ValidaObtieneActualizaDatosCartasCrViviendaECP()
        {
            try
            {
                return ObjCreditosEcpContexto.ValidaObtieneActualizaDatosCartasCrViviendaECP().ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public virtual object IniciarProcesoCreditoECP(int PI_Id_Formulario, int PI_Numero_de_Identificacion, int PI_Comodin, string PO_Url, int PO_Bandera, string PO_Proceso)
        {
            try
            {
                return ObjCreditosEcpContexto.IniciarProcesoCreditoECP(PI_Id_Formulario, PI_Numero_de_Identificacion, PI_Comodin, PO_Url, PO_Bandera, PO_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //CONSULTA LOS DATOS DE LAS CARTAS ADJUDICADAS FILTRANDO POR DOCUMENTO y TIPO </jarp>

        public IEnumerable<DatosCartasCrViviendaEcopetrolConsultaCarga> ObtenerDatosCargaAdjudicacion(string numeroIdentificacion, string tipoDocumento)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerDatosCargaAdjudicacion(numeroIdentificacion, tipoDocumento).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //CONSULTA LOS DATOS DE LOS TIPOS DE GESTION PARA EL PROCESO DE CREDITO ECOPETROL </jarp>

        public IEnumerable<DatosCartasCrViviendaEcopetrolConsultaTipoGestion> ObtenerTipoGestion()
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerTipoGestion();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<DatosCartasCrViviendaEcopetrolConsultaMenu> ObtenerMenu(string numeroIdentificacion, string comodin)
        {
            try
            {
                /*
                 Comodin 1 = Obtiene todos los menus cuyo proceso de gestion son 1 y no transversales
                 Comodin 2 = Obtiene el IdMenu filtrando por el nombre.
                 */
                return ObjCreditosEcpContexto.ObtenerMenu(numeroIdentificacion, comodin);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<DatosCartasCrViviendaEcopetrolConsultaMenu> ObtenerIdMenu(string numeroIdentificacion, string comodin)
        {
            try
            {
                /*
                 Comodin 1 = Obtiene todos los menus cuyo proceso de gestion son 1 y no transversales
                 Comodin 2 = Obtiene el IdMenu filtrando por el nombre.
                 */
                return ObjCreditosEcpContexto.ObtenerMenu(numeroIdentificacion, comodin);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<DatosCartasCrViviendaEcopetrolConsultaHistoricoBitacora> ObtenerDatosHistoricoGestion(string numeroIdentificacion)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerDatosHistoricoGestion(numeroIdentificacion);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<DatosCartasCrViviendaEcopetrolConsultaREPORTES_SES> ObtenerDatosAsegurabilidad_REPORTES_SES(string numeroIdentificacion, string tipodocumento)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerDatosAsegurabilidad_REPORTES_SES(numeroIdentificacion, tipodocumento);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<DatosCartasCrViviendaEcopetrolConsultaTipoProducto> ObtenerTipoProducto(string Comodin, string Filtro)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerTipoProducto(Comodin, Filtro);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<DatosCartasCrViviendaEcopetrolConsultaTipoNorma> ObtenerTipoNorma(string Comodin, string Filtro)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerTipoNorma(Comodin, Filtro);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ObjectInt GuardarGestionBitacora(CreditoEcopetrolBitacora BitacoraModel)
        {
            try
            {
                return ObjCreditosEcpContexto.GuardarGestionBitacora(BitacoraModel).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ObjectInt GuardarGestionAsegurabilidad(CreditoEcopetrolAsegurabilidad AsegurabilidadModel)
        {
            try
            {
                return ObjCreditosEcpContexto.GuardarGestionAsegurabilidad(AsegurabilidadModel).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public int GuardarAdjudicacion(CreditoEcopetrolAdjudicacion AdjudicacionModel)
        {
            try
            {
                return ObjCreditosEcpContexto.GuardarAdjudicacion(AdjudicacionModel).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        //CONVIERTE LISTA A DATATABLE PARA SU MANEJO </jarp>
        public DataTable ListToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection propiedades = TypeDescriptor.GetProperties(typeof(T));
            DataTable dt = new DataTable();

            //crea las columnas de la datatable
            foreach (PropertyDescriptor propiedad in propiedades)
            {
                dt.Columns.Add(propiedad.Name, Nullable.GetUnderlyingType(propiedad.PropertyType) ?? propiedad.PropertyType);
            }

            //crea las filas de la datatable
            foreach (T item in data)
            {
                DataRow row = dt.NewRow();
                //llena las celdas con la ifno de la datatable
                foreach (PropertyDescriptor propiedad in propiedades)
                {
                    row[propiedad.Name] = propiedad.GetValue(item) ?? DBNull.Value;
                }
                dt.Rows.Add(row);
            }
            return dt;
        }
        /// <summary>
        /// Obtiene los usuos de credito
        /// </summary>
        /// <returns>Lista con los usos del credito</returns>
        public IEnumerable<DatosUsosCreditos> ObtenerUsosSolicitudCreditoECP()
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerUsosSolicitudCreditoECP().ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Obtiene los usuos de credito
        /// </summary>
        /// <returns>Lista con los usos del credito</returns>
        public IEnumerable<DatosUsosCreditos> ObtenerInfoUsosSolicitudCreditoECP(string NumIdetificacion)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerInfoUsosSolicitudCreditoECP(NumIdetificacion).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Obtiene los usuos de credito
        /// </summary>
        /// <returns>Lista con los usos del credito</returns>
        public IEnumerable<DatosHipotecaPersistencia> ObtenerInfoHipotecaSolicitudECP(string NumIdentificacion)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerInfoHipotecaSolicitudECP(NumIdentificacion).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Obtiene las formas fe pago segun la norma
        /// </summary>
        /// <param name="norma">Norma que se adjudicó</param>
        /// <returns>Lista con las formas de pago segun la norma adjudicada</returns>
        public IEnumerable<DatosFormaPago> ObtenerFormasPago(string norma)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerFormasPago(norma).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Obtiene los datos de la carta de adjudicación
        /// </summary>
        /// <param name="nIdentificacion">Número de identificacion</param>
        /// <returns>Los datos de la carta adjudicada</returns>
        public IEnumerable<DatosCartasCrViviendaEcopetrolConsulta> ObtenerDatosCartaECP(string nIdentificacion)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerDatosCartaECP(nIdentificacion).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public Boolean ValidarSolicitud(int IdConsecutivo)
        {
            try
            {
                if (ObjCreditosEcpContexto.ValidarSolicitud(IdConsecutivo).FirstOrDefault() == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public virtual void GuardarUsosCreditoECP(List<DatosUsosCreditos> Usos)
        {
            foreach (var item in Usos)
            {
                ObjCreditosEcpContexto.GuardarUsosCreditoECP(Convert.ToInt32(item.IdUsoCredito), Convert.ToInt32(item.IdConsecutivo), Convert.ToDecimal(item.Valor), Convert.ToBoolean(item.Activo));
            }
        }

        public virtual void GuardarFormasPagoECP(List<DatosFormaPago> FormaPago)
        {
            foreach (var item in FormaPago)
            {
                ObjCreditosEcpContexto.GuardarFormasPagoECP(Convert.ToInt32(item.IdProductoNOrma), Convert.ToInt32(item.IdConsecutivo), Convert.ToInt32(item.Plazo), Convert.ToDecimal(item.Valor), Convert.ToDecimal(item.Cuota));
            }
        }

        public int GuardarSolicitudCreditoISYNETECP(List<DatosSolicitudISYNET> SolicitudISYNET)
        {
            RespuestaNegocio<object> respuesta = new RespuestaNegocio<object>();
            int numeroSolicitud = 0;
            try
            {
                Object result = ObjCreditosEcpContexto.GuardarSolicitudCreditoISYNETECP(
                                     Convert.ToInt32(SolicitudISYNET[0].IdConsecutivo),
                                     Convert.ToDecimal(SolicitudISYNET[0].MontoSolicitado),
                                     Convert.ToDecimal(SolicitudISYNET[0].MontoCheque),
                                     Convert.ToDecimal(SolicitudISYNET[0].MontoFAI),
                                     Convert.ToInt32(SolicitudISYNET[0].IdSolicitudFYC));
                respuesta.Estado = true;
                string strNumeroSolicitud = result.ToString();
                int.TryParse(strNumeroSolicitud, out numeroSolicitud);
                respuesta.Estado = true;

            }
            catch (Exception ex)
            {
                respuesta.Estado = false;
                respuesta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
            }
            return numeroSolicitud;
        }


        public IEnumerable<DatosHipoteca> ObtenerHipotecaECP(string nIdentificacion, string IdConsecutivo)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerHipotecaECP(nIdentificacion, IdConsecutivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<DatosGeneralesECP> ValidaNumSolicitudFYC(string Consecutivo)
        {
            try
            {
                return ObjCreditosEcpContexto.ValidaNumSolicitudFYC(Consecutivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void GuardarHipotecaECP(List<DatosHipoteca> DatosHipoteca)
        {
            foreach (var item in DatosHipoteca)
            {
                ObjCreditosEcpContexto.GuardarHipotecaECP(
                    item.HDireccion, Convert.ToDecimal(item.HAvaluoComercial), Convert.ToDecimal(item.H80AvaluoComercial), Convert.ToDecimal(item.HValorConstruccion), item.HAvaluador,
                    item.HIdentificacion, item.HMatriculaInmobiliaria, item.HCiudad, item.HNumeroEscritura, item.HNotaria, item.HFecha,
                    item.HCiudadNotaria, item.HClaseHipoteca, item.HFechaSeguros, item.HFechaCobroECP, item.HOtroCredito, item.HSeguroVida,
                    item.HSeguroIncendio, Convert.ToInt32(item.IdConsecutivo));
            }
        }

        public virtual void GuardarChequesECP(List<DatosCheque> DatosCheques)
        {
            foreach (var item in DatosCheques)
            {
                ObjCreditosEcpContexto.GuardarChequesECP(
                    item.Tipo_Registro, Convert.ToBoolean(item.Activo), Convert.ToBoolean(item.Habilitar_Eliminacion), item.Tipo_Id_Destinatario,
                    item.Id_Destinatario, item.Nombre_Destinatario, item.Tipo_Id_Quien_Recibe, item.Id_Quien_Recibe, item.Nombre_Quien_Recibe, item.Fecha_Desembolso,
                    Convert.ToDecimal(item.Monto), item.Fecha_Proceso, Convert.ToInt32(item.IdConsecutivo));
            }
        }
        /// <summary> EstudioTitulos
        /// RECIVE Y ENVIA LOS DATOS DEL TITULO A CREAR PARA PODER PERSISTIRLO
        /// </summary>
        public void CrearEstudioTitulo(EstudioTitulos InformacionTitulo)
        {
            try
            {
                ObjCreditosEcpContexto.CrearEstudioTitulo(InformacionTitulo);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        ///  OBTIENE LA INFORMACION DE UNO O VARIOS TITULOS SEGUN PARAMETROS
        /// </summary>
        public IEnumerable<ModeloCartas> ObtenerCartasECP(int cedula, int estado)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerCartasECP(cedula, estado).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// ACTUALIZA EL ESTADO DEL TITULO SEGUN PARAMETROS
        /// </summary>
        public void ActualizarEstudioTitulo(int concecutivo, int estado)
        {
            try
            {
                ObjCreditosEcpContexto.ActualizarEstudioTitulo(concecutivo, estado);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<EstudioTitulos> ObtenerInformacionEstudio(int concecutivo)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerInformacionEstudio(concecutivo);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //GUARDA LOS DOCUMENTOS QUE APLICA PARA EL ASOCIADO </jarp>
        public IEnumerable<DocumentosAsociadosCrViviendaEcopetrol> GuardarDocumentoAsociado(int IdUso, int IdDocumento, string NumeroIdentificacion, string Observacion)
        {
            try
            {
                return ObjCreditosEcpContexto.GuardarDocumentoAsociado(IdUso, IdDocumento, NumeroIdentificacion, Observacion);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //GUARDA LOS DOCUMENTOS QUE NO APLICA PARA EL ASOCIADO </jarp>
        public IEnumerable<DocumentosAsociadosCrViviendaEcopetrol> GuardarDocumentoNoAplicaAsociado(int IdUso, int IdDocumento, string NumeroIdentificacion, string Observacion)
        {
            try
            {
                return ObjCreditosEcpContexto.GuardarDocumentoNoAplicaAsociado(IdUso, IdDocumento, NumeroIdentificacion, Observacion);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public ObjectInt GuardarEnvioEstudio(CreditoEcopetrolEstudio EstudioModel)
        {
            try
            {
                return ObjCreditosEcpContexto.GuardarEnvioEstudio(EstudioModel).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<CreditoEcopetrolValorAdjudicacion> ObtenerInfoSolicitudCredito(string NumIdetificacion)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerInfoSolicitudCredito(NumIdetificacion);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<CreditoEcopetrolValorCheques> ObtenerValorTotalChequesGenerados(string Identificacion)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerValorTotalChequesGenerados(Identificacion);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<CreditoEcopetrolNotaContable> ObtenerDatosNotaContable(string NumeroIdentificacion, string Consecutivo)
        {
            try
            {
                return ObjCreditosEcpContexto.ObtenerDatosNotaContable(NumeroIdentificacion, Consecutivo);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
		public IEnumerable<ProcesoECP> ObtenerProcesoECP(int identificacion)
		{
			try
			{
				return ObjCreditosEcpContexto.ObtenerProcesoECP(identificacion).ToList();
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}

		public void ReiniciarProceso(int identificacion, int formulario)
		{
			try
			{
				ObjCreditosEcpContexto.ReiniciarProceso(identificacion, formulario);
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}

		public void AceptacionProcesoAdjudicacion(string tipoIdentificacion, string identificacion, string producto, string numeroSolicitud, string autorizacion)
		{
			try
			{
				ObjCreditosEcpContexto.AceptacionProcesoAdjudicacion(tipoIdentificacion, identificacion, producto, numeroSolicitud, autorizacion);
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}

		public IEnumerable<InformacionReporteECP> ObtenerInformacionReporteSolicitud(string identificacion, string concecutivo)
		{
			try
			{
				return ObjCreditosEcpContexto.ObtenerInformacionReporteSolicitud(identificacion, concecutivo).ToList();
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}

		public IEnumerable<FormasPagoECP> ObtenerFormasPagoECP(string identificacion, string concecutivo)
		{
			try
			{
				return ObjCreditosEcpContexto.ObtenerFormasPagoECP(identificacion, concecutivo).ToList();
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}

        public string GestionPagare(CreditoEcopetrolPagare PagareModel)
        {
            try
            {
                return ObjCreditosEcpContexto.GestionPagare(PagareModel).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<CreditoEcopetrolOrdenGiro> ConsultarProgramacionGiros(string FechaInicial, string FechaFinal)
        {
            try
            {
                return ObjCreditosEcpContexto.ConsultarProgramacionGiros(FechaInicial, FechaFinal).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string GenerarNumeroOrgen(CreditoEcopetrolCreacionOrden OrgenGiroModel)
        {
            try
            {
                return ObjCreditosEcpContexto.GenerarNumeroOrgen(OrgenGiroModel).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<CreditoEcopetrolReferenciaPago> ConsultaReferenciaPagos()
        {
            try
            {
                return ObjCreditosEcpContexto.ConsultaReferenciaPagos().ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

}
