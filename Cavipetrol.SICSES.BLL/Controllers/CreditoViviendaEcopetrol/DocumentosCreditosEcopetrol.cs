﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cavipetrol.SICSES.DAL;
using Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP;

namespace Cavipetrol.SICSES.BLL.Controllers.CreditoViviendaEcopetrol
{
    public class DocumentosCreditosEcopetrol
    {
        AdministracionContexto ObjAdministracionContexto = new AdministracionContexto();
        //OBTIENE LOS DATOS DE LOS DOCUMENTOS </jarp>
        public IEnumerable<DocumentosCreditosEcopetrolObtener> ObtenerDocumentosCreditoECP()
        {
            try
            {
                return ObjAdministracionContexto.ObtenerDocumentosCreditoECP().ToList();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool GuardarNuevoDocumento(DocumentosCreditosEcopetrolGuardar Descripcion)
        {
            try
            {
                ObjAdministracionContexto.GuardarNuevoDocumento(Descripcion);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        //ACTUALIZA LOS DATOS DEL DOCUMENTO DE CREDITO </jarp>
        public bool ActualizarDocumento(DocumentosCreditosEcopetrolObtener Descripcion)
        {
            try
            {
                ObjAdministracionContexto.ActualizarDocumento(Descripcion);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public bool GuardarRelacionUsoDocumento(UsosDocumentosCreditosEcopoetrolGuardar Obj)
        {
            try
            {
                ObjAdministracionContexto.GuardarRelacionUsoDocumento(Obj);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        //OBTIENE LOS DATOS DE LAS RELACIONES USOS - DOCUMENTOS </jarp>
        public IEnumerable<UsosDocumentosCreditosEcopoetrolObtener> ObtenerUsoDocumentoCreditoECP()
        {
            try
            {
                return ObjAdministracionContexto.ObtenerUsoDocumentoCreditoECP().ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //ACTUALIZA EL ESTADO DEL USO  DOCUMENTO DE CREDITO </jarp>
        public bool ActualizarEstadoUsoDocumentoHabilitar(UsosDocumentosCreditosEcopoetrolObtener Obj)
        {
            try
            {
                ObjAdministracionContexto.ActualizarEstadoUsoDocumentoHabilitar(Obj);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool ActualizarEstadoUsoDocumentoDeshabilitar(UsosDocumentosCreditosEcopoetrolObtener Obj)
        {
            try
            {
                ObjAdministracionContexto.ActualizarEstadoUsoDocumentoDeshabilitar(Obj);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool ActualizarUsoDocumento(UsosDocumentosCreditosEcopoetrolObtener Obj)
        {
            try
            {
                ObjAdministracionContexto.ActualizarUsoDocumento(Obj);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //OBTIENE LOS DATOS DE LAS RELACIONES USOS - DOCUMENTOS </jarp>
        public IEnumerable<UsosDocumentosCreditosEcopoetrolObtener> ObtenerDocumentoxUsoECP(int IdUso)
        {
            try
            {
                return ObjAdministracionContexto.ObtenerDocumentoxUsoECP(IdUso).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
