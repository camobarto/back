﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cavipetrol.SICSES.DAL;
using Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;

namespace Cavipetrol.SICSES.BLL.Controllers.CreditoViviendaEcopetrol
{
    public class AvaluosCreditoEcopetrol
    {
        SICAVContexto ObjSICAVContexto = new SICAVContexto();

        public IEnumerable<AvaluosCrViviendaEcopetrolObtener> ConsultaSolicitudesAvaluos(AvaluosCrViviendaEcopetrolObtener Obj)
        {
            try
            {
                return ObjSICAVContexto.ConsultaSolicitudesAvaluos(Obj).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<AvaluosCrViviendaEcopetrol> ConsultaInformacionSolicitudAvaluos(AvaluosCrViviendaEcopetrol Obj)
        {
            try
            {
                return ObjSICAVContexto.ConsultaInformacionSolicitudAvaluos(Obj).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public virtual void FinalizarProceso(string PI_Id_Formulario, string PI_Numero_de_Identificacion, string PI_Comodin, string PO_Url, string PO_Bandera, string PO_Proceso)
        {
            try
            {
                ObjSICAVContexto.FinalizarProceso(PI_Id_Formulario, PI_Numero_de_Identificacion, PI_Comodin, PO_Url, PO_Bandera, PO_Proceso);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<LogProcesosCrViviendaEcopetrol> GuardarLogAprobacionProceso(LogProcesosCrViviendaEcopetrol Obj)
        {
            try
            {
                return ObjSICAVContexto.GuardarLogAprobacionProceso(Obj);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// guarda el log de los avaluos
        /// </summary>
        public void GuardarLogAvaluos(int IdConsecutivo, string NitAfiliado, int NumServicio, string UsuarioSolicitud)
        {
            try
            {
                ObjSICAVContexto.GuardarLogAvaluos(IdConsecutivo, NitAfiliado, NumServicio, UsuarioSolicitud);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual int ObtenerNumSolicitudFechaAvaluo(string NitAfiliado)
        {
            try
            {
                return ObjSICAVContexto.ObtenerNumSolicitudFechaAvaluo(NitAfiliado).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
