﻿using Cavipetrol.SICSES.DAL;
using Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Cavipetrol.SICSES.BLL.Controllers.CreditoViviendaEcopetrol
{
    public class UsosCreditosEcopetrol
    {
        AdministracionContexto ObjAdministracionContexto = new AdministracionContexto();


        //GUARDA LOS DATOS DEL  NUEVO USO DE CREDITO </jarp>
        public bool GuardarNuevoUso(UsosCreditosEcopetrolGuardar Descripcion)
        {
            try
            {
                ObjAdministracionContexto.GuardarNuevoUso(Descripcion);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        //ACTUALIZA LOS DATOS DEL USO DE CREDITO </jarp>
        public bool ActualizarUso(UsosCreditosEcopetrolObtener Descripcion)
        {
            try
            {
                ObjAdministracionContexto.ActualizarUso(Descripcion);
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        //OBTIENE LOS DATOS DE LOS USOS </jarp>
        public IEnumerable<UsosCreditosEcopetrolObtener> ObtenerUsosCreditoECP()
        {
            try
            {
                return ObjAdministracionContexto.ObtenerUsosCreditoECP();
                
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
