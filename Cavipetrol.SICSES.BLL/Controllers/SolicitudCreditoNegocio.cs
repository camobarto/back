﻿using Cavipetrol.SICSES.BLL.Controllers.SolicitudCreditosValidaciones;
using Cavipetrol.SICSES.BLL.Mensajes;
using Cavipetrol.SICSES.DAL;
using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using Cavipetrol.SICSES.Infraestructura.Model.PreaprobacionVehiculo;
using Cavipetrol.SICSES.Infraestructura.Model.Garantias;
using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System.Collections.Generic;
using System.Linq;
using System;
using Cavipetrol.SICSES.Infraestructura.Model;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.Creditos;
using Microsoft.VisualBasic;
using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.BLL.Controllers.SolicitudCredito;
using Cavipetrol.SICSES.Infraestructura.Historico;
using static Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones.Procesos;
using System.Web.Script.Serialization;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Newtonsoft.Json;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Sicav;
using Cavipetrol.SICSES.Infraestructura.Model.fyc;
using Cavipetrol.SICSES.Infraestructura.Model.Reportes;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class SolicitudCreditoNegocio
    {
        private readonly ICreditosUnidadTrabajo _creditosUnidadTrabajo;
        private readonly IConsultasApoyoUnidadTrabajo _consultasApoyoUnidadTrabajo;
        private readonly ISicavUnidadTrabajo _SicavUnidadTrabajo;
        private readonly IHipotecasUnidadTrabajo _HipotecasUnidadTrabajo;
        private readonly CreditosContexto _contexto;
        private readonly SICAVContexto _contextoSICAV;

        private MensajesValidaciones mensajes;
        public SolicitudCreditoNegocio()
        {
            mensajes = new MensajesValidaciones();
            _creditosUnidadTrabajo = new CreditosUnidadTrabajo();
            _consultasApoyoUnidadTrabajo = new ConsultasApoyoUnidadTrabajo();
            _SicavUnidadTrabajo = new SicavUnidadTrabajo();
            _HipotecasUnidadTrabajo = new HipotecasUnidadTrabajo();
            _contexto = new CreditosContexto();
            _contextoSICAV = new SICAVContexto();
        }
        public IEnumerable<PreaprobadoCarta> GuardarDatosPreaprobacion(PreaprobadoCarta datos)
        {            
            return _contextoSICAV.GuardarDatosPreaprobacion(datos);
        }
        public IEnumerable<Solicitud> AdministrarPagares(PagaresModel datos)
        {
            return _contextoSICAV.AdministrarPagares(datos);
        }
        public IEnumerable<CreditoYPagares> GestionNormalizado(NormalizadoConsulta datos)
        {
            return _contextoSICAV.GestionNormalizado(datos);
        }
        public PrendaVehicular[] GestionPrendaVehicular(PrendaVehicular[] datos)
        {
            foreach (PrendaVehicular element in datos)
            {
                _contextoSICAV.Entry(element).State = System.Data.Entity.EntityState.Added;
            }
            
         
            if (_contextoSICAV.SaveChanges() > 0)
            {
                return datos;
            }
            else
            {
                return null;
            }
        }
        public RespuestaNegocio<InformacionSolicitanteCampanaEspecial> ObtenerDatosCampaniaVehiculo(string numeroIdentificacion, string tipoIdentificacion, int TipoVeh)
        {
            
            RespuestaNegocio<InformacionSolicitanteCampanaEspecial> informacionSolicitante = new RespuestaNegocio<InformacionSolicitanteCampanaEspecial>();
            
            informacionSolicitante.Respuesta = _contextoSICAV.ObtenerDatosCampaniaVehiculo(numeroIdentificacion, tipoIdentificacion, TipoVeh).FirstOrDefault();
            if (informacionSolicitante.Respuesta == null)
            {
                informacionSolicitante.MensajesError.Add(mensajes.NO_EXISTE_USUARIO.Replace("$1", "Asociado o Empleado"));
                informacionSolicitante.Estado = false;
                return informacionSolicitante;
            }
            else
                informacionSolicitante.Estado = true;

            return informacionSolicitante;
        }
        /*Reporte Saldos Diarios*/
        public IEnumerable<ReportesSaldos> ReporteSaldosDiarios(string selectedFechaInicio, string selectedFechaFin)
        {
            
            return _contexto.ReporteSaldosDiarios(selectedFechaInicio, selectedFechaFin);
        }
        public RespuestaNegocio<InformacionSolicitante> ObtenerInformacionEmpleado(string numeroIdentificacion, string tipoIdentificacion)
        {
            RespuestaNegocio<InformacionSolicitante> informacionEmpleado = new RespuestaNegocio<InformacionSolicitante>();
         
            informacionEmpleado.Respuesta = _creditosUnidadTrabajo.ObtenerInformacionEmpleado(numeroIdentificacion, tipoIdentificacion);
            if (informacionEmpleado.Respuesta == null)
            {
                informacionEmpleado.MensajesError.Add(mensajes.NO_EXISTE_USUARIO.Replace("$1", "Asociado o Empleado"));
                informacionEmpleado.Estado = false;
                return informacionEmpleado;
            }

            var validacionesInhabilidadCredito = ValidarReglasDeNegocioSolicitudCredito(numeroIdentificacion, tipoIdentificacion);
            if (!validacionesInhabilidadCredito.Estado)
            {
                informacionEmpleado.MensajesError = validacionesInhabilidadCredito.MensajesError;
                return informacionEmpleado;
            }

            informacionEmpleado.Estado = true;
            return informacionEmpleado;
        }

        public RespuestaNegocio<InformacionSolicitanteCampanaEspecial> ObtenerInformacionSolicitanteCampanaEspecial(string numeroIdentificacion, string tipoIdentificacion)
        {
            RespuestaNegocio<InformacionSolicitanteCampanaEspecial> informacionSolicitante = new RespuestaNegocio<InformacionSolicitanteCampanaEspecial>();
            if (string.IsNullOrEmpty(numeroIdentificacion) || string.IsNullOrEmpty(tipoIdentificacion))
            {
                informacionSolicitante.MensajesError.Add(mensajes.VALIDACION_NUMID_VACIO);
                return informacionSolicitante;
            }

            informacionSolicitante.Respuesta = _creditosUnidadTrabajo.ObtenerInformacionSolicitanteCampanaEspecial(numeroIdentificacion, tipoIdentificacion);
            if (informacionSolicitante.Respuesta == null)
            {
                informacionSolicitante.MensajesError.Add(mensajes.NO_EXISTE_USUARIO.Replace("$1", "Asociado o Empleado"));
                informacionSolicitante.Estado = false;
                return informacionSolicitante;
            } else            
                informacionSolicitante.Estado = true;

            return informacionSolicitante;
        }



        public RespuestaNegocio<ValidacionesSolicitudCreditoProducto> ValidacionesReestriccionProductos(string numeroIdentificacion, string tipoIdentificacion, string papel, IReglasSolicitudCreditoProductos reglaSolicitudNegocioCreditos)
        {
            var validacionesProducto = reglaSolicitudNegocioCreditos.ValidacionRestriccionesProducto(numeroIdentificacion, tipoIdentificacion, papel, _creditosUnidadTrabajo);
            if (validacionesProducto.Estado)
            {
                var validacionesCaviAlDia = reglaSolicitudNegocioCreditos.ValidacionProductoCaviAlDia(numeroIdentificacion, tipoIdentificacion, papel, _creditosUnidadTrabajo);
                if (validacionesCaviAlDia.Estado)
                {
                    validacionesProducto.Respuesta.CumpleCaviAlDia = true;
                }
                else
                {
                    validacionesProducto.Estado = false;
                    validacionesProducto.MensajesError.AddRange(validacionesCaviAlDia.MensajesError);
                }
            }
            return validacionesProducto;
        }

        public RespuestaNegocio<InformacionAsegurabilidad> ObtenerInformacionAsegurabilidad(string numeroIdentificacion, string tipoIdentificacion)
        {
            RespuestaNegocio<InformacionAsegurabilidad> informacionAsegurabilidad = new RespuestaNegocio<InformacionAsegurabilidad>();
            if (string.IsNullOrEmpty(numeroIdentificacion) || string.IsNullOrEmpty(tipoIdentificacion))
            {
                informacionAsegurabilidad.MensajesError.Add("El número de documento o tipo documento están vacios, por favor introducir un valor");
                return informacionAsegurabilidad;
            }

            informacionAsegurabilidad.Respuesta = _creditosUnidadTrabajo.ObtenerInformacionAsegurabilidad(numeroIdentificacion, tipoIdentificacion);
            if (informacionAsegurabilidad.Respuesta == null)
            {
                informacionAsegurabilidad.MensajesError.Add("No se encontro ningún usuario con el número de documento ingresado");
                return informacionAsegurabilidad;
            }

            informacionAsegurabilidad.Respuesta.ValidarInformacionDeAsegurabilidad();
            informacionAsegurabilidad.Estado = true;
            return informacionAsegurabilidad;
        }

        private RespuestaNegocio<ValidacionesSolicitudCreditoUsuario> ValidarReglasDeNegocioSolicitudCredito(string numeroIdentificacion, string tipoIdentificacion)
        {
            RespuestaNegocio<ValidacionesSolicitudCreditoUsuario> respuestaValidacion = new RespuestaNegocio<ValidacionesSolicitudCreditoUsuario>();
            respuestaValidacion.Respuesta = _creditosUnidadTrabajo.ObternerValidacionesSolicitudCredito(numeroIdentificacion, tipoIdentificacion);
            respuestaValidacion.Estado = true;

            if (respuestaValidacion.Respuesta.EstaEnListaClinton)
            {
                respuestaValidacion.MensajesError.Add(mensajes.LISTA_CLINTON.Replace("$1", "empleado") + " " + " El asociado esta en lista Clinton.");
            }
            if (respuestaValidacion.Respuesta.EstaEnListaITP)
            {
                respuestaValidacion.MensajesError.Add(mensajes.LISTA_ITP.Replace("$1", "empleado") + " El asociado esta en lista ITP.");
            }
            if (respuestaValidacion.Respuesta.TieneEmbargo)
            {
                respuestaValidacion.MensajesError.Add(mensajes.TIENE_EMBARGO.Replace("$1", "empleado") + " " + " El asociado Tiene embargo.");
            }

            if (respuestaValidacion.MensajesError.Count > 0)
                respuestaValidacion.Estado = true;

            return respuestaValidacion;
        }
        public IEnumerable<SaldosColocacion>ObtenerCreditosDisponiblesSaldo(string numeroIdentificacion, string tipoIdentificacion, string papel)
        {
            var listaProductos = _creditosUnidadTrabajo.ObtenerCreditosDisponiblesSaldo(numeroIdentificacion, tipoIdentificacion, papel);
            return listaProductos;
        }
        

        public IEnumerable<NormaProductos> ObtenerNormas(string strProducto, string strPapel, short idTipoCredito)
        {
            int idProducto = 0;
            short idPapel = 0;
            int.TryParse(strProducto, out idProducto);
            short.TryParse(strPapel, out idPapel);
            var producto = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposRolRelacion(0, idProducto).FirstOrDefault();
            var papel = _consultasApoyoUnidadTrabajo.ObtenerPapelCavipetrol(idPapel);
            //var papelDescripcion = _consultasApoyoUnidadTrabajo.ObtenerPapelCavipetrol(tipoCreditoTipoRolRelacion.IdPapelCavipetrol);

            return _creditosUnidadTrabajo.ObtenerInformacionNormas((producto.IdProductoFYC ?? string.Empty), papel.Nombre, producto.IdTipoCredito);
        }

        public Modalidades ObtenerModalidades(string producto, string norma)
        {
            NormaProductos Norma = new NormaProductos();
            Modalidades Modalidades = new Modalidades();
            ModalidadesMontoValorCuota ModalidadesMontoValorCuota = new ModalidadesMontoValorCuota();
            ModalidadesClavePlazo ModalidadesClave = new ModalidadesClavePlazo();
            ModalidadesClavePlazo ModalidadesPlazo = new ModalidadesClavePlazo();
            Norma = _creditosUnidadTrabajo.ObtenerInformacionDetalleNorma(producto, norma);

            Modalidades.PlazoMinimoAnhos = Norma.PlazoMinimo;
            Modalidades.PlazoMaximoAnhos = Norma.PlazoMaximo;
            Modalidades.MontoMaximo = Norma.MontoMinimo;
            Modalidades.MontoMaximo = Norma.MontoMaximo;
            Modalidades.Tasa = Norma.Tasa;

            Modalidades.ModalidadesClavePlazo.Add(ModalidadesClave);
            Modalidades.ModalidadesClavePlazo.Add(ModalidadesPlazo);

            for (int i = 0; i < 2; i++)
            {
                Modalidades.ModalidadesMontoValorCuota.Add(ModalidadesMontoValorCuota);
            }

            foreach (GrupoVencimientoNorma Item in Norma.Vencimientos)
            {
                switch (Item.Equivalencia)
                {
                    case "Quincenal":
                        Modalidades.TipoModalidades.EsQuincenal = true;
                        Modalidades.ModalidadesClavePlazo[0].Quincenal = Item.Clave;
                        break;
                    case "Mensual":
                        Modalidades.TipoModalidades.EsMensual = true;
                        Modalidades.ModalidadesClavePlazo[0].Mensual = Item.Clave;
                        break;
                    case "Trimestral":
                        Modalidades.TipoModalidades.EsTrimestral = true;
                        Modalidades.ModalidadesClavePlazo[0].Trimestral = Item.Clave;
                        break;
                    case "Semestral":
                        Modalidades.TipoModalidades.EsSemestral = true;
                        Modalidades.ModalidadesClavePlazo[0].Semestral = Item.Clave;
                        break;
                    case "Anual":
                        Modalidades.TipoModalidades.EsAnual = true;
                        Modalidades.ModalidadesClavePlazo[0].Anual = Item.Clave;
                        break;
                    default:
                        break;
                }


            }
            return Modalidades;
        }
        //public CapacidadPago ObtenerCapacidadPago(string numeroIdentificacion, string tipoIdentificacion, string tipoCredito)
        //{

        //    CapacidadPago CapacidadPago = new CapacidadPago();
        //    CuotasProductosCavipetrol producto1 = new CuotasProductosCavipetrol();
        //    CuotasProductosCavipetrol producto2 = new CuotasProductosCavipetrol();

        //    producto1.Producto = "Producto 1";
        //    producto1.Cuota = 100000F;

        //    producto2.Producto = "Producto 2";
        //    producto2.Cuota = 50000F;

        //    CapacidadPago.CuotasProductosCavipetrol.Add(producto1);
        //    CapacidadPago.CuotasProductosCavipetrol.Add(producto2);


        //    return CapacidadPago;

        //}
        public RespuestaNegocio<FormCapacidadPagoGruposAmortizacion> CalcularCapacidadPago(List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos,
                                                                                           string idTipoCreditoTipoRolRelacion,
                                                                                           ref List<Amortizacion> tablaAmortizacion,
                                                                                           double valorTotalInmueble,
                                                                                           short idTipoPoliza,
                                                                                           double valorNegociado,
                                                                                           byte edad,
                                                                                           double PorcenExtraPrima,
                                                                                           string tipoCapacidadPago)

        {
            RespuestaNegocio<FormCapacidadPagoGruposAmortizacion> respueta = new RespuestaNegocio<FormCapacidadPagoGruposAmortizacion>();
            FormCapacidadPagoGruposAmortizacion formCapacidadPagoGrupoAmortizacion = new FormCapacidadPagoGruposAmortizacion();
            int IdtipoCreditoTipoRolRelacion = 0;
            int.TryParse(idTipoCreditoTipoRolRelacion, out IdtipoCreditoTipoRolRelacion);

            /// CODEREVIEW Revisar porque se consultan todos los datos de la table
            IEnumerable<TipoCreditoTipoRolRelacion> listaTiposCreditoTiposRolRelacion = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposRolRelacion(0,IdtipoCreditoTipoRolRelacion);
            TipoCreditoTipoRolRelacion tiposCreditoTiposRolRelacion = listaTiposCreditoTiposRolRelacion
                                                                                    .Where(x => x.IdTipoCreditoTipoRolRelacion == IdtipoCreditoTipoRolRelacion)
                                                                                    .SingleOrDefault();

            List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion = new List<TiposCreditoTipoSeguroRelacion>();
            listTipoCreditoTipoSeguroRelacion = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposSeguroRelacion(tiposCreditoTiposRolRelacion.IdTipoCredito).ToList();

            List<TiposCreditoTiposGarantiaRelacion> listaTiposCreditoTiposGarantiaRelacion = new List<TiposCreditoTiposGarantiaRelacion>();
            listaTiposCreditoTiposGarantiaRelacion = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposGarantiaRelacion(tiposCreditoTiposRolRelacion.IdTipoCredito).ToList();
            var tasa = _consultasApoyoUnidadTrabajo.ObtenerSegurosTasas(idTipoSeguro: (int)enumTipoSeguro.Seguro_de_vida, edad: edad).FirstOrDefault();
            double tasaMensualSeguroVida = tasa == null ? 1 : tasa.TasaMensual;

            switch (tiposCreditoTiposRolRelacion.IdTipoCredito)
            {
                case (int)enumTiposCredito.Vivienda:
                case (int)enumTiposCredito.CompraCarteraVivienda:
                    formCapacidadPagoGrupoAmortizacion.ListaFormCapacidadPagoGrupos = CalcularCreditoVivienda(listaFormCapacidadPagoGrupos, ref tablaAmortizacion, IdtipoCreditoTipoRolRelacion, listTipoCreditoTipoSeguroRelacion, valorTotalInmueble: valorTotalInmueble, idTipoPoliza: idTipoPoliza, valorNegociado: (double)valorNegociado, tasaMensualSeguroVida: tasaMensualSeguroVida, PorcenExtraPrima: PorcenExtraPrima, idPapelCavipetrol: tiposCreditoTiposRolRelacion.IdPapelCavipetrol, tipoCapacidadPago: tipoCapacidadPago, edad: edad);
                    formCapacidadPagoGrupoAmortizacion.TablaAmortizacion = tablaAmortizacion;
                    formCapacidadPagoGrupoAmortizacion.TablaGarantias = listaTiposCreditoTiposGarantiaRelacion;
                    respueta.Respuesta = formCapacidadPagoGrupoAmortizacion;
                    respueta.Estado = true;
                    break;
                case (int)enumTiposCredito.Consumo:
                case (int)enumTiposCredito.CompraCarteraConsumo:
                    formCapacidadPagoGrupoAmortizacion.ListaFormCapacidadPagoGrupos = CalcularCreditoConsumo(listaFormCapacidadPagoGrupos, ref tablaAmortizacion, IdtipoCreditoTipoRolRelacion, listTipoCreditoTipoSeguroRelacion, idTipoPoliza, (double)valorNegociado, tasaMensualSeguroVida: tasaMensualSeguroVida, PorcenExtraPrima: PorcenExtraPrima, idPapelCavipetrol: tiposCreditoTiposRolRelacion.IdPapelCavipetrol, tipoCapacidadPago: tipoCapacidadPago, edad: edad);
                    formCapacidadPagoGrupoAmortizacion.TablaAmortizacion = tablaAmortizacion;
                    formCapacidadPagoGrupoAmortizacion.TablaGarantias = listaTiposCreditoTiposGarantiaRelacion;
                    respueta.Respuesta = formCapacidadPagoGrupoAmortizacion;
                    respueta.Estado = true;
                    break;
                case (int)enumTiposCredito.Libre:
                    formCapacidadPagoGrupoAmortizacion.ListaFormCapacidadPagoGrupos = CalcularCreditoConsumo(listaFormCapacidadPagoGrupos, ref tablaAmortizacion, IdtipoCreditoTipoRolRelacion, listTipoCreditoTipoSeguroRelacion, idTipoPoliza, (double)valorNegociado, tasaMensualSeguroVida: tasaMensualSeguroVida, PorcenExtraPrima: PorcenExtraPrima, idPapelCavipetrol: tiposCreditoTiposRolRelacion.IdPapelCavipetrol, tipoCapacidadPago: tipoCapacidadPago, edad: edad);
                    formCapacidadPagoGrupoAmortizacion.TablaAmortizacion = tablaAmortizacion;
                    formCapacidadPagoGrupoAmortizacion.TablaGarantias = listaTiposCreditoTiposGarantiaRelacion;
                    respueta.Respuesta = formCapacidadPagoGrupoAmortizacion;
                    respueta.Estado = true;
                    break;
                case (int)enumTiposCredito.ViviendaOrdinario:
                    formCapacidadPagoGrupoAmortizacion.ListaFormCapacidadPagoGrupos = CalcularCreditoVivienda(listaFormCapacidadPagoGrupos, ref tablaAmortizacion, IdtipoCreditoTipoRolRelacion, listTipoCreditoTipoSeguroRelacion, valorTotalInmueble: valorTotalInmueble, idTipoPoliza: idTipoPoliza, valorNegociado: (double)valorNegociado, tasaMensualSeguroVida: tasaMensualSeguroVida, PorcenExtraPrima: PorcenExtraPrima, idPapelCavipetrol: tiposCreditoTiposRolRelacion.IdPapelCavipetrol, tipoCapacidadPago: tipoCapacidadPago, edad: edad);
                    formCapacidadPagoGrupoAmortizacion.TablaAmortizacion = tablaAmortizacion;
                    formCapacidadPagoGrupoAmortizacion.TablaGarantias = listaTiposCreditoTiposGarantiaRelacion;
                    respueta.Respuesta = formCapacidadPagoGrupoAmortizacion;
                    respueta.Estado = true;
                    break;
                case (int)enumTiposCredito.ViviendaAdicional:
                    formCapacidadPagoGrupoAmortizacion.ListaFormCapacidadPagoGrupos = CalcularCreditoVivienda(listaFormCapacidadPagoGrupos, ref tablaAmortizacion, IdtipoCreditoTipoRolRelacion, listTipoCreditoTipoSeguroRelacion, valorTotalInmueble: valorTotalInmueble, idTipoPoliza: idTipoPoliza, valorNegociado: (double)valorNegociado, tasaMensualSeguroVida: tasaMensualSeguroVida, PorcenExtraPrima: PorcenExtraPrima, idPapelCavipetrol: tiposCreditoTiposRolRelacion.IdPapelCavipetrol, tipoCapacidadPago: tipoCapacidadPago, edad: edad);
                    formCapacidadPagoGrupoAmortizacion.TablaAmortizacion = tablaAmortizacion;
                    formCapacidadPagoGrupoAmortizacion.TablaGarantias = listaTiposCreditoTiposGarantiaRelacion;
                    respueta.Respuesta = formCapacidadPagoGrupoAmortizacion;
                    respueta.Estado = true;
                    break;
                default:
                    formCapacidadPagoGrupoAmortizacion.ListaFormCapacidadPagoGrupos = CalcularCreditoConsumo(listaFormCapacidadPagoGrupos, ref tablaAmortizacion, IdtipoCreditoTipoRolRelacion, listTipoCreditoTipoSeguroRelacion, idTipoPoliza, (double)valorNegociado, tasaMensualSeguroVida: tasaMensualSeguroVida, PorcenExtraPrima: PorcenExtraPrima, idPapelCavipetrol: tiposCreditoTiposRolRelacion.IdPapelCavipetrol, tipoCapacidadPago: tipoCapacidadPago, edad: edad);
                    formCapacidadPagoGrupoAmortizacion.TablaAmortizacion = tablaAmortizacion;
                    formCapacidadPagoGrupoAmortizacion.TablaGarantias = listaTiposCreditoTiposGarantiaRelacion;
                    respueta.Respuesta = formCapacidadPagoGrupoAmortizacion;
                    respueta.Estado = true;
                    break;
            }

            return respueta;
        }
        protected List<FormCapacidadPagoGrupos> CalcularCreditoConsumo(List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos
                                                                      , ref List<Amortizacion> tablaAmortizacion
                                                                      , int idTipoCreditoTipoRolRelacion
                                                                      , List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion
                                                                      , short idTipoPoliza = 0
                                                                      , double? valorNegociado = null
                                                                      , double tasaMensualSeguroVida = 0
                                                                      , double PorcenExtraPrima = 0
                                                                      , int idPapelCavipetrol = 0
                                                                      , string tipoCapacidadPago = null
                                                                      , int edad = 0,
            int idTipoGarantia = 0)
        {
            CapacidadPagoCalculoCreditoAbstracta calculoLinea = new CapacidadPagoCalculoCreditoLineaConsumo();

            calculoLinea.Calcular(ref listaFormCapacidadPagoGrupos, ref tablaAmortizacion, idTipoCreditoTipoRolRelacion, listTipoCreditoTipoSeguroRelacion, idTipoPoliza: idTipoPoliza, valorNegociado: (double)valorNegociado, valorTotalInmueble: 0, tasaMensualSeguroVida: tasaMensualSeguroVida, PorcenExtraPrima: PorcenExtraPrima, idPapelCavipetrol: idPapelCavipetrol, tipoCapacidadPago: tipoCapacidadPago, edad: edad, idTipoGarantia : idTipoGarantia);
            return listaFormCapacidadPagoGrupos;
        }
        protected List<FormCapacidadPagoGrupos> CalcularCreditoVivienda(List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos,
                                                                        ref List<Amortizacion> tablaAmortizacion,
                                                                        int idTipoCreditoTipoRolRelacion,
                                                                        List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion,
                                                                        double valorTotalInmueble,
                                                                        short idTipoPoliza,
                                                                        double valorNegociado,
                                                                        double tasaMensualSeguroVida,
                                                                        double PorcenExtraPrima = 0,
                                                                        int idPapelCavipetrol = 0,
                                                                        string tipoCapacidadPago = null,
                                                                        int edad = 0,
                                                                        int idTipoGarantia = 0)
        {
            CapacidadPagoCalculoCreditoAbstracta calculoLinea = new CapacidadPagoCalculoCreditoLineaVivienda();

            calculoLinea.Calcular(ref listaFormCapacidadPagoGrupos, ref tablaAmortizacion, idTipoCreditoTipoRolRelacion, listTipoCreditoTipoSeguroRelacion, valorTotalInmueble, idTipoPoliza, valorNegociado, tasaMensualSeguroVida, PorcenExtraPrima, idPapelCavipetrol, tipoCapacidadPago, edad, idTipoGarantia);
            return listaFormCapacidadPagoGrupos;
        }

        public RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago> GuardarCapacidadPago(Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago)
        {
            RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago> respuesta = new RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago>();
            try
            {
                respuesta.Respuesta = _SicavUnidadTrabajo.GuardarCapacidadPago(capacidadPago);
                respuesta.Estado = true;
            }
            catch (Exception ex)
            {
                respuesta.MensajesError.Add(ex.InnerException.Message ?? ex.Message);
                respuesta.Estado = false;
            }
            return respuesta;
        }

        public RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> GuardarSolicitudCredito(Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito solicitudCredito,
                                                                                                                        Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago,
                                                                                                                        Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras horasExtras,
                                                                                                                        List<FormaPago> listaFormaPago,
                                                                                                                        List<FormCapacidadPagoCodeudoresGrupos> listaFormCapacidadPagoCodeudoresGrupos,
                                                                                                                         List<FormCapacidadPagoTerceroCodeudor> listaFormCapacidadPagoTerceroCodeudor)
        {

            RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> respuesta = new RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito>();
            try
            {
                List<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCreditoFormaPago> listaSolicitudFormaPago = new List<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCreditoFormaPago>();
                listaSolicitudFormaPago = listaFormaPago.Select(post => new Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCreditoFormaPago
                {
                    IdFormaPago = post.IdFormaPago,
                    Plazo = post.Plazo,
                    Valor = post.Valor,
                    Activo = true
                }).ToList();

                respuesta = _SicavUnidadTrabajo.GuardarSolicitudCredito(solicitudCredito, capacidadPago, horasExtras, listaSolicitudFormaPago, listaFormCapacidadPagoCodeudoresGrupos, listaFormCapacidadPagoTerceroCodeudor);
                respuesta.Estado = respuesta.Estado == false ? false : true;
            }
            catch (Exception ex)
            {
                respuesta.MensajesError.Add(ex.InnerException.Message ?? ex.Message);
                respuesta.Estado = false;
            }
            return respuesta;
        }
        public RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras> GuardarCapacidadPagoHorasExtras(Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras horasExtras)
        {
            RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras> respuesta = new RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras>();
            try
            {
                respuesta.Respuesta = _SicavUnidadTrabajo.GuardarCapacidadPagoHorasExtras(horasExtras);
                respuesta.Estado = true;
            }
            catch (Exception ex)
            {
                respuesta.MensajesError.Add(ex.InnerException.Message ?? ex.Message);
                respuesta.Estado = false;
            }
            return respuesta;
        }
        public RespuestaNegocio<IEnumerable<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras>> ConsultarCapacidadPagoHorasExtras(int idCapacidadPago = 0)
        {
            RespuestaNegocio<IEnumerable<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras>> respuesta = new RespuestaNegocio<IEnumerable<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras>>();
            IEnumerable<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras> listaCapacidadPagoHorasExtra = new List<Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPagoHorasExtras>();
            try
            {
                respuesta.Respuesta = _SicavUnidadTrabajo.ConsultarCapacidadPagoHorasExtras(idCapacidadPago);
                respuesta.Estado = true;
            }
            catch (Exception ex)
            {
                respuesta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                respuesta.Estado = false;
            }
            return respuesta;
        }
        public RespuestaNegocio<object> GuardarAsociado(GuardarInformacionSolicitante guardarInformacionSolicitante)
        {
            RespuestaNegocio<object> respuesta = new RespuestaNegocio<object>();
            try
            {
                JavaScriptSerializer serializador = new JavaScriptSerializer();
                var hoy = DateTime.Now;
                InformacionSolicitante infoSolicitanteOld = _creditosUnidadTrabajo.ObtenerInformacionEmpleado(guardarInformacionSolicitante.nitAfiliado, guardarInformacionSolicitante.tipoIdentificacion);
                Historico historico = new Historico
                {
                    Datos = serializador.Serialize(infoSolicitanteOld),
                    IdMenu = 3,
                    IdProceso = (int)enumProcesos.Actualizacion_de_asociados_o_empleados,
                    AudFechaCreacion = hoy,
                    AudFechaModificacion = hoy,
                    AudUsuarioCreacion = guardarInformacionSolicitante.usuario,
                    AudUsuarioModificacion = guardarInformacionSolicitante.usuario
                };
                _SicavUnidadTrabajo.GuardarHistorico(historico);

                _creditosUnidadTrabajo.GuardarAsociado(guardarInformacionSolicitante);
                respuesta.Estado = true;
            }
            catch (Exception ex)
            {
                respuesta.Estado = false;
                respuesta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
            }
            return respuesta;
        }
        public RespuestaNegocio<Cartera> ObtenerCartera(string idTipoIdentificacion, string numeroDocumento)
        {
            RespuestaNegocio<Cartera> respuesta = new RespuestaNegocio<Cartera>();
            Moras Moras = new Moras();
            try
            {
                respuesta.Respuesta = _creditosUnidadTrabajo.ObtenerCartera(idTipoIdentificacion, numeroDocumento);
                //Se comentarea este codigo hasta validar la mora
                //Moras = _SicavUnidadTrabajo.ObtenerMoras(numeroDocumento);

                //if (Moras.Sancion > 0)
                //{
                //    if (Moras.Sancion < 90)
                //    {
                //        respuesta.Estado = false;
                //        respuesta.MensajesError.Add("El solicitante tiene una sancion de " + Moras.Sancion + "dias por haber entrado en mora de mas de 90 dias.");
                //    }
                //    else
                //    {
                //        if (Moras.Sancion >= 90)
                //        {
                //            respuesta.Estado = false;
                //            respuesta.MensajesError.Add("El solicitante se encuentra en mora.");
                //        }
                //    }
                //}
                //else
                //{
                //    if (!respuesta.Respuesta.CumpleAportes)
                //    {
                //        respuesta.Estado = false;
                //        respuesta.MensajesError.Add("El solicitante no está al día en aportes.");
                //    }
                //    else if (!respuesta.Respuesta.CumpleCartera)
                //    {
                //        respuesta.Estado = false;
                //        respuesta.MensajesError.Add("El solicitante no está al día en obligaciones.");
                //    }
                //    else
                //    {
                //        respuesta.Estado = true;
                //    }
                //}

            }
            catch (Exception ex)
            {
                respuesta.Estado = false;
                respuesta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return respuesta;
        }
        public RespuestaNegocio<IEnumerable<ViewModelSolicitudCredito>> ObtenerSolicitudCredito(string numeroIdentificacion = null, string idTipoIdentificacion = null, string consecutivoSolicitud = null, int? idTipoCredito = null, string idEstadoSolicitudCredito = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15", int? idPerfil = null, string Usuario = null)
        {
            RespuestaNegocio<IEnumerable<ViewModelSolicitudCredito>> rta = new RespuestaNegocio<IEnumerable<ViewModelSolicitudCredito>>();
            try
            {
                long longConsecutivoSolicitud = 0;
                long.TryParse(consecutivoSolicitud, out longConsecutivoSolicitud);
                rta.Respuesta = _SicavUnidadTrabajo.ObtenerSolicitudCredito(idTipoIdentificacion: idTipoIdentificacion,
                                                                            numeroIdentificacionAsociado: numeroIdentificacion,
                                                                            idTipoCredito: idTipoCredito,
                                                                            idEstadoSolicitudCredito: idEstadoSolicitudCredito,
                                                                            consecutivoSolicitud: (longConsecutivoSolicitud == 0 ? (long?)null : longConsecutivoSolicitud),
                                                                            idPerfil: idPerfil,
                                                                            usuarioModifica: Usuario);
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return rta;
        }
        public RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> CambiarEstadoSolicitud(int idSolicitud)
        {
            RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> rta = new RespuestaNegocio<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito>();
            var solicitud = _SicavUnidadTrabajo.CambiarEstadoSolicitud(idSolicitud);
            rta.Respuesta = solicitud;
            rta.Estado = true;
            return rta;
        }
        public RespuestaNegocio<ViewModelSolicitudCreditoDetalle> ObtenerSolicitudCreditoDetalle(int idSolicitud)
        {
            RespuestaNegocio<ViewModelSolicitudCreditoDetalle> rta = new RespuestaNegocio<ViewModelSolicitudCreditoDetalle>();
            try
            {
                JavaScriptSerializer serializador = new JavaScriptSerializer();
                ViewModelSolicitudCreditoDetalle viewModelSolicitudCreditoDetalle = new ViewModelSolicitudCreditoDetalle();
                List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos = new List<FormCapacidadPagoGrupos>();
                var capacidadPago = _SicavUnidadTrabajo.ObtenerSolicitudCapacidadPago(idSolicitud);
                listaFormCapacidadPagoGrupos = serializador.Deserialize<List<FormCapacidadPagoGrupos>>(capacidadPago.JsonCapacidadPago);
                foreach (FormCapacidadPagoGrupos item in listaFormCapacidadPagoGrupos)
                {
                    item.ListaFormCapacidadPago = item.ListaFormCapacidadPago.Where(x => x.ValorAsociado != null).ToList();
                }
                viewModelSolicitudCreditoDetalle.IdSolicitud = idSolicitud;
                viewModelSolicitudCreditoDetalle.ListaFormCapacidadPagoGrupos = listaFormCapacidadPagoGrupos;
                viewModelSolicitudCreditoDetalle.HorasExtras = listaFormCapacidadPagoGrupos.Where(x => x.HorasExtras != null).FirstOrDefault().HorasExtras;
                viewModelSolicitudCreditoDetalle.Observaciones = _SicavUnidadTrabajo.ObtenerSolicitudCredito(idSolicitud).Observaciones;
                var grupo = listaFormCapacidadPagoGrupos.Where(x => x.IdGrupo == (int)enumFormCapacidadPagoGrupos.Valor_Credito).FirstOrDefault().ListaFormCapacidadPago;
                int plazoMeses = 0;
                int.TryParse(grupo.Where(x => x.IdCapacidadPago == (int)enumFormCapacidadPago.Plazo_meses_valorCredito).SingleOrDefault().ValorAsociado.ToString(), out plazoMeses);
                viewModelSolicitudCreditoDetalle.Plazo = plazoMeses;
                rta.Respuesta = viewModelSolicitudCreditoDetalle;
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return rta;
        }
        public RespuestaNegocio<SICSES.Infraestructura.Model.Sicav.SolicitudCredito> ActuaizarSolicitudCredio(List<Parametros> causalesNegacion, int idSolicitud, short idEstadoSolicitudCredito, string observaciones, string UsuRegistra, int IdMenu, int IdProceso, Cavipetrol.SICSES.Infraestructura.Model.Sicav.CapacidadPago capacidadPago)
        {
            RespuestaNegocio<SICSES.Infraestructura.Model.Sicav.SolicitudCredito> rta = new RespuestaNegocio<Infraestructura.Model.Sicav.SolicitudCredito>();
            try
            {
                var solicitudCredito = _SicavUnidadTrabajo.ObtenerSolicitudCredito(idSolicitud);
                solicitudCredito.AudFechaModificacion = DateTime.Now;
                solicitudCredito.AudUsuarioModificacion = UsuRegistra;

                JavaScriptSerializer serializador = new JavaScriptSerializer();
                var hoy = DateTime.Now;

                SolicitudCreditoHistorico solicitudCreditoHistorico = new SolicitudCreditoHistorico();
                solicitudCreditoHistorico.AudFechaCreacion = solicitudCredito.AudFechaCreacion;
                solicitudCreditoHistorico.AudFechaModificacion = solicitudCredito.AudFechaModificacion;
                solicitudCreditoHistorico.AudUsuarioCreacion = solicitudCredito.AudUsuarioCreacion;
                solicitudCreditoHistorico.AudUsuarioModificacion = solicitudCredito.AudUsuarioModificacion;
                solicitudCreditoHistorico.ConsecutivoSolicitud = solicitudCredito.ConsecutivoSolicitud;
                solicitudCreditoHistorico.CursoFodes = solicitudCredito.CursoFodes;
                solicitudCreditoHistorico.fscNumSolicitud = solicitudCredito.fscNumSolicitud;
                solicitudCreditoHistorico.IdAseguabilidadOpcion = solicitudCredito.IdAseguabilidadOpcion;
                solicitudCreditoHistorico.IdCiuuFomento = solicitudCredito.IdCiuuFomento;
                solicitudCreditoHistorico.IdEstadoSolicitudCredito = solicitudCredito.IdEstadoSolicitudCredito;
                solicitudCreditoHistorico.IdOficina = solicitudCredito.IdOficina;
                solicitudCreditoHistorico.IdSolicitud = solicitudCredito.IdSolicitud;
                solicitudCreditoHistorico.IdTipoCreditoTipoRolRelacion = solicitudCredito.IdTipoCreditoTipoRolRelacion;
                solicitudCreditoHistorico.IdTipoGarantia = solicitudCredito.IdTipoGarantia;
                solicitudCreditoHistorico.IdTipoIdentificacionAsociado = solicitudCredito.IdTipoIdentificacionAsociado;
                solicitudCreditoHistorico.IdUsoCredito = solicitudCredito.IdUsoCredito;
                solicitudCreditoHistorico.NumeroIdentificacionAsociado = solicitudCredito.NumeroIdentificacionAsociado;
                solicitudCreditoHistorico.Observaciones = solicitudCredito.Observaciones;
                solicitudCreditoHistorico.ValorCruceInterproductos = solicitudCredito.ValorCruceInterproductos;
                solicitudCreditoHistorico.ValorCuentaFai = solicitudCredito.ValorCuentaFai;
                solicitudCreditoHistorico.ValorGirarCheque = solicitudCredito.ValorGirarCheque;
                solicitudCreditoHistorico.ValorInmueble = solicitudCredito.ValorInmueble;
                solicitudCreditoHistorico.ValorSaldoAnterior = solicitudCredito.ValorSaldoAnterior;
                solicitudCreditoHistorico.ValorSolicitado = solicitudCredito.ValorSolicitado;
                solicitudCreditoHistorico.TipoCapacidadPago = solicitudCredito.TipoCapacidadPago;
                _SicavUnidadTrabajo.GuardarSolicitudCreditoHistorico(solicitudCreditoHistorico);



                //Historico historico = new Historico
                //{
                //    Datos = serializador.Serialize(solicitudCredito),
                //    IdMenu = IdMenu,
                //    IdProceso = IdProceso,
                //    AudFechaCreacion = hoy,
                //    AudFechaModificacion = hoy,
                //    AudUsuarioCreacion = UsuRegistra,
                //    AudUsuarioModificacion = UsuRegistra
                //};
                //_SicavUnidadTrabajo.GuardarHistorico(historico);

                solicitudCredito.IdEstadoSolicitudCredito = idEstadoSolicitudCredito;
                solicitudCredito.Observaciones = observaciones;
                _SicavUnidadTrabajo.ActualizarEstadoSolicitudCredito(causalesNegacion, solicitudCredito, UsuRegistra, capacidadPago);
                rta.Respuesta = solicitudCredito;
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return rta;
        }


        public IEnumerable<Garantia> ObtenerInformacionGarantia(string NumSolicitud, string TipoDoc, string NumeroDoc)
        {
            return _creditosUnidadTrabajo.ObtenerInformacionGarantia(NumSolicitud, TipoDoc, NumeroDoc);
        }
        public RespuestaNegocio<CuposAsegurabilidad> ObtenerCupoAsegurabilidad(int edad)
        {
            RespuestaNegocio<CuposAsegurabilidad> rta = new RespuestaNegocio<CuposAsegurabilidad>();
            try
            {
                rta.Respuesta = _consultasApoyoUnidadTrabajo.ObtenerCuposAsegurabilidad().Where(x => x.EdadInicial <= edad && x.EdadFinal >= edad).FirstOrDefault() ?? new CuposAsegurabilidad { Valor = 0 };
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return rta;
        }
        public RespuestaNegocio<IEnumerable<FormaPago>> ObtenerFormasPagoFYC(string idTipoCreditoTipoRolRelacion, bool mesada14)
        {
            RespuestaNegocio<IEnumerable<FormaPago>> rta = new RespuestaNegocio<IEnumerable<FormaPago>>();
            try
            {
                int id = 0;
                int.TryParse(idTipoCreditoTipoRolRelacion, out id);
                var tipoCreditoTipoRolRelacion = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposRolRelacion(0, id).FirstOrDefault();
                var papel = _consultasApoyoUnidadTrabajo.ObtenerPapelCavipetrol(tipoCreditoTipoRolRelacion.IdPapelCavipetrol);

                List<FormaPago> rtalistaFormaPago = new List<FormaPago>();
                List<FormaPago> listaFormaPago = _consultasApoyoUnidadTrabajo.ObtenerFormaPago().ToList();

                List<FormaPagoRelacion> listaFormaPagoRelacion = _consultasApoyoUnidadTrabajo.ObtenerFormaPagoRelacion(tipoCreditoTipoRolRelacion.IdTipoCredito, tipoCreditoTipoRolRelacion.IdPapelCavipetrol, mesada14).ToList();

                listaFormaPago = listaFormaPagoRelacion.Join(listaFormaPago,
                                                                         post => post.IdFormaPago,
                                                                         meta => meta.IdFormaPago,
                                                                         (post, meta) => new FormaPago()
                                                                         {
                                                                             IdFormaPago = post.IdFormaPago,
                                                                             Nombre = meta.Nombre,
                                                                             EGV_Clave = post.Clave,
                                                                             Descripcion = meta.Descripcion
                                                                         }).ToList();                

                rta.Respuesta = listaFormaPago.OrderBy(x => x.IdFormaPago);
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return rta;
        }
        public RespuestaNegocio<IEnumerable<ViewModelConsolidadoAtribuciones>> ObtenerConsolidadoAtribuciones(string idTipoIdentificacion, string numeroIdentificacionAsociado, int? idPerfil, int? valorcredito)
        {
            RespuestaNegocio<IEnumerable<ViewModelConsolidadoAtribuciones>> rta = new RespuestaNegocio<IEnumerable<ViewModelConsolidadoAtribuciones>>();
            try
            {
                rta.Respuesta = _SicavUnidadTrabajo.ObtenerConsolidadoAtribuciones(idTipoIdentificacion, numeroIdentificacionAsociado, idPerfil, valorcredito);
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return rta;

        }
        public RespuestaNegocio<VMSaldoAsegurabilidad> ObtenerSaldoAsegurabilidad(string numeroIdentificacion)
        {
            RespuestaNegocio<VMSaldoAsegurabilidad> rta = new RespuestaNegocio<VMSaldoAsegurabilidad>();
            try
            {
                rta.Respuesta = _creditosUnidadTrabajo.ObtenerSaldoAsegurabilidad(numeroIdentificacion);
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return rta;
        }



        public RespuestaValidacion ValidarCodeudor(string NumeroDocumento)
        {
            {
                return _creditosUnidadTrabajo.ValidarCodeudor(NumeroDocumento);
            }

        }
        public IEnumerable<TipoGarantiaPorProducto> ObtenerTipoGarantiaPorProducto(string Producto)
        {
            return _creditosUnidadTrabajo.ObtenerTipoGarantiaPorProducto(Producto);
        }
        public RespuestaNegocio<SolicitudCreditoIntentosLog> GuardarSolicitudCreditoIntentoLog(SolicitudCreditoIntentosLog model)
        {
            RespuestaNegocio<SolicitudCreditoIntentosLog> rta = new RespuestaNegocio<SolicitudCreditoIntentosLog>();
            try
            {
                DateTime hoy = DateTime.Now;
                model.AudFechaCrea = hoy;
                model.AudFechaMod = hoy;
                _SicavUnidadTrabajo.GuardarSolicitudCreditoIntentoLog(model);
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
                rta.MensajesError.Add(ex.Source);
                rta.MensajesError.Add(ex.StackTrace);
            }
            return rta;
        }


        public RespuestaNegocio<Familiar> GuardarFamiliar(Familiar informacionFamiliar)
        {
            RespuestaNegocio<Familiar> rta = new RespuestaNegocio<Familiar>();
            try
            {
                rta.Respuesta = _creditosUnidadTrabajo.GuardarFamiliar(informacionFamiliar);
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return rta;
        }
        public RespuestaNegocio<List<Amortizacion>> GenerarTablaAmortizacion(int idLinea,
                                                                             int idTipoCreditoTipoRolRelacion,
                                                                             int periodos,
                                                                             double tasaNominal,
                                                                             double tasaEA,
                                                                             double valorCredito,
                                                                             double? valorTotalInmueble,
                                                                             short idTipoPoliza,
                                                                             double porcentajeExtraprima,
                                                                             short idPapelCavipetrol,
                                                                             byte edad,
                                                                             int idFormaPago,
                                                                             int gracia,
                                                                             int idTipoGarantia)
        {
            RespuestaNegocio<List<Amortizacion>> rta = new RespuestaNegocio<List<Amortizacion>>();
            int periodosAno = 0;
            var tasaMensual = _consultasApoyoUnidadTrabajo.ObtenerSegurosTasas(idTipoSeguro: (int)enumTipoSeguro.Seguro_de_vida, edad: edad).FirstOrDefault();
            double tasaMensualSeguroVida = tasaMensual == null ? 0 : tasaMensual.TasaMensual;
            float? valorSeguros = 0;

            switch (idFormaPago)
            {
                case (int)enumFormaPago.Anual:
                    periodosAno = 1;
                    tasaMensualSeguroVida = tasaMensualSeguroVida * 12;
                    break;
                case (int)enumFormaPago.Semestral:
                    periodosAno = 2;
                    tasaMensualSeguroVida = tasaMensualSeguroVida * 6;
                    break;
                case (int)enumFormaPago.Mensual:
                    periodosAno = 12;
                    break;
                case (int)enumFormaPago.Quincenal:
                    periodosAno = 24;
                    tasaMensualSeguroVida = tasaMensualSeguroVida / 2;
                    break;
                default:
                    periodosAno = 12;
                    break;
            }
            try
            {
                float? tasaNominalFloat = 0;
                List<Amortizacion> listaAmortizacion = new List<Amortizacion>();

                short idTipoCredito = 0;
                idTipoCredito = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposRolRelacion(idTipoCreditoTipoRolRelacion: idTipoCreditoTipoRolRelacion).SingleOrDefault().IdTipoCredito;

                List<TiposCreditoTipoSeguroRelacion> listTipoCreditoTipoSeguroRelacion = new List<TiposCreditoTipoSeguroRelacion>();
                listTipoCreditoTipoSeguroRelacion = _consultasApoyoUnidadTrabajo.ObtenerTiposCreditoTiposSeguroRelacion(idTipoCredito).ToList();




                switch (idLinea)
                {
                    case (int)enumLineasCredito.Vivienda:
                        CapacidadPagoCalculoCreditoAbstracta calculoLineaVivienda = new CapacidadPagoCalculoCreditoLineaVivienda();

                        tasaNominalFloat = (calculoLineaVivienda.CalcularTasaNominal((tasaEA / 100), periodosAno) / 100);
                        calculoLineaVivienda.GenerarTablaAmortizacion(periodos,
                                                              (double)tasaNominalFloat,
                                                              tasaEA,
                                                              valorCredito,
                                                              ref listaAmortizacion,
                                                              listTipoCreditoTipoSeguroRelacion,
                                                              valorTotalInmueble,
                                                              idTipoPoliza,
                                                              tasaMensualSeguroVida,
                                                              porcentajeExtraprima,
                                                              (Infraestructura.Utilidades.Enumeraciones.Globales.enumPapelCavipetrol)idPapelCavipetrol,
                                                              gracia, edad, idFormaPago, ref valorSeguros, idTipoGarantia);
                        break;
                    case (int)enumLineasCredito.Consumo:
                        CapacidadPagoCalculoCreditoAbstracta calculoLineaConsumo = new CapacidadPagoCalculoCreditoLineaConsumo();
                        tasaNominalFloat = (calculoLineaConsumo.CalcularTasaNominal((tasaEA / 100), periodosAno) / 100);
                        calculoLineaConsumo.GenerarTablaAmortizacion(periodos,
                                                                      (double)tasaNominalFloat,
                                                                      tasaEA,
                                                                      valorCredito,
                                                                      ref listaAmortizacion,
                                                                      listTipoCreditoTipoSeguroRelacion,
                                                                      valorTotalInmueble,
                                                                      idTipoPoliza,
                                                                      tasaMensualSeguroVida,
                                                                      porcentajeExtraprima,
                                                                      (Infraestructura.Utilidades.Enumeraciones.Globales.enumPapelCavipetrol)idPapelCavipetrol,
                                                                      gracia, edad, idFormaPago, ref valorSeguros, idTipoGarantia);
                        break;
                }
                rta.Respuesta = listaAmortizacion;
                rta.Estado = true;
            }
            catch (Exception ex)
            {
                rta.Estado = false;
                rta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.Message);
            }
            return rta;
        }

        public IEnumerable<FormaPagoCredito> ObtenerFormaDePagoSolicitud(string idSolicitud)
        {
            return _creditosUnidadTrabajo.ObtenerFormaDePagoSolicitud(idSolicitud);
        }
        public List<CapacidadPagoCodeudores> ObtenerCoincidenciaCodeudorCreditos(string numeroIdentificacion, string tipoIdentificacion)
        {
            List<CapacidadPagoCodeudores> listaCapacidadPagoCodeudores = new List<CapacidadPagoCodeudores>();
            List<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito> listaSolicitudCredito = new List<Cavipetrol.SICSES.Infraestructura.Model.Sicav.SolicitudCredito>();

            listaCapacidadPagoCodeudores = _SicavUnidadTrabajo.ObtenerCoincidenciaCodeudorCreditos().Where(x => x.IdTipoIdentificacionCodeudor == tipoIdentificacion
                                                                                                    && x.NumeroIdentificacionCodeudor == numeroIdentificacion).ToList();

            // Se obtienen las solicitudes de credito finalizadas (Id del estado es 13)
            listaSolicitudCredito = _SicavUnidadTrabajo.ObtenerTodasSolicitudCredito(13).ToList();

            listaCapacidadPagoCodeudores = listaCapacidadPagoCodeudores.Join(listaSolicitudCredito,
                                                                                            Codeudores => Codeudores.IdSolicitud,
                                                                                            SolicitudC => SolicitudC.IdSolicitud,
                                                                                            (Codeudores, SolicitudC) => new CapacidadPagoCodeudores()
                                                                                            {
                                                                                                IdSolicitud = Codeudores.IdSolicitud,
                                                                                                IdCapacidadPagoCodeudores = Codeudores.IdCapacidadPagoCodeudores
                                                                                            }).ToList();

            return listaCapacidadPagoCodeudores;
        }
        public List<CapacidadPagoCodeudores> ObtenerSolicitudCreditoDetalleCodeudores(int idSolicitud)
        {
            List<CapacidadPagoCodeudores> listaCapacidadPagoCodeudores = new List<CapacidadPagoCodeudores>();

            listaCapacidadPagoCodeudores = _SicavUnidadTrabajo.ObtenerSolicitudCreditoDetalleCodeudores(idSolicitud);

            return listaCapacidadPagoCodeudores;
        }

        public List<FormaPago> ObtenerFormaPagoPorNorma(string norma)
        {
            IEnumerable<ViewModelFormaPagoFYC> listFormaPagoFYC = _creditosUnidadTrabajo.ObtenerFormaPagoPorNorma(norma);

            List<FormaPago> listFormaPago = new List<FormaPago>();
            foreach (ViewModelFormaPagoFYC obj in listFormaPagoFYC)
            {
                FormaPago formaPago = new FormaPago();
                formaPago.Nombre = obj.PET_Equivalencia;
                formaPago.IdFormaPago = obj.IdFormaPago;
                formaPago.EGV_Clave = obj.EGV_Clave;
                formaPago.Gracia = norma.Equals("Laboral2016") ? 12 : 0;
                listFormaPago.Add(formaPago);
            }
            return listFormaPago;
        }
        public RespuestaValidacion ObtenerSolicitudCreditoAdjunto(int idSolicitud, int idTipoAdjunto)
        {
            return _SicavUnidadTrabajo.ObtenerSolicitudCreditoAdjunto(idSolicitud, idTipoAdjunto);
        }
        public SolicitudCreditoAdjunto GuardarSolicitudCreditoAdjunto(SolicitudCreditoAdjunto solicitudCreditoAdjunto)
        {
            //solicitudCreditoAdjunto.FechaCreacion = DateTime.Now;
            //solicitudCreditoAdjunto.FechaModificacion = DateTime.Now;
            return _SicavUnidadTrabajo.GuardarSolicitudCreditoAdjunto(solicitudCreditoAdjunto);
        }

        public RespuestaNegocio<Object> ObtenerHipotecaSolicitudCredito(int idSolicitud)
        {
            RespuestaNegocio<Object> respuesta = new RespuestaNegocio<Object>();
            var listaHipotecasDisponibles = _SicavUnidadTrabajo.ObtenerHipotecaSolicitudCredito(idSolicitud).Where(x => x.FechaEscrituracion.HasValue && x.NumeroEscritura != null);
            if (listaHipotecasDisponibles.Count() > 0)
            {
                listaHipotecasDisponibles = listaHipotecasDisponibles.Where(x => x.PagareGenerado == true);
                if (listaHipotecasDisponibles.Count() > 0)
                {
                    respuesta.Estado = true;
                    respuesta.Respuesta = 1;
                }
                else
                {
                    respuesta.Estado = true;
                    respuesta.Respuesta = 2;
                }
            }
            else
            {
                respuesta.Estado = true;
                respuesta.Respuesta = 3;
            }
            return respuesta;
        }



        public int guardarSolicitudCreditoFyc(SolicitudCreditoFyc solicitudCreditoFyc)
        {
            RespuestaNegocio<object> respuesta = new RespuestaNegocio<object>();
            int numeroSolicitud = 0;

            try
            {
                ObjectInt result = _creditosUnidadTrabajo.guardarSolicitudCreditoFyc(solicitudCreditoFyc);
                respuesta.Estado = true;
                string strNumeroSolicitud = result.numero.ToString();
                int.TryParse(strNumeroSolicitud, out numeroSolicitud);

            }
            catch (Exception ex)
            {
                respuesta.Estado = false;
                respuesta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
            }
            return numeroSolicitud;
        }
        /// <summary>
        /// negocio del guardado de la solicitud de credito fyc isynet ecp
        /// </summary>
        /// <param name="solicitudCreditoFyc"></param>
        /// <returns></returns>
        public int GuardarSolicitudCreditoFycIsynetECP(SolicitudCreditoFyc solicitudCreditoFyc)
        {
            RespuestaNegocio<object> respuesta = new RespuestaNegocio<object>();
            int numeroSolicitud = 0;

            try
            {
                CreditosContexto ObjCreditosContexto = new CreditosContexto();
                ObjectInt result = ObjCreditosContexto.GuardarSolicitudCreditoFycIsynetECP(solicitudCreditoFyc).FirstOrDefault();
                respuesta.Estado = true;
                string strNumeroSolicitud = result.numero.ToString();
                int.TryParse(strNumeroSolicitud, out numeroSolicitud);

            }
            catch (Exception ex)
            {
                respuesta.Estado = false;
                respuesta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
            }
            return numeroSolicitud;
        }

        public RespuestaNegocio<object> guardarPagareGarantia(SolicitudCreditoPagare solicitudCreditoPagare)
        {
            RespuestaNegocio<object> respuesta = new RespuestaNegocio<object>();
            try
            {
                int numeroPagare = 0;

                //solicitudCreditoPagare.numeroPagare = _creditosUnidadTrabajo.generarNumeroPagare(solicitudCreditoPagare);
                solicitudCreditoPagare.jsonConceptos = JsonConvert.SerializeObject(solicitudCreditoPagare.listaConceptosGarantia);

                //guardar pagare de la solicitud en la base SICAV
                _creditosUnidadTrabajo.guardarSolicitudCreditoPagare(solicitudCreditoPagare);
                //guardar pagare de la solicitud en la base FYC
                int.TryParse(solicitudCreditoPagare.numeroPagare, out numeroPagare);
                solicitudCreditoPagare.listaConceptosGarantia.Add(new VMConceptoGarantiaCredito
                {
                    pkFycItemDocumento = "LineaUnicaVivien",

                });
                foreach (VMConceptoGarantiaCredito vMConceptoGarantiaCredito in solicitudCreditoPagare.listaConceptosGarantia)
                {
                    if (vMConceptoGarantiaCredito.pkFycItemDocumento != null)
                    {
                        vMConceptoGarantiaCredito.IdPagare = numeroPagare;
                        vMConceptoGarantiaCredito.pagare = solicitudCreditoPagare.variable;
                        _creditosUnidadTrabajo.guardarPagareGarantia(vMConceptoGarantiaCredito);
                    }
                }
                respuesta.Estado = true;
            }
            catch (Exception ex)
            {
                respuesta.Estado = false;
                respuesta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
            }
            return respuesta;
        }
        public double ObtenerCupoMaximoCredito(string TipoIdentificacion, string NumeroIdentificacion)
        {
            return _SicavUnidadTrabajo.ObtenerCupoMaximoCredito(TipoIdentificacion, NumeroIdentificacion);
        }

        /**
         * Genera numero consecutivo del pagare.
         */
        public int generarNumeroConsecutivoPagare(SolicitudCreditoPagare solicitudCreditoPagare)
        {
            int numeroPagare = 0;
            //string strNumeroPagare = _creditosUnidadTrabajo.generarNumeroPagare(solicitudCreditoPagare);
            ObjectInt result = _creditosUnidadTrabajo.generarNumeroPagare(solicitudCreditoPagare);

            string strNumeroPagare = result.numero.ToString();
            int.TryParse(strNumeroPagare, out numeroPagare);
            return numeroPagare;

        }
        public bool GuardarSolicitudCreditoHipoteca(SolicitudCreditoHipoteca solicitudCreditoHipoteca)
        {
            if (_SicavUnidadTrabajo.GuardarSolicitudCreditoHipoteca(solicitudCreditoHipoteca))
            {
                return _HipotecasUnidadTrabajo.ActualizarHipoteca(solicitudCreditoHipoteca.IdHipoteca, solicitudCreditoHipoteca.Monto);
            }
            else
            {
                return false;
            }
        }

        public RespuestaNegocio<object> GenerarHipotecaGuardarMinuta(SolicitudMinutaFYC solicitudMinutaFYC)
        {
            RespuestaNegocio<object> respuesta = new RespuestaNegocio<object>();
            try
            {
                respuesta.Respuesta = _SicavUnidadTrabajo.GenerarHipotecaGuardarMinuta(solicitudMinutaFYC);
                respuesta.Estado = true;
            }
            catch (Exception ex)
            {
                respuesta.Estado = false;
                respuesta.MensajesError.Add(ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message);
            }
            return respuesta;
        }
        public Aportes ObtenerAportes(string numeroIdentificacion, string tipoIdentificacion, short IdTipoRol)
        {
            return _creditosUnidadTrabajo.ObtenerAportes(numeroIdentificacion, tipoIdentificacion, IdTipoRol);
        }
        public IEnumerable<SolicitudesCredito> ObtenerSolicitudes(int numeroSolicitud, string tipoDocumento, int numeroDocumento, int tipoCredito)
        {
            return _creditosUnidadTrabajo.ObtenerSolicitudes(numeroSolicitud, tipoDocumento, numeroDocumento, tipoCredito);
        }
        public string ObtenerCodeudores(int Identificacion)
        {
            return _SicavUnidadTrabajo.ObtenerCodeudores(Identificacion);
        }
        public IEnumerable<AsegurabilidadOpcion> ObtenerOpcionesAsegurabilidad(bool IdAsegurable, string idCredito, string idPapel)
        {
            return _creditosUnidadTrabajo.ObtenerOpcionesAsegurabilidad(IdAsegurable, idCredito, idPapel);
        }
        public string GuardarJubilado(DetalleAsociado asociado)
        {
            return _SicavUnidadTrabajo.GuardarJubilado(asociado);

        }
        public bool ConfirmarMesada14(string Cedula)
        {
            return _SicavUnidadTrabajo.ConfirmarMesada14(Cedula);

        }
        public int ValidarITP(string cedula, int bandera)
        {
            return _creditosUnidadTrabajo.ValidarITP(cedula, bandera);

        }
        public int ValidarEmbargo(string cedula, int bandera)
        {
            return _creditosUnidadTrabajo.ValidarEmbargo(cedula, bandera);

        }
        public int ValidarExisExtraprima(string cedula, string tipoIdentificacion, int bandera)
        {
            return _creditosUnidadTrabajo.ValidarExisExtraprima(cedula, tipoIdentificacion, bandera);

        }
        public IEnumerable<ExtraPrima> TraeExtraprima(string cedula, string tipoIdentificacion, int bandera)
        {
            return _creditosUnidadTrabajo.TraeExtraprima(cedula, tipoIdentificacion, bandera).ToList();
        }

        public void actualizarEstado(string NumeroSolicitud, string Estado)
        {           
            _creditosUnidadTrabajo.actualizarEstado(NumeroSolicitud, Estado);
        }

        public IEnumerable<ArbolAmortizacion> ObtenerArbolAmortizacion(string idtipocredito, string idpapelcavipetrol)
        {
            return _creditosUnidadTrabajo.ObtenerArbolAmortizacion(idtipocredito, idpapelcavipetrol);
        }

        public virtual void GuardarSolicitudInterproductos(List<InformacionProductosCreditos> datos)
        {
            _creditosUnidadTrabajo.GuardarSolicitudInterproductos(datos);
        }
    }
}
