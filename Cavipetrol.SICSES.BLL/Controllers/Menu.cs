﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class Menu
    {
        private readonly ISicavUnidadTrabajo _SicavUnidadTrabajo;
        public Menu()
        {
            _SicavUnidadTrabajo = new SicavUnidadTrabajo();
        }

        /// <summary>
        /// Recibe una lista de perfiles y obtiene los menús de cada uno
        /// </summary>
        /// <param name="listaPerfiles"></param>
        /// <returns>Retorna un consolidado de los menús de cada perfil</returns>
        public List<ViewModelMenu> ObtenerMenu(List<Perfil> listaPerfiles)
        {
            List<ViewModelMenu> listaVm = new List<ViewModelMenu>();

            List<PerfilMenuRelacion> listaMenus = new List<PerfilMenuRelacion>();
            List<PerfilMenuRelacion> listaMenusAux = new List<PerfilMenuRelacion>();

            //Se consultan los menús de cada perfil
            foreach (Perfil perfil in listaPerfiles)
            {
                //Lista auxiliar para comparar con la lista ppal,
                //para saber si hay menús repetidos
                listaMenusAux = _SicavUnidadTrabajo.ObtenerMenu(perfil.IdPerfil).ToList();

                //Se extraen los menús de la lista auxiliar que no estén en la lista ppal
                listaMenusAux = listaMenusAux.Except(listaMenus).ToList();

                //Se adicionan a la lista ppal los menús nuevos
                foreach(PerfilMenuRelacion menu in listaMenusAux)
                {
                    listaMenus.Add(menu);
                }
            }

            listaVm = listaMenus.Where(x => x.Menu.IdMenuOrigen == null)
                                  .Select(x => new ViewModelMenu
                                  {
                                      IdMenu = x.IdMenu,
                                      Nombre = x.Menu.Nombre,
                                      IdMenuOrigen = x.Menu.IdMenuOrigen,
                                      Ruta = x.Menu.Ruta,
                                      Icono = x.Menu.Icono,
                                      SubMenu = null,
                                      Orden = x.Menu.Orden,
                                      hijos = false
                                  })
                                  .OrderBy(y => y.Orden).ToList();

            foreach (ViewModelMenu Vm in listaVm)
            {
                Vm.SubMenu = listaMenus.Where(x => x.Menu.IdMenuOrigen == Vm.IdMenu)
                                        .Select(x => new ViewModelMenu
                                        {
                                            IdMenu = x.IdMenu,
                                            Nombre = x.Menu.Nombre,
                                            IdMenuOrigen = x.Menu.IdMenuOrigen,
                                            Ruta = x.Menu.Ruta,
                                            Icono = x.Menu.Icono,
                                            SubMenu = null,
                                            Orden = x.Menu.Orden,
                                        })
                                        .OrderBy(y => y.Orden).ToList();


                foreach(ViewModelMenu Vm2 in Vm.SubMenu)
                {
                    Vm2.SubMenu = listaMenus.Where(x => x.Menu.IdMenuOrigen == Vm2.IdMenu)
                                        .Select(x => new ViewModelMenu
                                        {
                                            IdMenu = x.IdMenu,
                                            Nombre = x.Menu.Nombre,
                                            IdMenuOrigen = x.Menu.IdMenuOrigen,
                                            Ruta = x.Menu.Ruta,
                                            Icono = x.Menu.Icono,
                                            SubMenu = null,
                                            Orden = x.Menu.Orden,
                                        })
                                        .OrderBy(y => y.Orden).ToList();
                    if (Vm2.SubMenu.Count != 0)
                    {
                        Vm2.hijos = true;
                    }
                }

            }
            return listaVm;
        }
        public IEnumerable<ViewModelMenu> MenuCompleto()
        {
            List<ViewModelMenu> listaVm = new List<ViewModelMenu>();
            List<Cavipetrol.SICSES.Infraestructura.Model.Sicav.Menu> listaPerfil = new List<Cavipetrol.SICSES.Infraestructura.Model.Sicav.Menu>();
            listaPerfil = _SicavUnidadTrabajo.MenuCompleto().ToList();
            listaVm = listaPerfil.Where(x => x.IdMenuOrigen == null)
                                  .Select(x => new ViewModelMenu
                                  {
                                      IdMenu = x.IdMenu,
                                      Nombre = x.Nombre,
                                      IdMenuOrigen = x.IdMenuOrigen,
                                      Ruta = x.Ruta,
                                      Icono = x.Icono,
                                      SubMenu = null,
                                      Orden = x.Orden,
                                  })
                                  .OrderBy(y => y.Orden).ToList();

            foreach (ViewModelMenu Vm in listaVm)
            {
                Vm.SubMenu = listaPerfil.Where(x => x.IdMenuOrigen == Vm.IdMenu)
                                        .Select(x => new ViewModelMenu
                                        {
                                            IdMenu = x.IdMenu,
                                            Nombre = x.Nombre,
                                            IdMenuOrigen = x.IdMenuOrigen,
                                            Ruta = x.Ruta,
                                            Icono = x.Icono,
                                            SubMenu = null,
                                            Orden = x.Orden,
                                        })
                                        .OrderBy(y => y.Orden).ToList();
            }
            return listaVm;
        }
        public bool ActualizarMenuPerfil(ListaMenuPerfil PerfilMenuRelacion)
        {
            return _SicavUnidadTrabajo.ActualizarPerfilMenuRelacion(PerfilMenuRelacion);
        }
        public void NuevoMetododoParaFusion() { }

    }
}
