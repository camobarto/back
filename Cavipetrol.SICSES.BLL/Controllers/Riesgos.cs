﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model.fyc.Riesgos;
using System.Collections.Generic;


namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class Riesgos
    {
        SicavUnidadTrabajo _sicavUnidadTrabajo = new SicavUnidadTrabajo();
        public IEnumerable<SolicitudOperacion> ObtenerOperacion(string TipoIdentificacion, string NumeroIdentificacion,string NumeroOperacion)
        {
            return _sicavUnidadTrabajo.SolicitudOperacion(TipoIdentificacion, NumeroIdentificacion, NumeroOperacion);
        }

        public void GuardarDatosPersonaTransaccionEfectivo(int Operacion, string Tipoidentificacion, string Numeroidentificacion, string Primernombre,
                                                        string Segundonombre, string Primerapellido, string Segundoapellido, string Direccion, string Telefono,
                                                        string Consulta, string Tipopersona)
        {
            _sicavUnidadTrabajo.GuardarDatosPersonaTransaccionEfectivo(Operacion, Tipoidentificacion, Numeroidentificacion, Primernombre, Segundonombre, Primerapellido,
                                                                    Segundoapellido, Direccion, Telefono, Consulta, Tipopersona);
        }
        public void GuardarDatosOrigenFondos(int Operacion, int Opcion, string Consulta, string Detalle)
        {
            _sicavUnidadTrabajo.GuardarDatosOrigenFondos(Operacion, Opcion, Consulta, Detalle);
        }
    }
}
