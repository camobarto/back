﻿using Cavipetrol.SICSES.DAL.UnidadTrabajo;
using Cavipetrol.SICSES.Infraestructura.Model.Sap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.BLL.Controllers
{
    public class Sap
    {

        private readonly ISapUnidadTrabajo _sapUnidadTrabajo;

        public Sap()
        {
            _sapUnidadTrabajo = new SapUnidadTrabajo();
        }
        public IEnumerable<AsiSoporte> ObtenerAsiSoporte()
        {
            return _sapUnidadTrabajo.ObtenerAsiSoporte();
        }
        public IEnumerable<Asientos> ConsultarAsientos(string AsiSoporte, string AsiConsecutivo, string Asioperacion, string AsiOpeConsecutivo, string Fecha, string Ciudad, string MensajeError)
        {
            return _sapUnidadTrabajo.ConsultarAsientos(AsiSoporte, AsiConsecutivo, Asioperacion, AsiOpeConsecutivo, Fecha, Ciudad, MensajeError);
        }

        public IEnumerable<string> ActualizarAsiento()
        {
            return  _sapUnidadTrabajo.ActualizarAsiento();
        }

        public IEnumerable<Proveedores> ConsultaProveedores(string CardCode, string CardName)
        {
            return _sapUnidadTrabajo.ConsultaProveedores(CardCode, CardName);
        }

        public IEnumerable<string> ActualizarEstadoProveedores()
        {
            return _sapUnidadTrabajo.ActualizarEstadoProveedores();
        }

        public IEnumerable<MensajeErrorAsientos> ConsultarMensajeError()
        {
            return _sapUnidadTrabajo.ConsultarMensajeError();
        }
    }
}
