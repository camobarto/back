﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad
{
    public class RespuestaValidacion
    {
        public string Respuesta { get; set; }
    }
}
