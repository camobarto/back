﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad
{
    public class Aseguradoras
    {
        public int id_aseguradora { get; set; }
        public string Nombre_aseguradora { get; set; }
        public string Tipo_nit { get; set; }
        public string Numero_nit { get; set; }
        public string Nombre_Aseguradora_vida { get; set; }
    }
}
