﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad
{
    public class DatosDocumento
    {
        public string[][] DatosExcel { get; set; }

        public string tipo { get; set; }

        public string[][] Datos { get; set; }

    }
}
