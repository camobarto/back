﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad
{
   public class PolizasExternas
    {
        [NotMapped]
        public int id { get; set; }
        public string Oficina { get; set; }

        public string Tipo_documento { get; set; }
        public string Cedula { get; set; }
        public string Nombre_Apellidos { get; set; }
        public int id_Aseguradora { get; set; }
        [NotMapped]
        public string Aseguradora { get; set; }
        public string Numero_Poliza { get; set; }
        public double Valor_Asegurado { get; set; }
        public string Tomador { get; set; }
        public string Nit_Tomador { get; set; }

        public string TipoNitTomador { get; set; }
        //public string Beneficiario { get; set; }
        //public string Nit_beneficiario { get; set; }
        public DateTime Vigencia_Desde { get; set; }
        public DateTime Vigencia_Hasta { get; set; }
        [NotMapped]
        public string Producto_Amparado { get; set; }
        [NotMapped]
        public string Documento_No { get; set; }
        public string Observaciones { get; set; }
        public string Direccion { get; set; }
        public string Matricula_Inmobiliaria { get; set; }
        public int Tipo_poliza { get; set; }

        public string Activo { get; set; }

        public int Aplicar_Colectiva { get; set; }


        [NotMapped]
        public string Nombre_poliza { get; set; }
        [NotMapped]
        public string Validacion { get; set; }

    }
}
