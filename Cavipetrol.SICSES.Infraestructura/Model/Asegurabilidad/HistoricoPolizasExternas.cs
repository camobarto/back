﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad
{
    public class HistoricoPolizasExternas
    {
        public string TSE_TipoNit { get; set; }
        public string TSE_NumNit { get; set; }
        public string TSE_Nombre_Apellidos { get; set; }
        public int?  TSE_Aseguradora { get; set; }
        public string TSE_NumPoliza { get; set; }
        public double TSE_ValorAsegura { get; set; }
        public DateTime TSE_VigenteDesde { get; set; }
        public DateTime TSE_VigenteHasta { get; set; }
        public int? TSE_AplicarColectiva { get; set; }
        public string TSE_TipoNitTomador { get; set; }
        public string TSE_NumNitTomador { get; set; }
        public string TSE_NombreTomaPoliza { get; set; }
        public string TSE_Observaciones { get; set; }
        public string TSE_Direccion { get; set; }
        public string TSE_Matricula { get; set; }
        public string TSE_Oficina { get; set; }
        public DateTime TSE_Fecha_Modificacion { get; set; }
        public string Usuario { get; set; }
        public string TIPO { get; set; }
         

    }
}
