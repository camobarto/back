﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad
{
    public class AsegurabilidadModel
    {

        public int? id { get; set; }        //public int? cedula { get; set; }

        public int? Reporte_deudores { get; set; }
        public string nombre { get; set; }
        public string nit { get; set; }
        public string Ciudad_Residencia { get; set; }
        public string Dirreccion_Residencia { get; set; }
        public string Telefono_Residencia { get; set; }
        public string Ciudad_oficina { get; set; }
        public string Dirreccion_oficina { get; set; }
        public string TelefonoOficina { get; set; }
        public string Correo_Electronico_oficina { get; set; }
        public string CorreoElectronico_residencia { get; set; }
        public string Fecha_nacimiento { get; set; }
        public string Documento_ciudad { get; set; }
        public double Saldo { get; set; }
        public int? Control_inclusiones_cavipetrol { get; set; }
        public string Cedula { get; set; }
        public string Mes_inc { get; set; }
        public DateTime UltimoMes_reporte { get; set; }
        public int? interrupcion { get; set; }
        public DateTime mes_inclusion { get; set; }
        public string Va_por_Refinanciacion { get; set; }
        public string Cumulo_Refinan { get; set; }
        public double? Etiquetas_de_fila { get; set; }
        public double? Continuidad { get; set; }
        public double? Di_17 { get; set; }
        public string Rechazo_Aseguradora_anterior { get; set; }
        public string Observacion { get; set; }
        public string Tipologia { get; set; }
        public int? Declaracion { get; set; }
        public double? Vr_asegurado_mes_ant { get; set; }
        public string Observacion_mes_anterior { get; set; }
        public string Porcentaje_extraprima { get; set; }
        public string Calculo { get; set; }
        public string Concepto_Extraprima { get; set; }


        public int?  Edad_al_ingreso { get; set; }
        public string Tasa_por_mil_anual { get; set; }
        public double? Limite_asegurado_segun_edad { get; set; }
        public string Aplica_cumulo { get; set; }
        public double? Valor_asegurado { get; set; }
        public string Valor_sin_cobertura { get; set; }
        public string Prima_mensual { get; set; }
        public string Valor_extraprima { get; set; }
        public string Prima_mensual_Extraprima { get; set; }
        public string Valor_asegurado_tomado { get; set; }
        public string Retorno { get; set; }
    }
}
