﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Asegurabilidad
{
    public class TipoPoliza
    {
        public int id_TipoPoliza { get; set; }
        public string Nombre_Poliza { get; set; }
    }
}
