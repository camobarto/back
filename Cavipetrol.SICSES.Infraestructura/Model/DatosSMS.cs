﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
   public  class DatosSMS
    {
        public string LMOL_LLAVE { get; set; }
        public string LMOL_AFINIT { get; set; }
        public string LMOL_AFINOMBRE { get; set; }
        public string LMOL_VALORACUM { get; set; }
        public string LMOL_ESTADO { get; set; }
        public string LMOL_TRANSACCION{ get; set; }

        public string Radicado { get; set; }

    }
}

