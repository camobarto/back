﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoCredito
    {
      public short IdTipoCredito { get; set; }
      public string Nombre { get; set; }   
      public int? IdTipoLineaCredito { get; set; }
    }
}
