﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
    public class DocumentosCreditosEcopetrolObtener
    {
        [Key]
        public int IdDocumento { get; set; }
        [Key]
        public string Descripcion { get; set; }
        [Key]
        public string FechaCreacionDocumento { get; set; }
    }

    public class DocumentosCreditosEcopetrolGuardar
    {
        [Key]
        public int IdDocumento { get; set; }
        [Key]
        public string Descripcion { get; set; }
        
    }
}
