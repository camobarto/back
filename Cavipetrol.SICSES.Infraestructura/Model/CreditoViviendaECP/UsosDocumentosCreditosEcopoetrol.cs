﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
    public class UsosDocumentosCreditosEcopoetrolGuardar
    {
        public int IdUso { get; set; }
        public int IdDocumento { get; set; }
        public int IdUsoDocumento { get; set; }
    }

    public class UsosDocumentosCreditosEcopoetrolObtener
    {
        public int IdUsoDocumento { get; set; } 
        public int IdUso { get; set; }
        public string DescripcionUso { get; set; }
        public int IdDocumento { get; set; }
        public string DescripcionDoc { get; set; }
        public string FechaRelacion { get; set; }
        public int Estado { get; set; }
    }
}
