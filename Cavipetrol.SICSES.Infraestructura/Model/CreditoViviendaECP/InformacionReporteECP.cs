﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
	public class InformacionReporteECP
	{
		public string TASAEA { get; set; }
		public int PLAZO { get; set; }
		public string PRODUCTO { get; set; }
		public string NORMA { get; set; }
	}
}
