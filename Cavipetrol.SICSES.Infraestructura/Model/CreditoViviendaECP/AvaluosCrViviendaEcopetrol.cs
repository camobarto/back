﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
    public class AvaluosCrViviendaEcopetrol
    {
        //avaluos externos
        public int AVAE_NumeroServicio { get; set; }
        public string AVAE_TipoNit { get; set; }
        public string AVAE_NumeroNit { get; set; }

        public string AVAE_Nombre { get; set; }
        public string AVAE_TipoServicio { get; set; }
        public string AVAE_TipoInmueble { get; set; }
        public string AVAE_Direccion { get; set; }
        public string AVAE_Barrio { get; set; }
        public string AVAE_Ciudad { get; set; }
        public string AVAE_DescripcioUbicacion { get; set; }
        public string AVAE_TelefonoContacto { get; set; }
        public string AVAE_CelularContacto { get; set; }
        public string AVAE_MailContacto { get; set; }
        public string AVAE_NombreOtrosContactos { get; set; }
        public string AVAE_FechaIngreso { get; set; }
        public string AVAE_Usuario { get; set; }
        public string AVAE_NitArquitecto { get; set; }
        public string AVAE_NumNitArquitecto { get; set; }
        public string AVAENombreArquitecto { get; set; }
        public string AVAE_NumOficina { get; set; }
        public string AVAE_NumMatricula { get; set; }
        public double? AVAE_ValorConstruccion { get; set; }
        public double? AVAE_Inmuebles { get; set; }
        public string AVAE_DescripcionInmueble { get; set; }
        public string AVAE_FechaAvaluo { get; set; }


        //

        public int AVA_NumeroServicio { get; set; }
        public string AVA_TipoNit { get; set; }
        public string AVA_NunNit { get; set; }
        public string Ava_Nombre { get; set; }
        public string AVA_TipoSerivicio { get; set; }
        public string AVA_TipoSolicitud { get; set; }
        public string AVA_TipoCobro { get; set; }
        public string AVA_CiudadCobro { get; set; }
        public int? AVA_Empleado { get; set; }
        public string AVA_TipoInmueble { get; set; }
        public string AVA_Direccion { get; set; }
        public string AVA_Barrio { get; set; }
        public string AVA_Ciudad { get; set; }
        public string AVA_Sitio_Perimetro { get; set; }
        public string AVA_Motivo { get; set; }
        public string AVA_DescripcionUbicacion { get; set; }
        public string AVA_TelefonoAfiliado { get; set; }
        
        public string AVA_TelefonoContacto { get; set; }
        public string AVA_CelularContacto { get; set; }
        public string AVA_MailContacto { get; set; }
        public string AVA_NombreOtrosContactos { get; set; }
        public string AVA_FechaSolicitudServicio { get; set; }
        public string AVA_UsuarioIngSolicitud { get; set; }
        public double? AVA_PagoArquitecto { get; set; }
        public double? AVA_DerechoAvaluo { get; set; }
        public double? AVA_Transporte { get; set; }
        public double? AVA_IVA { get; set; }
        public double? AVA_TotalPagado { get; set; }
        public double? AVA_PagoEcopetrol { get; set; }
        public double? AVA_ValorAdjudicado { get; set; }
        public double? AVA_Valorinspeccion { get; set; }
        public string AVA_FechaAvaluoAdj { get; set; }
        public int? AVA_ContadorAdj { get; set; }

        public string AVA_NumOficina { get; set; }
        public string AVA_NumMatricula { get; set; }
        public double? AVA_ValorConstruccion { get; set; }
        public double? AVA_Inmuebles { get; set; }
        public string AVA_descripcionInmuebles { get; set; }
        public string AVA_FechaEngraServ { get; set; }
        public string AVA_UsuarioEntrega { get; set; }
        public string AVA_FechaEntregaAfi { get; set; }
        public string ARV_Nombre { get; set; }
        public string ARV_CiudadTrabaja { get; set; }
        public string ARV_Telefono { get; set; }
        public string AEI_CueEstadoInmueble { get; set; }
        public string AEI_FechaEntrega { get; set; }
        public string AEI_Prestamo { get; set; }
    }

    public class AvaluosCrViviendaEcopetrolObtener
    {
        public string NumFechaServicio { get; set; }
        public string Identificacion { get; set; }
        public int IdSolicitud { get; set; }
        public int NumServicio { get; set; }
    }

    public class AvaluosLog
    {
        public int IdConsecutivo { get; set; }
        public string NitAfiliado { get; set; }
        public int NumServicio { get; set; }
        public string UsuarioSolicitud { get; set; }
    }

    public class ObtenerAvaluoLog
    {
        public int NumServicio { get; set; }
    }
}
