﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
    public class DatosGeneralesECP
    {
        public int Consecutivo { get; set; }
        public int IdSolicitud { get; set; }
    }
}
