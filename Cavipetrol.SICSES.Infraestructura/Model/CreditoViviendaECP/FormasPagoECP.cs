﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
	public class FormasPagoECP
	{
		public int Plazo { get; set; }
		public decimal Valor { get; set; }
		public decimal Cuota { get; set; }
		public string NumeroIdentificacion { get; set; }
		public int Concecutivo { get; set; }
		public string TipoPago { get; set; }
		public string Clave { get; set; }
	}
}
