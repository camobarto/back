﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
	public class ProcesoECP
	{
		public int NumeroFormulario { get; set; }
		public string Identificacion { get; set; }
		public Boolean Estado { get; set; }
		public Boolean Gestion { get; set; }
		public int PasoProceso { get; set; }
		public string NombreFormulario  { get; set; }
	}
}
