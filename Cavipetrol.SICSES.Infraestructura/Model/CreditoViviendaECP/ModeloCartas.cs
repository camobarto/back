﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
	public class ModeloCartas
	{
		public int Consecutivo { get; set; }
		public string Numero_De_Identificacion { get; set; }
		public string Nombre { get; set; }
		public string Linea_Prestamo { get; set; }
		public decimal Monto_Autorizado { get; set; }
		public string Tasa { get; set; }
		public string Fecha_Cargue { get; set; }
		public string Fecha_Adjudicacion { get; set; }
		public string Fecha_Vigencia { get; set; }
		public string Fecha_Prorroga { get; set; }
		public string Observacion { get; set; }

	}
}
