﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
    public class UsosCreditosEcopetrolGuardar
    {
        [Key]
        public int IdUso { get; set; }
        [Key]
        public string Descripcion { get; set; }
    }

    public class UsosCreditosEcopetrolObtener
    {
        [Key]
        public int IdUso { get; set; }
        [Key]
        public string Descripcion { get; set; }
        [Key]
        public string FechaCreacion { get; set; }
    }
}
