﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
    //MAPEO DE LA TABLA TMP_CARTAS_ADJUDICACION_CR_VIVIENDA_ECP
    //ENTIDADES PARA INSERTAR MASIVAMENTE EN LA TEMPORAL </jarp>
    public class DatosCartasCrViviendaEcopetrol
    {
        [Key]
        public string Registro { get; set; }
        [Key]
        public string Numero_De_Identificacion { get; set; }
        [Key]
        public string Nombre { get; set; }
        [Key]
        public string Linea_Prestamo { get; set; }
        [Key]
        public string Linea_Credito_Cavipetrol { get; set; }
        [Key]
        public string Fecha_Adjudicacion { get; set; }
        [Key]
        public string Fecha_Vigencia { get; set; }
        [Key]
        public int Tiempo_Amortizacion_Carta { get; set; }
        [Key]
        public decimal Monto_Autorizado { get; set; }
        [Key]
        public string Tasa { get; set; }

    }

    // ENTIDADES PARA CONSULTAR EL LISTADO DE ERRORES </jarp>
    public class DatosCartasCrViviendaEcopetrolConsultaErrores
    {
        [Key]
        public string Tipo_Error { get; set; }
        [Key]
        public string Registro { get; set; }
        [Key]
        public string Numero_De_Identificacion { get; set; }
        [Key]
        public string Nombre { get; set; }
        [Key]
        public string Linea_Prestamo { get; set; }
        [Key]
        public string Linea_Credito_Cavipetrol { get; set; }
        [Key]
        public string Fecha_Adjudicacion { get; set; }
        [Key]
        public string Fecha_Vigencia { get; set; }
        [Key]
        public int Tiempo_Amortizacion_Carta { get; set; }
        [Key]
        public decimal Monto_Autorizado { get; set; }
        [Key]
        public string Tasa { get; set; }

    }

    // ENTIDADES PARA CONSULTAR LAS CARTAS DESDE LA TABLA PERMENENTE </jarp>
    public class DatosCartasCrViviendaEcopetrolConsulta
    {
        [Key]
        public int Consecutivo { get; set; }
        [Key]
        public string Registro { get; set; }
        [Key]
        public string Numero_De_Identificacion { get; set; }
        [Key]
        public string Nombre { get; set; }
        [Key]
        public string Linea_Prestamo { get; set; }
        [Key]
        public string Linea_Credito_Cavipetrol { get; set; }
        [Key]
        public string Fecha_Adjudicacion { get; set; }
        [Key]
        public string Fecha_Vigencia { get; set; }
        [Key]
        public string Fecha_Prorroga { get; set; }
        [Key]
        public int Tiempo_Amortizacion_Carta { get; set; }
        [Key]
        public decimal Monto_Autorizado { get; set; }
        [Key]
        public string Tasa { get; set; }
        [Key]
        public string Fecha_Cargue { get; set; }

    }

    //MAPEO DE LA TABLA TMP_ACTUALIZAR_CARTAS_ADJUDICACION_CR_VIVIENDA_ECP
    //ENTIDADES PARA GUARDAR EN LA TMP
    public class DatosCartasActualizarCrViviendaEcopetrol
    {
        [Key]
        public int Consecutivo { get; set; }
        [Key]
        public string Fecha_Fin_Prorroga { get; set; }
    }

    //ENTIDADES PARA CONSULTAR EN LA TMP
    public class DatosCartasActualizarCrViviendaEcopetrolConsultaErrores
    {
        [Key]
        public string Tipo_Error { get; set; }
        [Key]
        public int Consecutivo { get; set; }
        [Key]
        public DateTime? Fecha_Fin_Prorroga { get; set; }
    }
    /// <summary>
    /// Modelo de los usos de los creditos
    /// </summary>
    public class DatosUsosCreditos
    {
        [Key]
        public int IdUsoCredito { get; set; }
        [Key]
        public int IdConsecutivo { get; set; }
        [Key]
        public string Descripcion { get; set; }
        [Key]
        public Decimal Valor { get; set; }
        [Key]
        public Boolean Activo { get; set; }

    }


    // ENTIDADES PARA CONSULTAR LOS DATOS DE LA CARGA </jarp>
    public class DatosCartasCrViviendaEcopetrolConsultaCarga
    {
        [Key]
        public int Consecutivo { get; set; }
        [Key]
        public string Numero_De_Identificacion { get; set; }
        [Key]
        public string Nombre { get; set; }
        [Key]
        public string Linea_Prestamo { get; set; }
        [Key]
        public string Linea_Credito_Cavipetrol { get; set; }
        [Key]
        public decimal Monto_Autorizado { get; set; }
        [Key]
        public int Tiempo_Amortizacion_Carta { get; set; }
        [Key]
        public string Tasa { get; set; }
        [Key]
        public string Fecha_Adjudicacion { get; set; }
        [Key]
        public string Fecha_Vigencia { get; set; }
        [Key]
        public string Fecha_Prorroga { get; set; }
    }
    // ENTIDADES PARA CONSULTAR LOS TIPOS DE GESTION </jarp>
    public class DatosCartasCrViviendaEcopetrolConsultaTipoGestion
    {
        [Key]
        public int Id_TipoGestion { get; set; }
        [Key]
        public string Nombre_TipoGestion { get; set; }
        [Key]
        public byte Estado_TipoGestion { get; set; }
    }

    public class DatosCartasCrViviendaEcopetrolConsultaMenu
    {
        [Key]
        public int Id_Formulario { get; set; }
        [Key]
        public string Nombre_Formulario { get; set; }
        [Key]
        public string Url_Formulario { get; set; }
        [Key]
        public Boolean Estado_Proceso { get; set; }
        [Key]
        public Boolean Gestion_Proceso { get; set; }
    }

    public class DatosCartasCrViviendaEcopetrolConsultaHistoricoBitacora
    {
        [Key]
        public int Id_HistoricoGestion { get; set; }
        [Key]
        public string Fecha_HistoricoGestion { get; set; }
        [Key]
        public string Nombre_TipoGestion { get; set; }
        [Key]
        public string UniqueName { get; set; }
        [Key]
        public string Observaciones_HistoricoGestion { get; set; }
    }

    public class DatosCartasCrViviendaEcopetrolConsultaREPORTES_SES
    {
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string NombreSolicitante { get; set; }
        public string TipoSolicitante { get; set; }
        public string Papel { get; set; }
        public string Registro { get; set; }
        public DateTime? FechaIngreso { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Ciudad { get; set; }
        public string EstadoCivil { get; set; }
        public string Regimen { get; set; } //TODO
        public string Cargo { get; set; }
        public string DireccionResidencia { get; set; }
        public string TelefonoResidencia { get; set; }
        public int? Estrato { get; set; } //TODO
        public string DireccionOficina { get; set; }
        public string TelefonoOficina { get; set; }
        public string CorreoCorporativo { get; set; }
        public string DistritoTrabajo { get; set; } //TODO??
        public string Profesion { get; set; }
        public string Ocupacion { get; set; }
        public string NumeroMovil { get; set; } //TODO
        public string Barrio { get; set; }
        public int? NumeroHijos { get; set; } //TODO
        public int? PersonasACargo { get; set; } //TODO
        public string CorreoPersonal { get; set; }
        public decimal? IngresosMensuales { get; set; }
        public decimal? EgresosMensuales { get; set; }
        public decimal? TotalActivos { get; set; }
        public decimal? TotalPasivos { get; set; }
        public int? SalarioActual { get; set; }
        public int? OtrosIngresos { get; set; } //TODO
        public DateTime? FechaUltimaActualizacion { get; set; } //TODO
        public double? AsegurabilidadPorcentajeExtraprima { get; set; }
        public DateTime? AsegurabilidadVigencia { get; set; }
        public string AsegurabilidadObservaciones { get; set; }
        public int? AsegurabilidadPagoAfiliado { get; set; }
        public string AsegurabilidadEstado { get; set; }
        public int? AsegurabilidadRechazado { get; set; }
        //public short? IdTipoRol { get; set; }
        public short? IdPapelCavipetrol { get; set; }
        public string TipoContrato { get; set; }

        public string OficinaOperacion { get; set; }
        public string Nomina { get; set; }
        public string CiudadOficina { get; set; }
        public string Ciiu { get; set; }
        public string Ciiu2 { get; set; }
        public int? Edad { get; set; }
        public int? Antiguedad { get; set; }
        public DateTime? FechaFinalizacionContrato { get; set; }
    }



    public class DatosCartasCrViviendaEcopetrolConsultaTipoProducto
    {
        public int Id_Producto { get; set; }
        public string Nombre_Producto { get; set; }
        public string Tipo_Producto { get; set; }
    }

    public class DatosCartasCrViviendaEcopetrolConsultaTipoNorma
    {
        public int Id_Norma { get; set; }
        public string Nombre_Norma { get; set; }
        public string Tipo_Norma { get; set; }
    }

    /// <summary>
    /// Modelo de las formas de pago
    /// </summary>
    public class DatosFormaPago
    {
        [Key]
        public int IdProductoNOrma { get; set; }
        [Key]
        public string Norma { get; set; }
        [Key]
        public string TipoPago { get; set; }
        [Key]
        public string Clave { get; set; }
        [Key]
        public string Producto { get; set; }
        [Key]
        public decimal PorcentajeInicial { get; set; }
        [Key]
        public decimal PorcentajeFinal { get; set; }
        [Key]
        public int IdConsecutivo { get; set; }
        [Key]
        public int Plazo { get; set; }
        [Key]
        public Decimal Valor { get; set; }
        [Key]
        public Decimal Cuota { get; set; }
    }


    public class DatosSolicitudISYNET
    {
        [Key]
        public int IdConsecutivo { get; set; }
        [Key]
        public Decimal MontoSolicitado { get; set; }
        [Key]
        public Decimal MontoCheque { get; set; }
        [Key]
        public Decimal MontoFAI { get; set; }
        [Key]
        public int IdSolicitudFYC { get; set; }
    }

    public class DatosHipoteca
    {
        [Key]
        public string HDireccion { get; set; }
        [Key]
        public decimal HAvaluoComercial { get; set; }
        [Key]
        public decimal H80AvaluoComercial { get; set; }
        [Key]
        public decimal HValorConstruccion { get; set; }
        [Key]
        public string HAvaluador { get; set; }
        [Key]
        public string HIdentificacion { get; set; }
        [Key]
        public string HMatriculaInmobiliaria { get; set; }
        [Key]
        public string HCiudad { get; set; }
        [Key]
        public string HNumeroEscritura { get; set; }
        [Key]
        public string HNotaria { get; set; }
        [Key]
        public string HFecha { get; set; }
        [Key]
        public string HCiudadNotaria { get; set; }
        [Key]
        public string HClaseHipoteca { get; set; }
        [Key]
        public string HFechaSeguros { get; set; }
        [Key]
        public string HFechaCobroECP { get; set; }
        [Key]
        public string HOtroCredito { get; set; }
        [Key]
        public string HSeguroVida { get; set; }
        [Key]
        public string HSeguroIncendio { get; set; }
        [Key]
        public int IdConsecutivo { get; set; }

    }

    public class DatosHipotecaPersistencia
    {
        [Key]
        public string HDireccion { get; set; }
        [Key]
        public decimal HAvaluoComercial { get; set; }
        [Key]
        public decimal H80AvaluoComercial { get; set; }
        [Key]
        public decimal HValorConstruccion { get; set; }
        [Key]
        public string HAvaluador { get; set; }
        [Key]
        public string HIdentificacion { get; set; }
        [Key]
        public string HMatriculaInmobiliaria { get; set; }
        [Key]
        public string HCiudad { get; set; }
        [Key]
        public string HNumeroEscritura { get; set; }
        [Key]
        public string HNotaria { get; set; }
        [Key]
        public DateTime HFecha { get; set; }
        [Key]
        public string HCiudadNotaria { get; set; }
        [Key]
        public string HClaseHipoteca { get; set; }
        [Key]
        public DateTime HFechaSeguros { get; set; }
        [Key]
        public DateTime HFechaCobroECP { get; set; }
        [Key]
        public string HOtroCredito { get; set; }
        [Key]
        public string HSeguroVida { get; set; }
        [Key]
        public string HSeguroIncendio { get; set; }
        [Key]
        public int IdConsecutivo { get; set; }
    }

    public class DatosCheque
    {
        [Key]
        public string Tipo_Registro { get; set; }
        [Key]
        public Boolean Activo { get; set; }
        [Key]
        public Boolean Habilitar_Eliminacion { get; set; }
        [Key]
        public string Tipo_Id_Destinatario { get; set; }
        [Key]
        public string Id_Destinatario { get; set; }
        [Key]
        public string Nombre_Destinatario { get; set; }
        [Key]
        public string Tipo_Id_Quien_Recibe { get; set; }
        [Key]
        public string Id_Quien_Recibe { get; set; }
        [Key]
        public string Nombre_Quien_Recibe { get; set; }
        [Key]
        public string Fecha_Desembolso { get; set; }
        [Key]
        public decimal Monto { get; set; }
        [Key]
        public string Fecha_Proceso { get; set; }
        [Key]
        public int IdConsecutivo { get; set; }
    }

    public class CreditoEcopetrolNotaContable
    {
        [Key]
        public Int32 ECPN_Consecutivo { get; set; }
        [Key]
        public Int32 ECPN_NumeroSolicitud { get; set; }
        [Key]
        public string ECPN_Soporte { get; set; }
        [Key]
        public Int32 ECPN_ConsecutivoSoporte { get; set; }
        [Key]
        public Int32 ECPN_Linea { get; set; }
        [Key]
        public string ECPN_Cuenta { get; set; }
        [Key]
        public Double ECPN_Valor_Debito { get; set; }
        [Key]
        public Double ECPN_Valor_Credito { get; set; }
        [Key]
        public string ECPN_TerceroTipoNit { get; set; }
        [Key]
        public string ECPN_TerceroNit { get; set; }
        [Key]
        public string ECPN_CUC_Descripcion { get; set; }
        [Key]
        public string ECPN_SubCentro { get; set; }
        [Key]
        public string ECPN_Tipo { get; set; }
        [Key]
        public Int32 ECPN_OperacionConsecutivo { get; set; }
        [Key]
        public string ECPN_OPT_Descripcion { get; set; }
        [Key]
        public string ECPN_Ciudad { get; set; }
        [Key]
        public string ECPN_Oficina { get; set; }
        [Key]
        public string ECPN_Usuario { get; set; }
    }


    public class CreditoEcopetrolOrdenGiro
    {
        [Key]
        public string TIPO_REGISTRO { get; set; }
        [Key]
        public int SEC_PAGO { get; set; }
        [Key]
        public int SEC_DETALLE { get; set; }
        [Key]
        public int SEC_REGISTRO { get; set; }
        [Key]
        public string TIPO { get; set; }
        [Key]
        public string NUMERO { get; set; }
        [Key]
        public string NOMBRE { get; set; }
        [Key]
        public string VALOR_PORC { get; set; }
        [Key]
        public int TIPO_DESTINO { get; set; }
        [Key]
        public int TIPO_CUENTA_DESTINO { get; set; }
        [Key]
        public string CUENTA_DESTINO { get; set; }
        [Key]
        public int BANCO_DESTINO { get; set; }
        [Key]
        public int SUCURSAL { get; set; }
        [Key]
        public string FECHA { get; set; }
        [Key]
        public string FACTURA { get; set; }
        [Key]
        public int NUM_PAGOS { get; set; }
        [Key]
        public string CONS_PAGO { get; set; }
        [Key]
        public string REST_SN { get; set; }
        [Key]
        public string GMF_SN { get; set; }
        [Key]
        public string PAIS { get; set; }
        [Key]
        public int DEPTO { get; set; }
        [Key]
        public int CIUDAD { get; set; }
        [Key]
        public int ESCENARIO { get; set; }
        [Key]
        public int MVTO { get; set; }
        [Key]
        public int MVTO_CXP { get; set; }
        [Key]
        public string C_COSTO { get; set; }
        [Key]
        public string SUBC_COSTO { get; set; }
        [Key]
        public string OBSERVACION { get; set; }

    }
    public class CreditoEcopetrolCreacionOrden
    {
        public int Id_Orden { get; set; }
        [Key]
        public string PI_Fk_Usuario { get; set; }
        [Key]
        public string PI_Fecha_Inicial { get; set; }
        [Key]
        public string PI_Fecha_Final { get; set; }
        [Key]
        public string PI_Total_Orden { get; set; }
    }

    public class CreditoEcopetrolReferenciaPago
    {
        [Key]
        public string BODY { get; set; }
    }
}