﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
	public class EstudioTitulos
	{
		public string ECPT_CiudadInmueble { get; set; }
		public string ECPT_CiudadExpedicionCedula { get; set; }
		public string ECPT_CiudadFirmaEscritura { get; set; }
		public string ECPT_NombreVendedor { get; set; }
		public string ECPT_CedulaDeudor { get; set; }
		public string ECPT_NombreDeudor { get; set; }
		public string ECPT_RegistroDeudor { get; set; }
		public string ECPT_DireccionInmueble { get; set; }
		public DateTime ECPT_FechaGeneracion { get; set; }
		public DateTime ECPT_FechaAdjudicacion { get; set; }
		public string ECPT_CiudadDeudor { get; set; }
		public string ECPT_MatriculaInmobiliaria { get; set; }
		public string ECPT_DescripcionMatricula { get; set; }
		public string ECPT_Notaria { get; set; }
		public string ECPT_NumeroEscritura { get; set; }
		public string ECPT_OficinaPublica { get; set; }
		public string ECPT_PlazoHipoteca { get; set; }
		public string ECPT_Producto { get; set; }
		public string ECPT_TasaEA { get; set; }
		public string ECPT_TiempoPrestamo { get; set; }
		public string ECPT_Tipo_Hipoteca { get; set; }
		public string ECPT_Usuario { get; set; }
		public string ECPT_ValorPrestado { get; set; }
		public string ECPT_Observaciones { get; set; }
		public string FK_ECPE_ESTADO { get; set; }
		public string FK_Consecutivo { get; set; }
	}
}
