﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
    public class DocumentosAsociadosCrViviendaEcopetrol
    {
        public int IdUso { get; set; }
        public int IdDocumento { get; set; }
        public int Consecutivo { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string Observacion { get; set; }
        public int Estado { get; set; }
    }
}
