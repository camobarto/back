﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CreditoViviendaECP
{
    public class LogProcesosCrViviendaEcopetrol
    {
     public string PI_IdUsuario { get; set; }
     public string PI_Id_Formulario { get; set; }
     public string PI_Numero_De_Identificacion { get; set; }
    }
}
