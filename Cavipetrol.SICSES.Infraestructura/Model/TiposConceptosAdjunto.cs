﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
   public class TiposConceptosAdjunto
    {
        public int IdConceptoAdjunto { get; set; }
        public string Concepto { get; set; }
        public string Descripcion { get; set; }
        public string TipoVariable { get; set; }
        public bool Visible { get; set; }
        public bool Editable { get; set; }
        public string Valor { get; set; }
    }
}
