﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TiposTitulos
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
