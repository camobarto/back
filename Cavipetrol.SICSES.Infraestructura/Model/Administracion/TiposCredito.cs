﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Administracion
{
    public class TiposCredito
    {
        public short IdTipoCredito { get; set; }
        public string Nombre { get; set; }
        public int IdTipoLineaCredito { get; set; }

        [NotMapped]
        public string NombreLinea { get; set; }

    }
}
