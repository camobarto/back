﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Administracion
{
   public  class PapelCavipetrol
    {
        public short IdPapelCavipetrol { get; set; }
        public string Nombre { get; set; }

        public string Descripcion { get; set; }
        public short IdTipoContrato { get; set; }
    }
}
