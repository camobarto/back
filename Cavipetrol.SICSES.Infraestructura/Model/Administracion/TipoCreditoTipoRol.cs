﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Administracion
{
   public class TipoCreditoTipoRol
    {
        public int IdTipoCreditoTipoRolRelacion { get; set; }
        public short IdTipoCredito { get; set; }
        public short IdPapelCavipetrol { get; set; }
        public string PlazoMaximo { get; set; }
        public short IdTipoCupoMaximo { get; set; }
        public string CupoMaximo { get; set; }
        public string InteresNominalQuincenaVendida { get; set; }
        public string InteresEfectivoAnual { get; set; }
        public string PorcentajeNovacion { get; set; }
        public string TiempoMinimoDeAfiliacionAnhos { get; set; }
        public string PlazoMinimo { get; set; }
        public string IdProductoFYC { get; set; }
        public int Bandera { get; set; }
        public string Usuario { get; set; }


    }
}
