﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoCreditoTipoRolRelacion
    {
        public int IdTipoCreditoTipoRolRelacion { get; set; }
        public short IdTipoCredito { get; set; }
        public short IdPapelCavipetrol { get; set; }
        /// <summary>
        /// Plazo en quincenas
        /// </summary>
        public double? PlazoMaximo { get; set; }
        public short? IdTipoCupoMaximo { get; set; }
        public double? CupoMaximo { get; set; }
        public double? InteresNominalQuincenaVendida { get; set; }
        public double? InteresEfectivoAnual { get; set; }
        public double? PorcentajeNovacion { get; set; }
        public double? TiempoMinimoDeAfiliacionAnhos { get; set; }
        /// <summary>
        /// Plazo en quincenas
        /// </summary>
        public double? PlazoMinimo { get; set; }
        public string IdProductoFYC { get; set; }
        public Boolean Activo { get; set; }
        public int TipoVehiculo { get; set; }
        public string Norma { get; set; }
    }
}
