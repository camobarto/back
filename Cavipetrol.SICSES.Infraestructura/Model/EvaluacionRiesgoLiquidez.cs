﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class EvaluacionRiesgoLiquidez    {

        public int UNIDAD_CAPTURA { get; set; }
        public int CODIGO_RENGLON { get; set; }
        public string DESCRIPCION_RENGLON { get; set; }
        public double? SALDO_A_LA_FECHA { get; set; }
        public double? PRIMER_MES { get; set; }
        public double? DE_1_A_2_MESES { get; set; }
        public double? DE_2_A_3_MESES { get; set; }
        public double? DE_3_A_6_MESES { get; set; }
        public double? DE_6_A_9_MESES { get; set; }
        public double? DE_9_A_12_MESES { get; set; }
        public double? MAYOR_DE_12_MESES { get; set; }
        public int ID_EVALUACION_RIESGO_LIQUIDEZ { get; set; }

    }
}
