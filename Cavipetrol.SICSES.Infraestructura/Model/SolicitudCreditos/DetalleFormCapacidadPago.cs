﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class DetalleFormCapacidadPago
    {
        public string Descripcion { get; set; }
        public string Valor { get; set; }
    }
}
