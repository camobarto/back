﻿namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos.Consulta
{
    public class DetalleNorma
    {        
        public int PlazoMinimo { get; set; }
        public double PlazoMaximo { get; set; }
        public string Tipo { get; set; }
        public double MontoMinimo { get; set; }
        public double MontoMaximo { get; set; }
        public int Clave { get; set; }
        public string Equivalencia { get; set; }
        public double Tasa { get; set; }
    }
}
