﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class ExtraPrima
    {
        public double TEP_PorcExtraprima { get; set; }
        public string TEP_Anotacion { get; set; }
        public int TEP_PagoAfiliado { get; set; }
    }
}
