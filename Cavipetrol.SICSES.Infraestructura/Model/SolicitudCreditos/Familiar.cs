﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
   public  class Familiar
    {
        public string TipoIdentificacion { get; set; }
        public int NumeroIdentificacion { get; set; }
        public string NombresApellidos { get; set; }
        public string DireccionResidencia { get; set; }
        public int Telefono { get; set; }
        public string CorreoElectronico { get; set; }
        public string Parentesco { get; set; }
        public string TipoIdentificacionAsociado { get; set; }
        public string NumeroIdentificacionAsociado { get; set; }
    }
}
