﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{    
    public class Amortizacion
    {
        public int Periodo {get;set;}
        public string Capital {get;set;}
        public string Intereses {get;set;}
        public string Cuota {get;set;}
        public string Saldo {get;set;}
        public string CapitalAcumulado {get;set;}
        public string Abonos {get;set;}
        public string Seguro { get; set; }
        public string SeguroVida { get; set; }
        public string SeguroIncendio { get; set; }        
    }
}
