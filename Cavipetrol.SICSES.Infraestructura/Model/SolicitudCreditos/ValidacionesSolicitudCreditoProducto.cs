﻿using System;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class ValidacionesSolicitudCreditoProducto
    {
        public DateTime FechaDesembolso { get; set; }
        public string Producto { get; set; }
        public double Saldo { get; set; }
        public int PlazoMaximo { get; set; }
        public bool CumpleCaviAlDia { get; set; }
        public bool CumpleFechaLimite { get => FechaDesembolso.AddYears(PlazoMaximo) <= DateTime.Now; }
        public bool CumpleSaldo { get; set; }
    }
}
