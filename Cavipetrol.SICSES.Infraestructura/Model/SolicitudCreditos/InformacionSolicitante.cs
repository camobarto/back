﻿using System;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class InformacionSolicitante
    {
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string NombreSolicitante { get; set; }
        public string TipoSolicitante { get; set; }
        public string Papel { get; set; }
        public string Registro { get; set; }
        public DateTime? FechaIngreso { get; set; }  
        public DateTime? FechaNacimiento { get; set; }
        public string Ciudad { get; set; }
        public string EstadoCivil { get; set; }
        public string Regimen { get; set; } //TODO
        public string Cargo { get; set; } 
        public string DireccionResidencia { get; set; } 
        public string TelefonoResidencia { get; set; }
        public int? Estrato { get; set; } //TODO
        public string DireccionOficina { get; set; }
        public string TelefonoOficina { get; set; }
        public string CorreoCorporativo { get; set; } 
        public string DistritoTrabajo { get; set; } //TODO??
        public string Profesion { get; set; }
        public string Ocupacion { get; set; }
        public string NumeroMovil { get; set; } //TODO
        public string Barrio { get; set; }
        public int? NumeroHijos { get; set; } //TODO
        public int? PersonasACargo { get; set; } //TODO
        public string CorreoPersonal { get; set; }
        public decimal? IngresosMensuales { get; set; } 
        public decimal? EgresosMensuales { get; set; }
        public decimal? TotalActivos { get; set; }
        public decimal? TotalPasivos { get; set; }
        public int? SalarioActual { get; set; }
        public int? OtrosIngresos { get; set; } //TODO
        public DateTime? FechaUltimaActualizacion { get; set; } //TODO
        public List<InformacionProductosCreditos> Productos { get; set; }
        public double? AsegurabilidadPorcentajeExtraprima { get; set; }
        public DateTime? AsegurabilidadVigencia { get; set; }
        public string AsegurabilidadObservaciones { get; set; }
        public int? AsegurabilidadPagoAfiliado { get; set; }
        public string AsegurabilidadEstado { get; set; }
        public int? AsegurabilidadRechazado { get; set; }
        //public short? IdTipoRol { get; set; }
        public short? IdPapelCavipetrol { get; set; }
        public string TipoContrato { get; set; }

        public string OficinaOperacion { get; set; }
        public string Nomina { get; set; }
        public string CiudadOficina { get; set; }
        public string Ciiu { get; set; }
        public string Ciiu2 { get; set; }
        public int? Edad { get; set; }
        public int? Antiguedad { get; set; }
        public DateTime? FechaFinalizacionContrato { get; set; }
        public string LugarExpedicionCedula { get; set; }        
    }
}
