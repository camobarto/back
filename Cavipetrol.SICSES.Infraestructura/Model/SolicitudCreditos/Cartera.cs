﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class Cartera
    {
        public double? ValorCartera { get; set; }
        public double? ValorUltimoAporte { get; set; }
        public DateTime? FechaUltimoAporte { get; set; }
        public bool CumpleCartera { get; set; }
        public bool CumpleAportes { get; set; }
    }
}
