﻿

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class SaldosColocacion
    {
        public string Documento_tipo { get; set; }
        public int Documento_Numero { get; set; }
        public string Norma { get; set; }
        public string Producto { get; set; }
        public double Saldo { get; set; }
        public double Valor_prestado { get; set; }
    }
}
