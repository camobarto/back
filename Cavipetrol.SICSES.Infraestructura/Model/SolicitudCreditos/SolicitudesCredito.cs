﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class SolicitudesCredito
    {
        public int Solicitud { get; set; }
        public long? ConsecutivoSolicitud { get; set; }
        public Int16 Id_Tipo_Credito { get; set; }
        public string Tipo_Credito { get; set; }
        public string Tipo_Linea_Credito { get; set; }
        public string Tipo_Identificacion { get; set; }
        public string Numero_Identificion { get; set; }
        public string Estado { get; set; }
        public DateTime? Creacion { get; set; }
        public string Usuario { get; set; }
    }
}
