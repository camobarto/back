﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class HorasExtras
    {
        public IEnumerable<Etiquetas> Desprendible1 { get; set; }
        public IEnumerable<Etiquetas> Desprendible2 { get; set; }
        public IEnumerable<Etiquetas> Desprendible3 { get; set; }
        public IEnumerable<Etiquetas> Desprendible4 { get; set; }
    }
}
