﻿namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class ValidacionesSolicitudCreditoUsuario
    {
        public bool EstaEnListaClinton { get; set; }
        public bool TieneEmbargo { get; set; }
        public bool EstaEnListaITP { get; set; }
    }
}
