﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class Garantia
    {

        public int? FSC_NUMSOLICITUD { get; set; }
        public string FSC_TNITAFLIADO { get; set; }
        public string FSC_NITAFILIADO { get; set; }
        public string FSC_NOMAFILIADO { get; set; }
        public string FSC_CIUDAD { get; set; }
        public string FSC_PRODUCTO { get; set; }
        public string FSC_NORMA { get; set; }
        public Double? FSC_SALDO { get; set; }

    }
}
