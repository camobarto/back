﻿using System.Collections.Generic;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class NormaProductos
    {
        public NormaProductos()
        {
            Vencimientos = new List<GrupoVencimientoNorma>();
        }
        public string Norma { get; set; }
        public double? Tasa { get; set; }
        public string Tipo { get; set; }
        public string TipoPlazo { get; set; }
        public int PlazoMinimo { get; set; }
        public int PlazoMaximo { get; set; }
        public double MontoMinimo { get; set; }
        public double MontoMaximo { get; set; }
        public string Descripcion { get; set; }
        public List<GrupoVencimientoNorma> Vencimientos { get; set; }
    }
}
