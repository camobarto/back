﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class Interproductos
    {
        public string NombreProducto { get; set; }
        public decimal Valor { get; set; }
        public string ValorACruzar { get; set; }
    }
}
