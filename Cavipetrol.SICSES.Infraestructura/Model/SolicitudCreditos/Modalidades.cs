﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{

    public class Modalidades
    {
        public Modalidades()
        {
            TipoModalidades = new TipoModalidades();
            ModalidadesMontoValorCuota = new List<ModalidadesMontoValorCuota>();
            ModalidadesClavePlazo = new List<ModalidadesClavePlazo>();
        }
        public double? Tasa { get; set; }
        public int PlazoMinimoAnhos { get; set; }
        public int PlazoMaximoAnhos { get; set; }
        public double MontoMinimo { get; set; }
        public double MontoMaximo { get; set; }
        public TipoModalidades TipoModalidades { get; set; }
        public List<ModalidadesMontoValorCuota> ModalidadesMontoValorCuota { get; set; }
        public List<ModalidadesClavePlazo> ModalidadesClavePlazo { get; set; }
    }


    public class TipoModalidades
    {
        public TipoModalidades()
        {
            EsQuincenal = false;
            EsMensual = false;
            EsTrimestral = false;
            EsSemestral = false;
            EsAnual = false;
        }
        public bool EsQuincenal { get; set; }
        public bool EsMensual { get; set; }
        public bool EsTrimestral { get; set; }
        public bool EsSemestral { get; set; }
        public bool EsAnual { get; set; }
    }

    public class ModalidadesMontoValorCuota
    {
        public ModalidadesMontoValorCuota()
        {
            Quincenal = 0.0;
            Mensual = 0.0;
            Trimestral = 0.0;
            Semestral = 0.0;
            Anual = 0.0;
        }
        public double Quincenal { get; set; }
        public double Mensual { get; set; }
        public double Trimestral { get; set; }
        public double Semestral { get; set; }
        public double Anual { get; set; }
    }

    public class ModalidadesClavePlazo
    {
        public ModalidadesClavePlazo()
        {
            Quincenal = 0;
            Mensual = 0;
            Trimestral = 0;
            Semestral = 0;
            Anual = 0;
        }
        public int Quincenal { get; set; }
        public int Mensual { get; set; }
        public int Trimestral { get; set; }
        public int Semestral { get; set; }
        public int Anual { get; set; }
    }

}
