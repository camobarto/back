﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
   public class CreditoEcopetrolBitacora
    {
        public long? BitConsecutivo { get; set; }
        public long? BitIdTipoGestion { get; set; }
        public string BitIdUsuario { get; set; }
        public long? BitNumeroIdentificacion { get; set; }
        public string BitObservaciones { get; set; }
    }

    public class CreditoEcopetrolAsegurabilidad
    {
        public string AsegConsecutivo { get; set; }
        public string AsegIdUsuario { get; set; }
        public string AsegNumeroIdentificacion { get; set; }
        public string AsegEstado_Asegurabilidad { get; set; }
        public string AsegVigencia_Asegurabilidad { get; set; }
        public string AsegExtraPrima_Asegurabilidad { get; set; }
        public string AsegRechazoAplazo_Asegurabilidad { get; set; }
        public string AsegObservaciones_Asegurabilidad { get; set; }
        public string AsegObservaciones_Gestion_Asegurabilidad { get; set; }

    }

    public class CreditoEcopetrolAdjudicacion
    {
        public string AdjConsecutivo { get; set; }
        public string AdjIdUsuario { get; set; }
        public string AdjId_Producto { get; set; }
        public string AdjId_Norma { get; set; }
        public string AdjNumero_Identificacion { get; set; }
        public string AdjPapel_Adjudicacion { get; set; }
        public string AdjRegistro_Adjudicacion { get; set; }
        public string AdjActa_Adjudicacion { get; set; }
        public string AdjTipo_Adjudicacion { get; set; }
        public string AdjValor_Adjudicacion { get; set; }
        public string AdjFecha_Generacion { get; set; }

    }
    public class CreditoEcopetrolEstudio
    {
        public string EstConsecutivo { get; set; }
        public string EstDocumento { get; set; }
        public string EstEstado { get; set; }
    }

    public class CreditoEcopetrolValorAdjudicacion
    {
        public string Valor_Adjudicacion { get; set; }
    }

    public class CreditoEcopetrolValorCheques
    {
        public string MontoRestanteCheque { get; set; }
    }

    public class CreditoEcopetrolPagare
    {
        public int Comodin { get; set; }
        public string Numero_Identificacion { get; set; }
        public string IdUsuario { get; set; }
        public int Id_TipoPagare { get; set; }
        public int Numero_Solicitud { get; set; }
        public string Id_Pagare { get; set; }
        public string Campo1_PagDes { get; set; }
        public string Campo2_PagDes { get; set; }
        public string Campo3_PagDes { get; set; }
        public string Campo4_PagDes { get; set; }
        public string Campo5_PagDes { get; set; }
        public string Campo6_PagDes { get; set; }
        public string Campo7_PagDes { get; set; }
        public string Campo8_PagDes { get; set; }
        public string Campo9_PagDes { get; set; }
        public string Campo10_PagDes { get; set; }
        public string Campo11_PagDes { get; set; }
        public string Campo12_PagDes { get; set; }
        public string Campo13_PagDes { get; set; }
        public string Campo14_PagDes { get; set; }
        public string Campo15_PagDes { get; set; }
        public string Campo16_PagDes { get; set; }
        public string Campo17_PagDes { get; set; }
        public string Campo18_PagDes { get; set; }
        public string Campo19_PagDes { get; set; }
        public string Campo20_PagDes { get; set; }
        public string Campo21_PagDes { get; set; }
        public string Campo22_PagDes { get; set; }
        public string Campo23_PagDes { get; set; }
        public string Campo24_PagDes { get; set; }
        public string Campo25_PagDes { get; set; }
        public string Campo26_PagDes { get; set; }
        public string Campo27_PagDes { get; set; }
        public string Campo28_PagDes { get; set; }
        public string Campo29_PagDes { get; set; }
        public string Campo30_PagDes { get; set; }

    }
}
