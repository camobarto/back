﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class Moras
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Identificacion { get; set; }
        public DateTime Fecha_ultimo_aporte { get; set; }
        public int Dias_mora { get; set; }
        public int Sancion { get; set; }
    }
}
