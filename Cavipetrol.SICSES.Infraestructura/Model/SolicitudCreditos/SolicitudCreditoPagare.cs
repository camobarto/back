﻿using Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class SolicitudCreditoPagare
    {
        public int? idSolicitudCreditoAdjunto { get; set; }
        public int? idSolicitud { get; set; }
        public int? idPagare { get; set; }
        public string numeroPagare { get; set; }
        public DateTime? fechaCreacion { get; set; }
        public DateTime? fechaModificacion { get; set; }
        public string usuarioCreacion { get; set; }
        public string usuarioModificacion { get; set; }
        public int? activo { get; set; }
        public string jsonConceptos { get; set; }
        public double? tasaPactadaEA { get; set; }
        public string variable { get; set; }
        public string ciudad { get; set; }
        public string ciudadAdjunto { get; set; }
        public string tipoNitBeneficiario { get; set; }
        public string nitBeneficiario { get; set; }
        public DateTime? fecha { get; set; }
        public int? variableValor { get; set; }
        public List<VMConceptoGarantiaCredito> listaConceptosGarantia { get; set; }
    }
}
