﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
   public class ModelCartaSolicitudCredito
    {
        public string titulo { get; set; }
        public decimal ValorSolicitado { get; set; }

        public string valorNegociado { get; set; }

        public string FormCapacidad { get; set; }
        public int plazoMeses { get; set; }
        public double tasaEA { get; set; }
        public string tipoCredito { get; set; }
        public string documento { get; set; }
        public decimal ValorFai  { get; set; }
        public string cuentaFAI { get; set; }
        public string girarCheque { get; set; }
        public decimal ValorCheque { get; set; }
        public decimal ValorInterproductos  { get; set; }
        public string cruceInterproductos { get; set; }
        public decimal saldoAnterior { get; set; }
        public string tipoGarantia { get; set; }
        public string papel { get; set; }
        public string documentoAsociado {get; set; }
        public string nombreAsociado { get; set; }
        public string registro { get; set; }
        public string nomina { get; set; }
        public string regimen { get; set; }
        public string fechaNacimiento { get; set; }
        public string profesion { get; set; }
        public string cargo { get; set; }
        public string ocupacion { get; set; }
        public string estadoCivil { get; set; }
        public string direccionResidencia { get; set; }
        public string ciudadResidencia { get; set; }
        public string telefonoResidencia { get; set; }
        public string celular { get; set; }
        public string estrato { get; set; }
        public string barrio { get; set; }
        public string direccionOficina { get; set; }
        public string ciudadOficina { get; set; }
        public string telefonoOficina { get; set; }
        public string numeroHijos { get; set; }
        public string correoCorporativo { get; set; }
        public string correoPersonal { get; set; }
        public decimal ingresos { get; set; }
        public string ingresosMensuales { get; set; }
        public string  egresosMensuales  { get; set; }
        public decimal egresos { get; set; }

        public string  totalActivos { get; set; }
        public decimal Activos { get; set; }

        public string totalPasivos  { get; set; }
        public decimal Pasivos { get; set; }
        public int salario { get; set; }

        public string  salarioActual { get; set; }
        public string otrosIngresos { get; set; }
        public long numeroSolicitud { get; set; }
        public DateTime fecha { get; set; }
        public String ciudad { get; set; }
        public string dia { get; set; }
        public string mes { get; set; }
        public string anho { get; set; }
        public string destinacionCredito { get; set; }
        public string tipoLiniea { get; set; }
        public decimal monto { get; set; }
        public string usuario { get; set; }
        public string formaPago { get; set; }
        public int numeroRadicado { get; set; }
        public int idLinea { get; set; }
        public string NombreReferencia { get; set; }
        public string NumeroReferencia { get; set; }
        public string Relacion { get; set; }
        public int plazoAnhos { get; set; }
        public double TEA { get; set; }
        public double tNominal { get; set; }
        public byte? TipoReferencia { get; set; }
        public List<ReferenciasPersonales> referenciaPersonal { get; set; }

        public List<ReferenciasFamiliares> referenciaFamiliar { get; set; }










    }
}
