﻿
namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class InformacionProductosCreditos
    {
        public string NombreProducto { get; set; }
        public string NombreDocumento { get; set; }
        public double SaldoActual { get; set; }
        public double ValorACruzar { get; set; }
        public string DocumentoTipo { get; set; }
        public string DocumentoNumero { get; set; }
        public string ConsecutivoSolicitud { get; set; }



        public string Documento_tipo { get; set; }
        public string Documento_Numero { get; set; }
        public string Producto { get; set; }
        public string Saldo { get; set; }
        public string Valor_prestado { get; set; }
    }
}


