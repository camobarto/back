﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{

    public class CapacidadPago
    {
        public CapacidadPago()
        {
            Ingresos = new Ingresos();
            Deducciones = new Deducciones();
            CuotasProductosCavipetrol = new List<CuotasProductosCavipetrol>();
        }

        public float TotalDeduccionesLey { get; set; }
        public float NetoSalario { get; set; }
        public float MaximoDescuentoPorNomina { get; set; }
        public Ingresos Ingresos { get; set; }
        public Deducciones Deducciones { get; set; }
        public List<CuotasProductosCavipetrol> CuotasProductosCavipetrol { get; set; }
    }

    public class Deducciones
    {
        public float Salud { get; set; }
        public float Pension { get; set; }
        public float FondoSolidaridad { get; set; }
        public float Otros { get; set; }
        public float OtrasDeducciones { get; set; }
        public float GastosFinancieros { get; set; }

    }

    public class Ingresos
    {

        public float SalarioQuincenal { get; set; }
        public float AuxilioAlimentacion { get; set; }
        public float Otros { get; set; }
  
    }

    public class CuotasProductosCavipetrol
    {
        public string Producto { get; set; }
        public float Cuota { get; set; }
    }

}
