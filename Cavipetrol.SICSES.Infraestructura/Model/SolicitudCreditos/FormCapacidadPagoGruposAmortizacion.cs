﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class FormCapacidadPagoGruposAmortizacion
    {
        public List<FormCapacidadPagoGrupos> ListaFormCapacidadPagoGrupos { get; set; }
        public List<Amortizacion> TablaAmortizacion { get; set; }
        public List<TiposSeguro> TablaSeguros { get; set; }
        public List<TiposCreditoTiposGarantiaRelacion> TablaGarantias { get; set; }
    }
}
