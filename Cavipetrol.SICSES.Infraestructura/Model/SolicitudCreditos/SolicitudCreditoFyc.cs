﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class SolicitudCreditoFyc
    {
        public int? fscNumsolicitud { get; set; }
        public string fscCiudad { get; set; }
        public string fscOficina { get; set; }
        public string fscTnitafliado { get; set; }
        public string fscNitafiliado { get; set; }
        public string fscNomafiliado { get; set; }
        public string fscProducto { get; set; }
        public string fscDocumento { get; set; }
        public string fscNorma { get; set; }
        public string fscTipogarantia { get; set; }
        public DateTime fscFecha { get; set; }
        public double? fscMquincenal { get; set; }
        public double? fscMmensual { get; set; }
        public double? fscMsemestral { get; set; }
        public double? fscMtrimestral { get; set; }
        public double? fscManual { get; set; }
        public double? fscSaldo { get; set; }
        public double? fscSaldonormalizado { get; set; }
        public double? fscPlazoanhos { get; set; }
        public int? fscClavequincenal { get; set; }
        public int? fscClavemensual { get; set; }
        public int? fscClavesemestral { get; set; }
        public int? fscClavetrimestral { get; set; }
        public int? fscClaveanual { get; set; }
        public short? fscDesembolsofai { get; set; }
        public short? fscDesembolsocheque { get; set; }
        public short? fscDesembolsocruze { get; set; }
        public string fscUsuario { get; set; }
        public string fscEstado { get; set; }
        public string fscDescripcion { get; set; }
        public string fscUsuariocontrol { get; set; }
        public double? fscValorcuentafai { get; set; }
        public double? fscValorcheque { get; set; }
        public double? fscValorinterproduc { get; set; }
        public double? fscTasa { get; set; }
        public short? fscMesada { get; set; }
        public string fscObservacion { get; set; }
        public int? fscPlazoquincenal { get; set; }
        public int? fscPlazomensual { get; set; }
        public int? fscPlazosemestral { get; set; }
        public int? fscPlazotrimestral { get; set; }
        public int? fscPlazoanual { get; set; }
        public double? fscVcuotaquincenal { get; set; }
        public double? fscVcuotamensual { get; set; }
        public double? fscVcuotasemestral { get; set; }
        public double? fscVcuotatrimestral { get; set; }
        public double? fscVcuotanual { get; set; }
        public short? fscPagaremayor { get; set; }

    }
}
