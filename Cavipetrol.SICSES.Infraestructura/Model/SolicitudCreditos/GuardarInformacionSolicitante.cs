﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class GuardarInformacionSolicitante
    {
        public string nitAfiliado { get; set; }
        public string registro { get; set; }
        public string fechaNacimiento { get; set; }
        public string direccionResidencia { get; set; }
        public string ciudadResidencia { get; set; }
        public string telefonoResidencia { get; set; }
        public string celular { get; set; }
        public string correoResidencia { get; set; }
        public string correoOficina { get; set; }
        public string barrio { get; set; }
        public string nomina { get; set; }
        public string papel { get; set; }
        public string ocupacion { get; set; }
        public string profesion { get; set; }
        public string cargo { get; set; }
        public string direccOficina { get; set; }
        public string ciudadOficina { get; set; }
        public string telefonoOficina { get; set; }
        public string estadoCivil { get; set; }
        public string regimen { get; set; }
        public string personaCargo { get; set; }
        public string estrato { get; set; }
        public string ingreMensuales { get; set; }
        public string egreMensuales { get; set; }
        public string activos { get; set; }
        public string pasivos { get; set; }
        public string salarioActual { get; set; }
        public string otrosIngresos { get; set; }
        public string numeroHijos { get; set; }
        public string cabezaHogar { get; set; }
        public string tipoContrato { get; set; }
        public string actividadPrincipal { get; set; }
        public string actividadSecundaria { get; set; }
        public string usuario { get; set; }
        public string tipoIdentificacion { get; set; }

    }
}
