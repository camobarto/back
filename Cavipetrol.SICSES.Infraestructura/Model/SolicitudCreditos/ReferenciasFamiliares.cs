﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
   public class ReferenciasFamiliares
    {
        public string NombreReferencia { get; set; }

        public string parentesco { get; set; }

        public string NumeroReferencia { get; set; }
    }
}
