﻿namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class InformacionAsegurabilidad
    {
        public double? TasaExtraprima { get; set; }        
        public string NumeroIdentificacion { get; set; }      
        public int? Asegurabilidad { get; set; }
        public bool EsAsegurable { get; set; }        

        public void ValidarInformacionDeAsegurabilidad() {
            if (!TasaExtraprima.HasValue) {
                TasaExtraprima = 0;
            }


            if (string.IsNullOrEmpty(NumeroIdentificacion)) {
                EsAsegurable = true;
            }
            else
            {
                if(Asegurabilidad > 0)
                    EsAsegurable = true;
            }
        }
    }
}
