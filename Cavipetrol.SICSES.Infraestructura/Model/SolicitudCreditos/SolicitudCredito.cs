﻿using System;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class SolicitudCredito
    {
        public int NumeroSolicitud { get; set; }
        public string Ciudad { get; set; }
        public string Oficina { get; set; }
        public string TipoSolicitante { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string NombreSolicitante { get; set; }
        public string NombreProducto { get; set; }
        public string NombreDocumento { get; set; }
        public string NombreNorma { get; set; }
        public string TipoGarantia { get; set; }
        public DateTime FechaSolicitud { get; set; }
    }
}
