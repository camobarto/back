﻿using System;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class InformacionSolicitanteCampanaEspecial
    {
        public string CEP_TipoIdentificacion { get; set; }
        public string CEP_IdIdentificacion { get; set; }
        public string CEP_Nombre { get; set; }
        public string CEP_Ciudad { get; set; }
        public string CEP_Oficina { get; set; }
        public double CEP_SalarioBase { get; set; }
        public double? CEP_SalarioAprobado { get; set; }
        public string CEP_Papel { get; set; }
        public int CEP_IdPapel { get; set; }
        public double CEP_CupoCredito { get; set; }
        public double? CEP_CupoCreditoUtilizado { get; set; }
        public string CEP_Campana { get; set; }
        public DateTime CEP_FechaCargue { get; set; }
        public Int32 CEP_TipoRolRelacion { get; set; }
        public string CEP_Norma { get; set; }
        public double CEP_PlazoMaximo { get; set; }
        public double CEP_IV { get; set; }
        public double CEP_IEA { get; set; }
        public Int16 CEP_IdCredito { get; set; }
        public string CEP_ProductoFYC { get; set; }
        public double CEP_PlazoMinimo { get; set; }
        public Int32 CEP_Clave { get; set; }
        public Int64 CEP_Control { get; set; }
    }
}
