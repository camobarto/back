﻿namespace Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos
{
    public class GrupoVencimientoNorma
    {
        public string Equivalencia { get; set; }
        public int Clave { get; set; }
    }
}
