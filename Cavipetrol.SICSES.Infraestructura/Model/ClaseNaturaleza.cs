﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class ClaseNaturaleza
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
