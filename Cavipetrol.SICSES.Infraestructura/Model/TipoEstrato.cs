﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoEstrato
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
