﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Fodes
{
	public class InfoFodes
	{
		public double TasaSubsidio { get; set; }
		public double TasaEASinSubsidio { get; set; }
		public double TasaEAConSubsidio { get; set; }
		public double TasaNominalConSubsidio { get; set; }
		public double TasaNominalSinSubsidio { get; set; }
		         
	}
}
