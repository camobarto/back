﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Fodes
{
	public class FodesGenerico
	{
		public string AcuerdoTipo { get; set; }
		public string NumeroDocumento { get; set; }
		public int NumeroUnico { get; set; }
		public string TipoIdentificacion { get; set; }
		public string Identificacion { get; set; }
		public string Descripcion { get; set; }
	}
}
