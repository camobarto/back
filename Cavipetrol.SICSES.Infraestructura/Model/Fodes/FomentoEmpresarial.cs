﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Fodes
{
	public class FomentoEmpresarial
	{
		public int NumeroDocumento { get; set; }
		public int NumeroUnico { get; set; }
		public string AcuerdoTipo { get; set; }
		public string Saldo { get; set; }
		public double Intereses { get; set; }
		public string TipoIdentificacion { get; set; }
		public string Identificacion { get; set; }
		public string Nombre { get; set; }
		public string Papel { get; set; }
		public string Ciudad { get; set; }
		public string Servidor { get; set; }
	}
}
