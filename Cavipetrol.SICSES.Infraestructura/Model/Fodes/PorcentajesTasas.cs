﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Fodes
{
	public class PorcentajesTasas
	{
		public string CIN_BaseInterés { get; set; }
		public double CIN_Coeficiente { get; set; }
		public DateTime CIN_Fecha { get; set; }
		public string CIN_V_Fecha { get; set; }
		
	}
}
