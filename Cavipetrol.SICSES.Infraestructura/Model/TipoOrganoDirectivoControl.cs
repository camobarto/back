﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoOrganoDirectivoControl
    {
        public int Id { get; set; }
        public int NumeroSuplentes { get; set; }
        public int NumeroPrincipales { get; set; }
        public string Descripcion { get; set; }
        public string EquivalenteCavipetrol { get; set; }
        public string TipoPersona { get; set; }
    }
}
