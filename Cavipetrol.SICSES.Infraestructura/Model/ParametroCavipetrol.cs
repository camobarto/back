﻿using Cavipetrol.SICSES.Infraestructura.Model.Reportes;
using System;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class ParametroUsuarioCavipetrol
    {
        public int Id { get; set; }
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public string Sigla { get; set; }
        public int GrupoNormaTecnica { get; set; }
        public string NIT { get; set; }
        public DateTime FechaConstitucion { get; set; }
        public string Direccion { get; set; }
        public int Telefono { get; set; }
        public int? Fax { get; set; }
        public string IdDepartamento { get; set; }
        public string IdMunicipio { get; set; }
        public ICollection<ReporteIdentificacion> ReportesIdentificacion { get; set; }
        
    }
}
