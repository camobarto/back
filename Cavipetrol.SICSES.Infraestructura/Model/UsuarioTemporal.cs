﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class UsuarioTemporal
    {
        public string Usuario { get; set; }
        public string Contrasena { get; set; }
    }
}
