﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Seguridad
{
    public class Usuario
    {
        public int IdUsuario { get; set; }
        public string UniqueName { get; set; }
        public string Contrasena { get; set; }
        public int IdPerfil { get; set; }
        public int IdOficina { get; set; }
        public string IP { get; set; }
        public bool Estado { get; set; }
        public virtual Perfil Perfil { get; set; }
        [NotMapped]
        public string IdMunicipio  { get; set; }
        [NotMapped]
        public string NombreOficina  { get; set; }
        [NotMapped]
        public string NombreMunicipio { get; set; }
    }
}
