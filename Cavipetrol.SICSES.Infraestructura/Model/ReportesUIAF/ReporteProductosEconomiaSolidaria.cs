﻿using System.Runtime.Serialization;


namespace Cavipetrol.SICSES.Infraestructura.Model.ReportesUIAF
{
    [DataContract(Name = "ReporteProductosEconomiaSolidaria")]
    public class ReporteProductosEconomiaSolidaria                   
    {
     [DataMember(Name = "CONSECUTIVO")]
      public int? CONSECUTIVO { get; set; }

    [DataMember(Name = "NUMERO_PRODUCTO")]
        public int? NUMERO_PRODUCTO { get; set; }

        [DataMember(Name = "FECHA_VINCULACION")]
        public string FECHA_VINCULACION { get; set; }

        [DataMember(Name = "TIPO_PRODUCTO")]
        public int? TIPO_PRODUCTO { get; set; }

        [DataMember(Name = "CODIGO_DEPARTAMENTO")]
        public string CODIGO_DEPARTAMENTO { get; set; }

        [DataMember(Name = "TIPO_IDENTIFICACION")]
        public int? TIPO_IDENTIFICACION { get; set; }

        [DataMember(Name = "NUMERO_IDENTIFICACION")]
        public string  NUMERO_IDENTIFICACION { get; set; }

        [DataMember(Name = "PRIMER_APELLIDO")]
        public string PRIMER_APELLIDO { get; set; }

        [DataMember(Name = "SEGUNDO_APELLIDO")]
        public string SEGUNDO_APELLIDO { get; set; }

        [DataMember(Name = "PRIMER_NOMBRE")]
        public string PRIMER_NOMBRE { get; set; }

       

    }
}
