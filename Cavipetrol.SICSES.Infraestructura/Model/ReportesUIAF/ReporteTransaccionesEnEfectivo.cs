﻿using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Cavipetrol.SICSES.Infraestructura.Model.ReportesUIAF
{
    [DataContract(Name = "ReporteTransaccionesEnEfectivo")]
    public class ReporteTransaccionesEnEfectivo
    {
        [DataMember(Name = "CONSECUTIVO")]
        public long? CONSECUTIVO { get; set; }

        [DataMember(Name = "FECHA_TRANSACCION")]
        public string FECHA_TRANSACION { get; set; }

        [DataMember(Name = "VALOR_TRANSACCION")]
        public double? VALOR_TRANSACCION { get; set; }

        [DataMember(Name = "TIPO_MONEDA")]
        public int TIPO_MONEDA { get; set; }

        [DataMember(Name = "CODIGO_OFICINA")]
        public string CODIGO_OFICINA { get; set; }

        [DataMember(Name = "CODIGO_DEPARTAMENTO_MUNICIPIO")]
        public string CODIGO_DEPARTAMENTO { get; set; }

        [DataMember(Name = "TIPO_PRODUCTO")]
        public string TIPO_PRODUCTO { get; set; }

        [DataMember(Name = "TIPO_TRANSACCION")]
        public int TIPO_TRANSACCION { get; set; }

        [DataMember(Name = "NRO_CUENTA_O_PRODUCTO")]
        public int? NUMERO_CUENTA { get; set; }

        [DataMember(Name = "TIPO_IDENTIFICACION_DEL_TITULAR")]
        public int? rpo_idtiponit { get; set; }

        [DataMember(Name = "NRO_IDENTIFICACION_DEL_TITULAR")]
        public string NUMERO_IDENTIFICACION { get; set; }

        [DataMember(Name = "PRIMER_APELLIDO_DEL_TITULAR")]
        public string PRIMER_APELLIDO { get; set; }

        [DataMember(Name = "SEGUNDO_APELLIDO_DEL_TITULAR")]
        public string SEGUNDO_APELLIDO { get; set; }

        [DataMember(Name = "PRIMER_NOMBRE_DEL_TITULAR")]
        public string PRIMER_NOMBRE { get; set; }

        [DataMember(Name = "OTROS_NOMBRES_DEL_TITULAR")]
        public string OTROS_NOMBRES { get; set; }

        [DataMember(Name = "RAZON_SOCIAL_DEL_TITULAR")]
        public string RAZON_SOCIAL { get; set; }

        [DataMember(Name = "ACTIVIDAD_ECONOMICA_DEL_TITULAR")]
        public string ACTIVIDA_ECONOMICA { get; set; }

        [DataMember(Name = "INGRESO_MENSUAL_DEL_TITULAR")]
        public string INGRESO_MENSUAL_DEL_TITULAR { get; set; }

        [DataMember(Name = "TIPO_IDENTIFICACION_PERSONA_QUE_REALIZA_LA_TRANSACCION_INDIVIDUAL")]
        public string TIPO_IDENTIFICACION_PERSONA_TRANSACCION { get; set; }

        [DataMember(Name = "NRO_IDENTIFICACION_PERSONA_QUE_REALIZA_LA_TRANSACCION_INDIVIDUAL")]
        public string NUMERO_PERSONA_TRANSACCION { get; set; }

        [DataMember(Name = "PRIMER_APELLIDO_PERSONA_QUE_REALIZA_LA_TRANSACCION_INDIVIDUAL")]
        public string PRIMER_APELLIDO_PERSONA_TRANSACCION { get; set; }

        [DataMember(Name = "SEGUNDO_APELLIDO_PERSONA_QUE_REALIZA_LA_TRANSACCION_INDIVIDUAL")]
        public string SEGUNDO_APELLIDO_PERSONA_TRANSACCION { get; set; }

        [DataMember(Name = "PRIMER_NOMBRE_PERSONA_QUE_REALIZA_LA_TRANSACCION_INDIVIDUAL")]
        public string PRIMER_NOMBRE_PERSONA_TRANSACCION { get; set; }

        [DataMember(Name = "OTROS_NOMBRES_PERSONA_QUE_REALIZA_LA_TRANSACCION_INDIVIDUAL")]
        public string OTROS_NOMBRES_PERSONA_TRANSACCION { get; set; }
    }
}
