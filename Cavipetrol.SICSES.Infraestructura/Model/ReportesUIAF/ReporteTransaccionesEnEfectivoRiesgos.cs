﻿using System;
using System.Runtime.Serialization;

namespace Cavipetrol.SICSES.Infraestructura.Model.ReportesUIAF
{
    [DataContract(Name = "ReporteTransaccionesEnEfectivoRiesgos")]
    public class ReporteTransaccionesEnEfectivoRiesgos
    {
        [DataMember(Name = "Tipo_Documento_Titular")]
        public string Tipo_Documento_Titular { get; set; }

        [DataMember(Name = "N_Documento_Titular")]
        public string N_Documento_Titular { get; set; }

        [DataMember(Name = "Nombres_Titular")]
        public string Nombres_Titular { get; set; }

        [DataMember(Name = "Apellidos_Titular")]
        public string Apellidos_Titular { get; set; }

        [DataMember(Name = "Producto")]
        public string Producto { get; set; }

        [DataMember(Name = "Tipo_Producto")]
        public string Tipo_Producto { get; set; }

        [DataMember(Name = "N_Producto")]
        public string N_Producto { get; set; }

        [DataMember(Name = "N_Termino")]
        public string N_Termino { get; set; }

        [DataMember(Name = "Tipo_Transaccion")]
        public string Tipo_Transaccion { get; set; }

        [DataMember(Name = "Tipo_Operación")]
        public string Tipo_Operación { get; set; }

        [DataMember(Name = "N_Operacion")]
        public string N_Operacion { get; set; }

        [DataMember(Name = "Valor_Operacion")]
        public double? Valor_Operacion { get; set; }

        [DataMember(Name = "Usuario")]
        public string Usuario { get; set; }

        [DataMember(Name = "Fecha")]
        public string Fecha { get; set; }

        [DataMember(Name = "Ciudad")]
        public string Ciudad { get; set; }

        [DataMember(Name = "Oficina")]
        public string Oficina { get; set; }

        [DataMember(Name = "Tipo_Documento_Ordenante")]
        public string Tipo_Documento_Ordenante { get; set; }

        [DataMember(Name = "N_Documento_Ordenante")]
        public string N_Documento_Ordenante { get; set; }

        [DataMember(Name = "Primer_Apellido_Ordenante")]
        public string Primer_Apellido_Ordenante { get; set; }

        [DataMember(Name = "Segundo_Apellido_Ordenante")]
        public string Segundo_Apellido_Ordenante { get; set; }

        [DataMember(Name = "Primer_Nombre_Ordenante")]
        public string Primer_Nombre_Ordenante { get; set; }

        [DataMember(Name = "Segundo_Nombre_Ordenante")]
        public string Segundo_Nombre_Ordenante { get; set; }

        [DataMember(Name = "Consulta_Listas_Ordenante")]
        public string Consulta_Listas_Ordenante { get; set; }

        [DataMember(Name = "Tipo_Documento_Quien_Realiza_Transaccion")]
        public string Tipo_Documento_Quien_Realiza_Transaccion { get; set; }

        [DataMember(Name = "N_Documento_Quien_Realiza_Transaccion")]
        public string N_Documento_Quien_Realiza_Transaccion { get; set; }

        [DataMember(Name = "Primer_Apellido_Quien_Realiza_Transaccion")]
        public string Primer_Apellido_Quien_Realiza_Transaccion { get; set; }

        [DataMember(Name = "Segundo_Apellido_Quien_Realiza_Transaccion")]
        public string Segundo_Apellido_Quien_Realiza_Transaccion { get; set; }

        [DataMember(Name = "Primer_Nombre_Quien_Realiza_Transaccion")]
        public string Primer_Nombre_Quien_Realiza_Transaccion { get; set; }

        [DataMember(Name = "Segundo_Nombre_Quien_Realiza_Transaccion")]
        public string Segundo_Nombre_Quien_Realiza_Transaccion { get; set; }

        [DataMember(Name = "Consulta_Listas_Quien_Realiza_Transaccion")]
        public string Consulta_Listas_Quien_Realiza_Transaccion { get; set; }

        [DataMember(Name = "N_Consecutivo")]
        public long N_Consecutivo { get; set; }

        [DataMember(Name = "Origen_Fondos")]
        public string Origen_Fondos { get; set; }

        [DataMember(Name = "Detalle_Fondos")]
        public string Detalle_Fondos { get; set; }

        [DataMember(Name = "Consulta_Fondos")]
        public string Consulta_Fondos { get; set; }
    }
}
