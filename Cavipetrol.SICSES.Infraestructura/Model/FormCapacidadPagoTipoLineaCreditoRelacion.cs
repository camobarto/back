﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class FormCapacidadPagoTipoCreditoTipoRolRelacion        
    {
        public int IdCapacidadPagoTipoCreditoTipoRolRelacion { get; set; }
        public int IdTipoCreditoTipoRolRelacion { get; set; }
        public int IdCapacidadPago { get; set; }

        public int? Orden { get; set; } 
      
    }
}
