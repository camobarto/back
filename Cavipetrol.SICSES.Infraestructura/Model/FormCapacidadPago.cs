﻿using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class FormCapacidadPago
    {
        public int IdCapacidadPago { get; set; }
        public Int16 IdGrupo { get; set; }
        public string Texto { get; set; }
        public float? ValorAsociado { get; set; }
        public float? ValorFamiliar { get; set; }
        public bool? EsSoloLecturaAsociado { get; set; }
        public bool? EsSoloLecturaFamiliar { get; set; }
        public double? Factor { get; set; }
        public string Tipo { get; set; }
        public string TipoLista { get; set; }
        [NotMapped]
        public bool? Confirmado { get; set; }
        [NotMapped]
        public List<DetalleFormCapacidadPago> DetalleFormCapacidadPago { get; set; }

    }   
}
