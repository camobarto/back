﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class Municipio
    {
        public string Id { get; set; }
        public string IdDepartamento { get; set; }
        public string Nombre { get; set; }
        public Departamento Departamento { get; set; }
    }
}
