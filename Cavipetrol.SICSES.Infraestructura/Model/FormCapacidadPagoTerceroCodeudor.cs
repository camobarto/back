﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class FormCapacidadPagoTerceroCodeudor
    {
        public string TipoIdentificacion { get; set; }
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public List<FormCapacidadPagoGrupos> FormCapacidadPagoGrupos { get; set; }
    }
}
