﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoContrato
    {
        public int IdContrato { get; set; }
        public string Descripcion { get; set; }
    }
}
