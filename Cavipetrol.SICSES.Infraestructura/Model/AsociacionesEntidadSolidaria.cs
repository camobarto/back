﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class AsociacionEntidadSolidaria
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
