﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sap
{
    public class Asientos
    {

        public string Asi_fecha { get; set; }
        public string RefDate { get; set; }
        public string TaxDate { get; set; }
        public int Series { get; set; }
        public string Asi_soporte { get; set; }
        public int Asi_consecutivosoporte { get; set; }
        public string Mvc_terceronit { get; set; }
        public int Ldb_cheque { get; set; }
        public string Asi_Operacion { get; set; }
        public int Asi_operacion_consecutivo { get; set; }
        public string Opr_usuario { get; set; }
        public string Memo { get; set; }
        public string Asi_centrocosto { get; set; }
        public string Mvc_cuenta { get; set; }
        public double Mvc_valor_credito { get; set; }
        public double Mvc_valor_debito { get; set; }
        public string U_InfoCo01 { get; set; }
        public string ReferenceDate1 { get; set; }
        public string DueDate { get; set; }
        public string LineMemo { get; set; }
        public string ProjectCode { get; set; }
        public string Mvc_subcentro { get; set; }
        public string ProfitCode { get; set; }
        public string U_css_oficina { get; set; }
        public decimal CashFlowLineItemID { get; set; }
        public decimal AmountLC { get; set; }
        public string U_codret { get; set; }
        public double Mcp_valor { get; set; }
        public decimal U_tarifaret { get; set; }
        public string Asi_tipo { get; set; }
        public int Asi_consecutivo { get; set; }
        public int Mvc_línea { get; set; }
        public string U_CSS_Chequera { get; set; }
        public int Estado_fyc { get; set; }
        public string Mensaje_log { get; set; }
        public string Fecha_integracion { get; set; }






    }
}

