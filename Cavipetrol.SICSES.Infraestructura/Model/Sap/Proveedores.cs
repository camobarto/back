﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sap
{
    public class Proveedores
    {
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string CardType { get; set; }
        public string DebPayAcct { get; set; }
        public string Currency { get; set; }
        public string E_Mail { get; set; }
        public string LictradNum { get; set; }
        public decimal GroupCode { get; set; }
        public int GroupNum { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Cellular { get; set; }
        public bool Activo { get; set; }
        public bool Verificado { get; set; }
        public int Estado_fyc { get; set; }
        public string Mensaje_log { get; set; }

        public string Fecha_integracion { get; set; }
    }
}
