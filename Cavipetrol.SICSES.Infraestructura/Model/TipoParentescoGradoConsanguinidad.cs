﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoParentescoGradoConsanguinidad
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
