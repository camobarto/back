﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoSectorEconomico
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }
}
