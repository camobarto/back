﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoRol
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
