﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class FormatoVigenteSICSES
    {
        public int Id { get; set; }
        public string Formato { get; set; }
        public string ReportesPeriodicos { get; set; }
        public string CodigoSicses { get; set; }
        public string CodigoRol { get; set; }
    }
}
