﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class TipoDocumentoPorCedula
    {
        public string TipoDocumentoI { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string NOMBRE { get; set; }
        public string ReinaPRoducto { get; set; }
        public string TER_DocumentoTipo { get; set; }
        public int NumeroDocumento { get; set; }
        //public int NumeroTermino { get; set; }
        public string DCT_NumeroUnico { get; set; }
        //public DateTime? TER_Desde { get; set; }
        //public DateTime? TER_Hasta { get; set; }
    }
}
