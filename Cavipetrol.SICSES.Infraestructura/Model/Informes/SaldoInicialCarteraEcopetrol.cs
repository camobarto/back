﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class SaldoInicialCarteraEcopetrol
    {
        public String SCP_DOCUMENTOTIPO { get; set; }
        public int SCP_DOCUMENTONUMERO { get; set; }
        public int SCP_NUMEROTERMINO { get; set; }
        public double SALDO_INICIAL { get; set; }
    }
}
