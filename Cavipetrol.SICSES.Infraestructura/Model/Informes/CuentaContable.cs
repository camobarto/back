﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class CuentaContable
    {
        public string cuc_codigo { get; set; }
        public string cuc_descripcion { get; set; }
    }
}
