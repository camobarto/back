﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class EncabezadoCarteraEcopetrol
    {
        public string SCP_DOCUMENTOTIPO { get; set; }
        public int SCP_DOCUMENTONUMERO { get; set; }
        public int SCP_NUMEROTERMINO { get; set; }
        public double CAPITAL { get; set; }
        public double INTERESCTE { get; set; }
        public double CARTERA { get; set; }
        public double SEGUROS { get; set; }
        public double MORA_COBRO { get; set; }
        public double CREINTEGRO { get; set; }
        public double NEGOINT { get; set; }
        public double Tasa { get; set; }
        public double SALDO_FINAL { get; set; }
    }
}
