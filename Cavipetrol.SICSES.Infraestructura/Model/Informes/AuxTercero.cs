﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class AuxTercero
    {
        public string AUT_CUENTA { get; set; }
        public DateTime? AUT__FECHA_CORTE { get; set; }
        public string AUT__TIPO_NIT { get; set; }
        public string AUT_NIT { get; set; }
        public double? AUT_SALDO_DEBITO { get; set; }
        public double? AUT_SALDO_CREDITO { get; set; }
        public double? AUT_MOVIMIENTO_DEBITO { get; set; }
        public double? AUT_MOVIMIENTO_CREDITO { get; set; }

    }
}
