﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class RetencionProveedores
    {

        public string CIUDAD_PRACTICO { get; set; }
        public string CONCEPTO_PAGO { get; set; }
        public string VALOR_RETENCION { get; set; }
        public string RET_NUM_NIT { get; set; }
        public string TARIFA_RETENCION { get; set; }
        public string BASE_RETENCION { get; set; }
        public string RAZON_SOCIAL_NOMBRE { get; set; }
        //Double? 

    }
}
