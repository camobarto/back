﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class RetencionProveedores_IVA
    {
        public string CONCEPTO_PAGO { get; set; }
        public string BASE_RETENCION { get; set; }
        public string VALOR_IVA { get; set; }
        public string IVA_RETENIDO_15 { get; set; }
        public string CIUDAD_DONDE_CONSIGNO { get; set; }
        public string CIUDAD_DONDE_PRACTICO { get; set; }
        public string FECHA_EXPEDICION { get; set; }
        public string NIT { get; set; }
        public string RAZON_SOCIAL_NOMBRE { get; set; }
        
    }
}
