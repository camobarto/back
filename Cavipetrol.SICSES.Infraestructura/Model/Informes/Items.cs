﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class Items
    {
        public long? IdItemNota { get; set; }
        public long? IdNota { get; set; }
        public int NumeroItem { get; set; }
        public long IdTipoImpuesto { get; set; }
        public decimal ValorSinImpuesto { get; set; }

        public decimal Porcentaje { get; set; }
        public decimal TotalImpuesto { get; set; }
        public decimal Descuento { get; set; }
        public string CodigoProducto { get; set; }
        public string DescripcionProducto { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal ValorTotalItem { get; set; }
        public decimal PorcentajeIVA { get; set; }
        public decimal ValorIVA { get; set; }
        public decimal PorcentajeConsumo { get; set; }
        public decimal ValorConsumo { get; set; }
        public decimal PorcentajeICA { get; set; }
        public decimal ValorICA { get; set; }
        public string DescripcionAdicional { get; set; }
        public decimal ValorDescuentoAplicado { get; set; }
        public long PorcentajeImpuesto { get; set; }
    }
}
