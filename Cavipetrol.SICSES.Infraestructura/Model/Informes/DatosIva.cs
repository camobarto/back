﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class DatosIva
    {
        [Key]
        public string RET_TIPO_SOPORTE { get; set; }
        [Key]
        public int RET_NUM_SOPORTE { get; set; }
        public DateTime ASI_FECHA { get; set; }
        [Key]
        public string RET_CIUDAD { get; set; }
        [Key]
        public string RET_CUENTA { get; set; }
        public string NOMBRE_CUENTA { get; set; }
        public double MVC_VALOR_DEBITO { get; set; }
        public double MVC_VALOR_CREDITO { get; set; }
        [Key]
        public string RET_TIPO_NIT { get; set; }
        [Key]
        public string RET_NUM_NIT { get; set; }
        public  double?  RET_PORCENTAJE { get; set; }
        public double? RET_BASE { get; set; }
        public string TIPO { get; set; }
    }
}
