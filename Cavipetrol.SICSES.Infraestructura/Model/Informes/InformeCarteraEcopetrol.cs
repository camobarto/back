﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class InformeCarteraEcopetrol
    {
        //public string TIPO_IDENTIFICACION { get; set; }
        //public string NUMERO_IDENTIFICACION { get; set; }
        //public string NOMBRE_ASOCIADO { get; set; }
        //public string LINEA_DE_CREDITO { get; set; }
        //public double VALOR_DE_DESEMBOLSO { get; set; }
        //public int PLAZO { get; set; }
        //public DateTime FECHA_INICIO { get; set; }
        //public DateTime FECHA_TERMINACION { get; set; }
        //public decimal TASA_INTERES { get; set; }
        //public int NUMERO_CUOTAS { get; set; }
        //public double VALOR_CUOTA_CAPITAL { get; set; }
        //public double VALOR_CUOTA_INTERES { get; set; }
        //public double VALOR_SEGUROS { get; set; }
        //public string PLAN_DE_PAGOS { get; set; }
        //        public List<PlanPagosCarteraEcopetrol> Planpagos = new List<PlanPagosCarteraEcopetrol>();


        public List<SaldoInicialCarteraEcopetrol> SaldoInicial { get; set; }
        public List<EncabezadoCarteraEcopetrol> Encabezado  { get; set; } 

        public List<PlanPagosCarteraEcopetrol> Planpagos { get; set; }
        public List<MovimientosCarteraEcopetrol> Movimientos { get; set; }



    }
}
