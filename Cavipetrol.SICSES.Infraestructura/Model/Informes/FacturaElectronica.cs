﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
   public class FacturaElectronica
    {
        public int NumeroFactura { get; set; }
        public int CodTipoDocumento { get; set; }
        public int MotivoFactura { get; set; }
        public string Secuencial { get; set; }
        public DateTime Fecha_Emision { get; set; }
        public DateTime Hora_emision { get; set; }
        public string Tipo_identificacion { get; set; }
        public string numeroIdentificacion { get; set; }
        public string RazonSocial { get; set; }
        public string NombreComercial { get; set; }
        public string Departamento { get; set; }
        public string Municipio { get; set; }
        public string Ciudad { get; set; }
        public string Dirreccion { get; set; }
        public string PAIS { get; set; }
        public int? Regimen { get; set; }
        public string TipoIdentificacionEmi { get; set; }
        public string NumeroIdentificacionEmi { get; set; }
        public string RazonSocialEmi { get; set; }

        public string DepartamentoEmi { get; set; }
        public string MunicipioEmi { get; set; }
        public string CiudadEmi { get; set; }
        public string DirreccionEmi { get; set; }
        public string PaisEmi { get; set; }
        public string RegimenEmi { get; set; }
        public string EmailEmi { get; set; }
        public int TelefonoEmi { get; set; }
        public int CodigoImpuesto { get; set; }
        public double ValorSinUmpuesto { get; set; }
        public double PorcImpuesto { get; set; }
        public double TotalImpuesto { get; set; }

        public double? SubtotalFactura { get; set; }
        public double? Impuestos { get; set; }
        public double? TotalFactura { get; set; }
        public int Descuento { get; set; }
        public string Moneda { get; set; }
        public string Codigo_producto { get; set; }
        public string DescripcionProducto { get; set; }
        public int Cantidad { get; set; }
        public double PrioridadUnitario { get; set; }
        public double ValorTotalItem { get; set; }
        public double PorcIva { get; set; }
        public double ValorIva { get; set; }
        public int PorceConsumo { get; set; }
        public int ValorConsumo { get; set; }

        public int ValorIca { get; set; }
        public int PorcIca { get; set; }
        public int DescuentoItem { get; set; }
        public string DescripcionAdicional { get; set; }
        public string Etiqueta { get; set; }
        public string ValorEtiqueta { get; set; }
    }
}
