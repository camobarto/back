﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class GMF_BDos
    {
        public string Servidor { get; set; }
        public DateTime? ASI_Fecha { get; set; }
        public DateTime? OPR_Fecha { get; set; }
        public string ASI_Tipo { get; set; }
        public string ASI_CentroCosto { get; set; }
        public string ASI_Soporte { get; set; }
        public int? ASI_ConsecutivoSoporte { get; set; }
        public int? MVC_Línea { get; set; }
        public string MVC_Cuenta { get; set; }
        public double? MVC_Valor_Débito { get; set; }
        public double? MVC_Valor_Crédito { get; set; }
        public string MVC_TerceroTipoNit { get; set; }
        public string MVC_TerceroNit { get; set; }
        public string MVC_CentroCosto { get; set; }
        public string OPR_EstadoProducto { get; set; }
        public string OPR_UsoConsecutivo { get; set; }
        public string opr_ciudad { get; set; }
        public string opr_oficina { get; set; }
        public string OPR_Usuario { get; set; }
        public string ASI_OPERACION { get; set; }
        public int? ASI_OPERACION_CONSECUTIVO { get; set; }
        public double? Base_Gravado { get; set; }
        public int? Base_Excento { get; set; }
        public double? ImpuestoConta { get; set; }

    }
}
