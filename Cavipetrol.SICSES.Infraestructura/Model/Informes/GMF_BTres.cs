﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class GMF_BTres
    {
        public string RPD_IdNit { get; set; }
        public string TER_DocumentoTipo { get; set; }
        public int? TER_DocumentoNúmero { get; set; }
        public DateTime? TER_Desde { get; set; }
        public DateTime? TER_Hasta { get; set; }
        public string NTR_PrimerApellido { get; set; }
        public string NTR_SegundoApellido { get; set; }
        public string NTR_Nombres { get; set; }

    }
}
