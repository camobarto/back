﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class Nota
    {
        public long? IdNota { get; set; }
        public long? IdTipoNota { get; set; }
        public string IdFactura { get; set; }
        public long NumAsientoFYC { get; set; }
        public string MotivoNotaCredito { get; set; }
        public long IdTipoIdentificacionCliente { get; set; }
        public string NumIdentificacionCliente { get; set; }
        public string SecuencialDocumentoSustento { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Usuario { get; set; }
        [NotMapped]
        public List<Items> Items { get; set; }
        [NotMapped]
        public List<Etiquetas> Etiquetas { get; set; }
        [NotMapped]
        public string Servidor { get; set; }
    }
}
