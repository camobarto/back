﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class Etiquetas
    {
        public int IdEtiquetaNota { get; set; }
        public long? IdNota { get; set; }
        public string Etiqueta { get; set; }
        public string ValorEtiqueta { get; set; }

    }
}
