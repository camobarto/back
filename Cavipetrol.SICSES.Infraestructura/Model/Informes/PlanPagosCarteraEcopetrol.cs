﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class PlanPagosCarteraEcopetrol
    {
        public string PVD_AspectoDeDocumentoTipo { get; set; }
        public int PDV_AspectoDeDocumentoNúmero { get; set; }
        public int termino { get; set; }
        public string Periodo { get; set; }
        public double VrPrestado { get; set; }
        public int NumCuotas { get; set; }
        public int CuotasFaltantes { get; set; }
        public int PorcPagado { get; set; }
        public double Cuota { get; set; }
        public double CuotaV { get; set; }
        public DateTime FechaInicio { get; set; }
        public double Tasa { get; set; }
    }
}
