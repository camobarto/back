﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class Certificaciones_Ingresos_Retenciones
    {
        public string PRODUCTO { get; set; }
        public double INTERESES { get; set; }
        public double RETENCION_FUENTE { get; set; }
        public double RENDIMIENTOS_PAGADOS { get; set; }
        public double VALOR_NETO { get; set; }
    }
}
