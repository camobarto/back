﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class AuditaCuenta
    {
        public DateTime? ASI_FECHA { get; set; }
        public string ASI_Tipo { get; set; }
        public int? ASI_Consecutivo { get; set; }
        public string ASI_CentroCosto { get; set; }
        public string ASI_Soporte { get; set; }
        public int? ASI_ConsecutivoSoporte { get; set; }
        public string ASI_OPERACION { get; set; }
        public int? ASI_OPERACION_CONSECUTIVO { get; set; }
        public string MVC_AsientoTipo { get; set; }
        public int? MVC_AsientoConsecutivo { get; set; }
        public int? MVC_Línea { get; set; }
        public string MVC_Cuenta { get; set; }
        public double? MVC_Valor_Débito { get; set; }
        public double? MVC_Valor_Crédito { get; set; }
        public string MVC_TerceroTipoNit { get; set; }
        public string MVC_TerceroNit { get; set; }
        public string MVC_CentroCosto { get; set; }
        public string MVC_SubCentro { get; set; }
        public string OPR_Tipo { get; set; }
        public int? OPR_Consecutivo { get; set; }
        public DateTime? OPR_Fecha { get; set; }
        public string OPR_Centro { get; set; }
        public string OPR_DocumentoTipo { get; set; }
        public int? OPR_DocumentoNúmero { get; set; }
        public int? OPR_NúmeroTérmino { get; set; }
        public string OPR_EstadoProducto { get; set; }
        public string OPR_EstadoNombre { get; set; }
        public DateTime? OPR_EstadoFecha { get; set; }
        public string OPR_UsoConsecutivo { get; set; }
        public string OPR_Soporte { get; set; }
        public int? OPR_ConsecutivoSoporte { get; set; }
        public string OPR_Ciudad { get; set; }
        public string OPR_Oficina { get; set; }
        public string OPR_Usuario { get; set; }
        public string RPD_DocumentoTipo { get; set; }
        public int? RPD_DocumentoNúmero { get; set; }
        public string RPD_JuegaRolTipo { get; set; }
        public string RPD_IdTipoNit { get; set; }
        public string RPD_IdNit { get; set; }

    }
}
