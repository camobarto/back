﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class ProveedoresFaltantes
    {
        public string RET_TIPO_NIT { get; set; }
        public string RET_NUM_NIT { get; set; }

    }
}
