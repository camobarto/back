﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
    public class RetencionProveedores_ICA
    {
        public string CONCEPTO_PAGO { get; set; }
        public string BASE_RETENCION { get; set; }
        public string ICA_RETENIDO { get; set; }
        public string PORCENTAJE_ICA_RETENIDO { get; set; }
        public string CIUDAD { get; set; }
        public string FECHA_EXPEDICION { get; set; }
        public string NIT { get; set; }
        public string RAZON_SOCIAL_NOMBRE { get; set; }

    }
}
