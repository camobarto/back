﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Informes
{
   public class MovimientosCarteraEcopetrol
    {
        public string FECHA { get; set; }
        public string CIUDAD { get; set; }
        public string OPERACION { get; set; }
        public int NUMERO_OPERACION { get; set; }
        public string CUENTA { get; set; }
        public double VALOR { get; set; }
    }
}
