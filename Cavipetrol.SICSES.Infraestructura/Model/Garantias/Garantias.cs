﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Garantias
{
    public class Solicitud
    {
        public int NumeroSolicitud { get; set; }
        public string IdNit { get; set; }
        public string NombreSolicitante { get; set; }
        public string Producto { get; set; }
        public string TipoGarantia { get; set; }        
        public DateTime FechaGeneracion { get; set; }
        public int EstadoSolicitud { get; set; }
        public int NumeroPagare { get; set; }
    }
    public class PagaresModel
    {
        public string IdNit { get; set; }
        public string NumNit { get; set; }
        public int Proceso { get; set; }
        public string TipoPagare { get; set; }
        public string Ciudad { get; set; }
        public string Producto { get; set; }
        public string Usuario { get; set; }
        public int NumeroSolicitud { get; set; }
        public string NumeroMatricula { get; set; }
        public string OficinaMatricula { get; set; }
    }
    public class PrendaVehicular
    {
        public int NumeroConsecutivo { get; set; }
        public string TipoNit { get; set; }
        public string NumNit { get; set; }
        public string Vendedor { get; set; }
        public string NitVendedor { get; set; }
        public string Placa { get; set; }
        public string Chasis { get; set; }
        public string Motor { get; set; }
        public string ClaseVeh { get; set; }
        public string Modelo { get; set; }
        public string Marca { get; set; }
        public string Linea { get; set; }
        public string Serie { get; set; }
        public string Servicio { get; set; }
        public string VIN { get; set; }        
        public string Color { get; set; }
        public string DireccionDeudor { get; set; }
        public string Barrio { get; set; }
        public string CiudadDeudor { get; set; }
        public string Estado { get; set; }
        public string NombreDeudor { get; set; }
        public int ValorPrestamo { get; set; }
    }
    public class NormalizadoConsulta
    {
        public string IdNit { get; set; }
        public string NumNit { get; set; }
        public int Proceso { get; set; }
        public string TipoPagare { get; set; }
        public string Ciudad { get; set; }
        public string Producto { get; set; }
        public string DocumentoTipo { get; set; }
    }
    public class CreditoYPagares
    {
        public string DocumentoTipo { get; set; }
        public string DocumentoNumero { get; set; }
        public string Item { get; set; }
        public int NumeroSolicitud { get; set; }
        public string IdNit { get; set; }
        public string NumNit { get; set; }
        public string NombreSolicitante { get; set; }
        public string Producto { get; set; }
        public string TipoGarantia { get; set; }
        public DateTime FechaGeneracion { get; set; }
    }
}