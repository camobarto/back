﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoObligacion
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }

    }
}
