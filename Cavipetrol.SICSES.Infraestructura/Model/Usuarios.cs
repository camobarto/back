﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class Usuarios
    {
        public int IdUsuario { get; set; }
        public string UniqueName { get; set; }
        public int IdPerfil { get; set; }
        public int IdOficina { get; set; }
    }
}
