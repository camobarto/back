﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoNivelEscolaridad
    {
        public int Id { get; set; }
        public int IdEscolaridad { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionCavipetrol { get; set; }
    }
}
