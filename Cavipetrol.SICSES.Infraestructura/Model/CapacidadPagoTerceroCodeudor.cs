﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class CapacidadPagoTerceroCodeudor
    {
        public int IdCapacidadPagoTerceroCodeudor { get; set; }
        public int idSolicitud { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string Nombre { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        //public List<FormCapacidadPagoGrupos> FormCapacidadPagoGrupos { get; set; }
        public string JsonCapacidadPago { get; set; }

    }
}
