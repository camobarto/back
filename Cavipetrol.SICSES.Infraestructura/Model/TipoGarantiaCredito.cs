﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoGarantiaCredito
    {
        // public int IDTIPOCREDITOROL { get; set; }
        public short IDTIPOCREDITO { get; set; }
        public string NOMBRE { get; set; }
        public short IDTIPOGARANTIA { get; set; }
        public string NOMBRE_GARANTIA { get; set; }


    }
}
