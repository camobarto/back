﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class ProductoPorAsociado
    {

        public string tmp_documentotipo { get; set; }
        public int tmp_documentonumero { get; set; }

        public double saldo { get; set; }
    }
}
