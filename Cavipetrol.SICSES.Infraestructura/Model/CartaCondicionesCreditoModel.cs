﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class CartaCondicionesCreditoModel
    {
        public long numeroSolicitud { get; set; }
        public int numeroRadicado { get; set; }
        public decimal monto { get; set; }
        public Int16 IdPapelBen { get; set; }
        public string formaPago { get; set; }
        public string JsonCapacidadPago { get; set; }
        public string plazoAnhos { get; set; }
        public double TEA { get; set; }
        public float? tNominal { get; set; }
        public string linea { get; set; }
        public int idLinea { get; set; }
    }
}
