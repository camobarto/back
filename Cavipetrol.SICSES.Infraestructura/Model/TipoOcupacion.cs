﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoOcupacion
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
