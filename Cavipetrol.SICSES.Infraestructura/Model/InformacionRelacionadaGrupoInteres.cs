﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class InformacionRelacionadaGrupoInteres
    {

        public int ID_INFORMACION_DETALLADA { get; set; }
        public int UNIDAD_CAPTURA { get; set; }
        public int CODIGO_RENGLON { get; set; }
        public string DESCRIPCION_DEL_RENGLON { get; set; }
        public int? NUMERO { get; set; }
        public double? PORCENTAJE { get; set; }
        public double? RECURSOS_GIRADOS_EN_EL_ANO { get; set; }
        public int? NUMERO_DE_ASOCIADOS_BENEFICIADOS { get; set; }
        public double? RECURSOS_GIRADOS_EN_EL_ANO_PARA_BENEFICIOS { get; set; }
        public int? NUMERO_DE_EMPLEADOS_BENEFICIADOS { get; set; }
        public double? RECURSOS_GIRADOS_EN_ANO_BENEF_COMUNIDAD { get; set; }
        public int?  NUMERO_DE_PERSONAS_DE_LA_COMUNIDAD_BENEFICIADAS { get; set; }

        public int? TIPO_DESHABILITADO { get; set; }

        public int? TIPO_DESHABILITADO2 { get; set; }

        public int? TIPO_DESHABILITADO3 { get; set; }

    }
}
