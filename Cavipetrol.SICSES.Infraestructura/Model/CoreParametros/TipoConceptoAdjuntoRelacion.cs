﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class TipoConceptoAdjuntoRelacion
    {
        public int IdTipoConceptoAdjuntoRelacion { get; set; }
        public int IdTipoAdjunto { get; set; }
        public int IdConceptoAdjunto { get; set; }
        public bool Activo { get; set; }
    }
}
