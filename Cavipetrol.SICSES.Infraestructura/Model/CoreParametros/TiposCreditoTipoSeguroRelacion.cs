﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class TiposCreditoTipoSeguroRelacion
    {
        public short IdTipoCredito { get; set; }
        public int IdTipoSeguro { get; set; }
        public double? Factor { get; set; }
        public double RangoInicial { get; set; }
        public double RangoFinal { get; set; }
        public double TasaPorMil { get; set; }
    }
}
