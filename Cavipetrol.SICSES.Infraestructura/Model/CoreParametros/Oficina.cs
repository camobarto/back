﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class Oficina
    {
        public int IdOficina { get; set; }
        public string OFI_NOM_OFICINA { get; set; }
        public string OFI_NOM_CIUDAD { get; set; }
        public string UBICACION { get; set; }
        public DateTime? FECHA_APERTURA { get; set; }
        public DateTime? FECHA_CIERRE { get; set; }
        public int? CANTIDAD_HABITANTES { get; set; }
        public int? CANTIDAD_ENTIDADES_FRAS { get; set; }
        public int? CANTIDAD_COOPERATIVAS { get; set; }
        public string DEPARTAMENTO { get; set; }
        public string MUNICIPIO { get; set; }
        public string DPR_ID { get; set; }
        public string MNC_ID { get; set; }
    }
}
