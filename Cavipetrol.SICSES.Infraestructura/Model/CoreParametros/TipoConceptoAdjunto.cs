﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class TipoConceptoAdjunto
    {
        public int IdConceptoAdjunto { get; set; }
        public string Concepto { get; set; }
        public string Descripcion { get; set; }
    }
}
