﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class CuposAsegurabilidad
    {
        public short IdCupoAsegurabilidad { get; set; }
        public double EdadInicial { get; set; }
        public double EdadFinal { get; set; }
        public decimal Valor { get; set; }        
    }
}
