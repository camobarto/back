﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class FormaPagoRelacion
    {
        public int IdFormaPagoRelacion { get; set; }
        public int IdPapelCavipetrol { get; set; }
        public int IdTipoCredito { get; set; }
        public int IdFormaPago { get; set; }
        public bool Activo { get; set; }
        public int Clave { get; set; }
        public bool Mesada14 { get; set; }
    }
}
