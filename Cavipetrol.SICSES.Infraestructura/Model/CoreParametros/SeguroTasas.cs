﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class SeguroTasas
    {
        public int IdSeguroTasa { get; set; }
        public int IdTipoSeguro { get; set; }
        public byte EdadInicial { get; set; }
        public byte EdadFinal { get; set; }
        public double TasaAnual { get; set; }
        public double TasaMensual { get; set; }

    }
}
