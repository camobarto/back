﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
   public class SalarioMinimo
    {
        public int Id { get; set; }
        public string Valor { get; set; }
    }
}
