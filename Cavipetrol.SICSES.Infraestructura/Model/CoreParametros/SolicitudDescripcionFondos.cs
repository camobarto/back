﻿namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class SolicitudDescripcionFondos
    {
        public int Id { get; set; }
        public string Descripcion_Origen { get; set; }
        public string Opcion { get; set; }        
    }
}
