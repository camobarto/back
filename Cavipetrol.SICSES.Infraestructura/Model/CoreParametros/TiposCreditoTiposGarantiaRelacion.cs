﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class TiposCreditoTiposGarantiaRelacion
    {
        public short IdTipoCreditoTIpoGarantia { get; set; }
        public short IdTipoCredito { get; set; }
        public short IdTipoGarantia { get; set; }
        public int IdPagare { get; set; }
        public byte Codeudores { get; set; }
        public virtual TiposGarantia TiposGarantia {get;set;}
    }
}
