﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class Cupo
    {
        public short IdCupo { get; set; }
        public short IdTipoCredito { get; set; }
        public short IdTipoCupo { get; set; }
        public double? AntiguedadInicial { get; set; }
        public double? AntiguedadFinal { get; set; }
        public double? Valor { get; set; }
    }
}
