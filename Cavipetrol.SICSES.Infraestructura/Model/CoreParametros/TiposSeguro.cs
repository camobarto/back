﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class TiposSeguro
    {
        public int IdTipoSeguro { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
