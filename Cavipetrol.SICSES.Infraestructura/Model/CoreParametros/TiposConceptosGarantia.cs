﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class TiposConceptosGarantia
    {
        public int IdConceptoGarantia { get; set; }
        public string Concepto { get; set; }
        public string Descripcion { get; set; }
        public string CampoRemplaza { get; set; }
        public string tipoVariable { get; set; }
        public bool? visible { get; set; }
        public bool? editable { get; set; }
        public bool? ciudad { get; set; }
    }
}
