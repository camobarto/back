﻿using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class FormaPago
    {
        public int IdFormaPago { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        [NotMapped]
        public int Plazo { get; set; }
        [NotMapped]
        public decimal Valor { get; set; }
        [NotMapped]
        public VMDatosTablaAmortizacion DatosTablaAmortizacion { get; set; }
        [NotMapped]
        public List<Amortizacion> TablaAmortizacion { get; set; }
        [NotMapped]
        public int? EGV_Clave { get; set; }
        [NotMapped]
        public int? Gracia { get; set; }


    }
    public class VMDatosTablaAmortizacion {
        public int IdTipoLineaCreditoSeleccionada { get; set; }
        public int IdTipoCreditoTipoRolRelacion { get; set; }
        public int Plazo { get; set; }
        public double TasaNominal { get; set; }
        public double TasaEA { get; set; }
        public double Valor { get; set; }
        public double? ValorInmueble { get; set; }
        public int IdAsegurabilidadOpcion { get; set; }
        public double? AsegurabilidadPorcentajeExtraprima { get; set; }
        public int IdPapelCavipetrol { get; set; }
        public int Edad { get; set; }
        public int IdFormaPago { get; set; }
        //[NotMapped]
        //public double? TasaPorMilVida { get; set; }
        //[NotMapped]
        //public double? TasaPorMilIncendio { get; set; }
    }
}
