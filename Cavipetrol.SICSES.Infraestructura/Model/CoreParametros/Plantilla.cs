﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class Plantilla
    {
        public short IdPlantilla { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Html { get; set; }

    }
}
