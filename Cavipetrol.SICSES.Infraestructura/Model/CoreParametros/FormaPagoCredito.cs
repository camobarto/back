﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class FormaPagoCredito
    {
        public int IdFormaPago { get; set; }
        public Double Plazo { get; set; }
        public decimal Valor { get; set; }
        public String Nombre { get; set; }
    }
}
