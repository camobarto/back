﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class TiposPagareTiposConceptoRelacion
    {
        public int IdTipoPagareTipoConcepto { get; set; }
        public int IdPagare { get; set; }
        public int IdConceptoGarantia { get; set; }
        public string PkFycItemDocumento { get; set; }
    }
}
