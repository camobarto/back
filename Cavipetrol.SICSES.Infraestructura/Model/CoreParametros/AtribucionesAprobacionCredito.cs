﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.CoreParametros
{
    public class AtribucionesAprobacionCredito
    {
        public int IdAtribucionAprobacion { get; set; }
        public int IdPerfil { get; set; }
        public double Maximo { get; set; }
        public double Consolidado { get; set; }
    }
}
