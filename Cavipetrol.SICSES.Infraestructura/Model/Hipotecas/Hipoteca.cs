﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Hipotecas
{
    public class Hipoteca
    {
        public int IdHipoteca { get; set; }
        public string NumeroHipoteca { get; set; }
        public string TipoDocumentoHipotecario { get; set; }
        public string CedulaHipotecario { get; set; }
        public string NombreHipotecario { get; set; }
        public DateTime? FechaEscrituracion { get; set; }
        public string NumeroEscritura { get; set; }
        public string AudUsuarioCreacion { get; set; }
        public DateTime? AudFechaCreacion { get; set; }
        public string AudUsuarioModificacion { get; set; }
        public DateTime? AudFechaModificacion { get; set; }
        public Boolean OpcionEditar { get; set; }
        public double? ValorCubierto { get; set; }
        public double? ValorCobertura { get; set; }
        public double? ValorAvaluo { get; set; }
        public DateTime? VigenciaInicial { get; set; }
        public DateTime? VigenciaFinal { get; set; }

        public string NumeroNotaria { get; set; }

        public string Oficina { get; set; }

    }
}
