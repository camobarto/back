﻿using System.Collections.Generic;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class Departamento
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public ICollection<Municipio> Municipios { get; set; }
    }
}
