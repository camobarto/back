﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class TipoIdentificacion
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }
        public string IdCavipetrol { get; set; }
    }
}
