﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class Etiquetas
    {
        public int IdEtiqueta { get; set; }
        public int IdTipoEtiqueta { get; set; }
        public string Nombre { get; set; }
        public bool esSoloLectura { get; set; }
        public bool esVisible { get; set; }
        public string Valor { get; set; }
    }
}
