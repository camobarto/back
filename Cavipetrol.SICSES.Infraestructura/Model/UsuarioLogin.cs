﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class UsuarioLogin
    {
        public string Username   { get; set; }

        public string contrasena { get; set; }

        public string NuevaContrasena { get; set; }

        public int bandera { get; set; }

        public string Respuesta { get; set; }
    }
}
