﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.fyc
{
    public class Aportes
    {
        public DateTime Fecha { get; set; }
        public double? ValorAportes { get; set; }        
    }
}
