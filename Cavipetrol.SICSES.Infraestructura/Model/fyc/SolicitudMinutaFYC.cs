﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.fyc
{
    public class SolicitudMinutaFYC
    {
        public int? IdSolicitud { get; set; }
        public string Ciudad { get; set; }
        public string CiudadInmueble { get; set; }
        public string CiudadNotaria { get; set; }
        public string Compra { get; set; }
        public string DeudorTipoCedula { get; set; }
        public string DeudorCedula { get; set; }
        public string DeudorNombre { get; set; }
        public string DeudorRegistro { get; set; }
        public string DeudorDireccion { get; set; }
        public string DireccionInmueble { get; set; }
        public string Matricula { get; set; }
        public string MatriculaDescripcion { get; set; }
        public string Notaria { get; set; }
        public string NumEscritura { get; set; }
        public string Oficina { get; set; }
        public string Plazo { get; set; }
        public string Producto { get; set; }
        public string TiempoECP { get; set; }
        public string TipoHipoteca { get; set; }
        public string Usuario { get; set; }
        public double? Tasa { get; set; }
        public double? ValorPrestamo { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime FechaFirma { get; set; }
    }
}
