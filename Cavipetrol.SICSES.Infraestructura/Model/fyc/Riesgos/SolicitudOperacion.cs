﻿using System;

namespace Cavipetrol.SICSES.Infraestructura.Model.fyc.Riesgos
{
    public class SolicitudOperacion
    {
        public string Ciudad { get; set; }
        public string Oficina { get; set; }
        public DateTime Fecha { get; set; }
        public int Soporte { get; set; }
        public string EstadoProducto { get; set; }
        public string TipoDocumento { get; set; }
        public int? NumeroDocumento { get; set; }
        public int? NumeroTermino { get; set; }
        public string TipoOperacion { get; set; }
        public int Consecutivo { get; set; }
        public double? Valor { get; set; }
        public string Usuario { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
    }
}
