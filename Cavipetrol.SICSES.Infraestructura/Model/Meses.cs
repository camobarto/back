﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class Meses
    {
        public int Id { get; set; }
        public string Mes { get; set; }
    }
}
