﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class SolicitudCreditoIntentosLog
    {
        public long IdSolicitudCreditoIntentoLog { get; set; }
        public string IdTipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public int IdSolicitudCreditoConcepto { get; set; }
        public int? IdTipoCreditoTipoRolRelacion { get; set; }
        public string Descripcion { get; set; }
        public DateTime AudFechaCrea { get; set; }
        public DateTime AudFechaMod { get; set; }
        public string AudUsuCrea { get; set; }
        public string AudUsuMod { get; set; }
        public bool Activo { get; set; }

    }
}
