﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class ReferenciasAsociado
    {        
        public long IdReferenciaAsociado { get; set; }
        public string IdTipoIdentificacion  {get;set;}
        public string NumeroIdentificacion  {get;set;}
        public string Nombre                {get;set;}
        public string Relacion              {get;set;}
        public string Numero                {get;set;}
        public byte IdTipoReferencia      {get;set;}
        public bool? Activo  { get; set; }
    }
}
