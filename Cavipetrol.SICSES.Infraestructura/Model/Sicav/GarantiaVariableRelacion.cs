﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class GarantiaVariableRelacion
    {
        public Int16 IdGarantiaVariableRel { get; set; }
        public Int16 IdTipoCreditoTipoGarantia { get; set; }
        public Int16 IdGarantiaVariable { get; set; }
        public double RangoInicial { get; set; }
        public double RangoFinal { get; set; }
        public int Codeudores { get; set; }
    }
}
