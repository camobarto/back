﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class ListaMenuPerfil
    {
        public List<PerfilMenuRelacion> PerfilMenu { get; set; }
        public int IdPerfil { get; set; }
    }
}
