﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class CapacidadPagoHorasExtras
    {
        public int IdCapacidadPagoHorasExtras { get; set; }
        public int IdCapacidadPago { get; set; }
        public string JsonHorasExtras { get; set; }
    }
}
