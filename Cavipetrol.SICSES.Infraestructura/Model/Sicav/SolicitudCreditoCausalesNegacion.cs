﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class SolicitudCreditoCausalesNegacion
    {
        public short IdCausalNegacion { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
