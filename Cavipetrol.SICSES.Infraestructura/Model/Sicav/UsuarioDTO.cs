﻿using Cavipetrol.SICSES.Infraestructura.Model.Seguridad;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class UsuarioDTO
    {
        public Usuario Usuario { get; set; }
        public List<Perfil> Perfiles { get; set; }
    }
}
