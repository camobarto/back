﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class SolicitudCreditoHipoteca
    {
        public int IdSolicitudHipoteca { get; set; }
        public int IdSolicitud { get; set; }
        public int IdHipoteca { get; set; }
        public double Monto { get; set; }
        public bool Activo { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public bool PagareGenerado { get; set; }
    }
}
