﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class SolicitudCreditoFormaPago
    {
        public long IdSolicitudCreditoFormaPago { get; set; }
        public int IdSolicitud { get; set; }
        public int IdFormaPago { get; set; }
        public double Plazo { get; set; }
        public decimal Valor { get; set; }
        public bool Activo { get; set; }
    }
}
