﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class PerfilMenuRelacion
    {
        public int IdPerfilMenuRelacion { get; set; }
        public int IdPerfil { get; set; }
        public int IdMenu { get; set; }
        public virtual Menu Menu { get; set; }
    }
}
