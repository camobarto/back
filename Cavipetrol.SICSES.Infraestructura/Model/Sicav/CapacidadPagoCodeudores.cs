﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class CapacidadPagoCodeudores
    {
        public int IdCapacidadPagoCodeudores { get; set; }
        public int IdSolicitud { get; set; }
        public string IdTipoIdentificacionCodeudor { get; set; }
        public string NumeroIdentificacionCodeudor { get; set; }
        public string NombreCodeudor { get; set; }
        public string JsonCapacidadPago { get; set; }
        public short? IdEstadoCapacidadPago { get; set; }
        public DateTime? AudFechaCreacion { get; set; }
        public DateTime? AudFechaModificacion { get; set; }
        public string AudUsuarioCreacion { get; set; }
        public string AudUsuarioModificacion { get; set; }
        [NotMapped]
        public string DireccionCodeudor { get; set; }
    }
}
