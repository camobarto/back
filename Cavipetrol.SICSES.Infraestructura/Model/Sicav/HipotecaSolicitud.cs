﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class HipotecaSolicitud
    {
        public int IdHipoteca { get; set; }
        public string NumeroHipoteca { get; set; }
        public DateTime? FechaEscrituracion { get; set; }
        public string NumeroEscritura { get; set; }
        public bool PagareGenerado { get; set; }
    }
}
