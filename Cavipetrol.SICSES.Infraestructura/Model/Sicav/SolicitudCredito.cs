﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class SolicitudCredito
    {
            public int IdSolicitud {get;set;}
            public Int64? ConsecutivoSolicitud { get; set; }
            public int IdTipoCreditoTipoRolRelacion { get;set;}
            public string IdTipoIdentificacionAsociado  {get;set;}
            public string NumeroIdentificacionAsociado  {get;set;}
            public DateTime? AudFechaCreacion  {get;set;}
            public DateTime? AudFechaModificacion  {get;set;}
            public string AudUsuarioCreacion {get;set;}
            public string AudUsuarioModificacion { get; set; }
            public short? IdEstadoSolicitudCredito { get; set; }
            public decimal? ValorSolicitado { get; set; }
            public decimal? ValorCuentaFai { get; set; }
            public decimal? ValorGirarCheque { get; set; }
            public decimal? ValorCruceInterproductos { get; set; }
            public decimal? ValorSaldoAnterior { get; set; }
            public decimal? ValorInmueble { get; set; }
            public short? IdAseguabilidadOpcion { get; set; }
            public string Observaciones { get; set; }
            public short? IdUsoCredito { get; set; }
            public int? IdOficina { get; set; }
            public short? IdTipoGarantia { get; set; }
            public int? fscNumSolicitud { get; set; }
            public int? IdCiuuFomento { get; set; }
            public Boolean? CursoFodes { get; set; }
            public string TipoCapacidadPago { get; set; }

    }
}
