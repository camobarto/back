﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class ConsolidadoAprobacion
    {
        public int IdConsolidadoAprobacion { get; set; }
        public int IdSolicitud { get; set; }
        // public string FechaAprobacion { get; set; }
        public string UsuarioAprobo { get; set; }
    }
}
