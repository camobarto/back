﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class SolicitudCreditoNegado
    {
        public long IdSolicitudCreditoNegacion { get; set; }
        public int IdSolicitud { get; set; }
        public short IdCausalNegacion { get; set; }
        public bool Activo { get; set; }
    }
}
