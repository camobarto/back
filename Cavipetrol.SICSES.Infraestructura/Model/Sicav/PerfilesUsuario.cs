﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    [Table("usuarios_perfiles")]
    public class PerfilUsuario
    {
        [Key]
        public int IdUsuarioPerfil { get; set; }
        public int IdUsuario { get; set; }
        public int Idperfil { get; set; }
    }
}
