﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Sicav
{
    public class CapacidadPago
    {
        public int IdCapacidadPago { get; set; }
        public int IdSolicitud { get; set; }
        public string JsonCapacidadPago { get; set; }
        public short? IdEstadoCapacidadPago { get; set; }
        public DateTime? AudFechaCreacion { get; set; }
        public DateTime? AudFechaModificacion { get; set; }
        public string AudUsuarioCreacion { get; set; }
        public string AudUsuarioModificacion {get; set;}
        public decimal? ValorNegociado { get; set; }

    }
}
