﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Historico
{
    public class Historico
    {
        public Int64 IdHistorico { get; set; }
        public int  IdMenu { get; set; }
        public int IdProceso { get; set; }
        public string Datos { get; set; }
        public DateTime? AudFechaCreacion { get; set; }
        public DateTime? AudFechaModificacion { get; set; }
        public string AudUsuarioCreacion { get; set; }
        public string AudUsuarioModificacion { get; set; }
    }
}
