﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class ArbolAmortizacion
    {
        public int IdArbolAmortizacion { get; set; }
        public short? IdTipoCredito { get; set; }
        public short? IdPapelCavipetrol { get; set; }
        public short? TieneNominaCavipetrol { get; set; }
        public short? VinculoConEcopetrol { get; set; }
        public short? PagoConAFC { get; set; }
        public string TipoCapacidadPago { get; set; }
    }
}
