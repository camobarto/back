﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    [Serializable]
    public class ReporteInformeDeudoresPatronalesyEmpresas
    {
        public string TIPO_DE_IDENTIFICACION { get; set; }
        public string NUMERO_DE_IDENTIFICACION { get; set; }
        public string NUMERO_ASOCIADOS_CON_LIBRANZA { get; set; }
        public string SALDO_TOTAL { get; set; }
        public string SALDO_TOTAL_APORTES { get; set; }
        public string SALDO_TOTAL_CARTERA_CREDITOS { get; set; }
        public string SALDO_TOTAL_DEPOSITOS_DE_AHORRO { get; set; }
        public string SALDO_TOTAL_OTROS_DESCUENTOS { get; set; }
        public string CRITERIOS_RIESGO_CONTRAPARTE { get; set; }
        public string MONTO_TOTAL_DE_DETERIORO_ESTABLECIDO { get; set; }
        public string NUMERO_MESES_INCUMPLIMIENTO { get; set; }
        public string MONTO_ULTIMO_PAGO_RECIBIDO { get; set; }
        public string FECHA_ULTIMO_PAGO_RECIBIDO { get; set; }
        public string NUMERO_PAGOS_POR_FUERA_PLAZO { get; set; }
        public string ACCIONES_JUDICIALES_EN_CONTRA { get; set; }
        public string ESTADO_PROCESO_JUDICIAL_EN_CONTRA { get; set; }
    }
}
