﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteIndividualCarteraCredito
    {        
        public string Tipo_identificacion { get; set; }
        public string Numero_identificacion { get; set; }
        public int? Codigo_contable { get; set; }
        public int? Restructurado { get; set; }
        public string Documento_numero { get; set; }
        public string Fecha_desembolso { get; set; }
        public string Fecha_vencimiento { get; set; }
        public int? Dias_mora { get; set; }
        public int? Tipo_Cuota { get; set; }
        public int? Cuotas_pagadas { get; set; }
        public int? Periodicidad_intereses { get; set; } 
        public int? Modalidad { get; set; }
        public double? Tasa_nominal { get; set; }
        public double? Tasa_efectiva { get; set; }
        public double? Valor_prestado { get; set; }
        public double? Valor_cuota { get; set; }
        public double? Saldo_capital { get; set; }
        public double? Saldo_intereses { get; set; }
        public double? Otros_saldos { get; set; }
        public double? Valor_garantia { get; set; }
        public string FechaUltimoAvaluo { get; set; }
         public int? Deterioro_capital { get; set; }
        public int? Deterioro_intereses { get; set; }
        public int? Contigencia_intereses { get; set; }
        public string ValorCuotasExtra { get; set; }
        public string MesesCuotaExtra { get; set; }
        public string FechaUltimoPago { get; set; }
        public int? Clase_garantia { get; set; }
        public string Destino_credito { get; set; }
        public string Codigo_oficina { get; set; }
        public int? Periodicidad_capital { get; set; }
        public double? ValorCapitalMora { get; set; }
        public string ClaseVivienda { get; set; }
        public string SenalVIS { get; set; }
        public string TipoVivienda { get; set; }
        public string SenalSubsidio { get; set; }
        public string EntidadVIS { get; set; }
        public string MargenRedescuentoVIS { get; set; }
        public int? Sujeto_Desembolso { get; set; }
        public int? Moneda_Credito { get; set; }
        public string Fecha_Restructurado { get; set; }
        public string Categoria_Restructurada { get; set; }
        public double? Aporte_social { get; set; }
        public string Tipo_credito { get; set; }


    }
}

