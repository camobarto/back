﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteCompraVentaCreditos
    {
        public string TIPO_TRANSACCION { get; set; }
        public string FECHA_TRANSACCION { get; set; }
        public string NO_TRANSACCION { get; set; }
        public string VALOR_TRANSACCION { get; set; }
        public string TIPO_IDENTIFICACION_COMPRADOR { get; set; }
        public string NIT_COMPRADOR { get; set; }
        public string NUMERO_CREDITO { get; set; }
        public string TIPO_IDENTIFICACION { get; set; }
        public string NIT { get; set; }
        public string FECHA_DESEMBOLSO_INICIAL { get; set; }
        public string FECHA_VENCIMIENTO { get; set; }
        public string TASA_INTERES_EFECTIVA { get; set; }
        public string PLAZO_CREDITO { get; set; }
        public string TIPO_CUOTA { get; set; }
        public string AMORTIZACION { get; set; }
        public string VALOR_PRESTAMO { get; set; }
        public string SALDO_VENCIDO { get; set; }
        public string SALDO_CAPITAL { get; set; }
        public string SALDO_INTERESES { get; set; }
        public string OTROS_SALDOS { get; set; }
        public string CLASIFICACION { get; set; }
        public string MODALIDAD { get; set; }
        public string LIBRANZA { get; set; }
        public string RESPONSABILIDAD { get; set; }
        public string SUSTITUTO { get; set; }
        public string NOPAGARESUS { get; set; }
        public string VALOR_PAGARE_SUS { get; set; }
        public string RECAUDO { get; set; }
        
    }
}
