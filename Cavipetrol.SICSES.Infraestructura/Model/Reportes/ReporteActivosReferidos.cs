﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    [Serializable]
    public class ReporteActivosReferidos
    {
        public string DESCRIPCION_DIFERIDO { get; set; }
        public string VALOR_INICIAL_DIFERIDO { get; set; }
        public string MESES_ESTA_POR_DIFERIR_FECHA_CORTE { get; set; }
        public string VALOR_AMORTIZACION_MENSUAL { get; set; }
        public string TOTAL_AMORTIZADO_FECHA_CORTE_1 { get; set; }
        public string SALDO_POR_AMORTIZAR_LA_FECHA_CORTE { get; set; }
        public string VALOR_ADICION { get; set; }
        public string FECHA_CONTABLE_ADICION { get; set; }
        public string MESES_DIFERIR_ADICION { get; set; }
        public string VALOR_AMORTIZACION_MENSUAL_ADICION { get; set; }
        public string TIEMPO_MESES_DIFERIR_ADICION { get; set; }
        public string TOTAL_AMORTIZADO_ADICION { get; set; }
        public string SALDO_AMORTIZAR_ADICION { get; set; }
        public string TOTAL_SALDO_RESTA_POR_AMORTIZAR { get; set; }
        public string TOTAL_AMORTIZADO_FECHA_CORTE_2 { get; set; }
        public string CODIGO_CUENTA_DEL_GASTO { get; set; }
        public string CUENTA_PUC_DIFERIDO { get; set; }
        public string FECHA_CONTABILIZACION_DEL_DIFERIDO { get; set; }
        public string MESES_DIFERIR { get; set; }
        public string SALDO_POR_AMORTIZAR_CIERRE_EJERCICIO_ANTERIOR { get; set; }
    }
}
