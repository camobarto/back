﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteInformacionNoReportada
    {
        public int CONSECUTIVO { get; set; }
        public string CODIGO_FORMATO { get; set; }
        public string NOMBRE_FORMATO { get; set; }
        public string MOTIVO { get; set; }
    }
}
