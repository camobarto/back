﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
   public  class ReporteAplicacionExcedentes
    {
        public int UNIDAD_CAPTURA { get; set; }
        public int CODIGO_RENGLON { get; set; }
        public string DESCRIPCION_RENGLON { get; set; }

        public string SALDO { get; set; }

        public int TIPO { get; set; }

    }
}
