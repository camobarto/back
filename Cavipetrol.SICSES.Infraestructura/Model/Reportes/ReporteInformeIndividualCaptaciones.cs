﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteInformeIndividualCaptaciones
    {
        public string Tipo_identificacion { get; set; }
        public string Numero_identificacion { get; set; }
        public string codigo_contable_puc { get; set; }
        public string Nombre_deposito { get; set; }
        public int? Tipo_Ahorro { get; set; }
        public int? Periodicidad_tasa { get; set; }
        public string Fecha_apertura { get; set; }
        public int? plazo { get; set; }
        public string Fecha_vencimiento { get; set; }        
        public int Modalidad { get; set; }
        public double? Tasa_nominal { get; set; }
        public double? Tasa_efectiva { get; set; }        
        public double? InteresesCausados { get; set; }
        public double? Saldo { get; set; }
        public double? Deposito_inicial { get; set; }
        public int? DocumentoNumero { get; set; }
        public int? Excenta_GMF { get; set; }
        public string Fecha_Excepcion_GMF { get; set; }
        public int estado_cuenta { get; set; }
        public int cuenta_bajo_monto { get; set; }
        public int Cotitulares { get; set; }
     //   public int? Periodicidad_tasa { get; set; }

    }
}
