﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteProcesosJudiciales
    {
        public string TIPO_IDENTIFICACION { get; set; }
        public string IDENTIFICACION { get; set; }
        public string NOMBRE { get; set; }
        public string NATURALEZA { get; set;              }
        public string SALDO_DE_LA_OBLIGACION { get; set; }
        public string CUANTIA_DE_LA_DEMANDA { get; set; }
        public string PROVISION { get; set; }
        //public string NUMERO_CREDITO { get; set; }
        public string FECHA_DE_INICIO { get; set; }
        public string CODIGO_AUTORIDAD_JUDICIAL { get; set; }
        public string CODIGO_DEL_MUNICIPIO_AUTORIDAD_JUDICIAL { get; set; }
        public string ESTADO_PROCESO_ACTUAL { get; set; }
        public string NUMPROCESO { get; set; }
        //public string TIPO_CUOTA { get; set; }
        //public string MODALIDAD { get; set; }
        //public string TASA_INTERES_EFECTIVA { get; set; }
        //public string SALDO_CAPITAL { get; set; }
        //public string DESTINO_CREDITO { get; set; }

    }
}
