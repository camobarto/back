﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteParentescos
    {
        public int ID { get; set; }
        public string TIPO_IDENTIFICACION { get; set; }
        public string NUMERO_IDENTIFICACION { get; set; }
        public string TIPO_IDENTIFICACION_PARIENTE { get; set; }
        public string NUMERO_IDENTIFICACION_PARIENTE { get; set; }
        public Int16 PARENTESCOPOR_GRADO_CONSANGUINIDAD { get; set; }
      
    }
}
