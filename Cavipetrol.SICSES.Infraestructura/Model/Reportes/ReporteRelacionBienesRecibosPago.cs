﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteRelacionBienesRecibosPago
    {
        public string CODIGO_CONTABLE { get; set; }
        public string DESCRIPCION_BIEN { get; set; }
        public int CLASE_BIEN { get; set; }
        public string FECHA_RECIBO { get; set; }
        public string NOMBRE_DEUDOR { get; set; }
        public decimal VALOR_OBLIGACION { get; set; }
        public decimal VALOR_RECIBO { get; set; }
        public decimal ADICIONES { get; set; }
        public decimal VALOR_TOTAL { get; set; }
        public string FECHA_ULTIMO_AVALUO { get; set; }
        public decimal VALOR_AVALUO { get; set; }
        public decimal PROVISION { get; set; }
        
    }
}
