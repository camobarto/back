﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    [Serializable]
    public class ReporteInformeCuentasPorPagarOtras
    {
        public string TIPO_DE_IDENTIFICACION { get; set; }
        public string NUMERO_DE_IDENTIFICACION { get; set; }
        public string TIPO_DE_USUARIO { get; set; }
        public string CODIGO_CONTABLE { get; set; }
        public string CONCEPTO { get; set; }
        public string FECHA_DE_CONTABILIZACION { get; set; }
        public string SALDO_FECHA_DE_CORTE { get; set; }
        public string FECHA_DE_VENCIMIENTO { get; set; }
        public string PLAZO { get; set; }
        public string DIAS_DE_MORA { get; set; }
        public string INTERES_POR_MORA { get; set; }
    }
}
