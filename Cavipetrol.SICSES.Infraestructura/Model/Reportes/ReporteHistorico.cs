﻿using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteHistorico
    {
        public int? IdReporteHistorico { get; set; }
        public int? NumeroReporte { get; set; }
        public string NombreReporte { get; set; }
        public string DatosReporte { get; set; }
        public IEnumerable<ReporteUsuarios> DatosReporte2 { get; set; }
        public string Mes { get; set; }
        public string Ano { get; set; }
        public DateTime? FechaReporte { get; set; }
    }
}
