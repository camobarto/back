﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteProductosOfrecidos
    {
        public int CONSECUTIVO { get; set; }
        public int NUMERO_DEL_PRODUCTO { get; set; }
        public string FECHA_DE_VINCULACION { get; set; }
        public string TIPO_PRODUCTO { get; set; }
        public int CODIGO_DEPARTAMENTO { get; set; }
        public string TIPO_IDENTIFICACION_TITULAR1 { get; set; }
        public int NUMERO_IDENTIFICACION_TITULAR1 { get; set; }
        public string PRIMER_APELLIDO_TITULAR1 { get; set; }
        public string SEGUNDO_APELLIDO_TITULAR1 { get; set; }
        public string PRIMER_NOMBRE_TITULAR1 { get; set; }
        public string OTROS_NOMBRE_TITULAR1 { get; set; }
        public string RAZON_SOCIAL_TITULAR1 { get; set; }
        public string TIPO_IDENTIFICACION_TITULAR2 { get; set; }
        public int NUMERO_IDENTIFICACION_TITULAR2 { get; set; }
        public string PRIMER_APELLIDO_TITULAR2 { get; set; }
        public string SEGUNDO_APELLIDO_TITULAR2 { get; set; }
        public string PRIMER_NOMBRE_TITULAR2 { get; set; }
        public string OTROS_NOMBRE_TITULAR2 { get; set; }
        public string RAZON_SOCIAL_TITULAR2 { get; set; }
    }
}

