﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteOrganosDireccionyControl
    {

        public int ID { get; set; }
        public string TIPO_IDENTIFICACION { get; set; }
        public string NUMERO_IDENTIFICACION { get; set; }
        public Int16 PRINCIPALSUPLENTE { get; set; }
        public string FECHA_NOMBRAMIENTO { get; set; }
        public string EMPRESA_REVISORIA_FISCAL { get; set; }
        public Int16 TARJETA_PROFESIONAL_REVISORFISCAL { get; set; }
        public string FECHA_POSESION { get; set; }
        public Int16 PERIODO_VIGENCIA { get; set; }
        public Int16 PARENTESCO { get; set; }
        public Int16 DIRECTIVO_ACCIONISTA { get; set; }
        public string FECHA_CORTE { get; set; }
    }
}
