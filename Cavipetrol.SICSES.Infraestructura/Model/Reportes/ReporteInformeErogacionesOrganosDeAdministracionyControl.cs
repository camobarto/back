﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    [Serializable]
    public class ReporteInformeErogacionesOrganosDeAdministracionyControl
    {
        public string TIPO_DE_IDENTIFICACION { get; set; }
        public string NUMERO_DE_IDENTIFICACION { get; set; }
        public string VALOR_TOTAL_DESEMBOLSO { get; set; }
        public string CONCEPTO { get; set; }
        public string DESCRIPCION_OTRO_TIPO_EROGACION { get; set; }
    }
}
