﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteRetiroeIngresoAsociados
    {
        public string TIPO_IDENTIFICACION { get; set; }
        public string NUMERO_IDENTIFICACION { get; set; }
        public string NOMBRE_ASOCIADO { get; set; }
        public string FECHA_INGRESO { get; set; }
        public string FECHA_RETIRO { get; set; }
        public string CAUSAL_RETIRO { get; set; }
        
    }
}
