﻿using System;
using System.Collections;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteIdentificacion
    {
        public int IdReporteIdentificacion { get; set; }
        public int IdParametroCavipetrol { get; set; }
        public string Categoria { get; set; }
        public string Sector { get; set; }
        public string Grado { get; set; }
        public string TipoEntidad { get; set; }
        public string ClasificacionCooperativas { get; set; }
        public string ActividadEconomica { get; set; }
        public string ClasificacionEconomica { get; set; }
        public int? NumeroResolucionActividadFinanciera { get; set; }
        public DateTime? FechaResolucionActvidadFinanciera { get; set; }
        public int? NumeroInscripcionFOAGCOOP { get; set; }
        public DateTime? FechaResolucionFOAGCOOP { get; set; }
        public int? NumeroResolucionDesmonte { get; set; }
        public DateTime? FechaResolucionDesmonte { get; set; }
        public bool? PreCooperativa { get; set; }
        public string ClaseTransporte { get; set; }
        public string SubClaseTransporte { get; set; }
        public string SenalVinculo { get; set; }
        public int? RegistroCamaraComercio { get; set; }
        public string Estado { get; set; }
        public DateTime? FechaLiquidacion { get; set; }
        public int? NumeroResolucion { get; set; }
        public DateTime? FechaResolucionLiquidacion { get; set; }
        public string CompaniaAseguradora { get; set; }
        public DateTime? FechaProximaRenovacion { get; set; }
        public string AsosiacionAfiliada { get; set; }
        public int? ResolucionMinTransporte { get; set; }
        public int? CapacidadTransporteMinima { get; set; }
        public int? CapacidadTransporteMaxima { get; set; }
        public ParametroUsuarioCavipetrol ParametroCavipetrol { get; set; }
    }
}
