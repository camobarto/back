﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteUsuarios
    {
        public string tipo_identificacion { get; set; } 
        public string identificacion { get; set; }
        public string primer_apellido { get; set; }
        public string segundo_apellido { get; set; }
        public string razon_social { get; set; }
        public string fecha_ingreso { get; set; }
        public string TelefonoResidencia { get; set; }
        public string direccion_residencia { get; set; }
        public string rol { get; set; }
        public int activo { get; set; }
        public string actividad_economica { get; set; }
        public string codigo_municipio { get; set; }
        public string correo { get; set; }
        public string genero { get; set; }
        public string empleado { get; set; }
        public string tipo_contrato { get; set; }
        public string nivel_escolaridad { get; set; }
        public string estrato { get; set; }
        public string nivel_ingresos { get; set; }
        public string fecha_nacimiento { get; set; }
        public string estado_civil { get; set; }
        public string mujer_cabeza { get; set; }
        public string ocupacion { get; set; }
        public string sector_economico { get; set; }
        public string jornada_laboral { get; set; }
        public string fecha_retiro { get; set; }
        public string asistio_asamblea { get; set; }
    }
}
