﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteAportesContribuciones
    {
        public string TIPO_IDENTIFICACION { get; set; }
        public int NUMERO_IDENTIFICACION { get; set; }
        public double SALDO_FECHA_APORTES_SOCIALES { get; set; }
        public double VALOR_APORTE_MENSUAL { get; set; }
        public double VALOR_APORTE_ORDINARIO { get; set; }
        public int VALOR_APORTE_EXTRAORDINARIO { get; set; }
        public double? VALOR_REVALORIZACION { get; set; }
        public int MONTO_PROMEDIO { get; set; }
        public string FECHA_PAGO_APORTES_CONTRIBUCIONES { get; set; }

    
    }
}
