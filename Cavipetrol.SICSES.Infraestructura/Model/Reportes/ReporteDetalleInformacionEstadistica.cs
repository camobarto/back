﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteDetalleInformacionEstadistica
    {
        public string TIPONIT { get; set; }
        public string NIT { get; set; }
        public DateTime? FECHANACIMIENTO { get; set; }
        public string SEXO { get; set; }
        public int MES { get; set; }
        public string PAPEL { get; set; }
        public string DESCRIPCION { get; set; }
    }
}
