﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    [Serializable]
    public class ReporteIngresosRecibidosParaTerceros
    {        
        public string TIPO_IDENTIFICACION { get; set; }
        public string IDENTIFICACION { get; set; }
        public string CONCEPTO { get; set; }
        public string VALOR_RECIBIDO { get; set; }
        public string FECHA_CONTABILIZACION { get; set; }
        public string CODIGO_CONTABLE { get; set; }
    }
}
