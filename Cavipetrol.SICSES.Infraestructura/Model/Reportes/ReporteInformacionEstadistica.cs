﻿namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteInformacionEstadistica
    {
        public string Columna { get; set; }
        public int Id { get; set; }
        public double Valor { get; set; }
        public decimal Porcentaje { get; set; }
    }
}
