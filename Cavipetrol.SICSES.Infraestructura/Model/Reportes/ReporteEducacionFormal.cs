﻿namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteEducacionFormal
    {
        public string NIT_ENTIDAD { get; set; }
        public string NOMBRE_ENTIDAD_FINANCIERA { get; set; }
        public string TIPO_ENTIDAD_BENEFICIARIA { get; set; }
        public string RESOLUCION_MINEDUCACION { get; set; }
        public string CODIGO_DE_MUNICIPIO { get; set; }
        public string DESTINO { get; set; }
        public string NIVEL_DE_EDUCACION { get; set; }
        public string FONDO_ORIGEN_DE_RECURSOS { get; set; }
        public string BENEFICIARIOS { get; set; }
        public string ESTRATO { get; set; }
        public string NUMERO_DE_BENEFICIARIOS { get; set; }
        public string MONTO_DE_RECURSOS_DESEMBOLSADOS { get; set; }
        public string DECRETO2880 { get; set; }
        public string FECHA_DESEMBOLSO { get; set; }



    }
}
