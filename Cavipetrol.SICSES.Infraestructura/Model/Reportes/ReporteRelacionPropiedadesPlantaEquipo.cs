﻿using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    [DataContract(Name = "ReporteRelacionPropiedadesPlantaEquipo")]
    public class ReporteRelacionPropiedadesPlantaEquipo
    {
        [DataMember(Name = "CodigoContable")]
        public string CodigoContable { get; set; }

        [DataMember(Name = "Descripcion")]
        public string Descripcion { get; set; }

        [DataMember(Name = "UsoDeBien")]
        public int Uso_bien { get; set; }

        [DataMember(Name = "Fecha_Adquisicion")]
        public string Fecha_Adquisicion { get; set; }

        [DataMember(Name = "CostoHistorico")]
        public decimal Costo_historico { get; set; }        
        
        [DataMember(Name = "Adiciones")]
        public decimal Adiciones { get; set; }

        [DataMember(Name = "DepreciacionAcumulada")]
        public decimal Depreciación_acumulada { get; set; }

        [DataMember(Name = "ValorLibros")]
        public decimal Valor_libros { get; set; }

        [DataMember(Name = "FechaAvaluo")]
        public string Fecha_avaluo { get; set; }

        [DataMember(Name = "ValorAvaluo")]
        public decimal Valor_avalúo { get; set; }

        [DataMember(Name = "Provision")]
        public decimal Provision { get; set; }

        [DataMember(Name = "Valorizacion")]
        public decimal Valorizacion { get; set; }

        [DataMember(Name = "Grupo")]
        public int Grupo { get; set; }

        [DataMember(Name = "FechaPagoImpuesto")]
        public string Fecha_pago_impuesto { get; set; }

        [DataMember(Name = "Agotamiento")]
        public int Agotamiento { get; set; }

        [DataMember(Name = "GravamenJuridico")]
        public int Gravamen_juridico { get; set; }

        [DataMember(Name = "DescripcionGravamenJuridico")]
        public string Descripcion_gravamen_juriidico { get; set; }

        [DataMember(Name = "TiempoDepreciar")]
        public int? Tiempo_Depreciar { get; set; }

        [DataMember(Name = "ValorDepreciacionMensual")]
        public decimal Depreciacion_Mensual { get; set; }
        
        
        [DataMember(Name = "DadoBaja")]
        public int DadoBaja { get; set; }

        [DataMember(Name = "ValorDadoBaja")]
        public decimal ValorDadoBaja { get; set; }

        [DataMember(Name = "NumeroActaAprobacion")]
        public int NumeroActaAprobacion { get; set; }

        [DataMember(Name = "FechaActa")]
        public string FechaActa { get; set; }
    }
}
