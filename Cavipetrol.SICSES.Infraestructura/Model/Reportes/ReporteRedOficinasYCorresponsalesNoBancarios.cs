﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteRedOficinasYCorresponsalesNoBancarios
    {
        //public int ID { get; set; }
        public string fecha_apertura { get; set; } 
        public string fecha_cierre { get; set; }
        public int sede_propia { get; set; }
        public Nullable<decimal> Total_aportes { get; set; }
        public Nullable<decimal> Total_cartera { get; set; }
        public Nullable<decimal> Total_depositos { get; set; }
        public Nullable<decimal> Total_asociados { get; set; }
        public Nullable<decimal> Total_empleados { get; set; }
        public Nullable<decimal> Gastos_de_Peronal { get; set; }
        public Nullable<decimal> Gastos_Negenerales { get; set; }

        public string Codigo_municipio { get; set; }

        public Nullable<int> Nomero_habitantes { get; set; }
        public Nullable<int> Entidades_financieras { get; set; }
        public Nullable<int> Nomero_Coopertativas { get; set; }

        public string Codigo_oficina { get; set; }
        public string Nombre_Oficina { get; set; }
        public Nullable<int> IndicadorCorresponsal { get; set; }
        public Nullable<int> Tiponegocio { get; set; }
        public Nullable<decimal> Total_Ingresos { get; set; }
        public Nullable<decimal> Total_Costos { get; set; }
        public Nullable<decimal> OtrosGastosAdm { get; set; }
        public Nullable<decimal> ValoreExecedentesOFIC { get; set; }
        public Nullable<int> VAlidado { get; set; }
        
    }

   

}
