﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    [Serializable]
    public class ReporteContratacion
    {
        public string TIPO_DE_IDENTIFICACION { get; set; }
        public string NUMERO_DE_IDENTIFICACION { get; set; }
        public string MODALIDAD_CONTRATACION { get; set; }
        public string OTRA_MODALIDAD { get; set; }
        public string PRODUCTO_SERVICIO_CONTRATADO { get; set; }
        public string NUMERO_CONTRATO { get; set; }
        public string OBJETO_CONTRATADO { get; set; }
        public string DETALLE_CONTRATADO { get; set; }
        public string CUANTIA_DEL_CONTRATO { get; set; }
        public string COD_MUNICIPIO_EJECUCION { get; set; }
        public string DIRECCION_EJECUCCION { get; set; }
        public string FECHA_INICIO_CONTRATO { get; set; }
        public string FECHA_FIN_CONTRATO { get; set; }
        public string PERSONAL_REQUERIDO_EJECUCION { get; set; }
        public string PRESENTA_OTRO_SI { get; set; }
        public string CONDICION_MEDIOS_MATERIALES { get; set; }
    }
}
