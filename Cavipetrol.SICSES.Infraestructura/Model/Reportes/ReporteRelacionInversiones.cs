﻿using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    [DataContract(Name = "ReporteRelacionInversiones")]
    public class ReporteRelacionInversiones
    {
        [DataMember(Name = "CodigoContable")]
        public string CodigoContable { get; set; }

        [DataMember(Name = "Nit")]
        public string Nit { get; set; }

        [DataMember(Name = "NombreEmisor")]
        public string NombreEmisor { get; set; }

        [DataMember(Name = "ClaseTitulo")]
        public string ClaseTitulo { get; set; }

        [DataMember(Name = "NumeroTitulo")]
        public string NumeroTitulo { get; set; }

        [DataMember(Name = "TipoTitulo")]
        public string TipoTitulo { get; set; }

        [DataMember(Name = "FechaEmision")]
        public string FechaEmision { get; set; }

        [DataMember(Name = "FechaVencimiento")]
        public string FechaVencimiento { get; set; }

        [DataMember(Name = "ValorNominal")]
        public string ValorNominal { get; set; }

        [DataMember(Name = "Moneda")]
        public string Moneda { get; set; }

        [DataMember(Name = "TipoTasa")]
        public string TipoTasa { get; set; }

        [DataMember(Name = "TasaFijaPuntos")]
        public string TasaFijaPuntos { get; set; }

        [DataMember(Name = "ValorUnidad")]
        public string ValorUnidad { get; set; }

        [DataMember(Name = "Modalidad")]
        public string Modalidad { get; set; }

        [DataMember(Name = "PeriodicidadPago")]
        public string PeriodicidadPago { get; set; }

        [DataMember(Name = "FechaCompra")]
        public string FechaCompra { get; set; }

        [DataMember(Name = "ValorCompra")]
        public string ValorCompra { get; set; }

        [DataMember(Name = "TasaCompra")]
        public string TasaCompra { get; set; }

        [DataMember(Name = "NumeroAccionesCuotas")]
        public string NumeroAccionesCuotas { get; set; }

        [DataMember(Name = "FechaValoracion")]
        public string FechaValoracion { get; set; }

        [DataMember(Name = "ValorActual")]
        public string ValorActual { get; set; }

        [DataMember(Name = "TasaEfectivaValoracion")]
        public string TasaEfectivaValoracion { get; set; }

        [DataMember(Name = "ValorUnidadVal")]
        public string ValorUnidadVal { get; set; }

        [DataMember(Name = "Valoracion")]
        public string Valoracion { get; set; }

        [DataMember(Name = "Deterioro")]
        public string Deterioro { get; set; }

        [DataMember(Name = "BaseMedicion")]
        public string BaseMedicion { get; set; }

    }
}
