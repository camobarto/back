﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
   public  class ReporteIngresoyRetiro
    {
        public string TIPO_IDENTIFICACION { get; set; }
        public string NUMERO_DE_IDENTIFICACION { get; set; }
        public string NOMBRE_DE_ASOCIADO { get; set; }
        public string FECHA_DE_INGRESO { get; set; }
        public string FECHA_DE_RETIRO { get; set; }
        public string CAUSAL_DE_RETIRO { get; set; }
    }
}
