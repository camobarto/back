﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    [Serializable]
    public class ReporteActivosCastigados
    {
        public string TIPO_DE_IDENTIFICACION { get; set; }
        public string NUMERO_DE_IDENTIFICACION { get; set; }
        public string CLASE_ACTIVO { get; set; }
        public string NUMERO_PAGARE { get; set; }
        public string SALDO_CAPITAL { get; set; }
        public string SALDO_INTERESES { get; set; }
        public string DIAS_MORA { get; set; }
        public string CATEGORIA { get; set; }
        public string PROVISION_CAPITAL { get; set; }
        public string PROVISION_INTERESES { get; set; }
        public string VALOR_APORTES { get; set; }
        public string VALOR_AHORROS { get; set; }
        public string VALOR_CASTIGO_CAPITAL { get; set; }
        public string VALOR_CASTIGO_INTERESES { get; set; }
        public string FECHA_DE_CASTIGO { get; set; }
        public string NUMERO_ACTA_DE_CONSEJO { get; set; }
        public string CONCEPTO_ABOGADO { get; set; }
        public string FECHA_ACTA_APROBACION { get; set; }

    }
}
