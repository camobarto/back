﻿using Cavipetrol.SICSES.Infraestructura.Respuesta;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReportePUC
    {
        
        public string CUENTA { get; set; }
        public string DESCRIPCION_CUENTA { get; set; }
        public double SALDO { get; set; }
    }
}
