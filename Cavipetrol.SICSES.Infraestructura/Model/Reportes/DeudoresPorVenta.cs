﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class DeudoresPorVenta
    {
          public string TIPO_IDENTIFICACION { get; set; }
          public string NUMERO_DE_IDENTIFICACION { get; set; }
          public string CODIGO_CONTABLE { get; set; }
          public string CODIGO_INTERNO_DEL_CLIENTE { get; set; }
          public string PAIS_DE_DESTINO { get; set; }
          public string CLASE_DOCUMENTO_COBRO { get; set; }
          public string NUMERO_DE_DOCUMENTO { get; set; }
          public string FECHA_ACEPTACION_FACTURA { get; set; }
          public string FECHA_DE_VENCIMIENTO { get; set; }
          public string PLAZO { get; set; }
          public string VALOR_DE_VENTA { get; set; }
          public string NOTAS_DEBITO { get; set; }
          public string NOTAS_CREDITO { get; set; }
          public string DEVOLUCIONES { get; set; }
          public string SALDO_DE_LA_OPERACION { get; set; }
          public string TIPO_DE_MONEDA { get; set; }
          public string TASA_DE_CAMBIO { get; set; }
          public string TASA_DE_INTERES { get; set; }
          public string MOROSIDAD { get; set; }
          public string CLASE_DE_GARANTIA { get; set; }
          public string SALDO_INTERESES { get; set; }
          public string OTROS_SALDOS { get; set; }
          public string PROVISION_CAPITAL { get; set; }
          public string PROVISION_INTERESES_Y_OTROS { get; set; }
          public string CONTINGENCIA { get; set; }
          public string ESTADO_ACTUAL { get; set; }
          public string GRUPO { get; set; }
          public string CODIGO_OFICINA { get; set; }

    }
}
