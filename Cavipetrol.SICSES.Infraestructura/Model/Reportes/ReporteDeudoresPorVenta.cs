﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReporteDeudoresPorVenta
    {
        public string TIPO_IDENTIFICACION { get; set; }
        public string NUMERO_DE_IDENTIFICACION { get; set; }
        public int CODIGO_CONTABLE { get; set; }
        public int  CODIGO_INTERNO_DEL_CLIENTE { get; set; }
        public int PAIS_DE_DESTINO{ get; set; }
        public string CLASE_DOCUMENTO_COBRO { get; set; }
        public int NUMERO_DE_DOCUMENTO { get; set; }
        public string FECHA_ACEPTACION_FACTURA { get; set; }
        public string FECHA_DE_VENCIMIENTO { get; set; }
        public string PLAZO { get; set; }
        public int VALOR_DE_VENTA { get; set; }
        public string NOTAS_DEBITO { get; set; }
        public string NOTAS_CREDITO { get; set; }
        public string DEVOLUCIONES { get; set; }
        public string SALDO_DE_LA_OPERACION { get; set; }
        public string TIPO_DE_MONEDA { get; set; }
        public int TASA_DE_CAMBIO { get; set; }
        public int TASA_DE_INTERES { get; set; }
        public int MOROSIDAD { get; set; }
        public string CLASE_DE_GARANTIA { get; set; }
        public int SALDO_INTERESES { get; set; }
        public int OTROS_SALDOS { get; set; }
        public string PROVISION_CAPITAL { get; set; }
        public string PROVISION_INTERESES_Y_OTROS { get; set; }
        public string CONTINGENCIA { get; set; }
        public string ESTADO_ACTUAL { get; set; }
        public string GRUPO { get; set; }
        public int CODIGO_OFICINA { get; set; }
    }
}
