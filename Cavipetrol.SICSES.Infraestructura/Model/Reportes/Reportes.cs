﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    public class ReportesSaldos
    {
        public Int32 UnidadCaptura {get; set;}
        public Int64 CodigoRenglon { get; set; }
        public string DescripcionRenglon { get; set; }
        public Double Saldo { get; set; }
        
    }
}
 