﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    [Serializable]
    public class ReporteIndividualDeCarteraDeCredito
    {
        public string TIPO_DE_IDENTIFICACION { get; set; }
        public string NUMERO_DE_IDENTIFICACION { get; set; }
        public string CODIGO_CONTABLE { get; set; }
        public string REESTRUCTURADO { get; set; }
        public string NUMERO_DE_CREDITO { get; set; }
        public string FECHA_DESEMBOLSO_INICIAL { get; set; }
        public string FECHA_DE_VENCIMIENTO { get; set; }
        public string MOROSIDAD { get; set; }
        public string TIPO_DE_CUOTA { get; set; }
        public string NUMERO_DE_CUOTAS_PAGADAS { get; set; }
        public string PERIODICIDAD_AMORTIZACION_DE_INTERESES { get; set; }
        public string MODALIDAD { get; set; }
        public string TASA_DE_INTERES_NOMINAL { get; set; }
        public string TASA_DE_INTERES_EFECTIVA { get; set; }
        public string VALOR_PRESTAMO { get; set; }
        public string VALOR_CUOTA { get; set; }
        public string SALDO_DE_CAPITAL { get; set; }
        public string SALDO_DE_INTERESES { get; set; }
        public string OTROS_SALDOS { get; set; }
        public string VALOR_GARANTIA_DIFERENTE_DE_APORTES { get; set; }
        public string FECHA_ULTIMO_AVALUO { get; set; }
        public string DETERIORO { get; set; }
        public string DETERIORO_INTERESES_Y_OTROS_SALDOS { get; set; }
        public string CONTINGENCIA_INTERESES { get; set; }
        public string VALOR_CUOTAS_EXTRAORDINARIAS { get; set; }
        public string MESES_CUOTAS_EXTRAORDINARIAS { get; set; }
        public string FECHA_DEL_ULTIMO_PAGO { get; set; }
        public string CLASE_DE_GARANTIA { get; set; }
        public string DESTINO_DEL_CREDITO { get; set; }
        public string CODIGO_OFICINA { get; set; }
        public string PERIODICIDAD_AMORTIZACION_DE_CAPITAL { get; set; }
        public string VALOR_CAPITAL_CUOTAS_EN_MORA { get; set; }
        public string CLASE_DE_VIVIENDA { get; set; }
        public string SENAL_VIS { get; set; }
        public string TIPO_O_RANGO_DE_VIVIENDA { get; set; }
        public string SENAL_DE_SUBSIDIO { get; set; }
        public string ENTIDAD_DE_REDESCUENTO { get; set; }
        public string MARGEN_DE_REDESCUENTO { get; set; }
        public string SENAL_DE_SUJETO_DE_DESEMBOLSO { get; set; }
        public string MONEDA_DEL_CREDITO { get; set; }
        public string FECHA_DE_REESTRUCTURADO { get; set; }
        public string CATEGORIA_EN_REESTRUCTURADO { get; set; }
        public string VALOR_APORTES_SOCIALES { get; set; }
        public string VALOR_LINEA_DE_CREDITO_DE_LA_ENTIDAD { get; set; }

    }
}
