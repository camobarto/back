﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.Reportes
{
    [Serializable]
    public class ReporteFondoDeLiquidez
    {
        public string NIT { get; set; }
        public string NOMBRE_ENTIDAD { get; set; }
        public string TIPO_TITULO { get; set; }
        public string NUMERO_TITULO { get; set; }
        public string VALOR_CAPITAL { get; set; }
        public string VALOR_INTERESES { get; set; }
        public string PLAZO { get; set; }
        public string FECHA_VENCIMIENTO { get; set; }
        public string CAPITALIZA_INTERESES { get; set; }
    }
}
