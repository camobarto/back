﻿namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class ClasificacionExcedentes
    {

        public int UNIDAD_CAPTURA { get; set; }
        public int CODIGO_RENGLON { get; set; }
        public string DESCRIPCION_RENGLON { get; set; }
        public string SALDO { get; set; }

        public int TIPO { get; set; }
    }
}
