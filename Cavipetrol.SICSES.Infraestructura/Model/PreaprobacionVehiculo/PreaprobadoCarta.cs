﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model.PreaprobacionVehiculo
{
    public class PreaprobadoCarta
    {
        public int NumeroConsecutivo { get; set;}
        public int Cupo { get; set; }
        public String DeudorNombre { get; set; }
        public int Tasa { get; set; }
        public String DeudorCedula { get; set; }
        public String Ciudad { get; set; }        
        public String Periodicidad { get; set; }
        public DateTime FechaGeneracion { get; set; }
        public int Tarea { get; set; }
    }
}
