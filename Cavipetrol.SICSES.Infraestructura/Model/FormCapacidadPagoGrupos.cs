﻿using Cavipetrol.SICSES.Infraestructura.Model.CoreParametros;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Model
{
    public class FormCapacidadPagoGrupos
    {
        public short IdGrupo { get; set; }
        public string Nombre { get; set; }
        public List<FormCapacidadPago> ListaFormCapacidadPago { get; set; }

        public double? Total { get; set; }

        public bool? MostrarTotal { get; set; }
        public bool? Mostrar { get; set; }

        public int? Orden { get; set; }
        [NotMapped]
        public virtual HorasExtras HorasExtras { get; set; }
        [NotMapped]
        public virtual List<FormaPago> FormaPago { get; set; }       
    }    
}
