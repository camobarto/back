﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Respuesta
{
    public class Respuesta_Consulta_Adjudicacion<T> where T : class
    {
        public Respuesta_Consulta_Adjudicacion()
        {
            MensajesError = new List<string>();
        }
        public bool Estado { get; set; } = false;
        public T Respuesta { get; set; }
        public List<string> MensajesError { get; set; }
    }
}
