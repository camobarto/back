﻿namespace Cavipetrol.SICSES.Infraestructura.Respuesta
{
    public class RespuestaReporte<T> where T : class
    {
        public bool EsHistorico { get; set; }
        public T InformacionReporte { get; set; }
    }
}
