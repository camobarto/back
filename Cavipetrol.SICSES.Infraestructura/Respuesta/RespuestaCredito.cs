﻿using System.Collections.Generic;

namespace Cavipetrol.SICSES.Infraestructura.Respuesta
{
    public class RespuestaNegocio<T>  where T : class
    {
        public RespuestaNegocio()
        {
            MensajesError = new List<string>();
        }
        public bool Estado { get; set; } = false;
        public T Respuesta { get; set; }
        public List<string> MensajesError { get; set; }
    }
}
