﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cavipetrol.SICSES.Infraestructura.Model.Sicav;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Menu
{
    public class ViewModelMenu
    {
        public int IdMenu { get; set; }
        public string Nombre { get; set; }
        public int? IdMenuOrigen { get; set; }
        public string Ruta { get; set; }
        public string Icono { get; set; }
        public int Orden { get; set; }
        public List<ViewModelMenu> SubMenu { get; set; }
        public Boolean hijos { get; set; }
    }
}
