﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos
{
    public class ViewModelSolicitudCreditoDetalle
    {        
        public int IdSolicitud { get; set; }
        public List<FormCapacidadPagoGrupos> ListaFormCapacidadPagoGrupos { get; set; }
        public HorasExtras HorasExtras { get; set; }
        public string Observaciones { get; set; }

        public int Plazo { get; set; }

    }
}
