﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos
{
    public class ViewModelConsolidadoAtribuciones
    {
        public string Mensaje { get; set; }
        public string TipoMsg { get; set; }
        public int Resultado { get; set; }

    }
}
