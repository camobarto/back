﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos
{
    public class VMSolicitudCreditoCodeudores
    {
        public List<FormCapacidadPagoGrupos> listaFormCapacidadPagoGrupos { get; set; }
        public List<FormCapacidadPagoCodeudoresGrupos> listaFormCapacidadPagoCodeudoresGrupos { get; set; }
        public List<FormCapacidadPagoTerceroCodeudor> listaFormCapacidadPagoTerceroCodeudor { get; set; }
        public List<DetalleFormCapacidadPago> listaFormCapacidadPagoOtrosIngresos { get; set; }
            


    }
}
