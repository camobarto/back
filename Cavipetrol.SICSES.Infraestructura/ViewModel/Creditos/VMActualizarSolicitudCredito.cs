﻿using Cavipetrol.SICSES.Infraestructura.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos
{
    public class VMActualizarSolicitudCredito
    {
        public List<SelectListItem> causalesNegacion { get; set; }
        public List<FormCapacidadPagoGrupos> FormCapacidadPagoGrupos { get; set; }
        public decimal? ValorMenor { get; set; }
    }
}
