﻿using Cavipetrol.SICSES.Infraestructura.Model;
using Cavipetrol.SICSES.Infraestructura.Model.SolicitudCreditos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos
{
    public class ViewModelFormCapacidadPagoGrupos
    {
        public short IdGrupo { get; set; }
        public string Nombre { get; set; }
        public List<FormCapacidadPago> ListaFormCapacidadPago { get; set; }

        public double? Total { get; set; }

        public bool? MostrarTotal { get; set; }
        public bool? Mostrar { get; set; }

        public int? Orden { get; set; }

        public HorasExtras HorasExtras { get; set; }
    }
}
