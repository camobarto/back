﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos
{
    public class SolicitudesCreditoDTO
    {
        public long? ConsecutivoSolicitud { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public double? ValorSolicitado { get; set; }
        public double? ValorCuentaFai { get; set; }
        public double? ValorGirarCheque { get; set; }
        public double? ValorCruceInterproductos { get; set; }
        public double? ValorSaldoAnterior { get; set; }
        public double? ValorInmueble { get; set; }
        public string OpcionAsegurabilidad { get; set; }
        public string EstadoSolicitudCredito { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioModificacion { get; set; }
        public string Observaciones { get; set; }
        public string UsoCredito { get; set; }
        public string Oficina { get; set; }
        public string Ciudad { get; set; }
        public string TipoGarantia { get; set; }
        public string Fuente { get; set; }
    }
}
