﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Creditos
{
    public class ViewModelSolicitudCredito
    {
        public int IdSolicitud { get; set; }
        public long ConsecutivoSolicitud { get; set; }
        public string TipoCredito { get; set; }
        public string TipoIdentificacion { get; set; }
        public string Identificacion { get; set; }
        public string Nombre { get; set; }
        public string TipoLinea { get; set; }        
        public DateTime FechaCreacion { get; set; }
        public string EstadoSolicitud { get; set; }
        public string AsegurabilidadOpcion { get; set; }
        public short IdTipoCredito { get; set; }
        public int IdTipoLineaCredito { get; set; }
        public decimal? ValorSolicitado { get; set; }
        public decimal? ValorCuentaFai { get; set; }
        public decimal? ValorGirarCheque { get; set; }
        public decimal? ValorCruceInterproductos { get; set; }
        public decimal? ValorSaldoAnterior { get; set; }
        public string IdTipoIdentificacion { get; set; }
        public decimal? ValorInmueble { get; set; }
        public Int16? IdAseguabilidadOpcion { get; set; }
        public int? Edad { get; set; }
        public double? PorcExtraprima { get; set; }
        public int IdTipoCreditoTipoRolRelacion { get; set; }
        public string Usuario { get; set; }
        public short IdEstadoSolicitudCredito { get; set; }
        public decimal? ValorPreAprobado { get; set; }
        public short? idTipoGarantia { get; set; }
        public string norma { get; set; }
        public double? tasaEA { get; set; }
        public string formaPago { get; set; }
        public string IdProductoFYC { get; set; }
        public string TipoCapacidadPago { get; set; }
        public string Email { get; set; }
    }
}
