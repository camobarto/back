﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Administracion
{
    public class ViewTipoCreditoTipoRelacion
    {
        public int IdTipoCreditoTipoRolRelacion { get; set; }
        public short IdTipoCredito { get; set; }
        public string  NombreTipoCredito { get; set; }
        public short IdPapelCavipetrol { get; set; }
        public string NombrePapelCavipetrol { get; set; }
        public double? PlazoMaximo { get; set; }
        public short IdTipoCupoMaximo { get; set; }
        public string NombreCupoMaximo { get; set; }
        public double? CupoMaximo { get; set; }
        public double? InteresNominalQuincenaVendida { get; set; }
        public double? InteresEfectivoAnual { get; set; }
        public double? PorcentajeNovacion { get; set; }
        public double? TiempoMinimoDeAfiliacionAnhos { get; set; }
        public double? PlazoMinimo { get; set; }
        public string IdProductoFYC { get; set; }
    }
}
