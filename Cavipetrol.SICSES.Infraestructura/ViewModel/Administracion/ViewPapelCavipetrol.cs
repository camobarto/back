﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Administracion
{
    public class ViewPapelCavipetrol
    {
        public short IdPapelCavipetrol { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public short IdTipoContrato { get; set; }
        public string NombreContrato { get; set; }
    }
}
