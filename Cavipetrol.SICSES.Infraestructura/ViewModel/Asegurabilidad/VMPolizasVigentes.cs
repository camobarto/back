﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad
{
    public class VMPolizasVigentes
    {
        public string Aseguradora { get; set; }
        public double? ValorAsegurado { get; set; }
        public string TipoPoliza { get; set; }
        public DateTime? Desde { get; set; }
        public DateTime? Hasta { get; set; }        
		public int? AplicaColectiva { get; set; }
    }
}
