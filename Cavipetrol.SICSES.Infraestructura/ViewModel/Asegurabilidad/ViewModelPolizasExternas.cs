﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Asegurabilidad
{
   public  class ViewModelPolizasExternas
    {        
        public string tipoIdentificacion { get; set; }
        public string NumeroDocumento { get; set; }
        public string Nombre { get; set; }
        public int Aseguradora { get; set; }
        public string NumeroPoliza { get; set; }
        public double ValorAsegurado { get; set; }
        public int TipoPoliza { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public int TSE_AplicarColectiva { get; set; }
        public string nitTomador { get; set; }
        public string NumeroTomador { get; set; }
        public string NombreTomaPoliza { get; set; }            
        public string Observaciones { get; set; }
        public string Direcciones { get; set; }
        public string Matricula { get; set; }
        public string oficina { get; set; }
        public string Producto { get; set; }
        public string NumeroDocu { get; set; }
        public bool Editando { get { return true; } set { } }
        public int dias { get; set; }
        public int idAsegurado { get; set; }
        public string EstadoPoliza { get; set; }
    }
}
