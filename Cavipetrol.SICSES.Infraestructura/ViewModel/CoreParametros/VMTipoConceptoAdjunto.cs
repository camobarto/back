﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros
{
    public class VMTipoConceptoAdjunto
    {
        public int IdTipoAdjunto { get; set; }
        public int IdConceptoAdjunto { get; set; }
        public string Concepto { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
    }
}
