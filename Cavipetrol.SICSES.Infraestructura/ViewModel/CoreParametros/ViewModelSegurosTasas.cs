﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros
{
    public class ViewModelSegurosTasas
    {
        public double Incendio { get; set; }
        public double Vida { get; set; }
    }
}
