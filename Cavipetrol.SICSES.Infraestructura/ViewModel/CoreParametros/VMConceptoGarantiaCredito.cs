﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros
{
    public class VMConceptoGarantiaCredito
    {
        public int IdPagare { get; set; }
        public int IdConceptoGarantia { get; set; }
        public string Concepto { get; set; }
        public string Valor { get; set; }
        public string CampoRemplaza { get; set; }
        public string tipoVariable { get; set; }
        public string pagare { get; set; }
        public string pkFycItemDocumento { get; set; }
        public bool? visible { get; set; }
        public bool? editable { get; set; }
        public bool? ciudad { get; set; }

    }
}
