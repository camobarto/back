﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros
{
    public class ViewModelTiposCreditoTiposRol
    {
        public int IdTipoCreditoTipoRolRelacion { get; set; }
        public short IdTipoCredito { get; set; }
        public short IdTipoRol { get; set; }
        public short IdPapelCavipetrol { get; set; }
        public string Nombre { get; set; }
        public int? IdTipoLineaCredito { get; set; }
        public double? TiempoMinimoDeAfiliacionAnhos { get; set; }
        public double? PlazoMaximo { get; set; }
        public double? PlazoMinimo { get; set; }
        public double? CupoMaximo { get; set; }
        public double? Aportes { get; set; }
        public short? AntiguedadAnhos { get; set; }
        public string IdProductoFYC { get; set; }
        public double? TasaEA { get; set; }
        public Boolean Activo { get; set; }
        public double? InteresNominal { get; set; }
        public double? PorcentajeNovacion { get; set; }
        public int TipoVehiculo { get; set; }
        public string Norma { get; set; }
    }
}