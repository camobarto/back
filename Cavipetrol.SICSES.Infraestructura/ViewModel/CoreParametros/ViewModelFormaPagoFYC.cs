﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.CoreParametros
{
    public class ViewModelFormaPagoFYC
    {
        public int? NRM_PlazoMínimo { get; set; }
        public int? NRM_PlazoMáximo { get; set; }
        public string NRM_Granularidad { get; set; }
        public double? ANM_Máximo { get; set; }
        public double? ANM_Mínimo { get; set; }
        public int? EGV_Clave { get; set; }
        public string PET_Equivalencia { get; set; }
        public string CLA_Tipo { get; set; }
        public int IdFormaPago { get; set; }
    }
}
