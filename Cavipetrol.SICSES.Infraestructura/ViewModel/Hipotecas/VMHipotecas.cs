﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Hipotecas
{
    public class VMHipotecas
    {
        public int IdHipoteca { get; set; }
        public string NumeroHipoteca { get; set; }
        public DateTime? Vigencia { get; set; }
        public double? ValorAvaluo { get; set; }
        public double? ValorCubierto { get; set; }
        public double? ValorPosible { get; set; }
        public double? ValorTotal { get; set; }
        public bool AplicaPagare { get; set; }
    }
}
