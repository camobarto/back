﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.ViewModel.Sicav
{
    public class ViewModelReferenciasAsociado
    {
        public long IdReferenciaAsociado { get; set; }                                  
        public string Nombre { get; set; }
        public string Relacion { get; set; }
        public string Numero { get; set; }
        public byte IdTipoReferencia { get; set; }
        public string TipoReferencia { get; set; }
        public bool? Confirmado { get; set; }
    }
}
