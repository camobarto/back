﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura
{
    public class InformacionEstadisticaModel
    {
        public Nullable<int> NumeroDeAsociadosHombres { get; set; }
        public Nullable<int> NumeroDeAsociadosMujeres { get; set; }
        public Nullable<int> NumeroDeAsociadosMenoresDe14Anos { get; set; }
        public Nullable<int> NumeroDeAsociadosPersonasJuridicas { get; set; }
        public Nullable<int> TotalNumeroDeAsociadosDeLaEntidadSolidaria { get; set; }
        public Nullable<int> NumeroDeAsociadosQueIngresaronEnElPeriodo { get; set; }
        public Nullable<int> NumeroDeAsociadosRetiradosEnElPeriodo { get; set; } 
        public Nullable<int> NumeroDeAsociadosPeriodoAnterior { get; set; }
        public Nullable<int> NumeroDePrestamosConcedidosEnElPeriodoDeReporte { get; set; }
        public Nullable<double> ValorPrestamosConcedidosEnElPeriodoDeReporte { get; set; } 
        public Nullable<int> PoseePolizaDeVidaDeudores { get; set; }
        public Nullable<int> NumeroTotalDeEmpleadosMujeresDeLaEntidadSolidaria { get; set; }
        public Nullable<int> NumeroTotalDeEmpleadosHombresDeLaEntidadSolidaria { get; set; }
        public Nullable<int> NumeroDeEmpleadosDeLaEntidadSolidaria { get; set; }
        public Nullable<int> NumeroDeSucursalesYAgenciasDeLaEntidadSolidaria { get; set; }
        public Nullable<int> NumeroDeOficinasPorCorresponsaliasDeLaEntidadSolidaria { get; set; }
        public Nullable<int> ObligacionesConElFondoNacionalDelCafeCooperativasCafeteras { get; set; }
        public Nullable<int> CapitalMinimoIrreducibleEnPesosEstablecidoEnElEstatuto { get; set; }
        public Nullable<int> RelacionDeSolvenciaRequeridaPorNorma { get; set; }
        public Nullable<int> RelacionDeSolvenciaCalculadoPorLaEntidadParaElPeriodo { get; set; }
        public Nullable<int> TotalActivosPonderadosPorNivelDeRiesgo { get; set; }
        public Nullable<int> PatrimonioTecnico { get; set; }
        public Nullable<int> PorcentajeDeExcedentesQuePonderaParaElCalculoDeLaRelacionDeSolvencia { get; set; }
        public Nullable<int> TasaPromedioEfectivaPonderadaDeColocacionDeCartera { get; set; }
        public Nullable<int> TasaPromedioEfectivaPonderadaDeCaptacionDeCuentasDeAhorro { get; set; }
        public Nullable<int> TasaPromedioEfectivaPonderadaDeCaptacionDeCdatS { get; set; }
        public Nullable<int> TieneExcepcionParaEjercerActividadFinanciera { get; set; }
        public Nullable<int> NumeroDeAsociadosQueAsistieronALaUltimaAsamblea { get; set; }

    }
}
