﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Utilidades
{
    public static class LimpiarExcepciones
    {
        public static void LimpiarExcepcion(Exception ex, ref List<string> mensaje) {            
            if (ex.InnerException != null) {
                mensaje.Add(ex.InnerException.Message);
                LimpiarExcepcion(ex.InnerException, ref mensaje);
            }            
        }        
    }
}
