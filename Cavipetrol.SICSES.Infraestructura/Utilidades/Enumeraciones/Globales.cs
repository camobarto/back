﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones
{
    public class Globales
    {
        public enum enumPapelCavipetrol
        {
            Activo = 1,
            Convenio = 2,
            Empleado = 3,
            Empleado_ECP = 4,
            Empleado_Defunci = 5,
            Empleado_Jub = 6,
            Empleado_Temp = 7,
            Ex_empleado = 8, 
            Jub_Afil = 9,
            Jub_Desa = 10,
            Jub_No_Afil = 11,
            Jub_Reafil = 12,
            Outsourcing = 13,
            Producto = 14,
            Retirado = 15,
            Retirado_Defunci = 16,
            Retirado_Expulsa = 17,
            Temporal_ECP = 18,
            VLDBicenActivo = 19,
            VLDOcensaActivo = 20,
            VLDOcensaRet = 21,
        }
    }
}
