﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones
{
    public class Creditos
    {
        public enum enumTipoPoliza{
            Interna = 1,
            Externa = 2
        }
        public enum enumTipoSeguro {
            Seguro_de_vida = 1,
            Seguro_de_incendio_y_terremoto = 2
        }
        public enum enumTiposCupo {
            SMMLV = 1,
            PORC_DE_CUPO_SOBRE_APORTES_DIRECTOS_SYP_RETORNO_INV_2010 = 2,
            SALARIO_EMPLEADO = 3
        }
        public enum enumEstadosSolicitudCredito{
            CREADO = 1,
            ANALISIS = 2,
            DEVUELTO = 3,
            APROBACION_EN_TRAMITE = 4,
            PENDIENTE = 5,
            COMITE = 6,
            APROBADO = 7,
            NEGADO = 8,
            LEGALIZACION_DE_GARANTIAS = 9,
            DESEMBOLSADO = 10,
            LEGALIZACION_DEL_DESEMBOLSO = 11,
            CANCELADO = 12,
            FINALIZADO = 13,
            ANALISIS_ASEGURABILIDAD = 14,
            MENOR_VALOR = 15,
        }
        public enum enumLineasCredito
        {
           Vivienda = 1,
           Consumo = 2            
        }
        public enum enumTiposCredito
        {
            Libre_Inversion = 1,
            Consumo = 2,
            Adicional = 3,
            Bienestar = 4,
            Educativo = 5,
            Cavifacil = 6,
            Vivienda = 7,
            FomentoDesarrolloEmpresarial = 8,
            Calamidad = 9,
            Pignoración = 10,
            ViviendaOrdinario = 11,
            ViviendaAdicional = 12,
            Ordinario = 13,
            Extraordinario = 14,
            LargoPlazo = 15,
            Libre = 16,
            CompraCarteraVivienda = 20,
            CompraCarteraConsumo = 21
        }
        public enum enumFormCapacidadPagoGrupos
        {
            Ingresos = 1 ,
            Descuentos_por_ley = 2,
            Otras_Deducciones = 3,
            Otros_Ingresos = 4,
            Gastos = 5,
            Ingreso_Neto_Disponible = 6,
            Maximo_Descuento_por_Nomina_y_Caja = 7,
            Valor_Cuota = 8,
            Valor_Credito = 9,
            Endeudamiento_Financiero = 10,
            Simulador_de_Cuota = 11,
            Solvencia = 12
            
        }
        public enum enumFormCapacidadPago
        {
            Salario_Básico = 1,
            Subsidio_de_Arriendo_ = 2,
            Horas_extras = 3,
            Pensión_por_sustitución = 4,
            Aporte_Salud = 5,
            Aporte_pensión = 6,
            Aporte_FDS_o_Subsistencia = 7,
            Retención_en_la_Fuente = 8,
            Otros = 9,
            Otras_deducciones = 10,
            Otros_Ingresos = 11,
            Gastos_financieros = 12,
            Gastos_Familiares = 13,
            Ingreso_Disponible = 14,
            Ingreso_Familiar_Disponible_Total = 15,
            Factor_ingreso_Disponible__Ingreso_Total = 16,
            Ingreso_Neto_Vivienda = 17,
            Máximo_descuento_de_nomina = 18,
            Máximo_valor_pago_por_caja = 19,
            Valor_Cuota_cierre_Financiero = 20,
            Plazo_anos_valorCredito = 21,
            Plazo_meses_valorCredito = 22,
            Tasa_Credito_EA_Vivienda = 23,
            Tasa_Credito_N_M_V = 24,
            Valor_Maximo_Vivienda_No_VIS = 25,
            Valor_Maximo_Vivienda_VIS = 26,
            Total_Ingresos = 27,
            Total_Deuda_Financiera = 28,
            Valor_solicitado_de_credito = 29,
            Plazo_anos_simulador = 30,
            Plazo_meses_simulador = 31,
            TASA_E_A = 32,
            TASA_NMV = 33,
            Valor_Credito_Vivienda = 34,
            Subtotal_Ingresos_menos_descuentos = 35,
            Total_ingresos_brutos = 36,
            Tipo_de_credito = 37,
            Tasa_Credito_EA_Consumo = 38,
            Valor_maximo_por_capacidad_de_pago = 39,
            Activo = 40,
            Pasivo = 41,
            Patrimonio = 42,
            Solvencia = 43,
            Nomina = 44,
            Caja = 45,
            Cuota_Cierre_Final = 46,
            smmlv = 47
        }
        public enum enumTiposCreditoTiposRolRelacion
        {
            ActivoLínea_única_de_vivienda = 1,            
            JubiladoLínea_única_de_vivienda = 2,
            ActivoLibre_Inversión = 3,
            JubiladoLibre_Inversión = 4,
            ActivoConsumo = 5,
            JubiladoConsumo = 6,
            ActivoAdicional_de_consumo = 7,
            JubiladoAdicional_de_consumo = 8,
            ActivoBienestar = 9,
            JubiladoBienestar = 10,
            ActivoFomento_y_desarrollo_empresarial = 11,
            JubiladoFomento_y_desarrollo_empresarial = 12,
            ActivoEducativo = 13,
            JubiladoEducativo = 14,
            ActivoCavifácil = 15,
            JubiladoCavifácil = 16,
            ActivoCalamidad = 17,
            JubiladoCalamidad = 18,
            ActivoPignoración = 19,
            JubiladoPignoración = 20,
            TemporalLínea_única_de_vivienda = 21,
            TemporalLibre_Inversión = 22,
            TemporalConsumo = 23,
            TemporalAdicional_de_consumo = 24,
            TemporalBienestar = 25,
            TemporalFomento_y_desarrollo_empresarial = 26,
            TemporalEducativo = 27,
            TemporalCalamidad = 28,
            TemporalPignoración = 29,
            EmpleadoViviendaAdicional = 30,
            EmpleadoViviendaOrdinario = 34
        }
        public enum enumFormaPago {
            Quincenal = 1,
            Mensual = 2,
            Semestral = 3,
        	Anual = 4
        }
        public enum enumTipoCapacidadPago {
            A_Vivienda,
            B_Vivienda,
            C_Vivienda,
            D_Vivienda,
            E_Vivienda,
            A_Consumo,
            B_Consumo,
            C_Consumo,
            D_Consumo,
            E_Consumo
        }
        public enum enumTipoGarantia {
            Real = 1,
            Real2 = 2,
            Personal = 3,
            Pagare = 4,
            Pagare_en_blanco = 5,
            Pagare_Provisional = 6
        }
    }
}
