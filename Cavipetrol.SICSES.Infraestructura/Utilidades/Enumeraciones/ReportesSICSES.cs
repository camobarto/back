﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cavipetrol.SICSES.Infraestructura.Utilidades.Enumeraciones
{
    public class ReportesSICSES
    {
        public enum enumReportesSICSES
        {
            ReporteAplicacionExcedentes = 3,
            ReporteRelacionDeInversion = 17,
            ReporteindividualDeCarteraDeCredito = 19,
            ReporteDeudoresPorVenta = 20,
            ReporteFondoDeLiquidez = 27,
            ReporteEvaluacionRiesgoLiquidez = 29,
            ReporteInversionEducacionFormal = 37,
            ReporteInformacionNoReportada = 38,
            ReporteInfoGrupoInteres = 40,
            ReporteActivosReferidos = 41,
            ReporteActivosCastigados = 43,
            ReporteInformeDeudoresPatronalesyEmpresas = 46,
            ReporteCompraVentaCredito = 47,
            CreditosBancosyFinanciero = 48,
            ReporteInformeCuentasPorPagarOtras = 49,
            ReporteProcesosJudiciales = 50,
            ReporteIngresosRecibidosParaTerceros = 51,
            ReporteContratacion = 52,
            ReporteRetiroeingresoAsociados = 53,
            ReporteInformeErogacionesOrganosDeAdministracionyControl = 56
        }
    }
}
